﻿$PBExportHeader$w_microhelp.srw
$PBExportComments$Copiata dalle PC per eliminare il controllo delle risorse, modifica anche sulla data/ora
forward
global type w_microhelp from window
end type
type st_time from statictext within w_microhelp
end type
end forward

global type w_microhelp from window
integer x = 1797
integer y = 1800
integer width = 1097
integer height = 52
boolean border = false
windowtype windowtype = popup!
long backcolor = 12632256
st_time st_time
end type
global w_microhelp w_microhelp

type prototypes

end prototypes

type variables
INTEGER		i_FrameWidth	
INTEGER		i_FrameHeight

INTEGER		i_NumberResources
STATICTEXT	i_ResourceText[3]
RECTANGLE	i_ResourceRect[3]

INTEGER		i_WindowWidth
INTEGER		i_ClockX

LONG		i_Green
LONG		i_Yellow
LONG		i_Red

WINDOW		i_MDIFrame
INTEGER		i_UpdateSeconds
BOOLEAN	i_ShowResources	= FALSE
BOOLEAN	i_ShowClock	= TRUE

BOOLEAN	i_ResourcesVisible
BOOLEAN	i_ClockVisible
BOOLEAN	i_Visible
end variables

forward prototypes
public subroutine fw_setposition ()
public subroutine fw_updateindicators ()
end prototypes

public subroutine fw_setposition ();//******************************************************************
//  PC Module     : w_MicroHelp
//  Subroutine    : fw_SetPosition
//  Description   : Moves the w_MicroHelp window when the MDI
//                  Frame moves or changes size.  If the 
//                  PowerClass w_MDI_Frame in not being used, this
//                  routine must be called by the developer from
//                  the Resize and pbm_Move events on the MDI Frame.
//                  (Note: the developer will have to define the
//                  pbm_Move event on the MDI Frame because it is
//                  not one of default events defined by
//                  PowerBuilder).
//
//  Parameters    : (None)
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Idx
INTEGER  l_RightX, l_BottomY, l_XPos, l_YPos, l_Width

//------------------------------------------------------------------
//  Become invisible while we set the position.  We can't use
//  the ".Visible" attribute because when we set it back to TRUE
//  the window becomes activated and grabs focus.
//------------------------------------------------------------------

Move(-20000, -20000)

//------------------------------------------------------------------
//  Assume what the developer requested will be visible.
//------------------------------------------------------------------

i_ResourcesVisible = i_ShowResources
i_ClockVisible     = i_ShowClock

//------------------------------------------------------------------
//  Copy the original size of the window.
//------------------------------------------------------------------

l_Width = i_WindowWidth

//------------------------------------------------------------------
//  See if we are too big for the MDI Frame.  If we are, turn
//  the resource indicator off.
//------------------------------------------------------------------

IF i_MDIFrame.WorkSpaceWidth() < 1.5 * l_Width THEN
   i_ResourcesVisible = FALSE
END IF

//------------------------------------------------------------------
//  If only one of the indicators is shown, cut the window size
//  in half.
//------------------------------------------------------------------

IF i_ResourcesVisible THEN
   IF NOT i_ClockVisible THEN
      l_Width = i_WindowWidth / 2
   END IF
ELSE
   l_Width = i_WindowWidth / 2
END IF

//------------------------------------------------------------------
//  See if we are still too big for the MDI Frame.
//------------------------------------------------------------------

IF (i_MDIFrame.WorkSpaceWidth() < 1.5 * l_Width) THEN
   i_ResourcesVisible = FALSE
   i_ClockVisible     = FALSE
ELSE
   IF i_Visible THEN
      SetRedraw(FALSE)
   END IF

   //---------------------------------------------------------------
   //  Set the visibility of the Resource Indicators.
   //---------------------------------------------------------------

   FOR l_Idx = 1 TO i_NumberResources
      i_ResourceText[l_Idx].Visible = i_ResourcesVisible
      i_ResourceRect[l_Idx].Visible = i_ResourcesVisible
   NEXT

   //---------------------------------------------------------------
   //  Set the visibility of the Clock.
   //---------------------------------------------------------------

   IF i_ClockVisible THEN
      IF NOT i_ResourcesVisible THEN
         st_Time.X = i_ClockX - i_WindowWidth / 2
      ELSE
         st_Time.X = i_ClockX
      END IF

      IF NOT st_Time.Visible THEN
         st_Time.Visible = i_ClockVisible
      END IF
   ELSE
      IF st_Time.Visible THEN
         st_Time.Visible = i_ClockVisible
      END IF
   END IF

   //---------------------------------------------------------------
   //  fw_UpdateIndicators() will turn redraw back on.
   //---------------------------------------------------------------

   fw_UpdateIndicators()

   //---------------------------------------------------------------
   //  Update the position and size.
   //---------------------------------------------------------------

   l_RightX  = i_MDIFrame.WorkSpaceX() + &
               i_MDIFrame.WorkSpaceWidth()

   l_BottomY = i_MDIFrame.WorkSpaceY() + &
               i_MDIFrame.WorkSpaceHeight()

   l_XPos    = l_RightX  - l_Width - i_FrameWidth
   l_YPos    = l_BottomY - Height  - i_FrameHeight

   Resize(l_Width, Height)
   Move(l_XPos,  l_YPos)
END IF

RETURN
end subroutine

public subroutine fw_updateindicators ();//******************************************************************
//  PO Module     : w_MicroHelp
//  Subroutine    : fw_UpdateIndicators
//  Description   : Updates the indicators on the w_MicroHelp
//                  window.  Typically will not be called by
//                  the developer.
//
//  Parameters    : (None)
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

UNSIGNEDINT  l_Resources[]
INTEGER      l_Idx, l_NumResources

IF i_Visible THEN
   SetRedraw(FALSE)
END IF

//------------------------------------------------------------------
//  If the resources are displayed, update them.
//------------------------------------------------------------------

//IF i_ResourcesVisible THEN
//   w_POManager_Std.EXT.fu_GetResources(l_Resources[])
//
//   FOR l_Idx = 1 TO i_NumberResources
//      CHOOSE CASE l_Resources[l_Idx]
//         CASE 0 to 29
//            i_ResourceRect[l_Idx].FillColor = i_Red
//         CASE 30 to 59
//            i_ResourceRect[l_Idx].FillColor = i_Yellow
//         CASE ELSE
//            i_ResourceRect[l_Idx].FillColor = i_Green
//      END CHOOSE
//   NEXT
//END IF

//------------------------------------------------------------------
//  If the clock is displayed, update the time.
//------------------------------------------------------------------

IF i_ClockVisible THEN
   st_Time.Text = String(Today(), "[ShortDate]") + "  " + &
                  String(Now(),   "[Time]")
END IF

IF i_Visible THEN
   SetRedraw(TRUE)
END IF

RETURN
end subroutine

on timer;//******************************************************************
//  PO Module     : w_MicroHelp
//  Event         : Timer
//  Description   : Updates the w_MicroHelp window.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

i_Visible = TRUE
fw_UpdateIndicators()
end on

event open;//******************************************************************
//  PO Module     : w_MicroHelp
//  Event         : Open
//  Description   : Initializes the w_MicroHelp window.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER l_Idx
STRING  l_Label[]

//------------------------------------------------------------------
//  Make sure the timer is off.
//------------------------------------------------------------------

Timer(0)

//------------------------------------------------------------------
//  Become "invisible".
//------------------------------------------------------------------

Move(-20000, -20000)

//------------------------------------------------------------------
//  Initialize instance variables.
//------------------------------------------------------------------

i_MDIFrame         = w_POManager_STD.PARM.MDI_Frame
i_UpdateSeconds    = w_POManager_STD.PARM.Update_Seconds
i_ShowClock        = w_POManager_STD.PARM.Show_Clock
i_ShowResources    = w_POManager_STD.PARM.Show_Resources

i_Visible          = FALSE
i_ResourcesVisible = FALSE
i_ClockVisible     = FALSE

i_WindowWidth      = Width
i_ClockX           = st_Time.X

//i_Green            = RGB(  0, 255,   0)
//i_Yellow           = RGB(255, 255,   0)
//i_Red              = RGB(255,   0,   0)
//
//i_ResourceText[1]  = st_res1
//i_ResourceText[2]  = st_res2
//i_ResourceText[3]  = st_res3
//
//i_ResourceRect[1]  = r_res1
//i_ResourceRect[2]  = r_res2
//i_ResourceRect[3]  = r_res3

//i_NumberResources = w_POManager_Std.EXT.fu_DefineResources(l_Label[])
//FOR l_Idx = 1 TO i_NumberResources
//   i_ResourceText[l_Idx].Text = l_Label[l_Idx]
//NEXT

w_POManager_Std.EXT.fu_GetFrameSize(i_FrameWidth, i_FrameHeight)

//---------------------------------------------------
//  Only set up the timer if services are required.
//  This functionality is required by w_MDI_Frame.
//---------------------------------------------------

IF i_ShowResources OR i_ShowClock THEN
   fw_SetPosition()
   Timer(i_UpdateSeconds, THIS)
   PostEvent("Timer")
END IF

end event

on clicked;//******************************************************************
//  PO Module     : w_MicroHelp
//  Event         : Clicked
//  Description   : Flips the indicator being displayed.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

IF i_ShowResources AND NOT i_ShowClock OR &
   NOT i_ShowResources AND i_ShowClock THEN
   i_ShowResources = (NOT i_ShowResources)
   i_ShowClock     = (NOT i_ShowClock)
   fw_SetPosition()
ELSE
   fw_UpdateIndicators()
END IF

i_MDIFrame.SetFocus()
end on

on w_microhelp.create
this.st_time=create st_time
this.Control[]={this.st_time}
end on

on w_microhelp.destroy
destroy(this.st_time)
end on

type st_time from statictext within w_microhelp
integer x = 503
integer width = 526
integer height = 60
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
boolean enabled = false
string text = "12/12/12   09:12 pm"
alignment alignment = right!
boolean focusrectangle = false
end type

