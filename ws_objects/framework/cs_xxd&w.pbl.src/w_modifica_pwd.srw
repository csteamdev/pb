﻿$PBExportHeader$w_modifica_pwd.srw
$PBExportComments$Window modifica password con inserimento vecchia pwd
forward
global type w_modifica_pwd from window
end type
type st_6 from statictext within w_modifica_pwd
end type
type st_5 from statictext within w_modifica_pwd
end type
type st_4 from statictext within w_modifica_pwd
end type
type sle_vecchia_pwd from singlelineedit within w_modifica_pwd
end type
type st_3 from statictext within w_modifica_pwd
end type
type sle_conferma_pwd from singlelineedit within w_modifica_pwd
end type
type sle_pwd from singlelineedit within w_modifica_pwd
end type
type cb_annulla from commandbutton within w_modifica_pwd
end type
type cb_ok from commandbutton within w_modifica_pwd
end type
type st_2 from statictext within w_modifica_pwd
end type
type st_1 from statictext within w_modifica_pwd
end type
end forward

global type w_modifica_pwd from window
integer width = 1920
integer height = 1000
boolean titlebar = true
string title = "Impostazione Password"
windowtype windowtype = response!
long backcolor = 12632256
boolean center = true
st_6 st_6
st_5 st_5
st_4 st_4
sle_vecchia_pwd sle_vecchia_pwd
st_3 st_3
sle_conferma_pwd sle_conferma_pwd
sle_pwd sle_pwd
cb_annulla cb_annulla
cb_ok cb_ok
st_2 st_2
st_1 st_1
end type
global w_modifica_pwd w_modifica_pwd

on w_modifica_pwd.create
this.st_6=create st_6
this.st_5=create st_5
this.st_4=create st_4
this.sle_vecchia_pwd=create sle_vecchia_pwd
this.st_3=create st_3
this.sle_conferma_pwd=create sle_conferma_pwd
this.sle_pwd=create sle_pwd
this.cb_annulla=create cb_annulla
this.cb_ok=create cb_ok
this.st_2=create st_2
this.st_1=create st_1
this.Control[]={this.st_6,&
this.st_5,&
this.st_4,&
this.sle_vecchia_pwd,&
this.st_3,&
this.sle_conferma_pwd,&
this.sle_pwd,&
this.cb_annulla,&
this.cb_ok,&
this.st_2,&
this.st_1}
end on

on w_modifica_pwd.destroy
destroy(this.st_6)
destroy(this.st_5)
destroy(this.st_4)
destroy(this.sle_vecchia_pwd)
destroy(this.st_3)
destroy(this.sle_conferma_pwd)
destroy(this.sle_pwd)
destroy(this.cb_annulla)
destroy(this.cb_ok)
destroy(this.st_2)
destroy(this.st_1)
end on

type st_6 from statictext within w_modifica_pwd
integer x = 23
integer y = 780
integer width = 1829
integer height = 140
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "Caratteri Utilizzabili  ! # $ % & ( ) * + , - . / : ; < > = ? @ [ ] \ _ | { }"
boolean focusrectangle = false
end type

type st_5 from statictext within w_modifica_pwd
integer x = 23
integer y = 620
integer width = 1829
integer height = 160
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "La password deve essere di almeno 8 caratteri e contenere Maiuscole, Minuscole, Numeri e un Carattere."
boolean focusrectangle = false
end type

type st_4 from statictext within w_modifica_pwd
integer y = 480
integer width = 1829
integer height = 120
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
long textcolor = 255
long backcolor = 12632256
string text = "Attenzione: la nuova password deve essere diversa dalla password in scadenza"
boolean focusrectangle = false
end type

type sle_vecchia_pwd from singlelineedit within w_modifica_pwd
integer x = 731
integer y = 40
integer width = 1097
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean border = false
boolean password = true
integer limit = 15
end type

type st_3 from statictext within w_modifica_pwd
integer x = 160
integer y = 40
integer width = 512
integer height = 64
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Vecchia password:"
boolean focusrectangle = false
end type

type sle_conferma_pwd from singlelineedit within w_modifica_pwd
integer x = 731
integer y = 280
integer width = 1097
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean border = false
boolean password = true
integer limit = 15
end type

type sle_pwd from singlelineedit within w_modifica_pwd
integer x = 731
integer y = 160
integer width = 1097
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean border = false
boolean password = true
integer limit = 15
end type

type cb_annulla from commandbutton within w_modifica_pwd
integer x = 1463
integer y = 380
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;close(parent)
end event

type cb_ok from commandbutton within w_modifica_pwd
integer x = 1074
integer y = 380
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Ok"
boolean default = true
end type

event clicked;n_cst_crypto lnv_crypt

// *** Michela 28/11/2007: controllo che la vecchia password corrisponda
if sle_vecchia_pwd.text <> s_cs_xx.parametri.parametro_s_2 then
	g_mb.messagebox( "APICE", "Attenzione: la vecchia password inserita non è stata digitata correttamente!", stopsign!)
	return 
end if

// *** la vecchia password deve essere diversa da quella nuova
if sle_vecchia_pwd.text = sle_pwd.text then
	g_mb.messagebox( "APICE", "Attenzione: la nuova password deve essere diversa dalla password in scadenza. Digitare una nuova password!", stopsign!)
	return
end if

// *** la nuova password e quella di conferma devono corrispondere
if sle_pwd.text <> sle_conferma_pwd.text then
	g_mb.messagebox("APICE","La nuova password non è stata confermata correttamente. Assicurarsi di confermare esattamente la nuova password.",stopsign!)
	return
end if

lnv_crypt = CREATE n_cst_crypto

if lnv_crypt.uof_pwd_strenght(sle_pwd.text) = 0 then
	// validazione non superata
	g_mb.messagebox("APICE","La password non è sicura.~nPer essere SICURA la password deve avere lunghezza minima di 8 caratteri e contenere maiuscole, minuscole, numeri e simboli",stopsign!)
	return
end if

if lnv_crypt.EncryptData(sle_conferma_pwd.text,s_cs_xx.parametri.parametro_s_1) < 0 then
	g_mb.messagebox("APICE","La nuova password contiene caratteri non consentiti.~nI caratteri consentiti comprendono:~n~n" + &
					"- tutte le cifre numeriche 0,1,2,...,9~n- tutte le lettere maiuscole A,B,C,...,Z e minuscole a,b,c,...,z~n" + &
					"- alcuni simboli tra cui ! # $ % & ( ) * + , - . / : ; < > = ? @ [ ] \ _ | { }~n~nModificare la password e riprovare",stopsign!)
	return
end if

DESTROY lnv_crypt

s_cs_xx.parametri.parametro_b_1 = true

close(parent)
end event

type st_2 from statictext within w_modifica_pwd
integer x = 160
integer y = 280
integer width = 558
integer height = 64
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Conferma password:"
boolean focusrectangle = false
end type

type st_1 from statictext within w_modifica_pwd
integer x = 160
integer y = 160
integer width = 462
integer height = 64
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Nuova password:"
boolean focusrectangle = false
end type

