﻿$PBExportHeader$w_news.srw
forward
global type w_news from w_cs_xx_principale
end type
type lv_news from listview within w_news
end type
type gb_1 from groupbox within w_news
end type
end forward

global type w_news from w_cs_xx_principale
integer width = 1714
integer height = 1432
string title = "News"
boolean minbox = false
boolean maxbox = false
boolean resizable = false
lv_news lv_news
gb_1 gb_1
end type
global w_news w_news

forward prototypes
public function integer wf_carica_news ()
end prototypes

public function integer wf_carica_news ();boolean  lb_true = true

string	ls_titolo, ls_messaggio

datetime ldt_today, ldt_data_inizio

listviewitem llv_item


lv_news.deleteitems()

ldt_today = datetime(today(),00:00:00)

declare news cursor for
select	data_inizio,
			des_news,
		 	messaggio
from	 	news
where	 	cod_azienda = :s_cs_xx.cod_azienda and
		 	data_inizio <= :ldt_today and
		 	(data_fine is null or
			 data_fine > :ldt_today) and
			(cod_area_aziendale is null or
			 cod_area_aziendale in (select cod_area_aziendale
			 								from 	 mansionari
											where  cod_azienda = :s_cs_xx.cod_azienda and
													 cod_utente = :s_cs_xx.cod_utente) or
			 :s_cs_xx.admin = :lb_true)
order by data_inizio ASC, des_news ASC;

open news;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("NEWS","Errore in open cursore news: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

do while true
	
	fetch news
	into	:ldt_data_inizio,
			:ls_titolo,
			:ls_messaggio;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("NEWS","Errore in fetch cursore news: " + sqlca.sqlerrtext,stopsign!)
		close news;
		return -1
	elseif sqlca.sqlcode = 100 then
		close news;
		exit
	end if
	
	llv_item.data = ls_messaggio
	llv_item.label = string(date(ldt_data_inizio)) + " - " + ls_titolo
	llv_item.pictureindex = 1
	llv_item.selected = false
	
	lv_news.additem(llv_item)
	
loop

return 0
end function

on w_news.create
int iCurrent
call super::create
this.lv_news=create lv_news
this.gb_1=create gb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.lv_news
this.Control[iCurrent+2]=this.gb_1
end on

on w_news.destroy
call super::destroy
destroy(this.lv_news)
destroy(this.gb_1)
end on

event pc_setwindow;call super::pc_setwindow;string ls_path


ls_path = s_cs_xx.volume + s_cs_xx.risorse + "attenzione.bmp"

lv_news.addsmallpicture(ls_path)

setpointer(hourglass!)

if wf_carica_news() <> 0 then
	lv_news.deleteitems()
end if

setpointer(arrow!)

timer(300)
end event

event timer;call super::timer;setpointer(hourglass!)

if wf_carica_news() <> 0 then
	lv_news.deleteitems()
end if

setpointer(arrow!)
end event

type lv_news from listview within w_news
integer x = 69
integer y = 100
integer width = 1554
integer height = 1180
integer taborder = 30
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string pointer = "HyperLink!"
long textcolor = 33554432
long backcolor = 12632256
boolean border = false
boolean fixedlocations = true
boolean labelwrap = false
listviewview view = listviewlist!
long largepicturemaskcolor = 536870912
long smallpicturemaskcolor = 536870912
long statepicturemaskcolor = 536870912
end type

event clicked;listviewitem llv_item


if isnull(index) or index = 0  or index < 0 then
	return -1
end if

getitem(index,llv_item)

g_mb.messagebox(llv_item.label,string(llv_item.data))
end event

type gb_1 from groupbox within w_news
integer x = 23
integer y = 20
integer width = 1646
integer height = 1300
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Elenco News"
borderstyle borderstyle = stylelowered!
end type

