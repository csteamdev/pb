﻿$PBExportHeader$w_nuovo_url.srw
forward
global type w_nuovo_url from w_cs_xx_risposta
end type
type cb_annulla from commandbutton within w_nuovo_url
end type
type cb_inserisci from commandbutton within w_nuovo_url
end type
type sle_url from singlelineedit within w_nuovo_url
end type
end forward

global type w_nuovo_url from w_cs_xx_risposta
integer width = 2427
integer height = 140
boolean titlebar = false
string title = ""
boolean controlmenu = false
cb_annulla cb_annulla
cb_inserisci cb_inserisci
sle_url sle_url
end type
global w_nuovo_url w_nuovo_url

type variables
boolean ib_chiudi = false
end variables

on w_nuovo_url.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.cb_inserisci=create cb_inserisci
this.sle_url=create sle_url
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.cb_inserisci
this.Control[iCurrent+3]=this.sle_url
end on

on w_nuovo_url.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.cb_inserisci)
destroy(this.sle_url)
end on

event closequery;call super::closequery;if not(ib_chiudi) then
	return 1
end if
end event

event pc_setwindow;call super::pc_setwindow;sle_url.text = s_cs_xx.parametri.parametro_s_1
end event

type cb_annulla from commandbutton within w_nuovo_url
integer x = 2011
integer y = 20
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
boolean cancel = true
end type

event clicked;s_cs_xx.parametri.parametro_d_1 = -1

setnull(s_cs_xx.parametri.parametro_s_1)

ib_chiudi = true

close(parent)
end event

type cb_inserisci from commandbutton within w_nuovo_url
integer x = 1623
integer y = 20
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "OK"
boolean default = true
end type

event clicked;if sle_url.text = "" or isnull(sle_url.text) then
	g_mb.messagebox("Inserimento indirizzo web","Immettere un URL valido prima di continuare",exclamation!)
	return -1
end if

s_cs_xx.parametri.parametro_d_1 = 0

s_cs_xx.parametri.parametro_s_1 = sle_url.text

ib_chiudi = true

close(parent)
end event

type sle_url from singlelineedit within w_nuovo_url
integer x = 23
integer y = 20
integer width = 1577
integer height = 80
integer taborder = 10
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
integer limit = 100
borderstyle borderstyle = stylelowered!
end type

