﻿$PBExportHeader$w_ole_documenti.srw
$PBExportComments$Gestione Documenti Allegati ma non ole .. altrimenti non si vede niente in intranet
forward
global type w_ole_documenti from w_cs_xx_risposta
end type
type ole from olecontrol within w_ole_documenti
end type
type st_1 from statictext within w_ole_documenti
end type
type cb_oggetti from commandbutton within w_ole_documenti
end type
type st_2 from statictext within w_ole_documenti
end type
type cb_chiudi from uo_cb_close within w_ole_documenti
end type
type st_3 from statictext within w_ole_documenti
end type
type st_4 from statictext within w_ole_documenti
end type
type cb_salva from commandbutton within w_ole_documenti
end type
end forward

global type w_ole_documenti from w_cs_xx_risposta
integer width = 2391
integer height = 2264
string title = "Documento"
ole ole
st_1 st_1
cb_oggetti cb_oggetti
st_2 st_2
cb_chiudi cb_chiudi
st_3 st_3
st_4 st_4
cb_salva cb_salva
end type
global w_ole_documenti w_ole_documenti

type prototypes
FUNCTION long ShellExecuteA(long hwnd, string lpOperation, string lpFile, string lpParameters, string lpDirectory, long nShowCmd) LIBRARY "SHELL32.DLL" ALIAS FOR "ShellExecuteA;ansi"
end prototypes

type variables
string is_file_inizio // eventuale path del documento già inserito 

string is_file_corrente      // documento corrente aperto (attenzione, tutto il path!)

string is_path

long   il_prog_mimetype

blob   ib_blob
end variables

forward prototypes
public function string wf_nome_file (string ws_path_file)
public function blob wf_carica_blob (string fs_path)
public subroutine wf_elimina_file ()
public subroutine wf_imposta_dir ()
public subroutine wf_imposta_file (long fl_prog_mimetype, blob fb_blob)
end prototypes

public function string wf_nome_file (string ws_path_file);if (pos(ws_path_file, "\") > 0) and (not isnull(pos(ws_path_file, "\") > 0)) then
	do
		ws_path_file = mid(ws_path_file, pos(ws_path_file, "\") + 1)
	loop while (pos(ws_path_file, "\") > 0) and (not isnull(pos(ws_path_file, "\") > 0))
end if
return ws_path_file
end function

public function blob wf_carica_blob (string fs_path);long ll_filelen, ll_bytes_read, ll_new_pos
integer li_counter, li_loops, fh1

blob lb_tempblob, lb_bufferblob, lb_resultblob, lb_blankblob
		
// Trova la lunghezza del file
ll_filelen = FileLength(fs_path)

// FileRead legge 32KB alla volta => Se il file supera i 32 KB, SERVONO PIU' CICLI
// Calcola il numero di cicli
IF ll_filelen > 32765 THEN 
	IF Mod(ll_filelen,32765) = 0 THEN 
		li_loops = ll_filelen/32765 
	ELSE 
		// ...se c'è il resto, serve un ciclo in più
		li_loops = (ll_filelen/32765) + 1 
	END IF 
ELSE 
	li_loops = 1 
END IF

//carico il blob
fh1 = FileOpen(fs_path, StreamMode!)
if fh1 <> -1 then
	ll_new_pos = 0
	// Esegue li_loops cicli
	FOR li_counter = 1 to li_loops 
		// Legge i dati in una variabile TEMPORANEA, di tipo BLOB
		ll_bytes_read = FileRead(fh1, lb_tempblob) 
		// ...e li accoda in un secondo BLOB
		lb_bufferblob = lb_bufferblob + lb_tempblob 
		ll_new_pos = ll_new_pos + ll_bytes_read 
		// Sposta il puntatore al file nella posizione di lettura successiva
		FileSeek(fh1,ll_new_pos,FROMBEGINNING!)
		// Accoda i segmenti alla cache fino a costituire una "tranche" di circa 1 mega
		if len(lb_bufferblob) > 1000000 then
			// Se la dimensione limite è superata, accoda i dati in un terzo BLOB "risultato"
			lb_resultblob = lb_resultblob + lb_bufferblob 
			// ...e "sega" il BLOB temporaneo 2 assegnandoli un blob nullo
			lb_resultblob = lb_blankblob 
		end if 
	NEXT
	// Accoda i dati restanti in total_blob
	lb_resultblob = lb_resultblob + lb_bufferblob 
	// Fine lettura
	FileClose(fh1)
else
	g_mb.messagebox("OMNIA","Attenzione: Errore nell'apertura del file!")	
	return lb_resultblob	
end if

return lb_resultblob


end function

public subroutine wf_elimina_file ();if is_file_corrente <> "" and not isnull(is_file_corrente) then
	
	if filedelete(is_file_corrente) <> true then
		
		g_mb.messagebox("OMNIA","Impossibile cancellare il file " + is_file_corrente)	
		
	end if
	
end if

return
end subroutine

public subroutine wf_imposta_dir ();//string  ls_dir, ls_path, ls_valore, ls_str, ls_vol, ls_ris
//integer li_risposta
//
//li_risposta = Registryget(s_cs_xx.chiave_root + "applicazione_" +  s_cs_xx.profilocorrente, "VQL", ls_dir)	  // parametro registro VQL
//
//setnull(is_path)
//
//if isnull(ls_dir) then ls_dir = ""
//
//select stringa
//into   :ls_valore
//from   parametri_azienda
//where  cod_azienda = :s_cs_xx.cod_azienda and    
//       cod_parametro = 'TDD';
//
//if sqlca.sqlcode < 0 then	
//	g_mb.messagebox("OMNIA","Attenzione: Errore nella select del parametro TDD! Dettaglio: " + sqlca.sqlerrtext)	
//	return	
//end if
//
//if isnull(ls_valore) or ls_valore = "" or len(trim(ls_valore)) = 0 then // *** metto tutto nelle risorse
//
//	// *** Michela 13/03/2006: leggo il parametro VOL se non c'è TDD, cosi metto il documento dove
//	//                         sono situate anche le risorse
//	li_risposta = Registryget(s_cs_xx.chiave_root + "applicazione_" +  s_cs_xx.profilocorrente, "VOL", ls_vol)	  // parametro registro VOL
//	li_risposta = Registryget(s_cs_xx.chiave_root + "applicazione_" +  s_cs_xx.profilocorrente, "ris", ls_ris)	  // parametro registro VOL
//	ls_valore = ls_vol + ls_ris
//
//else
//	if right( ls_dir, 1) <> "\" and left(ls_valore,1) <> "\" then
//		ls_valore = ls_dir + "\" + ls_valore
//	else
//		ls_valore = ls_dir + ls_valore
//	end if
//	
//end if
//
//setnull(ls_str)
//
//ls_str = right(ls_valore, 1)
//
//if ls_str <> "\" then ls_valore = ls_valore + "\"
//
//is_path = ls_valore



is_path = guo_functions.uof_get_user_temp_folder()

return
end subroutine

public subroutine wf_imposta_file (long fl_prog_mimetype, blob fb_blob);string  ls_estensione, ls_p

long    ll_pos

integer li_FileNum

// *** cancello eventuale documento aperto

if not isnull(is_file_corrente) and is_file_corrente <> "" then
	
	filedelete(is_file_corrente)
	
end if

// *** trovo il path di dove mettere il file

wf_imposta_dir()

select estensione
into   :ls_estensione
from   tab_mimetype
where  cod_azienda = :s_cs_xx.cod_azienda	and    
       prog_mimetype = :fl_prog_mimetype;
	
if sqlca.sqlcode <> 0 or ls_estensione = "" then
	
	g_mb.messagebox("OMNIA", "Impossibile aggiornare il documento!")

	return

end if
	
ls_p = is_path + "cs" + string(Hour(Now())) + string(Minute(Now())) + string(Second(Now())) + "." + ls_estensione	
	
li_FileNum = FileOpen( ls_p, StreamMode!, Write!, Shared!, Replace!)

if li_FileNum = -1 then
	
	g_mb.messagebox("OMNIA","Errore nell'apertura del file: " + ls_p)
	
	return
	
end if	
	
ll_pos = 1

Do While FileWrite(li_FileNum,BlobMid( fb_blob, ll_pos, 32765)) > 0
	
	ll_pos += 32765
	
Loop

FileClose(li_FileNum)

is_file_corrente = ls_p

return 
	

end subroutine

event pc_setwindow;call super::pc_setwindow;string ls_nome_file

il_prog_mimetype = s_cs_xx.parametri.parametro_d_1

ib_blob = s_cs_xx.parametri.parametro_bl_1

if not isnull(ib_blob) and len(ib_blob) > 0 and il_prog_mimetype > 0 and not isnull(il_prog_mimetype) then
	
	wf_imposta_file( il_prog_mimetype, ib_blob)
	
	ole.insertfile(is_file_corrente)
				
else
	
	setnull(is_path)
	
	setnull(is_file_corrente)
	
	setnull(ib_blob)
	
	setnull(il_prog_mimetype)
		
end if

wf_imposta_dir()



end event

event close;call super::close;setpointer(hourglass!)

if not isnull(is_file_corrente) and is_file_corrente <> "" then
	
	ib_blob = wf_carica_blob(is_file_corrente)
	
	filedelete(is_file_corrente)
	
end if

s_cs_xx.parametri.parametro_bl_1  = ib_blob

s_cs_xx.parametri.parametro_d_1 = il_prog_mimetype

setpointer(arrow!)
end event

on w_ole_documenti.create
int iCurrent
call super::create
this.ole=create ole
this.st_1=create st_1
this.cb_oggetti=create cb_oggetti
this.st_2=create st_2
this.cb_chiudi=create cb_chiudi
this.st_3=create st_3
this.st_4=create st_4
this.cb_salva=create cb_salva
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.ole
this.Control[iCurrent+2]=this.st_1
this.Control[iCurrent+3]=this.cb_oggetti
this.Control[iCurrent+4]=this.st_2
this.Control[iCurrent+5]=this.cb_chiudi
this.Control[iCurrent+6]=this.st_3
this.Control[iCurrent+7]=this.st_4
this.Control[iCurrent+8]=this.cb_salva
end on

on w_ole_documenti.destroy
call super::destroy
destroy(this.ole)
destroy(this.st_1)
destroy(this.cb_oggetti)
destroy(this.st_2)
destroy(this.cb_chiudi)
destroy(this.st_3)
destroy(this.st_4)
destroy(this.cb_salva)
end on

type ole from olecontrol within w_ole_documenti
integer x = 23
integer y = 220
integer width = 2309
integer height = 1820
integer taborder = 10
borderstyle borderstyle = stylelowered!
long backcolor = 16777215
boolean focusrectangle = false
string binarykey = "w_ole_documenti.win"
omdisplaytype displaytype = displayascontent!
omcontentsallowed contentsallowed = containsany!
end type

event doubleclicked;integer li_ris

long    hwnd

hwnd = handle(parent)	

if not isnull(is_file_corrente) and is_file_corrente <> "" and len(is_file_corrente) > 0 then

	li_ris = shellexecuteA( hwnd, 'open', is_file_corrente, '', '', 1)
	
	if li_ris <= 32 then
		
		FileDelete(is_file_corrente)
		
		g_mb.messagebox("Attenzione", "Impossibile visualizzare il documento " + is_file_corrente, exclamation!, ok!)		
		
	end if		

end if
end event

type st_1 from statictext within w_ole_documenti
integer x = 686
integer y = 20
integer width = 1143
integer height = 80
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean enabled = false
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type cb_oggetti from commandbutton within w_ole_documenti
integer x = 1577
integer y = 2060
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Oggetti"
end type

event clicked;string  ls_path, ls_reverse, ls_tipof, ls_file, ls_nome_doc

integer li_FileNum

long    ll_ret, ll_pos

blob    lb_blob

ll_ret = getfileopenname( "Cerca Oggetto", ls_path, ls_file, "DOC","Microsoft Word (*.DOC),*.DOC," + &
  		                                                         + "Microsoft Excel (*.XLS),*.XLS," + &
  		                                                         + "Tutti i files (*.*),*.*")
if ll_ret < 1 then return

ll_ret = ole.InsertFile(ls_path)

if ll_ret < 0 then 
	
	g_mb.messagebox("Omnia","Errore durante il caricamento dell'oggetto OLE (ad es. il potrebbe non essere più disponibile in rete): contattare l'amministratore del sistema",StopSign!)
	
	return 0
	
end if

lb_blob = wf_carica_blob(ls_path)
	
ll_pos = 0

ls_reverse = reverse(ls_path)

ll_pos = pos(ls_reverse,".")

if ll_pos > 0 then
	
	ls_tipof = right( ls_file, (ll_pos - 1))
	
	ls_tipof = upper(ls_tipof)
	
else
	
	setnull(ls_tipof)
	
	g_mb.messagebox("Omnia","Attenzione, il tipo di documento non è codificato! Impossibile effettuare l'inserimento.",stopsign!)
	
	rollback;
	
	return 0
	
end if

ls_nome_doc = "cs" + string(Hour(Now())) + string(Minute(Now())) + string(Second(Now())) + "." + ls_tipof

wf_elimina_file()

ls_nome_doc = is_path + ls_nome_doc

li_FileNum = FileOpen( ls_nome_doc, StreamMode!, Write!, Shared!, Replace!)

if li_FileNum = -1 then
	
	g_mb.messagebox("OMNIA","Attenzione: errore durante l'apertura del file " + ls_nome_doc)
	
	rollback;
	
	return -1
	
end if

ll_pos = 1

Do While FileWrite( li_FileNum, BlobMid( lb_blob, ll_pos, 32765)) > 0
	
	ll_pos += 32765
	
Loop

FileClose(li_FileNum)

is_file_corrente = ls_nome_doc

select prog_mimetype
into   :il_prog_mimetype
from   tab_mimetype
where  cod_azienda = :s_cs_xx.cod_azienda	and    
       upper(estensione) = :ls_tipof;
	
if isnull(il_prog_mimetype) then il_prog_mimetype = 1

return 0
end event

type st_2 from statictext within w_ole_documenti
integer x = 23
integer y = 20
integer width = 640
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Documento Allegato:"
alignment alignment = right!
long bordercolor = 8388608
boolean focusrectangle = false
end type

type cb_chiudi from uo_cb_close within w_ole_documenti
integer x = 1966
integer y = 2060
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Chiudi"
end type

type st_3 from statictext within w_ole_documenti
integer y = 120
integer width = 663
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 79741120
boolean enabled = false
string text = "Path Origine Documento:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_4 from statictext within w_ole_documenti
integer x = 686
integer y = 120
integer width = 1143
integer height = 80
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean enabled = false
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type cb_salva from commandbutton within w_ole_documenti
integer x = 1189
integer y = 2060
integer width = 366
integer height = 80
integer taborder = 21
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "&Salva su File"
end type

event clicked;integer li_FileNum,value
string docname, named
long ll_lunghezza_blob,ll_num_cicli,ll_t,ll_num_buffer
blob lbb_buffer,lbb_ole

lbb_ole = ole.objectdata
value = GetFileSaveName("Select File",  & 
	docname, named, "DOC",  &
	"Tutti i File (*.*),*.*,")
	
IF value = 1 THEN 
	ll_lunghezza_blob = len (lbb_ole)
	if ll_lunghezza_blob < 32765 then
		li_FileNum = FileOpen(docname,streamMode!, Write!, Shared!, replace!)			
		FileWrite(li_FileNum, lbb_ole)
	else
		ll_num_cicli = long(ll_lunghezza_blob/32765)
		ll_num_buffer = blobedit(lbb_ole,0,lbb_buffer)
		li_FileNum = FileOpen(docname,streamMode!, Write!, Shared!, replace!)			
		FileWrite(li_FileNum, lbb_buffer)
		fileclose(li_filenum)
		li_FileNum = FileOpen(docname,streamMode!, Write!, Shared!, append!)			
		for ll_t = 1 to ll_num_cicli
			ll_num_buffer = blobedit(lbb_ole,ll_t*32765,lbb_buffer)
			FileWrite(li_FileNum, lbb_buffer)
		next
		blobedit(lbb_ole,ll_num_buffer,lbb_buffer)
		FileWrite(li_FileNum, lbb_buffer)
		fileclose(li_filenum)
	end if

end if

end event


Start of PowerBuilder Binary Data Section : Do NOT Edit
04w_ole_documenti.bin 
2800000c00e011cfd0e11ab1a1000000000000000000000000000000000003003e0009fffe000000060000000000000000000000010000000100000000000010000000000200000001fffffffe0000000000000000fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdfffffffefffffffe00000004fffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff006f00520074006f004500200074006e00790072000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000050016ffffffffffffffff000000010003000c00000000000000c046000000000000000000000000000000ef781d9001cfb081000000030000034000000000004f00010065006c003000310061004e0069007400650076000000000000000000000000000000000000000000000000000000000000000000000000000000000102001a00000002ffffffffffffffff00000000000000000000000000000000000000000000000000000000000000000000000000000002000002890000000000430001006d006f004f0070006a006200000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000020012ffffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000000000000000004c000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ffffffffffffffffffffffff00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001fffffffe000000030000000400000005000000060000000700000008000000090000000a0000000b0000000cfffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
29fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffe000100000a03ffffffff0003000c00000000000000c0460000000000000c20454c4f6b6361500065676100000000000000086b6361500065676171b239f400000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000002857541000278656f74622e63656300746155415c3a58454f54422e434500005441310003004300000073555c3a5c737265616e6f645c636f74447070415c61746161636f4c65545c6c415c706d454f54552e4345580054414200000187485441505c3a4320444e49573b53574f575c3a434f444e49435c5357414d4d4f633b444e71735c3a796e616c775c303532336e695c3a633b616c71733035796e6e69775c41500a0d25204854485441503a633b253462705c633b6b6477705c3a74666f733462705c5c3a633b6f737770775c7466346c717369775c30500a0d6e2048544154415025633b254867645c3a737070616e69625c45530a0d51532054594e414c5c3a633d616c71733035796e6f6d0a0d6320656463206e6f7065646f20656761706572703d657261353828284320293049575c3a574f444e4f435c534e414d4d67655c4470632e610a0d296965646f6d6e6f6320646f632067617065657320657463656c3035383d656b0a0d69206279432c2c7449575c3a574f444e4f435c534e414d4d656b5c44616f6279732e64720a0d7379204d4552575c3a434f444e49435c5357414d4d4f735c444e657261686578652e3a430a0d4e49575c53574f444d4f435c444e414d6168735c652e65722f20657830353a6c662f20303031353a300a0d30430000005c003a0073005500720065005c0073006f00640061006e006f0074005c0063007000410044007000740061005c0061006f004c00610063005c006c006500540070006d0041005c005400550045004f00450058002e004300410042000c005400410000007400750065006f00650078002e006300610062000f007400630000005c003a00550041004f005400580045004300450042002e0054004100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
14w_ole_documenti.bin 
End of PowerBuilder Binary Data Section : No Source Expected After This Point
