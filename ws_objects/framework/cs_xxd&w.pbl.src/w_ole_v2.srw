﻿$PBExportHeader$w_ole_v2.srw
forward
global type w_ole_v2 from window
end type
type st_loading from statictext within w_ole_v2
end type
type cb_cancella from commandbutton within w_ole_v2
end type
type lv_documenti from listview within w_ole_v2
end type
type ddlb_style from dropdownlistbox within w_ole_v2
end type
type cb_sfoglia from commandbutton within w_ole_v2
end type
end forward

global type w_ole_v2 from window
integer width = 2496
integer height = 1424
boolean titlebar = true
boolean controlmenu = true
windowtype windowtype = response!
boolean center = true
event ue_load_documents ( )
event ue_stop_loading ( )
event ue_custom_color ( )
event pc_setwindow ( )
st_loading st_loading
cb_cancella cb_cancella
lv_documenti lv_documenti
ddlb_style ddlb_style
cb_sfoglia cb_sfoglia
end type
global w_ole_v2 w_ole_v2

type prototypes

end prototypes

type variables
public:
	// Permette di selezionare solo i file con estensioni presenti all'interno della tabella mimetype
	boolean ib_filtra_mimetype_conosciuti = true
	
	// Indica se bisogna caricare i documenti all'apertura della finestra
	boolean ib_load_document_onopen = true
	
	// Indica se il documento deve essere salvato dall'utente o solo visualizzato
	boolean ib_save_document = false	
	
	// Indica che il documento viene passato e restituito per blob
	boolean ib_document_over_blob = false
	
	long il_anno_registrazione
	long il_num_registrazione
	long il_progressivo // !!! da usare SOLO nelle tabelle che hanno 4 chiavi e non per memorizzare il progressivo corrente !!!
	string is_cod_area_aziendale
	
	
private:
	// Contiene l'estensione (doc, xls, pdf), l'indice corrisponde all'immagine nella list view
	string is_mimetype[] 
	
	// Contiene il numero di id del prog_mimetype, l'indice corrisponde all'immagine nella list view 
	//int ii_mimetype_id[] 
	
	// Contiene le estensioni ammesse per l'upload del documento
	string is_extension_filter
	
	// Contiene l'elenco dei file aperti nella cartella temporanea e che sono da cancellare alla chiusura
	// della finestra
	string is_file_downloaded[]
	
	// Caretella base delle risorse
	string is_base_path
	
	string is_loading_text = "Caricamento documenti..."
	string is_empty_text = "Nessun documento allegato"
	
	
	// Indica se può essere caricato un solo documento
	boolean ib_can_have_only_one_document = false
	
	// Indica se bisogna cancellare tutta la riga o solo il blob
	boolean ib_delete_only_blob = false
	
	// Variabile che memorizza il blob del file passato, NON USARE MAI VIENE GESTITO AUTOMATICAMETNE DALLA FUNZIONE f_documento()
	blob iblb_blob_buffer
	string is_ext_blob_buffer
	long il_prog_mimetype_buffer
	boolean ib_modified_dob = false
	
	
	boolean ib_sintexcal = false
end variables

forward prototypes
public subroutine wf_add_column ()
private function string wf_sanatize_filename (string as_filename)
public function boolean wf_rename_file (integer ai_index, ref listviewitem alv_item, str_ole astr_data, string as_new_name)
private function boolean wf_get_file_info (string as_file, ref string as_file_name, ref string as_file_ext)
private subroutine wf_load_mimetype ()
public subroutine wf_load_documents ()
public function boolean wf_upload_file (string as_file_path, string as_file_name, string as_file_ext, long al_prog_mimetype, ref blob ab_file_blob)
private function integer wf_get_mimetype_index (string as_file_ext)
public function boolean wf_download_file (integer ai_index, ref listviewitem alv_item, str_ole astr_data, ref blob ab_file_blob)
public function boolean wf_delete_file (integer ai_index, ref listviewitem alv_item, str_ole astr_data)
public function integer wf_load_mimetype_icon (string as_file_ext)
private function integer wf_get_prog_mimetype (string as_file_ext)
public function integer wf_add_document (string as_label, str_ole astr_data)
private function string wf_get_ext_from_mimetype (integer ai_prog_mimetype)
private subroutine wf_document_from_blob ()
public function integer wf_add_document (string as_label, string as_file_ext)
public subroutine wf_color ()
end prototypes

event ue_load_documents();wf_load_documents()

if lv_documenti.totalitems() < 1 then
	st_loading.text = is_empty_text
else
	event post ue_stop_loading()
end if

end event

event ue_stop_loading();st_loading.visible = false
end event

event ue_custom_color();st_loading.backcolor = rgb(255,255,255)
end event

event pc_setwindow();/**
 * Stefanop
 * 07/09/2010
 *
 * Codificare i metodi base:
 * 1. wf_download_file
 * 2. wf_upload_file
 * 3. wf_rename_file
 * 4. wf_delete_file
 * 5. wf_load_documents
 *
 * Esistono due flag che si possono usare:
 * ib_filtra_mimetype_conosciuti = true serve per poter limitare l'upload dei documenti solo per i mimetype inseriti nel db
 * ib_load_document_onopen = true carica i documenti all'apertura della finestra, chiama l'evento wf_load_documents
 * ib_save_document = true salva i file in una cartella temporanea (cancellati alla chiusura) e apre, altrimenti chiede dove salvare
 * 
 * Usare l'evento ue_stop_loading() per nascondere la scritta di caricamento documenti
 *
 * Per tutto il resto (c'è mastercard) ci pensa la finestra
 **/
 
int li_index
string ls_prova

resizable = true

//su alcune postazioni e sul terminal per sintexcal da problemi la visualizzazione per icone piccole
//all'apertura della finestra, quindi inibisco il salvataggio della modalità di visualizzazione
select stringa
into   :ls_prova
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'AZP';
		 
if sqlca.sqlcode = 0 and not isnull(ls_prova) then
	ib_sintexcal = true
else
	ib_sintexcal = false
end if



wf_color()

st_loading.text = is_loading_text
is_base_path = s_cs_xx.volume + s_cs_xx.risorse + "mimetype\"
	
// imposto finestra
wf_load_mimetype()
wf_add_column()

if not ib_sintexcal then

	// leggo modalità di visualizzazione
	select numero
	into :li_index
	from parametri_azienda_utente
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		cod_utente = :s_cs_xx.cod_utente and
		flag_parametro = 'N' and
		cod_parametro = 'OLV';
		
	if sqlca.sqlcode = 0 and not isnull(li_index) and li_index > 0 and  li_index < 3 then
		ddlb_style.selectitem(li_index)
		ddlb_style.event selectionchanged(li_index)
	else
		ddlb_style.selectitem(1)
	end if
	
else
	//su alcune postazioni e sul terminal per sintexcal da problemi la visualizzazione per icone piccole
	//all'apertura della finestra, quindi all'apertura visualizzo sempre in modalità ICONE GRANDI
	ddlb_style.selectitem(1)
end if

this.postevent("resize")
// ----

// document over blob
if s_cs_xx.parametri.parametro_s_15 = "DOB" then
	ib_document_over_blob = true
	lv_documenti.editlabels = false
	ib_delete_only_blob = true
	ib_can_have_only_one_document = true
	
	// s_cs_xx.parametri.parametro_bl_1 = BLOB
	// s_cs_xx.parametri.parametro_s_1 = file_name
	// s_cs_xx.parametri.parametro_ul_1 = prog_mimetype
	wf_document_from_blob()
else
	if ib_load_document_onopen then
		event post ue_load_documents()
	end if
end if
end event

public subroutine wf_add_column ();/**
 * Stefanop
 * 02/09/2010
 *
 * Aggiunge le colonne per la vistra report
 * al momento non viene usata
 **/
 
/*
AGGIUNGE COLONNA
lv_documenti.addcolumn("nome_colonna")

AGGIUNGE DETTAGLIO A COLONNA
lv_documenti.setitem(index_colonna, testo_da_visualizzare)
*/
end subroutine

private function string wf_sanatize_filename (string as_filename);/**
 * Stefanop
 * 02/09/2010
 *
 * Controlla che il file non contenga caratteri non ammessi come nome file
 **/
 
 
as_filename = trim(as_filename)

f_sostituzione(ref as_filename, "*", "")
f_sostituzione(ref as_filename, "|", "")
f_sostituzione(ref as_filename, "\", "")
f_sostituzione(ref as_filename, "/", "")
f_sostituzione(ref as_filename, ":", "")
f_sostituzione(ref as_filename, "<", "")
f_sostituzione(ref as_filename, ">", "")
f_sostituzione(ref as_filename, "?", "")
f_sostituzione(ref as_filename, '"', "")

return as_filename
end function

public function boolean wf_rename_file (integer ai_index, ref listviewitem alv_item, str_ole astr_data, string as_new_name);/**
 * Stefanop
 * 02/09/2010
 *
 * Codificare questo evento per rinominare il file
 **/
 
/* ESEMPIO

update nome_tabella
set des_blob = :as_new_name
where
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_registrazione = :astr_data.anno_registrazione and
	num_registrazione = :astr_data.num_registrazione and
	progressivo = :astr_data.progressivo;
	
if sqlca.sqlcode <> 0 then
	// errore durante il salvataggio
	return false
else
	// tutto ok
	return true
end if

*/

return true


end function

private function boolean wf_get_file_info (string as_file, ref string as_file_name, ref string as_file_ext);/**
 * Stefanop
 * 02/09/2010
 *
 * Ritorno l'estensione del file
 **/
 
int li_point

as_file_name = ""
as_file_ext = ""

li_point = lastpos(as_file, ".")

// se il file non ha estensione.
if isnull(li_point) or li_point < 1 then
	as_file_name = as_file
	return true
end if

as_file_name = left(as_file, li_point - 1)
as_file_ext = lower(mid(as_file, li_point + 1))
return true

end function

private subroutine wf_load_mimetype ();/**
 * Stefanop
 * 02/09/2010
 *
 * Carico la lista dei mimetype e assegno la corrispondente immagine alla lista
 **/
 
string ls_sql, ls_ext, ls_exts, ls_base_small, ls_base_large
int li_rows, li_i, li_add, li_ext_id
datastore lds_store

// reset list view
lv_documenti.deleteitems()
lv_documenti.DeleteLargePictures()
lv_documenti.DeleteSmallPictures()
lv_documenti.largepictureheight = 128
lv_documenti.largepicturewidth = 128

lv_documenti.addlargepicture(is_base_path + "large\unknow.png")
lv_documenti.addsmallpicture(is_base_path + "small\unknow.png")

is_mimetype[1] = ""

// data store
is_extension_filter = ""
ls_exts = ""
ls_sql = "SELECT estensione FROM tab_mimetype WHERE cod_azienda='" + s_cs_xx.cod_azienda + "'"

if not f_crea_datastore(ref lds_store, ls_sql) then
	g_mb.error("Documenti", "Errore durante la creazione del datastore mimetype")
	return
end if

li_rows = lds_store.retrieve()
if li_rows < 0 then
	g_mb.error("Documenti", "Errore durante il controllo delle estensioni disponibili")
	return
elseif li_rows = 0 then
	return
end if

for li_i = 1 to li_rows
	//li_ext_id = lds_store.getitemnumber(li_i, 1) // prog_mimetype
	//ls_ext = lower(lds_store.getitemstring(li_i, 2))
	//is_mimetype[li_i + 1] = ls_ext
	//ii_mimetype_id[li_i + 1] = li_ext_id
	ls_exts += "*." + lower(lds_store.getitemstring(li_i, 1))
	
	// listview image
//	if lv_documenti.addlargepicture(ls_base_large + ls_ext + ".png") < 0 then
//		lv_documenti.addlargepicture(ls_base_large + "unknow.png")
//	end if
//	
//	if lv_documenti.addsmallpicture(ls_base_small + ls_ext + ".png") < 0 then
//		lv_documenti.addsmallpicture(ls_base_small + "unknow.png")
//	end if
	// ----
	
	if li_i < li_rows then ls_exts += "; "
next

is_extension_filter = "Tutti i file conosciuti, " + ls_exts
end subroutine

public subroutine wf_load_documents ();/**
 * Stefanop
 * 02/09/2010
 *
 * Sovrascrivere questo metodo per il recupero dei documenti associati al padre.
 * Passare i paremetri interessati come s_cs_xx.parametri.xxx e recuperarli nell'evento open o pcd_set_window
 *
 * 1. creare datastore
 * 2. ciclare e inserire i valori della chiave primaria dentro la struttura str_ole (se non ci sono aggiungerli)
 * 3. all'interno del ciclo usare la funzione wf_add_document(titolo documento, struttura)
 **/
 
 /* ESEMPIO
 
string ls_sql
long li_rows, li_i
datastore lds_documenti
str_ole lstr_data

ls_sql = "SELECT &
	cod_nota, &
	des_nota, &
	prog_mimetype &
FROM !!!_NOME_TABELLA_!!!! &
WHERE &
	cod_azienda ='" + s_cs_xx.cod_azienda +"' and &
	anno_registrazione =" + string(il_anno_registrazione) + " and &
	num_registrazione =" + string(il_num_registrazione) + " &
ORDER BY &
	cod_azienda ASC, &
	anno_registrazione ASC, &
	num_registrazione ASC "
	
if not f_crea_datastore(ref lds_documenti, ls_sql) then
	return 
end if

li_rows = lds_documenti.retrieve()
for li_i = 1 to li_rows
	lstr_data.anno_registrazione = il_anno_registrazione
	lstr_data.num_registrazione = il_num_registrazione
	lstr_data.progressivo_s = lds_documenti.getitemstring(li_i, "cod_nota")
	lstr_data.prog_mimetype = lds_documenti.getitemnumber(li_i, "prog_mimetype")
	
	wf_add_document(lds_documenti.getitemstring(li_i, "des_nota"), lstr_data)
next

*/
 
end subroutine

public function boolean wf_upload_file (string as_file_path, string as_file_name, string as_file_ext, long al_prog_mimetype, ref blob ab_file_blob);/**
 * Stefanop
 * 02/09/2010
 *
 * Sovrascrivere questa funzione per gestire il salvataggio del documento nel DB
 * ritornare true se il salvataggio ok e false per se errore
 **/
 
/* ESEMPIO

long ll_progressivo
str_ole lstr_data

// Calcolo progressivo
select max(progressivo)
into ll_progressivo
from nome_tabella
where
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_registrazione = :il_anno_registrazione and
	num_registrazione = :il_num_registrazione;
	
if sqlca.sqlcode < 0 then
	g_mb.error("Documenti", "Errore durante il calcolo del progressivo per il file " + as_file_name + ". " + sqlca.sqlerrtext)
	return false
elseif sqlca.sqlcode = 100 or isnull(ll_progressivo) or ll_progressivo < 1 then
	ll_progressivo = 0
end if

ll_progressivo ++
	
// Inserimento nuova riga
inser into nome_tabella (lista_campi)
values (lista_valori)

// Aggiornamento campo blob
updateblob nome_tabella
set nome_colonna_blob = :ab_file_blob
where
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_registrazione = :il_anno_registrazione and
	num_registrazione = :il_num_registrazione and
	progressivo = :ll_progressivo;
	
if sqlca.sqlcode < 0 then
	g_mb.error("Documenti", "Errore durante il salvataggio del file " + as_file_name + ". " + sqlca.sqlerrtext)
	return false
end if

// Creo struttura e aggiongo icona
lstr_data.anno_registrazione = il_anno_registrazione
lstr_data.num_registrazione = il_num_registrazione
lstr_data.progressivo = ll_progressivo
lstr_data.prog_mimetype = al_prog_mimetype
// aggiungere informazioni alla struttura se necessario

wf_add_document(as_file_name, lstr_data)

return true

*/

return true
end function

private function integer wf_get_mimetype_index (string as_file_ext);/**
 * Stefanop
 * 02/09/2010
 *
 * Scorre l'array dei mimetype alla ricerca dell'index dell'immagine associata
 **/
 
int li_i

for li_i = 1 to upperbound(is_mimetype)
	if is_mimetype[li_i] = as_file_ext then
		return li_i
	end if
next

// 1 corrisponde all'immagine sconosciuta
return 1
end function

public function boolean wf_download_file (integer ai_index, ref listviewitem alv_item, str_ole astr_data, ref blob ab_file_blob);/**
 * stefanop
 * 02/09/2010
 *
 * Sovrascrivere la funzione per il recupero del blob dal database ed associarlo alla variabile passata
 * per riferimento ab_file_blob
 *
 * Return TRUE se tutto corretto altrimenti FALSE
 **/
 
 /* ESEMPIO
 
selectblob colonna_blob
into :ab_file_blob
from nome_tabella
where
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_registrazione = :il_anno_registrazione and
	num_registrazione = :il_num_registrazione and
	progressivo = :astr_data.progressivo;

if sqlca.sqlcode <> 0 then
	g_mb.error("Documenti", "Errore durante la lettura del documento " + alv_item.label + ". " + sqlca.sqlerrtext)
	return false
else
	return true
end if

 */
 
 return false
end function

public function boolean wf_delete_file (integer ai_index, ref listviewitem alv_item, str_ole astr_data);/**
 * stefanop
 * 02/09/2010
 *
 * Sovrascrivere la funzione inserendo il codice necessario per la cancellazione del file
 * dal DB.
 * Ritornare TRUE se la cancellazione è andata a buon fine altrimenti FALSE
 **/
 
 /* ESEMPIO

delete from nome_tabella
where
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_registrazione = :il_anno_registrazione and
	num_registrazione = :il_num_registrazione and
	progressivo = :astr_data.progressivo;
end if
	
if sqlca.sqlcode <> 0 then
	g_mb.error("Documenti", "Errore durante la cancellazione del documento " + alv_item.label +". " + sqlca.sqlerrtext)
	return false
else
	return true
end if

*/

return false
end function

public function integer wf_load_mimetype_icon (string as_file_ext);/**
 * Stefanop
 * 07/09/2010
 *
 * Data un'estensione la funzione carica l'immagine nella lista se necessario
 **/
 
int li_i

for li_i = 1 to upperbound(is_mimetype)
	if is_mimetype[li_i] = as_file_ext then
		return li_i
	end if
next

// immagine non ancora caricata, quindi procedo con il caricamento
li_i = lv_documenti.addlargepicture(is_base_path + "large\" + as_file_ext + ".png") 
if li_i < 0 then
	li_i = lv_documenti.addlargepicture(is_base_path + "large\unknow.png")
end if

if lv_documenti.addsmallpicture(is_base_path + "small\" + as_file_ext + ".png") < 0 then
	lv_documenti.addsmallpicture(is_base_path + "small\unknow.png")
end if

// stefanop 22-12-2010: risorsa non trovata
if li_i >= 0 then
	is_mimetype[li_i] = as_file_ext
end if

return li_i
end function

private function integer wf_get_prog_mimetype (string as_file_ext);/**
 * Stefanop
 * 02/09/2010
 *
 * Scorre l'array dei mimetype alla ricerca dell'id del prog_mimetype
 **/
 
int li_prog_mimetype

select prog_mimetype
into :li_prog_mimetype
from tab_mimetype
where
	cod_azienda = :s_cs_xx.cod_azienda and
	estensione = :as_file_ext;
	
if sqlca.sqlcode < 0 then
	g_mb.error("Documenti", "Errore durante il controllo dell'estensione all'interno del database")
	return -1
elseif sqlca.sqlcode = 100 or isnull(li_prog_mimetype) or li_prog_mimetype = 0 then
	g_mb.error("Documenti", "Estensione " + as_file_ext + " non trovata all'interno del database")
	return -1
else
	return li_prog_mimetype
end if
end function

public function integer wf_add_document (string as_label, str_ole astr_data);/**
 * Stefanop
 * 03/09/2010
 *
 * Questa funzione consente di creare l'item all'interno della listview ed impostare correttamente
 * tutte le sue proprietà come icona e data object
 **/
 
listviewitem llvi_item

// aggiungo l'estensione alla struttura dati
astr_data.ext = wf_get_ext_from_mimetype(astr_data.prog_mimetype)
llvi_item.label = as_label
llvi_item.data = astr_data
llvi_item.pictureindex =wf_load_mimetype_icon(astr_data.ext) // wf_get_mimetype_index(astr_data.prog_mimetype)


return lv_documenti.additem(llvi_item)
end function

private function string wf_get_ext_from_mimetype (integer ai_prog_mimetype);/**
 * Stefanop
 * 02/09/2010
 *
 * Ricerca l'estensione associata ad un determinato progressivo mimetype
 **/
 
string ls_ext

select estensione
into :ls_ext
from tab_mimetype
where
	cod_azienda = :s_cs_xx.cod_azienda and
	prog_mimetype = :ai_prog_mimetype;
	
if sqlca.sqlcode <> 0 or isnull(ls_ext) then
	return ""
else
	return lower(ls_ext)
end if
end function

private subroutine wf_document_from_blob ();/**
 * disegno documento
 **/
 
// s_cs_xx.parametri.parametro_bl_1 = BLOB
// s_cs_xx.parametri.parametro_s_1 = file_name
// s_cs_xx.parametri.parametro_ul_1 = prog_mimetype

string ls_file_name
long ll_prog_mimetype
	
ls_file_name = s_cs_xx.parametri.parametro_s_1
iblb_blob_buffer = s_cs_xx.parametri.parametro_bl_1

//ll_prog_mimetype = s_cs_xx.parametri.parametro_ul_1
ll_prog_mimetype = s_cs_xx.parametri.parametro_ul_3

if not isnull(ll_prog_mimetype) or ll_prog_mimetype > 0 then
	is_ext_blob_buffer = wf_get_ext_from_mimetype(ll_prog_mimetype)
else
	is_ext_blob_buffer = ""
end if

wf_add_document(ls_file_name, is_ext_blob_buffer)

if lv_documenti.totalitems() > 0 then
	if st_loading.visible then st_loading.visible = false
end if

setnull(s_cs_xx.parametri.parametro_s_1)
setnull(s_cs_xx.parametri.parametro_bl_1)
setnull(s_cs_xx.parametri.parametro_ul_1)
end subroutine

public function integer wf_add_document (string as_label, string as_file_ext);/**
 * Stefanop
 * 03/09/2010
 *
 * Questa funzione consente di creare l'item all'interno della listview ed impostare correttamente
 * tutte le sue proprietà come icona e data object
 **/
 
listviewitem llvi_item

// aggiungo l'estensione alla struttura dati
//astr_data.ext = wf_get_ext_from_mimetype(astr_data.prog_mimetype)
llvi_item.label = as_label
//llvi_item.data = astr_data
llvi_item.pictureindex =wf_load_mimetype_icon(as_file_ext) // wf_get_mimetype_index(astr_data.prog_mimetype)


return lv_documenti.additem(llvi_item)
end function

public subroutine wf_color ();backcolor = s_themes.theme[s_themes.current_theme].w_backcolor
end subroutine

on w_ole_v2.create
this.st_loading=create st_loading
this.cb_cancella=create cb_cancella
this.lv_documenti=create lv_documenti
this.ddlb_style=create ddlb_style
this.cb_sfoglia=create cb_sfoglia
this.Control[]={this.st_loading,&
this.cb_cancella,&
this.lv_documenti,&
this.ddlb_style,&
this.cb_sfoglia}
end on

on w_ole_v2.destroy
destroy(this.st_loading)
destroy(this.cb_cancella)
destroy(this.lv_documenti)
destroy(this.ddlb_style)
destroy(this.cb_sfoglia)
end on

event resize;/**
 * ATTENZIONE
 * Tolto ancestor script
 **/
 
if isnull(newheight) then return

cb_sfoglia.move(20, newheight - cb_sfoglia.height - 40)
cb_cancella.move(cb_sfoglia.x + cb_sfoglia.width + 40, cb_sfoglia.y)
ddlb_style.move(newwidth - ddlb_style.width - 20, cb_sfoglia.y)

lv_documenti.move(-1, -1)
lv_documenti.resize(this.width - 20, cb_sfoglia.y - 40)

st_loading.move(10, round((lv_documenti.height / 2), 0) - 70)
st_loading.width = lv_documenti.width - 10

end event

event close;int li_i

// cancello tutti i documenti salvati nella cartella temporanea
if not ib_save_document then
	for li_i = 1 to upperbound(is_file_downloaded)
		filedelete(is_file_downloaded[li_i])
	next
end if

// Document over blob
if ib_document_over_blob then
	s_cs_xx.parametri.parametro_bl_1 = iblb_blob_buffer
	s_cs_xx.parametri.parametro_ul_1 = il_prog_mimetype_buffer
	s_cs_xx.parametri.parametro_b_1 = ib_modified_dob
end if
end event

event open;triggerevent("pc_setwindow")
end event

type st_loading from statictext within w_ole_v2
integer x = 23
integer y = 420
integer width = 2446
integer height = 140
integer textsize = -18
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 10789024
long backcolor = 16777215
string text = "Caricamento documenti..."
alignment alignment = center!
boolean focusrectangle = false
end type

type cb_cancella from commandbutton within w_ole_v2
integer x = 686
integer y = 1140
integer width = 343
integer height = 100
integer taborder = 30
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Cancella"
end type

event clicked;/**
 * Stefanop
 * 03/09/2010
 *
 * Permette di cancellare i file dalla listview e dal DB
 **/
 
int li_index
listviewitem llv_item
str_ole lstr_data

li_index = lv_documenti.selectedindex()

if isnull(li_index) or li_index < 1 then return
if lv_documenti.getitem(li_index, llv_item) < 0 then return -1


if not g_mb.confirm("Documenti", "Sicuro di voler cancellare il file " + llv_item.label + "?") then return 0

if not ib_delete_only_blob then
	lstr_data = llv_item.data
	if not isvalid(lstr_data) or isnull(lstr_data) then return -1

	if wf_delete_file(li_index, ref llv_item, lstr_data) then
		lv_documenti.deleteitem(li_index)
		commit;
	else
		rollback;
	end if
else
	setnull(iblb_blob_buffer)
	lv_documenti.deleteitem(li_index)
	ib_modified_dob = true
end if

if lv_documenti.totalitems() < 1 then
	st_loading.visible = true
	st_loading.text = is_empty_text
end if

end event

type lv_documenti from listview within w_ole_v2
integer width = 2491
integer height = 1100
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
boolean autoarrange = true
boolean editlabels = true
boolean gridlines = true
boolean fullrowselect = true
long largepicturemaskcolor = 536870912
long smallpicturemaskcolor = 536870912
long statepicturemaskcolor = 536870912
end type

event doubleclicked;string ls_file_name, ls_path, ls_file_ext, ls_user_ext, ls_filter
int li_ris
long hwnd
listviewitem ltv_item
str_ole lstr_data
blob lb_file_blob

if index < 1 then return

if not ib_document_over_blob then
	if getitem(index, ltv_item) < 0 then return
	
	if isvalid(ltv_item.data) and not isnull(ltv_item.data) then
		lstr_data = ltv_item.data
		ls_file_ext = lstr_data.ext
		
		// devo salvare il file nella caretlla temporanea o può decidere l'utente?
		if ib_save_document then
			
			// recupero nome applicazione
			SELECT applicazione
			INTO :ls_filter
			FROM tab_mimetype
			WHERE
				cod_azienda = :s_cs_xx.cod_azienda AND
				prog_mimetype = :lstr_data.prog_mimetype;
				
			if sqlca.sqlcode <> 0 or isnull(ls_filter) then
				ls_filter = ls_file_ext + "(*." + ls_file_ext + "), " + ls_file_ext
			else
				ls_filter += "(*." + ls_file_ext + "), " + ls_file_ext
			end if
		
			
			if GetFileSaveName ("Salva documento", ls_path, ls_file_name, ls_file_ext, ls_filter) < 1 then
				return 
			end if
		
			if len(ls_file_name) < 1 then
				g_mb.show("Documenti", "Attenzione il nome del file non risulta essere valido")
				return
			end if
			
			// controllo che il file non contenga caratteri anomali, nel caso l'utente lo avesse messo a mano
			wf_get_file_info(ls_file_name, ref ls_file_name, ref ls_user_ext)
			ls_file_name = wf_sanatize_filename(ls_file_name)
			
			// L'utente potrebbe inserire un'estensione non coerente con quella del file, controllo e forzo
			if not isnull(ls_user_ext) and  ls_user_ext <> ls_file_ext then ls_path += "." + ls_file_ext
			
		else
			
			ls_path = f_env_var("APPDATA") + "\csteam\"
			if not directoryexists(ls_path) then createdirectory(ls_path)
			
			ls_path += ltv_item.label + "." + ls_file_ext
			
		end if
		
		// Recupero il blob del file
		wf_download_file(index, ref ltv_item, lstr_data, ref lb_file_blob)
		
		if len(lb_file_blob) < 0 then return 0
		if f_blob_to_file(ls_path, lb_file_blob) < 0 then
			g_mb.error("Documenti", "Errore durante il salvataggio del file nella cartella " + ls_path)
			return -1
		end if
		
		// apro automaticamente il file se necessario
		if not ib_save_document then
			hwnd = handle(parent)	
			li_ris = shellexecuteA(hwnd, 'open', ls_path, '', '', 1)
			if li_ris <= 32 then
				FileDelete(ls_path)
				g_mb.messagebox("Attenzione", "Impossibile visualizzare il documento " + ls_path, exclamation!, ok!)		
			else
				is_file_downloaded[upperbound(is_file_downloaded) + 1] = ls_path
			end if	
		end if
		
	end if
else
	// il blob è memorizzato nella variabile di istanza
	ls_path = f_env_var("APPDATA") + "\csteam\"
	if not directoryexists(ls_path) then createdirectory(ls_path)

	ls_path += ltv_item.label + "." + is_ext_blob_buffer

	if f_blob_to_file(ls_path, iblb_blob_buffer) < 0 then
		g_mb.error("Documenti", "Errore durante il salvataggio del file nella cartella " + ls_path)
		return -1
	end if

	hwnd = handle(parent)	
	li_ris = shellexecuteA(hwnd, 'open', ls_path, '', '', 1)
	if li_ris <= 32 then
		FileDelete(ls_path)
		g_mb.messagebox("Attenzione", "Impossibile visualizzare il documento " + ls_path, exclamation!, ok!)		
	else
		is_file_downloaded[upperbound(is_file_downloaded) + 1] = ls_path
	end if	
end if
end event

event endlabeledit;string ls_file_name
listviewitem ltv_item

if index < 1 or isnull(newlabel) or len(trim(newlabel)) < 1 then return 1
if getitem(index, ltv_item) < 0 then return 1

if isvalid(ltv_item.data) and not isnull(ltv_item.data) then
	ls_file_name = wf_sanatize_filename(newlabel)
		
	if len(ls_file_name) < 1 then return 1
	
	if wf_rename_file(index, ref ltv_item,  ltv_item.data, ls_file_name) then
		ltv_item.label = ls_file_name
		setitem(index, ltv_item)
		commit;
		return 1
	else
		rollback;
		return 1
	end if
end if
end event

type ddlb_style from dropdownlistbox within w_ole_v2
integer x = 1851
integer y = 1144
integer width = 617
integer height = 400
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string item[] = {"Icone grandi","Icone piccole"}
borderstyle borderstyle = stylelowered!
end type

event selectionchanged;int li_test

choose case index
	case 1
		lv_documenti.view = listviewlargeicon!
	case 2
		lv_documenti.view = listviewsmallicon!
//	case 3
//		lv_documenti.view = listviewreport!
end choose

// salvo preferenza utente solo se non sono in sintexcal (vedi note in pc_setwindow)
if not ib_sintexcal then
	select numero
	into :li_test
	from parametri_azienda_utente
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		cod_utente = :s_cs_xx.cod_utente and
		flag_parametro = 'N' and
		cod_parametro = 'OLV';
		
	if sqlca.sqlcode < 0 then
		return
	elseif sqlca.sqlcode = 100 then
		
		insert into parametri_azienda_utente (
			cod_azienda,
			cod_utente,
			cod_parametro,
			flag_parametro,
			numero,
			des_parametro)
		values (
			:s_cs_xx.cod_azienda,
			:s_cs_xx.cod_utente,
			'OLV',
			'N',
			:index,
			'Gestione vista documenti, NON MODIFICARE'
		);
		
		commit using sqlca;
		
	elseif sqlca.sqlcode = 0 then
		
		update parametri_azienda_utente
		set numero = :index
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_utente = :s_cs_xx.cod_utente and
			flag_parametro = 'N' and
			cod_parametro = 'OLV';
			
			commit using sqlca;
	end if

end if
// ----

end event

type cb_sfoglia from commandbutton within w_ole_v2
integer x = 46
integer y = 1140
integer width = 617
integer height = 100
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Allega file"
end type

event clicked;string ls_path, ls_file, ls_file_name, ls_ext, ls_filter
int li_index
long ll_mimetype_id
blob lb_file_blob

// Filtra file per estensione
ls_filter = ""
if ib_filtra_mimetype_conosciuti then
	ls_filter = is_extension_filter
end if

if ib_can_have_only_one_document and lv_documenti.totalitems() > 0 then
	if not g_mb.confirm("", "E' possibile caricare un solo documento.~r~nSovrascrivere quello esistente?") then return
end if

if GetFileOpenName("Seleziona un documento", ls_path, ls_file, "", ls_filter) < 1 then
	// L'utente ha cancellato la selezione o c'è stato un errore.
	return
end if

wf_get_file_info(ls_file, ref ls_file_name, ref ls_ext)
if f_file_to_blob(ls_path, ref lb_file_blob) < 0 then
	g_mb.error("Documenti", "Errore durante la conversione del file!")
	return
end if

ll_mimetype_id = wf_get_prog_mimetype(ls_ext)
if ll_mimetype_id < 0 then
	return
end if

if not ib_document_over_blob then
	if wf_upload_file(ls_path, ls_file_name, ls_ext, ll_mimetype_id, ref lb_file_blob) then
		commit;
	else
		rollback;
		setnull(lb_file_blob)
	end if
else
	ib_modified_dob = true
	iblb_blob_buffer = lb_file_blob
	il_prog_mimetype_buffer = ll_mimetype_id
	is_ext_blob_buffer = ls_ext
	
	lv_documenti.deleteitems()
	wf_add_document(ls_file_name, is_ext_blob_buffer)
end if

if lv_documenti.totalitems() > 0 then
	if st_loading.visible then st_loading.visible = false
end if
end event

