﻿$PBExportHeader$w_parametri.srw
$PBExportComments$Finestra Gestione Parametri
forward
global type w_parametri from w_cs_xx_principale
end type
type dw_parametri_lista from uo_cs_xx_dw within w_parametri
end type
end forward

global type w_parametri from w_cs_xx_principale
integer width = 2757
integer height = 1644
string title = "Gestione Parametri"
dw_parametri_lista dw_parametri_lista
end type
global w_parametri w_parametri

event open;call super::open;string ls_modify


dw_parametri_lista.set_dw_options(sqlca, &
                                  pcca.null_object, &
                                  c_default, &
                                  c_default)

ls_modify = "stringa.protect='0~tif(flag_parametro<>~~'S~~',1,0)'~t"
ls_modify = ls_modify + "flag.protect='0~tif(flag_parametro<>~~'F~~',1,0)'~t"
ls_modify = ls_modify + "data.protect='0~tif(flag_parametro<>~~'D~~',1,0)'~t"
ls_modify = ls_modify + "numero.protect='0~tif(flag_parametro<>~~'N~~',1,0)'~t"
dw_parametri_lista.modify(ls_modify)

end event

on w_parametri.create
int iCurrent
call super::create
this.dw_parametri_lista=create dw_parametri_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_parametri_lista
end on

on w_parametri.destroy
call super::destroy
destroy(this.dw_parametri_lista)
end on

event pc_new;call super::pc_new;dw_parametri_lista.setitem(dw_parametri_lista.getrow(), "data", datetime(today()))
end event

type dw_parametri_lista from uo_cs_xx_dw within w_parametri
integer x = 23
integer y = 20
integer width = 2674
integer height = 1500
integer taborder = 10
string dataobject = "d_parametri_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_saveafter;call super::pcd_saveafter;string ls_codice, ls_valore
long ll_i
integer li_risposta

for ll_i = 1 to this.rowcount()
   if this.getitemstring(ll_i, "flag_ini") = "S" then
      ls_codice = this.getitemstring(ll_i, "cod_parametro")
      choose case this.getitemstring(ll_i, "flag_parametro")
         case "S"
            ls_valore = this.getitemstring(ll_i, "stringa")
         case "F"
            ls_valore = this.getitemstring(ll_i, "flag")
         case "D"
            ls_valore = string(this.getitemdatetime(ll_i, "data"), "DD/MM/YYYY")
         case "N"
            ls_valore = string(this.getitemdecimal(ll_i, "numero"), "000000000000.0000")
      end choose
		
		li_risposta = RegistrySet(s_cs_xx.chiave_root + "applicazione_" +  s_cs_xx.profilocorrente, ls_codice, ls_valore)
		
   end if
next
end event

event pcd_retrieve;call super::pcd_retrieve;long ll_numero, ll_valore, ll_errore
datetime ldt_data, ldt_valore
string ls_sql, ls_flag_parametro, ls_cod_parametro, ls_stringa, ls_flag, ls_flag_ini, ls_valore
integer li_risposta

declare cu_ini dynamic cursor for sqlsa;

ls_sql = "select parametri.flag_parametro, parametri.cod_parametro, parametri.stringa, parametri.flag, parametri.data, parametri.numero, parametri.flag_ini from parametri where parametri.flag_ini = 'S'"

prepare sqlsa from :ls_sql;

open dynamic cu_ini;

do while 1 = 1
   fetch cu_ini into :ls_flag_parametro, :ls_cod_parametro,   
                     :ls_stringa, :ls_flag,   
                     :ldt_data, :ll_numero,   
                     :ls_flag_ini;
   if sqlca.sqlcode = 100 then exit

   li_risposta = Registryget(s_cs_xx.chiave_root + "applicazione_" +  s_cs_xx.profilocorrente, ls_cod_parametro, ls_valore)
	
	choose case ls_flag_parametro
      case "S"
         update parametri set
                stringa = :ls_valore
                where flag_parametro = :ls_flag_parametro and
                      cod_parametro = :ls_cod_parametro;
      case "F"
         update parametri set
                flag = :ls_valore
                where flag_parametro = :ls_flag_parametro and
                      cod_parametro = :ls_cod_parametro;
      case "D"
         ldt_valore = datetime(ls_valore)
         update parametri set
                data = :ldt_valore
                where flag_parametro = :ls_flag_parametro and
                      cod_parametro = :ls_cod_parametro;
      case "N"
         ls_valore = replace(ls_valore, pos(ls_valore, ","), 1, ".")
         ll_valore = dec(ls_valore)
         update parametri set
                numero = :ls_valore
                where flag_parametro = :ls_flag_parametro and
                      cod_parametro = :ls_cod_parametro;
   end choose
loop

close cu_ini;


ll_errore = retrieve()

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_modify;call super::pcd_modify;string ls_modify


ls_modify = "stringa.color='0~tif(flag_parametro<>~~'S~~',rgb(128,128,128),0)'~t"
ls_modify = ls_modify + "flag.color='0~tif(flag_parametro<>~~'F~~',rgb(128,128,128),0)'~t"
ls_modify = ls_modify + "data.color='0~tif(flag_parametro<>~~'D~~',rgb(128,128,128),0)'~t"
ls_modify = ls_modify + "numero.color='0~tif(flag_parametro<>~~'N~~',rgb(128,128,128),0)'~t"
this.modify(ls_modify)
end event

event pcd_new;call super::pcd_new;string ls_modify


ls_modify = "stringa.color='0~tif(flag_parametro<>~~'S~~',rgb(128,128,128),0)'~t"
ls_modify = ls_modify + "flag.color='0~tif(flag_parametro<>~~'F~~',rgb(128,128,128),0)'~t"
ls_modify = ls_modify + "data.color='0~tif(flag_parametro<>~~'D~~',rgb(128,128,128),0)'~t"
ls_modify = ls_modify + "numero.color='0~tif(flag_parametro<>~~'N~~',rgb(128,128,128),0)'~t"
this.modify(ls_modify)
end event

