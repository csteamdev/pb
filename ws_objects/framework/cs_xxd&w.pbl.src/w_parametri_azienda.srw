﻿$PBExportHeader$w_parametri_azienda.srw
$PBExportComments$Finestra Gestione Parametri Azienda
forward
global type w_parametri_azienda from w_cs_xx_principale
end type
type dw_parametri_azienda_lista from uo_cs_xx_dw within w_parametri_azienda
end type
end forward

global type w_parametri_azienda from w_cs_xx_principale
integer width = 2757
integer height = 1644
string title = "Parametri Aziendali"
dw_parametri_azienda_lista dw_parametri_azienda_lista
end type
global w_parametri_azienda w_parametri_azienda

event open;call super::open;string ls_modify

dw_parametri_azienda_lista.set_dw_key("cod_azienda")
dw_parametri_azienda_lista.set_dw_options(sqlca, &
                                          pcca.null_object, &
                                          c_default, &
                                          c_default)
ls_modify = "stringa.protect='0~tif(flag_parametro<>~~'S~~',1,0)'~t"
ls_modify = ls_modify + "flag.protect='0~tif(flag_parametro<>~~'F~~',1,0)'~t"
ls_modify = ls_modify + "data.protect='0~tif(flag_parametro<>~~'D~~',1,0)'~t"
ls_modify = ls_modify + "numero.protect='0~tif(flag_parametro<>~~'N~~',1,0)'~t"
dw_parametri_azienda_lista.modify(ls_modify)

end event

on w_parametri_azienda.create
int iCurrent
call super::create
this.dw_parametri_azienda_lista=create dw_parametri_azienda_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_parametri_azienda_lista
end on

on w_parametri_azienda.destroy
call super::destroy
destroy(this.dw_parametri_azienda_lista)
end on

event pc_new;call super::pc_new;dw_parametri_azienda_lista.setitem(dw_parametri_azienda_lista.getrow(), "data", datetime(today()))
end event

type dw_parametri_azienda_lista from uo_cs_xx_dw within w_parametri_azienda
integer x = 23
integer y = 20
integer width = 2674
integer height = 1500
integer taborder = 10
string dataobject = "d_parametri_azienda_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

event pcd_savebefore;call super::pcd_savebefore;long ll_i
datetime ldt_date

ll_i = 0
do while ll_i <= this.rowcount()
   ll_i = this.getnextmodified(ll_i, Primary!)

   if ll_i = 0 then
      exit
   end if

   choose case this.getitemstring(ll_i, "flag_parametro")
      case "S"
         this.setitem(ll_i, "flag", "")
         this.setitem(ll_i, "data", ldt_date)
         this.setitem(ll_i, "numero", 0)
      case "F"
         this.setitem(ll_i, "stringa", "")
         this.setitem(ll_i, "data", ldt_date)
         this.setitem(ll_i, "numero", 0)
      case "D"
         this.setitem(ll_i, "stringa", "")
         this.setitem(ll_i, "flag", "")
         this.setitem(ll_i, "numero", 0)
      case "N"
         this.setitem(ll_i, "stringa", "")
         this.setitem(ll_i, "flag", "")
         this.setitem(ll_i, "data", ldt_date)
   end choose
loop
end event

event pcd_modify;call super::pcd_modify;string ls_modify


ls_modify = "stringa.color='0~tif(flag_parametro<>~~'S~~',rgb(128,128,128),0)'~t"
ls_modify = ls_modify + "flag.color='0~tif(flag_parametro<>~~'F~~',rgb(128,128,128),0)'~t"
ls_modify = ls_modify + "data.color='0~tif(flag_parametro<>~~'D~~',rgb(128,128,128),0)'~t"
ls_modify = ls_modify + "numero.color='0~tif(flag_parametro<>~~'N~~',rgb(128,128,128),0)'~t"
this.modify(ls_modify)
end event

event pcd_new;call super::pcd_new;string ls_modify


ls_modify = "stringa.color='0~tif(flag_parametro<>~~'S~~',rgb(128,128,128),0)'~t"
ls_modify = ls_modify + "flag.color='0~tif(flag_parametro<>~~'F~~',rgb(128,128,128),0)'~t"
ls_modify = ls_modify + "data.color='0~tif(flag_parametro<>~~'D~~',rgb(128,128,128),0)'~t"
ls_modify = ls_modify + "numero.color='0~tif(flag_parametro<>~~'N~~',rgb(128,128,128),0)'~t"
this.modify(ls_modify)
end event

