﻿$PBExportHeader$w_parametri_azienda_utente.srw
$PBExportComments$Finestra Gestione Parametri
forward
global type w_parametri_azienda_utente from w_cs_xx_principale
end type
type dw_parametri_lista from uo_cs_xx_dw within w_parametri_azienda_utente
end type
end forward

global type w_parametri_azienda_utente from w_cs_xx_principale
integer width = 2757
integer height = 1644
string title = "Gestione Parametri Utente"
dw_parametri_lista dw_parametri_lista
end type
global w_parametri_azienda_utente w_parametri_azienda_utente

type variables
string is_cod_utente
end variables

event open;call super::open;string ls_modify


dw_parametri_lista.set_dw_options(sqlca, &
                                  pcca.null_object, &
                                  c_default, &
                                  c_default)

ls_modify = "stringa.protect='0~tif(flag_parametro<>~~'S~~',1,0)'~t"
ls_modify = ls_modify + "flag.protect='0~tif(flag_parametro<>~~'F~~',1,0)'~t"
ls_modify = ls_modify + "data.protect='0~tif(flag_parametro<>~~'D~~',1,0)'~t"
ls_modify = ls_modify + "numero.protect='0~tif(flag_parametro<>~~'N~~',1,0)'~t"
dw_parametri_lista.modify(ls_modify)

is_cod_utente = s_cs_xx.parametri.parametro_s_1
setnull(s_cs_xx.parametri.parametro_s_1)

end event

on w_parametri_azienda_utente.create
int iCurrent
call super::create
this.dw_parametri_lista=create dw_parametri_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_parametri_lista
end on

on w_parametri_azienda_utente.destroy
call super::destroy
destroy(this.dw_parametri_lista)
end on

event pc_new;call super::pc_new;dw_parametri_lista.setitem(dw_parametri_lista.getrow(), "data_1", datetime(today()))
end event

type dw_parametri_lista from uo_cs_xx_dw within w_parametri_azienda_utente
integer x = 23
integer y = 20
integer width = 2674
integer height = 1500
integer taborder = 10
string dataobject = "d_parametri_azienda_utente_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore

ll_errore = retrieve(s_cs_xx.cod_azienda, is_cod_utente)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_modify;call super::pcd_modify;string ls_modify


ls_modify = "stringa.color='0~tif(flag_parametro<>~~'S~~',rgb(128,128,128),0)'~t"
ls_modify = ls_modify + "flag.color='0~tif(flag_parametro<>~~'F~~',rgb(128,128,128),0)'~t"
ls_modify = ls_modify + "data.color='0~tif(flag_parametro<>~~'D~~',rgb(128,128,128),0)'~t"
ls_modify = ls_modify + "numero.color='0~tif(flag_parametro<>~~'N~~',rgb(128,128,128),0)'~t"
this.modify(ls_modify)
end event

event pcd_new;call super::pcd_new;string ls_modify


ls_modify = "stringa.color='0~tif(flag_parametro<>~~'S~~',rgb(128,128,128),0)'~t"
ls_modify = ls_modify + "flag.color='0~tif(flag_parametro<>~~'F~~',rgb(128,128,128),0)'~t"
ls_modify = ls_modify + "data.color='0~tif(flag_parametro<>~~'D~~',rgb(128,128,128),0)'~t"
ls_modify = ls_modify + "numero.color='0~tif(flag_parametro<>~~'N~~',rgb(128,128,128),0)'~t"
this.modify(ls_modify)

this.setitem(this.getrow(), "data_1", today())
end event

event pcd_setkey;call super::pcd_setkey;long ll_i

for ll_i = 1 to this.rowcount()
	if isnull(this.getitemstring(ll_i, "cod_azienda")) then
		this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
		this.setitem(ll_i, "cod_utente", is_cod_utente)
	end if
next
end event

event pcd_savebefore;call super::pcd_savebefore;long ll_i
datetime ldt_date

ll_i = 0
do while ll_i <= this.rowcount()
   ll_i = this.getnextmodified(ll_i, Primary!)

   if ll_i = 0 then
      exit
   end if

   choose case this.getitemstring(ll_i, "flag_parametro")
      case "S"
         //this.setitem(ll_i, "flag", "")
         this.setitem(ll_i, "data", ldt_date)
         this.setitem(ll_i, "numero", 0)
      case "F"
         this.setitem(ll_i, "stringa", "")
         this.setitem(ll_i, "data", ldt_date)
         this.setitem(ll_i, "numero", 0)
      case "D"
         this.setitem(ll_i, "stringa", "")
         //this.setitem(ll_i, "flag", "")
         this.setitem(ll_i, "numero", 0)
      case "N"
         this.setitem(ll_i, "stringa", "")
         //this.setitem(ll_i, "flag", "")
         this.setitem(ll_i, "data", ldt_date)
   end choose
loop
end event

