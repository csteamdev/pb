﻿$PBExportHeader$w_parametri_blocco.srw
forward
global type w_parametri_blocco from w_cs_xx_principale
end type
type dw_dettaglio from uo_cs_xx_dw within w_parametri_blocco
end type
type dw_lista from uo_cs_xx_dw within w_parametri_blocco
end type
end forward

global type w_parametri_blocco from w_cs_xx_principale
integer width = 5289
integer height = 1548
string title = "Parametri Blocco e Blocco Finanziario"
string icon = "Window!"
boolean center = true
dw_dettaglio dw_dettaglio
dw_lista dw_lista
end type
global w_parametri_blocco w_parametri_blocco

on w_parametri_blocco.create
int iCurrent
call super::create
this.dw_dettaglio=create dw_dettaglio
this.dw_lista=create dw_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_dettaglio
this.Control[iCurrent+2]=this.dw_lista
end on

on w_parametri_blocco.destroy
call super::destroy
destroy(this.dw_dettaglio)
destroy(this.dw_lista)
end on

event pc_setwindow;call super::pc_setwindow;
dw_lista.set_dw_key("cod_azienda")

if s_cs_xx.cod_utente <> "CS_SYSTEM" then
	dw_lista.set_dw_options(sqlca, pcca.null_object, c_nonew, c_default)
	dw_dettaglio.set_dw_options(sqlca, dw_lista, c_nonew + c_sharedata + c_scrollparent, c_default)
else
	dw_lista.set_dw_options(sqlca, pcca.null_object, c_default, c_default)
	dw_dettaglio.set_dw_options(sqlca, dw_lista, c_sharedata + c_scrollparent, c_default)
end if



end event

type dw_dettaglio from uo_cs_xx_dw within w_parametri_blocco
integer x = 2633
integer y = 20
integer width = 2606
integer height = 1408
integer taborder = 10
string dataobject = "d_parametri_blocco_dettaglio"
borderstyle borderstyle = styleraised!
end type

event pcd_retrieve;call super::pcd_retrieve;
retrieve(s_cs_xx.cod_azienda)
end event

type dw_lista from uo_cs_xx_dw within w_parametri_blocco
integer x = 23
integer y = 20
integer width = 2587
integer height = 1408
integer taborder = 10
string dataobject = "d_parametri_blocco_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;
retrieve(s_cs_xx.cod_azienda)
end event

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
	if isnull(this.getitemstring(ll_i, "cod_azienda")) then
		this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
	end if
next
end event

