﻿$PBExportHeader$w_plaque.srw
$PBExportComments$Finestra Placca
forward
global type w_plaque from window
end type
type st_3 from statictext within w_plaque
end type
type st_2 from statictext within w_plaque
end type
type p_applicazione_bmp from picture within w_plaque
end type
type st_applicazione_versione from statictext within w_plaque
end type
type st_applicazione_nome from statictext within w_plaque
end type
end forward

global type w_plaque from window
integer x = 891
integer y = 704
integer width = 1664
integer height = 1544
boolean border = false
windowtype windowtype = popup!
long backcolor = 16777215
event po_close pbm_custom75
st_3 st_3
st_2 st_2
p_applicazione_bmp p_applicazione_bmp
st_applicazione_versione st_applicazione_versione
st_applicazione_nome st_applicazione_nome
end type
global w_plaque w_plaque

event po_close;close(this)
end event

event open;long ll_secondo, ll_adesso
string ls_prova


setredraw(false)

if len(trim(w_pomanager_std.parm.application_bitmap)) = 0 then
   p_applicazione_bmp.visible = false
else
   p_applicazione_bmp.picturename  = w_pomanager_std.parm.application_bitmap
end if
st_applicazione_nome.text = w_pomanager_std.parm.application_name
st_applicazione_versione.text  = w_pomanager_std.parm.application_rev

//ls_prova = s_cs_xx.volume + s_cs_xx.risorse + "placca_1.bmp"

//p_placca_1.PictureName = s_cs_xx.volume + s_cs_xx.risorse + "placca_1.bmp"
//p_placca_2.PictureName = s_cs_xx.volume + s_cs_xx.risorse + "placca_2.bmp"

p_applicazione_bmp.x = round((width - p_applicazione_bmp.width) / 2,0)

f_po_setwindowposition(this)

setposition(topmost!)

setredraw(true)

// eseguo questo ciclo per perdere un paio di secondi e far vedere la placca
ll_secondo = hour(now())*3600 + minute(now()) * 60 + second(now())
ll_secondo = ll_secondo + 1
do while ll_secondo > ll_adesso 
	ll_adesso = hour(now())*3600 + minute(now()) * 60 + second(now())
loop



postevent("po_close")

end event

on w_plaque.create
this.st_3=create st_3
this.st_2=create st_2
this.p_applicazione_bmp=create p_applicazione_bmp
this.st_applicazione_versione=create st_applicazione_versione
this.st_applicazione_nome=create st_applicazione_nome
this.Control[]={this.st_3,&
this.st_2,&
this.p_applicazione_bmp,&
this.st_applicazione_versione,&
this.st_applicazione_nome}
end on

on w_plaque.destroy
destroy(this.st_3)
destroy(this.st_2)
destroy(this.p_applicazione_bmp)
destroy(this.st_applicazione_versione)
destroy(this.st_applicazione_nome)
end on

type st_3 from statictext within w_plaque
integer x = 14
integer y = 1332
integer width = 1646
integer height = 120
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
boolean enabled = false
string text = "Questo programma è protetto dalle norme internazionali sul copyright © come specificato nella finestra informazioni."
alignment alignment = center!
long bordercolor = 16777215
boolean focusrectangle = false
end type

type st_2 from statictext within w_plaque
integer x = 18
integer y = 1448
integer width = 1655
integer height = 80
integer textsize = -10
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
boolean enabled = false
string text = "Copyright © 1995-2004  Consulting&&Software"
alignment alignment = center!
long bordercolor = 16777215
boolean focusrectangle = false
end type

type p_applicazione_bmp from picture within w_plaque
integer x = 9
integer y = 220
integer width = 1646
integer height = 1000
boolean enabled = false
boolean originalsize = true
boolean focusrectangle = false
end type

type st_applicazione_versione from statictext within w_plaque
integer x = 5
integer y = 1224
integer width = 1669
integer height = 100
integer textsize = -18
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
boolean enabled = false
string text = "Versione"
alignment alignment = center!
long bordercolor = 16777215
boolean focusrectangle = false
end type

type st_applicazione_nome from statictext within w_plaque
integer width = 1655
integer height = 220
integer textsize = -36
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
boolean enabled = false
string text = "Applicazione"
alignment alignment = center!
boolean focusrectangle = false
end type

