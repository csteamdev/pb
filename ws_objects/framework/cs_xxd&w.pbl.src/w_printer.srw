﻿$PBExportHeader$w_printer.srw
forward
global type w_printer from w_cs_xx_risposta
end type
type sle_printer from singlelineedit within w_printer
end type
type cb_copia_nome from commandbutton within w_printer
end type
type st_1 from statictext within w_printer
end type
type p_printer from picture within w_printer
end type
type plb_printers from picturelistbox within w_printer
end type
end forward

global type w_printer from w_cs_xx_risposta
integer width = 1595
integer height = 1844
string title = "Cambia Stampante"
sle_printer sle_printer
cb_copia_nome cb_copia_nome
st_1 st_1
p_printer p_printer
plb_printers plb_printers
end type
global w_printer w_printer

type variables
private constant string REGEX = "^([\w\(\) -_]+)\t"

private:
	string is_active_label = " - (corrente)"
end variables

forward prototypes
public function integer wf_printer_list ()
public function integer wf_printers_list ()
end prototypes

public function integer wf_printer_list ();string ls_prntrs, ls_printname, ls_curent_printer
int li_pos = 0, li_icon = 1, li_current

li_current = 0

// Nome della stampante corrente
ls_curent_printer = PrintGetPrinter()
ls_curent_printer = left(ls_curent_printer, pos(ls_curent_printer, "~t") - 1)

// Lista delle stampanti disponibili
ls_prntrs  = PrintGetPrinters ( )
li_pos = pos(ls_prntrs, "~n")
do while  li_pos > 0
	ls_printname = left(ls_prntrs, pos(ls_prntrs, "~t") - 1)
	
	if ls_printname = ls_curent_printer then 
		li_icon = 2
		ls_printname += is_active_label
	end if
	
	plb_printers.additem(ls_printname, li_icon)
	
	li_icon = 1
	ls_prntrs = mid(ls_prntrs, li_pos + 1)
	li_pos = pos(ls_prntrs, "~n")
loop

li_current = plb_printers.finditem(ls_curent_printer, 1)
li_current = plb_printers.selectitem(li_current)
return -1
end function

public function integer wf_printers_list ();string ls_printers, ls_curent_printer, ls_printer_name
int li_count, li_i, li_icon, li_current
uo_regex luo_regex

// Lista delle stampanti disponibili
ls_printers  = PrintGetPrinters()

luo_regex = create uo_regex
luo_regex.setdotmatchnewline(false)
luo_regex.setmultiline(true)
luo_regex.setextendedsyntax(false)
luo_regex.setungreedy(true)

if luo_regex.initialize(REGEX, true ,false) then 
	li_count = luo_regex.search(ls_printers)
	
	for li_i = 1 to li_count 
		ls_printer_name = trim(luo_regex.match(li_i))
		
		// fix: devo eliminare il carattere \t che il pattern mi porta dentro
		// se lo elimino dalla regex non trova più una mazza; chissà che 
		// cazzo fa PB
		ls_printer_name = left(ls_printer_name, len(ls_printer_name) - 1)
		
		plb_printers.additem(ls_printer_name, 1)
	next
	
	// Carico la stampante corrente
	try 
		ls_curent_printer = PrintGetPrinter()
		ls_curent_printer = left(ls_curent_printer, pos(ls_curent_printer, "~t") - 1)
				
		li_current = plb_printers.finditem(ls_curent_printer, 1)
		li_current = plb_printers.selectitem(li_current)
	catch (RuntimeError e)
		
	end try
	
	
	
else
	g_mb.error("Errore in sintassi della REGEX")
end if

destroy luo_regex

return -1
end function

on w_printer.create
int iCurrent
call super::create
this.sle_printer=create sle_printer
this.cb_copia_nome=create cb_copia_nome
this.st_1=create st_1
this.p_printer=create p_printer
this.plb_printers=create plb_printers
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.sle_printer
this.Control[iCurrent+2]=this.cb_copia_nome
this.Control[iCurrent+3]=this.st_1
this.Control[iCurrent+4]=this.p_printer
this.Control[iCurrent+5]=this.plb_printers
end on

on w_printer.destroy
call super::destroy
destroy(this.sle_printer)
destroy(this.cb_copia_nome)
destroy(this.st_1)
destroy(this.p_printer)
destroy(this.plb_printers)
end on

event pc_setwindow;call super::pc_setwindow;string ls_base_path

ls_base_path =  s_cs_xx.volume + s_cs_xx.risorse + "11.5\"
p_printer.picturename =ls_base_path + "printer_large.png"

plb_printers.deletepictures()
plb_printers.addpicture(ls_base_path + "printer.png")
plb_printers.addpicture(ls_base_path + "printer_current.png")

wf_printers_list()
end event

event resize;/**
 * !! TOLTO ANCESTOR SCRIPT !!
 **/
plb_printers.x = 20
plb_printers.width = newwidth - 40
end event

type sle_printer from singlelineedit within w_printer
boolean visible = false
integer x = 55
integer y = 1504
integer width = 402
integer height = 112
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
string text = "none"
borderstyle borderstyle = stylelowered!
end type

type cb_copia_nome from commandbutton within w_printer
integer x = 1079
integer y = 224
integer width = 439
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Copia Nome"
end type

event clicked;string ls_printer

ls_printer = plb_printers.selecteditem()
	
// controllo che non ci sia la scritta " - (corrente)" nella label
if pos(ls_printer, is_active_label) > 0 then
	ls_printer = left(ls_printer, pos(ls_printer, is_active_label) - 1)
end if

sle_printer.text = ls_printer
sle_printer.SelectText(1, len(sle_printer.text) )
sle_printer.copy()
end event

type st_1 from statictext within w_printer
integer x = 366
integer y = 48
integer width = 1152
integer height = 224
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Seleziona, tramite doppio click, la stampante da usare durante la sessione di lavoro."
boolean focusrectangle = false
end type

type p_printer from picture within w_printer
integer x = 23
integer y = 20
integer width = 293
integer height = 256
string picturename = "C:\cs_115\framework\risorse\11.5\printer_large.png"
boolean focusrectangle = false
end type

type plb_printers from picturelistbox within w_printer
integer x = 18
integer y = 320
integer width = 1518
integer height = 1392
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
string picturename[] = {"C:\cs_115\framework\risorse\11.5\printer.png","C:\cs_115\framework\risorse\11.5\printer_pdf.png"}
integer picturewidth = 32
integer pictureheight = 32
long picturemaskcolor = 536870912
end type

event doubleclicked;if index > 0 then
	string ls_printer
	
	ls_printer = selecteditem()
	
	// controllo che non ci sia la scritta " - (corrente)" nella label
	if pos(ls_printer, is_active_label) > 0 then
		ls_printer = left(ls_printer, pos(ls_printer, is_active_label) - 1)
	end if
	
	try
		if PrintSetPrinter(ls_printer) = 1 then
			close(parent)
		end if
	catch (RuntimeError ex)
		g_mb.error("APICE", ex.getMessage())
	end try
end if
end event

