﻿$PBExportHeader$w_profili_db.srw
$PBExportComments$Window configurazione profili DB
forward
global type w_profili_db from w_cs_xx_principale
end type
type em_1 from editmask within w_profili_db
end type
type st_2 from statictext within w_profili_db
end type
type cb_2 from commandbutton within w_profili_db
end type
type cb_1 from commandbutton within w_profili_db
end type
type tab_1 from tab within w_profili_db
end type
type tabpage_1 from userobject within tab_1
end type
type dw_1 from datawindow within tabpage_1
end type
type tabpage_1 from userobject within tab_1
dw_1 dw_1
end type
type tabpage_2 from userobject within tab_1
end type
type dw_2 from datawindow within tabpage_2
end type
type tabpage_2 from userobject within tab_1
dw_2 dw_2
end type
type tab_1 from tab within w_profili_db
tabpage_1 tabpage_1
tabpage_2 tabpage_2
end type
type ddlb_1 from dropdownlistbox within w_profili_db
end type
type cb_imposta from commandbutton within w_profili_db
end type
type cb_default from commandbutton within w_profili_db
end type
type cbx_dbdefault from checkbox within w_profili_db
end type
end forward

global type w_profili_db from w_cs_xx_principale
integer width = 2418
integer height = 1716
string title = "Configura Profili"
em_1 em_1
st_2 st_2
cb_2 cb_2
cb_1 cb_1
tab_1 tab_1
ddlb_1 ddlb_1
cb_imposta cb_imposta
cb_default cb_default
cbx_dbdefault cbx_dbdefault
end type
global w_profili_db w_profili_db

forward prototypes
public subroutine wf_mostra_chiave (string fs_num_chiave)
end prototypes

public subroutine wf_mostra_chiave (string fs_num_chiave);string ls_values[], ls_key, ls_str
integer li_i, li_row, li_ret
long    ll_posizione

tab_1.tabpage_1.dw_1.reset()

ll_posizione = pos( fs_num_chiave, "-")
if not isnull(ll_posizione) and ll_posizione > 0 then
	
	fs_num_chiave = left( fs_num_chiave, (ll_posizione - 2))
	
end if

ls_key = s_cs_xx.chiave_root + "database_" + fs_num_chiave
registryvalues(ls_key, ls_values[])

for li_i = 1 to upperbound(ls_values[]) 
	li_ret = registryget(ls_key, ls_values[li_i], ls_str)
	
	if li_ret < 1 then
		g_mb.messagebox("Framework", "Errore in lettura chiave "+ ls_key + " valore " + ls_values[li_i])
		return 
	end if
	li_row = tab_1.tabpage_1.dw_1.insertrow(0)
	
	tab_1.tabpage_1.dw_1.setitem(li_row, "chiave_completa", s_cs_xx.chiave_root + "database_")
	tab_1.tabpage_1.dw_1.setitem(li_row, "chiave", ls_values[li_i])
	tab_1.tabpage_1.dw_1.setitem(li_row, "valore", ls_str)
	
next

tab_1.tabpage_2.dw_2.reset()
ls_key = s_cs_xx.chiave_root + "applicazione_" + fs_num_chiave
registryvalues(ls_key, ls_values[])

for li_i = 1 to upperbound(ls_values[]) 
	li_ret = registryget(ls_key, ls_values[li_i], ls_str)
	
	if li_ret < 1 then
		g_mb.messagebox("Framework", "Errore in lettura chiave "+ ls_key + " valore " + ls_values[li_i])
		return 
	end if
	li_row = tab_1.tabpage_2.dw_2.insertrow(0)
	
	tab_1.tabpage_2.dw_2.setitem(li_row, "chiave_completa", s_cs_xx.chiave_root + "applicazione_")
	tab_1.tabpage_2.dw_2.setitem(li_row, "chiave", ls_values[li_i])
	tab_1.tabpage_2.dw_2.setitem(li_row, "valore", ls_str)
	
next

return
end subroutine

on w_profili_db.create
int iCurrent
call super::create
this.em_1=create em_1
this.st_2=create st_2
this.cb_2=create cb_2
this.cb_1=create cb_1
this.tab_1=create tab_1
this.ddlb_1=create ddlb_1
this.cb_imposta=create cb_imposta
this.cb_default=create cb_default
this.cbx_dbdefault=create cbx_dbdefault
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.em_1
this.Control[iCurrent+2]=this.st_2
this.Control[iCurrent+3]=this.cb_2
this.Control[iCurrent+4]=this.cb_1
this.Control[iCurrent+5]=this.tab_1
this.Control[iCurrent+6]=this.ddlb_1
this.Control[iCurrent+7]=this.cb_imposta
this.Control[iCurrent+8]=this.cb_default
this.Control[iCurrent+9]=this.cbx_dbdefault
end on

on w_profili_db.destroy
call super::destroy
destroy(this.em_1)
destroy(this.st_2)
destroy(this.cb_2)
destroy(this.cb_1)
destroy(this.tab_1)
destroy(this.ddlb_1)
destroy(this.cb_imposta)
destroy(this.cb_default)
destroy(this.cbx_dbdefault)
end on

event open;call super::open;integer li_risposta, li_i
string  ls_keys[], ls_profilo,ls_flag_amministratore, ls_id

if s_cs_xx.cod_utente <> "CS_SYSTEM" then
	select flag_collegato
	into   :ls_flag_amministratore
	from   utenti
	where  cod_utente = :s_cs_xx.cod_utente;
	
	if ls_flag_amministratore <> "S" or isnull(ls_flag_amministratore) then
		g_mb.messagebox("Framework","Funziona accessibile solo agli utenti amministratori.")
		postevent("pc_close")
		return
	end if
end if

li_risposta = Registryget(s_cs_xx.chiave_root_user + "profilodefault", "numero", s_cs_xx.profilodefault)

RegistryKeys(s_cs_xx.chiave_root,ls_keys)

for li_i = 1 to upperbound(ls_keys)
	if pos(lower(ls_keys[li_i]), "applicazione_") > 0 then
		ls_profilo = mid(ls_keys[li_i], pos(lower(ls_keys[li_i]), "applicazione_")+ 13)
		
		// *** Michela 21/07/2006 visualizzo l'id se esiste come valore
		setnull(ls_id)
		li_risposta = Registryget(s_cs_xx.chiave_root + ls_keys[li_i], "id", ls_id)
		if isnull(ls_id) then
			ddlb_1.additem(ls_profilo)
		else
			ddlb_1.additem(ls_profilo + " - " + ls_id)
		end if
		// ***				
	end if
next

ddlb_1.selectitem(s_cs_xx.profilodefault, 0)

wf_mostra_chiave(s_cs_xx.profilodefault)


end event

type em_1 from editmask within w_profili_db
integer x = 1266
integer y = 1484
integer width = 165
integer height = 92
integer taborder = 30
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "##"
end type

type st_2 from statictext within w_profili_db
integer x = 805
integer y = 1500
integer width = 416
integer height = 60
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Duplica su profilo:"
alignment alignment = right!
boolean focusrectangle = false
end type

type cb_2 from commandbutton within w_profili_db
integer x = 1979
integer y = 1492
integer width = 366
integer height = 80
integer taborder = 100
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Applica"
end type

event clicked;string ls_valore, ls_key, ls_str,ls_num_chiave
integer li_i, li_row, li_ret
long    ll_posizione


tab_1.tabpage_1.dw_1.accepttext()
tab_1.tabpage_2.dw_2.accepttext()

ls_num_chiave = ddlb_1.text

ll_posizione = pos( ls_num_chiave, "-")
if not isnull(ll_posizione) and ll_posizione > 0 then
	ls_num_chiave = left( ls_num_chiave, (ll_posizione - 2))
end if

for li_i = 1 to tab_1.tabpage_1.dw_1.rowcount()
	
	ls_key = tab_1.tabpage_1.dw_1.getitemstring(li_i,"chiave_completa") + ls_num_chiave
	ls_valore = tab_1.tabpage_1.dw_1.getitemstring(li_i,"valore")
	ls_str = tab_1.tabpage_1.dw_1.getitemstring(li_i,"chiave")
	
	li_ret = registryset(ls_key, ls_str, regstring!, ls_valore)
	
	if li_ret < 1 then
		g_mb.messagebox("Framework", "Errore in scrittura chiave "+ ls_key + ls_num_chiave + " valore " + ls_valore)
		return 
	end if
	
next

for li_i = 1 to tab_1.tabpage_2.dw_2.rowcount()
	
	ls_key = tab_1.tabpage_2.dw_2.getitemstring(li_i,"chiave_completa") + ls_num_chiave
	ls_valore = tab_1.tabpage_2.dw_2.getitemstring(li_i,"valore")
	ls_str = tab_1.tabpage_2.dw_2.getitemstring(li_i,"chiave")
	
	li_ret = registryset(ls_key, ls_str, regstring!, ls_valore)
	
	if li_ret < 1 then
		g_mb.messagebox("Framework", "Errore in scrittura chiave "+ ls_key + ls_num_chiave + " valore " + ls_valore)
		return 
	end if
	
next

if cbx_dbdefault.checked then
	choose case s_cs_xx.livello_profilo
		case "LOCAL_USER"
			Registryset(s_cs_xx.chiave_root_user + "profilodefault", "numero", regstring!, ls_num_chiave)
		case "LOCAL_MACHINE"
			Registryset(s_cs_xx.chiave_root + "profilodefault", "numero", regstring!, ls_num_chiave)
		case else
			Registryset(s_cs_xx.chiave_root_user + "profilodefault", "numero", regstring!, ls_num_chiave)
	end choose
	cbx_dbdefault.checked = false
end if

g_mb.messagebox("Framework","Impostazioni Effettuate.")

end event

type cb_1 from commandbutton within w_profili_db
integer x = 27
integer y = 1492
integer width = 366
integer height = 80
integer taborder = 470
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Regedit"
end type

event clicked;RUN("regedit.exe")
end event

type tab_1 from tab within w_profili_db
event create ( )
event destroy ( )
integer x = 18
integer y = 124
integer width = 2331
integer height = 1352
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean fixedwidth = true
boolean raggedright = true
boolean focusonbuttondown = true
integer selectedtab = 1
tabpage_1 tabpage_1
tabpage_2 tabpage_2
end type

on tab_1.create
this.tabpage_1=create tabpage_1
this.tabpage_2=create tabpage_2
this.Control[]={this.tabpage_1,&
this.tabpage_2}
end on

on tab_1.destroy
destroy(this.tabpage_1)
destroy(this.tabpage_2)
end on

type tabpage_1 from userobject within tab_1
event create ( )
event destroy ( )
integer x = 18
integer y = 112
integer width = 2295
integer height = 1224
long backcolor = 12632256
string text = "Database"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_1 dw_1
end type

on tabpage_1.create
this.dw_1=create dw_1
this.Control[]={this.dw_1}
end on

on tabpage_1.destroy
destroy(this.dw_1)
end on

type dw_1 from datawindow within tabpage_1
integer width = 2281
integer height = 1220
integer taborder = 10
string title = "none"
string dataobject = "d_profili_db"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

type tabpage_2 from userobject within tab_1
event create ( )
event destroy ( )
integer x = 18
integer y = 112
integer width = 2295
integer height = 1224
long backcolor = 12632256
string text = "Applicazione"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_2 dw_2
end type

on tabpage_2.create
this.dw_2=create dw_2
this.Control[]={this.dw_2}
end on

on tabpage_2.destroy
destroy(this.dw_2)
end on

type dw_2 from datawindow within tabpage_2
integer y = 4
integer width = 2286
integer height = 1212
integer taborder = 30
string title = "none"
string dataobject = "d_profili_db"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

type ddlb_1 from dropdownlistbox within w_profili_db
integer x = 23
integer y = 16
integer width = 1047
integer height = 464
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event selectionchanged;if index > 0 then
	wf_mostra_chiave(ddlb_1.text(index))
end if
cbx_dbdefault.checked = false
end event

type cb_imposta from commandbutton within w_profili_db
integer x = 411
integer y = 1492
integer width = 366
integer height = 80
integer taborder = 460
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Duplica"
end type

event clicked;string ls_valore, ls_key, ls_str, ls_num_chiave_dest
integer li_i, li_row, li_ret

ls_num_chiave_dest = em_1.text

for li_i = 1 to tab_1.tabpage_1.dw_1.rowcount()
	
	ls_key = tab_1.tabpage_1.dw_1.getitemstring(li_i,"chiave_completa") + ls_num_chiave_dest
	ls_valore = tab_1.tabpage_1.dw_1.getitemstring(li_i,"valore")
	ls_str = tab_1.tabpage_1.dw_1.getitemstring(li_i,"chiave")
	
	li_ret = registryset(ls_key, ls_str, regstring!, ls_valore)
	
	if li_ret < 1 then
		g_mb.messagebox("Framework", "Errore in scrittura chiave "+ ls_key + " valore " + ls_valore)
		return 
	end if
	
next

for li_i = 1 to tab_1.tabpage_2.dw_2.rowcount()
	
	ls_key = tab_1.tabpage_2.dw_2.getitemstring(li_i,"chiave_completa") + ls_num_chiave_dest
	ls_valore = tab_1.tabpage_2.dw_2.getitemstring(li_i,"valore")
	ls_str = tab_1.tabpage_2.dw_2.getitemstring(li_i,"chiave")
	
	li_ret = registryset(ls_key, ls_str, regstring!, ls_valore)
	
	if li_ret < 1 then
		g_mb.messagebox("Framework", "Errore in scrittura chiave "+ ls_key + " valore " + ls_valore)
		return 
	end if
	
next

if cbx_dbdefault.checked then
	choose case s_cs_xx.livello_profilo
		case "LOCAL_USER"
			Registryset(s_cs_xx.chiave_root_user + "profilodefault", "numero", regstring!, ls_num_chiave_dest)
		case "LOCAL_MACHINE"
			Registryset(s_cs_xx.chiave_root + "profilodefault", "numero", regstring!, ls_num_chiave_dest)
		case else
			Registryset(s_cs_xx.chiave_root + "profilodefault", "numero", regstring!, ls_num_chiave_dest)
	end choose
	cbx_dbdefault.checked = false
end if

g_mb.messagebox("Framework","Impostazioni Effettuate.")

end event

type cb_default from commandbutton within w_profili_db
integer x = 1591
integer y = 1492
integer width = 366
integer height = 80
integer taborder = 90
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Default"
end type

event clicked;long ll_row

window_open(w_scelta_default, 0)

ddlb_1.additem("ODBC")
ddlb_1.additem("Sybase ASE")
ddlb_1.additem("ORACLE REL 7")
ddlb_1.additem("ORACLE REL 8")
ddlb_1.additem("ORACLE REL 9")
ddlb_1.additem("Microsoft Sql Server")

if s_cs_xx.parametri.parametro_i_1 = -1 then return

tab_1.tabpage_1.dw_1.reset()

choose case s_cs_xx.parametri.parametro_i_1
	case 1 		// ODBC
		
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "servername")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "logid")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "logpass")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "dbms")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "ODBC")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "database")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "userid")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "dbpass")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "dbparm")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "Connectstring='DSN=CS_DB'")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "lock")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "enginetype")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "SYBASE_ASA")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "navmanut")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "Connectstring='DSN=CS_DB'")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "OPT")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", ",connectoption='SQL_DRIVER_NO_CONNECT, SQL_DRIVER_NOPROMPT'")
		
	case 2		//Sybase ASE
		
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "servername")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "ASE")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "logid")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "dba")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "logpass")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "dbms")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "SYC Sybase System 10/11")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "database")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "cs_db")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "userid")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "dbpass")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "dbparm")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "Release='11',appname='utente',charset='cp850'")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "lock")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "enginetype")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "SYBASE_ASE")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "navmanut")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "Connectstring='DSN=CS_DB'")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "OPT")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", ",connectoption='SQL_DRIVER_NO_CONNECT, SQL_DRIVER_NOPROMPT'")

	case 3		//ORACLE 7
		
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "servername")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "@CSDB")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "logid")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "CS_XX")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "logpass")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "dbms")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "O73")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "database")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "userid")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "dbpass")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "dbparm")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "lock")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "enginetype")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "ORACLE")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "navmanut")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "Connectstring='DSN=CS_DB'")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "OPT")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", ",connectoption='SQL_DRIVER_NO_CONNECT, SQL_DRIVER_NOPROMPT'")
	
	case 4		// ORACLE 8
		
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "servername")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "@CSDB")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "logid")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "CS_XX")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "logpass")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "dbms")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "O84")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "database")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "userid")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "dbpass")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "dbparm")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "lock")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "enginetype")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "ORACLE")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "navmanut")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "Connectstring='DSN=CS_DB'")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "OPT")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", ",connectoption='SQL_DRIVER_NO_CONNECT, SQL_DRIVER_NOPROMPT'")
		
	case 5		// ORACLE 9
		
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "servername")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "@CSDB")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "logid")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "CS_XX")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "logpass")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "dbms")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "O90")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "database")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "userid")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "dbpass")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "dbparm")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "lock")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "enginetype")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "ORACLE")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "navmanut")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "Connectstring='DSN=CS_DB'")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "OPT")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", ",connectoption='SQL_DRIVER_NO_CONNECT, SQL_DRIVER_NOPROMPT'")
	case 6		// MSSQL
		
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "servername")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "\nome_server\nome_servizio")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "logid")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "sa")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "logpass")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "dbms")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "MSS")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "database")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "cs_db")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "userid")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "dbpass")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "dbparm")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "lock")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "enginetype")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "MSSQL")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "navmanut")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", "Connectstring='DSN=CS_DB'")
		ll_row = tab_1.tabpage_1.dw_1.insertrow(0)
		tab_1.tabpage_1.dw_1.setitem(ll_row,"chiave", "OPT")
		tab_1.tabpage_1.dw_1.setitem(ll_row,"valore", ",connectoption='SQL_DRIVER_NO_CONNECT, SQL_DRIVER_NOPROMPT'")
end choose
end event

type cbx_dbdefault from checkbox within w_profili_db
integer x = 1353
integer y = 20
integer width = 974
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Imposta come profilo predefinito"
boolean lefttext = true
end type

event clicked;//
//cbx_dbdefault_2.checked = false
//cbx_dbdefault_3.checked = false
//cbx_dbdefault_4.checked = false
end event

