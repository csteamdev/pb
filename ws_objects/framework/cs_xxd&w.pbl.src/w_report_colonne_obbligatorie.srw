﻿$PBExportHeader$w_report_colonne_obbligatorie.srw
$PBExportComments$Window Standard per Report ( da copiare )
forward
global type w_report_colonne_obbligatorie from w_cs_xx_principale
end type
type dw_1 from datawindow within w_report_colonne_obbligatorie
end type
type cb_annulla from commandbutton within w_report_colonne_obbligatorie
end type
type cb_report from commandbutton within w_report_colonne_obbligatorie
end type
type dw_selezione from uo_cs_xx_dw within w_report_colonne_obbligatorie
end type
type dw_report from uo_cs_xx_dw within w_report_colonne_obbligatorie
end type
type dw_folder from u_folder within w_report_colonne_obbligatorie
end type
end forward

global type w_report_colonne_obbligatorie from w_cs_xx_principale
integer width = 3589
integer height = 1692
string title = "Report Dati Obbligatori Mancanti"
dw_1 dw_1
cb_annulla cb_annulla
cb_report cb_report
dw_selezione dw_selezione
dw_report dw_report
dw_folder dw_folder
end type
global w_report_colonne_obbligatorie w_report_colonne_obbligatorie

forward prototypes
public subroutine fwf_cerca_colonne_pk (ref string as_colonne[])
end prototypes

public subroutine fwf_cerca_colonne_pk (ref string as_colonne[]);string ls_str, ls_type, ls_oggetto, ls_testo, ls_cod_lingua, ls_objects, ls_dataobjects, &
		 ls_key

long   ll_riga, ll_pos, ll_controls, ll_i, ll_pos2


ls_str = dw_1.Object.DataWindow.Objects
ll_i =0
do while true
	ll_pos = pos(ls_str, "~t", 1)
	
	if ll_pos = 0 then 
		ls_oggetto = ls_str
	else
		ls_oggetto = left(ls_str, ll_pos - 1)
	end if

	ls_type 	= dw_1.Describe(ls_oggetto + ".type")
	ls_key 	= dw_1.Describe(ls_oggetto + ".Key")
	
	if ls_type = "column" and lower(ls_key) = "yes" then 
		ll_i ++
		as_colonne[ll_i] = ls_oggetto
	end if
	
	ls_str = mid(ls_str, ll_pos + 1)
	
	if len(trim(ls_str)) < 1 or ll_pos = 0 then exit
loop

return
end subroutine

event pc_setwindow;call super::pc_setwindow;windowobject lw_oggetti[]

dw_report.ib_dw_report = true

lw_oggetti[1] = dw_report
dw_folder.fu_assigntab(2, "Report", lw_oggetti[])
lw_oggetti[1] = dw_selezione
lw_oggetti[2] = cb_annulla
lw_oggetti[3] = cb_report
dw_folder.fu_assigntab(1, "Selezione", lw_oggetti[])

dw_folder.fu_foldercreate(2, 4)

dw_folder.fu_selecttab(1)


set_w_options(c_closenosave + c_autoposition + c_noresizewin + c_NoEnablePopup)


//set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
iuo_dw_main = dw_report

end event

on w_report_colonne_obbligatorie.create
int iCurrent
call super::create
this.dw_1=create dw_1
this.cb_annulla=create cb_annulla
this.cb_report=create cb_report
this.dw_selezione=create dw_selezione
this.dw_report=create dw_report
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_1
this.Control[iCurrent+2]=this.cb_annulla
this.Control[iCurrent+3]=this.cb_report
this.Control[iCurrent+4]=this.dw_selezione
this.Control[iCurrent+5]=this.dw_report
this.Control[iCurrent+6]=this.dw_folder
end on

on w_report_colonne_obbligatorie.destroy
call super::destroy
destroy(this.dw_1)
destroy(this.cb_annulla)
destroy(this.cb_report)
destroy(this.dw_selezione)
destroy(this.dw_report)
destroy(this.dw_folder)
end on

event resize;call super::resize;dw_report.width = newwidth - 100
dw_report.height = newheight - 172

dw_folder.x= 30
dw_folder.y = 30
dw_folder.width = newwidth - 60
dw_folder.height = newheight - 60
end event

type dw_1 from datawindow within w_report_colonne_obbligatorie
boolean visible = false
integer x = 2441
integer y = 1712
integer width = 686
integer height = 400
integer taborder = 50
string title = "none"
boolean livescroll = true
end type

type cb_annulla from commandbutton within w_report_colonne_obbligatorie
integer x = 1349
integer y = 528
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

on clicked;close(parent)
end on

type cb_report from commandbutton within w_report_colonne_obbligatorie
integer x = 1737
integer y = 528
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;boolean lb_inizio=true, lb_exit=false

string ls_nome_dw, ls_nome_oggetto, ls_nome_dw_old, ls_colonne[], ls_vuoto[],ls_dbname[], ls_table[],& 
		 ls_colonne_pk[], ls_testo, ls_error_syntaxfromSQL, ls_sql, ls_dw_syntax, ls_error_create, &
		 ls_coltype, ls_filter, ls_name, ls_string, ls_pk, ls_nome_colonna

long   ll_i, ll_row, ll_x, ll_rows, ll_num_col_data, ll_num_col_key,ll_y, ll_ret

date   ld_date

datetime ld_datetime

time	 lt_time

decimal ld_number

datastore ds_column

dw_report.setredraw(false)


declare 	cu_obb cursor for
select 	nome_dw, nome_oggetto, testo
from 		tab_dw_text_repository
where 	cod_azienda = :s_cs_xx.cod_azienda and
		 	cod_lingua is null and
		 	(flag_importante = 'S' or flag_obbligatorio ='S')
order by 	nome_dw, nome_oggetto;

open cu_obb;

dw_report.reset()
ls_nome_dw_old = ""
ll_i = 0

do while true
	
	fetch cu_obb into :ls_nome_dw, :ls_nome_oggetto, :ls_testo;
	
	Yield()
	
	if sqlca.sqlcode <> 0 then 
		lb_exit = true
		ls_nome_dw_old = ""
	end if
	
	if ls_nome_dw <> ls_nome_dw_old then
		
		// se non è il primo giro verifico se i campi importanti / obbligatori hanno dei valori
		if not lb_inizio then
			
			// ottengo l'elenco dei campi che sono PK nella tabella di update della DW
			ls_colonne_pk[] = ls_vuoto[]
			fwf_cerca_colonne_pk(ls_colonne_pk[] )
			
			// verifico quali colonne non sono state compilateù
			// STEP 1: creo la sintassi SQL

			ls_sql = ""
			
			ll_num_col_key  = upperbound( ls_colonne_pk[] )
			ll_num_col_data = upperbound( ls_dbname[] )
			
			for ll_x = 1 to ll_num_col_key
				if len(ls_sql) > 1 then ls_sql += ", "
				ls_sql += ls_colonne_pk[ll_x]
			next
			
			for ll_x = 1 to ll_num_col_data
				if len(ls_sql) > 1 then ls_sql += ", "
				ls_sql += ls_dbname[ll_x]
			next
			
			ls_sql = "SELECT " + ls_sql + " from " + ls_table[1] 
			
			// STEP 2: creo datastore
			ls_dw_syntax = SQLCA.SyntaxFromSQL(ls_sql, 'Style(Type=Grid)', ls_error_syntaxfromSQL)
			
			if Len(ls_error_syntaxfromSQL) > 0 THEN
				g_mb.messagebox("Framework", "Errore durante il controllo colonne abbligatorie~r~n" + 	ls_error_syntaxfromSQL)
				rollback;
				return
			end if
			
			ds_column = CREATE datastore
			
        	ds_column.Create(ls_dw_syntax, ls_error_create)

			if Len(ls_error_create) > 0 THEN
				g_mb.messagebox("Framework", "Errore durante creazione struttura dati.~r~n" + 	ls_error_create)
				rollback;
				return
			end if

			ds_column.settransobject(sqlca)
			
			ll_rows = ds_column.retrieve()
			
			if ll_rows > 0 then
			
				// STEP 3: scorro il datastore per verificare dati mancanti
				// quindi imposto il filtro in base al datatype
				ls_filter = ""
				
				for ll_x = ll_num_col_key + 1 to ll_num_col_key + ll_num_col_data
					
					ls_coltype = ds_column.describe("#" + string(ll_x) + ".coltype")
					ls_name = ds_column.describe("#" + string(ll_x) + ".name")
					
					if len(ls_filter) > 0 then ls_filter += " or "
					
					choose case lower( left(ls_coltype,5) )
						case "char("
							ls_filter += ' ( isnull(' + ls_name  + ') or len(' + ls_name + ') < 1 ) '
							
						case "date", "datet", "times", "time"
							ls_filter += ' ( isnull(' + ls_name  + ') or ' + ls_name + ' <= '+ string(s_cs_xx.db_funzioni.data_neutra, s_cs_xx.db_funzioni.formato_data ) + ') '
							
						case "long", "numbe", "int", "decim"
							ls_filter += ' (' + ls_name  + ' is null or ' + ls_name + ' = 0 ) '
							
					end choose		
			
				next
				
				// applico il filtro
				ll_ret = ds_column.setfilter(ls_filter)
				ll_ret = ds_column.filter()
				ls_pk = ""
				ll_ret = ds_column.rowcount()
				
				for ll_x = 1 to ll_ret
					ls_pk = ""
					for ll_y = 1 to ll_num_col_key
						
						ls_coltype = ds_column.describe("#" + string(ll_y) + ".coltype")
						ls_name = ds_column.describe("#" + string(ll_y) + ".name")
						
						
						choose case lower( left(ls_coltype,5) )
							case "char("
								ls_string = ds_column.getitemstring(ll_x, ls_name)
								
							case "date"
								ls_string = string( ds_column.getitemdate(ll_x, ls_name), "dd/mm/yyyy")
								
							case "datet", "times", "time"
								ls_string = string( ds_column.getitemdatetime(ll_x, ls_name), "dd/mm/yyyy hh:mm:ss")
								
							case "long", "numbe", "int", "decim"
								ls_string = string( ds_column.getitemnumber(ll_x, ls_name) )
							
						end choose	
						
						if len(ls_pk) > 0 then ls_pk += ", "
						ls_pk += ls_name + "=" + ls_string
					
					next
					
					ll_row = dw_report.insertrow(0)
					dw_report.setitem(ll_row, "nome_window", ls_testo)
					dw_report.setitem(ll_row, "valore_chiave", ls_pk)
					ls_nome_colonna = ""
						
					for ll_y = ll_num_col_key + 1 to ll_num_col_key + ll_num_col_data
						
						ls_coltype = ds_column.describe("#" + string(ll_y) + ".coltype")
						ls_name = ds_column.describe("#" + string(ll_y) + ".name")
						
						choose case lower( left(ls_coltype,5) )
							case "char("
								ls_string = ds_column.getitemstring(ll_x, ls_name)
								if isnull(ls_string) or len(ls_string) < 1 then ls_nome_colonna += ls_name + "~r~n"
								
							case "date"
								ld_date = ds_column.getitemdate(ll_x, ls_name)
								if isnull(ld_date) or ld_date <= date("01/01/1900") then ls_nome_colonna += ls_name + "~r~n"
								
							case"datet", "times"
								ld_datetime = ds_column.getitemdatetime(ll_x, ls_name)
								if isnull(ld_datetime) or ld_datetime <= datetime(date("01/01/1900"), 00:00:00) then ls_nome_colonna += ls_name + "~r~n"
								
								
							case "time"
								lt_time = ds_column.getitemtime(ll_x, ls_name)
								if isnull(lt_time) or lt_time <= time(00:00:00) then ls_nome_colonna += ls_name + "~r~n"
								
							case "long", "numbe", "int", "decim"
								ld_number = ds_column.getitemnumber(ll_x, ls_name)
								if isnull(ld_number) or ld_number = 0 then ls_nome_colonna += ls_name + "~r~n"
								
						end choose		
				
					next
					
					dw_report.setitem(ll_row, "nome_colonna", trim(ls_nome_colonna))

					
					
				next	
				
			end if
			
			// elimino il datastore
			destroy ds_column
			
		end if
		
		// passo alla DW successiva
		string ls_str
		dw_1.dataobject = ls_nome_dw
		ls_str = dw_1.Object.DataWindow.Objects
		// reset variabili buffer
		ls_nome_dw_old = ls_nome_dw
		ls_colonne[]   = ls_vuoto[]
		ls_dbname[]    = ls_vuoto[]
		ls_table[]     = ls_vuoto[]
		ll_i = 0
		lb_inizio = false
		
	end if
	
	if lb_exit = true then exit

	
	ll_i ++
	ls_colonne[ll_i] = ls_nome_oggetto
	
	ls_dbname[ll_i]= dw_1.Describe(ls_nome_oggetto + ".dbName")
	ls_table[ll_i]	= dw_1.Describe("DataWindow.Table.UpdateTable")
	
loop

close cu_obb;

rollback;

dw_report.setsort("nome_window A, valore_chiave A")
dw_report.sort()

dw_report.setredraw(true)


dw_folder.fu_selecttab(2)
end event

type dw_selezione from uo_cs_xx_dw within w_report_colonne_obbligatorie
integer x = 50
integer y = 140
integer width = 2080
integer height = 380
integer taborder = 20
borderstyle borderstyle = stylelowered!
end type

type dw_report from uo_cs_xx_dw within w_report_colonne_obbligatorie
integer x = 50
integer y = 140
integer width = 3451
integer height = 1420
integer taborder = 10
string dataobject = "d_report_colonne_obbligatorie"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
end type

type dw_folder from u_folder within w_report_colonne_obbligatorie
integer x = 27
integer y = 20
integer width = 3493
integer height = 1548
integer taborder = 40
end type

