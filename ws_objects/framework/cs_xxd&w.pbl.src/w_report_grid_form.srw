﻿$PBExportHeader$w_report_grid_form.srw
$PBExportComments$Window Report di tipo Grid e Form
forward
global type w_report_grid_form from w_cs_xx_principale
end type
type st_3 from statictext within w_report_grid_form
end type
type st_1 from statictext within w_report_grid_form
end type
type uo_background_color from uo_color_cell within w_report_grid_form
end type
type uo_font_color from uo_color_cell within w_report_grid_form
end type
type uo_font from uo_font_tab within w_report_grid_form
end type
type st_2 from statictext within w_report_grid_form
end type
type ddlb_border from dropdownpicturelistbox within w_report_grid_form
end type
type uo_colors from uo_color_tab within w_report_grid_form
end type
type dw_report from uo_cs_xx_dw within w_report_grid_form
end type
type cb_esporta from commandbutton within w_report_grid_form
end type
type em_nome_report from editmask within w_report_grid_form
end type
end forward

global type w_report_grid_form from w_cs_xx_principale
integer x = 37
integer y = 212
integer width = 4133
integer height = 2016
string title = "Report"
windowstate windowstate = maximized!
st_3 st_3
st_1 st_1
uo_background_color uo_background_color
uo_font_color uo_font_color
uo_font uo_font
st_2 st_2
ddlb_border ddlb_border
uo_colors uo_colors
dw_report dw_report
cb_esporta cb_esporta
em_nome_report em_nome_report
end type
global w_report_grid_form w_report_grid_form

type variables
//string is_nome_oggetti []
//string is_hold_color []
//string is_hold_backcolor []
//string is_hold_backmode []
string is_nome_oggetti
string is_hold_color
string is_hold_backcolor
string is_hold_backmode
boolean is_text
boolean ib_updating


private:
	string is_current_object
	boolean ib_focus_on_color = true

	constant string IS_TAB = "~t"
	
	long il_select_color = 0
	long il_select_backcolor = 15461355
end variables

forward prototypes
public function string wf_update_color_font (string fs_tipo)
public function string wf_conv_altezza_punti (datawindow fdw_parm, string fs_altezza)
public subroutine wf_set_fontsize (integer ai_font_size)
public function string wf_update_dw (string as_modifytext)
public subroutine wf_set_border (object_style aos_style)
public subroutine wf_remove_style ()
end prototypes

public function string wf_update_color_font (string fs_tipo);//object_style los_style
string	ls_colore_testo, ls_colore_sfondo, ls_mod_string, ls_rc, ls_tipo
//int		li_i
//
//if ib_updating then Return ""
//
//SetPointer(HourGlass!)
//
//for li_i = 1 to UpperBound(is_nome_oggetti)
//	if is_nome_oggetti [li_i] = "" then continue
//
//	if fs_tipo = "fore_color" then
//		uo_color.uf_get_colors(ls_colore_testo, ls_colore_sfondo)
//		is_hold_color[li_i] = ls_colore_testo
//		ls_mod_string = ls_mod_string + is_nome_oggetti[li_i] + ".color=~"" + ls_colore_testo + "~" "
//	elseif fs_tipo = "back_color" then
//		uo_color.uf_get_colors(ls_colore_testo, ls_colore_sfondo)
//		is_hold_backcolor[li_i] = ls_colore_sfondo
//
//		if is_hold_backmode[li_i] = "1" then
//			ls_mod_string = ls_mod_string + is_nome_oggetti[li_i] + '.background.mode="1" '
//		else
//			ls_mod_string = ls_mod_string + is_nome_oggetti[li_i] + ".background.color=~"" + ls_colore_sfondo + "~" "
//		end if
//
//	elseif fs_tipo = "font" then
//		ls_tipo = uo_style.uf_get_style(los_style)
//
//		Choose Case lower(ls_tipo)	
//		Case "font"
//			ls_mod_string = ls_mod_string + is_nome_oggetti[li_i] + ".font.face=~"" + los_style.font_face + "~" "
//		Case "bold"
//			ls_mod_string = ls_mod_string + is_nome_oggetti[li_i] + ".font.weight=~"" + los_style.font_weight + "~" "
//		Case "italic"
//			ls_mod_string = ls_mod_string + is_nome_oggetti[li_i] +".font.italic=~"" + los_style.italic + "~" "
//		Case "underline"
//			ls_mod_string = ls_mod_string + is_nome_oggetti[li_i] +".font.underline=~"" + los_style.underline + "~" "
//		Case "border"
//			ls_mod_string = ls_mod_string + is_nome_oggetti[li_i] + ".border=~"" + los_style.border + "~" "
//		Case "size"
//			los_style.font_height = String(Integer(los_style.font_height) * -1)
//			ls_mod_string = ls_mod_string + is_nome_oggetti[li_i] + ".font.height=~"" + los_style.font_height + "~" "
//		Case "left", "right", "center"
//			ls_mod_string = ls_mod_string + is_nome_oggetti[li_i] + ".alignment=~"" + los_style.alignment + "~" "
//		Case "text"
//			if is_text then 
//				los_style.text = f_elimina_carattere ( los_style.text, "~"" )
//				ls_mod_string = ls_mod_string + is_nome_oggetti[li_i] + ".text=~"" + los_style.text + "~" "
//			end if
//		end Choose
//
//	end if
//next
//
//if ls_mod_string <> "" then
//	ls_rc = dw_report.Modify(ls_mod_string)
//end if
//
return ls_rc
end function

public function string wf_conv_altezza_punti (datawindow fdw_parm, string fs_altezza);string 	ls_unita
int		li_altezza

li_altezza = Integer(fs_altezza)

ls_unita = fdw_parm.describe ("datawindow.units")
choose case ls_unita
	case "0" //pb units
		li_altezza = li_altezza / 4
	case "1" //pixels
		li_altezza = li_altezza 
	case "2" //inches
		li_altezza = li_altezza / 12
	case "3" //cm
		li_altezza = li_altezza / 31
end choose

Return String(li_altezza)
end function

public subroutine wf_set_fontsize (integer ai_font_size);// a
string ls_error
int li_font_height

li_font_height = integer(dw_report.Describe(is_current_object + ".font.height"))
if li_font_height > 0 Then 
	li_font_height = integer(wf_conv_altezza_punti(dw_report, string(li_font_height)))
end if


ls_error = dw_report.modify(is_current_object + ".font.height=~"" + string(ai_font_size * li_font_height) + "~" ")

if ls_error <> "" then
	g_mb.error("", ls_error)
end if
end subroutine

public function string wf_update_dw (string as_modifytext);// Imposta i nuovi parametri nella DW
string ls_return

if isnull(is_current_object) or is_current_object = "" then return "Nessun oggetto selezionato"

ls_return = dw_report.modify(is_current_object+ "." + as_modifytext)
if ls_return <> "" then
	g_mb.error("",ls_return)
end if

return ls_return
end function

public subroutine wf_set_border (object_style aos_style);string ls_text

choose case aos_style.border
	case "0"
		ls_text = "None"
	case "1"
		ls_text = "Shadow Box"
	case "2"
		ls_text = "Box"
	case "4"
		ls_text = "Underline"
	case "5"
		ls_text = "3D Lowered"
	case "6"
		ls_text = "3D Raised"
end choose

if ddlb_border.text <> ls_text then ddlb_border.text = ls_text
end subroutine

public subroutine wf_remove_style ();/** 
 * stefanop
 * 27/10/11
 *
 * Rimuovo lo stile delle colonne che potrebbe dare fastidio in stampa; ad esempio
 * le checkbox vengono visualizzate una sopra l'altra e non si capisce il valore
 * impostato
 **/
 
int li_i

for li_i = 1 to  integer(dw_report.describe("Datawindow.Column.Count"))
	dw_report.modify("#" + string(li_i) + ".TabSequence=0")
	dw_report.modify("#" + string(li_i) + ".EditMask.Spin=No")
next
end subroutine

on w_report_grid_form.create
int iCurrent
call super::create
this.st_3=create st_3
this.st_1=create st_1
this.uo_background_color=create uo_background_color
this.uo_font_color=create uo_font_color
this.uo_font=create uo_font
this.st_2=create st_2
this.ddlb_border=create ddlb_border
this.uo_colors=create uo_colors
this.dw_report=create dw_report
this.cb_esporta=create cb_esporta
this.em_nome_report=create em_nome_report
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_3
this.Control[iCurrent+2]=this.st_1
this.Control[iCurrent+3]=this.uo_background_color
this.Control[iCurrent+4]=this.uo_font_color
this.Control[iCurrent+5]=this.uo_font
this.Control[iCurrent+6]=this.st_2
this.Control[iCurrent+7]=this.ddlb_border
this.Control[iCurrent+8]=this.uo_colors
this.Control[iCurrent+9]=this.dw_report
this.Control[iCurrent+10]=this.cb_esporta
this.Control[iCurrent+11]=this.em_nome_report
end on

on w_report_grid_form.destroy
call super::destroy
destroy(this.st_3)
destroy(this.st_1)
destroy(this.uo_background_color)
destroy(this.uo_font_color)
destroy(this.uo_font)
destroy(this.st_2)
destroy(this.ddlb_border)
destroy(this.uo_colors)
destroy(this.dw_report)
destroy(this.cb_esporta)
destroy(this.em_nome_report)
end on

event pc_setwindow;call super::pc_setwindow;string ls_sintassi,ls_errore,ls_indice
long ll_width
integer li_num_colonne,li_i

dw_report.ib_dw_report = true

dw_report.Create(s_cs_xx.parametri.parametro_s_15, ls_errore)
if len(ls_errore) > 0 then
	g_mb.messagebox("Apice", "Errore Durante la Creazione del Report", Exclamation!)
end if
wf_update_dw("border=0")

wf_remove_style()

dw_report.set_dw_options(sqlca, &
                                 c_nulldw, &
                                 c_nonew + &
                                 c_nomodify + &
                                 c_nodelete + &
                                 c_noenablenewonopen + &
                                 c_noenablemodifyonopen + &
                                 c_scrollparent + &
											c_disablecc, &
											c_noresizedw + &
                                 c_nohighlightselected + &
                                 c_nocursorrowfocusrect + &
                                 c_nocursorrowpointer)

iuo_dw_main = dw_report

save_on_close(c_socnosave)

em_nome_report.text = "Rep_" + s_cs_xx.parametri.parametro_s_14
this.title = "Report : " + em_nome_report.text
end event

event resize;/**
 * stefanop
 * 01/01/2010
 *
 * TOLTO ANCESTOR SCRIPT
 **/
 
 dw_report.move(uo_font.x, uo_font.y + uo_font.height + 40)
 dw_report.resize(newwidth - 60, newheight - (uo_font.y + uo_font.height + 60))
 em_nome_report.width = newwidth - em_nome_report.x - 40
end event

type st_3 from statictext within w_report_grid_form
integer x = 2240
integer y = 20
integer width = 169
integer height = 60
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Sfondo"
boolean focusrectangle = false
end type

type st_1 from statictext within w_report_grid_form
integer x = 2080
integer y = 20
integer width = 160
integer height = 60
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Testo"
alignment alignment = center!
boolean focusrectangle = false
end type

type uo_background_color from uo_color_cell within w_report_grid_form
integer x = 2263
integer y = 100
integer width = 137
integer height = 140
integer taborder = 210
boolean ib_fix_size = false
end type

on uo_background_color.destroy
call uo_color_cell::destroy
end on

event clicked;call super::clicked;if is_current_object <> "datawindow" then
	ib_focus_on_color = false
	
	is_hold_backmode = "0"
	is_hold_backcolor = string(ai_color)
	wf_update_dw("background.color='" + is_hold_backcolor + "' ")
end if
end event

type uo_font_color from uo_color_cell within w_report_grid_form
integer x = 2103
integer y = 100
integer width = 137
integer height = 140
integer taborder = 210
boolean ib_fix_size = false
end type

on uo_font_color.destroy
call uo_color_cell::destroy
end on

event clicked;call super::clicked;ib_focus_on_color = true
is_hold_color = string(ai_color)
wf_update_dw("color='" + is_hold_color + "' ")

end event

type uo_font from uo_font_tab within w_report_grid_form
integer x = 23
integer y = 20
integer taborder = 40
end type

on uo_font.destroy
call uo_font_tab::destroy
end on

event ue_font_size_changed;call super::ue_font_size_changed;string ls_mod

ls_mod = "font.height='" + string(ai_font_size * -1) + "' "
wf_update_dw(ls_mod)
end event

event ue_font_type_changed;call super::ue_font_type_changed;wf_update_dw("font.face=~"" + as_font_name + "~" ")
end event

event ue_font_variant_changed;call super::ue_font_variant_changed;string ls_mod

dw_report.setredraw(false)

if ab_bold then
	ls_mod = "font.weight='700'"
else
	ls_mod = "font.weight='400'"
end if
wf_update_dw(ls_mod)

if ab_italic then
	ls_mod = "font.italic=1"
else
	ls_mod = "font.italic=0"
end if
wf_update_dw(ls_mod)

if ab_underline then
	ls_mod = "font.weight=1"
else
	ls_mod = "font.weight=0"
end if
wf_update_dw(ls_mod)

dw_report.setredraw(true)

end event

event ue_font_align_changed;call super::ue_font_align_changed;string ls_mod

//0 - (Default) Left
//1 - Right
//2 - Center
//3 - Justified

ls_mod = "alignment=" + string(ai_align)
wf_update_dw(ls_mod)
end event

type st_2 from statictext within w_report_grid_form
integer x = 1097
integer y = 60
integer width = 402
integer height = 64
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Bordo"
boolean focusrectangle = false
end type

type ddlb_border from dropdownpicturelistbox within w_report_grid_form
integer x = 1097
integer y = 140
integer width = 905
integer height = 804
integer taborder = 30
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean sorted = false
string item[] = {"None","Shadow Box","Box","Resize","Underline","3D Lowered","3D Raised"}
borderstyle borderstyle = stylelowered!
integer itempictureindex[] = {1,2,3,6,7,4,5}
string picturename[] = {"BorderNone!","BorderShadowBox!","BorderBox!","Border3DLowered!","Border3DRaised!","BorderResize!","BorderUnderline!"}
integer picturewidth = 16
integer pictureheight = 16
long picturemaskcolor = 536870912
end type

event selectionchanged;string ls_mod

ls_mod = "border=" + string(index - 1) + ""
wf_update_dw(ls_mod)
end event

type uo_colors from uo_color_tab within w_report_grid_form
integer x = 2423
integer y = 60
integer width = 1198
integer taborder = 40
end type

on uo_colors.destroy
call uo_color_tab::destroy
end on

event ue_selected_color;call super::ue_selected_color;if ib_focus_on_color then
	uo_font_color.uof_set_color(ai_color)
	uo_font_color.event Clicked(ai_color)
else
	uo_background_color.uof_set_color(ai_color)
	uo_background_color.event clicked(ai_color)
end if
end event

type dw_report from uo_cs_xx_dw within w_report_grid_form
event ue_leftbottonup pbm_lbuttonup
integer x = 23
integer y = 280
integer width = 4023
integer height = 1600
integer taborder = 30
string dataobject = ""
boolean hscrollbar = true
boolean vscrollbar = true
end type

event ue_leftbottonup;//object_style los_style
//string ls_mod, ls_nome_oggetto, ls_colore_sfondo
//int li_i
//
//// se ho degli oggetti selezionati ma il tasto Cntl non è premuto,
//// allora questa non è una multi-selezione e devo deselezionare tutti
//// gli oggetti precedentemente selezionati resettando i colori di sfondo e
//// testo ai vecchi valori
//
//if KeyDown(keycontrol!) = false and UpperBound(is_nome_oggetti) > 0 then
//	for li_i = 1 to UpperBound(is_nome_oggetti)
//		if is_nome_oggetti[li_i] <> "" then
//			// Se lo sfondo originale è trasparente forzo il "mode"
//			if is_hold_backmode [li_i] = "1" then
//				ls_colore_sfondo = is_nome_oggetti [li_i] + ".background.mode=~"1~"" 
//			else
//				ls_colore_sfondo = is_nome_oggetti [li_i] + ".background.color=~"" + is_hold_backcolor[li_i] + "~""
//			end if
//			ls_mod = is_nome_oggetti[li_i] + ".color=~"" + is_hold_color[li_i] + "~" " + ls_colore_sfondo
//			dw_report.Modify(ls_mod)
//			is_nome_oggetti[li_i] = ""
//		end if
//	next
//end if
//
//ls_nome_oggetto = dw_report.GetObjectAtPointer( )
//if ls_nome_oggetto = "" then Return
//ls_nome_oggetto = f_estrai_token(ls_nome_oggetto, "~t")
//
//if UpperBound(is_nome_oggetti) = 0 then
//	li_i = 1
//else
//	For li_i = 1 to UpperBound(is_nome_oggetti)
//		if is_nome_oggetti [li_i] = ls_nome_oggetto then return
//		if is_nome_oggetti [li_i] = "" then exit
//	next
//end if
//
//is_nome_oggetti[li_i] = ls_nome_oggetto
//
//is_hold_color[li_i] = dw_report.Describe(ls_nome_oggetto + ".color")
//is_hold_backcolor [li_i] = dw_report.Describe(ls_nome_oggetto + ".background.color")
//is_hold_backmode [li_i] = dw_report.Describe(ls_nome_oggetto + ".background.mode")
//
//if is_hold_color[li_i] = "?" then is_hold_color[li_i] = "0"
//
//if is_hold_backmode [li_i] = "1" then
//	is_hold_backcolor [li_i] = dw_report.Describe("datawindow.color")
//end if
//
//los_style.font_face = dw_report.Describe(ls_nome_oggetto + ".font.face")
//los_style.font_weight = dw_report.Describe(ls_nome_oggetto + ".font.weight") 
//los_style.italic = dw_report.Describe(ls_nome_oggetto + ".font.italic")
//los_style.underline = dw_report.Describe(ls_nome_oggetto + ".font.underline")
//los_style.border = dw_report.Describe(ls_nome_oggetto + ".border")
//los_style.alignment = dw_report.Describe(ls_nome_oggetto + ".alignment")
//los_style.font_height = dw_report.Describe(ls_nome_oggetto + ".font.height")
//
//if Integer(los_style.font_height) > 0 Then 
//	los_style.font_height = 	wf_conv_altezza_punti(dw_report,los_style.font_height)
//else
//	los_style.font_height =  String(Abs(Integer(los_style.font_height)))
//end if
//
//if dw_report.Describe(ls_nome_oggetto + ".type") = "text" and li_i = 1 then
//	los_style.text = f_elimina_carattere (dw_report.Describe(ls_nome_oggetto + ".text"), "~"")
//	is_text = true
//else
//	los_style.text = ""
//	is_text = false
//end if
//
//ls_mod = ls_nome_oggetto + ".background.mode=~"0~" " + &
//				 ls_nome_oggetto + ".color=~"" + string(rgb(255,255,255)) + "~" " + &
//				 ls_nome_oggetto + ".background.color=~"0~"" 
//dw_report.Modify(ls_mod)
//
//ib_updating = true
//
//uo_font.uof_set_fontstyle(los_style)
//wf_set_border(los_style)
//
//uo_font_color.uof_set_color(long(dw_report.Describe(ls_nome_oggetto + ".color")))
//uo_background_color.uof_set_color(long(dw_report.Describe(ls_nome_oggetto + ".background.color")))
//
//ib_updating = false
end event

event pcd_retrieve;call super::pcd_retrieve;long ll_errore


ll_errore = retrieve()

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event clicked;call super::clicked;string ls_mod
object_style los_style

if len(is_current_object) > 0 then
	if is_current_object <> "" then
		if is_hold_backmode = "1" then
			ls_mod = is_current_object + ".background.mode=~"1~"" 
		else
			ls_mod = is_current_object + ".background.color=~"" + is_hold_backcolor + "~""
		end if
		
		ls_mod = is_current_object + ".color=~"" + is_hold_color + "~" " + ls_mod
		dw_report.Modify(ls_mod)
		is_current_object = ""
		ls_mod = ""
	end if
end if

is_current_object = string(dwo.name)
parent.title = is_current_object

los_style.font_face = dw_report.Describe(is_current_object + ".font.face")
los_style.font_weight = dw_report.Describe(is_current_object + ".font.weight") 
los_style.italic = dw_report.Describe(is_current_object + ".font.italic")
los_style.underline = dw_report.Describe(is_current_object + ".font.underline")
los_style.border = dw_report.Describe(is_current_object + ".border")
los_style.alignment = dw_report.Describe(is_current_object + ".alignment")
los_style.font_height = dw_report.Describe(is_current_object + ".font.height")

if Integer(los_style.font_height) > 0 Then 
	los_style.font_height = wf_conv_altezza_punti(dw_report,los_style.font_height)
else
	los_style.font_height = String(Abs(Integer(los_style.font_height)))
end if


is_hold_color= dw_report.Describe(is_current_object + ".color")
is_hold_backcolor  = dw_report.Describe(is_current_object + ".background.color")
is_hold_backmode = dw_report.Describe(is_current_object + ".background.mode")

if is_hold_color = "?" then is_hold_color = "0"

if is_hold_backmode = "1" then
	is_hold_backcolor = dw_report.Describe("datawindow.color")
end if

los_style.font_face = dw_report.Describe(is_current_object + ".font.face")
los_style.font_weight = dw_report.Describe(is_current_object + ".font.weight") 
los_style.italic = dw_report.Describe(is_current_object + ".font.italic")
los_style.underline = dw_report.Describe(is_current_object + ".font.underline")
los_style.border = dw_report.Describe(is_current_object + ".border")
los_style.alignment = dw_report.Describe(is_current_object + ".alignment")
los_style.font_height = dw_report.Describe(is_current_object + ".font.height")

if Integer(los_style.font_height) > 0 Then 
	los_style.font_height = wf_conv_altezza_punti(dw_report,los_style.font_height)
else
	los_style.font_height =  String(Abs(Integer(los_style.font_height)))
end if

//if dw_report.Describe(is_current_object + ".type") = "text" and li_i = 1 then
//	los_style.text = f_elimina_carattere (dw_report.Describe(ls_nome_oggetto + ".text"), "~"")
//	is_text = true
//else
//	los_style.text = ""
//	is_text = false
//end if

// colore di selezione
ls_mod = is_current_object + ".background.mode=~"0~" " + &
				 is_current_object + ".color='" + string(il_select_color) + "' " + &
				 is_current_object + ".background.color='" + string(il_select_backcolor) + "' " 
dw_report.Modify(ls_mod)

ib_updating = true

uo_font.uof_set_fontstyle(los_style)
wf_set_border(los_style)

uo_font_color.uof_set_color(long(is_hold_color))
uo_background_color.uof_set_color(long(is_hold_backcolor))

ib_updating = false
end event

type cb_esporta from commandbutton within w_report_grid_form
event clicked pbm_bnclicked
integer x = 3680
integer y = 60
integer width = 366
integer height = 100
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Esporta IM"
end type

event clicked;string ls_sintassi, ls_errore, ls_nome_pbl, ls_nome_report
integer li_risposta

ls_sintassi = dw_report.Describe("DataWindow.Syntax")
integer value

GetFileSaveName("APICE: Selezione Archivio", ls_nome_pbl, ls_nome_report, "", "Pbl Files (*.PBL),*.PBL,")
if len(ls_nome_pbl) = 0 then return

if len(em_nome_report.text) = 0 then
	g_mb.messagebox("Apice","Indicare il Nome del Report",Exclamation!)
	return
end if

if not FileExists(ls_nome_pbl) then
	LibraryCreate(ls_nome_pbl)
end if

li_risposta = LibraryImport(ls_nome_pbl, em_nome_report.text, ImportDataWindow!, ls_sintassi, ls_errore)
if li_risposta = -1 then
	g_mb.messagebox("Apice","Errore Durante l'Esportazione del Report", Exclamation!)
end if

end event

type em_nome_report from editmask within w_report_grid_form
integer x = 3680
integer y = 160
integer width = 366
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
string text = "Report_1"
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
string mask = "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
string displaydata = "$"
end type

event modified;parent.title = "Report : " + this.text
end event

