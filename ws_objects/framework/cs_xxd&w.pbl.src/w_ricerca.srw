﻿$PBExportHeader$w_ricerca.srw
forward
global type w_ricerca from window
end type
type cbx_salva_ricerca from checkbox within w_ricerca
end type
type tab_1 from tab within w_ricerca
end type
type ricerca from userobject within tab_1
end type
type dw_ricerca from datawindow within ricerca
end type
type dw_selezione from datawindow within ricerca
end type
type ricerca from userobject within tab_1
dw_ricerca dw_ricerca
dw_selezione dw_selezione
end type
type personalizza from userobject within tab_1
end type
type dw_colonne from datawindow within personalizza
end type
type personalizza from userobject within tab_1
dw_colonne dw_colonne
end type
type tab_1 from tab within w_ricerca
ricerca ricerca
personalizza personalizza
end type
end forward

global type w_ricerca from window
integer width = 3223
integer height = 1952
boolean titlebar = true
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 12632256
boolean center = true
event ue_dw_ricerca_focus ( )
event ue_dw_selezione_focus ( )
cbx_salva_ricerca cbx_salva_ricerca
tab_1 tab_1
end type
global w_ricerca w_ricerca

type variables
protected:
	str_ricerca istr_data

private:
	string is_select_columns[]
	string is_join_query = ""
	string is_where_query = ""
	
	// tipi di operatori
	constant string STRINGA = "S"
	constant string NUMERO = "N"
	constant string DATA_TYPE = "D"
	
	// stile
	constant string FONT_FACE = "Arial"
	constant int FONT_SIZE = 9
	constant int COLUMN_OPERATOR_WIDTH = 100
	constant int SEARCH_HEADER_HEIGHT = 80
	constant int SEARCH_DETAIL_HEIGHT = 80
	constant long COLUMN_OPERATOR_BACKGROUND = 13303807
	
	// drag&drop
	int ii_dragged_row = -1
	
	// sort
	string is_last_sort_column = ""
	boolean ib_sorting_asc = true
	
	boolean ib_autoretrieve = false
	boolean ib_user_cancel = true
	boolean ib_dw = true
	
	// transazione diversa per non lanciare commit su altre transazioni
	string is_ultima_ricerca
	transaction it_sqlcb
end variables

forward prototypes
private subroutine wf_read_table_column ()
private function string wf_join (string as_array[], string as_separator)
private function boolean wf_hidden_column (string as_column_name)
private function boolean wf_fixed_column (string as_column_name)
public subroutine wf_return_selected_value (long al_row)
private function boolean wf_build_dw_ricerca ()
private function boolean wf_build_dw_selezione ()
private function string wf_build_query_string ()
private function boolean wf_dw_status_modified ()
private subroutine wf_read_user_column ()
private subroutine wf_save_user_column ()
public subroutine wf_set_dw_ricerca_focus ()
private function boolean wf_create_column (integer ai_id, integer ai_column_id, long al_x, integer ai_tab_sequence)
private subroutine wf_set_editmask (string as_col_name)
public subroutine wf_close_window (ref any aa_return_values[])
public function boolean wf_column_code (string as_column_name, ref long al_position)
public subroutine wf_save_window_state ()
private subroutine wf_build_where_query ()
private subroutine wf_set_window_title (long al_result)
public subroutine wf_read_user_column_input ()
public subroutine wf_set_dw_selezione_focus ()
private function boolean wf_get_user_column (ref string as_columns)
public subroutine wf_read_user_check ()
public subroutine wf_save_user_check ()
end prototypes

event ue_dw_ricerca_focus();wf_set_dw_ricerca_focus()
end event

event ue_dw_selezione_focus();wf_set_dw_selezione_focus()
end event

private subroutine wf_read_table_column ();string ls_nome_colonna,  ls_db, ls_nome, ls_sql, ls_logid, ls_owner, ls_errore, ls_column_name, ls_nome_tabella
integer li_i, li_count, li_j, li_sort
long ll_id, ll_row, ll_pk_sort
datastore lds_store

ls_db = f_db()
choose case ls_db
	case "SYBASE_ASA"
						
		select table_id
		into   :ll_id
		from   systable
		where  table_name = :istr_data.table_name;

		if sqlca.sqlcode <> 0 then
			g_mb.error("Errore nel caricamento id tabella (" + string(sqlca.sqlcode) + ")(" + istr_data.table_name + ").", sqlca)
			return
		end if
			
		ls_sql = " SELECT column_name FROM syscolumn WHERE table_id = " + string(ll_id)
		ls_sql += " AND domain_id <> 12 "	
		ls_sql += " ORDER BY column_name, column_id "		
		
		
	case "SYBASE_ASE"
				
		select id
		into   :ll_id
		from   sysobjects
		where  name = :istr_data.table_name;
		
		if sqlca.sqlcode <> 0 then
			g_mb.error("Errore nel caricamento id tabella (" + string(sqlca.sqlcode) + ")(" + istr_data.table_name + ").", sqlca)
			return
		end if

		ls_sql = " select name from syscolumns WHERE id = " + string(ll_id) 
		ls_sql = ls_sql + " AND type <> 34 "
		ls_sql = ls_sql + " ORDER BY name "		

	case "MSSQL"
		
		select id
		into   :ll_id
		from   sysobjects
		where  name = :istr_data.table_name;
		
		if sqlca.sqlcode <> 0 then
			g_mb.error("Errore nel caricamento id tabella (" + string(sqlca.sqlcode) + ")(" + istr_data.table_name + ").", sqlca)
			return
		end if		

		ls_sql = " select name from syscolumns WHERE id = " + string(ll_id) 
		ls_sql += " AND xtype <> 34 "
		ls_sql += " ORDER BY name, colid "	
	
		
	case "ORACLE"
				
		ls_nome_tabella = Upper(istr_data.table_name)
		
		ls_logid = sqlca.logid
		
		SELECT sys_context('USERENV', 'CURRENT_SCHEMA') into   :ls_owner FROM dual;
		select OBJECT_ID into :ll_id from all_objects where OWNER=:ls_owner and OBJECT_NAME=:ls_nome_tabella ;
/*
		select User#
		into   :ls_owner
		from   sys.user$
		where  name = :ls_logid;
				
		select obj#
		into   :ll_id
		from   sys.obj$
		where  name = :ls_nome_tabella
		and    owner# = :ls_owner;
*/
		if sqlca.sqlcode <> 0 then
			g_mb.error("Errore nel caricamento id tabella (" + string(sqlca.sqlcode) + ")(" + ls_nome_tabella + ").", sqlca)
			return
		end if
			
		ls_sql = " SELECT name FROM sys.col$ WHERE obj# = " + string(ll_id)
		ls_sql = ls_sql + " AND type# <> 24 order by name"
		
end choose

if not f_crea_datastore(ref lds_store, ls_sql) then
	g_mb.error("Errore durante la creazione del datastore per la tabella " + istr_data.table_name)
	return
end if

li_count = lds_store.retrieve()
if li_count < 0 then
	g_mb.error("Errore durante il controllo delle righe presenti nella tabella " + istr_data.table_name)
	return
end if

tab_1.personalizza.dw_colonne.reset()
li_sort = 500 // parto da un valore altro così l'ordinamento funziona

for li_i = 1 to li_count
	
	ls_column_name = lds_store.getitemstring(li_i, 1)
	
	if not wf_hidden_column(ls_column_name) then
		ll_row = tab_1.personalizza.dw_colonne.insertrow(0)
		tab_1.personalizza.dw_colonne.setitem(ll_row, "column_label", ls_column_name)
		tab_1.personalizza.dw_colonne.setitem(ll_row, "column_name", ls_column_name)
		tab_1.personalizza.dw_colonne.setitem(ll_row, "column_table", istr_data.table_name)
		
		if wf_fixed_column(ls_column_name) then
			tab_1.personalizza.dw_colonne.setitem(ll_row, "column_visible", "S")
			tab_1.personalizza.dw_colonne.setitem(ll_row, "column_fixed", "S")
		end if
	end if
	
	if wf_column_code(ls_column_name, ll_pk_sort) then tab_1.personalizza.dw_colonne.setitem(ll_row, "column_pk", "S")
	
	tab_1.personalizza.dw_colonne.setitem(ll_row, "column_sort", li_sort)
	li_sort++
next

// aggiungo colonne join
for li_i = 1 to upperbound(istr_data.joins)
	for li_j = 1 to upperbound(istr_data.joins[li_i].columns)
		
		tab_1.personalizza.dw_colonne.setitem(ll_row, "column_label", istr_data.joins[li_i].columns[li_j])
		tab_1.personalizza.dw_colonne.setitem(ll_row, "column_name", istr_data.joins[li_i].columns[li_j])
		tab_1.personalizza.dw_colonne.setitem(ll_row, "column_table", istr_data.joins[li_i].table)
		tab_1.personalizza.dw_colonne.setitem(ll_row, "column_join", "S")
		tab_1.personalizza.dw_colonne.setitem(ll_row, "join_query", istr_data.joins[li_i].where)
		
		if wf_fixed_column(istr_data.joins[li_i].columns[li_j]) then
			tab_1.personalizza.dw_colonne.setitem(ll_row, "column_visible", "S")
			tab_1.personalizza.dw_colonne.setitem(ll_row, "column_fixed", "S")
		end if
	next
	
	if wf_column_code(ls_column_name, ll_pk_sort) then tab_1.personalizza.dw_colonne.setitem(ll_row, "column_pk", "S")

	tab_1.personalizza.dw_colonne.setitem(ll_row, "column_sort", li_sort)
	li_sort++
next

end subroutine

private function string wf_join (string as_array[], string as_separator);/**
 * da inserire nell'oggetto globale
 **/

string ls_join
int li_i

if upperbound(as_array) < 1 then
	return ""
elseif upperbound(as_array) = 1 then
	return as_array[1]
else
	ls_join = as_array[1]

	for li_i = 2 to upperbound(as_array)
	
		ls_join += as_separator + as_array[li_i]
	
	next
		
	return ls_join
end if

return ""

end function

private function boolean wf_hidden_column (string as_column_name);/**
 * stefanop
 * 20/09/2011
 *
 * Controllo che la colonna non si nascosta, in quel caso ritorno true (se nascosta) e false (se visibile)
 **/

return guo_functions.uof_in_array(as_column_name, istr_data.column_hidden) 
end function

private function boolean wf_fixed_column (string as_column_name);/**
 * stefanop
 * 20/09/2011
 *
 * Controllo che la colonna non si nascosta, in quel caso ritorno true (se nascosta) e false (se visibile)
 **/

return guo_functions.uof_in_array(as_column_name, istr_data.column_fixed)
end function

public subroutine wf_return_selected_value (long al_row);/**
 * stefanop
 * 23/09/2011
 *
 * Ritorna i valori selezionati e chiude la finestra
 **/
 
string ls_coltype, ls_value, ls_colname
int li_i
decimal ld_value
datetime ldt_value
any la_return_value[]
dwobject ldw_object

// controllo che la finestra padre non sia stata chiusa
if ib_dw then
	if not isvalid(istr_data.dw) then
		g_mb.show("Ricerca", "Impossibile impostare il valore selezionato.~r~nChiusura anticipata della finestra destinazione del valore.")
		close(this)
		return
	end if
end if


if ib_dw then istr_data.dw.accepttext()
	
for li_i = 1 to upperbound(istr_data.dw_search_columns)
	
	ls_colname = istr_data.column_read[li_i]
	ls_coltype = lower(left(tab_1.ricerca.dw_selezione.Describe(ls_colname + ".ColType"), 4))
	
	if ib_dw then
		// imposto il fuoco sulla colonna altrimenti il framework non legge correttamente le
		// variabili i_colname e i_coltext
		istr_data.dw.setcolumn(istr_data.dw_search_columns[li_i])
		
		// leggo il dwobject per far scattare l'itemchanged
		// !!! FUNZIONE SUPER SEGRETA !!!
		ldw_object = istr_data.dw.object.__get_attribute(istr_data.dw_search_columns[li_i],false) 
	end if
	
	choose case ls_coltype
		case "char"
			ls_value = tab_1.ricerca.dw_selezione.getitemstring(al_row, ls_colname)
			if ib_dw then
				istr_data.dw.setitem(istr_data.dw.getrow(), istr_data.dw_search_columns[li_i], ls_value)
				istr_data.dw.event itemchanged(istr_data.dw.getrow(), ldw_object, ls_value)				
			end if
			la_return_value[upperbound(la_return_value) + 1] = ls_value

		case "date"
			ldt_value = tab_1.ricerca.dw_selezione.getitemdatetime(al_row, ls_colname)
			if ib_dw then
				istr_data.dw.setitem(istr_data.dw.getrow(), istr_data.dw_search_columns[li_i], ldt_value)
				istr_data.dw.event itemchanged(istr_data.dw.getrow(), ldw_object, string(ldt_value))
			end if
			la_return_value[upperbound(la_return_value) + 1] = ldt_value
			
		case "deci", "long", "numb"
			ld_value = tab_1.ricerca.dw_selezione.getitemnumber(al_row, ls_colname)
			if ib_dw then
				istr_data.dw.setitem(istr_data.dw.getrow(), istr_data.dw_search_columns[li_i], ld_value)
				istr_data.dw.event itemchanged(istr_data.dw.getrow(), ldw_object, string(ld_value))
			end if
			la_return_value[upperbound(la_return_value) + 1] = ld_value
			
	end choose

next

wf_save_window_state()
ib_user_cancel = false

if ib_dw then 
	istr_data.dw.accepttext()
	istr_data.dw.setfocus()
	
	window lw_win
	lw_win = pcca.window_current
	close(this)
else
	str_ricerca lstr_ricerca
	lstr_ricerca.return_values = la_return_value
	CloseWithReturn(this, lstr_ricerca)
end if

end subroutine

private function boolean wf_build_dw_ricerca ();/**
 * Creo ed imposto le proprietà per la datawindow
 * di ricerca
 **/
 
string ls_query, ls_syntax, ls_presentation, ls_error, ls_columns[], ls_col_name
int li_i, li_column_count, li_col_op_width = 100, li_height = 0, li_tab_sequence
long ll_width_offset = 0

tab_1.ricerca.dw_ricerca.setredraw(false)

// ciclo le colonne "vere" e inietto le colonne "operatore".
// E' necessario farlo qui in quanto la creazione della DW crea un ID assegnato alla colonna
// che verrà poi usato per creare in maniera dimanica le DROP.
// Le colonne op_hidden saranno nascoste alla visualizzazione
ls_columns = is_select_columns
for li_i = 1 to upperbound(is_select_columns)
	
	ls_columns[upperbound(ls_columns) + 1] = "'     ' as op_hidden_" + string(li_i)
	
next

// Creo datawindow
ls_query = "SELECT " + wf_join(ls_columns, ", ") 
ls_query += " FROM " + istr_data.table_name + is_join_query //  + is_where_query


ls_presentation = "style( type=Grid) &
column( Font.Face='"+FONT_FACE+"' Font.Height=-"+string(FONT_SIZE) +") &
text( Font.Face='"+FONT_FACE+"' Font.Height=-"+string(FONT_SIZE)+")"

ls_syntax = sqlca.SyntaxFromSQL(ls_query, ls_presentation, ls_error)

if len(ls_error) > 0 then
	g_mb.error("Errore creazione sintassi SQL dw_ricerca!~r~n" + ls_error)
	return false
else
	tab_1.ricerca.dw_ricerca.create(ls_syntax, ls_error)
	if len(ls_error) > 0 then 
		g_mb.error("Errore creazione datawindow dw_ricerca!~r~n" + ls_error)
		return false
	end if
end if

ll_width_offset = 0
li_tab_sequence = 0

// Processo solo le colonne reali non le colonne "operatore"
li_column_count = integer(tab_1.ricerca.dw_ricerca.Describe("DataWindow.Column.Count")) / 2
for li_i = 1 to li_column_count
	
	ls_col_name = "#" + string(li_i)
	
	// nascondo la colonna operatore creata dalla query (non posso usare questa colonna
	// in quanto PB la crea con style "edit" e non posso cambiarlo in "DropDownListBox")
	tab_1.ricerca.dw_ricerca.modify("op_hidden_" + string(li_i) + ".Visible=0")
	
	// Riposiziono correttamente le colonne: prima la colonna "vera" e poi la colonna "operatore"
	tab_1.ricerca.dw_ricerca.modify(ls_col_name + '.x="' + string(ll_width_offset) + "'")
	ll_width_offset += long(tab_1.ricerca.dw_ricerca.Describe(ls_col_name + ".Width"))
	
	// Imposto il tab sequence corretto
	li_tab_sequence++
	tab_1.ricerca.dw_ricerca.modify(ls_col_name + ".TabSequence=" + string(li_tab_sequence * 10))
	li_tab_sequence++
	
	wf_create_column(li_i, li_column_count + li_i, ll_width_offset, li_tab_sequence * 10)
	ll_width_offset += COLUMN_OPERATOR_WIDTH	 
	
	tab_1.ricerca.dw_ricerca.modify(ls_col_name + ".Edit.Required=No")
	tab_1.ricerca.dw_ricerca.modify(ls_col_name + ".Edit.NilIsNull=Yes")
	tab_1.ricerca.dw_ricerca.modify(ls_col_name + ".Validation=''")
	tab_1.ricerca.dw_ricerca.modify(ls_col_name + ".Initial='empty'")
next

// Imposto le altezze
tab_1.ricerca.dw_ricerca.modify('DataWindow.Header.Height=' + string(SEARCH_HEADER_HEIGHT))
tab_1.ricerca.dw_ricerca.modify('DataWindow.Detail.Height=' + string(SEARCH_DETAIL_HEIGHT))
tab_1.ricerca.dw_ricerca.modify('DataWindow.Detail.Height.Autosize=Yes')

tab_1.ricerca.dw_ricerca.Object.DataWindow.Grid.ColumnMove = 'No'
tab_1.ricerca.dw_ricerca.Object.DataWindow.Detail.Color= COLUMN_OPERATOR_BACKGROUND
tab_1.ricerca.dw_ricerca.insertrow(0)
tab_1.ricerca.dw_selezione.reset()
tab_1.ricerca.dw_ricerca.Object.DataWindow.Syntax.Modified = "no"

wf_read_user_column_input()

if ib_autoretrieve then 
	wf_build_dw_selezione()
	postevent("ue_dw_selezione_focus")
else
	postevent("ue_dw_ricerca_focus")
end if

tab_1.ricerca.dw_ricerca.setredraw(true)
return true
end function

private function boolean wf_build_dw_selezione ();/**
 * Creo ed imposto le proprietà per la datawindow
 * di selezione
 **/
 
string ls_query, ls_syntax, ls_presentation, ls_error
int li_i, li_col_width, ll_shift

tab_1.ricerca.dw_selezione.setredraw(false)

ls_query = wf_build_query_string()

ls_presentation = "style( type=Grid) &
column( Font.Face='"+FONT_FACE+"' Font.Height=-"+string(FONT_SIZE)+") &
text( Font.Face='"+FONT_FACE+"' Font.Height=-"+string(FONT_SIZE)+")"

ls_syntax = it_sqlcb.SyntaxFromSQL(ls_query, ls_presentation, ls_error)

if len(ls_error) > 0 then
	g_mb.error("Errore creazione sintassi SQL dw_selezione!~r~n" + ls_error)
	return false
else
	tab_1.ricerca.dw_selezione.create(ls_syntax, ls_error)
	if len(ls_error) > 0 then
		g_mb.error("Errore creazione datawindow dw_selezione!~r~n" + ls_error)
		return false
	end if
end if

//  metto a 0 l'altezza dell'header, qui non si deve vedere è già presente nella ricerca
tab_1.ricerca.dw_selezione.modify("DataWindow.Header.Height=0")
ll_shift = 0
for li_i = 1 to integer(tab_1.ricerca.dw_selezione.describe("Datawindow.Column.Count"))
	ll_shift ++
	if tab_1.ricerca.dw_selezione.describe("#" + string(li_i) + ".Name") = "blocked_record" then
		tab_1.ricerca.dw_selezione.modify("#" + string(li_i) + ".Visible=0")
		ll_shift --
		continue
	end if
	
	string ls_name1, ls_name2
	
	ls_name1 = tab_1.ricerca.dw_ricerca.describe("#" + string(ll_shift) + ".Name")
	ls_name2 = tab_1.ricerca.dw_selezione.describe("#" + string(li_i) + ".Name")

	li_col_width = integer(tab_1.ricerca.dw_ricerca.describe("#" + string(ll_shift) + ".Width"))
	li_col_width += integer(tab_1.ricerca.dw_ricerca.describe("op_" + string(ll_shift) + ".Width"))
	li_col_width += 10 // penso sia la dimensione dei bordi, cmq serve altrimenti non viene allineato

	tab_1.ricerca.dw_selezione.modify("#" + string(li_i) + ".Width=" + string(li_col_width))
	tab_1.ricerca.dw_selezione.modify("#" + string(li_i) + ".TabSequence=0")
	tab_1.ricerca.dw_selezione.modify("#" + string(li_i) + ".EditMask.Spin=No")	
	tab_1.ricerca.dw_selezione.modify("#" + string(li_i) + ".Color='0~tIf(blocked_record = ~~'S~~',255,16711680)'")	

next 

tab_1.ricerca.dw_selezione.modify("DataWindow.Detail.Height=" + string(SEARCH_DETAIL_HEIGHT))
tab_1.ricerca.dw_selezione.settransobject(sqlca)
tab_1.ricerca.dw_selezione.setredraw(true)
wf_set_window_title(tab_1.ricerca.dw_selezione.retrieve())
return true
end function

private function string wf_build_query_string ();/**
 * stefanop
 * 07/09/2011
 *
 * Crea la stringa di ricerca per creare la datawindow in base alle colonne personali
 **/
 
string ls_col_name, ls_col_type, ls_col_op, ls_value_string, ls_column
string ls_query, ls_where[]
int li_i
decimal ld_value_number
date ldt_value_date
datetime ldt_value_date_time

ls_query   = "SELECT " 

if istr_data.distinct then ls_query += "DISTINCT "

if istr_data.evidenzia_bloccati then ls_query += " flag_blocco blocked_record, "

ls_query += wf_join(is_select_columns, ", ") 
ls_query += " FROM " + istr_data.table_name + is_join_query 

if len(is_where_query) > 0 then ls_query += " WHERE " + is_where_query

tab_1.ricerca.dw_ricerca.accepttext()

for li_i=1 to upperbound(is_select_columns)
	
	ls_column = is_select_columns[li_i]
	ls_col_type = lower(tab_1.ricerca.dw_ricerca.describe("#" + string(li_i) + ".ColType"))
	ls_col_op = tab_1.ricerca.dw_ricerca.getitemstring(1, "op_" + string(li_i))
	
	choose case left(ls_col_type, 3)
		case "cha"
			ls_value_string = tab_1.ricerca.dw_ricerca.getitemstring(1, li_i)
			if isnull(ls_value_string) or ls_value_string = "" then continue
			
			// stefanop: 19/01/2012: abilito l'utilizzo del carattere * per poter filtrare
			guo_functions.uof_replace_string(ls_value_string, "*", "%")
			// ----
			
			// stefanop 21/12/212: abilito case insensitive
			ls_column = "upper(" + is_select_columns[li_i] + ")"
			ls_value_string = upper(ls_value_string)
			// ----
			
			if ls_col_op = "%L" then
				ls_value_string = "'%" + ls_value_string + "'"
				ls_col_op = " LIKE "
			elseif ls_col_op = "L%" then
				ls_value_string = "'" + ls_value_string + "%'"
				ls_col_op = " LIKE "
			elseif ls_col_op = "=" then
				ls_value_string = "'" + ls_value_string + "'"
			elseif ls_col_op = "L" then
				ls_value_string = "'" + ls_value_string + "'"
				ls_col_op = " LIKE "
			else
				ls_value_string = "'%" + ls_value_string + "%'"
				ls_col_op = " LIKE "
			end if
			
			
		case "dec", "int", "lon", "num", "rea"
			ld_value_number = tab_1.ricerca.dw_ricerca.getitemnumber(1, li_i)
			if isnull(ld_value_number) then continue
			ls_value_string = string(ld_value_number)
			guo_functions.uof_replace_string(ls_value_string, ",", ".")
			
		case "dat"
			if ls_col_type = "datetime" then
				ldt_value_date_time = tab_1.ricerca.dw_ricerca.getitemdatetime(1, li_i)
				if isnull(ldt_value_date_time) then continue
				ls_value_string = "'" + string(ldt_value_date_time, s_cs_xx.db_funzioni.formato_data) + "'"
			else
				ldt_value_date = tab_1.ricerca.dw_ricerca.getitemdate(1, li_i)
				if isnull(ldt_value_date) then continue
				ls_value_string = "'" + string(ldt_value_date, s_cs_xx.db_funzioni.formato_data) + "'"
			end if
			
		case else
			g_mb.show("La tipologia " + ls_col_type + " non è codificata!")
			continue
			
	end choose

		
	ls_where[upperbound(ls_where) + 1] = ls_column + " " + ls_col_op + ls_value_string
	
next

if upperbound(ls_where) > 0 then

	if len(is_where_query) > 0 then
		ls_query += " AND "
	else
		ls_query += " WHERE "
	end if
	
	ls_query += wf_join(ls_where, " AND ")
	
end if

return ls_query
end function

private function boolean wf_dw_status_modified ();/**
 * stefanop
 * 06/09/2011
 *
 * Controllo se è stato modificato qualcosa nella dw delle colonne personalizzate, in quel caso del salvare le modifiche
 **/
 
long ll_i 

for ll_i = 1 to tab_1.personalizza.dw_colonne.rowcount()
	
	if tab_1.personalizza.dw_colonne.GetItemStatus(ll_i, 0, Primary!) = DataModified!	 then
		return true
	end if
	
next

return false
end function

private subroutine wf_read_user_column ();/**
 * stefanop
 * 06/09/2011
 *
 * Leggo le colonne impostate dall'utente per creare la datawindow
 **/
 
string ls_db_columns, ls_column[], ls_find, ls_join_tables[], ls_column_table
long ll_i, ll_row, ll_window_width, ll_window_height

is_join_query = ""

select colonne, window_width, window_height, ultima_ricerca
into :ls_db_columns, :ll_window_width, :ll_window_height, :is_ultima_ricerca
from tab_ricerche_utente
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_utente = :s_cs_xx.cod_utente and 
	nome_tabella = :istr_data.table_name;
		
if not isnull(ll_window_width) and ll_window_width > 500 then width = ll_window_width
if not isnull(ll_window_height) and ll_window_height > 500 then height = ll_window_height
	
if sqlca.sqlcode < 0 then
	g_mb.error("Errore durante il recupero delle colonne personali", sqlca)
	return
elseif sqlca.sqlcode = 0 and not isnull(ls_db_columns) and ls_db_columns <> "" then
	
	f_split(ls_db_columns, "~t", is_select_columns)
	
else
	
	// carico le colonne di default
	is_select_columns = istr_data.column_default
	
end if

	
for ll_i = 1 to upperbound(is_select_columns)
	
	if pos(is_select_columns[ll_i], ".") > 0 then
		// divido nome_tabella.colonna in array per poi utilizzare solo il nome colonna
		f_split(is_select_columns[ll_i], ".", ls_column)
		
		ls_find = "column_table='" + ls_column[1] + "' and column_name='" + ls_column[2] +"'"
	else
		ls_find = "column_name='" + is_select_columns[ll_i] +"'"
	end if

	ll_row = tab_1.personalizza.dw_colonne.find(ls_find, 0 , tab_1.personalizza.dw_colonne.rowcount())
	if ll_row < 1 then continue
	
	tab_1.personalizza.dw_colonne.setitem(ll_row, "column_sort", ll_i)
	
	// controllo che la colonna non sia "fixata", nel caso sia fixata è sicuramente selezionata
	if tab_1.personalizza.dw_colonne.getitemstring(ll_row, "column_fixed") = "N" then
		tab_1.personalizza.dw_colonne.setitem(ll_row, "column_visible", "S")
	end if
	// ---
	
	// colonna di join?
	if tab_1.personalizza.dw_colonne.getitemstring(ll_row, "column_join") = "S" then
		
		ls_column_table = tab_1.personalizza.dw_colonne.getitemstring(ll_row, "column_table")
		
		// controllo che non sia già stata inserita la tablenna nella query di join
		if not guo_functions.uof_in_array(ls_column_table, ls_join_tables) then
			is_join_query += " LEFT OUTER JOIN " + ls_column_table + " ON " + tab_1.personalizza.dw_colonne.getitemstring(ll_row, "join_query") + "~r~n"
			ls_join_tables[upperbound(ls_join_tables)+1] = ls_column_table
		end if
		
	end if
next

tab_1.personalizza.dw_colonne.sort()

// resetto lo stato della DW
tab_1.personalizza.dw_colonne.ResetUpdate()
end subroutine

private subroutine wf_save_user_column ();/**
 * stefanop
 * 06/09/2011
 *
 * Salve le colonne selezionate dall'utente per la tabella corrente
 **/
 
string ls_columns, ls_empty_array[], ls_test, ls_column, ls_column_table, ls_join_tables[]
long ll_i

wf_get_user_column(ref ls_columns)

// controllo se esiste il record nella tabella
select nome_tabella
into :ls_test
from tab_ricerche_utente
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_utente = :s_cs_xx.cod_utente and
	nome_tabella = :istr_data.table_name
using it_sqlcb;
	
if it_sqlcb.sqlcode <> 0 then
	
	insert into tab_ricerche_utente (
		cod_azienda,
		cod_utente,
		nome_tabella,
		colonne
	) values (
		:s_cs_xx.cod_azienda,
		:s_cs_xx.cod_utente,
		:istr_data.table_name,
		:ls_columns
	)
	using it_sqlcb;
			
else
	
	update tab_ricerche_utente
	set 
		colonne = :ls_columns,
		ultima_ricerca = :is_ultima_ricerca
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		cod_utente = :s_cs_xx.cod_utente and
		nome_tabella = :istr_data.table_name
	using it_sqlcb;
		
end if

if it_sqlcb.sqlcode <> 0 then
	g_mb.error("Errore durante il salvataggio delle colonne personalizzate", it_sqlcb)
	rollback using it_sqlcb;
else
	tab_1.personalizza.dw_colonne.ResetUpdate()
	commit using it_sqlcb;
end if
end subroutine

public subroutine wf_set_dw_ricerca_focus ();/**
 * Imposta il fuoco sulla datawindow di ricerca e ne seleziona la prima colonna
 **/
 
tab_1.ricerca.dw_ricerca.setfocus()
tab_1.ricerca.dw_ricerca.setcolumn(1)
end subroutine

private function boolean wf_create_column (integer ai_id, integer ai_column_id, long al_x, integer ai_tab_sequence);/**
 * stefanop
 * 10/10/2011
 * 
 * Creo la colonna operatore
 *
 * integer ai_id id della colonna da impostare
 * integer ai_column_id id della colonna op_hidden collegata
 * long al_x posizione x della colonna
 **/

string ls_column, ls_values, ls_initial, ls_error

ls_column = 'create column(band=detail id=' + string(ai_column_id) + ' alignment="2" tabsequence='+string(ai_tab_sequence)+' border="0" &
	color="33554432" x="' + string(al_x) + '" y="0" height="'+string(SEARCH_DETAIL_HEIGHT)+'" width="' +string(COLUMN_OPERATOR_WIDTH) +'" format="[general]" &
	html.valueishtml="0"  name=op_' + string(ai_id) +' visible="1" ddlb.limit=0 ddlb.allowedit=no ddlb.case=any &
	font.face="' + FONT_FACE + '" font.height="-'+string(FONT_SIZE)+'" font.weight="400"  font.family="2" font.pitch="2" &
	font.charset="0" background.mode="1" background.color="'+string(COLUMN_OPERATOR_BACKGROUND)+'" &
	background.transparency="0" transparency="0") '
	
// creo colonna
ls_error = tab_1.ricerca.dw_ricerca.modify(ls_column)
if ls_error <> "" then g_mb.error(ls_error)

// imposto correttamente la drop drop degli operatori in base alla tipologia di colonna
choose case lower(left(tab_1.ricerca.dw_ricerca.describe("#" + string(ai_id) + ".ColType"), 3))
	case "cha"
		ls_values = "=~t=/%~tLIKE/%L~t%L/L%~tL%/L~tL"
		ls_initial = "LIKE"
		
	case else
		ls_values = "=~t=/>~t>/<~t</<=~t<=/>=~t>="
		ls_initial = "="
		
end choose

tab_1.ricerca.dw_ricerca.modify('op_' + string(ai_id) + ".Values='" + ls_values + "'")
tab_1.ricerca.dw_ricerca.modify('op_' + string(ai_id) + ".Initial='" + ls_initial + "'")

return true
end function

private subroutine wf_set_editmask (string as_col_name);/**
 * stefanop
 * 11/10/2011
 *
 * Imposta il formato di maschera corretta in base alla tipologia della colonna
 **/
 
string ls_error, ls_col_type

ls_col_type = lower(tab_1.ricerca.dw_ricerca.Describe(as_col_name + ".ColType"))

choose case left(ls_col_type, 3)
		
	case "cha"
		
	case "dat"
		ls_error = tab_1.ricerca.dw_ricerca.modify(as_col_name + ".EditMask.DDCalendar=Yes")
		ls_error = tab_1.ricerca.dw_ricerca.modify(as_col_name + ".EditMask.Mask='dd/mm/yyyy'")
		//ls_error = tab_1.ricerca.dw_ricerca.modify(as_col_name + ".EditMask.DDCalendar=Yes")
		if ls_error <> "" then g_mb.error(ls_error)
		
		
end choose
end subroutine

public subroutine wf_close_window (ref any aa_return_values[]);/**
 * stefnaop
 * 12/10/2011
 *
 * Funzione astratta che chiude la finestra
 * Questa funzione viene estesa nella w_ricerca_response per 
 * permettere la chiusura della finestra corretta
 **/
 

end subroutine

public function boolean wf_column_code (string as_column_name, ref long al_position);/**
 * stefanop
 * 18/10/2011
 *
 * Se la colonna fa parte delle colonne che sono selezionate per il ritorno allora le 
 * fisso in prima posizione nella visualizzazione
 **/
 
return guo_functions.uof_in_array(as_column_name, istr_data.column_read, al_position)
end function

public subroutine wf_save_window_state ();/**
 * stefanop
 * 18/10/2011
 * 
 * Memorizzo le dimensioni della finestra per l'utente corrente
 **/

string ls_test, ls_utlima_ricerca, ls_values[], ls_colonne, ls_coltype
int li_column_count, li_i
decimal ld_value
datetime ldt_value

setnull(ls_utlima_ricerca)

// devo salvare i filtri impostati?
if cbx_salva_ricerca.checked then
	
	// Processo solo le colonne reali non le colonne "operatore"
	li_column_count = integer(tab_1.ricerca.dw_ricerca.Describe("DataWindow.Column.Count"))
	for li_i = 1 to li_column_count
		
		ls_coltype = lower(left(tab_1.ricerca.dw_ricerca.Describe("#" + string(li_i) + ".ColType"), 4))
	
		choose case ls_coltype
			case "char"
				ls_values[li_i] = tab_1.ricerca.dw_ricerca.getitemstring(1, li_i)
				
			case "date"
				ldt_value = tab_1.ricerca.dw_ricerca.getitemdatetime(1, li_i)
				ls_values[li_i] = string(ldt_value, "YYYY/MM/DD")
				
			case "deci", "long", "numb"
				ld_value = tab_1.ricerca.dw_ricerca.getitemnumber(1, li_i)
				ls_values[li_i] = string(ld_value)
				
		end choose
					
	next
	
	ls_utlima_ricerca = guo_functions.uof_implode(ls_values, "~t")
	
end if


// controllo se esiste il record nella tabella
select nome_tabella
into :ls_test
from tab_ricerche_utente
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_utente = :s_cs_xx.cod_utente and
	nome_tabella = :istr_data.table_name
using it_sqlcb;
	
if it_sqlcb.sqlcode <> 0 then
	
	// se devo fare un inserimento devo salvare le colonne
	wf_get_user_column(ref ls_colonne)
	
	insert into tab_ricerche_utente (
		cod_azienda,
		cod_utente,
		nome_tabella,
		colonne,
		window_width,
		window_height,
		ultima_ricerca
	) values (
		:s_cs_xx.cod_azienda,
		:s_cs_xx.cod_utente,
		:istr_data.table_name,
		:ls_colonne,
		:width,
		:height,
		:ls_utlima_ricerca)
	using it_sqlcb;
			
else
	
	update tab_ricerche_utente
	set 
		window_width = :width,
		window_height = :height,
		ultima_ricerca = :ls_utlima_ricerca
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		cod_utente = :s_cs_xx.cod_utente and
		nome_tabella = :istr_data.table_name
	using it_sqlcb;
		
end if

if it_sqlcb.sqlcode <> 0 then
	rollback using it_sqlcb;
else
	commit using it_sqlcb;
end if
end subroutine

private subroutine wf_build_where_query ();/**
 * stefanop
 * 21/10/2011
 *
 * Controllo se nella query di selezione devo aggiungere in maniera automatica il codice azienda
 * altrimenti lascio la variabile vuota.
 * La query di ricerca viene costruita poi nella funzione wf_buil_query()
 **/
 
 
string ls_table_non_cod_azienda[], ls_sql, ls_operatore, ls_valore_select, ls_colonna_db, ls_filtri[]
int li_count, li_i
datastore lds_store

ls_table_non_cod_azienda = { &
	"utenti", &
	"tab_abicab", &
	"parametri", &
	"aziende"}
	
	
if guo_functions.uof_in_array(istr_data.table_name, ls_table_non_cod_azienda) then
	
	is_where_query = ""
	
else
	
	is_where_query =  istr_data.table_name + ".cod_azienda='" + s_cs_xx.cod_azienda + "' "
end if

// stefanop 25/06/2012: aggiunta la possibilità di passare una where fissa
// --------------------------------------------
if not isnull(istr_data.forced_where) and istr_data.forced_where <> "" then
	
	if is_where_query <> "" then is_where_query += " AND "
	
	is_where_query += istr_data.forced_where + " "
	
end if
// --------------------------------------------

// Filtri automatici solo se riesci e se non si è CS_SYSTEM
if istr_data.disable_filters or s_cs_xx.cod_utente = "CS_SYSTEM" then return

ls_sql = "SELECT colonna_db, valore_select, flag_tipo_valore, operatore FROM tab_dw_filtri WHERE cod_azienda='" + s_cs_xx.cod_azienda +"' "
ls_sql += "AND (cod_utente='" + s_cs_xx.cod_utente + "' or cod_utente is null) AND UPPER(nome_dw) ='" + upper(istr_data.table_name) + "'"

li_count = guo_functions.uof_crea_datastore(lds_store, ls_sql)

for li_i = 1 to li_count
	
	ls_colonna_db = lds_store.getitemstring(li_i, "colonna_db")
	ls_valore_select = lds_store.getitemstring(li_i, "valore_select")
	ls_operatore = lds_store.getitemstring(li_i, "operatore")
	
	if isnull(ls_operatore) or len(ls_operatore) < 1 then ls_operatore = "="
	
	choose case lds_store.getitemstring(li_i, "flag_tipo_valore")
			
		case "S"
			ls_valore_select = "'" + ls_valore_select + "'"
			
		case "D"
			ls_valore_select = "'" + string(date(ls_valore_select), s_cs_xx.db_funzioni.formato_data) + "'"
			
			
	end choose
	
	ls_filtri[li_i] =  ls_colonna_db + ls_operatore + ls_valore_select
next

if upperbound(ls_filtri) > 0 then
	
	if len(is_where_query) > 0 then is_where_query += " AND "
	
	is_where_query += guo_functions.uof_implode(ls_filtri, " AND ")
	
end if
end subroutine

private subroutine wf_set_window_title (long al_result);/**
 * stefanop
 * 03/11/2011
 *
 * Imposto il nome della finestra e se ci sono risultati mostro anche il numero
 **/
 
 
title = istr_data.window_title

if al_result = 1 then
	title += " - " + string(al_result) + " riga"
elseif al_result > 1 then
	title += " - " + string(al_result) + " righe"
end if
end subroutine

public subroutine wf_read_user_column_input ();/**
 * stefanop
 * 10/11/2011
 *
 * Leggo dalla DW di partenza le colonne da impostare per cercare se l'utente ha
 * già iniziato ad inserire qualche dato per la ricerca, in quel caso riporto il testo
 * nella ricerca
 **/
 
string ls_value, ls_coltype, ls_values[]
datetime ldt_value
decimal ld_value
int li_i, li_max_values_index
long ll_row, ll_last_results_count
boolean lb_dw_set
any la_last_results[]

ll_last_results_count = 0

if isnull(istr_data.dw) then return

// recupero i valori dalla sessione precedente salvata in DB
if not isnull(is_ultima_ricerca) and is_ultima_ricerca <> "" then
	
	guo_functions.uof_explode(is_ultima_ricerca, "~t", ref ls_values)
	
	li_max_values_index = upperbound(ls_values) / 2
	
	for li_i = 1 to upperbound(ls_values)
		
		// Processo solo le colonne reali non le colonne "operatore"	
		ls_coltype = lower(left(tab_1.ricerca.dw_ricerca.Describe("#" + string(li_i) + ".ColType"), 4))
	
		if isnull(ls_values[li_i]) or ls_values[li_i] = "" then continue
		
		if li_i <= li_max_values_index then ib_autoretrieve = true
		
		choose case ls_coltype
			case "char"
				tab_1.ricerca.dw_ricerca.setitem(1, li_i, ls_values[li_i])
				
			case "date"
				ldt_value = datetime(date(ls_values[li_i]), 00:00:00)
				tab_1.ricerca.dw_ricerca.setitem(1, li_i, ldt_value)
				
			case "deci", "long", "numb"
				ld_value = dec(ls_values[li_i])
				tab_1.ricerca.dw_ricerca.setitem(1, li_i, ld_value)
				
		end choose
		
		
	next
	
	return
	
end if
// ---

return 

// recupero i valori di ritorno della sessione precedente se è la stessa tabella
if guo_ricerca.uof_get_last_table_name() = istr_data.table_name then
	guo_ricerca.uof_get_last_results(la_last_results)
	ll_last_results_count = upperbound(la_last_results)
end if
// ----

try

	istr_data.dw.accepttext()
	ll_row = istr_data.dw.getrow()
	
	for li_i = 1 to upperbound(istr_data.dw_search_columns)
		
		lb_dw_set = false
		ls_coltype = lower(left(tab_1.ricerca.dw_ricerca.Describe(istr_data.column_read[li_i] + ".ColType"), 4))
		
		choose case ls_coltype
			case "char"
				ls_value = istr_data.dw.getitemstring(ll_row, istr_data.dw_search_columns[li_i])
				if not isnull(ls_value) and len(ls_value) > 0 then
					tab_1.ricerca.dw_ricerca.setitem(1, istr_data.column_read[li_i], ls_value)
					ib_autoretrieve = true
					lb_dw_set = true
				end if
				
			case "date"
				ldt_value = istr_data.dw.getitemdatetime(ll_row, istr_data.dw_search_columns[li_i])
				if not isnull(ldt_value) then
					tab_1.ricerca.dw_ricerca.setitem(1, istr_data.column_read[li_i], ldt_value)
					ib_autoretrieve = true
					lb_dw_set = true
				end if
				
			case "deci", "long", "numb"
				ld_value = istr_data.dw.getitemnumber(ll_row, istr_data.dw_search_columns[li_i])
				
				if not isnull(ld_value) then
					tab_1.ricerca.dw_ricerca.setitem(1, istr_data.column_read[li_i], ld_value)
					ib_autoretrieve = true
					lb_dw_set = true
				end if
				
		end choose
		
		// è stato caricato un campo dalla DW? se no lo carico dalla sessione precedente
		if lb_dw_set = false and ll_last_results_count > 0 and ll_last_results_count <= li_i then
			tab_1.ricerca.dw_ricerca.setitem(1, istr_data.column_read[li_i], la_last_results[li_i])
			ib_autoretrieve = true
		end if
		// ---
		
	next
catch(RuntimeError ex)
end try
end subroutine

public subroutine wf_set_dw_selezione_focus ();/**
 * Imposta il fuoco sulla datawindow di ricerca e ne seleziona la prima colonna
 **/
 
tab_1.ricerca.dw_ricerca.setfocus()
tab_1.ricerca.dw_ricerca.setrow(1)
end subroutine

private function boolean wf_get_user_column (ref string as_columns);/**
 * stefanop
 * 10/02/2012
 *
 * La funzione ritorna la stringa delle colonne selezionate dall'utente
 **/

string ls_column_table, ls_column, ls_join_tables[], ls_empty[]
long ll_i

setnull(as_columns)
is_select_columns = ls_empty

for ll_i = 1 to tab_1.personalizza.dw_colonne.rowcount()
	
	if tab_1.personalizza.dw_colonne.getitemstring(ll_i, "column_visible") = "S" then
		
		ls_column_table = tab_1.personalizza.dw_colonne.getitemstring(ll_i, "column_table")
		ls_column = ls_column_table + "." + tab_1.personalizza.dw_colonne.getitemstring(ll_i, "column_name")
		is_select_columns[upperbound(is_select_columns) + 1] =  ls_column
		
		// colonna di join?
		if tab_1.personalizza.dw_colonne.getitemstring(ll_i, "column_join") = "S" then
					
			// controllo che non sia già stata inserita la tablenna nella query di join
			if not guo_functions.uof_in_array(ls_column_table, ls_join_tables) then
				is_join_query += " LEFT OUTER JOIN " + ls_column_table + " ON " + tab_1.personalizza.dw_colonne.getitemstring(ll_i, "join_query") + "~r~n"
				ls_join_tables[upperbound(ls_join_tables)+1] = ls_column_table
			end if
			
		end if
	end if
	
next

as_columns = wf_join(is_select_columns, "~t")
return true
end function

public subroutine wf_read_user_check ();/**
 * stefanop
 * 10/02/2012
 *
 * Controllo il flag nella parametri utente per verificare lo stato del check "Salva Ultima ricerca"
 **/
 
string ls_flag

ls_flag = "S"

if s_cs_xx.cod_utente <> "CS_SYSTEM" then

	select flag
	into :ls_flag
	from parametri_azienda_utente
	where 
		cod_azienda = :s_cs_xx.cod_azienda and
		cod_utente = :s_cs_xx.cod_utente and
		cod_parametro = 'SUR' and
		flag_parametro = 'F';
		
	if sqlca.sqlcode <> 0 or isnull(ls_flag) then ls_flag = "S"
	
end if

cbx_salva_ricerca.checked = (ls_flag = "S")
end subroutine

public subroutine wf_save_user_check ();/**
 * stefanop
 * 10/02/2012
 *
 * Controllo il flag nella parametri utente per verificare lo stato del check "Salva Ultima ricerca"
 **/
 
string ls_flag, ls_test

if s_cs_xx.cod_utente = "CS_SYSTEM" then return

ls_flag = "N" 
if cbx_salva_ricerca.checked then ls_flag = "S"

// testo l'esistenza del parametro
select flag
into :ls_test
from parametri_azienda_utente
where 
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_utente = :s_cs_xx.cod_utente and
	cod_parametro = 'SUR' and
	flag_parametro = 'F'
using it_sqlcb;
	
if it_sqlcb.sqlcode < 0 then
	return
	
elseif it_sqlcb.sqlcode = 100 then
	
	insert into parametri_azienda_utente (
		cod_azienda, 
		cod_utente,
		cod_parametro,
		flag_parametro,
		flag
	) values (
		:s_cs_xx.cod_azienda,
		:s_cs_xx.cod_utente,
		'SUR',
		'F',
		:ls_flag)
	using it_sqlcb;
		
elseif it_sqlcb.sqlcode = 0 then
	
	update parametri_azienda_utente
	set flag = :ls_flag
	where 
		cod_azienda = :s_cs_xx.cod_azienda and
		cod_utente = :s_cs_xx.cod_utente and
		cod_parametro = 'SUR' and
		flag_parametro = 'F'
	using it_sqlcb;
		
end if

if it_sqlcb.sqlcode <> 0 then
	rollback using it_sqlcb;
else
	commit using it_sqlcb;
end if
end subroutine

on w_ricerca.create
this.cbx_salva_ricerca=create cbx_salva_ricerca
this.tab_1=create tab_1
this.Control[]={this.cbx_salva_ricerca,&
this.tab_1}
end on

on w_ricerca.destroy
destroy(this.cbx_salva_ricerca)
destroy(this.tab_1)
end on

event pc_setwindow;call super::pc_setwindow;istr_data = message.powerobjectparm

setnull(message.powerobjectparm)

// imposto posizioni elmenti
tab_1.move(20,20)
tab_1.ricerca.dw_ricerca.move(0, 0)
tab_1.personalizza.dw_colonne.move(0,0)
tab_1.ricerca.dw_ricerca.height = SEARCH_HEADER_HEIGHT + SEARCH_DETAIL_HEIGHT
tab_1.ricerca.dw_selezione.move(0, tab_1.ricerca.dw_ricerca.height)

tab_1.personalizza.dw_colonne.dragicon = s_cs_xx.volume + s_cs_xx.risorse + "treeview\dragdrop.ico"

// imposto titolo della finestra
wf_set_window_title(-1)

// leggo le colonne della tabella
wf_read_table_column()

// leggo le colonne selezionate dall'utente
wf_read_user_column()

// la tabella richiede cod_azienda?
wf_build_where_query()

// creo la datawindow di selezione
wf_build_dw_ricerca()

// forzo il resize nel caso le dimensioni siano state cambiate nella funzione wf_read_user_column
tab_1.postevent("ue_resize")



end event

event resize;/* TOLTO ANCESTOR SCRIPT */

tab_1.resize(newwidth - 40, newheight - 40)
cbx_salva_ricerca.move(newwidth - 40 - cbx_salva_ricerca.width, 30)
end event

event pc_close;/** TOLTO STO CAZZO DI ANCESTOR **/
end event

event open;string ls_error

istr_data = message.powerobjectparm

setnull(message.powerobjectparm)

// imposto posizioni elmenti
tab_1.move(20,20)
tab_1.ricerca.dw_ricerca.move(0, 0)
tab_1.personalizza.dw_colonne.move(0,0)
tab_1.ricerca.dw_ricerca.height = SEARCH_HEADER_HEIGHT + SEARCH_DETAIL_HEIGHT
tab_1.ricerca.dw_selezione.move(0, tab_1.ricerca.dw_ricerca.height)

tab_1.personalizza.dw_colonne.dragicon = s_cs_xx.volume + s_cs_xx.risorse + "treeview\dragdrop.ico"

// inizializzo la nuova transazione
if not guo_functions.uof_create_transaction_from(sqlca, it_sqlcb, ls_error) then
	g_mb.error(ls_error)
	close(this)
end if

// devo fare il setitem in una datawindow?
ib_dw = not isnull(istr_data.dw)

// imposto titolo della finestra
wf_set_window_title(-1)

// leggo le colonne della tabella
wf_read_table_column()

// leggo le colonne selezionate dall'utente
wf_read_user_column()

// la tabella richiede cod_azienda?
wf_build_where_query()

// creo la datawindow di selezione
wf_build_dw_ricerca()

// salvo la ricerca?
wf_read_user_check()

// forzo il resize nel caso le dimensioni siano state cambiate nella funzione wf_read_user_column
tab_1.postevent("ue_resize")



end event

event closequery;disconnect using it_sqlcb;

// se chiudo la finestra di ricerca e l'avevo aperta in modalità response, mi aspetto in ritorno una struttura con i data
// che in questo caso son nulli
if not ib_dw and ib_user_cancel then
	str_ricerca lstr_empty
	message.powerobjectparm = lstr_empty
end if
end event

type cbx_salva_ricerca from checkbox within w_ricerca
integer x = 2702
integer y = 24
integer width = 434
integer height = 80
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Salva Ricerca"
boolean checked = true
boolean lefttext = true
end type

event clicked;wf_save_user_check()
end event

type tab_1 from tab within w_ricerca
event ue_resize pbm_size
integer x = 14
integer y = 16
integer width = 3122
integer height = 1720
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long backcolor = 12632256
boolean raggedright = true
boolean focusonbuttondown = true
integer selectedtab = 1
ricerca ricerca
personalizza personalizza
end type

event ue_resize;/* resize */

this.ricerca.dw_ricerca.width = this.ricerca.width

this.ricerca.dw_selezione.resize(this.ricerca.dw_ricerca.width, this.ricerca.height - this.ricerca.dw_ricerca.height)

this.personalizza.dw_colonne.resize(this.personalizza.width, this.personalizza.height)
end event

on tab_1.create
this.ricerca=create ricerca
this.personalizza=create personalizza
this.Control[]={this.ricerca,&
this.personalizza}
end on

on tab_1.destroy
destroy(this.ricerca)
destroy(this.personalizza)
end on

event selectionchanging;/**
 * stefanop
 * 06/09/2011
 **/
 
long ll_selected_columns
 
// sono in personalizza, devo controllare che ci sia almeno una colonna selezionata
if oldindex = 2 then
	
	ll_selected_columns = this.personalizza.dw_colonne.find("column_visible='S'", 0, this.personalizza.dw_colonne.rowcount())
	
	if ll_selected_columns < 1 then
		g_mb.show("Selezionare almeno una colonna da visualizzare")
		return -1
	end if
	
	if wf_dw_status_modified() then
		setnull(is_ultima_ricerca)
		wf_save_user_column()
		wf_build_dw_ricerca()
	end if
	
end if
end event

type ricerca from userobject within tab_1
integer x = 18
integer y = 116
integer width = 3086
integer height = 1588
long backcolor = 12632256
string text = "Ricerca"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_ricerca dw_ricerca
dw_selezione dw_selezione
end type

on ricerca.create
this.dw_ricerca=create dw_ricerca
this.dw_selezione=create dw_selezione
this.Control[]={this.dw_ricerca,&
this.dw_selezione}
end on

on ricerca.destroy
destroy(this.dw_ricerca)
destroy(this.dw_selezione)
end on

type dw_ricerca from datawindow within ricerca
event ue_key pbm_dwnkey
event ue_lbutton_up pbm_dwnlbuttonup
event ue_check_modified ( )
integer x = 9
integer y = 12
integer width = 3063
integer height = 276
integer taborder = 40
boolean bringtotop = true
string title = "none"
boolean border = false
end type

event ue_key;if key = KeyEnter! then
	
	if dw_selezione.rowcount() = 0 then
		wf_build_dw_selezione()
	end if
	
	dw_selezione.setfocus()
	
end if
end event

event ue_lbutton_up;/**
 * controllo se è stata spostata una colonna o ridimensionata
 **/
 
 this.event post ue_check_modified()
end event

event ue_check_modified();/**
 * controllo
 **/
 
if this.Object.DataWindow.Syntax.Modified = "yes" then
	
	this.Object.DataWindow.Syntax.Modified = "no"
	wf_build_dw_selezione()
	
end if
end event

event itemchanged;wf_build_dw_selezione()
end event

event doubleclicked;string ls_column

// Sorto la tabella
if row < 1 and dwo.type="text" then
	
	ls_column = string(dwo.name)
	ls_column = mid(ls_column, 1, len(ls_column) - 2)
	
	if ib_sorting_asc then
		tab_1.ricerca.dw_selezione.setsort(ls_column + " D")
	else
		tab_1.ricerca.dw_selezione.setsort(ls_column + " A")
	end if
	
	ib_sorting_asc = not ib_sorting_asc
	
	tab_1.ricerca.dw_selezione.sort()
	
end if
	
end event

type dw_selezione from datawindow within ricerca
event ue_key pbm_dwnkey
event ue_vertical_scroll pbm_dwnhscroll
integer x = 18
integer y = 304
integer width = 3063
integer height = 1284
integer taborder = 20
string title = "none"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
borderstyle borderstyle = styleraised!
end type

event ue_key;if key = KeyEnter! then
	
	if getrow() > 0 then
		wf_return_selected_value(getrow())
	end if
	
end if
end event

event ue_vertical_scroll;/**
 * stefanop
 * 18/10/2011
 *
 * Se scrollo la parte sotto devo scrollare anche quella sopra
 **/

string ls_position

ls_position = ""
if pane = 2 then ls_position = "-"

ls_position += string(scrollpos)

dw_ricerca.modify("DataWindow.HorizontalScrollPosition=" + ls_position)
end event

event doubleclicked;string ls_coltype, ls_value
int li_i
long ll_value
decimal ld_value
datetime ldt_value



if row > 0 then
	
	wf_return_selected_value(row)
	
end if
end event

event rowfocuschanged;selectrow(0, false)
selectrow(currentrow, true)
end event

type personalizza from userobject within tab_1
integer x = 18
integer y = 116
integer width = 3086
integer height = 1588
long backcolor = 12632256
string text = "Personalizza"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_colonne dw_colonne
end type

on personalizza.create
this.dw_colonne=create dw_colonne
this.Control[]={this.dw_colonne}
end on

on personalizza.destroy
destroy(this.dw_colonne)
end on

type dw_colonne from datawindow within personalizza
event ue_button_up pbm_dwnlbuttonup
integer x = 18
integer y = 28
integer width = 3035
integer height = 1548
integer taborder = 30
string title = "none"
string dataobject = "d_ricerca_colonne"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event ue_button_up;int li_i

if ii_dragged_row > 0 then
	ScrollToRow(ii_dragged_row)
end if

for li_i = 1 to RowCount() 
	SetItem(li_i, "column_dragging", "N")
next

Drag(End!)
end event

event itemchanged;if row > 0 and string(dwo.name) = "column_visible" then
	
	// se la colonna è fixata non posso deselezionare
	if wf_fixed_column(getitemstring(row, "column_name")) then return 2
	
end if
end event

event clicked;//if row < 1 or getitemstring(row, "column_pk") = "S"then
if row < 1 then
	ii_dragged_row = -1
else
	ii_dragged_row = row
	SelectRow(0, FALSE)
	SelectRow(Row, TRUE)
	Drag(Begin!)	
end if
end event

event dragdrop;if getitemstring(row, "column_pk") = "S" then
	
	// se è una colonna PK allora devo spostarla nella prima posizione non PK,
	// in quanto le prime posizioni sono riservate.
	row = upperbound(istr_data.column_read) + 1
	
end if

if ii_dragged_row > row then
	RowsMove(ii_dragged_row, ii_dragged_row, Primary!, this, row, Primary!)
elseif ii_dragged_row < row then
	RowsMove(ii_dragged_row, ii_dragged_row, Primary!, this, row + 1, Primary!)
end if
end event

event dragwithin;int li_i

for li_i = 1 to RowCount()
	SetItem(li_i, "column_dragging", "N")
next

SetItem(Row, "column_dragging", "S")
end event

