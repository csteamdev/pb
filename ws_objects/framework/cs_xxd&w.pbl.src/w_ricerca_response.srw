﻿$PBExportHeader$w_ricerca_response.srw
forward
global type w_ricerca_response from w_ricerca
end type
end forward

global type w_ricerca_response from w_ricerca
integer width = 3168
integer height = 1840
boolean resizable = false
windowtype windowtype = response!
end type
global w_ricerca_response w_ricerca_response

on w_ricerca_response.create
call super::create
end on

on w_ricerca_response.destroy
call super::destroy
end on

type tab_1 from w_ricerca`tab_1 within w_ricerca_response
end type

type ricerca from w_ricerca`ricerca within tab_1
end type

type dw_ricerca from w_ricerca`dw_ricerca within ricerca
end type

type dw_selezione from w_ricerca`dw_selezione within ricerca
end type

type personalizza from w_ricerca`personalizza within tab_1
end type

type dw_colonne from w_ricerca`dw_colonne within personalizza
end type

