﻿$PBExportHeader$w_scegli_colore.srw
forward
global type w_scegli_colore from window
end type
type st_20 from statictext within w_scegli_colore
end type
type st_19 from statictext within w_scegli_colore
end type
type st_18 from statictext within w_scegli_colore
end type
type st_17 from statictext within w_scegli_colore
end type
type st_16 from statictext within w_scegli_colore
end type
type st_15 from statictext within w_scegli_colore
end type
type st_14 from statictext within w_scegli_colore
end type
type st_13 from statictext within w_scegli_colore
end type
type st_12 from statictext within w_scegli_colore
end type
type st_11 from statictext within w_scegli_colore
end type
type st_10 from statictext within w_scegli_colore
end type
type st_9 from statictext within w_scegli_colore
end type
type st_8 from statictext within w_scegli_colore
end type
type st_7 from statictext within w_scegli_colore
end type
type st_6 from statictext within w_scegli_colore
end type
type st_5 from statictext within w_scegli_colore
end type
type st_4 from statictext within w_scegli_colore
end type
type st_3 from statictext within w_scegli_colore
end type
type st_2 from statictext within w_scegli_colore
end type
type st_1 from statictext within w_scegli_colore
end type
end forward

global type w_scegli_colore from window
integer width = 585
integer height = 708
boolean titlebar = true
string title = "Selezione Colore"
windowtype windowtype = response!
long backcolor = 67108864
st_20 st_20
st_19 st_19
st_18 st_18
st_17 st_17
st_16 st_16
st_15 st_15
st_14 st_14
st_13 st_13
st_12 st_12
st_11 st_11
st_10 st_10
st_9 st_9
st_8 st_8
st_7 st_7
st_6 st_6
st_5 st_5
st_4 st_4
st_3 st_3
st_2 st_2
st_1 st_1
end type
global w_scegli_colore w_scegli_colore

type variables
boolean ib_chiudi = false
end variables

on w_scegli_colore.create
this.st_20=create st_20
this.st_19=create st_19
this.st_18=create st_18
this.st_17=create st_17
this.st_16=create st_16
this.st_15=create st_15
this.st_14=create st_14
this.st_13=create st_13
this.st_12=create st_12
this.st_11=create st_11
this.st_10=create st_10
this.st_9=create st_9
this.st_8=create st_8
this.st_7=create st_7
this.st_6=create st_6
this.st_5=create st_5
this.st_4=create st_4
this.st_3=create st_3
this.st_2=create st_2
this.st_1=create st_1
this.Control[]={this.st_20,&
this.st_19,&
this.st_18,&
this.st_17,&
this.st_16,&
this.st_15,&
this.st_14,&
this.st_13,&
this.st_12,&
this.st_11,&
this.st_10,&
this.st_9,&
this.st_8,&
this.st_7,&
this.st_6,&
this.st_5,&
this.st_4,&
this.st_3,&
this.st_2,&
this.st_1}
end on

on w_scegli_colore.destroy
destroy(this.st_20)
destroy(this.st_19)
destroy(this.st_18)
destroy(this.st_17)
destroy(this.st_16)
destroy(this.st_15)
destroy(this.st_14)
destroy(this.st_13)
destroy(this.st_12)
destroy(this.st_11)
destroy(this.st_10)
destroy(this.st_9)
destroy(this.st_8)
destroy(this.st_7)
destroy(this.st_6)
destroy(this.st_5)
destroy(this.st_4)
destroy(this.st_3)
destroy(this.st_2)
destroy(this.st_1)
end on

event closequery;if not ib_chiudi then
	return 1
end if
end event

event open;if not isnull(s_cs_xx.parametri.parametro_d_2) and &
	not isnull(s_cs_xx.parametri.parametro_d_3) then
	move(s_cs_xx.parametri.parametro_d_2,s_cs_xx.parametri.parametro_d_3)
end if
	
end event

type st_20 from statictext within w_scegli_colore
integer x = 434
integer y = 500
integer width = 114
integer height = 100
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string pointer = "HyperLink!"
long textcolor = 33554432
long backcolor = 10789024
boolean border = true
boolean focusrectangle = false
end type

event clicked;s_cs_xx.parametri.parametro_d_1 = backcolor
ib_chiudi = true
close(parent)
end event

type st_19 from statictext within w_scegli_colore
integer x = 297
integer y = 500
integer width = 114
integer height = 100
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string pointer = "HyperLink!"
long textcolor = 33554432
long backcolor = 15793151
boolean border = true
boolean focusrectangle = false
end type

event clicked;s_cs_xx.parametri.parametro_d_1 = backcolor
ib_chiudi = true
close(parent)
end event

type st_18 from statictext within w_scegli_colore
integer x = 160
integer y = 500
integer width = 114
integer height = 100
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string pointer = "HyperLink!"
long textcolor = 33554432
long backcolor = 15780518
boolean border = true
boolean focusrectangle = false
end type

event clicked;s_cs_xx.parametri.parametro_d_1 = backcolor
ib_chiudi = true
close(parent)
end event

type st_17 from statictext within w_scegli_colore
integer x = 23
integer y = 500
integer width = 114
integer height = 100
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string pointer = "HyperLink!"
long textcolor = 33554432
long backcolor = 12639424
boolean border = true
boolean focusrectangle = false
end type

event clicked;s_cs_xx.parametri.parametro_d_1 = backcolor
ib_chiudi = true
close(parent)
end event

type st_16 from statictext within w_scegli_colore
integer x = 434
integer y = 380
integer width = 114
integer height = 100
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string pointer = "HyperLink!"
long textcolor = 33554432
long backcolor = 16711935
boolean border = true
boolean focusrectangle = false
end type

event clicked;s_cs_xx.parametri.parametro_d_1 = backcolor
ib_chiudi = true
close(parent)
end event

type st_15 from statictext within w_scegli_colore
integer x = 297
integer y = 380
integer width = 114
integer height = 100
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string pointer = "HyperLink!"
long textcolor = 33554432
long backcolor = 8388736
boolean border = true
boolean focusrectangle = false
end type

event clicked;s_cs_xx.parametri.parametro_d_1 = backcolor
ib_chiudi = true
close(parent)
end event

type st_14 from statictext within w_scegli_colore
integer x = 160
integer y = 380
integer width = 114
integer height = 100
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string pointer = "HyperLink!"
long textcolor = 33554432
long backcolor = 16711680
boolean border = true
boolean focusrectangle = false
end type

event clicked;s_cs_xx.parametri.parametro_d_1 = backcolor
ib_chiudi = true
close(parent)
end event

type st_13 from statictext within w_scegli_colore
integer x = 23
integer y = 380
integer width = 114
integer height = 100
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string pointer = "HyperLink!"
long textcolor = 33554432
long backcolor = 8388608
boolean border = true
boolean focusrectangle = false
end type

event clicked;s_cs_xx.parametri.parametro_d_1 = backcolor
ib_chiudi = true
close(parent)
end event

type st_12 from statictext within w_scegli_colore
integer x = 434
integer y = 260
integer width = 114
integer height = 100
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string pointer = "HyperLink!"
long textcolor = 33554432
long backcolor = 16776960
boolean border = true
boolean focusrectangle = false
end type

event clicked;s_cs_xx.parametri.parametro_d_1 = backcolor
ib_chiudi = true
close(parent)
end event

type st_11 from statictext within w_scegli_colore
integer x = 297
integer y = 260
integer width = 114
integer height = 100
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string pointer = "HyperLink!"
long textcolor = 33554432
long backcolor = 8421376
boolean border = true
boolean focusrectangle = false
end type

event clicked;s_cs_xx.parametri.parametro_d_1 = backcolor
ib_chiudi = true
close(parent)
end event

type st_10 from statictext within w_scegli_colore
integer x = 160
integer y = 260
integer width = 114
integer height = 100
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string pointer = "HyperLink!"
long textcolor = 33554432
long backcolor = 65280
boolean border = true
boolean focusrectangle = false
end type

event clicked;s_cs_xx.parametri.parametro_d_1 = backcolor
ib_chiudi = true
close(parent)
end event

type st_9 from statictext within w_scegli_colore
integer x = 23
integer y = 260
integer width = 114
integer height = 100
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string pointer = "HyperLink!"
long textcolor = 33554432
long backcolor = 32768
boolean border = true
boolean focusrectangle = false
end type

event clicked;s_cs_xx.parametri.parametro_d_1 = backcolor
ib_chiudi = true
close(parent)
end event

type st_8 from statictext within w_scegli_colore
integer x = 434
integer y = 140
integer width = 114
integer height = 100
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string pointer = "HyperLink!"
long textcolor = 33554432
long backcolor = 65535
boolean border = true
boolean focusrectangle = false
end type

event clicked;s_cs_xx.parametri.parametro_d_1 = backcolor
ib_chiudi = true
close(parent)
end event

type st_7 from statictext within w_scegli_colore
integer x = 297
integer y = 140
integer width = 114
integer height = 100
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string pointer = "HyperLink!"
long textcolor = 33554432
long backcolor = 32896
boolean border = true
boolean focusrectangle = false
end type

event clicked;s_cs_xx.parametri.parametro_d_1 = backcolor
ib_chiudi = true
close(parent)
end event

type st_6 from statictext within w_scegli_colore
integer x = 160
integer y = 140
integer width = 114
integer height = 100
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string pointer = "HyperLink!"
long textcolor = 33554432
long backcolor = 255
boolean border = true
boolean focusrectangle = false
end type

event clicked;s_cs_xx.parametri.parametro_d_1 = backcolor
ib_chiudi = true
close(parent)
end event

type st_5 from statictext within w_scegli_colore
integer x = 23
integer y = 140
integer width = 114
integer height = 100
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string pointer = "HyperLink!"
long textcolor = 33554432
long backcolor = 128
boolean border = true
boolean focusrectangle = false
end type

event clicked;s_cs_xx.parametri.parametro_d_1 = backcolor
ib_chiudi = true
close(parent)
end event

type st_4 from statictext within w_scegli_colore
integer x = 434
integer y = 20
integer width = 114
integer height = 100
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string pointer = "HyperLink!"
long textcolor = 33554432
long backcolor = 16777215
boolean border = true
boolean focusrectangle = false
end type

event clicked;s_cs_xx.parametri.parametro_d_1 = backcolor
ib_chiudi = true
close(parent)
end event

type st_3 from statictext within w_scegli_colore
integer x = 297
integer y = 20
integer width = 114
integer height = 100
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string pointer = "HyperLink!"
long textcolor = 33554432
long backcolor = 12632256
boolean border = true
boolean focusrectangle = false
end type

event clicked;s_cs_xx.parametri.parametro_d_1 = backcolor
ib_chiudi = true
close(parent)
end event

type st_2 from statictext within w_scegli_colore
integer x = 160
integer y = 20
integer width = 114
integer height = 100
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string pointer = "HyperLink!"
long textcolor = 33554432
long backcolor = 8421504
boolean border = true
boolean focusrectangle = false
end type

event clicked;s_cs_xx.parametri.parametro_d_1 = backcolor
ib_chiudi = true
close(parent)
end event

type st_1 from statictext within w_scegli_colore
integer x = 23
integer y = 20
integer width = 114
integer height = 100
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string pointer = "HyperLink!"
long textcolor = 33554432
long backcolor = 0
boolean border = true
boolean focusrectangle = false
end type

event clicked;s_cs_xx.parametri.parametro_d_1 = backcolor
ib_chiudi = true
close(parent)
end event

