﻿$PBExportHeader$w_scegli_colori.srw
forward
global type w_scegli_colori from w_cs_xx_principale
end type
type st_cod_b from statictext within w_scegli_colori
end type
type st_cod_g from statictext within w_scegli_colori
end type
type st_cod_r from statictext within w_scegli_colori
end type
type st_anteprima from statictext within w_scegli_colori
end type
type st_b from statictext within w_scegli_colori
end type
type st_g from statictext within w_scegli_colori
end type
type st_r from statictext within w_scegli_colori
end type
type htb_r from htrackbar within w_scegli_colori
end type
type htb_g from htrackbar within w_scegli_colori
end type
type htb_b from htrackbar within w_scegli_colori
end type
type sle_codice from singlelineedit within w_scegli_colori
end type
type st_1 from statictext within w_scegli_colori
end type
type gb_1 from groupbox within w_scegli_colori
end type
end forward

global type w_scegli_colori from w_cs_xx_principale
integer height = 532
string title = "Info Colori"
boolean minbox = false
boolean maxbox = false
boolean resizable = false
windowtype windowtype = response!
long backcolor = 67108864
st_cod_b st_cod_b
st_cod_g st_cod_g
st_cod_r st_cod_r
st_anteprima st_anteprima
st_b st_b
st_g st_g
st_r st_r
htb_r htb_r
htb_g htb_g
htb_b htb_b
sle_codice sle_codice
st_1 st_1
gb_1 gb_1
end type
global w_scegli_colori w_scegli_colori

type variables
long il_color = 0
end variables

forward prototypes
public subroutine wf_color ()
public subroutine wf_reverse ()
end prototypes

public subroutine wf_color ();il_color = htb_r.position + (htb_g.position * 256) + (htb_b.position * 65536)

sle_codice.text = string(il_color)

st_anteprima.backcolor = il_color
end subroutine

public subroutine wf_reverse ();if isnull(il_color) then il_color=16777215

st_anteprima.backcolor = il_color

sle_codice.text = string(il_color)

htb_b.position = truncate((il_color / 65536),0)

il_color -= (htb_b.position * 65536)

htb_g.position = truncate((il_color / 256),0)

il_color -= (htb_g.position * 256)

htb_r.position = il_color
end subroutine

on w_scegli_colori.create
int iCurrent
call super::create
this.st_cod_b=create st_cod_b
this.st_cod_g=create st_cod_g
this.st_cod_r=create st_cod_r
this.st_anteprima=create st_anteprima
this.st_b=create st_b
this.st_g=create st_g
this.st_r=create st_r
this.htb_r=create htb_r
this.htb_g=create htb_g
this.htb_b=create htb_b
this.sle_codice=create sle_codice
this.st_1=create st_1
this.gb_1=create gb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_cod_b
this.Control[iCurrent+2]=this.st_cod_g
this.Control[iCurrent+3]=this.st_cod_r
this.Control[iCurrent+4]=this.st_anteprima
this.Control[iCurrent+5]=this.st_b
this.Control[iCurrent+6]=this.st_g
this.Control[iCurrent+7]=this.st_r
this.Control[iCurrent+8]=this.htb_r
this.Control[iCurrent+9]=this.htb_g
this.Control[iCurrent+10]=this.htb_b
this.Control[iCurrent+11]=this.sle_codice
this.Control[iCurrent+12]=this.st_1
this.Control[iCurrent+13]=this.gb_1
end on

on w_scegli_colori.destroy
call super::destroy
destroy(this.st_cod_b)
destroy(this.st_cod_g)
destroy(this.st_cod_r)
destroy(this.st_anteprima)
destroy(this.st_b)
destroy(this.st_g)
destroy(this.st_r)
destroy(this.htb_r)
destroy(this.htb_g)
destroy(this.htb_b)
destroy(this.sle_codice)
destroy(this.st_1)
destroy(this.gb_1)
end on

event open;call super::open;il_color = s_cs_xx.parametri.parametro_d_1

wf_reverse()
end event

event closequery;call super::closequery;wf_color()

s_cs_xx.parametri.parametro_d_1 = il_color
end event

type st_cod_b from statictext within w_scegli_colori
integer x = 1157
integer y = 308
integer width = 206
integer height = 72
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
alignment alignment = center!
boolean focusrectangle = false
end type

type st_cod_g from statictext within w_scegli_colori
integer x = 1157
integer y = 192
integer width = 206
integer height = 72
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
alignment alignment = center!
boolean focusrectangle = false
end type

type st_cod_r from statictext within w_scegli_colori
integer x = 1157
integer y = 72
integer width = 206
integer height = 72
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
alignment alignment = center!
boolean focusrectangle = false
end type

type st_anteprima from statictext within w_scegli_colori
integer x = 1394
integer y = 8
integer width = 585
integer height = 320
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
boolean focusrectangle = false
end type

event clicked;backcolor = f_scegli_colore()

wf_reverse()
end event

type st_b from statictext within w_scegli_colori
integer x = 46
integer y = 320
integer width = 82
integer height = 64
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
string text = "B"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_g from statictext within w_scegli_colori
integer x = 46
integer y = 200
integer width = 82
integer height = 64
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
string text = "G"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_r from statictext within w_scegli_colori
integer x = 46
integer y = 80
integer width = 82
integer height = 64
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
string text = "R"
alignment alignment = center!
boolean focusrectangle = false
end type

type htb_r from htrackbar within w_scegli_colori
integer x = 91
integer y = 68
integer width = 1088
integer height = 108
integer maxposition = 255
integer position = 255
integer tickfrequency = 64
htickmarks tickmarks = hticksonneither!
end type

event moved;wf_color()

st_cod_r.text = string(position)
end event

type htb_g from htrackbar within w_scegli_colori
integer x = 91
integer y = 180
integer width = 1088
integer height = 108
integer maxposition = 255
integer position = 255
integer tickfrequency = 64
htickmarks tickmarks = hticksonneither!
end type

event moved;wf_color()

st_cod_g.text = string(position)
end event

type htb_b from htrackbar within w_scegli_colori
integer x = 91
integer y = 300
integer width = 1088
integer height = 108
integer maxposition = 255
integer position = 255
integer tickfrequency = 64
htickmarks tickmarks = hticksonneither!
end type

event moved;wf_color()

st_cod_b.text = string(position)
end event

type sle_codice from singlelineedit within w_scegli_colori
integer x = 1710
integer y = 340
integer width = 274
integer height = 80
integer taborder = 10
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
boolean border = false
boolean autohscroll = false
boolean displayonly = true
borderstyle borderstyle = stylelowered!
boolean hideselection = false
end type

type st_1 from statictext within w_scegli_colori
integer x = 1408
integer y = 360
integer width = 306
integer height = 52
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
string text = "Cod. Colore:"
alignment alignment = center!
boolean focusrectangle = false
end type

type gb_1 from groupbox within w_scegli_colori
integer x = 23
integer width = 1362
integer height = 420
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
end type

