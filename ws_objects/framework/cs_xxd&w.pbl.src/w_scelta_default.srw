﻿$PBExportHeader$w_scelta_default.srw
$PBExportComments$Window configurazione default
forward
global type w_scelta_default from w_cs_xx_principale
end type
type cb_2 from commandbutton within w_scelta_default
end type
type ddlb_1 from dropdownlistbox within w_scelta_default
end type
end forward

global type w_scelta_default from w_cs_xx_principale
integer width = 1115
integer height = 352
string title = "Configura Profili"
boolean minbox = false
boolean maxbox = false
boolean resizable = false
windowtype windowtype = response!
cb_2 cb_2
ddlb_1 ddlb_1
end type
global w_scelta_default w_scelta_default

type variables
integer il_index = 0
end variables

on w_scelta_default.create
int iCurrent
call super::create
this.cb_2=create cb_2
this.ddlb_1=create ddlb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_2
this.Control[iCurrent+2]=this.ddlb_1
end on

on w_scelta_default.destroy
call super::destroy
destroy(this.cb_2)
destroy(this.ddlb_1)
end on

event open;call super::open;integer li_risposta,li_default, li_i
string ls_default,ls_keys[], ls_profilo

ddlb_1.additem("ODBC")
ddlb_1.additem("Sybase ASE")
ddlb_1.additem("ORACLE REL 7")
ddlb_1.additem("ORACLE REL 8")
ddlb_1.additem("ORACLE REL 9")
ddlb_1.additem("Microsoft Sql Server")

s_cs_xx.parametri.parametro_i_1 = -1

end event

type cb_2 from commandbutton within w_scelta_default
integer x = 347
integer y = 136
integer width = 366
integer height = 80
integer taborder = 100
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Applica"
end type

event clicked;if il_index < 1 then
	g_mb.messagebox("Framework","Effettuare prima la scelta del tipo di database per il profilo.")
	return
end if

s_cs_xx.parametri.parametro_i_1 = il_index

close(parent)

end event

type ddlb_1 from dropdownlistbox within w_scelta_default
integer x = 23
integer y = 16
integer width = 1047
integer height = 464
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
boolean sorted = false
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event selectionchanged;il_index = index
end event

