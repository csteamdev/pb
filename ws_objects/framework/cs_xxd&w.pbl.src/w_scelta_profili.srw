﻿$PBExportHeader$w_scelta_profili.srw
$PBExportComments$Window configurazione profili DB
forward
global type w_scelta_profili from window
end type
type cbx_dbdefault from checkbox within w_scelta_profili
end type
type ddlb_1 from dropdownlistbox within w_scelta_profili
end type
type cb_2 from commandbutton within w_scelta_profili
end type
end forward

global type w_scelta_profili from window
integer width = 1111
integer height = 424
boolean titlebar = true
string title = "Selezione Profilo"
boolean controlmenu = true
windowtype windowtype = response!
long backcolor = 12632256
string icon = "AppIcon!"
boolean center = true
cbx_dbdefault cbx_dbdefault
ddlb_1 ddlb_1
cb_2 cb_2
end type
global w_scelta_profili w_scelta_profili

type variables
boolean ib_default
end variables

event open;integer	li_risposta, li_i

string		ls_keys[], ls_profilo, ls_id


if s_cs_xx.parametri.parametro_b_1 then
	cbx_dbdefault.enabled = false
	cbx_dbdefault.checked = true
	ib_default = true
else
	ib_default = false
end if

setnull(s_cs_xx.parametri.parametro_b_1)

RegistryKeys(s_cs_xx.chiave_root,ls_keys)

for li_i = 1 to upperbound(ls_keys)
	if pos(lower(ls_keys[li_i]), "applicazione_") > 0 then
		ls_profilo = mid(ls_keys[li_i], pos(lower(ls_keys[li_i]), "applicazione_") + 13)
		
		// *** Michela 21/07/2006 visualizzo l'id se esiste come valore
		setnull(ls_id)
		li_risposta = Registryget(s_cs_xx.chiave_root + ls_keys[li_i], "id", ls_id)
		if isnull(ls_id) then
			ddlb_1.additem(ls_profilo)
		else
			ddlb_1.additem(ls_profilo + " - " + ls_id)
		end if
		// ***				
		
	end if
next

if not isnull(s_cs_xx.profilodefault) then
	ddlb_1.selectitem(s_cs_xx.profilodefault, 0)
else
	ddlb_1.selectitem(1)
end if

s_cs_xx.parametri.parametro_i_1 = -1

end event

on w_scelta_profili.create
this.cbx_dbdefault=create cbx_dbdefault
this.ddlb_1=create ddlb_1
this.cb_2=create cb_2
this.Control[]={this.cbx_dbdefault,&
this.ddlb_1,&
this.cb_2}
end on

on w_scelta_profili.destroy
destroy(this.cbx_dbdefault)
destroy(this.ddlb_1)
destroy(this.cb_2)
end on

type cbx_dbdefault from checkbox within w_scelta_profili
integer x = 32
integer y = 124
integer width = 1024
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Imposta come profilo predefinito"
boolean lefttext = true
end type

event clicked;//
//cbx_dbdefault_2.checked = false
//cbx_dbdefault_3.checked = false
//cbx_dbdefault_4.checked = false
end event

type ddlb_1 from dropdownlistbox within w_scelta_profili
integer x = 23
integer y = 16
integer width = 1047
integer height = 464
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event selectionchanged;if not ib_default then
	cbx_dbdefault.checked = false
end if
end event

type cb_2 from commandbutton within w_scelta_profili
integer x = 343
integer y = 232
integer width = 366
integer height = 80
integer taborder = 10
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Connetti"
end type

event clicked;string ls_valore, ls_key, ls_str,ls_num_chiave
integer li_i, li_row, li_ret
long    ll_posizione

ls_num_chiave = ddlb_1.text


ll_posizione = pos( ls_num_chiave, "-")
if not isnull(ll_posizione) and ll_posizione > 0 then
	
	ls_num_chiave = left( ls_num_chiave, (ll_posizione - 2))
	
end if


if cbx_dbdefault.checked or ib_default then
	choose case s_cs_xx.livello_profilo
		case "LOCAL_USER"
			Registryset(s_cs_xx.chiave_root_user + "profilodefault", "numero", regstring!, ls_num_chiave)
		case "LOCAL_MACHINE"
			Registryset(s_cs_xx.chiave_root + "profilodefault", "numero", regstring!, ls_num_chiave)
		case else
			Registryset(s_cs_xx.chiave_root + "profilodefault", "numero", regstring!, ls_num_chiave)
	end choose
end if

s_cs_xx.parametri.parametro_i_1 = integer(ls_num_chiave)

close(parent)

end event

