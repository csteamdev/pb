﻿$PBExportHeader$w_seleziona_webfax.srw
$PBExportComments$Finestra Gestione  ABI
forward
global type w_seleziona_webfax from window
end type
type cb_annulla from commandbutton within w_seleziona_webfax
end type
type cb_seleziona from commandbutton within w_seleziona_webfax
end type
type dw_webfax from datawindow within w_seleziona_webfax
end type
end forward

global type w_seleziona_webfax from window
integer width = 3835
integer height = 1360
boolean titlebar = true
string title = "Seleziona web fax"
windowtype windowtype = response!
long backcolor = 12632256
string icon = "AppIcon!"
boolean center = true
cb_annulla cb_annulla
cb_seleziona cb_seleziona
dw_webfax dw_webfax
end type
global w_seleziona_webfax w_seleziona_webfax

event open;string ls_count, ls_col_index, ls_column_name
integer li_index

//numero di colonne
ls_count = dw_webfax.Describe("DataWindow.Column.Count")

for li_index = 1 to integer(ls_count)
	ls_col_index = "#"+string(li_index)
	ls_column_name = dw_webfax.Describe(ls_col_index+".name")
	
	dw_webfax.modify(ls_column_name+'.protect=1')
next

dw_webfax.settransobject(sqlca)
dw_webfax.retrieve(s_cs_xx.cod_azienda)

end event

on w_seleziona_webfax.create
this.cb_annulla=create cb_annulla
this.cb_seleziona=create cb_seleziona
this.dw_webfax=create dw_webfax
this.Control[]={this.cb_annulla,&
this.cb_seleziona,&
this.dw_webfax}
end on

on w_seleziona_webfax.destroy
destroy(this.cb_annulla)
destroy(this.cb_seleziona)
destroy(this.dw_webfax)
end on

type cb_annulla from commandbutton within w_seleziona_webfax
integer x = 457
integer y = 28
integer width = 402
integer height = 88
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;closewithreturn(parent, "")
end event

type cb_seleziona from commandbutton within w_seleziona_webfax
integer x = 46
integer y = 28
integer width = 402
integer height = 88
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Seleziona"
end type

event clicked;long ll_row
string ls_cod_webfax

ll_row=dw_webfax.getrow()

if ll_row>0 then
	ls_cod_webfax = dw_webfax.getitemstring(ll_row, "cod_fax")
	closewithreturn(parent, ls_cod_webfax)
else
	g_mb.show("Selezionare una riga!")
end if
end event

type dw_webfax from datawindow within w_seleziona_webfax
integer x = 37
integer y = 136
integer width = 3767
integer height = 1112
integer taborder = 10
string title = "none"
string dataobject = "d_webfax"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event rowfocuschanged;if currentrow>0 then
	selectrow(0, false)
	selectrow(currentrow, true)
end if
end event

