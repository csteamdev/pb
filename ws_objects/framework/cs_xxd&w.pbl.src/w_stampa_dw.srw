﻿$PBExportHeader$w_stampa_dw.srw
forward
global type w_stampa_dw from w_cs_xx_risposta
end type
type cb_stampante from commandbutton within w_stampa_dw
end type
type cb_annulla from commandbutton within w_stampa_dw
end type
type cb_ok from commandbutton within w_stampa_dw
end type
type dw_stampa_dw from datawindow within w_stampa_dw
end type
end forward

global type w_stampa_dw from w_cs_xx_risposta
integer x = 668
integer y = 469
integer width = 1595
integer height = 1068
string title = "Opzioni di stampa"
cb_stampante cb_stampante
cb_annulla cb_annulla
cb_ok cb_ok
dw_stampa_dw dw_stampa_dw
end type
global w_stampa_dw w_stampa_dw

type variables
uo_cs_xx_dw idw_dw
end variables

event open;call super::open;idw_dw = s_cs_xx.parametri.parametro_uo_dw_1

setnull(s_cs_xx.parametri.parametro_uo_dw_1)

dw_stampa_dw.insertrow(0)

dw_stampa_dw.setitem(1,"orientamento",long(idw_dw.object.datawindow.print.orientation))

dw_stampa_dw.setitem(1,"fascicola",idw_dw.object.datawindow.print.collate)

dw_stampa_dw.setitem(1,"copie",long(idw_dw.object.datawindow.print.copies))

dw_stampa_dw.setitem(1,"pagine","")

dw_stampa_dw.setitem(1,"colore",1)

dw_stampa_dw.setitem(1,"pari_dispari",long(idw_dw.object.datawindow.print.page.rangeinclude))

string ls_stampante

ls_stampante = printgetprinter()

ls_stampante = left(ls_stampante,pos(ls_stampante,"~t",1) - 1)

dw_stampa_dw.setitem(1,"stampante",ls_stampante)


end event

on w_stampa_dw.create
int iCurrent
call super::create
this.cb_stampante=create cb_stampante
this.cb_annulla=create cb_annulla
this.cb_ok=create cb_ok
this.dw_stampa_dw=create dw_stampa_dw
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_stampante
this.Control[iCurrent+2]=this.cb_annulla
this.Control[iCurrent+3]=this.cb_ok
this.Control[iCurrent+4]=this.dw_stampa_dw
end on

on w_stampa_dw.destroy
call super::destroy
destroy(this.cb_stampante)
destroy(this.cb_annulla)
destroy(this.cb_ok)
destroy(this.dw_stampa_dw)
end on

event key;call super::key;choose case key
	case keyescape!
		cb_annulla.triggerevent("clicked")
	case keyenter!
		cb_ok.triggerevent("clicked")
end choose
end event

type cb_stampante from commandbutton within w_stampa_dw
integer x = 1381
integer y = 80
integer width = 91
integer height = 88
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "..."
end type

event clicked;printsetup()

string ls_stampante

ls_stampante = printgetprinter()

ls_stampante = left(ls_stampante,pos(ls_stampante,"~t",1) - 1)

dw_stampa_dw.setitem(1,"stampante",ls_stampante)
end event

type cb_annulla from commandbutton within w_stampa_dw
integer x = 1120
integer y = 840
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;s_cs_xx.parametri.parametro_b_1 = false

close(parent)
end event

type cb_ok from commandbutton within w_stampa_dw
integer x = 1120
integer y = 760
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "OK"
end type

event clicked;long ll_copie

dw_stampa_dw.accepttext()

idw_dw.object.datawindow.print.orientation = int(dw_stampa_dw.getitemnumber(1,"orientamento"))

idw_dw.object.datawindow.print.collate = dw_stampa_dw.getitemstring(1,"fascicola")

idw_dw.object.datawindow.print.copies = int(dw_stampa_dw.getitemnumber(1,"copie"))

idw_dw.object.datawindow.print.page.range = dw_stampa_dw.getitemstring(1,"pagine")

idw_dw.object.datawindow.print.color = int(dw_stampa_dw.getitemnumber(1,"colore"))

idw_dw.object.datawindow.print.page.rangeinclude = int(dw_stampa_dw.getitemnumber(1,"pari_dispari"))

idw_dw.object.datawindow.print.documentname = ""

s_cs_xx.parametri.parametro_b_1 = true

close(parent)
end event

type dw_stampa_dw from datawindow within w_stampa_dw
integer x = 18
integer y = 16
integer width = 1499
integer height = 928
integer taborder = 10
string dataobject = "d_stampa_dw"
boolean border = false
end type

