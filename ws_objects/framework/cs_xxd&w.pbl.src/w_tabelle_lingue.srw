﻿$PBExportHeader$w_tabelle_lingue.srw
forward
global type w_tabelle_lingue from w_cs_xx_principale
end type
type dw_tabelle_lingue from uo_cs_xx_dw within w_tabelle_lingue
end type
end forward

global type w_tabelle_lingue from w_cs_xx_principale
integer width = 4142
integer height = 1652
string title = "Definzione Tabelle Descrizioni in Lingua"
dw_tabelle_lingue dw_tabelle_lingue
end type
global w_tabelle_lingue w_tabelle_lingue

event pc_setwindow;call super::pc_setwindow;dw_tabelle_lingue.set_dw_options(sqlca, &
                                 pcca.null_object, &
                                 c_default, &
                                 c_default)
end event

on w_tabelle_lingue.create
int iCurrent
call super::create
this.dw_tabelle_lingue=create dw_tabelle_lingue
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tabelle_lingue
end on

on w_tabelle_lingue.destroy
call super::destroy
destroy(this.dw_tabelle_lingue)
end on

type dw_tabelle_lingue from uo_cs_xx_dw within w_tabelle_lingue
integer y = 20
integer width = 4069
integer height = 1500
integer taborder = 10
string dataobject = "d_tabelle_lingue"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore

ll_errore = retrieve()

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

