﻿$PBExportHeader$w_test_mb.srw
forward
global type w_test_mb from window
end type
type st_5 from statictext within w_test_mb
end type
type em_default from editmask within w_test_mb
end type
type st_4 from statictext within w_test_mb
end type
type st_3 from statictext within w_test_mb
end type
type st_2 from statictext within w_test_mb
end type
type cbx_2 from checkbox within w_test_mb
end type
type cbx_1 from checkbox within w_test_mb
end type
type cb_2 from commandbutton within w_test_mb
end type
type st_1 from statictext within w_test_mb
end type
type ddlb_2 from dropdownlistbox within w_test_mb
end type
type mle_1 from multilineedit within w_test_mb
end type
type cb_1 from commandbutton within w_test_mb
end type
type ddlb_1 from dropdownlistbox within w_test_mb
end type
end forward

global type w_test_mb from window
integer width = 1787
integer height = 1344
boolean titlebar = true
boolean controlmenu = true
boolean minbox = true
windowtype windowtype = child!
long backcolor = 12632256
string icon = "AppIcon!"
boolean center = true
st_5 st_5
em_default em_default
st_4 st_4
st_3 st_3
st_2 st_2
cbx_2 cbx_2
cbx_1 cbx_1
cb_2 cb_2
st_1 st_1
ddlb_2 ddlb_2
mle_1 mle_1
cb_1 cb_1
ddlb_1 ddlb_1
end type
global w_test_mb w_test_mb

on w_test_mb.create
this.st_5=create st_5
this.em_default=create em_default
this.st_4=create st_4
this.st_3=create st_3
this.st_2=create st_2
this.cbx_2=create cbx_2
this.cbx_1=create cbx_1
this.cb_2=create cb_2
this.st_1=create st_1
this.ddlb_2=create ddlb_2
this.mle_1=create mle_1
this.cb_1=create cb_1
this.ddlb_1=create ddlb_1
this.Control[]={this.st_5,&
this.em_default,&
this.st_4,&
this.st_3,&
this.st_2,&
this.cbx_2,&
this.cbx_1,&
this.cb_2,&
this.st_1,&
this.ddlb_2,&
this.mle_1,&
this.cb_1,&
this.ddlb_1}
end on

on w_test_mb.destroy
destroy(this.st_5)
destroy(this.em_default)
destroy(this.st_4)
destroy(this.st_3)
destroy(this.st_2)
destroy(this.cbx_2)
destroy(this.cbx_1)
destroy(this.cb_2)
destroy(this.st_1)
destroy(this.ddlb_2)
destroy(this.mle_1)
destroy(this.cb_1)
destroy(this.ddlb_1)
end on

type st_5 from statictext within w_test_mb
integer x = 23
integer y = 940
integer width = 407
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Ritorno:"
boolean focusrectangle = false
end type

type em_default from editmask within w_test_mb
integer x = 457
integer y = 840
integer width = 114
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "1"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "#"
string minmax = "1~~3"
end type

type st_4 from statictext within w_test_mb
integer x = 23
integer y = 840
integer width = 407
integer height = 64
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Bottone Default:"
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type st_3 from statictext within w_test_mb
integer x = 23
integer y = 740
integer width = 402
integer height = 64
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Pulsanti:"
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type st_2 from statictext within w_test_mb
integer x = 23
integer y = 640
integer width = 402
integer height = 64
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Icona:"
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type cbx_2 from checkbox within w_test_mb
integer x = 23
integer y = 520
integer width = 846
integer height = 80
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Includi istruzione SQL errata"
end type

event clicked;if checked then
	cbx_1.checked = false
end if
end event

type cbx_1 from checkbox within w_test_mb
integer x = 23
integer y = 440
integer width = 869
integer height = 80
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Includi istruzione SQL corretta"
end type

event clicked;if checked then
	cbx_2.checked = false
end if
end event

type cb_2 from commandbutton within w_test_mb
integer x = 23
integer y = 1140
integer width = 1737
integer height = 100
integer taborder = 30
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "!!! GENERATE SYSTEM ERROR !!!"
end type

event clicked;window lw_window

open(lw_window,"adsgfsfg")
end event

type st_1 from statictext within w_test_mb
integer x = 457
integer y = 940
integer width = 114
integer height = 80
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
alignment alignment = center!
boolean border = true
borderstyle borderstyle = StyleLowered!
boolean focusrectangle = false
end type

type ddlb_2 from dropdownlistbox within w_test_mb
integer x = 457
integer y = 740
integer width = 640
integer height = 400
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "YESNOCANCEL"
string item[] = {"OK","OKCANCEL","YESNO","YESNOCANCEL"}
borderstyle borderstyle = stylelowered!
end type

type mle_1 from multilineedit within w_test_mb
integer x = 23
integer y = 20
integer width = 1737
integer height = 400
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "testo della messagebox"
boolean autohscroll = true
boolean autovscroll = true
borderstyle borderstyle = stylelowered!
end type

type cb_1 from commandbutton within w_test_mb
integer x = 23
integer y = 1040
integer width = 1737
integer height = 100
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Invia"
end type

event clicked;long ll_return

boolean ab_tran_info = false

icon l_icon
button l_button

choose case ddlb_1.text
	case "INFO"
		l_icon = information!
	case "STOPSIGN"
		l_icon = stopsign!
	case "WARNING"
		l_icon = exclamation!
	case "QUESTION"
		l_icon = question!
	case "NONE"
		l_icon = none!
end choose

choose case ddlb_2.text
	case "OK"
		l_button = ok!
	case "OKCANCEL"
		l_button = okcancel!
	case "YESNO"
		l_button = yesno!
	case "YESNOCANCEL"
		l_button = yesnocancel!
end choose

if cbx_1.checked then
	
	ab_tran_info = true
	
	long ll_test_1
	
	select count(*) into :ll_test_1 from aziende;
	
elseif cbx_2.checked then
	
	ab_tran_info = true
	
	long ll_test_2
	
	select rag_soc_1 into :ll_test_2 from aziende;
	
end if

nvo_messagebox luo_mb

luo_mb = create nvo_messagebox

ll_return = luo_mb.messagebox("Test MessageBox custom",mle_1.text,l_icon,l_button,ab_tran_info,sqlca,true,this,false,false)

st_1.text = string(ll_return)

destroy luo_mb
end event

type ddlb_1 from dropdownlistbox within w_test_mb
integer x = 457
integer y = 640
integer width = 640
integer height = 560
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "QUESTION"
string item[] = {"INFO","WARNING","QUESTION","STOPSIGN","NONE"}
borderstyle borderstyle = stylelowered!
end type

