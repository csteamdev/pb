﻿$PBExportHeader$w_test_mod_dw.srw
forward
global type w_test_mod_dw from window
end type
type dw_mod from datawindow within w_test_mod_dw
end type
end forward

global type w_test_mod_dw from window
integer width = 2373
integer height = 1608
boolean titlebar = true
string title = "Test Modifiche DW"
boolean controlmenu = true
windowtype windowtype = response!
long backcolor = 67108864
string icon = "AppIcon!"
boolean center = true
dw_mod dw_mod
end type
global w_test_mod_dw w_test_mod_dw

type variables
datawindow idw_test
end variables

forward prototypes
public function integer wf_test_mod ()
end prototypes

public function integer wf_test_mod ();long				ll_colore_new_tes, ll_colore_new_det, &
					ll_colore_mod_tes, ll_colore_mod_det, &
					ll_colore_del_tes, ll_colore_del_det

long				ll_id_new, ll_id_mod, ll_id_del

long				ll_row, ll_column, ll_newrow

string			ls_des_new, ls_des_mod, ls_des_del

dwitemstatus	l_dwstatus


setpointer(hourglass!)

dw_mod.reset()

dw_mod.setredraw(false)

ll_id_new				= 1
ls_des_new				= "RIGHE AGGIUNTE"
ll_colore_new_tes		= rgb(080,150,080)
ll_colore_new_det		= rgb(080,200,080)

ll_id_mod				= 2
ls_des_mod				= "RIGHE MODIFICATE"
ll_colore_mod_tes		= rgb(150,150,080)
ll_colore_mod_det		= rgb(200,200,080)

ll_id_del				= 3
ls_des_del				= "RIGHE CANCELLATE"
ll_colore_del_tes		= rgb(150,080,080)
ll_colore_del_det		= rgb(200,080,080)


// NEW
for ll_row = 1 to idw_test.rowcount()
	
	for ll_column = 1 to 1
		
		l_dwstatus = idw_test.getitemstatus(ll_row,0,primary!)
		
		if l_dwstatus = newmodified! then
			
			ll_newrow = dw_mod.insertrow(0)
			dw_mod.setitem(ll_newrow,"id_tipo",ll_id_new)
			dw_mod.setitem(ll_newrow,"des_tipo",ls_des_new)
			dw_mod.setitem(ll_newrow,"colore_tipo",ll_colore_new_tes)
			dw_mod.setitem(ll_newrow,"id_riga",ll_row)
			dw_mod.setitem(ll_newrow,"des_riga","Riga " + string(idw_test.getrowidfromrow(ll_row,primary!)))
			dw_mod.setitem(ll_newrow,"colore_riga",ll_colore_new_det)
			dw_mod.setitem(ll_newrow,"id_colonna",ll_column)
			dw_mod.setitem(ll_newrow,"des_colonna","n/a")
			dw_mod.setitem(ll_newrow,"colore_colonna",ll_colore_new_det)
			
		end if
		
	next
	
next


// MOD
for ll_row = 1 to idw_test.rowcount()
	
	for ll_column = 1 to long(idw_test.object.datawindow.column.count)
		
		l_dwstatus = idw_test.getitemstatus(ll_row,ll_column,primary!)
		
		if l_dwstatus = datamodified! then
			
			ll_newrow = dw_mod.insertrow(0)
			dw_mod.setitem(ll_newrow,"id_tipo",ll_id_mod)
			dw_mod.setitem(ll_newrow,"des_tipo",ls_des_mod)
			dw_mod.setitem(ll_newrow,"colore_tipo",ll_colore_mod_tes)
			dw_mod.setitem(ll_newrow,"id_riga",ll_row)
			dw_mod.setitem(ll_newrow,"des_riga","Riga " + string(idw_test.getrowidfromrow(ll_row,primary!)))
			dw_mod.setitem(ll_newrow,"colore_riga",ll_colore_mod_det)
			dw_mod.setitem(ll_newrow,"id_colonna",ll_column)
			dw_mod.setitem(ll_newrow,"des_colonna",idw_test.describe("#" + string(ll_column) + ".name"))
			dw_mod.setitem(ll_newrow,"colore_colonna",ll_colore_mod_det)
			
		end if
		
	next
	
next


// DEL
for ll_row = 1 to idw_test.deletedcount()
	
	for ll_column = 1 to 1

		ll_newrow = dw_mod.insertrow(0)
		dw_mod.setitem(ll_newrow,"id_tipo",ll_id_del)
		dw_mod.setitem(ll_newrow,"des_tipo",ls_des_del)
		dw_mod.setitem(ll_newrow,"colore_tipo",ll_colore_del_tes)
		dw_mod.setitem(ll_newrow,"id_riga",ll_row)
		dw_mod.setitem(ll_newrow,"des_riga","Riga " + string(idw_test.getrowidfromrow(ll_row,delete!)))
		dw_mod.setitem(ll_newrow,"colore_riga",ll_colore_del_det)
		dw_mod.setitem(ll_newrow,"id_colonna",ll_column)
		dw_mod.setitem(ll_newrow,"des_colonna","n/a")
		dw_mod.setitem(ll_newrow,"colore_colonna",ll_colore_del_det)
		
	next
	
next


dw_mod.sort()
dw_mod.groupcalc()

dw_mod.setredraw(true)

setpointer(arrow!)

return 0
end function

event open;idw_test = message.powerobjectparm

setnull(message.powerobjectparm)

wf_test_mod()
end event

on w_test_mod_dw.create
this.dw_mod=create dw_mod
this.Control[]={this.dw_mod}
end on

on w_test_mod_dw.destroy
destroy(this.dw_mod)
end on

type dw_mod from datawindow within w_test_mod_dw
integer width = 2354
integer height = 1520
integer taborder = 10
string dataobject = "d_test_mod_dw"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

