﻿$PBExportHeader$w_topic_help.srw
$PBExportComments$Finestra Gestione Topic Help
forward
global type w_topic_help from w_cs_xx_principale
end type
type dw_topic_help from uo_cs_xx_dw within w_topic_help
end type
end forward

global type w_topic_help from w_cs_xx_principale
int Width=3621
int Height=1149
boolean TitleBar=true
string Title="Gestione Help"
dw_topic_help dw_topic_help
end type
global w_topic_help w_topic_help

on pc_setddlb;call w_cs_xx_principale::pc_setddlb;f_po_loaddddw_dw(dw_topic_help, &
                 "num_help", &
                 sqlca, &
                 "tab_help", &
                 "num_help", &
                 "des_breve_help", &
                 "")

end on

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_topic_help.set_dw_options(sqlca, &
                             pcca.null_object, &
                             c_default, &
                             c_default)
end on

on w_topic_help.create
int iCurrent
call w_cs_xx_principale::create
this.dw_topic_help=create dw_topic_help
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_topic_help
end on

on w_topic_help.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_topic_help)
end on

type dw_topic_help from uo_cs_xx_dw within w_topic_help
int X=23
int Y=21
int Width=3543
int Height=1001
int TabOrder=20
string DataObject="d_topic_help"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;LONG  l_Error

l_Error = Retrieve()

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end on

