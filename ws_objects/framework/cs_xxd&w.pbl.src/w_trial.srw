﻿$PBExportHeader$w_trial.srw
forward
global type w_trial from window
end type
type st_7 from statictext within w_trial
end type
type st_6 from statictext within w_trial
end type
type cb_2 from commandbutton within w_trial
end type
type sle_seriale_attivazione from singlelineedit within w_trial
end type
type st_5 from statictext within w_trial
end type
type sle_seriale from singlelineedit within w_trial
end type
type st_4 from statictext within w_trial
end type
type st_3 from statictext within w_trial
end type
type sle_societa from singlelineedit within w_trial
end type
type sle_nome from singlelineedit within w_trial
end type
type st_2 from statictext within w_trial
end type
type st_1 from statictext within w_trial
end type
type cb_1 from commandbutton within w_trial
end type
end forward

global type w_trial from window
integer width = 1358
integer height = 1112
boolean titlebar = true
string title = "Untitled"
boolean controlmenu = true
windowtype windowtype = response!
long backcolor = 12632256
string icon = "AppIcon!"
boolean center = true
event keydown pbm_keydown
st_7 st_7
st_6 st_6
cb_2 cb_2
sle_seriale_attivazione sle_seriale_attivazione
st_5 st_5
sle_seriale sle_seriale
st_4 st_4
st_3 st_3
sle_societa sle_societa
sle_nome sle_nome
st_2 st_2
st_1 st_1
cb_1 cb_1
end type
global w_trial w_trial

type variables
n_cst_crypto lo_crypt
end variables

forward prototypes
public function long fwf_randomize (long al_range)
end prototypes

event keydown;// *** stefanop 14/07/2008: Per saltare la validazione premere CTRL+SHIFT+P e nel seriale attivazione deve essere scritto CS
if key = KeyP! and keyflags = 3 and left(sle_seriale_attivazione.text, 2) = "CS" then	
	
	sle_seriale_attivazione.text = lo_crypt.encryptserial(sle_seriale.text) + "AAA"
	
end if
end event

public function long fwf_randomize (long al_range);Long ll_Offset, ll_Return
Integer li_Range, li_Blocks
Integer li_BlockSize = 32767

IF al_range > 1073676289 THEN
	//Range is too big
	Return 0
END IF

//Determine The number of blocks
li_Blocks = Truncate(al_range/li_BlockSize,0) 

//Add an extra block to accommodate any remainder
IF Mod(al_range, li_BlockSize) > 0 THEN
	li_Blocks = li_Blocks + 1
END IF

// Improve efficiency for ranges less than Block Size
// where there is only one block 
IF al_range < li_BlockSize THEN
	li_Range = al_range
ELSE
	li_Range = li_BlockSize
END IF

ll_Return = 0

// Loop until the value is in range. 
// If the value is not in range, calculate the 
// Offset again to ensure even probability 
DO UNTIL (ll_Return > 0) And (ll_Return <= al_Range)
	// Calculate a Random Offset using the number
	// of blocks as the range. 
	// Offsets will range from [0 to (li_Blocks - 1)*BlockSize]
	// in increments of BlockSize
	ll_Offset = (Rand(li_Blocks) - 1) * li_BlockSize

	//Main Calculation
	ll_Return = ll_Offset + Rand(li_Range)
LOOP

Return ll_Return
end function

on w_trial.create
this.st_7=create st_7
this.st_6=create st_6
this.cb_2=create cb_2
this.sle_seriale_attivazione=create sle_seriale_attivazione
this.st_5=create st_5
this.sle_seriale=create sle_seriale
this.st_4=create st_4
this.st_3=create st_3
this.sle_societa=create sle_societa
this.sle_nome=create sle_nome
this.st_2=create st_2
this.st_1=create st_1
this.cb_1=create cb_1
this.Control[]={this.st_7,&
this.st_6,&
this.cb_2,&
this.sle_seriale_attivazione,&
this.st_5,&
this.sle_seriale,&
this.st_4,&
this.st_3,&
this.sle_societa,&
this.sle_nome,&
this.st_2,&
this.st_1,&
this.cb_1}
end on

on w_trial.destroy
destroy(this.st_7)
destroy(this.st_6)
destroy(this.cb_2)
destroy(this.sle_seriale_attivazione)
destroy(this.st_5)
destroy(this.sle_seriale)
destroy(this.st_4)
destroy(this.st_3)
destroy(this.sle_societa)
destroy(this.sle_nome)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.cb_1)
end on

event open;title = "Attivazione"

lo_crypt 		= create n_cst_crypto

Randomize(0)
sle_seriale.text = string(rand(9))

Randomize(0)
sle_seriale.text += string(rand(3))

Randomize(0)
sle_seriale.text += string(rand(2))

Randomize(0)
sle_seriale.text += string(rand(6))

Randomize(0)
sle_seriale.text += string(rand(4))

Randomize(0)
sle_seriale.text += string(rand(1))
end event

event close;destroy lo_crypt
end event

type st_7 from statictext within w_trial
integer x = 46
integer y = 960
integer width = 1257
integer height = 60
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
string text = "Attenzione: sono richiesti i diritti di amministratore"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_6 from statictext within w_trial
integer x = 46
integer y = 140
integer width = 1257
integer height = 140
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
string text = "Per procede con l~'uso del programma è necessario attivare la licenza d~'uso."
boolean focusrectangle = false
end type

type cb_2 from commandbutton within w_trial
integer x = 46
integer y = 800
integer width = 274
integer height = 112
integer taborder = 50
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Chiudi"
end type

event clicked;closeWithReturn(parent, -1)
end event

type sle_seriale_attivazione from singlelineedit within w_trial
integer x = 571
integer y = 640
integer width = 731
integer height = 80
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type st_5 from statictext within w_trial
integer x = 46
integer y = 640
integer width = 512
integer height = 64
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
string text = "Codice Attivazione:"
boolean focusrectangle = false
end type

type sle_seriale from singlelineedit within w_trial
integer x = 571
integer y = 540
integer width = 731
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
boolean displayonly = true
borderstyle borderstyle = stylelowered!
end type

type st_4 from statictext within w_trial
integer x = 46
integer y = 540
integer width = 389
integer height = 60
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
string text = "Seriale:"
boolean focusrectangle = false
end type

type st_3 from statictext within w_trial
integer x = 46
integer y = 440
integer width = 389
integer height = 60
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
string text = "Società:"
boolean focusrectangle = false
end type

type sle_societa from singlelineedit within w_trial
integer x = 571
integer y = 420
integer width = 731
integer height = 80
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type sle_nome from singlelineedit within w_trial
integer x = 571
integer y = 320
integer width = 731
integer height = 80
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type st_2 from statictext within w_trial
integer x = 46
integer y = 320
integer width = 389
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
string text = "Nome:"
boolean focusrectangle = false
end type

type st_1 from statictext within w_trial
integer x = 46
integer y = 40
integer width = 571
integer height = 100
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
string text = "Attivazione"
boolean focusrectangle = false
end type

type cb_1 from commandbutton within w_trial
integer x = 777
integer y = 800
integer width = 526
integer height = 112
integer taborder = 40
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Attiva"
boolean default = true
end type

event clicked;// Valido i parametri

string ls_nome, ls_societa, ls_seriale, ls_cod_att, ls_trial, ls_cod_trial, ls_trial_letter, ls_update
int 	li_i, li_giorni
date	ld_fine, ld_accesso

ls_nome 		= sle_nome.text
ls_societa 	= sle_societa.text
ls_seriale 	= sle_seriale.text
ls_cod_att 	= sle_seriale_attivazione.text

// *** stefanop 22/07/2008: Su richiesta di Donato aggiungo il codice per controllare se la tabella
// registrazioni esiste e se necessario la creo al volo.
// {
string ls_table, ls_sql

select 'prova'
into	:ls_table
from	registrazione
using	sqlca;

//ls_sql = "drop table registrazione"
//execute immediate :ls_sql using sqlca;

if sqlca.sqlcode < 0 then
	
	ls_sql = 	"create table registrazione " &
				+ "(nome varchar(30), "  &
				+ "societa varchar(30), "  &
				+ "cod_individuale varchar(20), " &
				+ "num_serie varchar(20)) "
	
	execute immediate :ls_sql using sqlca;
	
	if sqlca.sqlcode <> 0 then
		messagebox("Apice", "Impossibile eliminare tabella, verificare i parametri di connessione")
		return -1
	end if
	
end if

delete registrazione
using 	 sqlca;

if sqlca.sqlcode <> 0 then
	messagebox("Apice", "Impossibile eliminare dati.~r~n"+sqlca.sqlerrtext)
	return -1
end if
// }

ls_cod_trial 	= right(ls_cod_att, 3)
ls_cod_att 	= left(ls_cod_att, len(ls_cod_att) -3)

if lo_crypt.decryptserial( ls_seriale, ls_cod_att) then
	
	// Controllo il codice alla fine del seriale che identifica la durata della DEMO
	// Solo al codice AAA il programma non ha nessuna scadeza
	if ls_cod_trial <> "AAA" then
		li_giorni = 0
		
		// Lettra sinistra
		ls_trial_letter = left(ls_cod_trial, 1)
		li_giorni += (Asc(ls_trial_letter) - 65) * 100
		
		ls_trial_letter = mid(ls_cod_trial, 2, 1)
		li_giorni += (Asc(ls_trial_letter) - 65) * 10
		
		ls_trial_letter = right(ls_cod_trial, 1)
		li_giorni += Asc(ls_trial_letter) - 65
		
		ld_fine = RelativeDate(today(), li_giorni)
	else
		ld_fine = RelativeDate(today(), 365000)
	end if
	
	ld_accesso = today()
	ls_cod_att 	= sle_seriale_attivazione.text
	ls_update = string(ld_fine, "dd/mm/yyyy") + string(ld_accesso, "dd/mm/yyyy")
	ls_update = lo_crypt.encrypt_data( ls_update)
	
	// Aggiorno la tabella REGISTRAZIONE con i nuovi codici
	insert into 	registrazione (nome, societa, cod_individuale, num_serie)
	values 		(:ls_nome, :ls_societa, :ls_update, :ls_cod_att)
	using			sqlca;
	
	if sqlca.sqlcode <> 0 then
		messagebox("Apice", "Impossibile salvare l'attivazione, contattare l'Amministratore del sistema~r~n"+sqlca.sqlerrtext, StopSign!)
		return -1
	end if
	
	closeWithReturn(parent, 1)
else
	Messagebox("Apice", "Attenzione: codice di attivazione non valido")
end if
end event

