﻿$PBExportHeader$w_trova.srw
$PBExportComments$Finestra Trova
forward
global type w_trova from window
end type
type cb_trova_primo from commandbutton within w_trova
end type
type cb_chiudi from commandbutton within w_trova
end type
type cb_trova_successivo from commandbutton within w_trova
end type
type st_trova from statictext within w_trova
end type
type sle_trova from singlelineedit within w_trova
end type
type st_dova from statictext within w_trova
end type
type ddlb_dove from dropdownlistbox within w_trova
end type
type rb_su from radiobutton within w_trova
end type
type rb_giu from radiobutton within w_trova
end type
type gb_direzione from groupbox within w_trova
end type
type cbx_case from checkbox within w_trova
end type
type st_campi from statictext within w_trova
end type
type lb_campi from listbox within w_trova
end type
end forward

global type w_trova from window
integer x = 174
integer width = 2185
integer height = 668
boolean titlebar = true
string title = "Trova"
boolean controlmenu = true
windowtype windowtype = response!
long backcolor = 12632256
cb_trova_primo cb_trova_primo
cb_chiudi cb_chiudi
cb_trova_successivo cb_trova_successivo
st_trova st_trova
sle_trova sle_trova
st_dova st_dova
ddlb_dove ddlb_dove
rb_su rb_su
rb_giu rb_giu
gb_direzione gb_direzione
cbx_case cbx_case
st_campi st_campi
lb_campi lb_campi
end type
global w_trova w_trova

type variables
string is_nome_colonna[]
uo_cs_xx_dw iuo_dw

end variables

forward prototypes
public subroutine wf_trova (boolean ab_trova_primo)
end prototypes

public subroutine wf_trova (boolean ab_trova_primo);boolean lb_trova = true
integer li_dove
long ll_i, ll_prima_riga, ll_ultima_riga, ll_riga
string ls_nome_colonna, ls_tipo_colonna, ls_espressione, ls_tot_espressione, ls_valore


if lb_campi.totalselected() <= 0 then
   g_mb.messagebox("Attenzione", "Selezionare almeno un campo.", exclamation!)
   return
end if

if rb_giu.checked then
   ll_ultima_riga = iuo_dw.rowcount()
   if ab_trova_primo then
      ll_prima_riga = 1
   else
      ll_prima_riga = iuo_dw.getrow() + 1
      if ll_prima_riga > ll_ultima_riga then lb_trova = false
   end if
else
   ll_ultima_riga = 1
   if ab_trova_primo then
      ll_prima_riga = iuo_dw.rowcount()
   else
      ll_prima_riga = iuo_dw.getrow() - 1
      if ll_prima_riga = 0 then lb_trova = false
   end if
end if

if not lb_trova then
   g_mb.messagebox("Attenzione", "Testo non trovato.", exclamation!)
   return
end if

setpointer(hourglass!)

for ll_i = lb_campi.totalitems() to 1 step -1
   if lb_campi.state(ll_i) = 1 then
      ls_nome_colonna = is_nome_colonna[ll_i]
      ls_tipo_colonna = left(iuo_dw.describe(ls_nome_colonna + ".coltype"), 4)
      ls_espressione = ""
      ls_valore = sle_trova.text
      if ls_valore = "" then
         ls_espressione = "isnull(" + ls_nome_colonna + ")"
         if ls_tipo_colonna = "char" then
            ls_espressione = ls_espressione + " or " + ls_nome_colonna + "=''"
         end if
      else
         if not cbx_case.checked then
            if ls_tipo_colonna <> "char" then
               ls_nome_colonna = "string(" + ls_nome_colonna + ")"
               ls_tipo_colonna = "char"
            end if
            ls_nome_colonna = "upper(" + ls_nome_colonna + ")"
            ls_valore = upper(ls_valore)
         end if
         li_dove = ddlb_dove.finditem(ddlb_dove.text, 0)
         if li_dove = 1 then
            choose case ls_tipo_colonna
               case "char"
                  ls_valore = "'" + ls_valore + "'"
               case "numb", "deci"
                  if not isnumber(ls_valore) then ls_nome_colonna = ""
               case "date", "time"
                  ls_valore = "datetime('" + ls_valore + "')"
            end choose
            if ls_nome_colonna <> "" then
               ls_espressione = ls_nome_colonna + "=" + ls_valore
            end if
         else
            if li_dove = 2 then
               ls_espressione = "^" 
            else
               ls_espressione = ".*" 
            end if
            if ls_tipo_colonna <> "char" then
               ls_nome_colonna = "string(" + ls_nome_colonna + ")"
            end if
            ls_espressione = "match(" + ls_nome_colonna + ",'" + ls_espressione + ls_valore + &
                             ".*')"
         end if
      end if
      if ls_espressione <> "" then
         if ls_tot_espressione <> "" then ls_tot_espressione = ls_tot_espressione + "or"
         ls_tot_espressione = ls_tot_espressione + "(" + ls_espressione + ")"
      end if
   end if
next

ll_riga = iuo_dw.find(ls_tot_espressione, ll_prima_riga, ll_ultima_riga)
if ll_riga > 0 then
   iuo_dw.scrolltorow(ll_riga)
else
   g_mb.messagebox("Attenzione", "Testo non trovato.", exclamation!)  
end if

end subroutine

event open;string ls_nome_colonna_attiva, ls_nome_colonna_db, ls_nome_tabella, ls_nome_colonna, ls_header
long ll_i, ll_j, ll_k


iuo_dw = message.powerobjectparm

ls_nome_colonna_attiva = iuo_dw.getcolumnname()

for ll_i = 1 to iuo_dw.i_numcolumns
   iuo_dw.get_val_info(ll_i)
   if iuo_dw.i_colvisible[ll_i] then
      ll_j ++
      ls_nome_colonna_db = iuo_dw.i_xdbnames[ll_i]
      ls_nome_tabella = mid(ls_nome_colonna_db, 1, pos(ls_nome_colonna_db, ".") - 1)
      ls_nome_colonna = mid(ls_nome_colonna_db, pos(ls_nome_colonna_db, ".") + 1, len(ls_nome_colonna_db))
      select pbc_hdr
      into   :ls_header
      from   pbcatcol
      where  pbc_tnam = :ls_nome_tabella and
             pbc_cnam = :ls_nome_colonna;
      if isnull(ls_header) or ls_header = "" then
         lb_campi.additem(ls_nome_colonna)
      else
         lb_campi.additem(ls_header)
      end if
      is_nome_colonna[ll_j] = iuo_dw.i_xcolumnnames[ll_i]
      if ll_k = 0 then
         if ls_nome_colonna_attiva = iuo_dw.i_xcolumnnames[ll_i] then ll_k = ll_j
      end if
   end if
next

if ll_k > 0 then 
	lb_campi.setstate(ll_k, true)
else
	if lb_campi.totalitems() > 0 then
		for ll_i = 1 to lb_campi.totalitems()
			lb_campi.setstate(ll_i, true)
		next
	end if
end if
		
ddlb_dove.selectitem(3)

f_po_setwindowposition(this)
end event

on w_trova.create
this.cb_trova_primo=create cb_trova_primo
this.cb_chiudi=create cb_chiudi
this.cb_trova_successivo=create cb_trova_successivo
this.st_trova=create st_trova
this.sle_trova=create sle_trova
this.st_dova=create st_dova
this.ddlb_dove=create ddlb_dove
this.rb_su=create rb_su
this.rb_giu=create rb_giu
this.gb_direzione=create gb_direzione
this.cbx_case=create cbx_case
this.st_campi=create st_campi
this.lb_campi=create lb_campi
this.Control[]={this.cb_trova_primo,&
this.cb_chiudi,&
this.cb_trova_successivo,&
this.st_trova,&
this.sle_trova,&
this.st_dova,&
this.ddlb_dove,&
this.rb_su,&
this.rb_giu,&
this.gb_direzione,&
this.cbx_case,&
this.st_campi,&
this.lb_campi}
end on

on w_trova.destroy
destroy(this.cb_trova_primo)
destroy(this.cb_chiudi)
destroy(this.cb_trova_successivo)
destroy(this.st_trova)
destroy(this.sle_trova)
destroy(this.st_dova)
destroy(this.ddlb_dove)
destroy(this.rb_su)
destroy(this.rb_giu)
destroy(this.gb_direzione)
destroy(this.cbx_case)
destroy(this.st_campi)
destroy(this.lb_campi)
end on

type cb_trova_primo from commandbutton within w_trova
integer x = 1760
integer y = 40
integer width = 366
integer height = 81
integer taborder = 60
integer textsize = -8
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Trova &Primo"
boolean default = true
end type

on clicked;wf_trova(true)
end on

type cb_chiudi from commandbutton within w_trova
integer x = 1755
integer y = 280
integer width = 366
integer height = 81
integer taborder = 80
integer textsize = -8
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Chiudi"
boolean cancel = true
end type

on clicked;close(parent)
end on

type cb_trova_successivo from commandbutton within w_trova
integer x = 1760
integer y = 160
integer width = 366
integer height = 81
integer taborder = 70
integer textsize = -8
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Trova &Suc."
end type

on clicked;wf_trova(false)
end on

type st_trova from statictext within w_trova
integer x = 46
integer y = 40
integer width = 251
integer height = 60
integer textsize = -8
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
boolean enabled = false
string text = "Trova:"
alignment alignment = right!
boolean focusrectangle = false
end type

type sle_trova from singlelineedit within w_trova
integer x = 320
integer y = 40
integer width = 1394
integer height = 80
integer taborder = 10
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean autohscroll = false
borderstyle borderstyle = stylelowered!
end type

type st_dova from statictext within w_trova
integer x = 46
integer y = 140
integer width = 251
integer height = 60
integer textsize = -8
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
boolean enabled = false
string text = "Dove:"
alignment alignment = right!
boolean focusrectangle = false
end type

type ddlb_dove from dropdownlistbox within w_trova
integer x = 320
integer y = 140
integer width = 686
integer height = 300
integer taborder = 20
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean vscrollbar = true
string item[] = {"Campo intero","Inizio campo","Parte del campo"}
borderstyle borderstyle = stylelowered!
end type

type rb_su from radiobutton within w_trova
integer x = 1097
integer y = 240
integer width = 247
integer height = 72
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Su"
boolean lefttext = true
end type

type rb_giu from radiobutton within w_trova
integer x = 1097
integer y = 320
integer width = 247
integer height = 72
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Giù"
boolean checked = true
boolean lefttext = true
end type

type gb_direzione from groupbox within w_trova
integer x = 1051
integer y = 160
integer width = 663
integer height = 280
integer taborder = 40
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Direzione"
end type

type cbx_case from checkbox within w_trova
integer x = 1051
integer y = 480
integer width = 663
integer height = 60
integer taborder = 50
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Maiuscole/Minuscole"
boolean lefttext = true
end type

type st_campi from statictext within w_trova
integer x = 46
integer y = 240
integer width = 251
integer height = 60
integer textsize = -8
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
boolean enabled = false
string text = "Cerca in:"
alignment alignment = right!
boolean focusrectangle = false
end type

type lb_campi from listbox within w_trova
integer x = 320
integer y = 240
integer width = 686
integer height = 300
integer taborder = 30
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean vscrollbar = true
boolean sorted = false
boolean multiselect = true
borderstyle borderstyle = stylelowered!
end type

