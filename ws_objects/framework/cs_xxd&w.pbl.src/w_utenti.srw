﻿$PBExportHeader$w_utenti.srw
$PBExportComments$Finestra Gestione Utenti
forward
global type w_utenti from w_cs_xx_principale
end type
type cb_dashboard from commandbutton within w_utenti
end type
type cb_annulla_password from commandbutton within w_utenti
end type
type cb_imposta_password from commandbutton within w_utenti
end type
type dw_utenti_lista from uo_cs_xx_dw within w_utenti
end type
type dw_utenti_det from uo_cs_xx_dw within w_utenti
end type
type dw_utenti_det_mail from uo_cs_xx_dw within w_utenti
end type
type dw_folder from u_folder within w_utenti
end type
type cb_parametri from commandbutton within w_utenti
end type
type dw_folder_ricerca from u_folder within w_utenti
end type
type dw_ricerca from u_dw_search within w_utenti
end type
end forward

global type w_utenti from w_cs_xx_principale
integer width = 2638
integer height = 2456
string title = "Gestione Utenti"
cb_dashboard cb_dashboard
cb_annulla_password cb_annulla_password
cb_imposta_password cb_imposta_password
dw_utenti_lista dw_utenti_lista
dw_utenti_det dw_utenti_det
dw_utenti_det_mail dw_utenti_det_mail
dw_folder dw_folder
cb_parametri cb_parametri
dw_folder_ricerca dw_folder_ricerca
dw_ricerca dw_ricerca
end type
global w_utenti w_utenti

type variables


string				is_sql_base
end variables

forward prototypes
public subroutine wf_annulla ()
end prototypes

public subroutine wf_annulla ();string				ls_null

setnull(ls_null)

dw_ricerca.setitem(1, "cod_utente", ls_null)
dw_ricerca.setitem(1, "username", ls_null)
dw_ricerca.setitem(1, "nome_cognome", ls_null)
dw_ricerca.setitem(1, "e_mail", ls_null)
dw_ricerca.setitem(1, "flag_tipo_utente", ls_null)
dw_ricerca.setitem(1, "cod_cliente", ls_null)
dw_ricerca.setitem(1, "cod_fornitore", ls_null)

end subroutine

event pc_setwindow;call super::pc_setwindow;windowobject			lw_oggetti[], lw_vuoto[]


lw_oggetti[1] = dw_ricerca
dw_folder_ricerca.fu_assigntab(1, "R", lw_oggetti[])

lw_oggetti[1] = dw_utenti_lista
dw_folder_ricerca.fu_assigntab(2, "L", lw_oggetti[])

dw_folder_ricerca.fu_folderoptions(dw_folder.c_defaultheight, dw_folder.c_foldertableft)
dw_folder_ricerca.fu_foldercreate(2, 2)
dw_folder_ricerca.fu_selecttab(1)


//dw_utenti_lista.set_dw_options(sqlca, &
//                               pcca.null_object, &
//                               c_default, &
//                               c_default)
dw_utenti_lista.set_dw_options(	sqlca, &
													pcca.null_object, &
													c_noretrieveonopen, &
													c_default)

dw_utenti_det.set_dw_options(sqlca, &
                             dw_utenti_lista, &
                             c_sharedata + c_scrollparent, &
                             c_default)
									  
dw_utenti_det_mail.set_dw_options(sqlca, &
                             dw_utenti_lista, &
                             c_sharedata + c_scrollparent, &
                             c_default)
									  
iuo_dw_main = dw_utenti_lista

lw_oggetti[] = lw_vuoto[]
lw_oggetti[1] = dw_utenti_det
lw_oggetti[2] = cb_imposta_password
lw_oggetti[3] = cb_annulla_password
lw_oggetti[4] = cb_parametri
dw_folder.fu_assigntab(1, "Generale", lw_oggetti[])


lw_oggetti[] = lw_vuoto[]
lw_oggetti[1] = dw_utenti_det_mail
dw_folder.fu_assigntab(2, "Mail", lw_oggetti[])

dw_folder.fu_foldercreate(2, 4)
dw_folder.fu_selecttab(1)									  


is_sql_base = dw_utenti_lista.getsqlselect()


if s_cs_xx.cod_utente = "CS_SYSTEM" then
	dw_utenti_det.object.b_password.visible = true
end if


									  
									  

end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_utenti_det, &
                 "cod_azienda", &
                 sqlca, &
                 "aziende", &
                 "cod_azienda", &
                 "rag_soc_1", &
                 "(flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")")
					  
f_po_loaddddw_dw(dw_utenti_det, &
                 "cod_lingua", &
                 sqlca, &
                 "tab_lingue", &
                 "cod_lingua", &
                 "des_lingua", &
                 "(flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")")
end event

on w_utenti.create
int iCurrent
call super::create
this.cb_dashboard=create cb_dashboard
this.cb_annulla_password=create cb_annulla_password
this.cb_imposta_password=create cb_imposta_password
this.dw_utenti_lista=create dw_utenti_lista
this.dw_utenti_det=create dw_utenti_det
this.dw_utenti_det_mail=create dw_utenti_det_mail
this.dw_folder=create dw_folder
this.cb_parametri=create cb_parametri
this.dw_folder_ricerca=create dw_folder_ricerca
this.dw_ricerca=create dw_ricerca
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_dashboard
this.Control[iCurrent+2]=this.cb_annulla_password
this.Control[iCurrent+3]=this.cb_imposta_password
this.Control[iCurrent+4]=this.dw_utenti_lista
this.Control[iCurrent+5]=this.dw_utenti_det
this.Control[iCurrent+6]=this.dw_utenti_det_mail
this.Control[iCurrent+7]=this.dw_folder
this.Control[iCurrent+8]=this.cb_parametri
this.Control[iCurrent+9]=this.dw_folder_ricerca
this.Control[iCurrent+10]=this.dw_ricerca
end on

on w_utenti.destroy
call super::destroy
destroy(this.cb_dashboard)
destroy(this.cb_annulla_password)
destroy(this.cb_imposta_password)
destroy(this.dw_utenti_lista)
destroy(this.dw_utenti_det)
destroy(this.dw_utenti_det_mail)
destroy(this.dw_folder)
destroy(this.cb_parametri)
destroy(this.dw_folder_ricerca)
destroy(this.dw_ricerca)
end on

event close;call super::close;s_cs_xx.menu = 1
end event

type cb_dashboard from commandbutton within w_utenti
integer x = 1957
integer y = 2228
integer width = 425
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Dashboard"
end type

event clicked;//s_cs_xx.parametri.parametro_s_1 = dw_utenti_lista.getitemstring(dw_utenti_lista.getrow(), "cod_utente")

//window_open(w_utenti_dashboard, -1)
window_open_parm(w_utenti_dashboard, -1, dw_utenti_lista)
end event

type cb_annulla_password from commandbutton within w_utenti
integer x = 736
integer y = 2228
integer width = 585
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla Password"
end type

event clicked;string ls_cod_utente
long ll_current_row

ls_cod_utente = dw_utenti_det.getitemstring(dw_utenti_det.getrow(),"cod_utente")

if g_mb.messagebox("Apice","Sei sicuro di voler annullare la password dell'utente " + ls_cod_utente + "?",question!,okcancel!) = 2 then return

update utenti
set password=null
where cod_utente=:ls_cod_utente;

if sqlca.sqlcode<0 then
	g_mb.messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	rollback;
	return
end if

commit;

g_mb.messagebox("Apice","La password è stata annullata!",information!)

// stefanop 27/08/2010: ticket 2010/225: ricarico la lista altrimenti da errore se faccio un'altra modifica
ll_current_row = dw_utenti_lista.getrow()
dw_utenti_lista.event pcd_retrieve(0,0)
dw_utenti_lista.selectrow(0, false)
dw_utenti_lista.selectrow(ll_current_row, true)
dw_utenti_lista.setrow(ll_current_row)
// ----
end event

type cb_imposta_password from commandbutton within w_utenti
integer x = 1353
integer y = 2228
integer width = 585
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "&Imposta Password"
end type

event clicked;string ls_cod_utente

s_cs_xx.parametri.parametro_b_1 = false

window_open(w_imposta_pwd,0)

if s_cs_xx.parametri.parametro_b_1 = true then
	dw_utenti_det.setitem(dw_utenti_det.getrow(),"password",s_cs_xx.parametri.parametro_s_1)
	// stefanop 15/07/2010
	dw_utenti_det.setitem(dw_utenti_det.getrow(), "flag_pwd_provvisoria", s_cs_xx.parametri.parametro_s_2)
	dw_utenti_det.accepttext()
	
	setnull(s_cs_xx.parametri.parametro_s_1)
	setnull(s_cs_xx.parametri.parametro_s_2)
	//ls_cod_utente = dw_utenti_det.getitemstring(dw_utenti_det.getrow(),"cod_utente")
	
//	update utenti
//	set password=:s_cs_xx.parametri.parametro_s_1
//	where cod_utente=:ls_cod_utente;
//	
//	if sqlca.sqlcode<0 then
//		messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
//		return
//	end if
	
end if 
end event

type dw_utenti_lista from uo_cs_xx_dw within w_utenti
integer x = 174
integer y = 28
integer width = 2318
integer height = 776
integer taborder = 10
string dataobject = "d_utenti_lista"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;long				ll_errore
string				ls_sql, ls_valore


dw_ricerca.accepttext()

ls_sql = is_sql_base

//questa la metto per evitare di controllare ogni volta in seguito se mettere where o and nelle varie condizioni di ricerca


choose case upper( left(sqlca.DBMS,2) )
	case "OR","O9","O1"
		ls_sql += " where length(cod_utente) > 0 "
	case else
		ls_sql += " where len(cod_utente) > 0 "
end choose

ls_valore = dw_ricerca.getitemstring(1, "cod_utente")
if ls_valore<>"" and not isnull(ls_valore) then &
	ls_sql += " and cod_utente like '%"+ls_valore+"%'"

ls_valore = dw_ricerca.getitemstring(1, "username")
if ls_valore<>"" and not isnull(ls_valore) then &
	ls_sql += " and username like '%"+ls_valore+"%'"

ls_valore = dw_ricerca.getitemstring(1, "nome_cognome")
if ls_valore<>"" and not isnull(ls_valore) then &
	ls_sql += " and nome_cognome like '%"+ls_valore+"%'"

ls_valore = dw_ricerca.getitemstring(1, "e_mail")
if ls_valore<>"" and not isnull(ls_valore) then &
	ls_sql += " and e_mail like '%"+ls_valore+"%'"

ls_valore = dw_ricerca.getitemstring(1, "flag_tipo_utente")
if ls_valore<>"" and not isnull(ls_valore) and ls_valore<>"X" then &
			ls_sql += " and flag_tipo_utente = '"+ls_valore+"'"

ls_valore = dw_ricerca.getitemstring(1, "cod_cliente")
if ls_valore<>"" and not isnull(ls_valore) then &
	ls_sql += " and cod_cliente = '"+ls_valore+"'"

ls_valore = dw_ricerca.getitemstring(1, "cod_fornitore")
if ls_valore<>"" and not isnull(ls_valore) then &
	ls_sql += " and cod_fornitore = '"+ls_valore+"'"

ls_sql += " order by cod_utente"

dw_utenti_lista.setsqlselect(ls_sql)

ll_errore = dw_utenti_lista.retrieve()

if ll_errore < 0 then
   pcca.error = c_fatal
end if

dw_folder_ricerca.fu_selecttab(2)


end event

event pcd_save;call super::pcd_save;cb_imposta_password.enabled = false
cb_annulla_password.enabled = true
cb_parametri.enabled = true
end event

event pcd_view;call super::pcd_view;cb_imposta_password.enabled = false
cb_annulla_password.enabled = true
cb_parametri.enabled = true

// stefanop 27/08/2010: ticket 2010/225
dw_utenti_det.object.b_cliente.enabled = false
dw_utenti_det.object.b_fornitore.enabled = false
// ----
end event

event pcd_new;call super::pcd_new;cb_imposta_password.enabled = true
cb_annulla_password.enabled = false
cb_parametri.enabled = false

// stefanop 27/08/2010: ticket 2010/225
dw_utenti_det.object.b_cliente.enabled = true
dw_utenti_det.object.b_fornitore.enabled = true
// ----
end event

event pcd_modify;call super::pcd_modify;cb_imposta_password.enabled = true
cb_annulla_password.enabled = false
cb_parametri.enabled = false

// stefanop 27/08/2010: ticket 2010/225
dw_utenti_det.object.b_cliente.enabled = true
dw_utenti_det.object.b_fornitore.enabled = true
// ----
end event

type dw_utenti_det from uo_cs_xx_dw within w_utenti
integer x = 270
integer y = 988
integer width = 1943
integer height = 1200
integer taborder = 20
string dataobject = "d_utenti_det"
boolean border = false
boolean hsplitscroll = true
end type

event itemchanged;call super::itemchanged;long 	 ll_i


if isnull(i_coltext) then
	return 0
end if

for ll_i = 1 to rowcount()
		
	if ll_i <> row and getitemstring(ll_i,"username") = i_coltext then
		g_mb.messagebox("APICE","Lo username " + i_coltext + " è utilizzato in più di un utente!",exclamation!)
		return 1
	end if
	
next
end event

event clicked;call super::clicked;choose case dwo.name
	case "b_cliente"
		setnull(s_cs_xx.parametri.parametro_uo_dw_1)
		setnull(s_cs_xx.parametri.parametro_uo_dw_search)
		guo_ricerca.uof_ricerca_cliente(dw_utenti_det,"cod_cliente")
		
	case "b_fornitore"
		setnull(s_cs_xx.parametri.parametro_uo_dw_1)
		setnull(s_cs_xx.parametri.parametro_uo_dw_search)
		guo_ricerca.uof_set_response()
		guo_ricerca.uof_ricerca_fornitore(dw_utenti_det,"cod_fornitore")
	
end choose
end event

event buttonclicked;call super::buttonclicked;string				ls_password, ls_password_decrypt, ls_cod_utente
n_cst_crypto 	lnv_crypt


if row>0 then
else
	return
end if


choose case dwo.name
	case "b_password"
		
		//imposta il limit a 255
		//s_cs_xx.parametri.parametro_d_10 = 9998
		
		//ridimensiona la finestrella e togli la proprietà ACCEPT al tasto OK
		//s_cs_xx.parametri.parametro_d_11 = 6666

		ls_password = getitemstring(row, "password")
		ls_cod_utente = getitemstring(row, "cod_utente")
		
		if ls_cod_utente="" or isnull(ls_cod_utente) then return
		
		//-----------------------------------------------------------------------------
		if ls_password="" or isnull(ls_password) then
			ls_password = "<la password è vuota!!!>"
			ls_password_decrypt = "Password vuota"
		else
			
			lnv_crypt = create n_cst_crypto
			if lnv_crypt.decryptdata(ls_password, ls_password_decrypt) < 0 then
				ls_password_decrypt = "Caratteri non consentiti (usare 0..9, lettere oppure ! # $ % & ( ) * + , - . / : ; < > = ? @ [ ] \ _ | { })"
			end if
			destroy lnv_crypt
			
		end if
		
		ls_password += "~r~n"+ls_password_decrypt
		
		//-----------------------------------------------------------------------------
		
		openwithparm(w_inserisci_altro_valore, ls_password)
		
		
	
end choose



end event

type dw_utenti_det_mail from uo_cs_xx_dw within w_utenti
integer x = 270
integer y = 988
integer width = 2080
integer height = 940
integer taborder = 30
string dataobject = "d_utenti_det_mail"
boolean border = false
end type

event itemchanged;call super::itemchanged;long 	 ll_i


if isnull(i_coltext) then
	return 0
end if

for ll_i = 1 to rowcount()
		
	if ll_i <> row and getitemstring(ll_i,"username") = i_coltext then
		g_mb.messagebox("APICE","Lo username " + i_coltext + " è utilizzato in più di un utente!",exclamation!)
		return 1
	end if
	
next
end event

type dw_folder from u_folder within w_utenti
integer x = 18
integer y = 848
integer width = 2555
integer height = 1480
integer taborder = 30
end type

type cb_parametri from commandbutton within w_utenti
integer x = 293
integer y = 2228
integer width = 425
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Parametri"
end type

event clicked;s_cs_xx.parametri.parametro_s_1 = dw_utenti_lista.getitemstring(dw_utenti_lista.getrow(), "cod_utente")
// stefanop 23/06/2010: corretto parametro apertura finestra, il menu contestuale ora appare correttamente
window_open(w_parametri_azienda_utente, -1)
end event

type dw_folder_ricerca from u_folder within w_utenti
integer x = 18
integer y = 8
integer width = 2555
integer height = 820
integer taborder = 40
end type

type dw_ricerca from u_dw_search within w_utenti
integer x = 187
integer y = 36
integer width = 2304
integer height = 752
integer taborder = 20
string dataobject = "d_utenti_sel"
boolean border = false
end type

event buttonclicking;call super::buttonclicking;

choose case dwo.name
	case "b_cerca"
		dw_utenti_lista.change_dw_current()
		parent.postevent("pc_retrieve")
		
		
	case "b_annulla"
		wf_annulla()
		
		
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_ricerca,"cod_cliente")
		
		
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_ricerca,"cod_fornitore")
		
end choose
end event

