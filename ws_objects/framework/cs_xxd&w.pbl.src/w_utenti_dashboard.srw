﻿$PBExportHeader$w_utenti_dashboard.srw
forward
global type w_utenti_dashboard from w_cs_xx_principale
end type
type dw_lista from uo_cs_xx_dw within w_utenti_dashboard
end type
end forward

global type w_utenti_dashboard from w_cs_xx_principale
integer width = 2624
integer height = 2228
string title = "Dashboard per Utente"
dw_lista dw_lista
end type
global w_utenti_dashboard w_utenti_dashboard

type variables

end variables

on w_utenti_dashboard.create
int iCurrent
call super::create
this.dw_lista=create dw_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_lista
end on

on w_utenti_dashboard.destroy
call super::destroy
destroy(this.dw_lista)
end on

event pc_setwindow;call super::pc_setwindow;
dw_lista.set_dw_key("cod_utente")

dw_lista.set_dw_options(	sqlca, &
                                   		i_openparm, &
                                    	c_scrollparent, &
                                   		c_default + &
                                    	c_nohighlightselected + c_ViewModeBorderUnchanged + c_CursorRowPointer)

iuo_dw_main = dw_lista




DataWindowChild				dwc_1

dw_lista.GetChild("dashboard_id", dwc_1)
dwc_1.SetTransObject(SQLCA)
dwc_1.Retrieve()
end event

event pc_setddlb;call super::pc_setddlb;//
//
//f_po_loaddddw_dw(			  dw_lista, &
//									  "dashboard_id", &
//									  sqlca, &
//									  "tab_dashboard", &
//									  "id", &
//									  "titolo", &
//									  "1 = 1")
end event

type dw_lista from uo_cs_xx_dw within w_utenti_dashboard
integer x = 23
integer y = 20
integer width = 2537
integer height = 2080
integer taborder = 10
string dataobject = "d_utenti_dashboard"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;
string				ls_cod_utente, ls_nome, ls_username, ls_titolo
long				ll_errore


ls_cod_utente = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_utente")

select nome_cognome, username
into	:ls_nome, :ls_username
from utenti
where cod_utente=:ls_cod_utente;

ls_titolo = "Dashboard per Utente"

if ls_nome<>"" and not isnull(ls_nome) then ls_titolo+= " "+ls_nome
ls_titolo += " ("+ls_username+")"

parent.title = ls_titolo

ll_errore = retrieve(ls_cod_utente)

if ll_errore < 0 then
   pcca.error = c_fatal
end if


end event

event pcd_setkey;call super::pcd_setkey;long			ll_i
string			ls_cod_utente

ls_cod_utente = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_utente")

for ll_i = 1 to rowcount()
	if isnull(getitemstring(ll_i, "cod_utente")) or getitemstring(ll_i, "cod_utente") = "" then
		setitem(ll_i, "cod_utente", ls_cod_utente)
	end if
next

end event

