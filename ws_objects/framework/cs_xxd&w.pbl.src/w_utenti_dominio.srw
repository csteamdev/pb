﻿$PBExportHeader$w_utenti_dominio.srw
$PBExportComments$Finestra Gestione Utenti Dominio
forward
global type w_utenti_dominio from w_cs_xx_principale
end type
type cb_utenti_dominio from commandbutton within w_utenti_dominio
end type
type dw_utenti_dominio from uo_cs_xx_dw within w_utenti_dominio
end type
end forward

global type w_utenti_dominio from w_cs_xx_principale
integer width = 2423
integer height = 1536
string title = "Gestione Utenti/Utenti Dominio"
event we_set_dddw_filter ( )
cb_utenti_dominio cb_utenti_dominio
dw_utenti_dominio dw_utenti_dominio
end type
global w_utenti_dominio w_utenti_dominio

type variables

end variables

forward prototypes
public subroutine wf_dddw_filter ()
public subroutine wf_dddw_filter (string as_data)
end prototypes

event we_set_dddw_filter();wf_dddw_filter()
end event

public subroutine wf_dddw_filter ();string ls_cod_utente[], ls_filter
long ll_i
DataWindowChild ldw_child

ls_filter = ""

dw_utenti_dominio.accepttext()

for ll_i = 1 to dw_utenti_dominio.rowcount()

	if not isnull(dw_utenti_dominio.getitemstring(ll_i, "cod_utente")) then
		
		ls_cod_utente[upperbound(ls_cod_utente) + 1] = dw_utenti_dominio.getitemstring(ll_i, "cod_utente")
		
	end if
	
next

if upperbound(ls_cod_utente) > 0 then
	ls_filter = "data_column NOT IN ('" + guo_functions.uof_implode(ls_cod_utente, "','") + "')"
end if

dw_utenti_dominio.getchild("cod_utente", ref ldw_child)

if ldw_child.setfilter(ls_filter) < 1 then g_mb.error("Errore durante l'impostazione del filtro")
if ldw_child.filter() < 1 then g_mb.error("Errore drante il filter della dw")
end subroutine

public subroutine wf_dddw_filter (string as_data);string ls_cod_utente[], ls_filter
long ll_i
DataWindowChild ldw_child

ls_filter = ""

ldw_child.setredraw(false)

if not isnull(as_data) and as_data <> ""  then
	ls_filter = "data_column LIKE '%" + as_data + "%'"
end if

dw_utenti_dominio.getchild("cod_utente", ref ldw_child)

if ldw_child.setfilter(ls_filter) < 1 then g_mb.error("Errore durante l'impostazione del filtro")
if ldw_child.filter() < 1 then g_mb.error("Errore drante il filter della dw")
ldw_child.sort()

ldw_child.setredraw(true)
end subroutine

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_utenti_dominio, &
                 "cod_utente", &
                 sqlca, &
                 "utenti", &
                 "cod_utente", &
                 "nome_cognome", &
                 "(flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")")



//postevent("we_set_dddw_filter")
end event

event pc_setwindow;call super::pc_setwindow;
dw_utenti_dominio.set_dw_options(sqlca, pcca.null_object, c_default, c_default)

dw_utenti_dominio.object.p_nuovo.filename = s_cs_xx.volume + s_cs_xx.risorse + "new.png"

end event

on w_utenti_dominio.create
int iCurrent
call super::create
this.cb_utenti_dominio=create cb_utenti_dominio
this.dw_utenti_dominio=create dw_utenti_dominio
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_utenti_dominio
this.Control[iCurrent+2]=this.dw_utenti_dominio
end on

on w_utenti_dominio.destroy
call super::destroy
destroy(this.cb_utenti_dominio)
destroy(this.dw_utenti_dominio)
end on

type cb_utenti_dominio from commandbutton within w_utenti_dominio
integer x = 23
integer y = 20
integer width = 731
integer height = 100
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Leggi utenti del dominio"
end type

event clicked;/**
 * stefanop
 * 27/10/11
 *
 * Leggo gli utenti del dominio
 **/
 
string ls_where, ls_utente_dominio
long ll_count, ll_i, ll_row
datetime ldt_creato_il
n_ds_ldapquery lds_ldap

SetPointer(HourGlass!)
dw_utenti_dominio.setredraw(false)

ls_where = "objectClass='user' and objectCategory='person'"
lds_ldap = create n_ds_ldapquery

lds_ldap.DataObject = "d_ds_ldapquery"

ll_count = lds_ldap.of_Retrieve(ls_where)

for ll_i = 1 to ll_count
	
	ldt_creato_il =  lds_ldap.getitemdatetime(ll_i, "createtimestamp")
	ls_utente_dominio = lds_ldap.getitemstring(ll_i, "samaccountname")
	
	// controllo se l'utente dominio esiste già altrimenti lo inserisco
	ll_row = dw_utenti_dominio.find("utente_dominio='" + ls_utente_dominio + "'", 1, ll_count)
	
	if ll_row < 1 then
		
		ll_row = dw_utenti_dominio.insertrow(0)
		dw_utenti_dominio.setitem(ll_row, "utente_dominio", ls_utente_dominio)
		dw_utenti_dominio.setitem(ll_row, "utente_dominio_nuovo", "S")
		
	end if
	
next

destroy lds_ldap

dw_utenti_dominio.setredraw(true)
SetPointer(Arrow!)
end event

type dw_utenti_dominio from uo_cs_xx_dw within w_utenti_dominio
integer x = 23
integer y = 140
integer width = 2290
integer height = 1260
integer taborder = 30
string dataobject = "d_utenti_dominio"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore

ll_errore = retrieve()

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

event pcd_savebefore;call super::pcd_savebefore;/**
 * stefanop
 * 27/10/11
 *
 * Scorro tutte le righe e cancello quelle con l'utente vuoto dato che 
 * è chiave primaria darebbe errore.
 **/
 
string ls_cod_utente
long ll_i

setredraw(false)

for ll_i = 1 to rowcount()
	
	ls_cod_utente = getitemstring(ll_i, "cod_utente")
	
	if isnull(ls_cod_utente) or ls_cod_utente = "" then
		
		deleterow(ll_i)
		ll_i --
		
	end if
	
next

setredraw(true)
end event

event itemchanged;call super::itemchanged;//long ll_row
//
//choose case dwo.name
//		
//	case "cod_utente"
//		if not isnull(data) then
//			
//			ll_row = find("cod_utente='" + data + "'", 1, rowcount())
//			if ll_row > 0 then
//				
//				g_mb.show("Utente", "Attenzione l'utente è già assegnato ad un utente di dominio.")
//				return 1
//				
//			end if
//			
//		end if
//		
//		postevent("we_set_dddw_filter")
//		
//end choose
end event

event editchanged;call super::editchanged;//wf_dddw_filter(data)
end event

