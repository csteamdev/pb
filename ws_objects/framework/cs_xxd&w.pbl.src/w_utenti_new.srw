﻿$PBExportHeader$w_utenti_new.srw
forward
global type w_utenti_new from w_cs_xx_treeview
end type
type dw_1 from uo_cs_xx_dw within det_1
end type
type det_2 from userobject within tab_dettaglio
end type
type dw_2 from uo_cs_xx_dw within det_2
end type
type det_2 from userobject within tab_dettaglio
dw_2 dw_2
end type
end forward

global type w_utenti_new from w_cs_xx_treeview
integer width = 3662
string title = "Ricerca Utenti"
event pc_menu_annulla_password ( )
event pc_menu_imposta_password ( )
event pc_menu_imposta_parametri ( )
event pc_menu_dashboard ( )
event pc_mail_test ( )
end type
global w_utenti_new w_utenti_new

type variables
private:
	long il_livello_corrente
	boolean ib_new
	datastore ids_store
	int ICONA_UTENTE, ICONA_UTENTE_BLOCCATO, ICONA_AZIENDA, ICONA_TIPO_UTENTE
end variables

forward prototypes
public function integer wf_leggi_parent (long al_handle, ref string as_sql)
public function long wf_leggi_livello (long al_handle, integer ai_livello)
public subroutine wf_imposta_ricerca ()
public subroutine wf_treeview_icons ()
public subroutine wf_valori_livelli ()
public function integer wf_inserisci_aziende (long al_handle)
public function integer wf_inserisci_utenti (long al_handle)
public function long wf_inserisci_utente (long al_handle, string as_cod_utente, string as_nome_cognome, boolean ab_bloccato)
public function integer wf_inserisci_tipi_utenti (long al_handle)
end prototypes

event pc_menu_annulla_password();string ls_cod_utente
long ll_current_row

ls_cod_utente = tab_dettaglio.det_1.dw_1.getitemstring(tab_dettaglio.det_1.dw_1.getrow(),"cod_utente")

if g_mb.messagebox("Apice","Sei sicuro di voler annullare la password dell'utente " + ls_cod_utente + "?",question!,okcancel!) = 2 then return

update utenti
set password=null
where cod_utente=:ls_cod_utente;

if sqlca.sqlcode<0 then
	g_mb.messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	rollback;
	return
end if

commit;

g_mb.messagebox("Apice","La password è stata annullata!",information!)

// stefanop 27/08/2010: ticket 2010/225: ricarico la lista altrimenti da errore se faccio un'altra modifica
ll_current_row = tab_dettaglio.det_1.dw_1.getrow()
tab_dettaglio.det_1.dw_1.event pcd_retrieve(0,0)
tab_dettaglio.det_1.dw_1.selectrow(0, false)
tab_dettaglio.det_1.dw_1.selectrow(ll_current_row, true)
tab_dettaglio.det_1.dw_1.setrow(ll_current_row)
// ----
end event

event pc_menu_imposta_password();string ls_cod_utente

s_cs_xx.parametri.parametro_b_1 = false

window_open(w_imposta_pwd,0)

if s_cs_xx.parametri.parametro_b_1 = true then
	tab_dettaglio.det_1.dw_1.setitem(tab_dettaglio.det_1.dw_1.getrow(),"password",s_cs_xx.parametri.parametro_s_1)
	// stefanop 15/07/2010
	tab_dettaglio.det_1.dw_1.setitem(tab_dettaglio.det_1.dw_1.getrow(), "flag_pwd_provvisoria", s_cs_xx.parametri.parametro_s_2)
	tab_dettaglio.det_1.dw_1.accepttext()
	
	setnull(s_cs_xx.parametri.parametro_s_1)
	setnull(s_cs_xx.parametri.parametro_s_2)
	//ls_cod_utente = dw_utenti_det.getitemstring(dw_utenti_det.getrow(),"cod_utente")
	
//	update utenti
//	set password=:s_cs_xx.parametri.parametro_s_1
//	where cod_utente=:ls_cod_utente;
//	
//	if sqlca.sqlcode<0 then
//		messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
//		return
//	end if
	
end if 
end event

event pc_menu_imposta_parametri();s_cs_xx.parametri.parametro_s_1 = tab_dettaglio.det_1.dw_1.getitemstring(tab_dettaglio.det_1.dw_1.getrow(), "cod_utente")
// stefanop 23/06/2010: corretto parametro apertura finestra, il menu contestuale ora appare correttamente
window_open(w_parametri_azienda_utente, -1)
end event

event pc_menu_dashboard();window_open_parm(w_utenti_dashboard, -1, tab_dettaglio.det_1.dw_1)
end event

event pc_mail_test();// invio mail di test dell'utente

boolean 		lb_result
string			ls_destinatari[], ls_error,ls_from,ls_sender_name,ls_smtp_server,ls_smtp_usd,ls_smtp_pwd, ls_cod_utente
integer		li_ret

ls_cod_utente = tab_dettaglio.det_1.dw_1.getitemstring(tab_dettaglio.det_1.dw_1.getrow(),"cod_utente")


//recupero info principali per inviare email (sender, smtp, user_smtp, pwd_smtp)
SELECT	e_mail,
			sender_name,
			smtp_server,
			smtp_usr,
			smtp_pwd
into		:ls_from,
			:ls_sender_name,
			:ls_smtp_server,
			:ls_smtp_usd,
			:ls_smtp_pwd
from utenti
where cod_utente=:ls_cod_utente;

if sqlca.sqlcode < 0 then
	//errore
	g_mb.error("Errore in lettura valori SMTP in tabella utenti: "+sqlca.sqlerrtext + " PROCESSO TERMINATO")
	return
	
elseif sqlca.sqlcode = 100 then
	//utente non trovato in tabella
	g_mb.error("Utente con codice '"+s_cs_xx.cod_utente+"' non trovato in tabella utenti!  PROCESSO TERMINATO")
	return
end if

if isnull(ls_from) or ls_from="" then
	g_mb.error("E-mail non specificata per l'utente con codice '"+s_cs_xx.cod_utente+"!  PROCESSO TERMINATO")
	return
end if

if isnull(ls_smtp_server) or ls_smtp_server="" then
	g_mb.error("Indirizzo server SMTP non specificato per l'utente con codice '"+s_cs_xx.cod_utente+"!  PROCESSO TERMINATO")
	return
end if

if isnull(ls_sender_name) then ls_sender_name = "<Sender>"
if isnull(ls_smtp_usd) then ls_smtp_usd = ""
if isnull(ls_smtp_pwd) then ls_smtp_pwd = ""


uo_sendmail luo_sendmail
luo_sendmail = create uo_sendmail

ls_destinatari[1]=ls_from

luo_sendmail.uof_set_from(ls_from)
luo_sendmail.uof_set_to(ls_destinatari[])
//luo_sendmail.uof_set_cc(as_cc[])
//luo_sendmail.uof_set_bcc(as_bcc[])
//luo_sendmail.uof_set_attachments(as_attachment[])
luo_sendmail.uof_set_subject("Mail Test CSTEAM")
luo_sendmail.uof_set_message("Questa è una mail di test generata dalla gestione utenti di Apice/Omnia (Csteam srl)")
luo_sendmail.uof_set_smtp(ls_smtp_server, ls_smtp_usd, ls_smtp_pwd)
luo_sendmail.uof_set_html( true )

lb_result = luo_sendmail.uof_send()
ls_error = luo_sendmail.uof_get_error()

if lb_result then
	g_mb.show("Mail inviata con successo!")
else
	g_mb.Error("Errore invio mail~r~n"+ls_error)
end if
return


end event

public function integer wf_leggi_parent (long al_handle, ref string as_sql);treeviewitem ltv_item
str_treeview lstr_data

if al_handle = 0 then return 0

do
	
	tab_ricerca.selezione.tv_selezione.getitem(al_handle, ltv_item)
		
	lstr_data = ltv_item.data
	
	choose case lstr_data.tipo_livello		
			
		case "A"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND utenti.cod_azienda is null "
			else
				as_sql += " AND utenti.cod_azienda = '" + lstr_data.codice + "' "
			end if
				
		case "T"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND utenti.cod_tipo_utente is null "
			else
				as_sql += " AND utenti.cod_tipo_utente = '" + lstr_data.codice + "' "
			end if
				
	end choose
	
	al_handle = tab_ricerca.selezione.tv_selezione.finditem(parenttreeitem!, al_handle)
	
loop while al_handle <> -1

return 0
end function

public function long wf_leggi_livello (long al_handle, integer ai_livello);if ai_livello > 1 then
	return wf_inserisci_utenti(al_handle)
end if

il_livello_corrente = ai_livello

choose case tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "livello_" + string(ai_livello))
		
	case "A" // Azienda
		return wf_inserisci_aziende(al_handle)
		
	case "T" // Tipi Utenti
		return wf_inserisci_tipi_utenti(al_handle)

	case else
		return wf_inserisci_utenti(al_handle)
end choose
end function

public subroutine wf_imposta_ricerca ();string ls_var

is_sql_filtro = ""

// leggo filtri
ls_var = tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "cod_utente")
if not isnull(ls_var) and ls_var <> "" then is_sql_filtro += " AND utenti.cod_utente='" + ls_var + "' "

ls_var = tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "cod_azienda")
if not isnull(ls_var) and ls_var <> "" then is_sql_filtro += " AND utenti.cod_azienda='" + ls_var + "' "


// ----
end subroutine

public subroutine wf_treeview_icons ();ICONA_UTENTE = wf_treeview_add_icon("treeview\cliente.png")
ICONA_UTENTE_BLOCCATO = wf_treeview_add_icon("treeview\cliente_bloccato.png")
ICONA_AZIENDA = wf_treeview_add_icon("treeview\deposito.png")
ICONA_TIPO_UTENTE = wf_treeview_add_icon("treeview\gruppo.png")

end subroutine

public subroutine wf_valori_livelli ();wf_add_valore_livello("Non Specificato", "N")
wf_add_valore_livello("Azienda", "A")
wf_add_valore_livello("Tipo Utente", "T")
end subroutine

public function integer wf_inserisci_aziende (long al_handle);string ls_sql, ls_error, ls_cod_azienda, ls_rag_soc
long ll_rows, ll_i
treeviewitem ltvi_item
str_treeview lstr_data

ls_sql = "SELECT DISTINCT aziende.cod_azienda, aziende.rag_soc_1 FROM utenti left outer join aziende on utenti.cod_azienda = aziende.cod_azienda WHERE 1=1 " + is_sql_filtro
	
wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)

if ll_rows < 0 then
	g_mb.error(ls_error, sqlca)
	return -1
elseif ll_rows = 0 then
	return 0
end if

for ll_i = 1 to ll_rows
	ltvi_item = wf_new_item(true, ICONA_AZIENDA)
	
	ls_cod_azienda =  ids_store.getitemstring(ll_i, 1)
	ls_rag_soc =  ids_store.getitemstring(ll_i, 2)
	
	lstr_data.tipo_livello = "A"
	lstr_data.codice = ls_cod_azienda
	lstr_data.livello = il_livello_corrente
	
	if isnull(ls_cod_azienda) and isnull(ls_rag_soc)  then
		ltvi_item.label = "Azienda Mancante"
	elseif isnull(ls_rag_soc) then
		ltvi_item.label = ls_cod_azienda
	else
		ltvi_item.label = ls_cod_azienda + " - " + ls_rag_soc
	end if
	
	ltvi_item.data = lstr_data
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

setnull(ids_store)
return ll_rows
end function

public function integer wf_inserisci_utenti (long al_handle);string ls_sql, ls_error, ls_cod_utente, ls_nome_cognome, ls_label
long ll_rows, ll_i
boolean lb_bloccato
treeviewitem ltvi_item

ls_sql = "SELECT DISTINCT utenti.cod_utente, utenti.nome_cognome, utenti.flag_blocco FROM utenti WHERE 1 = 1 " + is_sql_filtro
	
wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)

if ll_rows < 0 then
	g_mb.error(ls_error, sqlca)
	return -1
elseif ll_rows = 0 then
	return 0
end if

for ll_i = 1 to ll_rows
	
	if (ids_store.getitemstring(ll_i, "flag_blocco")) = 'S' then
		lb_bloccato = true
	else
		lb_bloccato = false
	end if
	
	wf_inserisci_utente(al_handle, ids_store.getitemstring(ll_i, 1), ids_store.getitemstring(ll_i, 2), lb_bloccato)
next

setnull(ids_store)
return ll_rows
end function

public function long wf_inserisci_utente (long al_handle, string as_cod_utente, string as_nome_cognome, boolean ab_bloccato);string ls_label
treeviewitem ltvi_item
str_treeview lstr_data

lstr_data.tipo_livello = "U"
lstr_data.codice = as_cod_utente
lstr_data.livello = il_livello_corrente

ls_label = as_cod_utente + " - "

if len ( as_nome_cognome ) > 0 then
	ls_label += as_nome_cognome
else
	ls_label += "Nome/Cognome mancante"
end if

if ab_bloccato = false then
	ltvi_item = wf_new_item(ab_bloccato, ICONA_UTENTE)
else
	ltvi_item = wf_new_item(ab_bloccato, ICONA_UTENTE)
end if
	
ltvi_item.label = ls_label
ltvi_item.data = lstr_data
return tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)

end function

public function integer wf_inserisci_tipi_utenti (long al_handle);string ls_sql, ls_error, ls_cod_tipo_utente, ls_des_tipo_utente
long ll_rows, ll_i
treeviewitem ltvi_item
str_treeview lstr_data

ls_sql = "SELECT DISTINCT utenti.cod_tipo_utente, tab_tipi_utenti.des_tipo_utente FROM utenti left outer join tab_tipi_utenti on utenti.cod_tipo_utente = tab_tipi_utenti.cod_tipo_utente where 1=1 " + is_sql_filtro
	
wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)

if ll_rows < 0 then
	g_mb.error(ls_error, sqlca)
	return -1
elseif ll_rows = 0 then
	return 0
end if

for ll_i = 1 to ll_rows
	ltvi_item = wf_new_item(true, ICONA_TIPO_UTENTE)
	
	ls_cod_tipo_utente =  ids_store.getitemstring(ll_i, 1)
	ls_des_tipo_utente =  ids_store.getitemstring(ll_i, 2)
	
	lstr_data.tipo_livello = "T"
	lstr_data.codice = ls_cod_tipo_utente
	lstr_data.livello = il_livello_corrente
	
	if isnull(ls_cod_tipo_utente) and isnull(ls_des_tipo_utente)  then
		ltvi_item.label = "Tipo Utente Mancante"
	elseif isnull(ls_des_tipo_utente) then
		ltvi_item.label = ls_cod_tipo_utente
	else
		ltvi_item.label = ls_cod_tipo_utente + " - " + ls_des_tipo_utente
	end if
	
	ltvi_item.data = lstr_data
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

setnull(ids_store)
return ll_rows
end function

on w_utenti_new.create
int iCurrent
call super::create
end on

on w_utenti_new.destroy
call super::destroy
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(tab_dettaglio.det_1.dw_1, &
                 "cod_azienda", &
                 sqlca, &
                 "aziende", &
                 "cod_azienda", &
                 "rag_soc_1", &
                 "(flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")")
		
f_po_loaddddw_dw(tab_dettaglio.det_1.dw_1, &
                 "cod_tipo_utente", &
                 sqlca, &
                 "tab_tipi_utenti", &
                 "cod_tipo_utente", &
                 "des_tipo_utente", &
                 "" )
	
end event

event pc_setwindow;call super::pc_setwindow;is_codice_filtro = "USR"
tab_dettaglio.det_1.dw_1.set_dw_key("cod_azienda")
tab_dettaglio.det_1.dw_1.set_dw_options(sqlca,pcca.null_object,c_default,c_default)
tab_dettaglio.det_2.dw_2.set_dw_options(sqlca, tab_dettaglio.det_1.dw_1, c_sharedata+c_scrollparent,c_default)

iuo_dw_main = tab_dettaglio.det_1.dw_1

tab_dettaglio.det_1.dw_1.ib_dw_detail = true
tab_dettaglio.det_2.dw_2.ib_dw_detail = true

if s_cs_xx.cod_utente = "CS_SYSTEM" then
	tab_dettaglio.det_1.dw_1.object.b_password.visible = true
end if


end event

event close;call super::close;s_cs_xx.menu = 1
end event

type tab_dettaglio from w_cs_xx_treeview`tab_dettaglio within w_utenti_new
integer x = 1531
integer width = 2080
det_2 det_2
end type

on tab_dettaglio.create
this.det_2=create det_2
call super::create
this.Control[]={this.det_1,&
this.det_2}
end on

on tab_dettaglio.destroy
call super::destroy
destroy(this.det_2)
end on

event tab_dettaglio::clicked;call super::clicked;iuo_dw_main.change_dw_current()
end event

type det_1 from w_cs_xx_treeview`det_1 within tab_dettaglio
integer width = 2043
string text = "Informazioni"
dw_1 dw_1
end type

on det_1.create
this.dw_1=create dw_1
int iCurrent
call super::create
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_1
end on

on det_1.destroy
call super::destroy
destroy(this.dw_1)
end on

type tab_ricerca from w_cs_xx_treeview`tab_ricerca within w_utenti_new
integer width = 1486
end type

on tab_ricerca.create
call super::create
this.Control[]={this.ricerca,&
this.selezione}
end on

on tab_ricerca.destroy
call super::destroy
end on

type ricerca from w_cs_xx_treeview`ricerca within tab_ricerca
integer width = 1449
end type

type dw_ricerca from w_cs_xx_treeview`dw_ricerca within ricerca
integer width = 1440
string dataobject = "d_utenti_ricerca_new"
end type

event dw_ricerca::buttonclicked;call super::buttonclicked;choose case dwo.name
		
	case "b_ricerca_codice"
		guo_ricerca.uof_ricerca_utente(dw_ricerca, "cod_utente")
		
	case "b_ricerca_azienda"
		guo_ricerca.uof_ricerca_azienda(dw_ricerca, "cod_azienda")
				
end choose
end event

type selezione from w_cs_xx_treeview`selezione within tab_ricerca
integer width = 1449
end type

type tv_selezione from w_cs_xx_treeview`tv_selezione within selezione
integer width = 1440
end type

event tv_selezione::selectionchanged;call super::selectionchanged;treeviewitem ltvi_item

if AncestorReturnValue < 0 then return

tab_ricerca.selezione.tv_selezione.getitem(newhandle, ltvi_item)

istr_data = ltvi_item.data
	
tab_dettaglio.det_1.dw_1.change_dw_current()
getwindow().postevent("pc_retrieve")
end event

event tv_selezione::rightclicked;call super::rightclicked;treeviewitem ltvi_item
m_utenti lm_menu
lm_menu = create m_utenti

if AncestorReturnValue < 0 then return

tab_ricerca.selezione.tv_selezione.getitem(handle, ltvi_item)

istr_data = ltvi_item.data
	
tab_dettaglio.det_1.dw_1.change_dw_current()
getwindow().postevent("pc_retrieve")

lm_menu.m_impostapassword.enabled = true
lm_menu.m_annullapassword.enabled = true
lm_menu.m_impostaparametri.enabled = true
lm_menu.popmenu(w_cs_xx_mdi.pointerx(),w_cs_xx_mdi.pointery())

destroy lm_menu

end event

event tv_selezione::itempopulate;call super::itempopulate;treeviewitem ltvi_item
str_treeview lstr_data


if AncestorReturnValue < 0 then return

getitem(handle, ltvi_item)

lstr_data = ltvi_item.data

if wf_leggi_livello(handle, lstr_data.livello + 1) < 1 then
	ltvi_item.children = false
	setitem(handle, ltvi_item)
end if
end event

type dw_1 from uo_cs_xx_dw within det_1
integer x = 27
integer y = 32
integer width = 1989
integer height = 1620
integer taborder = 30
string dataobject = "d_utenti_det_new"
boolean border = false
boolean hsplitscroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore

ll_errore = retrieve(istr_data.codice)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_new;call super::pcd_new;ib_new = true
dw_1.object.b_cliente.enabled = true
dw_1.object.b_fornitore.enabled = true
end event

event updatestart;call super::updatestart;if DeletedCount() > 0 then
	wf_set_deleted_item_status()
end if

if ib_new then
	il_current_handle = wf_inserisci_utente(0, getitemstring(getrow(), "cod_utente"), getitemstring(getrow(), "nome_cognome"), false)
	tab_ricerca.selezione.tv_selezione.selectitem(il_current_handle)
end if
end event

event pcd_delete;call super::pcd_delete;ib_new = false
end event

event pcd_modify;call super::pcd_modify;ib_new = false
dw_1.object.b_cliente.enabled = true
dw_1.object.b_fornitore.enabled = true
end event

event pcd_view;call super::pcd_view;ib_new = false
dw_1.object.b_cliente.enabled = false
dw_1.object.b_fornitore.enabled = false
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_1, "cod_cliente")
	case "b_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_1, "cod_fornitore")
		
	case "b_password"
		string ls_password, ls_cod_utente, ls_password_decrypt
		n_cst_crypto  lnv_crypt
		
		ls_password = getitemstring(row, "password")
		ls_cod_utente = getitemstring(row, "cod_utente")
		
		if ls_cod_utente="" or isnull(ls_cod_utente) then return
		
		//-----------------------------------------------------------------------------
		if ls_password="" or isnull(ls_password) then
			ls_password = "<la password è vuota!!!>"
			ls_password_decrypt = "Password vuota"
		else
			
			lnv_crypt = create n_cst_crypto
			if lnv_crypt.decryptdata(ls_password, ls_password_decrypt) < 0 then
				ls_password_decrypt = "Caratteri non consentiti (usare 0..9, lettere oppure ! # $ % & ( ) * + , - . / : ; < > = ? @ [ ] \ _ | { })"
			end if
			destroy lnv_crypt
			
		end if
		
		ls_password += "~r~n"+ls_password_decrypt
		
		openwithparm(w_inserisci_altro_valore, ls_password)	
		
end choose

end event

type det_2 from userobject within tab_dettaglio
integer x = 18
integer y = 108
integer width = 2043
integer height = 1676
long backcolor = 12632256
string text = "Mail"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_2 dw_2
end type

on det_2.create
this.dw_2=create dw_2
this.Control[]={this.dw_2}
end on

on det_2.destroy
destroy(this.dw_2)
end on

type dw_2 from uo_cs_xx_dw within det_2
integer x = 5
integer y = 12
integer width = 2080
integer height = 920
integer taborder = 11
string dataobject = "d_utenti_det_mail_new"
boolean border = false
end type

