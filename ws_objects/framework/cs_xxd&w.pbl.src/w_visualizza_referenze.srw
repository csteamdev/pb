﻿$PBExportHeader$w_visualizza_referenze.srw
forward
global type w_visualizza_referenze from window
end type
type cbx_1 from checkbox within w_visualizza_referenze
end type
type tv_1 from treeview within w_visualizza_referenze
end type
type cb_ricarica from commandbutton within w_visualizza_referenze
end type
end forward

global type w_visualizza_referenze from window
integer width = 2231
integer height = 1916
boolean titlebar = true
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 12632256
string icon = "AppIcon!"
boolean center = true
cbx_1 cbx_1
tv_1 tv_1
cb_ricarica cb_ricarica
end type
global w_visualizza_referenze w_visualizza_referenze

type variables
constant string TREE_ITEM_PARENT = "P"
constant string TREE_ITEM_CHILD = "C"


private:
	str_referenze istr_data
	uo_espress_reg iuo_regex
	
	int DEFAULT_GROUP_ICON
	int DEFAULT_ITEM_ICON
	
	
	boolean ib_tv_delete = false
end variables

forward prototypes
public subroutine wf_check_references ()
private function integer wf_add_icon (string as_icon_path)
private function integer wf_conta_referenze (str_referenza_item astr_item)
public function integer wf_inserisci_referenze (long al_handle, str_referenza_item_data astr_data_parent, str_referenza_item astr_item)
end prototypes

public subroutine wf_check_references ();/** 
 * stefanop
 **/
 
int li_count, li_i, li_count_referenze, li_icon
str_referenza_item lstr_item
str_referenza_item_data lstr_data
treeviewitem ltvi_treeitem

ib_tv_delete = true
tv_1.DeleteItem(0)
ib_tv_delete = false

li_count = upperbound(istr_data.tabelle)

for li_i = 1 to li_count
	lstr_item = istr_data.tabelle[li_i]
	
	li_count_referenze = wf_conta_referenze(lstr_item)
	
	if cbx_1.checked and li_count_referenze <= 0 then
		continue
	end if
	
	if g_str.isnotempty(lstr_item.groupIcon) then
		li_icon = wf_add_icon(lstr_item.groupIcon)
	else
		li_icon = DEFAULT_GROUP_ICON
	end if
	
	lstr_data.index = li_i
	lstr_data.tipo = TREE_ITEM_PARENT
	
	ltvi_treeitem.label = lstr_item.groupname + " (" + string(li_count_referenze) + ")"
	ltvi_treeitem.children = true
	ltvi_treeitem.data = lstr_data
	ltvi_treeitem.pictureindex = li_icon
	ltvi_treeitem.selectedpictureindex = li_icon
	
	tv_1.insertItemLast(0, ltvi_treeitem)
next
end subroutine

private function integer wf_add_icon (string as_icon_path);/**
 * stefanop
 * 21/03/2016
 *
 * Aggiunge un'icona all'albero
 **/
 
return tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + as_icon_path)
end function

private function integer wf_conta_referenze (str_referenza_item astr_item);/**
 * stefanop 
 * 21/03/2016
 *
 * Esegui il count e ritorna il valore
 **/
 
string ls_sql, ls_error
int li_row_count
datastore lds_store	

ls_sql = "SELECT COUNT(*) FROM " + astr_item.table + " WHERE " + astr_item.where

li_row_count = guo_functions.uof_crea_datastore(lds_store, ls_sql, ls_error)

if li_row_count <= 0 then
	return 0
end if

return lds_store.getitemnumber(1,1)
end function

public function integer wf_inserisci_referenze (long al_handle, str_referenza_item_data astr_data_parent, str_referenza_item astr_item);/**
 * stefanop 
 * 
 **/
 
string ls_sql, ls_error, ls_printf[], ls_row_description, ls_value, ls_find
int li_row_count, li_i, li_j, li_row_dw, li_icon
datastore lds_store
treeviewitem ltvi_item
str_referenza_item_data lstr_item_data

ls_sql = "SELECT " + g_str.implode(astr_item.colonne, ", ") + " FROM " + astr_item.table + " WHERE " + astr_item.where
li_row_count = guo_functions.uof_crea_datastore(lds_store, ls_sql, ls_error)

if li_row_count <= 0 then
	return 0
end if

if g_str.isnotempty(astr_item.itemIcon) then
	li_icon = wf_add_icon(astr_item.itemIcon)
else
	li_icon = DEFAULT_ITEM_ICON
end if

iuo_regex.cerca_array(astr_item.itemName, ls_printf, true)

for li_i = 1 to li_row_count
	
	ls_row_description = astr_item.itemName
	
	for li_j = 1 to upperbound(ls_printf)
		
		// TODO spostare dentro uo_str
		// Sostituzione
		if "%s" = ls_printf[li_j] then
			ls_find = "%s"
			ls_value = lds_store.getitemstring(li_i, astr_item.colonne[li_j])
		elseif "%d" = ls_printf[li_j] then
			ls_find = "%d"
			ls_value = string(lds_store.getitemnumber(li_i, astr_item.colonne[li_j]))
		end if
		
		ls_row_description = g_str.replaceAll(ls_row_description, ls_find, ls_value, false)
	next
	
	lstr_item_data.index = astr_data_parent.index
	lstr_item_data.tipo = TREE_ITEM_CHILD
	
	ltvi_item.label = ls_row_description
	ltvi_item.data = lstr_item_data
	ltvi_item.pictureindex = li_icon
	ltvi_item.selectedpictureindex = li_icon
	
	tv_1.insertItemLast(al_handle, ltvi_item)
next

return li_row_count
end function

on w_visualizza_referenze.create
this.cbx_1=create cbx_1
this.tv_1=create tv_1
this.cb_ricarica=create cb_ricarica
this.Control[]={this.cbx_1,&
this.tv_1,&
this.cb_ricarica}
end on

on w_visualizza_referenze.destroy
destroy(this.cbx_1)
destroy(this.tv_1)
destroy(this.cb_ricarica)
end on

event open;string ls_error

istr_data = message.powerobjectparm

setnull(message.powerobjectparm)

title = istr_data.windowName

iuo_regex = create uo_espress_reg
iuo_regex.set_pattern("%(\w+)", ls_error)

DEFAULT_GROUP_ICON = wf_add_icon("treeview\cartella.png")
DEFAULT_ITEM_ICON = wf_add_icon("treeview\documento_blu.png")

wf_check_references()
end event

event resize;cb_ricarica.move(newwidth - cb_ricarica.width - 40, newheight - cb_ricarica.height - 40)
cbx_1.move(20, cb_ricarica.y)

tv_1.resize(newwidth - 80, cb_ricarica.y - 40)

end event

type cbx_1 from checkbox within w_visualizza_referenze
integer x = 55
integer y = 1632
integer width = 846
integer height = 76
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Visualizza solo referenze valide"
boolean checked = true
end type

type tv_1 from treeview within w_visualizza_referenze
integer x = 20
integer y = 20
integer width = 2139
integer height = 1584
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean border = false
boolean linesatroot = true
long picturemaskcolor = 536870912
long statepicturemaskcolor = 536870912
end type

event itempopulate;str_referenza_item lstr_data
str_referenza_item_data lstr_item_data
treeviewitem ltv_item

if ib_tv_delete then return

getitem(handle, ltv_item)

lstr_item_data = ltv_item.data
lstr_data = istr_data.tabelle[lstr_item_data.index]

if wf_inserisci_referenze(handle, lstr_item_data, lstr_data) <= 0 then
	ltv_item.children = false
	tv_1.setitem(handle, ltv_item)
end if


end event

event doubleclicked;str_referenza_item lstr_data
str_referenza_item_data lstr_item_data
treeviewitem ltv_item
window lw_window

if ib_tv_delete then return

getitem(handle, ltv_item)

lstr_item_data = ltv_item.data

// Solo se è un "figlio" allora eseguo l'apertura della finestra
// di gestione, altrimenti non faccio nulla
if lstr_item_data.tipo = TREE_ITEM_CHILD then
	lstr_data = istr_data.tabelle[lstr_item_data.index]
	
	if g_str.isnotempty(lstr_data.windowname) then
		window_type_open(lw_window, lstr_data.windowname, 6)
	end if
end if

end event

type cb_ricarica from commandbutton within w_visualizza_referenze
integer x = 1755
integer y = 1616
integer width = 402
integer height = 96
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Ricarica"
end type

event clicked;wf_check_references()
end event

