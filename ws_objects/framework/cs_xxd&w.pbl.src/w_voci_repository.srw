﻿$PBExportHeader$w_voci_repository.srw
forward
global type w_voci_repository from w_cs_xx_principale
end type
type cb_lingue from commandbutton within w_voci_repository
end type
type st_2 from statictext within w_voci_repository
end type
type cb_trova from commandbutton within w_voci_repository
end type
type sle_des from singlelineedit within w_voci_repository
end type
type st_1 from statictext within w_voci_repository
end type
type dw_voci_repository from uo_cs_xx_dw within w_voci_repository
end type
type dw_voce_selezionata from uo_cs_xx_dw within w_voci_repository
end type
end forward

global type w_voci_repository from w_cs_xx_principale
integer width = 2834
integer height = 2592
string title = "Elenco Voci Disponibili nel Repository"
boolean minbox = false
boolean maxbox = false
boolean resizable = false
cb_lingue cb_lingue
st_2 st_2
cb_trova cb_trova
sle_des sle_des
st_1 st_1
dw_voci_repository dw_voci_repository
dw_voce_selezionata dw_voce_selezionata
end type
global w_voci_repository w_voci_repository

type variables
boolean ib_modifica = false
end variables

event pc_setwindow;call super::pc_setwindow;dw_voci_repository.set_dw_key("id_voce")

iuo_dw_main = dw_voci_repository

if s_cs_xx.cod_utente = "CS_SYSTEM" then
	
	dw_voci_repository.set_dw_options(sqlca, &
												 pcca.null_object, &
												 c_default, &
												 c_default)
	
else
	
	set_w_options(c_noenablepopup + c_closenosave)
	
	dw_voci_repository.set_dw_options(sqlca, &
												 pcca.null_object, &
												 c_nonew + c_nodelete + c_nomodify, &
												 c_default)
	
end if

dw_voce_selezionata.set_dw_options(sqlca, &
												 dw_voci_repository, &
												 c_sharedata + c_scrollparent + c_nonew + c_nodelete + c_nomodify, &
												 c_default)

dw_voci_repository.object.nuovo.filename = s_cs_xx.volume + s_cs_xx.risorse + "attenzione.bmp"

dw_voce_selezionata.dragicon = s_cs_xx.volume + s_cs_xx.risorse + "drag.ico"
end event

on w_voci_repository.create
int iCurrent
call super::create
this.cb_lingue=create cb_lingue
this.st_2=create st_2
this.cb_trova=create cb_trova
this.sle_des=create sle_des
this.st_1=create st_1
this.dw_voci_repository=create dw_voci_repository
this.dw_voce_selezionata=create dw_voce_selezionata
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_lingue
this.Control[iCurrent+2]=this.st_2
this.Control[iCurrent+3]=this.cb_trova
this.Control[iCurrent+4]=this.sle_des
this.Control[iCurrent+5]=this.st_1
this.Control[iCurrent+6]=this.dw_voci_repository
this.Control[iCurrent+7]=this.dw_voce_selezionata
end on

on w_voci_repository.destroy
call super::destroy
destroy(this.cb_lingue)
destroy(this.st_2)
destroy(this.cb_trova)
destroy(this.sle_des)
destroy(this.st_1)
destroy(this.dw_voci_repository)
destroy(this.dw_voce_selezionata)
end on

type cb_lingue from commandbutton within w_voci_repository
integer x = 2400
integer y = 20
integer width = 389
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Lingue"
end type

event clicked;window_open_parm(w_voci_repository_lingue, -1, dw_voci_repository)
end event

type st_2 from statictext within w_voci_repository
integer x = 23
integer y = 40
integer width = 571
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Descrizione / Voce:"
boolean focusrectangle = false
end type

type cb_trova from commandbutton within w_voci_repository
integer x = 2034
integer y = 20
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Trova"
end type

event clicked;dw_voci_repository.triggerevent("ue_trova_voce")
end event

type sle_des from singlelineedit within w_voci_repository
event ue_keydown pbm_keydown
integer x = 617
integer y = 20
integer width = 1394
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
end type

event ue_keydown;if key = KeyEnter! then
	
	dw_voci_repository.triggerevent("ue_trova_voce")
	
end if
end event

event modified;//dw_voci_repository.triggerevent("ue_trova_voce")
end event

type st_1 from statictext within w_voci_repository
integer x = 23
integer y = 2400
integer width = 640
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Elemento Drag and Drop:"
boolean focusrectangle = false
end type

type dw_voci_repository from uo_cs_xx_dw within w_voci_repository
event ue_trova_voce ( )
integer x = 23
integer y = 140
integer width = 2743
integer height = 2240
integer taborder = 40
string dataobject = "d_voci_repository"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event ue_trova_voce();long ll_start, ll_end, ll_row, ll_i
boolean lb_trovato

if g_str.isempty(sle_des.text) then
	g_mb.warning("Inserire un parametro di ricerca")
	return
end if

lb_trovato = false
ll_end = rowcount()
ll_start = getrow()

// Riparto se sono alla fine
if ll_start >= ll_end then
	ll_start = 1
else 
	ll_start++
end if

for ll_i = ll_start to ll_end
	
	if g_str.contains(getitemstring(ll_i,"descrizione"), sle_des.text, true) or &
	 	g_str.contains(getitemstring(ll_i,"collegamento"), sle_des.text, true) then
			
		selectrow(0, false)
		selectrow(ll_i, true)
		scrolltorow(ll_i)
		setrow(ll_i)
		lb_trovato = true
		exit
		
	end if
		
next

if not lb_trovato and ll_i = ll_end then
	g_mb.warning("Voce non trovata.")
	
	// Mi riposiziono all'inizio
	selectrow(0, false)
	selectrow(1, true)
	scrolltorow(1)
	setrow(1)
	
end if

// w_voci_repository_lingue

//string  ls_descrizione, ls_window, ls_des_test, ls_win_test
//long 	  ll_i, ll_row
//boolean lb_trovato
//
//ls_descrizione = upper(sle_des.text)
//
//if ls_descrizione = "" then
//	setnull(ls_descrizione)
//end if
//
//ls_window = upper(sle_window.text)
//
//if ls_window = "" then
//	setnull(ls_window)
//end if
//
//if isnull(ls_descrizione) and isnull(ls_window) then
//	g_mb.messagebox("Repository Voci","Immettere i parametri di ricerca",exclamation!)
//	return
//end if
//
//ll_row = getrow()
//
//if isnull(ll_row) or ll_row <= 0 then
//	ll_row = 1
//else
//	ll_row++
//end if
//
//lb_trovato = false
//
//for ll_i = ll_row to rowcount()
//	
//	ls_des_test = upper(getitemstring(ll_i,"descrizione"))
//	
//	ls_win_test = upper(getitemstring(ll_i,"collegamento"))
//	
//	if pos(ls_des_test,ls_descrizione,1) > 0 or pos (ls_win_test,ls_window,1) > 0 then
//		setrow(ll_i)
//		scrolltorow(ll_i)
//		lb_trovato = true
//		exit
//	end if
//	
//next
//
//if not lb_trovato then
//	g_mb.messagebox("Repository Voci","Testo non trovato",information!)
//end if
end event

event pcd_retrieve;call super::pcd_retrieve;retrieve()
end event

event doubleclicked;call super::doubleclicked;if row > 0 and s_cs_xx.cod_utente = "CS_SYSTEM" and ib_modifica = false then
	window lw_window		
	window_type_open(lw_window,getitemstring(row,"collegamento"),-1)
end if

choose case dwo.name
	
	case "t_descrizione"
		
		setsort("descrizione A, id_voce A")
		
		sort()
		
	case "t_window"
		
		setsort("collegamento A, id_voce A")
		
		sort()
		
	case "t_nuovo"
		
		setsort("flag_aggiornato D, id_voce A")
		
		sort()
		
end choose
end event

event pcd_new;call super::pcd_new;long ll_id_voce


ib_modifica = true

dw_voce_selezionata.dragauto = false

select max(id_voce)
into   :ll_id_voce
from 	 voci_repository;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Repository Voci","Errore in lettura massimo progressivo voce: " + sqlca.sqlerrtext)
	return -1
elseif sqlca.sqlcode = 100 or isnull(ll_id_voce) then
	ll_id_voce = 0
end if

ll_id_voce ++

setitem(getrow(),"id_voce",ll_id_voce)

setitem(getrow(),"flag_aggiornato","S")
end event

event pcd_modify;call super::pcd_modify;ib_modifica = true

dw_voce_selezionata.dragauto = false
end event

event pcd_view;call super::pcd_view;ib_modifica = false

dw_voce_selezionata.dragauto = true
end event

event updateend;call super::updateend;ib_modifica = false

dw_voce_selezionata.dragauto = true

triggerevent("pcd_retrieve")
end event

event updatestart;call super::updatestart;string ls_window, ls_old
long   ll_i, ll_id_voce


for ll_i = 1 to rowcount()

	ls_window = getitemstring(ll_i,"collegamento")
	
	if isnull(ls_window) or trim(ls_window) = "" then
		g_mb.messagebox("Repository Voci","Inserire il nome della window!",exclamation!)
		rollback;
		return 1
	end if
	
	if getitemstatus(ll_i,"collegamento",primary!) = datamodified! and &
		getitemstatus(ll_i,0,primary!) <> newmodified! then
		
		ls_old = getitemstring(ll_i,"collegamento",primary!,true)
		
		update menu
		set    collegamento = :ls_window
		where  collegamento = :ls_old;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Repository Voci","Errore in aggiornamento menù collegati: " + sqlca.sqlerrtext)
			rollback;
			return 1
		end if
		
	end if
	
	if getitemstatus(ll_i,0,primary!) = newmodified! then
		
		update menu
		set    flag_tipo_voce = 'W'
		where  collegamento = :ls_window;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Repository Voci","Errore in aggiornamento menù collegati: " + sqlca.sqlerrtext)
			rollback;
			return 1
		end if
		
	end if
	
next

for ll_i = 1 to deletedcount()
	
	ls_window = getitemstring(ll_i,"collegamento",delete!,false)
	ll_id_voce =  getitemnumber(ll_i,"id_voce",delete!,false)
	
	if not isnull(ls_window) and trim(ls_window) <> "" then
	
		update menu
		set    flag_tipo_voce = 'E'
		where  collegamento = :ls_window;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Repository Voci","Errore in aggiornamento menù collegati: " + sqlca.sqlerrtext)
			rollback;
			return 1
		end if
		
		delete from voci_repository_lingue
		where id_voce = :ll_id_voce;
		
		if sqlca.sqlcode < 0 then
			g_mb.error("Errore durante la cancellazione delle traduzioni per la voce selezionata.", sqlca)
			rollback;
			return 1
		end if
		
	end if
	
next
end event

event itemchanged;call super::itemchanged;if ib_modifica and dwo.name = "collegamento" then
	
	if isnull(data) or trim(data) = "" then
		g_mb.messagebox("Repository Voci","Inserire il nome della window!",exclamation!)
		return 1
	end if
	
	long ll_count
	
	select count(*)
	into   :ll_count
	from   voci_repository
	where  collegamento = :data;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Repository Voci","Errore in controllo presenza voce: " + sqlca.sqlerrtext)
		return 1
	elseif sqlca.sqlcode = 100 or isnull(ll_count) then
		ll_count = 0
	end if
	
	if ll_count > 0 then
		g_mb.messagebox("Repository Voci","Questa window è già presente nel repository!",exclamation!)
		return 1
	end if
	
	setitem(row,"flag_aggiornato","S")
	
	string ls_descrizione
	
	ls_descrizione = getitemstring(row,"descrizione")
	
	if isnull(ls_descrizione) or trim(ls_descrizione) = "" then
		setitem(row,"descrizione",data)
	end if
	
end if
end event

event pcd_query;call super::pcd_query;dw_voce_selezionata.dragauto = false
end event

type dw_voce_selezionata from uo_cs_xx_dw within w_voci_repository
integer x = 709
integer y = 2400
integer width = 2057
integer height = 80
integer taborder = 50
string dataobject = "d_voce_selezionata"
boolean border = false
end type

event rbuttondown;//In questo script il flag EXTEND ANCESTOR DEVE RESTARE DISATTIVATO
end event

event dragleave;call super::dragleave;dragicon = s_cs_xx.volume + s_cs_xx.risorse + "stop.ico"
end event

event dragenter;call super::dragenter;dragicon = s_cs_xx.volume + s_cs_xx.risorse + "drag.ico"
end event

