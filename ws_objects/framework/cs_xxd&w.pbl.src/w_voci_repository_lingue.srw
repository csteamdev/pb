﻿$PBExportHeader$w_voci_repository_lingue.srw
$PBExportComments$Finestra Gestione Prodotti Lingue
forward
global type w_voci_repository_lingue from w_cs_xx_principale
end type
type dw_prodotti_lingue from uo_cs_xx_dw within w_voci_repository_lingue
end type
end forward

global type w_voci_repository_lingue from w_cs_xx_principale
integer width = 2866
integer height = 1148
string title = "Voci Repository Lingue"
dw_prodotti_lingue dw_prodotti_lingue
end type
global w_voci_repository_lingue w_voci_repository_lingue

type variables
private:
	long il_id_voce
	string is_descrizione_voce
end variables

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_prodotti_lingue, &
                 "cod_lingua", &
                 sqlca, &
                 "tab_lingue", &
                 "cod_lingua", &
                 "des_lingua", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

event pc_setwindow;call super::pc_setwindow;string ls_classname
s_cs_xx_parametri lstr_data

setnull(il_id_voce)
dw_prodotti_lingue.set_dw_key("id_voce")

ls_classname = ClassName(message.powerobjectparm)

// Ho aperto la finestra tramite il menu di sinistra e sto cercando di tradurre una singola voce
// del menu
if "s_cs_xx_parametri" = ls_classname then
	lstr_data = message.powerobjectparm
	il_id_voce = lstr_data.parametro_ul_1
	is_descrizione_voce = lstr_data.parametro_s_1
	dw_prodotti_lingue.set_dw_options(sqlca,  pcca.null_object, c_scrollparent, c_default)
	
// Altrimenti sto aprendo la finestra dalla w_vovi_repository dove viene passata
// la DW padre per eseguire la query di ricerca
else
	dw_prodotti_lingue.set_dw_options(sqlca, i_openparm, c_scrollparent, c_default)
end if

end event

on w_voci_repository_lingue.create
int iCurrent
call super::create
this.dw_prodotti_lingue=create dw_prodotti_lingue
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_prodotti_lingue
end on

on w_voci_repository_lingue.destroy
call super::destroy
destroy(this.dw_prodotti_lingue)
end on

type dw_prodotti_lingue from uo_cs_xx_dw within w_voci_repository_lingue
integer x = 23
integer y = 20
integer width = 2766
integer height = 1000
string dataobject = "d_voci_repository_lingue"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;string ls_descrizione
long ll_id_voce, ll_errore


if isnull(il_id_voce) then
	ls_descrizione =  i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "descrizione")
	ll_id_voce = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "id_voce")
else
	ls_descrizione = is_descrizione_voce
	ll_id_voce = il_id_voce
end if

parent.title = " Voci Repository Lingue - " + ls_descrizione

ll_errore = retrieve(ll_id_voce)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_id_voce, ll_i

if isnull(il_id_voce) then
	ll_id_voce = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "id_voce")
else
	ll_id_voce = il_id_voce
end if

for ll_i = 1 to this.rowcount()
	
   if isnull(this.getitemnumber(ll_i, "id_voce")) then
      this.setitem(ll_i, "id_voce", ll_id_voce)
   end if
	
next
end event

