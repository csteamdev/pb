﻿$PBExportHeader$w_webfax.srw
$PBExportComments$Finestra Gestione  ABI
forward
global type w_webfax from w_cs_xx_principale
end type
type st_10 from statictext within w_webfax
end type
type st_9 from statictext within w_webfax
end type
type st_8 from statictext within w_webfax
end type
type st_7 from statictext within w_webfax
end type
type st_6 from statictext within w_webfax
end type
type st_5 from statictext within w_webfax
end type
type st_4 from statictext within w_webfax
end type
type st_3 from statictext within w_webfax
end type
type st_1 from statictext within w_webfax
end type
type st_2 from statictext within w_webfax
end type
type dw_webfax from uo_cs_xx_dw within w_webfax
end type
end forward

global type w_webfax from w_cs_xx_principale
integer width = 3840
integer height = 1820
string title = "Web Fax"
st_10 st_10
st_9 st_9
st_8 st_8
st_7 st_7
st_6 st_6
st_5 st_5
st_4 st_4
st_3 st_3
st_1 st_1
st_2 st_2
dw_webfax dw_webfax
end type
global w_webfax w_webfax

event pc_setwindow;call super::pc_setwindow;dw_webfax.set_dw_options(sqlca, &
                      pcca.null_object, &
                      c_default, &
                      c_default)

end event

on w_webfax.create
int iCurrent
call super::create
this.st_10=create st_10
this.st_9=create st_9
this.st_8=create st_8
this.st_7=create st_7
this.st_6=create st_6
this.st_5=create st_5
this.st_4=create st_4
this.st_3=create st_3
this.st_1=create st_1
this.st_2=create st_2
this.dw_webfax=create dw_webfax
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_10
this.Control[iCurrent+2]=this.st_9
this.Control[iCurrent+3]=this.st_8
this.Control[iCurrent+4]=this.st_7
this.Control[iCurrent+5]=this.st_6
this.Control[iCurrent+6]=this.st_5
this.Control[iCurrent+7]=this.st_4
this.Control[iCurrent+8]=this.st_3
this.Control[iCurrent+9]=this.st_1
this.Control[iCurrent+10]=this.st_2
this.Control[iCurrent+11]=this.dw_webfax
end on

on w_webfax.destroy
call super::destroy
destroy(this.st_10)
destroy(this.st_9)
destroy(this.st_8)
destroy(this.st_7)
destroy(this.st_6)
destroy(this.st_5)
destroy(this.st_4)
destroy(this.st_3)
destroy(this.st_1)
destroy(this.st_2)
destroy(this.dw_webfax)
end on

type st_10 from statictext within w_webfax
integer x = 2469
integer y = 472
integer width = 1129
integer height = 136
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8421376
long backcolor = 12632256
string text = "al posto di [DOC] il sistema sostituirà la numerazione fiscale di una bolla o di un DDT vendita"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_9 from statictext within w_webfax
integer x = 2469
integer y = 304
integer width = 937
integer height = 136
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8421376
long backcolor = 12632256
string text = "ad esempio 12345*049101010 oppure [DOC]"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_8 from statictext within w_webfax
integer x = 1271
integer y = 412
integer width = 1129
integer height = 216
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "al posto di [NUM] il sistema sostituirà il numero del fax del destinatario, quindi è importante la presenza della stringa [NUM]"
boolean focusrectangle = false
end type

type st_7 from statictext within w_webfax
integer x = 1271
integer y = 316
integer width = 1129
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "ad esempio [NUM]@fax.servizio.it"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_6 from statictext within w_webfax
integer x = 2469
integer y = 36
integer width = 1129
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8421376
long backcolor = 12632256
string text = "STRINGA INVIO"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_5 from statictext within w_webfax
integer x = 2469
integer y = 104
integer width = 1129
integer height = 232
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8421376
long backcolor = 12632256
string text = "solitamente un codice pin che dipende dal servizio e un numero di fax che dipende dal mittente"
boolean focusrectangle = false
end type

type st_4 from statictext within w_webfax
integer x = 1271
integer y = 36
integer width = 1129
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "INDIRIZZO DESTINATARIO"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_3 from statictext within w_webfax
integer x = 1271
integer y = 112
integer width = 1129
integer height = 204
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "campo indirizzo a cui recapitare: solitamente il numero del fax del destinatario e un indirizzo che dipende dal servizio."
boolean focusrectangle = false
end type

type st_1 from statictext within w_webfax
integer x = 69
integer y = 36
integer width = 1129
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "INDIRIZZO MITTENTE"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_2 from statictext within w_webfax
integer x = 69
integer y = 112
integer width = 1129
integer height = 232
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = ":e-mail alla quale recapitare la ricevuta del FAX (qualora prevista dal servizio)"
boolean focusrectangle = false
end type

type dw_webfax from uo_cs_xx_dw within w_webfax
integer x = 27
integer y = 660
integer width = 3744
integer height = 1028
integer taborder = 20
string dataobject = "d_webfax"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event pcd_setkey;call super::pcd_setkey;long ll_idx

for ll_idx = 1 to rowcount()
	if IsNull(GetItemstring(ll_idx, "cod_azienda")) then
		setitem(ll_idx, "cod_azienda", s_cs_xx.cod_azienda)
	end if
next
end event

