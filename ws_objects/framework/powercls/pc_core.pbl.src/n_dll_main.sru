﻿$PBExportHeader$n_dll_main.sru
$PBExportComments$Provides definitions for DLL external functions
forward
global type n_dll_main from nonvisualobject
end type
end forward

global type n_dll_main from nonvisualobject
end type
global n_dll_main n_dll_main

type variables
INTEGER		c_Fatal 			= -1
INTEGER		c_Success 		= 0
UNSIGNEDLONG	c_Default 		= 0
UNSIGNEDLONG	c_Undefined 		= 0

UO_DW_MAIN	c_NullDW
UO_CB_MAIN	c_NullCB
MENU		c_NullMenu
PICTURE		c_NullPicture

STRING		c_INIUndefined 		= "[INI_Undefined]"
STRING		c_XKey 			= "X"
STRING		c_YKey 			= "Y"
STRING		c_WidthKey 		= "Width"
STRING		c_HeightKey 		= "Height"
STRING		c_WinStateKey 		= "WindowState"
INTEGER		c_MinimumOverlap 		= 25

UNSIGNEDLONG	c_SOCPromptUser 		= 100
UNSIGNEDLONG	c_SOCPromptUserOnce 	= 101
UNSIGNEDLONG	c_SOCSave 		= 102
UNSIGNEDLONG	c_SOCNoSave 		= 103

UNSIGNEDLONG	c_MDI_AllowRedrawMask 	= 1
UNSIGNEDLONG	c_MDI_NoAllowRedraw 	= 0
UNSIGNEDLONG	c_MDI_AllowRedraw 	= 1

UNSIGNEDLONG	c_MDI_ShowResourcesMask = 2
UNSIGNEDLONG	c_MDI_NoShowResources 	= 0
UNSIGNEDLONG	c_MDI_ShowResources	= 2

UNSIGNEDLONG	c_MDI_ShowClockMask 	= 4
UNSIGNEDLONG	c_MDI_NoShowClock 	= 0
UNSIGNEDLONG	c_MDI_ShowClock 		= 4

UNSIGNEDLONG	c_MDI_AutoConfigMenusMask = 8
UNSIGNEDLONG	c_MDI_NoAutoConfigMenus 	= 0
UNSIGNEDLONG	c_MDI_AutoConfigMenus 	= 8

UNSIGNEDLONG	c_MDI_SavePositionMask 	= 16
UNSIGNEDLONG	c_MDI_NoSavePosition 	= 0
UNSIGNEDLONG	c_MDI_SavePosition 	= 16

UNSIGNEDLONG	c_MDI_ToolBarPositionMask = 224
UNSIGNEDLONG	c_MDI_ToolBarNone 	= 0
UNSIGNEDLONG	c_MDI_ToolBarTop 	= 32
UNSIGNEDLONG	c_MDI_ToolBarBottom 	= 64
UNSIGNEDLONG	c_MDI_ToolBarLeft 	= 96
UNSIGNEDLONG	c_MDI_ToolBarRight 	= 128
UNSIGNEDLONG	c_MDI_ToolBarFloat 	= 160

UNSIGNEDLONG	c_MDI_ToolBarTextMask 	= 256
UNSIGNEDLONG	c_MDI_ToolBarHideText 	= 0
UNSIGNEDLONG	c_MDI_ToolBarShowText 	= 256

UNSIGNEDLONG	c_LoadCodeTableMask 	= 1
UNSIGNEDLONG	c_LoadCodeTable 		= 0
UNSIGNEDLONG	c_NoLoadCodeTable 	= 1

UNSIGNEDLONG	c_EnablePopupMask 	= 2
UNSIGNEDLONG	c_EnablePopup 		= 0
UNSIGNEDLONG	c_NoEnablePopup 		= 2

UNSIGNEDLONG	c_ResizeWinMask 		= 12
UNSIGNEDLONG	c_NoResizeWin 		= 0
UNSIGNEDLONG	c_ResizeWin 		= 4
UNSIGNEDLONG	c_ZoomWin 		= 8

UNSIGNEDLONG	c_AutoMinimizeMask 	= 16
UNSIGNEDLONG	c_NoAutoMinimize 		= 0
UNSIGNEDLONG	c_AutoMinimize 		= 16

UNSIGNEDLONG	c_AutoSizeMask 		= 96
UNSIGNEDLONG	c_NoAutoSize 		= 0
UNSIGNEDLONG	c_AutoSize 		= 32
UNSIGNEDLONG	c_AutoSize30 		= 64

UNSIGNEDLONG	c_AutoPositionMask 	= 128
UNSIGNEDLONG	c_NoAutoPosition 		= 0
UNSIGNEDLONG	c_AutoPosition 		= 128

UNSIGNEDLONG	c_SavePositionMask 	= 256
UNSIGNEDLONG	c_NoSavePosition 		= 0
UNSIGNEDLONG	c_SavePosition 		= 256

UNSIGNEDLONG	c_CloseMask 		= 1536
UNSIGNEDLONG	c_ClosePromptUser 	= 0
UNSIGNEDLONG	c_ClosePromptUserOnce 	= 512
UNSIGNEDLONG	c_CloseSave 		= 1024
UNSIGNEDLONG	c_CloseNoSave 		= 1536

UNSIGNEDLONG	c_ToolBarPositionMask 	= 117440512
UNSIGNEDLONG	c_ToolBarNone 		= 0
UNSIGNEDLONG	c_ToolBarTop 		= 16777216
UNSIGNEDLONG	c_ToolBarBottom 		= 33554432
UNSIGNEDLONG	c_ToolBarLeft 		= 50331648
UNSIGNEDLONG	c_ToolBarRight 		= 67108864
UNSIGNEDLONG	c_ToolBarFloat 		= 83886080

UNSIGNEDLONG	c_ToolBarTextMask 	= 134217728
UNSIGNEDLONG	c_ToolBarHideText 	= 0
UNSIGNEDLONG	c_ToolBarShowText 	= 134217728

UNSIGNEDLONG	c_CT_LoadCodeTableMask 	= 65536
UNSIGNEDLONG	c_CT_LoadCodeTable 	= 0
UNSIGNEDLONG	c_CT_NoLoadCodeTable 	= 65536

UNSIGNEDLONG	c_CT_ResizeContMask 	= 393216
UNSIGNEDLONG	c_CT_NoResizeCont 	= 0
UNSIGNEDLONG	c_CT_ResizeCont 		= 131072
UNSIGNEDLONG	c_CT_ZoomCont 		= 262144

UNSIGNEDLONG	c_CT_CloseMask 		= 1572864
UNSIGNEDLONG	c_CT_ClosePromptUser 	= 0
UNSIGNEDLONG	c_CT_ClosePromptUserOnce = 524288
UNSIGNEDLONG	c_CT_CloseSave 		= 1048576
UNSIGNEDLONG	c_CT_CloseNoSave 	= 1572864

UNSIGNEDLONG	c_CT_BringToTopMask 	= 2097152
UNSIGNEDLONG	c_CT_BringToTop 		= 0
UNSIGNEDLONG	c_CT_NoBringToTop 	= 2097152

UNSIGNEDLONG	c_MasterList 		= 0
UNSIGNEDLONG	c_MasterEdit 		= 0
UNSIGNEDLONG	c_DetailList 		= 0
UNSIGNEDLONG	c_DetailEdit 		= 0

UNSIGNEDLONG	c_ScrollParentMask 	= 1
UNSIGNEDLONG	c_ScrollSelf 		= 0
UNSIGNEDLONG	c_ScrollParent 		= 1

UNSIGNEDLONG	c_IsInstanceMask 		= 2
UNSIGNEDLONG	c_IsNotInstance 		= 0
UNSIGNEDLONG	c_IsInstance 		= 2

UNSIGNEDLONG	c_AlwaysCheckRequiredMask = 4
UNSIGNEDLONG	c_RequiredOnSave 	= 0
UNSIGNEDLONG	c_AlwaysCheckRequired 	= 4

UNSIGNEDLONG	c_NewOkMask 		= 8
UNSIGNEDLONG	c_NoNew 		= 0
UNSIGNEDLONG	c_NewOk 		= 8

UNSIGNEDLONG	c_ModifyOkMask 		= 16
UNSIGNEDLONG	c_NoModify 		= 0
UNSIGNEDLONG	c_ModifyOk 		= 16

UNSIGNEDLONG	c_DeleteOkMask 		= 32
UNSIGNEDLONG	c_NoDelete 		= 0
UNSIGNEDLONG	c_DeleteOk 		= 32

UNSIGNEDLONG	c_QueryOkMask 		= 64
UNSIGNEDLONG	c_NoQuery 		= 0
UNSIGNEDLONG	c_QueryOk 		= 64

UNSIGNEDLONG	c_SelectOnMask 		= 384
UNSIGNEDLONG	c_SelectOnDoubleClick 	= 0
UNSIGNEDLONG	c_SelectOnClick 		= 128
UNSIGNEDLONG	c_SelectOnRowFocusChange = 256

UNSIGNEDLONG	c_DrillDownMask 		= 512
UNSIGNEDLONG	c_NoDrillDown 		= 0
UNSIGNEDLONG	c_DrillDown 		= 512

UNSIGNEDLONG	c_ModeOnSelectMask 	= 3072
UNSIGNEDLONG	c_SameModeOnSelect 	= 0
UNSIGNEDLONG	c_ViewOnSelect 		= 1024
UNSIGNEDLONG	c_ModifyOnSelect 		= 2048

UNSIGNEDLONG	c_MultiSelectMask 		= 4096
UNSIGNEDLONG	c_NoMultiSelect 		= 0
UNSIGNEDLONG	c_MultiSelect 		= 4096

UNSIGNEDLONG	c_RefreshSelectMask 	= 24576
UNSIGNEDLONG	c_RefreshOnSelect 	= 0
UNSIGNEDLONG	c_NoAutoRefresh 		= 8192
UNSIGNEDLONG	c_RefreshOnMultiSelect 	= 16384

UNSIGNEDLONG	c_ModeOnOpenMask 	= 98304
UNSIGNEDLONG	c_ParentModeOnOpen 	= 0
UNSIGNEDLONG	c_ViewOnOpen 		= 32768
UNSIGNEDLONG	c_ModifyOnOpen 		= 65536
UNSIGNEDLONG	c_NewOnOpen 		= 98304

UNSIGNEDLONG	c_ViewAfterSaveMask 	= 131072
UNSIGNEDLONG	c_SameModeAfterSave 	= 0
UNSIGNEDLONG	c_ViewAfterSave 		= 131072

UNSIGNEDLONG	c_EnableModifyOnOpenMask = 262144
UNSIGNEDLONG	c_NoEnableModifyOnOpen 	= 0
UNSIGNEDLONG	c_EnableModifyOnOpen 	= 262144

UNSIGNEDLONG	c_EnableNewOnOpenMask	= 524288
UNSIGNEDLONG	c_NoEnableNewOnOpen 	= 0
UNSIGNEDLONG	c_EnableNewOnOpen 	= 524288

UNSIGNEDLONG	c_RetrieveOnOpenMask 	= 1048576
UNSIGNEDLONG	c_RetrieveOnOpen 	= 0
UNSIGNEDLONG	c_AutoRetrieveOnOpen 	= 0
UNSIGNEDLONG	c_NoRetrieveOnOpen 	= 1048576

UNSIGNEDLONG	c_RefreshOnOpenMask 	= 6291456
UNSIGNEDLONG	c_AutoRefresh 		= 0
UNSIGNEDLONG	c_SkipRefresh 		= 2097152
UNSIGNEDLONG	c_PostRefresh 		= 4194304
UNSIGNEDLONG	c_TriggerRefresh 		= 6291456

UNSIGNEDLONG	c_RetrieveAsNeededMask 	= 8388608
UNSIGNEDLONG	c_RetrieveAll 		= 0
UNSIGNEDLONG	c_RetrieveAsNeeded 	= 8388608

UNSIGNEDLONG	c_ShareDataMask 		= 16777216
UNSIGNEDLONG	c_NoShareData 		= 0
UNSIGNEDLONG	c_ShareData 		= 16777216

UNSIGNEDLONG	c_RefreshParentMask 	= 33554432
UNSIGNEDLONG	c_NoRefreshParent 	= 0
UNSIGNEDLONG	c_RefreshParent 		= 33554432

UNSIGNEDLONG	c_RefreshChildMask 	= 67108864
UNSIGNEDLONG	c_NoRefreshChild 		= 0
UNSIGNEDLONG	c_RefreshChild 		= 67108864

UNSIGNEDLONG	c_NoIgnoreNewRowsMask 	= 134217728
UNSIGNEDLONG	c_IgnoreNewRows 		= 0
UNSIGNEDLONG	c_NoIgnoreNewRows 	= 134217728

UNSIGNEDLONG	c_NewModeOnEmptyMask 	= 268435456
UNSIGNEDLONG	c_NoNewModeOnEmpty 	= 0
UNSIGNEDLONG	c_NewModeOnEmpty 	= 268435456

UNSIGNEDLONG	c_OnlyOneNewRowMask 	= 536870912
UNSIGNEDLONG	c_MultipleNewRows 	= 0
UNSIGNEDLONG	c_OnlyOneNewRow 	= 536870912

UNSIGNEDLONG	c_EnableCCMask 		= 1073741824
UNSIGNEDLONG	c_DisableCC 		= 0
UNSIGNEDLONG	c_EnableCC 		= 1073741824

UNSIGNEDLONG	c_EnableCCInsertMask 	= 2147483648
UNSIGNEDLONG	c_DisableCCInsert 		= 0
UNSIGNEDLONG	c_EnableCCInsert 		= 2147483648

UNSIGNEDLONG	c_ViewModeBorderMask 	= 7
UNSIGNEDLONG	c_ViewModeBorderUnchanged = 0
UNSIGNEDLONG	c_ViewModeNoBorder 	= 1
UNSIGNEDLONG	c_ViewModeShadowBox 	= 2
UNSIGNEDLONG	c_ViewModeBox		= 3
UNSIGNEDLONG	c_ViewModeResize 	= 4
UNSIGNEDLONG	c_ViewModeUnderline 	= 5
UNSIGNEDLONG	c_ViewModeLowered 	= 6
UNSIGNEDLONG	c_ViewModeRaised 	= 7

UNSIGNEDLONG	c_ViewModeColorMask 	= 248
UNSIGNEDLONG	c_ViewModeColorUnchanged = 0
UNSIGNEDLONG	c_ViewModeBlack 		= 8
UNSIGNEDLONG	c_ViewModeWhite 		= 16
UNSIGNEDLONG	c_ViewModeGray 		= 24
UNSIGNEDLONG	c_ViewModeLightGray 	= 24
UNSIGNEDLONG	c_ViewModeDarkGray 	= 32
UNSIGNEDLONG	c_ViewModeRed 		= 40
UNSIGNEDLONG	c_ViewModeDarkRed 	= 48
UNSIGNEDLONG	c_ViewModeGreen 		= 56
UNSIGNEDLONG	c_ViewModeDarkGreen 	= 64
UNSIGNEDLONG	c_ViewModeBlue 		= 72
UNSIGNEDLONG	c_ViewModeDarkBlue 	= 80
UNSIGNEDLONG	c_ViewModeMagenta 	= 88
UNSIGNEDLONG	c_ViewModeDarkMagenta 	= 96
UNSIGNEDLONG	c_ViewModeCyan 		= 104
UNSIGNEDLONG	c_ViewModeDarkCyan 	= 112
UNSIGNEDLONG	c_ViewModeYellow 	= 120
UNSIGNEDLONG	c_ViewModeBrown 		= 128

UNSIGNEDLONG	c_InactiveDWColorMask 	= 7936
UNSIGNEDLONG	c_InactiveDWColorUnchanged = 0
UNSIGNEDLONG	c_InactiveDWBlack 	= 256
UNSIGNEDLONG	c_InactiveDWWhite 	= 512
UNSIGNEDLONG	c_InactiveDWGray 		= 768
UNSIGNEDLONG	c_InactiveDWLightGray 	= 768
UNSIGNEDLONG	c_InactiveDWDarkGray 	= 1024
UNSIGNEDLONG	c_InactiveDWRed 		= 1280
UNSIGNEDLONG	c_InactiveDWDarkRed 	= 1536
UNSIGNEDLONG	c_InactiveDWGreen 	= 1792
UNSIGNEDLONG	c_InactiveDWDarkGreen 	= 2048
UNSIGNEDLONG	c_InactiveDWBlue 		= 2304
UNSIGNEDLONG	c_InactiveDWDarkBlue 	= 2560
UNSIGNEDLONG	c_InactiveDWMagenta 	= 2816
UNSIGNEDLONG	c_InactiveDWDarkMagenta 	= 3072
UNSIGNEDLONG	c_InactiveDWCyan 	= 3328
UNSIGNEDLONG	c_InactiveDWDarkCyan 	= 3584
UNSIGNEDLONG	c_InactiveDWYellow 	= 3840
UNSIGNEDLONG	c_InactiveDWBrown 	= 4096

UNSIGNEDLONG	c_InactiveTextMask 	= 8192
UNSIGNEDLONG	c_InactiveText 		= 0
UNSIGNEDLONG	c_NoInactiveText 		= 8192

UNSIGNEDLONG	c_InactiveLineMask 	= 16384
UNSIGNEDLONG	c_InactiveLine 		= 0
UNSIGNEDLONG	c_NoInactiveLine 		= 16384

UNSIGNEDLONG	c_InactiveColMask 	= 32768
UNSIGNEDLONG	c_InactiveCol 		= 0
UNSIGNEDLONG	c_NoInactiveCol 		= 32768

UNSIGNEDLONG	c_CursorRowFocusRectMask = 65536
UNSIGNEDLONG	c_CursorRowFocusRect 	= 0
UNSIGNEDLONG	c_NoCursorRowFocusRect 	= 65536

UNSIGNEDLONG	c_CursorRowPointerMask 	= 131072
UNSIGNEDLONG	c_CursorRowPointer 	= 0
UNSIGNEDLONG	c_NoCursorRowPointer 	= 131072

UNSIGNEDLONG	c_DWStyleMask 		= 786432
UNSIGNEDLONG	c_CalcDWStyle 		= 0
UNSIGNEDLONG	c_FreeFormStyle 		= 262144
UNSIGNEDLONG	c_TabularFormStyle 	= 524288

UNSIGNEDLONG	c_HighlightSelectedMask 	= 3145728
UNSIGNEDLONG	c_AutoCalcHighlightSelected = 0
UNSIGNEDLONG	c_HighlightSelected 	= 1048576
UNSIGNEDLONG	c_NoHighlightSelected 	= 2097152

UNSIGNEDLONG	c_ResizeDWMask 		= 4194304
UNSIGNEDLONG	c_NoResizeDW 		= 0
UNSIGNEDLONG	c_ResizeDW 		= 4194304

UNSIGNEDLONG	c_AutoConfigMenusMask 	= 8388608
UNSIGNEDLONG	c_NoAutoConfigMenus 	= 0
UNSIGNEDLONG	c_AutoConfigMenus 	= 8388608

UNSIGNEDLONG	c_NoShowEmptyMask 	= 16777216
UNSIGNEDLONG	c_ShowEmpty 		= 0
UNSIGNEDLONG	c_NoShowEmpty 		= 16777216

UNSIGNEDLONG	c_NoAutoFocusMask 	= 33554432
UNSIGNEDLONG	c_AutoFocus 		= 0
UNSIGNEDLONG	c_NoAutoFocus 		= 33554432

UNSIGNEDLONG	c_CCErrorColorMask 	= 1006632960
UNSIGNEDLONG	c_CCErrorRed 		= 0
UNSIGNEDLONG	c_CCErrorBlack 		= 67108864
UNSIGNEDLONG	c_CCErrorWhite 		= 134217728
UNSIGNEDLONG	c_CCErrorGray 		= 201326592
UNSIGNEDLONG	c_CCErrorLightGray 	= 201326592
UNSIGNEDLONG	c_CCErrorDarkGray 	= 268435456
UNSIGNEDLONG	c_CCErrorDarkRed 		= 335544320
UNSIGNEDLONG	c_CCErrorGreen 		= 402653184
UNSIGNEDLONG	c_CCErrorDarkGreen 	= 469762048
UNSIGNEDLONG	c_CCErrorBlue 		= 536870912
UNSIGNEDLONG	c_CCErrorDarkBlue 	= 603979776
UNSIGNEDLONG	c_CCErrorMagenta 		= 671088640
UNSIGNEDLONG	c_CCErrorDarkMagenta 	= 738197504
UNSIGNEDLONG	c_CCErrorCyan 		= 805306368
UNSIGNEDLONG	c_CCErrorDarkCyan 	= 872415232
UNSIGNEDLONG	c_CCErrorYellow 		= 939524096
UNSIGNEDLONG	c_CCErrorBrown 		= 1006632960

end variables

forward prototypes
public subroutine fu_initdefaultbits ()
public subroutine fu_buildgettabstring (uo_dw_main this_obj, long lnumcolumns, long lmaxlength, ref string lpsztabstring)
public subroutine fu_parsetabstring (uo_dw_main this_obj, long lnumcolumns, string lpszdwdescribe, ref long ltaborder[], ref boolean icolvisible[])
public subroutine fu_buildgetattribstring (uo_dw_main this_obj, long lnumcolumns, boolean icolvisible[], long lmaxlength, ref string lpszattribstring)
public subroutine fu_parseattribstring (uo_dw_main this_obj, long lnumcolumns, boolean icolvisible[], string lpszdwdescribe, ref long lcolborders[], ref long lcolcolors[], ref long lcolbcolors[])
public subroutine fu_buildattribstrings (uo_dw_main this_obj, long lnumcolumns, long ltaborder[], boolean icolvisible[], long lcolborders[], long lcolcolors[], long lcolbcolors[], string lpszviewmodeborder, string lpszviewmodecolor, long lmaxlength, ref string lpszmodifyborders, ref string lpszviewborders, ref string lpszmodifycolors, ref string lpszviewcolors, ref string lpsznonemptytextcolors, ref string lpszemptytextcolors)
public subroutine fu_buildgetobjtypestring (uo_dw_main this_obj, string lpszobjnames, long lmaxlength, ref string lpszobjtypestring, ref long lnumobjects)
public subroutine fu_parseobjtypestring (uo_dw_main this_obj, long lnumobjects, string lpszobjnames, string lpszdwdescribe, long lmaxlength, ref string lpszobjcolorstring, ref string lpszobjectnames[], ref long lobjecttypes[])
public subroutine fu_parseobjcolorstring (uo_dw_main this_obj, long lnumobjects, long lobjecttypes[], string lpszdwdescribe, ref long lobjectcolors[])
public subroutine fu_psbuildgettabstring (uo_dw_main this_obj, long lnumcolumns, long lmaxlength, ref string lpsztabstring)
public subroutine fu_psparsetabstring (uo_dw_main this_obj, long lnumcolumns, string lpszdwdescribe, ref long ltaborder[], ref boolean icolvisible[])
public subroutine fu_psbuildgetattribstring (uo_dw_main this_obj, long lnumcolumns, boolean icolvisible[], long lmaxlength, ref string lpszattribstring)
public subroutine fu_psparseattribstring (uo_dw_main this_obj, long lnumcolumns, boolean icolvisible[], string lpszdwdescribe, ref long lcolborders[], ref long lcolcolors[], ref long lcolbcolors[])
public subroutine fu_psbuildattribstrings (uo_dw_main this_obj, long lnumcolumns, long ltaborder[], boolean icolvisible[], long lcolborders[], long lcolcolors[], long lcolbcolors[], string lpszviewmodeborder, string lpszviewmodecolor, long lmaxlength, ref string lpszmodifyborders, ref string lpszviewborders, ref string lpszmodifycolors, ref string lpszviewcolors, ref string lpsznonemptytextcolors, ref string lpszemptytextcolors)
public subroutine fu_psbuildgetobjtypestring (uo_dw_main this_obj, string lpszobjnames, long lmaxlength, ref string lpszobjtypestring, ref long lnumobjects)
public subroutine fu_psparseobjtypestring (uo_dw_main this_obj, long lnumobjects, string lpszobjnames, string lpszdwdescribe, long lmaxlength, ref string lpszobjcolorstring, ref string lpszobjectnames[], ref long lobjecttypes[])
public subroutine fu_psparseobjcolorstring (uo_dw_main this_obj, long lnumobjects, long lobjecttypes[], string lpszdwdescribe, ref long lobjectcolors[])
public subroutine fu_ctinitdwbits (uo_container_main this_obj)
public subroutine fu_initctbits (uo_container_main this_obj)
public subroutine fu_initmdibits (w_mdi_frame this_obj)
public subroutine fu_initwmbits (w_main this_obj)
public subroutine fu_initwrbits (w_response this_obj)
public subroutine fu_setctdefaults (w_pcmanager_main this_obj, unsignedlong lcontrolword)
public subroutine fu_setctoptions (uo_container_main this_obj, unsignedlong lcontrolword)
public subroutine fu_setdwdefaults (w_pcmanager_main this_obj, unsignedlong lcontrolword, unsignedlong lvisualword)
public subroutine fu_setdwoptions (uo_dw_main this_obj, unsignedlong lcontrolword, unsignedlong lvisualword)
public subroutine fu_setdwpmenu (uo_dw_main this_obj)
public subroutine fu_setmdidefaults (w_pcmanager_main this_obj, unsignedlong lcontrolword)
public subroutine fu_setmdioptions (w_mdi_frame this_obj, unsignedlong lcontrolword)
public subroutine fu_setmdipmenu (w_mdi_frame this_obj)
public subroutine fu_setwdefaults (w_pcmanager_main this_obj, unsignedlong lcontrolword)
public subroutine fu_setwmoptions (w_main this_obj, unsignedlong lcontrolword)
public subroutine fu_setwmpmenu (w_main this_obj)
public subroutine fu_setwroptions (w_response this_obj, unsignedlong lcontrolword)
public subroutine fu_setwrpmenu (w_response this_obj)
public subroutine fu_wminitdwbits (w_main this_obj)
public subroutine fu_wrinitdwbits (w_response this_obj)
public subroutine fu_buildactivestrings (uo_dw_main this_obj, long lnumobjects, string lpszobjectnames[], long lobjecttypes[], long lobjectcolors[], boolean binactivecol, boolean binactivetext, boolean binactiveline, string lpszinactivedwcolor, string lpszviewmodecolor, long lmaxlength, ref string lpszactivenonemptycolors, ref string lpszactiveemptycolors, ref string lpszinactivenonemptycolors, ref string lpszinactiveemptycolors)
public subroutine fu_buildtabstrings (uo_dw_main this_obj, long lnumcolumns, long ltaborder[], boolean icolvisible[], ref long lfirsttabcolumn, ref long lfirstviscolumn, long lmaxlength, ref string lpszenabletabs, ref string lpszdisabletabs, ref string lpszquerytabs)
public subroutine fu_psbuildactivestrings (uo_dw_main this_obj, long lnumobjects, string lpszobjectnames[], long lobjecttypes[], long lobjectcolors[], long lcolbcolors[], boolean binactivecol, boolean binactivetext, boolean binactiveline, string lpszinactivedwcolor, string lpszviewmodecolor, long lmaxlength, ref string lpszactivenonemptycolors, ref string lpszactiveemptycolors, ref string lpszinactivenonemptycolors, ref string lpszinactiveemptycolors)
public subroutine fu_psbuildtabstrings (uo_dw_main this_obj, long lnumcolumns, long ltaborder[], boolean icolvisible[], ref long lfirsttabcolumn, ref long lfirstviscolumn, long lmaxlength, ref string lpszenabletabs, ref string lpszdisabletabs, ref string lpszquerytabs)
end prototypes

public subroutine fu_initdefaultbits ();//******************************************************************
//  PC Module     : n_DLL_Main
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

//------------------------------------------------------------------
//  MDI Window Bits
//------------------------------------------------------------------

PCCA.PCMGR.c_MDI_AllowRedraw       = c_MDI_AllowRedraw
PCCA.PCMGR.c_MDI_NoAllowRedraw     = c_MDI_NoAllowRedraw

PCCA.PCMGR.c_MDI_NoShowResources   = c_MDI_NoShowResources
PCCA.PCMGR.c_MDI_ShowResources     = c_MDI_ShowResources

PCCA.PCMGR.c_MDI_NoShowClock       = c_MDI_NoShowClock
PCCA.PCMGR.c_MDI_ShowClock         = c_MDI_ShowClock

PCCA.PCMGR.c_MDI_NoAutoConfigMenus = c_MDI_NoAutoConfigMenus
PCCA.PCMGR.c_MDI_AutoConfigMenus   = c_MDI_AutoConfigMenus

PCCA.PCMGR.c_MDI_NoSavePosition    = c_MDI_NoSavePosition
PCCA.PCMGR.c_MDI_SavePosition      = c_MDI_SavePosition

PCCA.PCMGR.c_MDI_ToolBarNone       = c_MDI_ToolBarNone
PCCA.PCMGR.c_MDI_ToolBarTop        = c_MDI_ToolBarTop
PCCA.PCMGR.c_MDI_ToolBarBottom     = c_MDI_ToolBarBottom
PCCA.PCMGR.c_MDI_ToolBarLeft       = c_MDI_ToolBarLeft
PCCA.PCMGR.c_MDI_ToolBarRight      = c_MDI_ToolBarRight
PCCA.PCMGR.c_MDI_ToolBarFloat      = c_MDI_ToolBarFloat

PCCA.PCMGR.c_MDI_ToolBarHideText   = c_MDI_ToolBarHideText
PCCA.PCMGR.c_MDI_ToolBarShowText   = c_MDI_ToolBarShowText

//------------------------------------------------------------------
//  Window Bits
//------------------------------------------------------------------

PCCA.PCMGR.c_LoadCodeTable       = c_LoadCodeTable
PCCA.PCMGR.c_NoLoadCodeTable     = c_NoLoadCodeTable

PCCA.PCMGR.c_EnablePopup         = c_EnablePopup
PCCA.PCMGR.c_NoEnablePopup       = c_NoEnablePopup

PCCA.PCMGR.c_NoResizeWin         = c_NoResizeWin
PCCA.PCMGR.c_ResizeWin           = c_ResizeWin
PCCA.PCMGR.c_ZoomWin             = c_ZoomWin

PCCA.PCMGR.c_NoAutoMinimize      = c_NoAutoMinimize
PCCA.PCMGR.c_AutoMinimize        = c_AutoMinimize

PCCA.PCMGR.c_NoAutoSize          = c_NoAutoSize
PCCA.PCMGR.c_AutoSize            = c_AutoSize
PCCA.PCMGR.c_AutoSize30          = c_AutoSize30

PCCA.PCMGR.c_NoAutoPosition      = c_NoAutoPosition
PCCA.PCMGR.c_AutoPosition        = c_AutoPosition

PCCA.PCMGR.c_NoSavePosition      = c_NoSavePosition
PCCA.PCMGR.c_SavePosition        = c_SavePosition

PCCA.PCMGR.c_ClosePromptUser     = c_ClosePromptUser
PCCA.PCMGR.c_ClosePromptUserOnce = c_ClosePromptUserOnce
PCCA.PCMGR.c_CloseSave           = c_CloseSave
PCCA.PCMGR.c_CloseNoSave         = c_CloseNoSave

PCCA.PCMGR.c_ToolBarNone         = c_ToolBarNone
PCCA.PCMGR.c_ToolBarTop          = c_ToolBarTop
PCCA.PCMGR.c_ToolBarBottom       = c_ToolBarBottom
PCCA.PCMGR.c_ToolBarLeft         = c_ToolBarLeft
PCCA.PCMGR.c_ToolBarRight        = c_ToolBarRight
PCCA.PCMGR.c_ToolBarFloat        = c_ToolBarFloat

PCCA.PCMGR.c_ToolBarHideText     = c_ToolBarHideText
PCCA.PCMGR.c_ToolBarShowText     = c_ToolBarShowText

//------------------------------------------------------------------
//  Container Bits
//------------------------------------------------------------------

PCCA.PCMGR.c_CT_LoadCodeTable       = c_CT_LoadCodeTable
PCCA.PCMGR.c_CT_NoLoadCodeTable     = c_CT_NoLoadCodeTable

PCCA.PCMGR.c_CT_NoResizeCont        = c_CT_NoResizeCont
PCCA.PCMGR.c_CT_ResizeCont          = c_CT_ResizeCont
PCCA.PCMGR.c_CT_ZoomCont            = c_CT_ZoomCont

PCCA.PCMGR.c_CT_ClosePromptUser     = c_CT_ClosePromptUser
PCCA.PCMGR.c_CT_ClosePromptUserOnce = c_CT_ClosePromptUserOnce
PCCA.PCMGR.c_CT_CloseSave           = c_CT_CloseSave
PCCA.PCMGR.c_CT_CloseNoSave         = c_CT_CloseNoSave

PCCA.PCMGR.c_CT_BringToTop          = c_CT_BringToTop
PCCA.PCMGR.c_CT_NoBringToTop        = c_CT_NoBringToTop

//------------------------------------------------------------------
//  DataWindow Bits
//------------------------------------------------------------------

PCCA.PCMGR.c_MasterList                = c_MasterList
PCCA.PCMGR.c_MasterEdit                = c_MasterEdit
PCCA.PCMGR.c_DetailList                = c_DetailList
PCCA.PCMGR.c_DetailEdit                = c_DetailEdit

PCCA.PCMGR.c_ScrollSelf                = c_ScrollSelf
PCCA.PCMGR.c_ScrollParent              = c_ScrollParent

PCCA.PCMGR.c_IsNotInstance             = c_IsNotInstance
PCCA.PCMGR.c_IsInstance                = c_IsInstance

PCCA.PCMGR.c_RequiredOnSave            = c_RequiredOnSave
PCCA.PCMGR.c_AlwaysCheckRequired       = c_AlwaysCheckRequired

PCCA.PCMGR.c_NoNew                     = c_NoNew
PCCA.PCMGR.c_NewOk                     = c_NewOk

PCCA.PCMGR.c_NoModify                  = c_NoModify
PCCA.PCMGR.c_ModifyOk                  = c_ModifyOk

PCCA.PCMGR.c_NoDelete                  = c_NoDelete
PCCA.PCMGR.c_DeleteOk                  = c_DeleteOk

PCCA.PCMGR.c_NoQuery                   = c_NoQuery
PCCA.PCMGR.c_QueryOk                   = c_QueryOk

PCCA.PCMGR.c_SelectOnDoubleClick       = c_SelectOnDoubleClick
PCCA.PCMGR.c_SelectOnClick             = c_SelectOnClick
PCCA.PCMGR.c_SelectOnRowFocusChange    = c_SelectOnRowFocusChange

PCCA.PCMGR.c_NoDrillDown               = c_NoDrillDown
PCCA.PCMGR.c_DrillDown                 = c_DrillDown

PCCA.PCMGR.c_SameModeOnSelect          = c_SameModeOnSelect
PCCA.PCMGR.c_ViewOnSelect              = c_ViewOnSelect
PCCA.PCMGR.c_ModifyOnSelect            = c_ModifyOnSelect

PCCA.PCMGR.c_NoMultiSelect             = c_NoMultiSelect
PCCA.PCMGR.c_MultiSelect               = c_MultiSelect

PCCA.PCMGR.c_RefreshOnSelect           = c_RefreshOnSelect
PCCA.PCMGR.c_NoAutoRefresh             = c_NoAutoRefresh
PCCA.PCMGR.c_RefreshOnMultiSelect      = c_RefreshOnMultiSelect

PCCA.PCMGR.c_ParentModeOnOpen          = c_ParentModeOnOpen
PCCA.PCMGR.c_ViewOnOpen                = c_ViewOnOpen
PCCA.PCMGR.c_ModifyOnOpen              = c_ModifyOnOpen
PCCA.PCMGR.c_NewOnOpen                 = c_NewOnOpen

PCCA.PCMGR.c_SameModeAfterSave         = c_SameModeAfterSave
PCCA.PCMGR.c_ViewAfterSave             = c_ViewAfterSave

PCCA.PCMGR.c_NoEnableModifyOnOpen      = c_NoEnableModifyOnOpen
PCCA.PCMGR.c_EnableModifyOnOpen        = c_EnableModifyOnOpen

PCCA.PCMGR.c_NoEnableNewOnOpen         = c_NoEnableNewOnOpen
PCCA.PCMGR.c_EnableNewOnOpen           = c_EnableNewOnOpen

PCCA.PCMGR.c_RetrieveOnOpen            = c_RetrieveOnOpen
PCCA.PCMGR.c_AutoRetrieveOnOpen        = c_AutoRetrieveOnOpen
PCCA.PCMGR.c_NoRetrieveOnOpen          = c_NoRetrieveOnOpen

PCCA.PCMGR.c_AutoRefresh               = c_AutoRefresh
PCCA.PCMGR.c_SkipRefresh               = c_SkipRefresh
PCCA.PCMGR.c_TriggerRefresh            = c_TriggerRefresh
PCCA.PCMGR.c_PostRefresh               = c_PostRefresh

PCCA.PCMGR.c_RetrieveAll               = c_RetrieveAll
PCCA.PCMGR.c_RetrieveAsNeeded          = c_RetrieveAsNeeded

PCCA.PCMGR.c_NoShareData               = c_NoShareData
PCCA.PCMGR.c_ShareData                 = c_ShareData

PCCA.PCMGR.c_NoRefreshParent           = c_NoRefreshParent
PCCA.PCMGR.c_RefreshParent             = c_RefreshParent

PCCA.PCMGR.c_NoRefreshChild            = c_NoRefreshChild
PCCA.PCMGR.c_RefreshChild              = c_RefreshChild

PCCA.PCMGR.c_IgnoreNewRows             = c_IgnoreNewRows
PCCA.PCMGR.c_NoIgnoreNewRows           = c_NoIgnoreNewRows

PCCA.PCMGR.c_NoNewModeOnEmpty          = c_NoNewModeOnEmpty
PCCA.PCMGR.c_NewModeOnEmpty            = c_NewModeOnEmpty

PCCA.PCMGR.c_MultipleNewRows           = c_MultipleNewRows
PCCA.PCMGR.c_OnlyOneNewRow             = c_OnlyOneNewRow

PCCA.PCMGR.c_DisableCC                 = c_DisableCC
PCCA.PCMGR.c_EnableCC                  = c_EnableCC

PCCA.PCMGR.c_DisableCCInsert           = c_DisableCCInsert
PCCA.PCMGR.c_EnableCCInsert            = c_EnableCCInsert

PCCA.PCMGR.c_ViewModeBorderUnchanged   = c_ViewModeBorderUnchanged
PCCA.PCMGR.c_ViewModeNoBorder          = c_ViewModeNoBorder
PCCA.PCMGR.c_ViewModeShadowBox         = c_ViewModeShadowBox
PCCA.PCMGR.c_ViewModeBox               = c_ViewModeBox
PCCA.PCMGR.c_ViewModeResize            = c_ViewModeResize
PCCA.PCMGR.c_ViewModeUnderline         = c_ViewModeUnderline
PCCA.PCMGR.c_ViewModeLowered           = c_ViewModeLowered
PCCA.PCMGR.c_ViewModeRaised            = c_ViewModeRaised

PCCA.PCMGR.c_ViewModeColorUnchanged    = c_ViewModeColorUnchanged
PCCA.PCMGR.c_ViewModeBlack             = c_ViewModeBlack
PCCA.PCMGR.c_ViewModeWhite             = c_ViewModeWhite
PCCA.PCMGR.c_ViewModeGray              = c_ViewModeGray
PCCA.PCMGR.c_ViewModeLightGray         = c_ViewModeLightGray
PCCA.PCMGR.c_ViewModeDarkGray          = c_ViewModeDarkGray
PCCA.PCMGR.c_ViewModeRed               = c_ViewModeRed
PCCA.PCMGR.c_ViewModeDarkRed           = c_ViewModeDarkRed
PCCA.PCMGR.c_ViewModeGreen             = c_ViewModeGreen
PCCA.PCMGR.c_ViewModeDarkGreen         = c_ViewModeDarkGreen
PCCA.PCMGR.c_ViewModeBlue              = c_ViewModeBlue
PCCA.PCMGR.c_ViewModeDarkBlue          = c_ViewModeDarkBlue
PCCA.PCMGR.c_ViewModeMagenta           = c_ViewModeMagenta
PCCA.PCMGR.c_ViewModeDarkMagenta       = c_ViewModeDarkMagenta
PCCA.PCMGR.c_ViewModeCyan              = c_ViewModeCyan
PCCA.PCMGR.c_ViewModeDarkCyan          = c_ViewModeDarkCyan
PCCA.PCMGR.c_ViewModeYellow            = c_ViewModeYellow
PCCA.PCMGR.c_ViewModeBrown             = c_ViewModeBrown

PCCA.PCMGR.c_InactiveDWColorUnchanged  = c_InactiveDWColorUnchanged
PCCA.PCMGR.c_InactiveDWBlack           = c_InactiveDWBlack
PCCA.PCMGR.c_InactiveDWWhite           = c_InactiveDWWhite
PCCA.PCMGR.c_InactiveDWGray            = c_InactiveDWGray
PCCA.PCMGR.c_InactiveDWLightGray       = c_InactiveDWLightGray
PCCA.PCMGR.c_InactiveDWDarkGray        = c_InactiveDWDarkGray
PCCA.PCMGR.c_InactiveDWRed             = c_InactiveDWRed
PCCA.PCMGR.c_InactiveDWDarkRed         = c_InactiveDWDarkRed
PCCA.PCMGR.c_InactiveDWGreen           = c_InactiveDWGreen
PCCA.PCMGR.c_InactiveDWDarkGreen       = c_InactiveDWDarkGreen
PCCA.PCMGR.c_InactiveDWBlue            = c_InactiveDWBlue
PCCA.PCMGR.c_InactiveDWDarkBlue        = c_InactiveDWDarkBlue
PCCA.PCMGR.c_InactiveDWMagenta         = c_InactiveDWMagenta
PCCA.PCMGR.c_InactiveDWDarkMagenta     = c_InactiveDWDarkMagenta
PCCA.PCMGR.c_InactiveDWCyan            = c_InactiveDWCyan
PCCA.PCMGR.c_InactiveDWDarkCyan        = c_InactiveDWDarkCyan
PCCA.PCMGR.c_InactiveDWYellow          = c_InactiveDWYellow
PCCA.PCMGR.c_InactiveDWBrown           = c_InactiveDWBrown

PCCA.PCMGR.c_NoInactiveText            = c_NoInactiveText
PCCA.PCMGR.c_InactiveText              = c_InactiveText
PCCA.PCMGR.c_NoInactiveLine            = c_NoInactiveLine
PCCA.PCMGR.c_InactiveLine              = c_InactiveLine
PCCA.PCMGR.c_NoInactiveCol             = c_NoInactiveCol
PCCA.PCMGR.c_InactiveCol               = c_InactiveCol

PCCA.PCMGR.c_CursorRowFocusRect        = c_CursorRowFocusRect
PCCA.PCMGR.c_NoCursorRowFocusRect      = c_NoCursorRowFocusRect

PCCA.PCMGR.c_CursorRowPointer          = c_CursorRowPointer
PCCA.PCMGR.c_NoCursorRowPointer        = c_NoCursorRowPointer

PCCA.PCMGR.c_CalcDWStyle               = c_CalcDWStyle
PCCA.PCMGR.c_FreeFormStyle             = c_FreeFormStyle
PCCA.PCMGR.c_TabularFormStyle          = c_TabularFormStyle

PCCA.PCMGR.c_AutoCalcHighlightSelected = c_AutoCalcHighlightSelected
PCCA.PCMGR.c_HighlightSelected         = c_HighlightSelected
PCCA.PCMGR.c_NoHighlightSelected       = c_NoHighlightSelected

PCCA.PCMGR.c_NoResizeDW                = c_NoResizeDW
PCCA.PCMGR.c_ResizeDW                  = c_ResizeDW

PCCA.PCMGR.c_NoAutoConfigMenus         = c_NoAutoConfigMenus
PCCA.PCMGR.c_AutoConfigMenus           = c_AutoConfigMenus

PCCA.PCMGR.c_ShowEmpty                 = c_ShowEmpty
PCCA.PCMGR.c_NoShowEmpty               = c_NoShowEmpty

PCCA.PCMGR.c_AutoFocus                 = c_AutoFocus
PCCA.PCMGR.c_NoAutoFocus               = c_NoAutoFocus

PCCA.PCMGR.c_CCErrorRed                = c_CCErrorRed
PCCA.PCMGR.c_CCErrorBlack              = c_CCErrorBlack
PCCA.PCMGR.c_CCErrorWhite              = c_CCErrorWhite
PCCA.PCMGR.c_CCErrorGray               = c_CCErrorGray
PCCA.PCMGR.c_CCErrorLightGray          = c_CCErrorLightGray
PCCA.PCMGR.c_CCErrorDarkGray           = c_CCErrorDarkGray
PCCA.PCMGR.c_CCErrorDarkRed            = c_CCErrorDarkRed
PCCA.PCMGR.c_CCErrorGreen              = c_CCErrorGreen
PCCA.PCMGR.c_CCErrorDarkGreen          = c_CCErrorDarkGreen
PCCA.PCMGR.c_CCErrorBlue               = c_CCErrorBlue
PCCA.PCMGR.c_CCErrorDarkBlue           = c_CCErrorDarkBlue
PCCA.PCMGR.c_CCErrorMagenta            = c_CCErrorMagenta
PCCA.PCMGR.c_CCErrorDarkMagenta        = c_CCErrorDarkMagenta
PCCA.PCMGR.c_CCErrorCyan               = c_CCErrorCyan
PCCA.PCMGR.c_CCErrorDarkCyan           = c_CCErrorDarkCyan
PCCA.PCMGR.c_CCErrorYellow             = c_CCErrorYellow
PCCA.PCMGR.c_CCErrorBrown              = c_CCErrorBrown

end subroutine

public subroutine fu_buildgettabstring (uo_dw_main this_obj, long lnumcolumns, long lmaxlength, ref string lpsztabstring);//******************************************************************
// Code is in the descendent.
//******************************************************************
end subroutine

public subroutine fu_parsetabstring (uo_dw_main this_obj, long lnumcolumns, string lpszdwdescribe, ref long ltaborder[], ref boolean icolvisible[]);//******************************************************************
// Code is in the descendent.
//******************************************************************
end subroutine

public subroutine fu_buildgetattribstring (uo_dw_main this_obj, long lnumcolumns, boolean icolvisible[], long lmaxlength, ref string lpszattribstring);//******************************************************************
// Code is in the descendent.
//******************************************************************
end subroutine

public subroutine fu_parseattribstring (uo_dw_main this_obj, long lnumcolumns, boolean icolvisible[], string lpszdwdescribe, ref long lcolborders[], ref long lcolcolors[], ref long lcolbcolors[]);//******************************************************************
// Code is in the descendent.
//******************************************************************
end subroutine

public subroutine fu_buildattribstrings (uo_dw_main this_obj, long lnumcolumns, long ltaborder[], boolean icolvisible[], long lcolborders[], long lcolcolors[], long lcolbcolors[], string lpszviewmodeborder, string lpszviewmodecolor, long lmaxlength, ref string lpszmodifyborders, ref string lpszviewborders, ref string lpszmodifycolors, ref string lpszviewcolors, ref string lpsznonemptytextcolors, ref string lpszemptytextcolors);//******************************************************************
// Code is in the descendent.
//******************************************************************
end subroutine

public subroutine fu_buildgetobjtypestring (uo_dw_main this_obj, string lpszobjnames, long lmaxlength, ref string lpszobjtypestring, ref long lnumobjects);//******************************************************************
// Code is in the descendent.
//******************************************************************
end subroutine

public subroutine fu_parseobjtypestring (uo_dw_main this_obj, long lnumobjects, string lpszobjnames, string lpszdwdescribe, long lmaxlength, ref string lpszobjcolorstring, ref string lpszobjectnames[], ref long lobjecttypes[]);//******************************************************************
// Code is in the descendent.
//******************************************************************
end subroutine

public subroutine fu_parseobjcolorstring (uo_dw_main this_obj, long lnumobjects, long lobjecttypes[], string lpszdwdescribe, ref long lobjectcolors[]);//******************************************************************
// Code is in the descendent.
//******************************************************************
end subroutine

public subroutine fu_psbuildgettabstring (uo_dw_main this_obj, long lnumcolumns, long lmaxlength, ref string lpsztabstring);//******************************************************************
//  PC Module     : n_DLL_Main
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN  l_SetTab
INTEGER  l_ColNbr
STRING   l_ColPrefix, l_ColNum

lpszTabString = ""

l_SetTab      = FALSE
l_ColPrefix   = "#"

FOR l_ColNbr = 1 TO lNumColumns

   l_ColNum      = l_ColPrefix + String(l_ColNbr)
   lpszTabString = lpszTabString + l_ColNum + ".TabSequence"

   //---------------------------------------------------------------

   IF NOT l_SetTab THEN
      l_SetTab    = TRUE
      l_ColPrefix = "~t#"
      l_ColNum    = l_ColPrefix + String(l_ColNbr)
   END IF

   //---------------------------------------------------------------

   lpszTabString = lpszTabString + l_ColNum + ".Visible"
NEXT

end subroutine

public subroutine fu_psparsetabstring (uo_dw_main this_obj, long lnumcolumns, string lpszdwdescribe, ref long ltaborder[], ref boolean icolvisible[]);//******************************************************************
//  PC Module     : n_DLL_Main
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_ColNbr, l_CurPos, l_EndPos
STRING   l_TabS,   l_VisS

//------------------------------------------------------------------
//  Allocate memory.
//------------------------------------------------------------------

lTabOrder[lNumColumns]   = -1
iColVisible[lNumColumns] = FALSE

//------------------------------------------------------------------
//  Parse the Tab/Visible string passed back from Describe.
//------------------------------------------------------------------

l_CurPos = 1
FOR l_ColNbr = 1 TO lNumColumns

   //---------------------------------------------------------------
   //  We are looking for the tab sequence of this column.
   //---------------------------------------------------------------

   l_EndPos = Pos(lpszdwDescribe, "~n",     l_CurPos)
   l_TabS   = Mid(lpszdwDescribe, l_CurPos, l_EndPos - l_CurPos)
   l_CurPos = l_EndPos + 1

   //---------------------------------------------------------------
   //  If the tab order is valid, convert it from string to long
   //  and save it.
   //---------------------------------------------------------------

   IF l_TabS = "?" THEN
      lTabOrder[l_ColNbr] = -1
   ELSE

      //------------------------------------------------------------
      //  The tab sequence may be a DataWindow expression.
      //------------------------------------------------------------

      IF NOT IsNumber(l_TabS) THEN
         l_TabS = Mid(l_TabS, 2)
      END IF

      lTabOrder[l_ColNbr] = Long(l_TabS)
   END IF

   //---------------------------------------------------------------
   //  We are looking for the visiblitiy of this column.
   //---------------------------------------------------------------

   l_EndPos = Pos(lpszdwDescribe, "~n", l_CurPos)
   IF l_EndPos > 0 THEN
      l_VisS = Mid(lpszdwDescribe, l_CurPos, l_EndPos - l_CurPos)
   ELSE
      l_VisS = Mid(lpszdwDescribe, l_CurPos)
   END IF
   l_CurPos = l_EndPos + 1

   //------------------------------------------------------------
   //  The visibility may be a DataWindow expression.
   //------------------------------------------------------------

   IF NOT IsNumber(l_VisS) THEN
      l_VisS = Mid(l_VisS, 2)
   END IF

   iColVisible[l_ColNbr] = (l_VisS = "1")
NEXT

end subroutine

public subroutine fu_psbuildgetattribstring (uo_dw_main this_obj, long lnumcolumns, boolean icolvisible[], long lmaxlength, ref string lpszattribstring);//******************************************************************
//  PC Module     : n_DLL_Main
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN  l_SetTab
INTEGER  l_ColNbr
STRING   l_ColPrefix, l_ColNum

lpszAttribString = ""

l_SetTab         = FALSE
l_ColPrefix      = "#"

FOR l_ColNbr = 1 TO lNumColumns

   IF iColVisible[l_ColNbr] THEN

      l_ColNum         = l_ColPrefix      + String(l_ColNbr)
      lpszAttribString = lpszAttribString + l_ColNum + ".Border"

      //------------------------------------------------------------

      IF NOT l_SetTab THEN
         l_SetTab    = TRUE
         l_ColPrefix = "~t#"
         l_ColNum    = l_ColPrefix + String(l_ColNbr)
      END IF

      //------------------------------------------------------------

      lpszAttribString = lpszAttribString + &
                         l_ColNum         + &
                         ".Color"
      lpszAttribString = lpszAttribString + &
                         l_ColNum         + &
                         ".BackGround.Color"
   END IF
NEXT

end subroutine

public subroutine fu_psparseattribstring (uo_dw_main this_obj, long lnumcolumns, boolean icolvisible[], string lpszdwdescribe, ref long lcolborders[], ref long lcolcolors[], ref long lcolbcolors[]);//******************************************************************
//  PC Module     : n_DLL_Main
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_ColNbr,  l_CurPos, l_EndPos
STRING   l_BorderS, l_ColorS, l_BColorS

//------------------------------------------------------------------
//  Allocate memory.
//------------------------------------------------------------------

lColBorders[lNumColumns] = -1
lColColors[lNumColumns]  = -1
lColBColors[lNumColumns] = -1

//------------------------------------------------------------------
//  Parse the Border/Color/BackGround.Color string passed back
//  from Describe.
//------------------------------------------------------------------

l_CurPos = 1
FOR l_ColNbr = 1 TO lNumColumns

   IF iColVisible[l_ColNbr] THEN

      //------------------------------------------------------------
      //  We are looking for the border of this column.
      //------------------------------------------------------------

      l_EndPos  = Pos(lpszdwDescribe, "~n", l_CurPos)
      l_BorderS = Mid(lpszdwDescribe, &
                      l_CurPos,       &
                      l_EndPos - l_CurPos)
      l_CurPos  = l_EndPos + 1

      //------------------------------------------------------------
      //  The border may be a DataWindow expression.
      //------------------------------------------------------------

      IF NOT IsNumber(l_BorderS) THEN
         l_BorderS = Mid(l_BorderS, 2)
      END IF

      lColBorders[l_ColNbr] = Long(l_BorderS)

      //------------------------------------------------------------
      //  We are looking for the color of this column.
      //------------------------------------------------------------

      l_EndPos = Pos(lpszdwDescribe, "~n",     l_CurPos)
      l_ColorS = Mid(lpszdwDescribe, l_CurPos, l_EndPos - l_CurPos)
      l_CurPos = l_EndPos + 1

      //------------------------------------------------------------
      //  The color may be a DataWindow expression.
      //------------------------------------------------------------

      IF NOT IsNumber(l_ColorS) THEN
         l_ColorS = Mid(l_ColorS, 2)
      END IF

      lColColors[l_ColNbr] = Long(l_ColorS)

      //------------------------------------------------------------
      //  We are looking for the background color of this column.
      //------------------------------------------------------------

      l_EndPos = Pos(lpszdwDescribe, "~n", l_CurPos)
      IF l_EndPos > 0 THEN
         l_BColorS = Mid(lpszdwDescribe, &
                         l_CurPos,       &
                         l_EndPos - l_CurPos)
      ELSE
         l_BColorS = Mid(lpszdwDescribe, l_CurPos)
      END IF
      l_CurPos = l_EndPos + 1

      IF NOT IsNumber(l_BColorS) THEN
         l_BColorS = Mid(l_BColorS, 2)
      END IF

      lColBColors[l_ColNbr] = Long(l_BColorS)
   ELSE
      lColBorders[l_ColNbr] = -1
      lColColors[l_ColNbr]  = -1
      lColBColors[l_ColNbr] = -1
   END IF
NEXT

end subroutine

public subroutine fu_psbuildattribstrings (uo_dw_main this_obj, long lnumcolumns, long ltaborder[], boolean icolvisible[], long lcolborders[], long lcolcolors[], long lcolbcolors[], string lpszviewmodeborder, string lpszviewmodecolor, long lmaxlength, ref string lpszmodifyborders, ref string lpszviewborders, ref string lpszmodifycolors, ref string lpszviewcolors, ref string lpsznonemptytextcolors, ref string lpszemptytextcolors);//******************************************************************
//  PC Module     : n_DLL_Main
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN  l_SetTab,    l_DoBorders, l_DoColors
INTEGER  l_ColNbr
STRING   l_ColPrefix, l_ColNum,    l_BorderS, l_ColorS, l_BColorS

lpszModifyBorders      = ""
lpszViewBorders        = ""
lpszModifyColors       = ""
lpszViewColors         = ""
lpszNonEmptyTextColors = ""
lpszEmptyTextColors    = ""

l_SetTab               = FALSE
l_DoBorders            = (Len(lpszViewModeBorder) > 0)
l_DoColors             = (Len(lpszViewModeColor)  > 0)
l_ColPrefix            = "#"

FOR l_ColNbr = 1 TO lNumColumns
   IF iColVisible[l_ColNbr] THEN
   IF lTabOrder[l_ColNbr] >= 0 THEN

      l_BorderS = String(lColBorders[l_ColNbr])
      l_ColorS  = String(lColColors[l_ColNbr])
      l_BColorS = String(lColBColors[l_ColNbr])
      l_ColNum  = l_ColPrefix + String(l_ColNbr)

      //------------------------------------------------------------

      IF l_DoBorders THEN
         lpszModifyBorders = lpszModifyBorders + &
                             l_ColNum          + &
                             ".Border="        + &
                             l_BorderS

         IF lTabOrder[l_ColNbr] = 0 THEN
            lpszViewBorders = lpszViewBorders + &
                              l_ColNum        + &
                              ".Border="      + &
                              l_BorderS
         ELSE
            lpszViewBorders = lpszViewBorders + &
                              l_ColNum        + &
                              ".Border="      + &
                              lpszViewModeBorder
         END IF
      END IF

      //------------------------------------------------------------

      IF l_DoColors THEN
         lpszModifyColors = lpszModifyColors     + &
                            l_ColNum             + &
                            ".BackGround.Color=" + &
                            l_BColorS

         IF lTabOrder[l_ColNbr] = 0 THEN
            lpszViewColors = lpszViewColors       + &
                             l_ColNum             + &
                             ".BackGround.Color=" + &
                             l_BColorS
         ELSE
            lpszViewColors = lpszViewColors       + &
                             l_ColNum             + &
                             ".BackGround.Color=" + &
                             lpszViewModeColor
         END IF
      END IF

      //------------------------------------------------------------

      lpszNonEmptyTextColors = lpszNonEmptyTextColors + &
                               l_ColNum               + &
                               ".Color="              + &
                               l_ColorS

      IF lTabOrder[l_ColNbr] = 0 OR NOT l_DoColors THEN
         lpszEmptyTextColors = lpszEmptyTextColors + &
                               l_ColNum            + &
                               ".Color="           + &
                               l_ColorS
      ELSE
         lpszEmptyTextColors = lpszEmptyTextColors + &
                               l_ColNum            + &
                               ".Color="           + &
                               lpszViewModeColor
      END IF

      //------------------------------------------------------------

      IF NOT l_SetTab THEN
         l_SetTab    = TRUE
         l_ColPrefix = "~t#"
      END IF
   END IF
   END IF
NEXT

end subroutine

public subroutine fu_psbuildgetobjtypestring (uo_dw_main this_obj, string lpszobjnames, long lmaxlength, ref string lpszobjtypestring, ref long lnumobjects);//******************************************************************
//  PC Module     : n_DLL_Main
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN  l_SetTab
INTEGER  l_CurPos,  l_EndPos
STRING   l_ObjName, l_Tab

lNumObjects       = 0
lpszObjTypeString = ""

l_SetTab          = FALSE
l_Tab             = ""

//------------------------------------------------------------------
//  While we keep finding DataWindow objects, keep chunking.
//------------------------------------------------------------------

l_CurPos = 1
DO WHILE l_CurPos > 0

   //---------------------------------------------------------------
   //  We are looking for the next object's name.
   //---------------------------------------------------------------

   l_EndPos = Pos(lpszObjNames, "~t", l_CurPos)
   IF l_EndPos > 0 THEN
      l_ObjName = Mid(lpszObjNames, l_CurPos, l_EndPos - l_CurPos)
      l_CurPos  = l_EndPos + 1
   ELSE
      l_ObjName = Mid(lpszObjNames, l_CurPos)
      l_CurPos  = 0
   END IF

   //---------------------------------------------------------------

   lNumObjects       = lNumObjects       + 1
   lpszObjTypeString = lpszObjTypeString + l_Tab + &
                       l_ObjName         + ".Type"

   //---------------------------------------------------------------

   IF NOT l_SetTab THEN
      l_SetTab = TRUE
      l_Tab    = "~t"
   END IF
LOOP

end subroutine

public subroutine fu_psparseobjtypestring (uo_dw_main this_obj, long lnumobjects, string lpszobjnames, string lpszdwdescribe, long lmaxlength, ref string lpszobjcolorstring, ref string lpszobjectnames[], ref long lobjecttypes[]);//******************************************************************
//  PC Module     : n_DLL_Main
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN  l_SetTab
INTEGER  l_ObjNbr,  l_CurPos1, l_EndPos1, l_CurPos2, l_EndPos2
STRING   l_ObjName, l_ObjType, l_Tab

//------------------------------------------------------------------
//  Allocate memory.
//------------------------------------------------------------------

lpszObjectNames[lNumObjects] = ""
lObjectTypes[lNumObjects]    = -1
lpszObjColorString           = ""

//------------------------------------------------------------------
//  Parse the object color string passed back from Describe.
//------------------------------------------------------------------

l_SetTab  = FALSE
l_Tab     = ""

l_CurPos1 = 1
l_CurPos2 = 1
FOR l_ObjNbr = 1 TO lNumObjects

   //---------------------------------------------------------------
   //  We are looking for the next object's name.
   //---------------------------------------------------------------

   l_EndPos1 = Pos(lpszObjNames, "~t", l_CurPos1)
   IF l_EndPos1 > 0 THEN
      l_ObjName = Mid(lpszObjNames, &
                      l_CurPos1,    &
                      l_EndPos1 - l_CurPos1)
   ELSE
      l_ObjName = Mid(lpszObjNames, l_CurPos1)
   END IF
   l_CurPos1 = l_EndPos1 + 1

   lpszObjectNames[l_ObjNbr] = l_ObjName

   //---------------------------------------------------------------
   //  We are looking for the type of this object.
   //---------------------------------------------------------------

   l_EndPos2 = Pos(lpszdwDescribe, "~n", l_CurPos2)
   IF l_EndPos2 > 0 THEN
      l_ObjType = Upper(Mid(lpszdwDescribe, &
                            l_CurPos2,      &
                            l_EndPos2 - l_CurPos2))
   ELSE
      l_ObjType = Upper(Mid(lpszdwDescribe, l_CurPos2))
   END IF
   l_CurPos2 = l_EndPos2 + 1

   CHOOSE CASE l_ObjType
      CASE "COLUMN"
         lObjectTypes[l_ObjNbr] = This_Obj.c_ObjTypeColumn
         lpszObjColorString     = lpszObjColorString + &
                                  l_Tab              + &
                                  l_ObjName          + &
                                  ".Color"

      CASE "TEXT", "COMPUTE"
         lObjectTypes[l_ObjNbr] = This_Obj.c_ObjTypeText
         lpszObjColorString     = lpszObjColorString + &
                                  l_Tab              + &
                                  l_ObjName          + &
                                  ".Color"

      CASE "LINE", "OVAL"
         lObjectTypes[l_ObjNbr] = This_Obj.c_ObjTypeLine
         lpszObjColorString     = lpszObjColorString + &
                                  l_Tab              + &
                                  l_ObjName          + &
                                  ".Pen.Color"

      CASE ELSE
         lObjectTypes[l_ObjNbr] = This_Obj.c_ObjTypeUndefined
   END CHOOSE

   IF NOT l_SetTab THEN
      l_SetTab = TRUE
      l_Tab    = "~t"
   END IF
NEXT

end subroutine

public subroutine fu_psparseobjcolorstring (uo_dw_main this_obj, long lnumobjects, long lobjecttypes[], string lpszdwdescribe, ref long lobjectcolors[]);//******************************************************************
//  PC Module     : n_DLL_Main
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_ObjNbr, l_CurPos, l_EndPos
STRING   l_ObjColor

//------------------------------------------------------------------
//  Allocate memory.
//------------------------------------------------------------------

lObjectColors[lNumObjects] = -1

//------------------------------------------------------------------
//  Parse the object color string passed back from Describe.
//------------------------------------------------------------------

l_CurPos = 1
FOR l_ObjNbr = 1 TO lNumObjects

   IF lObjectTypes[l_ObjNbr] <> This_Obj.c_ObjTypeUndefined THEN

      //------------------------------------------------------------
      //  We are looking for this object's color.
      //------------------------------------------------------------

      l_EndPos = Pos(lpszdwDescribe, "~n", l_CurPos)
      IF l_EndPos > 0 THEN
         l_ObjColor = Mid(lpszdwDescribe, &
                          l_CurPos,       &
                          l_EndPos - l_CurPos)
      ELSE
         l_ObjColor = Mid(lpszdwDescribe, l_CurPos)
      END IF
      l_CurPos = l_EndPos + 1

      //------------------------------------------------------------
      //  The color may be a DataWindow expression.
      //------------------------------------------------------------

      IF l_ObjColor <> "?" THEN
         IF NOT IsNumber(l_ObjColor) THEN
            l_ObjColor = Mid(l_ObjColor, 2)
         END IF
         lObjectColors[l_ObjNbr] = Long(l_ObjColor)
      ELSE
         lObjectColors[l_ObjNbr] = -1
      END IF
   ELSE
      lObjectColors[l_ObjNbr] = -1
   END IF
NEXT

end subroutine

public subroutine fu_ctinitdwbits (uo_container_main this_obj);//******************************************************************
//  PC Module     : n_DLL_Main
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

This_Obj.c_MasterList                = PCCA.PCMGR.c_MasterList
This_Obj.c_MasterEdit                = PCCA.PCMGR.c_MasterEdit
This_Obj.c_DetailList                = PCCA.PCMGR.c_DetailList
This_Obj.c_DetailEdit                = PCCA.PCMGR.c_DetailEdit

This_Obj.c_ScrollSelf                = PCCA.PCMGR.c_ScrollSelf
This_Obj.c_ScrollParent              = PCCA.PCMGR.c_ScrollParent

This_Obj.c_IsNotInstance             = PCCA.PCMGR.c_IsNotInstance
This_Obj.c_IsInstance                = PCCA.PCMGR.c_IsInstance

This_Obj.c_RequiredOnSave            = PCCA.PCMGR.c_RequiredOnSave
This_Obj.c_AlwaysCheckRequired       = PCCA.PCMGR.c_AlwaysCheckRequired

This_Obj.c_NoNew                     = PCCA.PCMGR.c_NoNew
This_Obj.c_NewOk                     = PCCA.PCMGR.c_NewOk

This_Obj.c_NoModify                  = PCCA.PCMGR.c_NoModify
This_Obj.c_ModifyOk                  = PCCA.PCMGR.c_ModifyOk

This_Obj.c_NoDelete                  = PCCA.PCMGR.c_NoDelete
This_Obj.c_DeleteOk                  = PCCA.PCMGR.c_DeleteOk

This_Obj.c_NoQuery                   = PCCA.PCMGR.c_NoQuery
This_Obj.c_QueryOk                   = PCCA.PCMGR.c_QueryOk

This_Obj.c_SelectOnDoubleClick       = PCCA.PCMGR.c_SelectOnDoubleClick
This_Obj.c_SelectOnClick             = PCCA.PCMGR.c_SelectOnClick
This_Obj.c_SelectOnRowFocusChange    = PCCA.PCMGR.c_SelectOnRowFocusChange

This_Obj.c_NoDrillDown               = PCCA.PCMGR.c_NoDrillDown
This_Obj.c_DrillDown                 = PCCA.PCMGR.c_DrillDown

This_Obj.c_SameModeOnSelect          = PCCA.PCMGR.c_SameModeOnSelect
This_Obj.c_ViewOnSelect              = PCCA.PCMGR.c_ViewOnSelect
This_Obj.c_ModifyOnSelect            = PCCA.PCMGR.c_ModifyOnSelect

This_Obj.c_NoMultiSelect             = PCCA.PCMGR.c_NoMultiSelect
This_Obj.c_MultiSelect               = PCCA.PCMGR.c_MultiSelect

This_Obj.c_RefreshOnSelect           = PCCA.PCMGR.c_RefreshOnSelect
This_Obj.c_NoAutoRefresh             = PCCA.PCMGR.c_NoAutoRefresh
This_Obj.c_RefreshOnMultiSelect      = PCCA.PCMGR.c_RefreshOnMultiSelect

This_Obj.c_ParentModeOnOpen          = PCCA.PCMGR.c_ParentModeOnOpen
This_Obj.c_ViewOnOpen                = PCCA.PCMGR.c_ViewOnOpen
This_Obj.c_ModifyOnOpen              = PCCA.PCMGR.c_ModifyOnOpen
This_Obj.c_NewOnOpen                 = PCCA.PCMGR.c_NewOnOpen

This_Obj.c_SameModeAfterSave         = PCCA.PCMGR.c_SameModeAfterSave
This_Obj.c_ViewAfterSave             = PCCA.PCMGR.c_ViewAfterSave

This_Obj.c_NoEnableModifyOnOpen      = PCCA.PCMGR.c_NoEnableModifyOnOpen
This_Obj.c_EnableModifyOnOpen        = PCCA.PCMGR.c_EnableModifyOnOpen

This_Obj.c_NoEnableNewOnOpen         = PCCA.PCMGR.c_NoEnableNewOnOpen
This_Obj.c_EnableNewOnOpen           = PCCA.PCMGR.c_EnableNewOnOpen

This_Obj.c_RetrieveOnOpen            = PCCA.PCMGR.c_RetrieveOnOpen
This_Obj.c_AutoRetrieveOnOpen        = PCCA.PCMGR.c_AutoRetrieveOnOpen
This_Obj.c_NoRetrieveOnOpen          = PCCA.PCMGR.c_NoRetrieveOnOpen

This_Obj.c_AutoRefresh               = PCCA.PCMGR.c_AutoRefresh
This_Obj.c_SkipRefresh               = PCCA.PCMGR.c_SkipRefresh
This_Obj.c_TriggerRefresh            = PCCA.PCMGR.c_TriggerRefresh
This_Obj.c_PostRefresh               = PCCA.PCMGR.c_PostRefresh

This_Obj.c_RetrieveAll               = PCCA.PCMGR.c_RetrieveAll
This_Obj.c_RetrieveAsNeeded          = PCCA.PCMGR.c_RetrieveAsNeeded

This_Obj.c_NoShareData               = PCCA.PCMGR.c_NoShareData
This_Obj.c_ShareData                 = PCCA.PCMGR.c_ShareData

This_Obj.c_NoRefreshParent           = PCCA.PCMGR.c_NoRefreshParent
This_Obj.c_RefreshParent             = PCCA.PCMGR.c_RefreshParent

This_Obj.c_NoRefreshChild            = PCCA.PCMGR.c_NoRefreshChild
This_Obj.c_RefreshChild              = PCCA.PCMGR.c_RefreshChild

This_Obj.c_IgnoreNewRows             = PCCA.PCMGR.c_IgnoreNewRows
This_Obj.c_NoIgnoreNewRows           = PCCA.PCMGR.c_NoIgnoreNewRows

This_Obj.c_NoNewModeOnEmpty          = PCCA.PCMGR.c_NoNewModeOnEmpty
This_Obj.c_NewModeOnEmpty            = PCCA.PCMGR.c_NewModeOnEmpty

This_Obj.c_MultipleNewRows           = PCCA.PCMGR.c_MultipleNewRows
This_Obj.c_OnlyOneNewRow             = PCCA.PCMGR.c_OnlyOneNewRow

This_Obj.c_DisableCC                 = PCCA.PCMGR.c_DisableCC
This_Obj.c_EnableCC                  = PCCA.PCMGR.c_EnableCC

This_Obj.c_DisableCCInsert           = PCCA.PCMGR.c_DisableCCInsert
This_Obj.c_EnableCCInsert            = PCCA.PCMGR.c_EnableCCInsert

This_Obj.c_ViewModeBorderUnchanged   = PCCA.PCMGR.c_ViewModeBorderUnchanged
This_Obj.c_ViewModeNoBorder          = PCCA.PCMGR.c_ViewModeNoBorder
This_Obj.c_ViewModeShadowBox         = PCCA.PCMGR.c_ViewModeShadowBox
This_Obj.c_ViewModeBox               = PCCA.PCMGR.c_ViewModeBox
This_Obj.c_ViewModeResize            = PCCA.PCMGR.c_ViewModeResize
This_Obj.c_ViewModeUnderline         = PCCA.PCMGR.c_ViewModeUnderline
This_Obj.c_ViewModeLowered           = PCCA.PCMGR.c_ViewModeLowered
This_Obj.c_ViewModeRaised            = PCCA.PCMGR.c_ViewModeRaised

This_Obj.c_ViewModeColorUnchanged    = PCCA.PCMGR.c_ViewModeColorUnchanged
This_Obj.c_ViewModeBlack             = PCCA.PCMGR.c_ViewModeBlack
This_Obj.c_ViewModeWhite             = PCCA.PCMGR.c_ViewModeWhite
This_Obj.c_ViewModeGray              = PCCA.PCMGR.c_ViewModeGray
This_Obj.c_ViewModeLightGray         = PCCA.PCMGR.c_ViewModeLightGray
This_Obj.c_ViewModeDarkGray          = PCCA.PCMGR.c_ViewModeDarkGray
This_Obj.c_ViewModeRed               = PCCA.PCMGR.c_ViewModeRed
This_Obj.c_ViewModeDarkRed           = PCCA.PCMGR.c_ViewModeDarkRed
This_Obj.c_ViewModeGreen             = PCCA.PCMGR.c_ViewModeGreen
This_Obj.c_ViewModeDarkGreen         = PCCA.PCMGR.c_ViewModeDarkGreen
This_Obj.c_ViewModeBlue              = PCCA.PCMGR.c_ViewModeBlue
This_Obj.c_ViewModeDarkBlue          = PCCA.PCMGR.c_ViewModeDarkBlue
This_Obj.c_ViewModeMagenta           = PCCA.PCMGR.c_ViewModeMagenta
This_Obj.c_ViewModeDarkMagenta       = PCCA.PCMGR.c_ViewModeDarkMagenta
This_Obj.c_ViewModeCyan              = PCCA.PCMGR.c_ViewModeCyan
This_Obj.c_ViewModeDarkCyan          = PCCA.PCMGR.c_ViewModeDarkCyan
This_Obj.c_ViewModeYellow            = PCCA.PCMGR.c_ViewModeYellow
This_Obj.c_ViewModeBrown             = PCCA.PCMGR.c_ViewModeBrown

This_Obj.c_InactiveDWColorUnchanged  = PCCA.PCMGR.c_InactiveDWColorUnchanged
This_Obj.c_InactiveDWBlack           = PCCA.PCMGR.c_InactiveDWBlack
This_Obj.c_InactiveDWWhite           = PCCA.PCMGR.c_InactiveDWWhite
This_Obj.c_InactiveDWGray            = PCCA.PCMGR.c_InactiveDWGray
This_Obj.c_InactiveDWLightGray       = PCCA.PCMGR.c_InactiveDWLightGray
This_Obj.c_InactiveDWDarkGray        = PCCA.PCMGR.c_InactiveDWDarkGray
This_Obj.c_InactiveDWRed             = PCCA.PCMGR.c_InactiveDWRed
This_Obj.c_InactiveDWDarkRed         = PCCA.PCMGR.c_InactiveDWDarkRed
This_Obj.c_InactiveDWGreen           = PCCA.PCMGR.c_InactiveDWGreen
This_Obj.c_InactiveDWDarkGreen       = PCCA.PCMGR.c_InactiveDWDarkGreen
This_Obj.c_InactiveDWBlue            = PCCA.PCMGR.c_InactiveDWBlue
This_Obj.c_InactiveDWDarkBlue        = PCCA.PCMGR.c_InactiveDWDarkBlue
This_Obj.c_InactiveDWMagenta         = PCCA.PCMGR.c_InactiveDWMagenta
This_Obj.c_InactiveDWDarkMagenta     = PCCA.PCMGR.c_InactiveDWDarkMagenta
This_Obj.c_InactiveDWCyan            = PCCA.PCMGR.c_InactiveDWCyan
This_Obj.c_InactiveDWDarkCyan        = PCCA.PCMGR.c_InactiveDWDarkCyan
This_Obj.c_InactiveDWYellow          = PCCA.PCMGR.c_InactiveDWYellow
This_Obj.c_InactiveDWBrown           = PCCA.PCMGR.c_InactiveDWBrown

This_Obj.c_NoInactiveText            = PCCA.PCMGR.c_NoInactiveText
This_Obj.c_InactiveText              = PCCA.PCMGR.c_InactiveText
This_Obj.c_NoInactiveLine            = PCCA.PCMGR.c_NoInactiveLine
This_Obj.c_InactiveLine              = PCCA.PCMGR.c_InactiveLine
This_Obj.c_NoInactiveCol             = PCCA.PCMGR.c_NoInactiveCol
This_Obj.c_InactiveCol               = PCCA.PCMGR.c_InactiveCol

This_Obj.c_CursorRowFocusRect        = PCCA.PCMGR.c_CursorRowFocusRect
This_Obj.c_NoCursorRowFocusRect      = PCCA.PCMGR.c_NoCursorRowFocusRect

This_Obj.c_CursorRowPointer          = PCCA.PCMGR.c_CursorRowPointer
This_Obj.c_NoCursorRowPointer        = PCCA.PCMGR.c_NoCursorRowPointer

This_Obj.c_CalcDWStyle               = PCCA.PCMGR.c_CalcDWStyle
This_Obj.c_FreeFormStyle             = PCCA.PCMGR.c_FreeFormStyle
This_Obj.c_TabularFormStyle          = PCCA.PCMGR.c_TabularFormStyle

This_Obj.c_AutoCalcHighlightSelected = PCCA.PCMGR.c_AutoCalcHighlightSelected
This_Obj.c_HighlightSelected         = PCCA.PCMGR.c_HighlightSelected
This_Obj.c_NoHighlightSelected       = PCCA.PCMGR.c_NoHighlightSelected

This_Obj.c_NoResizeDW                = PCCA.PCMGR.c_NoResizeDW
This_Obj.c_ResizeDW                  = PCCA.PCMGR.c_ResizeDW

This_Obj.c_NoAutoConfigMenus         = PCCA.PCMGR.c_NoAutoConfigMenus
This_Obj.c_AutoConfigMenus           = PCCA.PCMGR.c_AutoConfigMenus

This_Obj.c_ShowEmpty                 = PCCA.PCMGR.c_ShowEmpty
This_Obj.c_NoShowEmpty               = PCCA.PCMGR.c_NoShowEmpty

This_Obj.c_AutoFocus                 = PCCA.PCMGR.c_AutoFocus
This_Obj.c_NoAutoFocus               = PCCA.PCMGR.c_NoAutoFocus

This_Obj.c_CCErrorRed                = PCCA.PCMGR.c_CCErrorRed
This_Obj.c_CCErrorBlack              = PCCA.PCMGR.c_CCErrorBlack
This_Obj.c_CCErrorWhite              = PCCA.PCMGR.c_CCErrorWhite
This_Obj.c_CCErrorGray               = PCCA.PCMGR.c_CCErrorGray
This_Obj.c_CCErrorLightGray          = PCCA.PCMGR.c_CCErrorLightGray
This_Obj.c_CCErrorDarkGray           = PCCA.PCMGR.c_CCErrorDarkGray
This_Obj.c_CCErrorDarkRed            = PCCA.PCMGR.c_CCErrorDarkRed
This_Obj.c_CCErrorGreen              = PCCA.PCMGR.c_CCErrorGreen
This_Obj.c_CCErrorDarkGreen          = PCCA.PCMGR.c_CCErrorDarkGreen
This_Obj.c_CCErrorBlue               = PCCA.PCMGR.c_CCErrorBlue
This_Obj.c_CCErrorDarkBlue           = PCCA.PCMGR.c_CCErrorDarkBlue
This_Obj.c_CCErrorMagenta            = PCCA.PCMGR.c_CCErrorMagenta
This_Obj.c_CCErrorDarkMagenta        = PCCA.PCMGR.c_CCErrorDarkMagenta
This_Obj.c_CCErrorCyan               = PCCA.PCMGR.c_CCErrorCyan
This_Obj.c_CCErrorDarkCyan           = PCCA.PCMGR.c_CCErrorDarkCyan
This_Obj.c_CCErrorYellow             = PCCA.PCMGR.c_CCErrorYellow
This_Obj.c_CCErrorBrown              = PCCA.PCMGR.c_CCErrorBrown

end subroutine

public subroutine fu_initctbits (uo_container_main this_obj);//******************************************************************
//  PC Module     : n_DLL_Main
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

This_Obj.c_CT_LoadCodeTable       = PCCA.PCMGR.c_CT_LoadCodeTable
This_Obj.c_CT_NoLoadCodeTable     = PCCA.PCMGR.c_CT_NoLoadCodeTable

This_Obj.c_CT_NoResizeCont        = PCCA.PCMGR.c_CT_NoResizeCont
This_Obj.c_CT_ResizeCont          = PCCA.PCMGR.c_CT_ResizeCont
This_Obj.c_CT_ZoomCont            = PCCA.PCMGR.c_CT_ZoomCont

This_Obj.c_CT_ClosePromptUser     = PCCA.PCMGR.c_CT_ClosePromptUser
This_Obj.c_CT_ClosePromptUserOnce = PCCA.PCMGR.c_CT_ClosePromptUserOnce
This_Obj.c_CT_CloseSave           = PCCA.PCMGR.c_CT_CloseSave
This_Obj.c_CT_CloseNoSave         = PCCA.PCMGR.c_CT_CloseNoSave

This_Obj.c_CT_BringToTop          = PCCA.PCMGR.c_CT_BringToTop
This_Obj.c_CT_NoBringToTop        = PCCA.PCMGR.c_CT_NoBringToTop

end subroutine

public subroutine fu_initmdibits (w_mdi_frame this_obj);//******************************************************************
//  PC Module     : n_DLL_Main
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

This_Obj.c_MDI_AllowRedraw       = PCCA.PCMGR.c_MDI_AllowRedraw
This_Obj.c_MDI_NoAllowRedraw     = PCCA.PCMGR.c_MDI_NoAllowRedraw

This_Obj.c_MDI_NoShowResources   = PCCA.PCMGR.c_MDI_NoShowResources
This_Obj.c_MDI_ShowResources     = PCCA.PCMGR.c_MDI_ShowResources

This_Obj.c_MDI_NoShowClock       = PCCA.PCMGR.c_MDI_NoShowClock
This_Obj.c_MDI_ShowClock         = PCCA.PCMGR.c_MDI_ShowClock

This_Obj.c_MDI_NoAutoConfigMenus = PCCA.PCMGR.c_MDI_NoAutoConfigMenus
This_Obj.c_MDI_AutoConfigMenus   = PCCA.PCMGR.c_MDI_AutoConfigMenus

This_Obj.c_MDI_NoSavePosition    = PCCA.PCMGR.c_MDI_NoSavePosition
This_Obj.c_MDI_SavePosition      = PCCA.PCMGR.c_MDI_SavePosition

This_Obj.c_MDI_ToolBarNone       = PCCA.PCMGR.c_MDI_ToolBarNone
This_Obj.c_MDI_ToolBarTop        = PCCA.PCMGR.c_MDI_ToolBarTop
This_Obj.c_MDI_ToolBarBottom     = PCCA.PCMGR.c_MDI_ToolBarBottom
This_Obj.c_MDI_ToolBarLeft       = PCCA.PCMGR.c_MDI_ToolBarLeft
This_Obj.c_MDI_ToolBarRight      = PCCA.PCMGR.c_MDI_ToolBarRight
This_Obj.c_MDI_ToolBarFloat      = PCCA.PCMGR.c_MDI_ToolBarFloat

This_Obj.c_MDI_ToolBarHideText   = PCCA.PCMGR.c_MDI_ToolBarHideText
This_Obj.c_MDI_ToolBarShowText   = PCCA.PCMGR.c_MDI_ToolBarShowText

end subroutine

public subroutine fu_initwmbits (w_main this_obj);//******************************************************************
//  PC Module     : n_DLL_Main
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

This_Obj.c_LoadCodeTable       = PCCA.PCMGR.c_LoadCodeTable
This_Obj.c_NoLoadCodeTable     = PCCA.PCMGR.c_NoLoadCodeTable

This_Obj.c_EnablePopup         = PCCA.PCMGR.c_EnablePopup
This_Obj.c_NoEnablePopup       = PCCA.PCMGR.c_NoEnablePopup

This_Obj.c_NoResizeWin         = PCCA.PCMGR.c_NoResizeWin
This_Obj.c_ResizeWin           = PCCA.PCMGR.c_ResizeWin
This_Obj.c_ZoomWin             = PCCA.PCMGR.c_ZoomWin

This_Obj.c_NoAutoMinimize      = PCCA.PCMGR.c_NoAutoMinimize
This_Obj.c_AutoMinimize        = PCCA.PCMGR.c_AutoMinimize

This_Obj.c_NoAutoSize          = PCCA.PCMGR.c_NoAutoSize
This_Obj.c_AutoSize            = PCCA.PCMGR.c_AutoSize
This_Obj.c_AutoSize30          = PCCA.PCMGR.c_AutoSize30

This_Obj.c_NoAutoPosition      = PCCA.PCMGR.c_NoAutoPosition
This_Obj.c_AutoPosition        = PCCA.PCMGR.c_AutoPosition

This_Obj.c_NoSavePosition      = PCCA.PCMGR.c_NoSavePosition
This_Obj.c_SavePosition        = PCCA.PCMGR.c_SavePosition

This_Obj.c_ClosePromptUser     = PCCA.PCMGR.c_ClosePromptUser
This_Obj.c_ClosePromptUserOnce = PCCA.PCMGR.c_ClosePromptUserOnce
This_Obj.c_CloseSave           = PCCA.PCMGR.c_CloseSave
This_Obj.c_CloseNoSave         = PCCA.PCMGR.c_CloseNoSave

This_Obj.c_ToolBarNone         = PCCA.PCMGR.c_ToolBarNone
This_Obj.c_ToolBarTop          = PCCA.PCMGR.c_ToolBarTop
This_Obj.c_ToolBarBottom       = PCCA.PCMGR.c_ToolBarBottom
This_Obj.c_ToolBarLeft         = PCCA.PCMGR.c_ToolBarLeft
This_Obj.c_ToolBarRight        = PCCA.PCMGR.c_ToolBarRight
This_Obj.c_ToolBarFloat        = PCCA.PCMGR.c_ToolBarFloat

This_Obj.c_ToolBarHideText     = PCCA.PCMGR.c_ToolBarHideText
This_Obj.c_ToolBarShowText     = PCCA.PCMGR.c_ToolBarShowText

end subroutine

public subroutine fu_initwrbits (w_response this_obj);//******************************************************************
//  PC Module     : n_DLL_Main
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

This_Obj.c_LoadCodeTable       = PCCA.PCMGR.c_LoadCodeTable
This_Obj.c_NoLoadCodeTable     = PCCA.PCMGR.c_NoLoadCodeTable

This_Obj.c_EnablePopup         = PCCA.PCMGR.c_EnablePopup
This_Obj.c_NoEnablePopup       = PCCA.PCMGR.c_NoEnablePopup

This_Obj.c_NoResizeWin         = PCCA.PCMGR.c_NoResizeWin
This_Obj.c_ResizeWin           = PCCA.PCMGR.c_ResizeWin
This_Obj.c_ZoomWin             = PCCA.PCMGR.c_ZoomWin

This_Obj.c_NoAutoMinimize      = PCCA.PCMGR.c_NoAutoMinimize
This_Obj.c_AutoMinimize        = PCCA.PCMGR.c_AutoMinimize

This_Obj.c_NoAutoSize          = PCCA.PCMGR.c_NoAutoSize
This_Obj.c_AutoSize            = PCCA.PCMGR.c_AutoSize
This_Obj.c_AutoSize30          = PCCA.PCMGR.c_AutoSize30

This_Obj.c_NoAutoPosition      = PCCA.PCMGR.c_NoAutoPosition
This_Obj.c_AutoPosition        = PCCA.PCMGR.c_AutoPosition

This_Obj.c_NoSavePosition      = PCCA.PCMGR.c_NoSavePosition
This_Obj.c_SavePosition        = PCCA.PCMGR.c_SavePosition

This_Obj.c_ClosePromptUser     = PCCA.PCMGR.c_ClosePromptUser
This_Obj.c_ClosePromptUserOnce = PCCA.PCMGR.c_ClosePromptUserOnce
This_Obj.c_CloseSave           = PCCA.PCMGR.c_CloseSave
This_Obj.c_CloseNoSave         = PCCA.PCMGR.c_CloseNoSave

This_Obj.c_ToolBarNone         = PCCA.PCMGR.c_ToolBarNone
This_Obj.c_ToolBarTop          = PCCA.PCMGR.c_ToolBarTop
This_Obj.c_ToolBarBottom       = PCCA.PCMGR.c_ToolBarBottom
This_Obj.c_ToolBarLeft         = PCCA.PCMGR.c_ToolBarLeft
This_Obj.c_ToolBarRight        = PCCA.PCMGR.c_ToolBarRight
This_Obj.c_ToolBarFloat        = PCCA.PCMGR.c_ToolBarFloat

This_Obj.c_ToolBarHideText     = PCCA.PCMGR.c_ToolBarHideText
This_Obj.c_ToolBarShowText     = PCCA.PCMGR.c_ToolBarShowText

end subroutine

public subroutine fu_setctdefaults (w_pcmanager_main this_obj, unsignedlong lcontrolword);//******************************************************************
//  PC Module     : n_DLL_Main
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER       l_Idx
UNSIGNEDLONG  l_OptionWord, l_OptionBit, l_Divided, l_BitMask

l_OptionWord = lControlWord

//------------------------------------------------------------------

l_OptionBit  = 1
l_Divided    = l_OptionWord / 2
l_OptionWord = l_Divided

FOR l_Idx = 1 TO 15
   l_OptionBit  = 2 * l_OptionBit
   l_Divided    = l_OptionWord / 2
   l_OptionWord = l_Divided
NEXT

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   PCCA.PCMGR.c_CT_LoadCodeTable   = c_CT_LoadCodeTableMask
   PCCA.PCMGR.c_CT_NoLoadCodeTable = c_Default
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_BitMask = 0

FOR l_Idx = 1 TO 2
   l_OptionBit = 2 * l_OptionBit
   l_Divided   = l_OptionWord / 2
   IF (2 * l_Divided) <> l_OptionWord THEN
      l_BitMask = l_BitMask + l_OptionBit
   END IF
   l_OptionWord = l_Divided
NEXT

IF l_BitMask <> c_Default THEN
   CHOOSE CASE l_BitMask
      CASE c_CT_ResizeCont
         PCCA.PCMGR.c_CT_NoResizeCont = l_BitMask
         PCCA.PCMGR.c_CT_ResizeCont   = c_Default

      CASE c_CT_ZoomCont
         PCCA.PCMGR.c_CT_NoResizeCont = l_BitMask
         PCCA.PCMGR.c_CT_ZoomCont     = c_Default

      CASE ELSE
         PCCA.PCMGR.c_CT_NoResizeCont = l_BitMask
         PCCA.PCMGR.c_CT_ZoomCont     = c_Default
   END CHOOSE
END IF

//------------------------------------------------------------------

l_BitMask = 0

FOR l_Idx = 1 TO 2
   l_OptionBit = 2 * l_OptionBit
   l_Divided   = l_OptionWord / 2
   IF (2 * l_Divided) <> l_OptionWord THEN
      l_BitMask = l_BitMask + l_OptionBit
   END IF
   l_OptionWord = l_Divided
NEXT

IF l_BitMask <> c_Default THEN
   CHOOSE CASE l_BitMask
      CASE c_CT_ClosePromptUserOnce
         PCCA.PCMGR.c_CT_ClosePromptUser     = l_BitMask
         PCCA.PCMGR.c_CT_ClosePromptUserOnce = c_Default

      CASE c_CT_CloseSave
         PCCA.PCMGR.c_CT_ClosePromptUser     = l_BitMask
         PCCA.PCMGR.c_CT_CloseSave           = c_Default

      CASE c_CT_CloseNoSave
         PCCA.PCMGR.c_CT_ClosePromptUser     = l_BitMask
         PCCA.PCMGR.c_CT_CloseNoSave         = c_Default

      CASE ELSE
         // Should not get here.
   END CHOOSE
END IF

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   PCCA.PCMGR.c_CT_BringToTop   = c_CT_BringToTopMask
   PCCA.PCMGR.c_CT_NoBringToTop = c_Default
END IF
l_OptionWord = l_Divided
end subroutine

public subroutine fu_setctoptions (uo_container_main this_obj, unsignedlong lcontrolword);//******************************************************************
//  PC Module     : n_DLL_Main
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER       l_Idx
UNSIGNEDLONG  l_OptionWord, l_OptionBit, l_Divided, l_BitMask

l_OptionWord = lControlWord

//------------------------------------------------------------------

l_OptionBit  = 1
l_Divided    = l_OptionWord / 2
l_OptionWord = l_Divided

FOR l_Idx = 1 TO 15
   l_OptionBit  = 2 * l_OptionBit
   l_Divided    = l_OptionWord / 2
   l_OptionWord = l_Divided
NEXT

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   This_Obj.i_CT_LoadCodeTable = (PCCA.PCMGR.c_CT_LoadCodeTable = l_OptionBit)
ELSE
   This_Obj.i_CT_LoadCodeTable = (PCCA.PCMGR.c_CT_LoadCodeTable = 0)
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_BitMask = 0

FOR l_Idx = 1 TO 2
   l_OptionBit = 2 * l_OptionBit
   l_Divided   = l_OptionWord / 2
   IF (2 * l_Divided) <> l_OptionWord THEN
      l_BitMask = l_BitMask + l_OptionBit
   END IF
   l_OptionWord = l_Divided
NEXT

CHOOSE CASE l_BitMask
   CASE PCCA.PCMGR.c_CT_NoResizeCont
      This_Obj.i_CT_ResizeCont = FALSE
      This_Obj.i_CT_ZoomCont   = FALSE

   CASE PCCA.PCMGR.c_CT_ResizeCont
      This_Obj.i_CT_ResizeCont = TRUE
      This_Obj.i_CT_ZoomCont   = FALSE

   CASE PCCA.PCMGR.c_CT_ZoomCont
      This_Obj.i_CT_ResizeCont = TRUE
      This_Obj.i_CT_ZoomCont   = TRUE

   CASE ELSE
      This_Obj.i_CT_ResizeCont = TRUE
      This_Obj.i_CT_ZoomCont   = TRUE
END CHOOSE

//------------------------------------------------------------------

l_BitMask = 0

FOR l_Idx = 1 TO 2
   l_OptionBit = 2 * l_OptionBit
   l_Divided   = l_OptionWord / 2
   IF (2 * l_Divided) <> l_OptionWord THEN
      l_BitMask = l_BitMask + l_OptionBit
   END IF
   l_OptionWord = l_Divided
NEXT

CHOOSE CASE l_BitMask
   CASE PCCA.PCMGR.c_CT_ClosePromptUserOnce
      This_Obj.i_CT_SaveOnClose = c_SOCPromptUserOnce

   CASE PCCA.PCMGR.c_CT_CloseSave
      This_Obj.i_CT_SaveOnClose = c_SOCSave

   CASE PCCA.PCMGR.c_CT_CloseNoSave
      This_Obj.i_CT_SaveOnClose = c_SOCNoSave

   CASE ELSE
      This_Obj.i_CT_SaveOnClose = c_SOCPromptUser
END CHOOSE

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   This_Obj.i_CT_BringToTop = &
      (PCCA.PCMGR.c_CT_BringToTop = l_OptionBit)
ELSE
   This_Obj.i_CT_BringToTop = &
      (PCCA.PCMGR.c_CT_BringToTop = 0)
END IF
l_OptionWord = l_Divided
end subroutine

public subroutine fu_setdwdefaults (w_pcmanager_main this_obj, unsignedlong lcontrolword, unsignedlong lvisualword);//******************************************************************
//  PC Module     : n_DLL_Main
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER       l_Idx
UNSIGNEDLONG  l_OptionWord, l_OptionBit, l_Divided, l_BitMask

//******************************************************************
//  CONTROL BIT PROCESSING.
//******************************************************************

l_OptionWord = lControlWord

//------------------------------------------------------------------

l_OptionBit = 1
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   PCCA.PCMGR.c_ScrollSelf   = c_ScrollParentMask
   PCCA.PCMGR.c_ScrollParent = c_Default
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   PCCA.PCMGR.c_IsNotInstance = c_IsInstanceMask
   PCCA.PCMGR.c_IsInstance    = c_Default
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   PCCA.PCMGR.c_RequiredOnSave      = c_AlwaysCheckRequiredMask
   PCCA.PCMGR.c_AlwaysCheckRequired = c_Default
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   PCCA.PCMGR.c_NoNew = c_NewOkMask
   PCCA.PCMGR.c_NewOk = c_Default
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   PCCA.PCMGR.c_NoModify = c_ModifyOkMask
   PCCA.PCMGR.c_ModifyOk = c_Default
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   PCCA.PCMGR.c_NoDelete = c_DeleteOkMask
   PCCA.PCMGR.c_DeleteOk = c_Default
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   PCCA.PCMGR.c_NoQuery = c_QueryOkMask
   PCCA.PCMGR.c_QueryOk = c_Default
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_BitMask = 0

FOR l_Idx = 1 TO 2
   l_OptionBit = 2 * l_OptionBit
   l_Divided   = l_OptionWord / 2
   IF (2 * l_Divided) <> l_OptionWord THEN
      l_BitMask = l_BitMask + l_OptionBit
   END IF
   l_OptionWord = l_Divided
NEXT

IF l_BitMask <> c_Default THEN
   CHOOSE CASE l_BitMask
      CASE c_SelectOnClick
         PCCA.PCMGR.c_SelectOnDoubleClick    = l_BitMask
         PCCA.PCMGR.c_SelectOnClick          = c_Default

      CASE c_SelectOnRowFocusChange
         PCCA.PCMGR.c_SelectOnDoubleClick    = l_BitMask
         PCCA.PCMGR.c_SelectOnRowFocusChange = c_Default

      CASE ELSE
         // Should not get here.
   END CHOOSE
END IF

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   PCCA.PCMGR.c_NoDrillDown = c_DrillDownMask
   PCCA.PCMGR.c_DrillDown   = c_Default
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_BitMask = 0

FOR l_Idx = 1 TO 2
   l_OptionBit = 2 * l_OptionBit
   l_Divided   = l_OptionWord / 2
   IF (2 * l_Divided) <> l_OptionWord THEN
      l_BitMask = l_BitMask + l_OptionBit
   END IF
   l_OptionWord = l_Divided
NEXT

IF l_BitMask <> c_Default THEN
   CHOOSE CASE l_BitMask
      CASE c_ViewOnSelect
         PCCA.PCMGR.c_SameModeOnSelect = l_BitMask
         PCCA.PCMGR.c_ViewOnSelect     = c_Default

      CASE c_ModifyOnSelect
         PCCA.PCMGR.c_SameModeOnSelect = l_BitMask
         PCCA.PCMGR.c_ModifyOnSelect   = c_Default

      CASE ELSE
         // Should not get here.
   END CHOOSE
END IF

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   PCCA.PCMGR.c_NoMultiSelect = c_MultiSelectMask
   PCCA.PCMGR.c_MultiSelect   = c_Default
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_BitMask = 0

FOR l_Idx = 1 TO 2
   l_OptionBit = 2 * l_OptionBit
   l_Divided   = l_OptionWord / 2
   IF (2 * l_Divided) <> l_OptionWord THEN
      l_BitMask = l_BitMask + l_OptionBit
   END IF
   l_OptionWord = l_Divided
NEXT

IF l_BitMask <> c_Default THEN
   CHOOSE CASE l_BitMask
      CASE c_NoAutoRefresh
         PCCA.PCMGR.c_RefreshOnSelect      = l_BitMask
         PCCA.PCMGR.c_NoAutoRefresh        = c_Default

      CASE c_RefreshOnMultiSelect
         PCCA.PCMGR.c_RefreshOnSelect      = l_BitMask
         PCCA.PCMGR.c_RefreshOnMultiSelect = c_Default

      CASE ELSE
         // Should not get here.
   END CHOOSE
END IF

//------------------------------------------------------------------

l_BitMask = 0

FOR l_Idx = 1 TO 2
   l_OptionBit = 2 * l_OptionBit
   l_Divided   = l_OptionWord / 2
   IF (2 * l_Divided) <> l_OptionWord THEN
      l_BitMask = l_BitMask + l_OptionBit
   END IF
   l_OptionWord = l_Divided
NEXT

IF l_BitMask <> c_Default THEN
   CHOOSE CASE l_BitMask
      CASE c_ViewOnOpen
         PCCA.PCMGR.c_ParentModeOnOpen = l_BitMask
         PCCA.PCMGR.c_ViewOnOpen       = c_Default

      CASE c_ModifyOnOpen
         PCCA.PCMGR.c_ParentModeOnOpen = l_BitMask
         PCCA.PCMGR.c_ModifyOnOpen     = c_Default

      CASE c_NewOnOpen
         PCCA.PCMGR.c_ParentModeOnOpen = l_BitMask
         PCCA.PCMGR.c_NewOnOpen        = c_Default

      CASE ELSE
         // Should not get here.
   END CHOOSE
END IF

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   PCCA.PCMGR.c_SameModeAfterSave = c_ViewAfterSaveMask
   PCCA.PCMGR.c_ViewAfterSave     = c_Default
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   PCCA.PCMGR.c_NoEnableModifyOnOpen = c_EnableModifyOnOpenMask
   PCCA.PCMGR.c_EnableModifyOnOpen   = c_Default
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   PCCA.PCMGR.c_NoEnableNewOnOpen = c_EnableNewOnOpenMask
   PCCA.PCMGR.c_EnableNewOnOpen   = c_Default
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   PCCA.PCMGR.c_RetrieveOnOpen     = c_RetrieveOnOpenMask
   PCCA.PCMGR.c_AutoRetrieveOnOpen = c_RetrieveOnOpenMask
   PCCA.PCMGR.c_NoRetrieveOnOpen   = c_Default
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_BitMask = 0

FOR l_Idx = 1 TO 2
   l_OptionBit = 2 * l_OptionBit
   l_Divided   = l_OptionWord / 2
   IF (2 * l_Divided) <> l_OptionWord THEN
      l_BitMask = l_BitMask + l_OptionBit
   END IF
   l_OptionWord = l_Divided
NEXT

IF l_BitMask <> c_Default THEN
   CHOOSE CASE l_BitMask
      CASE c_SkipRefresh
         PCCA.PCMGR.c_AutoRefresh    = l_BitMask
         PCCA.PCMGR.c_SkipRefresh    = c_Default

      CASE c_PostRefresh
         PCCA.PCMGR.c_AutoRefresh    = l_BitMask
         PCCA.PCMGR.c_PostRefresh    = c_Default

      CASE c_TriggerRefresh
         PCCA.PCMGR.c_AutoRefresh    = l_BitMask
         PCCA.PCMGR.c_TriggerRefresh = c_Default

      CASE ELSE
         // Should not get here.
   END CHOOSE
END IF

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   PCCA.PCMGR.c_RetrieveAll      = c_RetrieveAsNeededMask
   PCCA.PCMGR.c_RetrieveAsNeeded = c_Default
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   PCCA.PCMGR.c_NoShareData = c_ShareDataMask
   PCCA.PCMGR.c_ShareData   = c_Default
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   PCCA.PCMGR.c_NoRefreshParent = c_RefreshParentMask
   PCCA.PCMGR.c_RefreshParent   = c_Default
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   PCCA.PCMGR.c_NoRefreshChild = c_RefreshChildMask
   PCCA.PCMGR.c_RefreshChild   = c_Default
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   PCCA.PCMGR.c_IgnoreNewRows   = c_NoIgnoreNewRowsMask
   PCCA.PCMGR.c_NoIgnoreNewRows = c_Default
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   PCCA.PCMGR.c_NoNewModeOnEmpty = c_NewModeOnEmptyMask
   PCCA.PCMGR.c_NewModeOnEmpty   = c_Default
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   PCCA.PCMGR.c_MultipleNewRows = c_OnlyOneNewRowMask
   PCCA.PCMGR.c_OnlyOneNewRow   = c_Default
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   PCCA.PCMGR.c_DisableCC = c_EnableCCMask
   PCCA.PCMGR.c_EnableCC  = c_Default
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   PCCA.PCMGR.c_DisableCCInsert = c_EnableCCInsertMask
   PCCA.PCMGR.c_EnableCCInsert  = c_Default
END IF
l_OptionWord = l_Divided

//******************************************************************
//  VISUAL BIT PROCESSING.
//******************************************************************

l_OptionWord = lVisualWord

//------------------------------------------------------------------

l_BitMask   = 0

l_OptionBit = 1
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   l_BitMask = l_BitMask + l_OptionBit
END IF
l_OptionWord = l_Divided

FOR l_Idx = 1 TO 2
   l_OptionBit = 2 * l_OptionBit
   l_Divided   = l_OptionWord / 2
   IF (2 * l_Divided) <> l_OptionWord THEN
      l_BitMask = l_BitMask + l_OptionBit
   END IF
   l_OptionWord = l_Divided
NEXT

IF l_BitMask <> c_Default THEN
   CHOOSE CASE l_BitMask
      // Most likely case ------------------------------------------
      CASE c_ViewModeLowered
         PCCA.PCMGR.c_ViewModeBorderUnchanged = l_BitMask
         PCCA.PCMGR.c_ViewModeLowered         = c_Default

      CASE c_ViewModeNoBorder
         PCCA.PCMGR.c_ViewModeBorderUnchanged = l_BitMask
         PCCA.PCMGR.c_ViewModeNoBorder        = c_Default

      CASE c_ViewModeShadowBox
         PCCA.PCMGR.c_ViewModeBorderUnchanged = l_BitMask
         PCCA.PCMGR.c_ViewModeShadowBox       = c_Default

      CASE c_ViewModeBox
         PCCA.PCMGR.c_ViewModeBorderUnchanged = l_BitMask
         PCCA.PCMGR.c_ViewModeBox             = c_Default

      CASE c_ViewModeResize
         PCCA.PCMGR.c_ViewModeBorderUnchanged = l_BitMask
         PCCA.PCMGR.c_ViewModeResize          = c_Default

      CASE c_ViewModeUnderline
         PCCA.PCMGR.c_ViewModeBorderUnchanged = l_BitMask
         PCCA.PCMGR.c_ViewModeUnderline       = c_Default

      CASE c_ViewModeRaised
         PCCA.PCMGR.c_ViewModeBorderUnchanged = l_BitMask
         PCCA.PCMGR.c_ViewModeRaised          = c_Default

      CASE ELSE
         // Should not get here.
   END CHOOSE
END IF

//------------------------------------------------------------------

l_BitMask = 0

FOR l_Idx = 1 TO 5
   l_OptionBit = 2 * l_OptionBit
   l_Divided   = l_OptionWord / 2
   IF (2 * l_Divided) <> l_OptionWord THEN
      l_BitMask = l_BitMask + l_OptionBit
   END IF
   l_OptionWord = l_Divided
NEXT

IF l_BitMask <> c_Default THEN
   CHOOSE CASE l_BitMask
      // Most likely case ------------------------------------------
      CASE c_ViewModeGray
         PCCA.PCMGR.c_ViewModeColorUnchanged = l_BitMask
         PCCA.PCMGR.c_ViewModeGray           = c_Default
         PCCA.PCMGR.c_ViewModeLightGray      = c_Default

      CASE c_ViewModeBlack
         PCCA.PCMGR.c_ViewModeColorUnchanged = l_BitMask
         PCCA.PCMGR.c_ViewModeBlack          = c_Default

      CASE c_ViewModeWhite
         PCCA.PCMGR.c_ViewModeColorUnchanged = l_BitMask
         PCCA.PCMGR.c_ViewModeWhite          = c_Default

      CASE c_ViewModeDarkGray
         PCCA.PCMGR.c_ViewModeColorUnchanged = l_BitMask
         PCCA.PCMGR.c_ViewModeDarkGray       = c_Default

      CASE c_ViewModeRed
         PCCA.PCMGR.c_ViewModeColorUnchanged = l_BitMask
         PCCA.PCMGR.c_ViewModeRed            = c_Default

      CASE c_ViewModeDarkRed
         PCCA.PCMGR.c_ViewModeColorUnchanged = l_BitMask
         PCCA.PCMGR.c_ViewModeDarkRed        = c_Default

      CASE c_ViewModeGreen
         PCCA.PCMGR.c_ViewModeColorUnchanged = l_BitMask
         PCCA.PCMGR.c_ViewModeGreen          = c_Default

      CASE c_ViewModeDarkGreen
         PCCA.PCMGR.c_ViewModeColorUnchanged = l_BitMask
         PCCA.PCMGR.c_ViewModeDarkGreen      = c_Default

      CASE c_ViewModeBlue
         PCCA.PCMGR.c_ViewModeColorUnchanged = l_BitMask
         PCCA.PCMGR.c_ViewModeBlue           = c_Default

      CASE c_ViewModeDarkBlue
         PCCA.PCMGR.c_ViewModeColorUnchanged = l_BitMask
         PCCA.PCMGR.c_ViewModeDarkBlue       = c_Default

      CASE c_ViewModeMagenta
         PCCA.PCMGR.c_ViewModeColorUnchanged = l_BitMask
         PCCA.PCMGR.c_ViewModeMagenta        = c_Default

      CASE c_ViewModeDarkMagenta
         PCCA.PCMGR.c_ViewModeColorUnchanged = l_BitMask
         PCCA.PCMGR.c_ViewModeDarkMagenta    = c_Default

      CASE c_ViewModeCyan
         PCCA.PCMGR.c_ViewModeColorUnchanged = l_BitMask
         PCCA.PCMGR.c_ViewModeCyan           = c_Default

      CASE c_ViewModeDarkCyan
         PCCA.PCMGR.c_ViewModeColorUnchanged = l_BitMask
         PCCA.PCMGR.c_ViewModeDarkCyan       = c_Default

      CASE c_ViewModeYellow
         PCCA.PCMGR.c_ViewModeColorUnchanged = l_BitMask
         PCCA.PCMGR.c_ViewModeYellow         = c_Default

      CASE c_ViewModeBrown
         PCCA.PCMGR.c_ViewModeColorUnchanged = l_BitMask
         PCCA.PCMGR.c_ViewModeBrown          = c_Default

      CASE ELSE
         // Should not get here.
   END CHOOSE
END IF

//------------------------------------------------------------------

l_BitMask = 0

FOR l_Idx = 1 TO 5
   l_OptionBit = 2 * l_OptionBit
   l_Divided   = l_OptionWord / 2
   IF (2 * l_Divided) <> l_OptionWord THEN
      l_BitMask = l_BitMask + l_OptionBit
   END IF
   l_OptionWord = l_Divided
NEXT

IF l_BitMask <> c_Default THEN
   CHOOSE CASE l_BitMask
      // Most likely case ------------------------------------------
      CASE c_InactiveDWDarkGray
         PCCA.PCMGR.c_InactiveDWColorUnchanged = l_BitMask
         PCCA.PCMGR.c_InactiveDWDarkGray       = c_Default

      CASE c_InactiveDWBlack
         PCCA.PCMGR.c_InactiveDWColorUnchanged = l_BitMask
         PCCA.PCMGR.c_InactiveDWBlack          = c_Default

      CASE c_InactiveDWWhite
         PCCA.PCMGR.c_InactiveDWColorUnchanged = l_BitMask
         PCCA.PCMGR.c_InactiveDWWhite          = c_Default

      CASE c_InactiveDWGray
         PCCA.PCMGR.c_InactiveDWColorUnchanged = l_BitMask
         PCCA.PCMGR.c_InactiveDWGray           = c_Default
         PCCA.PCMGR.c_InactiveDWLightGray      = c_Default

      CASE c_InactiveDWRed
         PCCA.PCMGR.c_InactiveDWColorUnchanged = l_BitMask
         PCCA.PCMGR.c_InactiveDWRed            = c_Default

      CASE c_InactiveDWDarkRed
         PCCA.PCMGR.c_InactiveDWColorUnchanged = l_BitMask
         PCCA.PCMGR.c_InactiveDWDarkRed        = c_Default

      CASE c_InactiveDWGreen
         PCCA.PCMGR.c_InactiveDWColorUnchanged = l_BitMask
         PCCA.PCMGR.c_InactiveDWGreen          = c_Default

      CASE c_InactiveDWDarkGreen
         PCCA.PCMGR.c_InactiveDWColorUnchanged = l_BitMask
         PCCA.PCMGR.c_InactiveDWDarkGreen      = c_Default

      CASE c_InactiveDWBlue
         PCCA.PCMGR.c_InactiveDWColorUnchanged = l_BitMask
         PCCA.PCMGR.c_InactiveDWBlue           = c_Default

      CASE c_InactiveDWDarkBlue
         PCCA.PCMGR.c_InactiveDWColorUnchanged = l_BitMask
         PCCA.PCMGR.c_InactiveDWDarkBlue       = c_Default

      CASE c_InactiveDWMagenta
         PCCA.PCMGR.c_InactiveDWColorUnchanged = l_BitMask
         PCCA.PCMGR.c_InactiveDWMagenta        = c_Default

      CASE c_InactiveDWDarkMagenta
         PCCA.PCMGR.c_InactiveDWColorUnchanged = l_BitMask
         PCCA.PCMGR.c_InactiveDWDarkMagenta    = c_Default

      CASE c_InactiveDWCyan
         PCCA.PCMGR.c_InactiveDWColorUnchanged = l_BitMask
         PCCA.PCMGR.c_InactiveDWCyan           = c_Default

      CASE c_InactiveDWDarkCyan
         PCCA.PCMGR.c_InactiveDWColorUnchanged = l_BitMask
         PCCA.PCMGR.c_InactiveDWDarkCyan       = c_Default

      CASE c_InactiveDWYellow
         PCCA.PCMGR.c_InactiveDWColorUnchanged = l_BitMask
         PCCA.PCMGR.c_InactiveDWYellow         = c_Default

      CASE c_InactiveDWBrown
         PCCA.PCMGR.c_InactiveDWColorUnchanged = l_BitMask
         PCCA.PCMGR.c_InactiveDWBrown          = c_Default

      CASE ELSE
         // Should not get here.
   END CHOOSE
END IF

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   PCCA.PCMGR.c_NoInactiveText = c_InactiveTextMask
   PCCA.PCMGR.c_InactiveText   = c_Default
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   PCCA.PCMGR.c_NoInactiveLine = c_InactiveLineMask
   PCCA.PCMGR.c_InactiveLine   = c_Default
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   PCCA.PCMGR.c_NoInactiveCol = c_InactiveColMask
   PCCA.PCMGR.c_InactiveCol   = c_Default
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   PCCA.PCMGR.c_CursorRowFocusRect   = c_CursorRowFocusRectMask
   PCCA.PCMGR.c_NoCursorRowFocusRect = c_Default
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   PCCA.PCMGR.c_CursorRowPointer   = c_CursorRowPointerMask
   PCCA.PCMGR.c_NoCursorRowPointer = c_Default
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_BitMask = 0

FOR l_Idx = 1 TO 2
   l_OptionBit = 2 * l_OptionBit
   l_Divided   = l_OptionWord / 2
   IF (2 * l_Divided) <> l_OptionWord THEN
      l_BitMask = l_BitMask + l_OptionBit
   END IF
   l_OptionWord = l_Divided
NEXT

IF l_BitMask <> c_Default THEN
   CHOOSE CASE l_BitMask
      CASE c_FreeFormStyle
         PCCA.PCMGR.c_CalcDWStyle      = l_BitMask
         PCCA.PCMGR.c_FreeFormStyle    = c_Default

      CASE c_TabularFormStyle
         PCCA.PCMGR.c_CalcDWStyle      = l_BitMask
         PCCA.PCMGR.c_TabularFormStyle = c_Default

      CASE ELSE
         // Should not get here.
   END CHOOSE
END IF

//------------------------------------------------------------------

l_BitMask = 0

FOR l_Idx = 1 TO 2
   l_OptionBit = 2 * l_OptionBit
   l_Divided   = l_OptionWord / 2
   IF (2 * l_Divided) <> l_OptionWord THEN
      l_BitMask = l_BitMask + l_OptionBit
   END IF
   l_OptionWord = l_Divided
NEXT

IF l_BitMask <> c_Default THEN
   CHOOSE CASE l_BitMask
      CASE c_HighlightSelected
         PCCA.PCMGR.c_AutoCalcHighlightSelected = l_BitMask
         PCCA.PCMGR.c_HighlightSelected         = c_Default

      CASE c_TabularFormStyle
         PCCA.PCMGR.c_AutoCalcHighlightSelected = l_BitMask
         PCCA.PCMGR.c_NoHighlightSelected       = c_Default

      CASE ELSE
         // Should not get here.
   END CHOOSE
END IF

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   PCCA.PCMGR.c_NoResizeDW = c_ResizeDWMask
   PCCA.PCMGR.c_ResizeDW   = c_Default
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   PCCA.PCMGR.c_NoAutoConfigMenus = c_AutoConfigMenusMask
   PCCA.PCMGR.c_AutoConfigMenus   = c_Default
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   PCCA.PCMGR.c_ShowEmpty   = c_NoShowEmptyMask
   PCCA.PCMGR.c_NoShowEmpty = c_Default
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   PCCA.PCMGR.c_AutoFocus   = c_NoAutoFocusMask
   PCCA.PCMGR.c_NoAutoFocus = c_Default
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_BitMask = 0

FOR l_Idx = 1 TO 5
   l_OptionBit = 2 * l_OptionBit
   l_Divided   = l_OptionWord / 2
   IF (2 * l_Divided) <> l_OptionWord THEN
      l_BitMask = l_BitMask + l_OptionBit
   END IF
   l_OptionWord = l_Divided
NEXT

IF l_BitMask <> c_Default THEN
   CHOOSE CASE l_BitMask
      CASE c_CCErrorBlack
         PCCA.PCMGR.c_CCErrorRed         = l_BitMask
         PCCA.PCMGR.c_CCErrorBlack       = c_Default

      CASE c_CCErrorWhite
         PCCA.PCMGR.c_CCErrorRed         = l_BitMask
         PCCA.PCMGR.c_CCErrorWhite       = c_Default

      CASE c_CCErrorGray
         PCCA.PCMGR.c_CCErrorRed         = l_BitMask
         PCCA.PCMGR.c_CCErrorGray        = c_Default
         PCCA.PCMGR.c_CCErrorLightGray   = c_Default

      CASE c_CCErrorDarkGray
         PCCA.PCMGR.c_CCErrorRed         = l_BitMask
         PCCA.PCMGR.c_CCErrorDarkGray    = c_Default

      CASE c_CCErrorRed
         PCCA.PCMGR.c_CCErrorRed         = l_BitMask
         PCCA.PCMGR.c_CCErrorRed         = c_Default

      CASE c_CCErrorDarkRed
         PCCA.PCMGR.c_CCErrorRed         = l_BitMask
         PCCA.PCMGR.c_CCErrorDarkRed     = c_Default

      CASE c_CCErrorGreen
         PCCA.PCMGR.c_CCErrorRed         = l_BitMask
         PCCA.PCMGR.c_CCErrorGreen       = c_Default

      CASE c_CCErrorDarkGreen
         PCCA.PCMGR.c_CCErrorRed         = l_BitMask
         PCCA.PCMGR.c_CCErrorDarkGreen   = c_Default

      CASE c_CCErrorBlue
         PCCA.PCMGR.c_CCErrorRed         = l_BitMask
         PCCA.PCMGR.c_CCErrorBlue        = c_Default

      CASE c_CCErrorDarkBlue
         PCCA.PCMGR.c_CCErrorRed         = l_BitMask
         PCCA.PCMGR.c_CCErrorDarkBlue    = c_Default

      CASE c_CCErrorMagenta
         PCCA.PCMGR.c_CCErrorRed         = l_BitMask
         PCCA.PCMGR.c_CCErrorMagenta     = c_Default

      CASE c_CCErrorDarkMagenta
         PCCA.PCMGR.c_CCErrorRed         = l_BitMask
         PCCA.PCMGR.c_CCErrorDarkMagenta = c_Default

      CASE c_CCErrorCyan
         PCCA.PCMGR.c_CCErrorRed         = l_BitMask
         PCCA.PCMGR.c_CCErrorCyan        = c_Default

      CASE c_CCErrorDarkCyan
         PCCA.PCMGR.c_CCErrorRed         = l_BitMask
         PCCA.PCMGR.c_CCErrorDarkCyan    = c_Default

      CASE c_CCErrorYellow
         PCCA.PCMGR.c_CCErrorRed         = l_BitMask
         PCCA.PCMGR.c_CCErrorYellow      = c_Default

      CASE c_CCErrorBrown
         PCCA.PCMGR.c_CCErrorRed         = l_BitMask
         PCCA.PCMGR.c_CCErrorBrown       = c_Default

      CASE ELSE
         // Should not get here.
   END CHOOSE
END IF
end subroutine

public subroutine fu_setdwoptions (uo_dw_main this_obj, unsignedlong lcontrolword, unsignedlong lvisualword);//******************************************************************
//  PC Module     : n_DLL_Main
//
//  In order to speed up of options, the fu_BitANDMask() function
//  is not used.  Instead the option bits are directly processed
//  in this routine.  The "algorithm" used is simple.  As each
//  option is processed, the number of bits needed for that option
//  is stripped off the option word.  During this process, the
//  option word is "shifted right", so that the "used" bits drop
//  off and the next option's bit is shifted into the low-order
//  bit.  For most options, only one bit is required (i.e. the
//  option is a flag of some sort).  The flag can be set by just
//  seeing if the low order bit is set.  For options that are not
//  simple flags, more than one bit will have to be stripped off
//  the option word.  The process for doing this is just a FOR
//  loop that grabs the number of bits needed and globs them into
//  l_BitMask.  l_BitMask can then be examined to see how the
//  developer specified the option.
//
//  The variables are used as follows:
//
//     UNSIGNEDLONG l_OptionWord -
//        The option word that is being processed.  This is a
//        copy of the option word passed by the developer.
//
//     UNSIGNEDLONG l_OptionBit -
//         Indicates the original position of the bit that is
//         being processed.  As the option word is shifted right,
//         we would have no idea where the low-order bit
//         orignally came from.
//
//     UNSIGNEDLONG l_Divided -
//        Used to determine the value of the low-order bit and
//        do the  right.  Dividing an ULONG by 2 causes the
//        bits to  right.  The divided value can then
//        multiplied by 2 and compared to the orignal value
//        before the divide.  If these two values are equal, then
//        the low order bit was a 0 (i.e. because the orignal
//        value was even).  If these two values are not equal,
//        then the low order bit was a 1 (i.e. because the
//        original value was odd).
//
//     UNSIGNEDLONG l_BitMask -
//        When a option takes more than one bit, this variable is
//        used to build up the option so that it can be compared
//        to valid option values.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER       l_Idx
UNSIGNEDLONG  l_OptionWord, l_OptionBit, l_Divided, l_BitMask

l_OptionWord = lControlWord

//------------------------------------------------------------------

l_OptionBit = 1
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   This_Obj.i_ScrollParent = &
      (PCCA.PCMGR.c_ScrollParent = l_OptionBit)
ELSE
   This_Obj.i_ScrollParent = &
      (PCCA.PCMGR.c_ScrollParent = 0)
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   This_Obj.i_IsInstance = &
      (PCCA.PCMGR.c_IsInstance = l_OptionBit)
ELSE
   This_Obj.i_IsInstance = &
      (PCCA.PCMGR.c_IsInstance = 0)
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   This_Obj.i_AlwaysCheckRequired = &
      (PCCA.PCMGR.c_AlwaysCheckRequired = l_OptionBit)
ELSE
   This_Obj.i_AlwaysCheckRequired = &
      (PCCA.PCMGR.c_AlwaysCheckRequired = 0)
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   This_Obj.i_AllowNew = &
      (PCCA.PCMGR.c_NewOk = l_OptionBit)
ELSE
   This_Obj.i_AllowNew = &
      (PCCA.PCMGR.c_NewOk = 0)
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   This_Obj.i_AllowModify = &
      (PCCA.PCMGR.c_ModifyOk = l_OptionBit)
ELSE
   This_Obj.i_AllowModify = &
      (PCCA.PCMGR.c_ModifyOk = 0)
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   This_Obj.i_AllowDelete = &
      (PCCA.PCMGR.c_DeleteOk = l_OptionBit)
ELSE
   This_Obj.i_AllowDelete = &
      (PCCA.PCMGR.c_DeleteOk = 0)
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   This_Obj.i_AllowQuery = &
      (PCCA.PCMGR.c_QueryOk = l_OptionBit)
ELSE
   This_Obj.i_AllowQuery = &
      (PCCA.PCMGR.c_QueryOk = 0)
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_BitMask = 0

FOR l_Idx = 1 TO 2
   l_OptionBit = 2 * l_OptionBit
   l_Divided   = l_OptionWord / 2
   IF (2 * l_Divided) <> l_OptionWord THEN
      l_BitMask = l_BitMask + l_OptionBit
   END IF
   l_OptionWord = l_Divided
NEXT

CHOOSE CASE l_BitMask
   CASE PCCA.PCMGR.c_SelectOnRowFocusChange
      This_Obj.i_RFCType = This_Obj.c_RFC_RowFocusChanged

   CASE PCCA.PCMGR.c_SelectOnDoubleClick
      This_Obj.i_RFCType = This_Obj.c_RFC_DoubleClicked

   CASE PCCA.PCMGR.c_SelectOnClick
      This_Obj.i_RFCType = This_Obj.c_RFC_Clicked

   CASE ELSE
      This_Obj.i_RFCType = This_Obj.c_RFC_RowFocusChanged
END CHOOSE

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   This_Obj.i_AllowDrillDown = &
      (PCCA.PCMGR.c_DrillDown = l_OptionBit)
ELSE
   This_Obj.i_AllowDrillDown = &
      (PCCA.PCMGR.c_DrillDown = 0)
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_BitMask = 0

FOR l_Idx = 1 TO 2
   l_OptionBit = 2 * l_OptionBit
   l_Divided   = l_OptionWord / 2
   IF (2 * l_Divided) <> l_OptionWord THEN
      l_BitMask = l_BitMask + l_OptionBit
   END IF
   l_OptionWord = l_Divided
NEXT

CHOOSE CASE l_BitMask
   CASE PCCA.PCMGR.c_SameModeOnSelect
      This_Obj.i_ModeOnSelect = This_Obj.c_ModeUndefined

   CASE PCCA.PCMGR.c_ViewOnSelect
      This_Obj.i_ModeOnSelect = This_Obj.c_ModeView

   CASE PCCA.PCMGR.c_ModifyOnSelect
      This_Obj.i_ModeOnSelect = This_Obj.c_ModeModify

   CASE ELSE
      This_Obj.i_ModeOnSelect = This_Obj.c_ModeUndefined
END CHOOSE

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   This_Obj.i_MultiSelect = &
      (PCCA.PCMGR.c_MultiSelect = l_OptionBit)
ELSE
   This_Obj.i_MultiSelect = &
      (PCCA.PCMGR.c_MultiSelect = 0)
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_BitMask = 0

FOR l_Idx = 1 TO 2
   l_OptionBit = 2 * l_OptionBit
   l_Divided   = l_OptionWord / 2
   IF (2 * l_Divided) <> l_OptionWord THEN
      l_BitMask = l_BitMask + l_OptionBit
   END IF
   l_OptionWord = l_Divided
NEXT

CHOOSE CASE l_BitMask
   CASE PCCA.PCMGR.c_RefreshOnSelect
      This_Obj.i_RefreshWhen = This_Obj.c_RFC_RefreshOnSelect


   CASE PCCA.PCMGR.c_NoAutoRefresh
      This_Obj.i_RefreshWhen = This_Obj.c_RFC_NoAutoRefresh

   CASE PCCA.PCMGR.c_RefreshOnMultiSelect
      This_Obj.i_RefreshWhen = This_Obj.c_RFC_RefreshOnMultiSelect

   CASE ELSE
      This_Obj.i_RefreshWhen = This_Obj.c_RFC_RefreshOnSelect
END CHOOSE

//------------------------------------------------------------------

l_BitMask = 0

FOR l_Idx = 1 TO 2
   l_OptionBit = 2 * l_OptionBit
   l_Divided   = l_OptionWord / 2
   IF (2 * l_Divided) <> l_OptionWord THEN
      l_BitMask = l_BitMask + l_OptionBit
   END IF
   l_OptionWord = l_Divided
NEXT

CHOOSE CASE l_BitMask
   CASE PCCA.PCMGR.c_ParentModeOnOpen
      This_Obj.i_ModeOnOpen = This_Obj.c_ModeUndefined

   CASE PCCA.PCMGR.c_ViewOnOpen
      This_Obj.i_ModeOnOpen = This_Obj.c_ModeView

   CASE PCCA.PCMGR.c_ModifyOnOpen
      This_Obj.i_ModeOnOpen = This_Obj.c_ModeModify

   CASE PCCA.PCMGR.c_NewOnOpen
      This_Obj.i_ModeOnOpen = This_Obj.c_ModeNew

   CASE ELSE
      This_Obj.i_ModeOnOpen = This_Obj.c_ModeView
END CHOOSE

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   This_Obj.i_ViewAfterSave = &
      (PCCA.PCMGR.c_ViewAfterSave = l_OptionBit)
ELSE
   This_Obj.i_ViewAfterSave = &
      (PCCA.PCMGR.c_ViewAfterSave = 0)
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   This_Obj.i_EnableModifyOnOpen = &
      (PCCA.PCMGR.c_EnableModifyOnOpen = l_OptionBit)
ELSE
   This_Obj.i_EnableModifyOnOpen = &
      (PCCA.PCMGR.c_EnableModifyOnOpen = 0)
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   This_Obj.i_EnableNewOnOpen = &
      (PCCA.PCMGR.c_EnableNewOnOpen = l_OptionBit)
ELSE
   This_Obj.i_EnableNewOnOpen = &
      (PCCA.PCMGR.c_EnableNewOnOpen = 0)
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   This_Obj.i_RetrieveOnOpen = &
      (PCCA.PCMGR.c_RetrieveOnOpen = l_OptionBit)
ELSE
   This_Obj.i_RetrieveOnOpen = &
      (PCCA.PCMGR.c_RetrieveOnOpen = 0)
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_BitMask = 0

FOR l_Idx = 1 TO 2
   l_OptionBit = 2 * l_OptionBit
   l_Divided   = l_OptionWord / 2
   IF (2 * l_Divided) <> l_OptionWord THEN
      l_BitMask = l_BitMask + l_OptionBit
   END IF
   l_OptionWord = l_Divided
NEXT

CHOOSE CASE l_BitMask
   CASE PCCA.PCMGR.c_AutoRefresh
      This_Obj.i_RefreshOpenOption = This_Obj.c_AutoRefreshOnOpen

   CASE PCCA.PCMGR.c_SkipRefresh
      This_Obj.i_RefreshOpenOption = This_Obj.c_SkipRefreshOnOpen

   CASE PCCA.PCMGR.c_PostRefresh
      This_Obj.i_RefreshOpenOption = This_Obj.c_PostRefreshOnOpen

   CASE PCCA.PCMGR.c_TriggerRefresh
      This_Obj.i_RefreshOpenOption = This_Obj.c_TriggerRefreshOnOpen

   CASE ELSE
      This_Obj.i_RefreshOpenOption = This_Obj.c_AutoRefreshOnOpen
END CHOOSE

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   This_Obj.i_RetrieveAsNeeded = &
      (PCCA.PCMGR.c_RetrieveAsNeeded = l_OptionBit)
ELSE
   This_Obj.i_RetrieveAsNeeded = &
      (PCCA.PCMGR.c_RetrieveAsNeeded = 0)
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   This_Obj.i_ShareData = &
      (PCCA.PCMGR.c_ShareData = l_OptionBit)
ELSE
   This_Obj.i_ShareData = &
      (PCCA.PCMGR.c_ShareData = 0)
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   This_Obj.i_RefreshParent = &
      (PCCA.PCMGR.c_RefreshParent = l_OptionBit)
ELSE
   This_Obj.i_RefreshParent = &
      (PCCA.PCMGR.c_RefreshParent = 0)
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   This_Obj.i_RefreshChild = &
      (PCCA.PCMGR.c_RefreshChild = l_OptionBit)
ELSE
   This_Obj.i_RefreshChild = &
      (PCCA.PCMGR.c_RefreshChild = 0)
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   This_Obj.i_IgnoreNewRows = &
      (PCCA.PCMGR.c_IgnoreNewRows = l_OptionBit)
ELSE
   This_Obj.i_IgnoreNewRows = &
      (PCCA.PCMGR.c_IgnoreNewRows = 0)
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   This_Obj.i_NewModeOnEmpty = &
      (PCCA.PCMGR.c_NewModeOnEmpty = l_OptionBit)
ELSE
   This_Obj.i_NewModeOnEmpty = &
      (PCCA.PCMGR.c_NewModeOnEmpty = 0)
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   This_Obj.i_OnlyOneNewRow = &
      (PCCA.PCMGR.c_OnlyOneNewRow = l_OptionBit)
ELSE
   This_Obj.i_OnlyOneNewRow = &
      (PCCA.PCMGR.c_OnlyOneNewRow = 0)
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   This_Obj.i_EnableCC = &
      (PCCA.PCMGR.c_EnableCC = l_OptionBit)
ELSE
   This_Obj.i_EnableCC = &
      (PCCA.PCMGR.c_EnableCC = 0)
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   This_Obj.i_EnableCCInsert = &
      (PCCA.PCMGR.c_EnableCCInsert = l_OptionBit)
ELSE
   This_Obj.i_EnableCCInsert = &
      (PCCA.PCMGR.c_EnableCCInsert = 0)
END IF
l_OptionWord = l_Divided

//******************************************************************
//  VISUAL BIT PROCESSING.
//******************************************************************

l_OptionWord = lVisualWord

//------------------------------------------------------------------

l_BitMask   = 0

l_OptionBit = 1
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   l_BitMask = l_BitMask + l_OptionBit
END IF
l_OptionWord = l_Divided

FOR l_Idx = 1 TO 2
   l_OptionBit = 2 * l_OptionBit
   l_Divided   = l_OptionWord / 2
   IF (2 * l_Divided) <> l_OptionWord THEN
      l_BitMask = l_BitMask + l_OptionBit
   END IF
   l_OptionWord = l_Divided
NEXT

CHOOSE CASE l_BitMask
   // Most likely case ---------------------------------------------
   CASE PCCA.PCMGR.c_ViewModeBorderUnchanged
      This_Obj.i_ViewModeBorderIdx = PCCA.MDI.c_DW_BorderUndefined

   // Second likely case -------------------------------------------
   CASE PCCA.PCMGR.c_ViewModeLowered
      This_Obj.i_ViewModeBorderIdx = PCCA.MDI.c_DW_BorderLowered

   CASE PCCA.PCMGR.c_ViewModeNoBorder
      This_Obj.i_ViewModeBorderIdx = PCCA.MDI.c_DW_BorderNone

   CASE PCCA.PCMGR.c_ViewModeShadowBox
      This_Obj.i_ViewModeBorderIdx = PCCA.MDI.c_DW_BorderShadowBox

   CASE PCCA.PCMGR.c_ViewModeBox
      This_Obj.i_ViewModeBorderIdx = PCCA.MDI.c_DW_BorderBox

   CASE PCCA.PCMGR.c_ViewModeResize
      This_Obj.i_ViewModeBorderIdx = PCCA.MDI.c_DW_BorderResize

   CASE PCCA.PCMGR.c_ViewModeUnderline
      This_Obj.i_ViewModeBorderIdx = PCCA.MDI.c_DW_BorderUnderLine

   CASE PCCA.PCMGR.c_ViewModeRaised
      This_Obj.i_ViewModeBorderIdx = PCCA.MDI.c_DW_BorderRaised

   CASE ELSE
      This_Obj.i_ViewModeBorderIdx = PCCA.MDI.c_DW_BorderUndefined
END CHOOSE

//------------------------------------------------------------------

l_BitMask = 0

FOR l_Idx = 1 TO 5
   l_OptionBit = 2 * l_OptionBit
   l_Divided   = l_OptionWord / 2
   IF (2 * l_Divided) <> l_OptionWord THEN
      l_BitMask = l_BitMask + l_OptionBit
   END IF
   l_OptionWord = l_Divided
NEXT

CHOOSE CASE l_BitMask
   // Most likely case ---------------------------------------------
   CASE PCCA.PCMGR.c_ViewModeColorUnchanged
      This_Obj.i_ViewModeColorIdx = PCCA.MDI.c_ColorUndefined

   // Second likely case -------------------------------------------
   CASE PCCA.PCMGR.c_ViewModeGray
      This_Obj.i_ViewModeColorIdx = PCCA.MDI.c_Gray

   CASE PCCA.PCMGR.c_ViewModeBlack
      This_Obj.i_ViewModeColorIdx = PCCA.MDI.c_Black

   CASE PCCA.PCMGR.c_ViewModeWhite
      This_Obj.i_ViewModeColorIdx = PCCA.MDI.c_White

   CASE PCCA.PCMGR.c_ViewModeDarkGray
      This_Obj.i_ViewModeColorIdx = PCCA.MDI.c_DarkGray

   CASE PCCA.PCMGR.c_ViewModeRed
      This_Obj.i_ViewModeColorIdx = PCCA.MDI.c_Red

   CASE PCCA.PCMGR.c_ViewModeDarkRed
      This_Obj.i_ViewModeColorIdx = PCCA.MDI.c_DarkRed

   CASE PCCA.PCMGR.c_ViewModeGreen
      This_Obj.i_ViewModeColorIdx = PCCA.MDI.c_Green

   CASE PCCA.PCMGR.c_ViewModeDarkGreen
      This_Obj.i_ViewModeColorIdx = PCCA.MDI.c_DarkGreen

   CASE PCCA.PCMGR.c_ViewModeBlue
      This_Obj.i_ViewModeColorIdx = PCCA.MDI.c_Blue

   CASE PCCA.PCMGR.c_ViewModeDarkBlue
      This_Obj.i_ViewModeColorIdx = PCCA.MDI.c_DarkBlue

   CASE PCCA.PCMGR.c_ViewModeMagenta
      This_Obj.i_ViewModeColorIdx = PCCA.MDI.c_Magenta

   CASE PCCA.PCMGR.c_ViewModeDarkMagenta
      This_Obj.i_ViewModeColorIdx = PCCA.MDI.c_DarkMagenta

   CASE PCCA.PCMGR.c_ViewModeCyan
      This_Obj.i_ViewModeColorIdx = PCCA.MDI.c_Cyan

   CASE PCCA.PCMGR.c_ViewModeDarkCyan
      This_Obj.i_ViewModeColorIdx = PCCA.MDI.c_DarkCyan

   CASE PCCA.PCMGR.c_ViewModeYellow
      This_Obj.i_ViewModeColorIdx = PCCA.MDI.c_Yellow

   CASE PCCA.PCMGR.c_ViewModeBrown
      This_Obj.i_ViewModeColorIdx = PCCA.MDI.c_Brown

   CASE ELSE
      This_Obj.i_ViewModeColorIdx = PCCA.MDI.c_ColorUndefined
END CHOOSE

//------------------------------------------------------------------

l_BitMask = 0

FOR l_Idx = 1 TO 5
   l_OptionBit = 2 * l_OptionBit
   l_Divided   = l_OptionWord / 2
   IF (2 * l_Divided) <> l_OptionWord THEN
      l_BitMask = l_BitMask + l_OptionBit
   END IF
   l_OptionWord = l_Divided
NEXT

CHOOSE CASE l_BitMask
   // Most likely case ---------------------------------------------
   CASE PCCA.PCMGR.c_InactiveDWColorUnchanged
      This_Obj.i_InactiveDWColorIdx = PCCA.MDI.c_ColorUndefined

   // Second likely case -------------------------------------------
   CASE PCCA.PCMGR.c_InactiveDWDarkGray
      This_Obj.i_InactiveDWColorIdx = PCCA.MDI.c_DarkGray

   CASE PCCA.PCMGR.c_InactiveDWBlack
      This_Obj.i_InactiveDWColorIdx = PCCA.MDI.c_Black

   CASE PCCA.PCMGR.c_InactiveDWWhite
      This_Obj.i_InactiveDWColorIdx = PCCA.MDI.c_White

   CASE PCCA.PCMGR.c_InactiveDWGray
      This_Obj.i_InactiveDWColorIdx = PCCA.MDI.c_Gray

   CASE PCCA.PCMGR.c_InactiveDWRed
      This_Obj.i_InactiveDWColorIdx = PCCA.MDI.c_Red

   CASE PCCA.PCMGR.c_InactiveDWDarkRed
      This_Obj.i_InactiveDWColorIdx = PCCA.MDI.c_DarkRed

   CASE PCCA.PCMGR.c_InactiveDWGreen
      This_Obj.i_InactiveDWColorIdx = PCCA.MDI.c_Green

   CASE PCCA.PCMGR.c_InactiveDWDarkGreen
      This_Obj.i_InactiveDWColorIdx = PCCA.MDI.c_DarkGreen

   CASE PCCA.PCMGR.c_InactiveDWBlue
      This_Obj.i_InactiveDWColorIdx = PCCA.MDI.c_Blue

   CASE PCCA.PCMGR.c_InactiveDWDarkBlue
      This_Obj.i_InactiveDWColorIdx = PCCA.MDI.c_DarkBlue

   CASE PCCA.PCMGR.c_InactiveDWMagenta
      This_Obj.i_InactiveDWColorIdx = PCCA.MDI.c_Magenta

   CASE PCCA.PCMGR.c_InactiveDWDarkMagenta
      This_Obj.i_InactiveDWColorIdx = PCCA.MDI.c_DarkMagenta

   CASE PCCA.PCMGR.c_InactiveDWCyan
      This_Obj.i_InactiveDWColorIdx = PCCA.MDI.c_Cyan

   CASE PCCA.PCMGR.c_InactiveDWDarkCyan
      This_Obj.i_InactiveDWColorIdx = PCCA.MDI.c_DarkCyan

   CASE PCCA.PCMGR.c_InactiveDWYellow
      This_Obj.i_InactiveDWColorIdx = PCCA.MDI.c_Yellow

   CASE PCCA.PCMGR.c_InactiveDWBrown
      This_Obj.i_InactiveDWColorIdx = PCCA.MDI.c_Brown

   CASE ELSE
      This_Obj.i_InactiveDWColorIdx = PCCA.MDI.c_ColorUndefined
END CHOOSE

//------------------------------------------------------------------

IF This_Obj.i_InactiveDWColorIdx = PCCA.MDI.c_ColorUndefined THEN
   This_Obj.i_InactiveText = FALSE
   This_Obj.i_InactiveLine = FALSE
   This_Obj.i_InactiveCol  = FALSE

   FOR l_Idx = 1 TO 3
      l_OptionBit  = 2 * l_OptionBit
      l_OptionWord = l_OptionWord / 2
   NEXT
ELSE

   l_OptionBit = 2 * l_OptionBit
   l_Divided   = l_OptionWord / 2
   IF  (2 * l_Divided) <> l_OptionWord THEN
      This_Obj.i_InactiveText = (PCCA.PCMGR.c_InactiveText = l_OptionBit)
   ELSE
      This_Obj.i_InactiveText = (PCCA.PCMGR.c_InactiveText = 0)
   END IF
   l_OptionWord = l_Divided

   //---------------------------------------------------------------

   l_OptionBit = 2 * l_OptionBit
   l_Divided   = l_OptionWord / 2
   IF  (2 * l_Divided) <> l_OptionWord THEN
      This_Obj.i_InactiveLine = (PCCA.PCMGR.c_InactiveLine = l_OptionBit)
   ELSE
      This_Obj.i_InactiveLine = (PCCA.PCMGR.c_InactiveLine = 0)
   END IF
   l_OptionWord = l_Divided

   //---------------------------------------------------------------

   l_OptionBit = 2 * l_OptionBit
   l_Divided   = l_OptionWord / 2
   IF  (2 * l_Divided) <> l_OptionWord THEN
      This_Obj.i_InactiveCol = (PCCA.PCMGR.c_InactiveCol = l_OptionBit)
   ELSE
      This_Obj.i_InactiveCol = (PCCA.PCMGR.c_InactiveCol = 0)
   END IF
   l_OptionWord = l_Divided

   //---------------------------------------------------------------

   IF NOT This_Obj.i_InactiveText AND &
      NOT This_Obj.i_InactiveLine AND &
      NOT This_Obj.i_InactiveCol THEN
      This_Obj.i_InactiveText = TRUE
      This_Obj.i_InactiveLine = TRUE
      This_Obj.i_InactiveCol  = TRUE
   END IF
END IF

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   This_Obj.i_CursorRowFocusRect = (PCCA.PCMGR.c_CursorRowFocusRect = l_OptionBit)
ELSE
   This_Obj.i_CursorRowFocusRect = (PCCA.PCMGR.c_CursorRowFocusRect = 0)
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   This_Obj.i_CursorRowPointer = (PCCA.PCMGR.c_CursorRowPointer = l_OptionBit)
ELSE
   This_Obj.i_CursorRowPointer = (PCCA.PCMGR.c_CursorRowPointer = 0)
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_BitMask = 0

FOR l_Idx = 1 TO 2
   l_OptionBit = 2 * l_OptionBit
   l_Divided   = l_OptionWord / 2
   IF (2 * l_Divided) <> l_OptionWord THEN
      l_BitMask = l_BitMask + l_OptionBit
   END IF
   l_OptionWord = l_Divided
NEXT

CHOOSE CASE l_BitMask
   CASE PCCA.PCMGR.c_CalcDWStyle
      This_Obj.i_FreeFormStyle    = FALSE
      This_Obj.i_TabularFormStyle = FALSE

   CASE PCCA.PCMGR.c_FreeFormStyle
      This_Obj.i_FreeFormStyle    = TRUE
      This_Obj.i_TabularFormStyle = FALSE

   CASE PCCA.PCMGR.c_TabularFormStyle
      This_Obj.i_FreeFormStyle    = FALSE
      This_Obj.i_TabularFormStyle = TRUE

   CASE ELSE
      This_Obj.i_FreeFormStyle    = FALSE
      This_Obj.i_TabularFormStyle = FALSE
END CHOOSE

//------------------------------------------------------------------

l_BitMask = 0

FOR l_Idx = 1 TO 2
   l_OptionBit = 2 * l_OptionBit
   l_Divided   = l_OptionWord / 2
   IF (2 * l_Divided) <> l_OptionWord THEN
      l_BitMask = l_BitMask + l_OptionBit
   END IF
   l_OptionWord = l_Divided
NEXT

CHOOSE CASE l_BitMask
   CASE PCCA.PCMGR.c_AutoCalcHighlightSelected
      This_Obj.i_HighlightSelected     = FALSE
      This_Obj.i_CalcHighlightSelected = TRUE

   CASE PCCA.PCMGR.c_HighlightSelected
      This_Obj.i_HighlightSelected     = TRUE
      This_Obj.i_CalcHighlightSelected = FALSE

   CASE PCCA.PCMGR.c_NoHighlightSelected
      This_Obj.i_HighlightSelected     = FALSE
      This_Obj.i_CalcHighlightSelected = FALSE

   CASE ELSE
      This_Obj.i_HighlightSelected     = FALSE
      This_Obj.i_CalcHighlightSelected = TRUE
END CHOOSE

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   This_Obj.i_ResizeDW = (PCCA.PCMGR.c_ResizeDW = l_OptionBit)
ELSE
   This_Obj.i_ResizeDW = (PCCA.PCMGR.c_ResizeDW = 0)
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   This_Obj.i_AutoConfigMenus = (PCCA.PCMGR.c_AutoConfigMenus = l_OptionBit)
ELSE
   This_Obj.i_AutoConfigMenus = (PCCA.PCMGR.c_AutoConfigMenus = 0)
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   This_Obj.i_ShowEmpty = (PCCA.PCMGR.c_ShowEmpty = l_OptionBit)
ELSE
   This_Obj.i_ShowEmpty = (PCCA.PCMGR.c_ShowEmpty = 0)
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   This_Obj.i_AutoFocus = (PCCA.PCMGR.c_AutoFocus = l_OptionBit)
ELSE
   This_Obj.i_AutoFocus = (PCCA.PCMGR.c_AutoFocus = 0)
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_BitMask = 0

FOR l_Idx = 1 TO 4
   l_OptionBit = 2 * l_OptionBit
   l_Divided   = l_OptionWord / 2
   IF (2 * l_Divided) <> l_OptionWord THEN
      l_BitMask = l_BitMask + l_OptionBit
   END IF
   l_OptionWord = l_Divided
NEXT

CHOOSE CASE l_BitMask
   // Most likely case ---------------------------------------------
   CASE PCCA.PCMGR.c_CCErrorRed
      This_Obj.i_CCErrorColorIdx = PCCA.MDI.c_Red

   CASE PCCA.PCMGR.c_CCErrorBlack
      This_Obj.i_CCErrorColorIdx = PCCA.MDI.c_Black

   CASE PCCA.PCMGR.c_CCErrorWhite
      This_Obj.i_CCErrorColorIdx = PCCA.MDI.c_White

   CASE PCCA.PCMGR.c_CCErrorGray
      This_Obj.i_CCErrorColorIdx = PCCA.MDI.c_Gray

   CASE PCCA.PCMGR.c_CCErrorDarkGray
      This_Obj.i_CCErrorColorIdx = PCCA.MDI.c_DarkGray

   CASE PCCA.PCMGR.c_CCErrorDarkRed
      This_Obj.i_CCErrorColorIdx = PCCA.MDI.c_DarkRed

   CASE PCCA.PCMGR.c_CCErrorGreen
      This_Obj.i_CCErrorColorIdx = PCCA.MDI.c_Green

   CASE PCCA.PCMGR.c_CCErrorDarkGreen
      This_Obj.i_CCErrorColorIdx = PCCA.MDI.c_DarkGreen

   CASE PCCA.PCMGR.c_CCErrorBlue
      This_Obj.i_CCErrorColorIdx = PCCA.MDI.c_Blue

   CASE PCCA.PCMGR.c_CCErrorDarkBlue
      This_Obj.i_CCErrorColorIdx = PCCA.MDI.c_DarkBlue

   CASE PCCA.PCMGR.c_CCErrorMagenta
      This_Obj.i_CCErrorColorIdx = PCCA.MDI.c_Magenta

   CASE PCCA.PCMGR.c_CCErrorDarkMagenta
      This_Obj.i_CCErrorColorIdx = PCCA.MDI.c_DarkMagenta

   CASE PCCA.PCMGR.c_CCErrorCyan
      This_Obj.i_CCErrorColorIdx = PCCA.MDI.c_Cyan

   CASE PCCA.PCMGR.c_CCErrorDarkCyan
      This_Obj.i_CCErrorColorIdx = PCCA.MDI.c_DarkCyan

   CASE PCCA.PCMGR.c_CCErrorYellow
      This_Obj.i_CCErrorColorIdx = PCCA.MDI.c_Yellow

   CASE PCCA.PCMGR.c_CCErrorBrown
      This_Obj.i_CCErrorColorIdx = PCCA.MDI.c_Brown

   CASE ELSE
      This_Obj.i_CCErrorColorIdx = PCCA.MDI.c_Red
END CHOOSE
end subroutine

public subroutine fu_setdwpmenu (uo_dw_main this_obj);//******************************************************************
//  PC Module     : n_DLL_Main
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

//------------------------------------------------------------------
//  Popup Menu Bits
//------------------------------------------------------------------

This_Obj.i_PopupMenuInit          = This_Obj.i_EditMenuInit
This_Obj.i_PopupMenuIsValid       = This_Obj.i_EditMenuIsValid
This_Obj.i_PopupMenuIsEdit        = This_Obj.i_EditMenuIsPopup
This_Obj.i_PopupMenu              = This_Obj.i_EditMenu

This_Obj.i_PopupMenuNewNum        = This_Obj.i_EditMenuNewNum
This_Obj.i_PopupMenuViewNum       = This_Obj.i_EditMenuViewNum
This_Obj.i_PopupMenuModifyNum     = This_Obj.i_EditMenuModifyNum
This_Obj.i_PopupMenuInsertNum     = This_Obj.i_EditMenuInsertNum
This_Obj.i_PopupMenuDeleteNum     = This_Obj.i_EditMenuDeleteNum
This_Obj.i_PopupMenuModeSepNum    = This_Obj.i_EditMenuModeSepNum
This_Obj.i_PopupMenuFirstNum      = This_Obj.i_EditMenuFirstNum
This_Obj.i_PopupMenuPrevNum       = This_Obj.i_EditMenuPrevNum
This_Obj.i_PopupMenuNextNum       = This_Obj.i_EditMenuNextNum
This_Obj.i_PopupMenuLastNum       = This_Obj.i_EditMenuLastNum
This_Obj.i_PopupMenuSearchSepNum  = This_Obj.i_EditMenuSearchSepNum
This_Obj.i_PopupMenuQueryNum      = This_Obj.i_EditMenuQueryNum
This_Obj.i_PopupMenuSearchNum     = This_Obj.i_EditMenuSearchNum
This_Obj.i_PopupMenuFilterNum     = This_Obj.i_EditMenuFilterNum

This_Obj.i_PopupMenuSaveSepNum    = This_Obj.i_EditMenuSaveSepNum
This_Obj.i_PopupMenuSaveNum       = This_Obj.i_EditMenuSaveNum
This_Obj.i_PopupMenuSaveRowsAsNum = This_Obj.i_EditMenuSaveRowsAsNum
This_Obj.i_PopupMenuPrintSepNum   = This_Obj.i_EditMenuPrintSepNum
This_Obj.i_PopupMenuPrintNum      = This_Obj.i_EditMenuPrintNum

This_Obj.i_PopupMenuFirst         = This_Obj.i_EditMenuFirst
This_Obj.i_PopupMenuPrev          = This_Obj.i_EditMenuPrev
This_Obj.i_PopupMenuNext          = This_Obj.i_EditMenuNext
This_Obj.i_PopupMenuLast          = This_Obj.i_EditMenuLast
This_Obj.i_PopupMenuModeSep       = This_Obj.i_EditMenuModeSep
This_Obj.i_PopupMenuNew           = This_Obj.i_EditMenuNew
This_Obj.i_PopupMenuView          = This_Obj.i_EditMenuView
This_Obj.i_PopupMenuModify        = This_Obj.i_EditMenuModify
This_Obj.i_PopupMenuInsert        = This_Obj.i_EditMenuInsert
This_Obj.i_PopupMenuDelete        = This_Obj.i_EditMenuDelete
This_Obj.i_PopupMenuSearchSep     = This_Obj.i_EditMenuSearchSep
This_Obj.i_PopupMenuQuery         = This_Obj.i_EditMenuQuery
This_Obj.i_PopupMenuSearch        = This_Obj.i_EditMenuSearch
This_Obj.i_PopupMenuFilter        = This_Obj.i_EditMenuFilter

This_Obj.i_PopupMenuSaveSep       = This_Obj.i_EditMenuSaveSep
This_Obj.i_PopupMenuSave          = This_Obj.i_EditMenuSave
This_Obj.i_PopupMenuSaveRowsAs    = This_Obj.i_EditMenuSaveRowsAs
This_Obj.i_PopupMenuPrintSep      = This_Obj.i_EditMenuPrintSep
This_Obj.i_PopupMenuPrint         = This_Obj.i_EditMenuPrint

end subroutine

public subroutine fu_setmdidefaults (w_pcmanager_main this_obj, unsignedlong lcontrolword);//******************************************************************
//  PC Module     : n_DLL_Main
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER       l_Idx
UNSIGNEDLONG  l_OptionWord, l_OptionBit, l_Divided, l_BitMask

l_OptionWord = lControlWord

//------------------------------------------------------------------

l_OptionBit = 1
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   // Default changed on 941218.
   PCCA.PCMGR.c_MDI_NoAllowRedraw = c_MDI_AllowRedrawMask
   PCCA.PCMGR.c_MDI_AllowRedraw   = c_Default
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   PCCA.PCMGR.c_MDI_NoShowResources = c_MDI_ShowResourcesMask
   PCCA.PCMGR.c_MDI_ShowResources   = c_Default
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   PCCA.PCMGR.c_MDI_NoShowClock = c_MDI_ShowClockMask
   PCCA.PCMGR.c_MDI_ShowClock   = c_Default
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   PCCA.PCMGR.c_MDI_NoAutoConfigMenus = c_MDI_AutoConfigMenusMask
   PCCA.PCMGR.c_MDI_AutoConfigMenus   = c_Default
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   PCCA.PCMGR.c_MDI_NoSavePosition = c_MDI_SavePositionMask
   PCCA.PCMGR.c_MDI_SavePosition   = c_Default
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_BitMask = 0

FOR l_Idx = 1 TO 3
   l_OptionBit = 2 * l_OptionBit
   l_Divided   = l_OptionWord / 2
   IF (2 * l_Divided) <> l_OptionWord THEN
      l_BitMask = l_BitMask + l_OptionBit
   END IF
   l_OptionWord = l_Divided
NEXT

IF l_BitMask <> c_Default THEN
   CHOOSE CASE l_BitMask
      CASE c_MDI_ToolBarTop
         PCCA.PCMGR.c_MDI_ToolBarNone   = l_BitMask
         PCCA.PCMGR.c_MDI_ToolBarTop    = c_Default

      CASE c_MDI_ToolBarBottom
         PCCA.PCMGR.c_MDI_ToolBarNone   = l_BitMask
         PCCA.PCMGR.c_MDI_ToolBarBottom = c_Default

      CASE c_MDI_ToolBarLeft
         PCCA.PCMGR.c_MDI_ToolBarNone   = l_BitMask
         PCCA.PCMGR.c_MDI_ToolBarLeft   = c_Default

      CASE c_MDI_ToolBarRight
         PCCA.PCMGR.c_MDI_ToolBarNone   = l_BitMask
         PCCA.PCMGR.c_MDI_ToolBarRight  = c_Default

      CASE c_MDI_ToolBarFloat
         PCCA.PCMGR.c_MDI_ToolBarNone   = l_BitMask
         PCCA.PCMGR.c_MDI_ToolBarFloat  = c_Default

      CASE ELSE
         // Should not get here.
   END CHOOSE
END IF

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   PCCA.PCMGR.c_MDI_ToolBarHideText = c_MDI_ToolBarTextMask
   PCCA.PCMGR.c_MDI_ToolBarShowText = c_Default
END IF
l_OptionWord = l_Divided
end subroutine

public subroutine fu_setmdioptions (w_mdi_frame this_obj, unsignedlong lcontrolword);//******************************************************************
//  PC Module     : n_DLL_Main
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER       l_Idx
UNSIGNEDLONG  l_OptionWord, l_OptionBit, l_Divided, l_BitMask

l_OptionWord = lControlWord

//------------------------------------------------------------------

l_OptionBit = 1
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   This_Obj.i_MDI_AllowRedraw = &
      (PCCA.PCMGR.c_MDI_AllowRedraw = l_OptionBit)
ELSE
   This_Obj.i_MDI_AllowRedraw = &
      (PCCA.PCMGR.c_MDI_AllowRedraw = 0)
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   This_Obj.i_MDI_ShowResources = &
      (PCCA.PCMGR.c_MDI_ShowResources = l_OptionBit)
ELSE
   This_Obj.i_MDI_ShowResources = &
      (PCCA.PCMGR.c_MDI_ShowResources = 0)
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   This_Obj.i_MDI_ShowClock = &
      (PCCA.PCMGR.c_MDI_ShowClock = l_OptionBit)
ELSE
   This_Obj.i_MDI_ShowClock = &
      (PCCA.PCMGR.c_MDI_ShowClock = 0)
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   This_Obj.i_MDI_AutoConfigMenus = &
      (PCCA.PCMGR.c_MDI_AutoConfigMenus = l_OptionBit)
ELSE
   This_Obj.i_MDI_AutoConfigMenus = &
      (PCCA.PCMGR.c_MDI_AutoConfigMenus = 0)
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   This_Obj.i_MDI_SavePosition = &
      (PCCA.PCMGR.c_MDI_SavePosition = l_OptionBit)
ELSE
   This_Obj.i_MDI_SavePosition = &
      (PCCA.PCMGR.c_MDI_SavePosition = 0)
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_BitMask = 0

FOR l_Idx = 1 TO 3
   l_OptionBit = 2 * l_OptionBit
   l_Divided   = l_OptionWord / 2
   IF (2 * l_Divided) <> l_OptionWord THEN
      l_BitMask = l_BitMask + l_OptionBit
   END IF
   l_OptionWord = l_Divided
NEXT

This_Obj.i_MDI_ToolBarPosition = l_BitMask

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   This_Obj.i_MDI_ToolBarText = &
      (PCCA.PCMGR.c_MDI_ToolBarShowText = l_OptionBit)
ELSE
   This_Obj.i_MDI_ToolBarText = &
      (PCCA.PCMGR.c_MDI_ToolBarShowText = 0)
END IF
l_OptionWord = l_Divided
end subroutine

public subroutine fu_setmdipmenu (w_mdi_frame this_obj);//******************************************************************
//  PC Module     : n_DLL_Main
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

//------------------------------------------------------------------
//  MDI Popup Menu Bits
//------------------------------------------------------------------

This_Obj.i_PopupMenuInit          = This_Obj.i_EditMenuInit
This_Obj.i_PopupMenuIsValid       = This_Obj.i_EditMenuIsValid
This_Obj.i_PopupMenuIsEdit        = This_Obj.i_EditMenuIsPopup
This_Obj.i_PopupMenu              = This_Obj.i_EditMenu

This_Obj.i_PopupMenuNewNum        = This_Obj.i_EditMenuNewNum
This_Obj.i_PopupMenuViewNum       = This_Obj.i_EditMenuViewNum
This_Obj.i_PopupMenuModifyNum     = This_Obj.i_EditMenuModifyNum
This_Obj.i_PopupMenuInsertNum     = This_Obj.i_EditMenuInsertNum
This_Obj.i_PopupMenuDeleteNum     = This_Obj.i_EditMenuDeleteNum
This_Obj.i_PopupMenuModeSepNum    = This_Obj.i_EditMenuModeSepNum
This_Obj.i_PopupMenuFirstNum      = This_Obj.i_EditMenuFirstNum
This_Obj.i_PopupMenuPrevNum       = This_Obj.i_EditMenuPrevNum
This_Obj.i_PopupMenuNextNum       = This_Obj.i_EditMenuNextNum
This_Obj.i_PopupMenuLastNum       = This_Obj.i_EditMenuLastNum
This_Obj.i_PopupMenuSearchSepNum  = This_Obj.i_EditMenuSearchSepNum
This_Obj.i_PopupMenuQueryNum      = This_Obj.i_EditMenuQueryNum
This_Obj.i_PopupMenuSearchNum     = This_Obj.i_EditMenuSearchNum
This_Obj.i_PopupMenuFilterNum     = This_Obj.i_EditMenuFilterNum

This_Obj.i_PopupMenuSaveSepNum    = This_Obj.i_EditMenuSaveSepNum
This_Obj.i_PopupMenuSaveNum       = This_Obj.i_EditMenuSaveNum
This_Obj.i_PopupMenuSaveRowsAsNum = This_Obj.i_EditMenuSaveRowsAsNum
This_Obj.i_PopupMenuPrintSepNum   = This_Obj.i_EditMenuPrintSepNum
This_Obj.i_PopupMenuPrintNum      = This_Obj.i_EditMenuPrintNum

This_Obj.i_PopupMenuFirst         = This_Obj.i_EditMenuFirst
This_Obj.i_PopupMenuPrev          = This_Obj.i_EditMenuPrev
This_Obj.i_PopupMenuNext          = This_Obj.i_EditMenuNext
This_Obj.i_PopupMenuLast          = This_Obj.i_EditMenuLast
This_Obj.i_PopupMenuModeSep       = This_Obj.i_EditMenuModeSep
This_Obj.i_PopupMenuNew           = This_Obj.i_EditMenuNew
This_Obj.i_PopupMenuView          = This_Obj.i_EditMenuView
This_Obj.i_PopupMenuModify        = This_Obj.i_EditMenuModify
This_Obj.i_PopupMenuInsert        = This_Obj.i_EditMenuInsert
This_Obj.i_PopupMenuDelete        = This_Obj.i_EditMenuDelete
This_Obj.i_PopupMenuSearchSep     = This_Obj.i_EditMenuSearchSep
This_Obj.i_PopupMenuQuery         = This_Obj.i_EditMenuQuery
This_Obj.i_PopupMenuSearch        = This_Obj.i_EditMenuSearch
This_Obj.i_PopupMenuFilter        = This_Obj.i_EditMenuFilter

This_Obj.i_PopupMenuSaveSep       = This_Obj.i_EditMenuSaveSep
This_Obj.i_PopupMenuSave          = This_Obj.i_EditMenuSave
This_Obj.i_PopupMenuSaveRowsAs    = This_Obj.i_EditMenuSaveRowsAs
This_Obj.i_PopupMenuPrintSep      = This_Obj.i_EditMenuPrintSep
This_Obj.i_PopupMenuPrint         = This_Obj.i_EditMenuPrint

end subroutine

public subroutine fu_setwdefaults (w_pcmanager_main this_obj, unsignedlong lcontrolword);//******************************************************************
//  PC Module     : n_DLL_Main
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER       l_Idx
UNSIGNEDLONG  l_OptionWord, l_OptionBit, l_Divided, l_BitMask

l_OptionWord = lControlWord

//------------------------------------------------------------------

l_OptionBit = 1
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   PCCA.PCMGR.c_LoadCodeTable   = c_LoadCodeTableMask
   PCCA.PCMGR.c_NoLoadCodeTable = c_Default
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   PCCA.PCMGR.c_EnablePopup   = c_EnablePopupMask
   PCCA.PCMGR.c_NoEnablePopup = c_Default
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_BitMask = 0

FOR l_Idx = 1 TO 2
   l_OptionBit = 2 * l_OptionBit
   l_Divided   = l_OptionWord / 2
   IF (2 * l_Divided) <> l_OptionWord THEN
      l_BitMask = l_BitMask + l_OptionBit
   END IF
   l_OptionWord = l_Divided
NEXT

IF l_BitMask <> c_Default THEN
   CHOOSE CASE l_BitMask
      CASE c_ResizeWin
         PCCA.PCMGR.c_NoResizeWin = l_BitMask
         PCCA.PCMGR.c_ResizeWin   = c_Default

      CASE c_ZoomWin
         PCCA.PCMGR.c_NoResizeWin = l_BitMask
         PCCA.PCMGR.c_ZoomWin     = c_Default

      CASE ELSE
         PCCA.PCMGR.c_NoResizeWin = l_BitMask
         PCCA.PCMGR.c_ZoomWin     = c_Default
   END CHOOSE
END IF

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   PCCA.PCMGR.c_NoAutoMinimize = c_AutoMinimizeMask
   PCCA.PCMGR.c_AutoMinimize   = c_Default
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_BitMask = 0

FOR l_Idx = 1 TO 2
   l_OptionBit = 2 * l_OptionBit
   l_Divided   = l_OptionWord / 2
   IF (2 * l_Divided) <> l_OptionWord THEN
      l_BitMask = l_BitMask + l_OptionBit
   END IF
   l_OptionWord = l_Divided
NEXT

IF l_BitMask <> c_Default THEN
   CHOOSE CASE l_BitMask
      CASE c_AutoSize
         PCCA.PCMGR.c_NoAutoSize = l_BitMask
         PCCA.PCMGR.c_AutoSize   = c_Default

      CASE c_AutoSize30
         PCCA.PCMGR.c_NoAutoSize = l_BitMask
         PCCA.PCMGR.c_AutoSize30 = c_Default

      CASE ELSE
         // Should not get here.
   END CHOOSE
END IF

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   PCCA.PCMGR.c_NoAutoPosition = c_AutoPositionMask
   PCCA.PCMGR.c_AutoPosition   = c_Default
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   PCCA.PCMGR.c_NoSavePosition = c_SavePositionMask
   PCCA.PCMGR.c_SavePosition   = c_Default
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_BitMask = 0

FOR l_Idx = 1 TO 2
   l_OptionBit = 2 * l_OptionBit
   l_Divided   = l_OptionWord / 2
   IF (2 * l_Divided) <> l_OptionWord THEN
      l_BitMask = l_BitMask + l_OptionBit
   END IF
   l_OptionWord = l_Divided
NEXT

IF l_BitMask <> c_Default THEN
   CHOOSE CASE l_BitMask
      CASE c_ClosePromptUserOnce
         PCCA.PCMGR.c_ClosePromptUser     = l_BitMask
         PCCA.PCMGR.c_ClosePromptUserOnce = c_Default

      CASE c_CloseSave
         PCCA.PCMGR.c_ClosePromptUser     = l_BitMask
         PCCA.PCMGR.c_CloseSave           = c_Default

      CASE c_CloseNoSave
         PCCA.PCMGR.c_ClosePromptUser     = l_BitMask
         PCCA.PCMGR.c_CloseNoSave         = c_Default

      CASE ELSE
         // Should not get here.
   END CHOOSE
END IF

//------------------------------------------------------------------

FOR l_Idx = 1 TO 13
   l_OptionBit  = 2 * l_OptionBit
   l_Divided    = l_OptionWord / 2
   l_OptionWord = l_Divided
NEXT

//------------------------------------------------------------------

l_BitMask = 0

FOR l_Idx = 1 TO 3
   l_OptionBit = 2 * l_OptionBit
   l_Divided   = l_OptionWord / 2
   IF (2 * l_Divided) <> l_OptionWord THEN
      l_BitMask = l_BitMask + l_OptionBit
   END IF
   l_OptionWord = l_Divided
NEXT

IF l_BitMask <> c_Default THEN
   CHOOSE CASE l_BitMask
      CASE c_ToolBarTop
         PCCA.PCMGR.c_ToolBarNone   = l_BitMask
         PCCA.PCMGR.c_ToolBarTop    = c_Default

      CASE c_ToolBarBottom
         PCCA.PCMGR.c_ToolBarNone   = l_BitMask
         PCCA.PCMGR.c_ToolBarBottom = c_Default

      CASE c_ToolBarLeft
         PCCA.PCMGR.c_ToolBarNone   = l_BitMask
         PCCA.PCMGR.c_ToolBarLeft   = c_Default

      CASE c_ToolBarRight
         PCCA.PCMGR.c_ToolBarNone   = l_BitMask
         PCCA.PCMGR.c_ToolBarRight  = c_Default

      CASE c_ToolBarFloat
         PCCA.PCMGR.c_ToolBarNone   = l_BitMask
         PCCA.PCMGR.c_ToolBarFloat  = c_Default

      CASE ELSE
         // Should not get here.
   END CHOOSE
END IF

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   PCCA.PCMGR.c_ToolBarHideText = c_ToolBarTextMask
   PCCA.PCMGR.c_ToolBarShowText = c_Default
END IF
l_OptionWord = l_Divided
end subroutine

public subroutine fu_setwmoptions (w_main this_obj, unsignedlong lcontrolword);//******************************************************************
//  PC Module     : n_DLL_Main
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER       l_Idx
UNSIGNEDLONG  l_OptionWord, l_OptionBit, l_Divided, l_BitMask

l_OptionWord = lControlWord

//------------------------------------------------------------------

l_OptionBit = 1
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   This_Obj.i_LoadCodeTable = &
      (PCCA.PCMGR.c_LoadCodeTable = l_OptionBit)
ELSE
   This_Obj.i_LoadCodeTable = &
      (PCCA.PCMGR.c_LoadCodeTable = 0)
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   This_Obj.i_EnablePopup = &
      (PCCA.PCMGR.c_EnablePopup = l_OptionBit)
ELSE
   This_Obj.i_EnablePopup = &
      (PCCA.PCMGR.c_EnablePopup = 0)
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_BitMask = 0

FOR l_Idx = 1 TO 2
   l_OptionBit = 2 * l_OptionBit
   l_Divided   = l_OptionWord / 2
   IF (2 * l_Divided) <> l_OptionWord THEN
      l_BitMask = l_BitMask + l_OptionBit
   END IF
   l_OptionWord = l_Divided
NEXT

CHOOSE CASE l_BitMask
   CASE PCCA.PCMGR.c_NoResizeWin
      This_Obj.i_ResizeWin = FALSE
      This_Obj.i_ZoomWin   = FALSE

   CASE PCCA.PCMGR.c_ResizeWin
      This_Obj.i_ResizeWin = TRUE
      This_Obj.i_ZoomWin   = FALSE

   CASE PCCA.PCMGR.c_ZoomWin
      This_Obj.i_ResizeWin = TRUE
      This_Obj.i_ZoomWin   = TRUE

   //CASE PCCA.PCMGR.c_ResizeWin AND
   //     PCCA.PCMGR.c_ZoomWin
   CASE ELSE
      This_Obj.i_ResizeWin = TRUE
      This_Obj.i_ZoomWin   = TRUE
END CHOOSE

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   This_Obj.i_AutoMinimize = &
      (PCCA.PCMGR.c_AutoMinimize = l_OptionBit)
ELSE
   This_Obj.i_AutoMinimize = &
      (PCCA.PCMGR.c_AutoMinimize = 0)
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_BitMask = 0

FOR l_Idx = 1 TO 2
   l_OptionBit = 2 * l_OptionBit
   l_Divided   = l_OptionWord / 2
   IF (2 * l_Divided) <> l_OptionWord THEN
      l_BitMask = l_BitMask + l_OptionBit
   END IF
   l_OptionWord = l_Divided
NEXT

CHOOSE CASE l_BitMask
   CASE PCCA.PCMGR.c_AutoSize
      This_Obj.i_AutoSize   = TRUE
      This_Obj.i_AutoSize30 = FALSE

   CASE PCCA.PCMGR.c_AutoSize30
      This_Obj.i_AutoSize   = FALSE
      This_Obj.i_AutoSize30 = TRUE

   //CASE c_NoAutoSize
   CASE ELSE
      This_Obj.i_AutoSize   = FALSE
      This_Obj.i_AutoSize30 = FALSE
END CHOOSE

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   This_Obj.i_AutoPosition = &
      (PCCA.PCMGR.c_AutoPosition = l_OptionBit)
ELSE
   This_Obj.i_AutoPosition = &
      (PCCA.PCMGR.c_AutoPosition = 0)
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   This_Obj.i_SavePosition = &
      (PCCA.PCMGR.c_SavePosition = l_OptionBit)
ELSE
   This_Obj.i_SavePosition = &
      (PCCA.PCMGR.c_SavePosition = 0)
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_BitMask = 0

FOR l_Idx = 1 TO 2
   l_OptionBit = 2 * l_OptionBit
   l_Divided   = l_OptionWord / 2
   IF (2 * l_Divided) <> l_OptionWord THEN
      l_BitMask = l_BitMask + l_OptionBit
   END IF
   l_OptionWord = l_Divided
NEXT

CHOOSE CASE l_BitMask
   CASE PCCA.PCMGR.c_ClosePromptUserOnce
      This_Obj.i_SaveOnClose = c_SOCPromptUserOnce

   CASE PCCA.PCMGR.c_CloseSave
      This_Obj.i_SaveOnClose = c_SOCSave

   CASE PCCA.PCMGR.c_CloseNoSave
      This_Obj.i_SaveOnClose = c_SOCNoSave

   //CASE PCCA.PCMGR.c_ClosePromptUser
   CASE ELSE
      This_Obj.i_SaveOnClose = c_SOCPromptUser
END CHOOSE

//------------------------------------------------------------------

FOR l_Idx = 1 TO 13
   l_OptionBit  = 2 * l_OptionBit
   l_Divided    = l_OptionWord / 2
   l_OptionWord = l_Divided
NEXT

//------------------------------------------------------------------

l_BitMask = 0

FOR l_Idx = 1 TO 3
   l_OptionBit = 2 * l_OptionBit
   l_Divided   = l_OptionWord / 2
   IF (2 * l_Divided) <> l_OptionWord THEN
      l_BitMask = l_BitMask + l_OptionBit
   END IF
   l_OptionWord = l_Divided
NEXT

This_Obj.i_ToolBarPosition = l_BitMask

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   This_Obj.i_ToolBarText = &
      (PCCA.PCMGR.c_ToolBarShowText = l_OptionBit)
ELSE
   This_Obj.i_ToolBarText = &
      (PCCA.PCMGR.c_ToolBarShowText = 0)
END IF
l_OptionWord = l_Divided
end subroutine

public subroutine fu_setwmpmenu (w_main this_obj);//******************************************************************
//  PC Module     : n_DLL_Main
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

//------------------------------------------------------------------
// Main Window Popup Menu Bits
//------------------------------------------------------------------

This_Obj.i_PopupMenuInit          = This_Obj.i_EditMenuInit
This_Obj.i_PopupMenuIsValid       = This_Obj.i_EditMenuIsValid
This_Obj.i_PopupMenuIsEdit        = This_Obj.i_EditMenuIsPopup
This_Obj.i_PopupMenu              = This_Obj.i_EditMenu

This_Obj.i_PopupMenuNewNum        = This_Obj.i_EditMenuNewNum
This_Obj.i_PopupMenuViewNum       = This_Obj.i_EditMenuViewNum
This_Obj.i_PopupMenuModifyNum     = This_Obj.i_EditMenuModifyNum
This_Obj.i_PopupMenuInsertNum     = This_Obj.i_EditMenuInsertNum
This_Obj.i_PopupMenuDeleteNum     = This_Obj.i_EditMenuDeleteNum
This_Obj.i_PopupMenuModeSepNum    = This_Obj.i_EditMenuModeSepNum
This_Obj.i_PopupMenuFirstNum      = This_Obj.i_EditMenuFirstNum
This_Obj.i_PopupMenuPrevNum       = This_Obj.i_EditMenuPrevNum
This_Obj.i_PopupMenuNextNum       = This_Obj.i_EditMenuNextNum
This_Obj.i_PopupMenuLastNum       = This_Obj.i_EditMenuLastNum
This_Obj.i_PopupMenuSearchSepNum  = This_Obj.i_EditMenuSearchSepNum
This_Obj.i_PopupMenuQueryNum      = This_Obj.i_EditMenuQueryNum
This_Obj.i_PopupMenuSearchNum     = This_Obj.i_EditMenuSearchNum
This_Obj.i_PopupMenuFilterNum     = This_Obj.i_EditMenuFilterNum

This_Obj.i_PopupMenuSaveSepNum    = This_Obj.i_EditMenuSaveSepNum
This_Obj.i_PopupMenuSaveNum       = This_Obj.i_EditMenuSaveNum
This_Obj.i_PopupMenuSaveRowsAsNum = This_Obj.i_EditMenuSaveRowsAsNum
This_Obj.i_PopupMenuPrintSepNum   = This_Obj.i_EditMenuPrintSepNum
This_Obj.i_PopupMenuPrintNum      = This_Obj.i_EditMenuPrintNum

This_Obj.i_PopupMenuFirst         = This_Obj.i_EditMenuFirst
This_Obj.i_PopupMenuPrev          = This_Obj.i_EditMenuPrev
This_Obj.i_PopupMenuNext          = This_Obj.i_EditMenuNext
This_Obj.i_PopupMenuLast          = This_Obj.i_EditMenuLast
This_Obj.i_PopupMenuModeSep       = This_Obj.i_EditMenuModeSep
This_Obj.i_PopupMenuNew           = This_Obj.i_EditMenuNew
This_Obj.i_PopupMenuView          = This_Obj.i_EditMenuView
This_Obj.i_PopupMenuModify        = This_Obj.i_EditMenuModify
This_Obj.i_PopupMenuInsert        = This_Obj.i_EditMenuInsert
This_Obj.i_PopupMenuDelete        = This_Obj.i_EditMenuDelete
This_Obj.i_PopupMenuSearchSep     = This_Obj.i_EditMenuSearchSep
This_Obj.i_PopupMenuQuery         = This_Obj.i_EditMenuQuery
This_Obj.i_PopupMenuSearch        = This_Obj.i_EditMenuSearch
This_Obj.i_PopupMenuFilter        = This_Obj.i_EditMenuFilter

This_Obj.i_PopupMenuSaveSep       = This_Obj.i_EditMenuSaveSep
This_Obj.i_PopupMenuSave          = This_Obj.i_EditMenuSave
This_Obj.i_PopupMenuSaveRowsAs    = This_Obj.i_EditMenuSaveRowsAs
This_Obj.i_PopupMenuPrintSep      = This_Obj.i_EditMenuPrintSep
This_Obj.i_PopupMenuPrint         = This_Obj.i_EditMenuPrint

end subroutine

public subroutine fu_setwroptions (w_response this_obj, unsignedlong lcontrolword);//******************************************************************
//  PC Module     : n_DLL_Main
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER       l_Idx
UNSIGNEDLONG  l_OptionWord, l_OptionBit, l_Divided, l_BitMask

l_OptionWord = lControlWord

//------------------------------------------------------------------

l_OptionBit = 1
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   This_Obj.i_LoadCodeTable = &
      (PCCA.PCMGR.c_LoadCodeTable = l_OptionBit)
ELSE
   This_Obj.i_LoadCodeTable = &
      (PCCA.PCMGR.c_LoadCodeTable = 0)
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   This_Obj.i_EnablePopup = &
      (PCCA.PCMGR.c_EnablePopup = l_OptionBit)
ELSE
   This_Obj.i_EnablePopup = &
      (PCCA.PCMGR.c_EnablePopup = 0)
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_BitMask = 0

FOR l_Idx = 1 TO 2
   l_OptionBit = 2 * l_OptionBit
   l_Divided   = l_OptionWord / 2
   IF (2 * l_Divided) <> l_OptionWord THEN
      l_BitMask = l_BitMask + l_OptionBit
   END IF
   l_OptionWord = l_Divided
NEXT

CHOOSE CASE l_BitMask
   CASE PCCA.PCMGR.c_NoResizeWin
      This_Obj.i_ResizeWin = FALSE
      This_Obj.i_ZoomWin   = FALSE

   CASE PCCA.PCMGR.c_ResizeWin
      This_Obj.i_ResizeWin = TRUE
      This_Obj.i_ZoomWin   = FALSE

   CASE PCCA.PCMGR.c_ZoomWin
      This_Obj.i_ResizeWin = TRUE
      This_Obj.i_ZoomWin   = TRUE

   //CASE PCCA.PCMGR.c_ResizeWin AND
   //     PCCA.PCMGR.c_ZoomWin
   CASE ELSE
      This_Obj.i_ResizeWin = TRUE
      This_Obj.i_ZoomWin   = TRUE
END CHOOSE

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   This_Obj.i_AutoMinimize = &
      (PCCA.PCMGR.c_AutoMinimize = l_OptionBit)
ELSE
   This_Obj.i_AutoMinimize = &
      (PCCA.PCMGR.c_AutoMinimize = 0)
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_BitMask = 0

FOR l_Idx = 1 TO 2
   l_OptionBit = 2 * l_OptionBit
   l_Divided   = l_OptionWord / 2
   IF (2 * l_Divided) <> l_OptionWord THEN
      l_BitMask = l_BitMask + l_OptionBit
   END IF
   l_OptionWord = l_Divided
NEXT

CHOOSE CASE l_BitMask
   CASE PCCA.PCMGR.c_AutoSize
      This_Obj.i_AutoSize   = TRUE
      This_Obj.i_AutoSize30 = FALSE

   CASE PCCA.PCMGR.c_AutoSize30
      This_Obj.i_AutoSize   = FALSE
      This_Obj.i_AutoSize30 = TRUE

   //CASE c_NoAutoSize
   CASE ELSE
      This_Obj.i_AutoSize   = FALSE
      This_Obj.i_AutoSize30 = FALSE
END CHOOSE

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   This_Obj.i_AutoPosition = &
      (PCCA.PCMGR.c_AutoPosition = l_OptionBit)
ELSE
   This_Obj.i_AutoPosition = &
      (PCCA.PCMGR.c_AutoPosition = 0)
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   This_Obj.i_SavePosition = &
      (PCCA.PCMGR.c_SavePosition = l_OptionBit)
ELSE
   This_Obj.i_SavePosition = &
      (PCCA.PCMGR.c_SavePosition = 0)
END IF
l_OptionWord = l_Divided

//------------------------------------------------------------------

l_BitMask = 0

FOR l_Idx = 1 TO 2
   l_OptionBit = 2 * l_OptionBit
   l_Divided   = l_OptionWord / 2
   IF (2 * l_Divided) <> l_OptionWord THEN
      l_BitMask = l_BitMask + l_OptionBit
   END IF
   l_OptionWord = l_Divided
NEXT

CHOOSE CASE l_BitMask
   CASE PCCA.PCMGR.c_ClosePromptUserOnce
      This_Obj.i_SaveOnClose = c_SOCPromptUserOnce

   CASE PCCA.PCMGR.c_CloseSave
      This_Obj.i_SaveOnClose = c_SOCSave

   CASE PCCA.PCMGR.c_CloseNoSave
      This_Obj.i_SaveOnClose = c_SOCNoSave

   //CASE PCCA.PCMGR.c_ClosePromptUser
   CASE ELSE
      This_Obj.i_SaveOnClose = c_SOCPromptUser
END CHOOSE

//------------------------------------------------------------------

FOR l_Idx = 1 TO 13
   l_OptionBit  = 2 * l_OptionBit
   l_Divided    = l_OptionWord / 2
   l_OptionWord = l_Divided
NEXT

//------------------------------------------------------------------

l_BitMask = 0

FOR l_Idx = 1 TO 3
   l_OptionBit = 2 * l_OptionBit
   l_Divided   = l_OptionWord / 2
   IF (2 * l_Divided) <> l_OptionWord THEN
      l_BitMask = l_BitMask + l_OptionBit
   END IF
   l_OptionWord = l_Divided
NEXT

This_Obj.i_ToolBarPosition = l_BitMask

//------------------------------------------------------------------

l_OptionBit = 2 * l_OptionBit
l_Divided   = l_OptionWord / 2
IF  (2 * l_Divided) <> l_OptionWord THEN
   This_Obj.i_ToolBarText = &
      (PCCA.PCMGR.c_ToolBarShowText = l_OptionBit)
ELSE
   This_Obj.i_ToolBarText = &
      (PCCA.PCMGR.c_ToolBarShowText = 0)
END IF
l_OptionWord = l_Divided
end subroutine

public subroutine fu_setwrpmenu (w_response this_obj);//******************************************************************
//  PC Module     : n_DLL_Main
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

//------------------------------------------------------------------
//  Response Window Popup Menu Bits
//------------------------------------------------------------------

This_Obj.i_PopupMenuInit          = This_Obj.i_EditMenuInit
This_Obj.i_PopupMenuIsValid       = This_Obj.i_EditMenuIsValid
This_Obj.i_PopupMenuIsEdit        = This_Obj.i_EditMenuIsPopup
This_Obj.i_PopupMenu              = This_Obj.i_EditMenu

This_Obj.i_PopupMenuNewNum        = This_Obj.i_EditMenuNewNum
This_Obj.i_PopupMenuViewNum       = This_Obj.i_EditMenuViewNum
This_Obj.i_PopupMenuModifyNum     = This_Obj.i_EditMenuModifyNum
This_Obj.i_PopupMenuInsertNum     = This_Obj.i_EditMenuInsertNum
This_Obj.i_PopupMenuDeleteNum     = This_Obj.i_EditMenuDeleteNum
This_Obj.i_PopupMenuModeSepNum    = This_Obj.i_EditMenuModeSepNum
This_Obj.i_PopupMenuFirstNum      = This_Obj.i_EditMenuFirstNum
This_Obj.i_PopupMenuPrevNum       = This_Obj.i_EditMenuPrevNum
This_Obj.i_PopupMenuNextNum       = This_Obj.i_EditMenuNextNum
This_Obj.i_PopupMenuLastNum       = This_Obj.i_EditMenuLastNum
This_Obj.i_PopupMenuSearchSepNum  = This_Obj.i_EditMenuSearchSepNum
This_Obj.i_PopupMenuQueryNum      = This_Obj.i_EditMenuQueryNum
This_Obj.i_PopupMenuSearchNum     = This_Obj.i_EditMenuSearchNum
This_Obj.i_PopupMenuFilterNum     = This_Obj.i_EditMenuFilterNum

This_Obj.i_PopupMenuSaveSepNum    = This_Obj.i_EditMenuSaveSepNum
This_Obj.i_PopupMenuSaveNum       = This_Obj.i_EditMenuSaveNum
This_Obj.i_PopupMenuSaveRowsAsNum = This_Obj.i_EditMenuSaveRowsAsNum
This_Obj.i_PopupMenuPrintSepNum   = This_Obj.i_EditMenuPrintSepNum
This_Obj.i_PopupMenuPrintNum      = This_Obj.i_EditMenuPrintNum

This_Obj.i_PopupMenuFirst         = This_Obj.i_EditMenuFirst
This_Obj.i_PopupMenuPrev          = This_Obj.i_EditMenuPrev
This_Obj.i_PopupMenuNext          = This_Obj.i_EditMenuNext
This_Obj.i_PopupMenuLast          = This_Obj.i_EditMenuLast
This_Obj.i_PopupMenuModeSep       = This_Obj.i_EditMenuModeSep
This_Obj.i_PopupMenuNew           = This_Obj.i_EditMenuNew
This_Obj.i_PopupMenuView          = This_Obj.i_EditMenuView
This_Obj.i_PopupMenuModify        = This_Obj.i_EditMenuModify
This_Obj.i_PopupMenuInsert        = This_Obj.i_EditMenuInsert
This_Obj.i_PopupMenuDelete        = This_Obj.i_EditMenuDelete
This_Obj.i_PopupMenuSearchSep     = This_Obj.i_EditMenuSearchSep
This_Obj.i_PopupMenuQuery         = This_Obj.i_EditMenuQuery
This_Obj.i_PopupMenuSearch        = This_Obj.i_EditMenuSearch
This_Obj.i_PopupMenuFilter        = This_Obj.i_EditMenuFilter

This_Obj.i_PopupMenuSaveSep       = This_Obj.i_EditMenuSaveSep
This_Obj.i_PopupMenuSave          = This_Obj.i_EditMenuSave
This_Obj.i_PopupMenuSaveRowsAs    = This_Obj.i_EditMenuSaveRowsAs
This_Obj.i_PopupMenuPrintSep      = This_Obj.i_EditMenuPrintSep
This_Obj.i_PopupMenuPrint         = This_Obj.i_EditMenuPrint

end subroutine

public subroutine fu_wminitdwbits (w_main this_obj);//******************************************************************
//  PC Module     : n_DLL_Main
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

This_Obj.c_MasterList                = PCCA.PCMGR.c_MasterList
This_Obj.c_MasterEdit                = PCCA.PCMGR.c_MasterEdit
This_Obj.c_DetailList                = PCCA.PCMGR.c_DetailList
This_Obj.c_DetailEdit                = PCCA.PCMGR.c_DetailEdit

This_Obj.c_ScrollSelf                = PCCA.PCMGR.c_ScrollSelf
This_Obj.c_ScrollParent              = PCCA.PCMGR.c_ScrollParent

This_Obj.c_IsNotInstance             = PCCA.PCMGR.c_IsNotInstance
This_Obj.c_IsInstance                = PCCA.PCMGR.c_IsInstance

This_Obj.c_RequiredOnSave            = PCCA.PCMGR.c_RequiredOnSave
This_Obj.c_AlwaysCheckRequired       = PCCA.PCMGR.c_AlwaysCheckRequired

This_Obj.c_NoNew                     = PCCA.PCMGR.c_NoNew
This_Obj.c_NewOk                     = PCCA.PCMGR.c_NewOk

This_Obj.c_NoModify                  = PCCA.PCMGR.c_NoModify
This_Obj.c_ModifyOk                  = PCCA.PCMGR.c_ModifyOk

This_Obj.c_NoDelete                  = PCCA.PCMGR.c_NoDelete
This_Obj.c_DeleteOk                  = PCCA.PCMGR.c_DeleteOk

This_Obj.c_NoQuery                   = PCCA.PCMGR.c_NoQuery
This_Obj.c_QueryOk                   = PCCA.PCMGR.c_QueryOk

This_Obj.c_SelectOnDoubleClick       = PCCA.PCMGR.c_SelectOnDoubleClick
This_Obj.c_SelectOnClick             = PCCA.PCMGR.c_SelectOnClick
This_Obj.c_SelectOnRowFocusChange    = PCCA.PCMGR.c_SelectOnRowFocusChange

This_Obj.c_NoDrillDown               = PCCA.PCMGR.c_NoDrillDown
This_Obj.c_DrillDown                 = PCCA.PCMGR.c_DrillDown

This_Obj.c_SameModeOnSelect          = PCCA.PCMGR.c_SameModeOnSelect
This_Obj.c_ViewOnSelect              = PCCA.PCMGR.c_ViewOnSelect
This_Obj.c_ModifyOnSelect            = PCCA.PCMGR.c_ModifyOnSelect

This_Obj.c_NoMultiSelect             = PCCA.PCMGR.c_NoMultiSelect
This_Obj.c_MultiSelect               = PCCA.PCMGR.c_MultiSelect

This_Obj.c_RefreshOnSelect           = PCCA.PCMGR.c_RefreshOnSelect
This_Obj.c_NoAutoRefresh             = PCCA.PCMGR.c_NoAutoRefresh
This_Obj.c_RefreshOnMultiSelect      = PCCA.PCMGR.c_RefreshOnMultiSelect

This_Obj.c_ParentModeOnOpen          = PCCA.PCMGR.c_ParentModeOnOpen
This_Obj.c_ViewOnOpen                = PCCA.PCMGR.c_ViewOnOpen
This_Obj.c_ModifyOnOpen              = PCCA.PCMGR.c_ModifyOnOpen
This_Obj.c_NewOnOpen                 = PCCA.PCMGR.c_NewOnOpen

This_Obj.c_SameModeAfterSave         = PCCA.PCMGR.c_SameModeAfterSave
This_Obj.c_ViewAfterSave             = PCCA.PCMGR.c_ViewAfterSave

This_Obj.c_NoEnableModifyOnOpen      = PCCA.PCMGR.c_NoEnableModifyOnOpen
This_Obj.c_EnableModifyOnOpen        = PCCA.PCMGR.c_EnableModifyOnOpen

This_Obj.c_NoEnableNewOnOpen         = PCCA.PCMGR.c_NoEnableNewOnOpen
This_Obj.c_EnableNewOnOpen           = PCCA.PCMGR.c_EnableNewOnOpen

This_Obj.c_RetrieveOnOpen            = PCCA.PCMGR.c_RetrieveOnOpen
This_Obj.c_AutoRetrieveOnOpen        = PCCA.PCMGR.c_AutoRetrieveOnOpen
This_Obj.c_NoRetrieveOnOpen          = PCCA.PCMGR.c_NoRetrieveOnOpen

This_Obj.c_AutoRefresh               = PCCA.PCMGR.c_AutoRefresh
This_Obj.c_SkipRefresh               = PCCA.PCMGR.c_SkipRefresh
This_Obj.c_TriggerRefresh            = PCCA.PCMGR.c_TriggerRefresh
This_Obj.c_PostRefresh               = PCCA.PCMGR.c_PostRefresh

This_Obj.c_RetrieveAll               = PCCA.PCMGR.c_RetrieveAll
This_Obj.c_RetrieveAsNeeded          = PCCA.PCMGR.c_RetrieveAsNeeded

This_Obj.c_NoShareData               = PCCA.PCMGR.c_NoShareData
This_Obj.c_ShareData                 = PCCA.PCMGR.c_ShareData

This_Obj.c_NoRefreshParent           = PCCA.PCMGR.c_NoRefreshParent
This_Obj.c_RefreshParent             = PCCA.PCMGR.c_RefreshParent

This_Obj.c_NoRefreshChild            = PCCA.PCMGR.c_NoRefreshChild
This_Obj.c_RefreshChild              = PCCA.PCMGR.c_RefreshChild

This_Obj.c_IgnoreNewRows             = PCCA.PCMGR.c_IgnoreNewRows
This_Obj.c_NoIgnoreNewRows           = PCCA.PCMGR.c_NoIgnoreNewRows

This_Obj.c_NoNewModeOnEmpty          = PCCA.PCMGR.c_NoNewModeOnEmpty
This_Obj.c_NewModeOnEmpty            = PCCA.PCMGR.c_NewModeOnEmpty

This_Obj.c_MultipleNewRows           = PCCA.PCMGR.c_MultipleNewRows
This_Obj.c_OnlyOneNewRow             = PCCA.PCMGR.c_OnlyOneNewRow

This_Obj.c_DisableCC                 = PCCA.PCMGR.c_DisableCC
This_Obj.c_EnableCC                  = PCCA.PCMGR.c_EnableCC

This_Obj.c_DisableCCInsert           = PCCA.PCMGR.c_DisableCCInsert
This_Obj.c_EnableCCInsert            = PCCA.PCMGR.c_EnableCCInsert

This_Obj.c_ViewModeBorderUnchanged   = PCCA.PCMGR.c_ViewModeBorderUnchanged
This_Obj.c_ViewModeNoBorder          = PCCA.PCMGR.c_ViewModeNoBorder
This_Obj.c_ViewModeShadowBox         = PCCA.PCMGR.c_ViewModeShadowBox
This_Obj.c_ViewModeBox               = PCCA.PCMGR.c_ViewModeBox
This_Obj.c_ViewModeResize            = PCCA.PCMGR.c_ViewModeResize
This_Obj.c_ViewModeUnderline         = PCCA.PCMGR.c_ViewModeUnderline
This_Obj.c_ViewModeLowered           = PCCA.PCMGR.c_ViewModeLowered
This_Obj.c_ViewModeRaised            = PCCA.PCMGR.c_ViewModeRaised

This_Obj.c_ViewModeColorUnchanged    = PCCA.PCMGR.c_ViewModeColorUnchanged
This_Obj.c_ViewModeBlack             = PCCA.PCMGR.c_ViewModeBlack
This_Obj.c_ViewModeWhite             = PCCA.PCMGR.c_ViewModeWhite
This_Obj.c_ViewModeGray              = PCCA.PCMGR.c_ViewModeGray
This_Obj.c_ViewModeLightGray         = PCCA.PCMGR.c_ViewModeLightGray
This_Obj.c_ViewModeDarkGray          = PCCA.PCMGR.c_ViewModeDarkGray
This_Obj.c_ViewModeRed               = PCCA.PCMGR.c_ViewModeRed
This_Obj.c_ViewModeDarkRed           = PCCA.PCMGR.c_ViewModeDarkRed
This_Obj.c_ViewModeGreen             = PCCA.PCMGR.c_ViewModeGreen
This_Obj.c_ViewModeDarkGreen         = PCCA.PCMGR.c_ViewModeDarkGreen
This_Obj.c_ViewModeBlue              = PCCA.PCMGR.c_ViewModeBlue
This_Obj.c_ViewModeDarkBlue          = PCCA.PCMGR.c_ViewModeDarkBlue
This_Obj.c_ViewModeMagenta           = PCCA.PCMGR.c_ViewModeMagenta
This_Obj.c_ViewModeDarkMagenta       = PCCA.PCMGR.c_ViewModeDarkMagenta
This_Obj.c_ViewModeCyan              = PCCA.PCMGR.c_ViewModeCyan
This_Obj.c_ViewModeDarkCyan          = PCCA.PCMGR.c_ViewModeDarkCyan
This_Obj.c_ViewModeYellow            = PCCA.PCMGR.c_ViewModeYellow
This_Obj.c_ViewModeBrown             = PCCA.PCMGR.c_ViewModeBrown

This_Obj.c_InactiveDWColorUnchanged  = PCCA.PCMGR.c_InactiveDWColorUnchanged
This_Obj.c_InactiveDWBlack           = PCCA.PCMGR.c_InactiveDWBlack
This_Obj.c_InactiveDWWhite           = PCCA.PCMGR.c_InactiveDWWhite
This_Obj.c_InactiveDWGray            = PCCA.PCMGR.c_InactiveDWGray
This_Obj.c_InactiveDWLightGray       = PCCA.PCMGR.c_InactiveDWLightGray
This_Obj.c_InactiveDWDarkGray        = PCCA.PCMGR.c_InactiveDWDarkGray
This_Obj.c_InactiveDWRed             = PCCA.PCMGR.c_InactiveDWRed
This_Obj.c_InactiveDWDarkRed         = PCCA.PCMGR.c_InactiveDWDarkRed
This_Obj.c_InactiveDWGreen           = PCCA.PCMGR.c_InactiveDWGreen
This_Obj.c_InactiveDWDarkGreen       = PCCA.PCMGR.c_InactiveDWDarkGreen
This_Obj.c_InactiveDWBlue            = PCCA.PCMGR.c_InactiveDWBlue
This_Obj.c_InactiveDWDarkBlue        = PCCA.PCMGR.c_InactiveDWDarkBlue
This_Obj.c_InactiveDWMagenta         = PCCA.PCMGR.c_InactiveDWMagenta
This_Obj.c_InactiveDWDarkMagenta     = PCCA.PCMGR.c_InactiveDWDarkMagenta
This_Obj.c_InactiveDWCyan            = PCCA.PCMGR.c_InactiveDWCyan
This_Obj.c_InactiveDWDarkCyan        = PCCA.PCMGR.c_InactiveDWDarkCyan
This_Obj.c_InactiveDWYellow          = PCCA.PCMGR.c_InactiveDWYellow
This_Obj.c_InactiveDWBrown           = PCCA.PCMGR.c_InactiveDWBrown

This_Obj.c_NoInactiveText            = PCCA.PCMGR.c_NoInactiveText
This_Obj.c_InactiveText              = PCCA.PCMGR.c_InactiveText
This_Obj.c_NoInactiveLine            = PCCA.PCMGR.c_NoInactiveLine
This_Obj.c_InactiveLine              = PCCA.PCMGR.c_InactiveLine
This_Obj.c_NoInactiveCol             = PCCA.PCMGR.c_NoInactiveCol
This_Obj.c_InactiveCol               = PCCA.PCMGR.c_InactiveCol

This_Obj.c_CursorRowFocusRect        = PCCA.PCMGR.c_CursorRowFocusRect
This_Obj.c_NoCursorRowFocusRect      = PCCA.PCMGR.c_NoCursorRowFocusRect

This_Obj.c_CursorRowPointer          = PCCA.PCMGR.c_CursorRowPointer
This_Obj.c_NoCursorRowPointer        = PCCA.PCMGR.c_NoCursorRowPointer

This_Obj.c_CalcDWStyle               = PCCA.PCMGR.c_CalcDWStyle
This_Obj.c_FreeFormStyle             = PCCA.PCMGR.c_FreeFormStyle
This_Obj.c_TabularFormStyle          = PCCA.PCMGR.c_TabularFormStyle

This_Obj.c_AutoCalcHighlightSelected = PCCA.PCMGR.c_AutoCalcHighlightSelected
This_Obj.c_HighlightSelected         = PCCA.PCMGR.c_HighlightSelected
This_Obj.c_NoHighlightSelected       = PCCA.PCMGR.c_NoHighlightSelected

This_Obj.c_NoResizeDW                = PCCA.PCMGR.c_NoResizeDW
This_Obj.c_ResizeDW                  = PCCA.PCMGR.c_ResizeDW

This_Obj.c_NoAutoConfigMenus         = PCCA.PCMGR.c_NoAutoConfigMenus
This_Obj.c_AutoConfigMenus           = PCCA.PCMGR.c_AutoConfigMenus

This_Obj.c_ShowEmpty                 = PCCA.PCMGR.c_ShowEmpty
This_Obj.c_NoShowEmpty               = PCCA.PCMGR.c_NoShowEmpty

This_Obj.c_AutoFocus                 = PCCA.PCMGR.c_AutoFocus
This_Obj.c_NoAutoFocus               = PCCA.PCMGR.c_NoAutoFocus

This_Obj.c_CCErrorRed                = PCCA.PCMGR.c_CCErrorRed
This_Obj.c_CCErrorBlack              = PCCA.PCMGR.c_CCErrorBlack
This_Obj.c_CCErrorWhite              = PCCA.PCMGR.c_CCErrorWhite
This_Obj.c_CCErrorGray               = PCCA.PCMGR.c_CCErrorGray
This_Obj.c_CCErrorLightGray          = PCCA.PCMGR.c_CCErrorLightGray
This_Obj.c_CCErrorDarkGray           = PCCA.PCMGR.c_CCErrorDarkGray
This_Obj.c_CCErrorDarkRed            = PCCA.PCMGR.c_CCErrorDarkRed
This_Obj.c_CCErrorGreen              = PCCA.PCMGR.c_CCErrorGreen
This_Obj.c_CCErrorDarkGreen          = PCCA.PCMGR.c_CCErrorDarkGreen
This_Obj.c_CCErrorBlue               = PCCA.PCMGR.c_CCErrorBlue
This_Obj.c_CCErrorDarkBlue           = PCCA.PCMGR.c_CCErrorDarkBlue
This_Obj.c_CCErrorMagenta            = PCCA.PCMGR.c_CCErrorMagenta
This_Obj.c_CCErrorDarkMagenta        = PCCA.PCMGR.c_CCErrorDarkMagenta
This_Obj.c_CCErrorCyan               = PCCA.PCMGR.c_CCErrorCyan
This_Obj.c_CCErrorDarkCyan           = PCCA.PCMGR.c_CCErrorDarkCyan
This_Obj.c_CCErrorYellow             = PCCA.PCMGR.c_CCErrorYellow
This_Obj.c_CCErrorBrown              = PCCA.PCMGR.c_CCErrorBrown

end subroutine

public subroutine fu_wrinitdwbits (w_response this_obj);//******************************************************************
//  PC Module     : n_DLL_Main
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

This_Obj.c_MasterList                = PCCA.PCMGR.c_MasterList
This_Obj.c_MasterEdit                = PCCA.PCMGR.c_MasterEdit
This_Obj.c_DetailList                = PCCA.PCMGR.c_DetailList
This_Obj.c_DetailEdit                = PCCA.PCMGR.c_DetailEdit

This_Obj.c_ScrollSelf                = PCCA.PCMGR.c_ScrollSelf
This_Obj.c_ScrollParent              = PCCA.PCMGR.c_ScrollParent

This_Obj.c_IsNotInstance             = PCCA.PCMGR.c_IsNotInstance
This_Obj.c_IsInstance                = PCCA.PCMGR.c_IsInstance

This_Obj.c_RequiredOnSave            = PCCA.PCMGR.c_RequiredOnSave
This_Obj.c_AlwaysCheckRequired       = PCCA.PCMGR.c_AlwaysCheckRequired

This_Obj.c_NoNew                     = PCCA.PCMGR.c_NoNew
This_Obj.c_NewOk                     = PCCA.PCMGR.c_NewOk

This_Obj.c_NoModify                  = PCCA.PCMGR.c_NoModify
This_Obj.c_ModifyOk                  = PCCA.PCMGR.c_ModifyOk

This_Obj.c_NoDelete                  = PCCA.PCMGR.c_NoDelete
This_Obj.c_DeleteOk                  = PCCA.PCMGR.c_DeleteOk

This_Obj.c_NoQuery                   = PCCA.PCMGR.c_NoQuery
This_Obj.c_QueryOk                   = PCCA.PCMGR.c_QueryOk

This_Obj.c_SelectOnDoubleClick       = PCCA.PCMGR.c_SelectOnDoubleClick
This_Obj.c_SelectOnClick             = PCCA.PCMGR.c_SelectOnClick
This_Obj.c_SelectOnRowFocusChange    = PCCA.PCMGR.c_SelectOnRowFocusChange

This_Obj.c_NoDrillDown               = PCCA.PCMGR.c_NoDrillDown
This_Obj.c_DrillDown                 = PCCA.PCMGR.c_DrillDown

This_Obj.c_SameModeOnSelect          = PCCA.PCMGR.c_SameModeOnSelect
This_Obj.c_ViewOnSelect              = PCCA.PCMGR.c_ViewOnSelect
This_Obj.c_ModifyOnSelect            = PCCA.PCMGR.c_ModifyOnSelect

This_Obj.c_NoMultiSelect             = PCCA.PCMGR.c_NoMultiSelect
This_Obj.c_MultiSelect               = PCCA.PCMGR.c_MultiSelect

This_Obj.c_RefreshOnSelect           = PCCA.PCMGR.c_RefreshOnSelect
This_Obj.c_NoAutoRefresh             = PCCA.PCMGR.c_NoAutoRefresh
This_Obj.c_RefreshOnMultiSelect      = PCCA.PCMGR.c_RefreshOnMultiSelect

This_Obj.c_ParentModeOnOpen          = PCCA.PCMGR.c_ParentModeOnOpen
This_Obj.c_ViewOnOpen                = PCCA.PCMGR.c_ViewOnOpen
This_Obj.c_ModifyOnOpen              = PCCA.PCMGR.c_ModifyOnOpen
This_Obj.c_NewOnOpen                 = PCCA.PCMGR.c_NewOnOpen

This_Obj.c_SameModeAfterSave         = PCCA.PCMGR.c_SameModeAfterSave
This_Obj.c_ViewAfterSave             = PCCA.PCMGR.c_ViewAfterSave

This_Obj.c_NoEnableModifyOnOpen      = PCCA.PCMGR.c_NoEnableModifyOnOpen
This_Obj.c_EnableModifyOnOpen        = PCCA.PCMGR.c_EnableModifyOnOpen

This_Obj.c_NoEnableNewOnOpen         = PCCA.PCMGR.c_NoEnableNewOnOpen
This_Obj.c_EnableNewOnOpen           = PCCA.PCMGR.c_EnableNewOnOpen

This_Obj.c_RetrieveOnOpen            = PCCA.PCMGR.c_RetrieveOnOpen
This_Obj.c_AutoRetrieveOnOpen        = PCCA.PCMGR.c_AutoRetrieveOnOpen
This_Obj.c_NoRetrieveOnOpen          = PCCA.PCMGR.c_NoRetrieveOnOpen

This_Obj.c_AutoRefresh               = PCCA.PCMGR.c_AutoRefresh
This_Obj.c_SkipRefresh               = PCCA.PCMGR.c_SkipRefresh
This_Obj.c_TriggerRefresh            = PCCA.PCMGR.c_TriggerRefresh
This_Obj.c_PostRefresh               = PCCA.PCMGR.c_PostRefresh

This_Obj.c_RetrieveAll               = PCCA.PCMGR.c_RetrieveAll
This_Obj.c_RetrieveAsNeeded          = PCCA.PCMGR.c_RetrieveAsNeeded

This_Obj.c_NoShareData               = PCCA.PCMGR.c_NoShareData
This_Obj.c_ShareData                 = PCCA.PCMGR.c_ShareData

This_Obj.c_NoRefreshParent           = PCCA.PCMGR.c_NoRefreshParent
This_Obj.c_RefreshParent             = PCCA.PCMGR.c_RefreshParent

This_Obj.c_NoRefreshChild            = PCCA.PCMGR.c_NoRefreshChild
This_Obj.c_RefreshChild              = PCCA.PCMGR.c_RefreshChild

This_Obj.c_IgnoreNewRows             = PCCA.PCMGR.c_IgnoreNewRows
This_Obj.c_NoIgnoreNewRows           = PCCA.PCMGR.c_NoIgnoreNewRows

This_Obj.c_NoNewModeOnEmpty          = PCCA.PCMGR.c_NoNewModeOnEmpty
This_Obj.c_NewModeOnEmpty            = PCCA.PCMGR.c_NewModeOnEmpty

This_Obj.c_MultipleNewRows           = PCCA.PCMGR.c_MultipleNewRows
This_Obj.c_OnlyOneNewRow             = PCCA.PCMGR.c_OnlyOneNewRow

This_Obj.c_DisableCC                 = PCCA.PCMGR.c_DisableCC
This_Obj.c_EnableCC                  = PCCA.PCMGR.c_EnableCC

This_Obj.c_DisableCCInsert           = PCCA.PCMGR.c_DisableCCInsert
This_Obj.c_EnableCCInsert            = PCCA.PCMGR.c_EnableCCInsert

This_Obj.c_ViewModeBorderUnchanged   = PCCA.PCMGR.c_ViewModeBorderUnchanged
This_Obj.c_ViewModeNoBorder          = PCCA.PCMGR.c_ViewModeNoBorder
This_Obj.c_ViewModeShadowBox         = PCCA.PCMGR.c_ViewModeShadowBox
This_Obj.c_ViewModeBox               = PCCA.PCMGR.c_ViewModeBox
This_Obj.c_ViewModeResize            = PCCA.PCMGR.c_ViewModeResize
This_Obj.c_ViewModeUnderline         = PCCA.PCMGR.c_ViewModeUnderline
This_Obj.c_ViewModeLowered           = PCCA.PCMGR.c_ViewModeLowered
This_Obj.c_ViewModeRaised            = PCCA.PCMGR.c_ViewModeRaised

This_Obj.c_ViewModeColorUnchanged    = PCCA.PCMGR.c_ViewModeColorUnchanged
This_Obj.c_ViewModeBlack             = PCCA.PCMGR.c_ViewModeBlack
This_Obj.c_ViewModeWhite             = PCCA.PCMGR.c_ViewModeWhite
This_Obj.c_ViewModeGray              = PCCA.PCMGR.c_ViewModeGray
This_Obj.c_ViewModeLightGray         = PCCA.PCMGR.c_ViewModeLightGray
This_Obj.c_ViewModeDarkGray          = PCCA.PCMGR.c_ViewModeDarkGray
This_Obj.c_ViewModeRed               = PCCA.PCMGR.c_ViewModeRed
This_Obj.c_ViewModeDarkRed           = PCCA.PCMGR.c_ViewModeDarkRed
This_Obj.c_ViewModeGreen             = PCCA.PCMGR.c_ViewModeGreen
This_Obj.c_ViewModeDarkGreen         = PCCA.PCMGR.c_ViewModeDarkGreen
This_Obj.c_ViewModeBlue              = PCCA.PCMGR.c_ViewModeBlue
This_Obj.c_ViewModeDarkBlue          = PCCA.PCMGR.c_ViewModeDarkBlue
This_Obj.c_ViewModeMagenta           = PCCA.PCMGR.c_ViewModeMagenta
This_Obj.c_ViewModeDarkMagenta       = PCCA.PCMGR.c_ViewModeDarkMagenta
This_Obj.c_ViewModeCyan              = PCCA.PCMGR.c_ViewModeCyan
This_Obj.c_ViewModeDarkCyan          = PCCA.PCMGR.c_ViewModeDarkCyan
This_Obj.c_ViewModeYellow            = PCCA.PCMGR.c_ViewModeYellow
This_Obj.c_ViewModeBrown             = PCCA.PCMGR.c_ViewModeBrown

This_Obj.c_InactiveDWColorUnchanged  = PCCA.PCMGR.c_InactiveDWColorUnchanged
This_Obj.c_InactiveDWBlack           = PCCA.PCMGR.c_InactiveDWBlack
This_Obj.c_InactiveDWWhite           = PCCA.PCMGR.c_InactiveDWWhite
This_Obj.c_InactiveDWGray            = PCCA.PCMGR.c_InactiveDWGray
This_Obj.c_InactiveDWLightGray       = PCCA.PCMGR.c_InactiveDWLightGray
This_Obj.c_InactiveDWDarkGray        = PCCA.PCMGR.c_InactiveDWDarkGray
This_Obj.c_InactiveDWRed             = PCCA.PCMGR.c_InactiveDWRed
This_Obj.c_InactiveDWDarkRed         = PCCA.PCMGR.c_InactiveDWDarkRed
This_Obj.c_InactiveDWGreen           = PCCA.PCMGR.c_InactiveDWGreen
This_Obj.c_InactiveDWDarkGreen       = PCCA.PCMGR.c_InactiveDWDarkGreen
This_Obj.c_InactiveDWBlue            = PCCA.PCMGR.c_InactiveDWBlue
This_Obj.c_InactiveDWDarkBlue        = PCCA.PCMGR.c_InactiveDWDarkBlue
This_Obj.c_InactiveDWMagenta         = PCCA.PCMGR.c_InactiveDWMagenta
This_Obj.c_InactiveDWDarkMagenta     = PCCA.PCMGR.c_InactiveDWDarkMagenta
This_Obj.c_InactiveDWCyan            = PCCA.PCMGR.c_InactiveDWCyan
This_Obj.c_InactiveDWDarkCyan        = PCCA.PCMGR.c_InactiveDWDarkCyan
This_Obj.c_InactiveDWYellow          = PCCA.PCMGR.c_InactiveDWYellow
This_Obj.c_InactiveDWBrown           = PCCA.PCMGR.c_InactiveDWBrown

This_Obj.c_NoInactiveText            = PCCA.PCMGR.c_NoInactiveText
This_Obj.c_InactiveText              = PCCA.PCMGR.c_InactiveText
This_Obj.c_NoInactiveLine            = PCCA.PCMGR.c_NoInactiveLine
This_Obj.c_InactiveLine              = PCCA.PCMGR.c_InactiveLine
This_Obj.c_NoInactiveCol             = PCCA.PCMGR.c_NoInactiveCol
This_Obj.c_InactiveCol               = PCCA.PCMGR.c_InactiveCol

This_Obj.c_CursorRowFocusRect        = PCCA.PCMGR.c_CursorRowFocusRect
This_Obj.c_NoCursorRowFocusRect      = PCCA.PCMGR.c_NoCursorRowFocusRect

This_Obj.c_CursorRowPointer          = PCCA.PCMGR.c_CursorRowPointer
This_Obj.c_NoCursorRowPointer        = PCCA.PCMGR.c_NoCursorRowPointer

This_Obj.c_CalcDWStyle               = PCCA.PCMGR.c_CalcDWStyle
This_Obj.c_FreeFormStyle             = PCCA.PCMGR.c_FreeFormStyle
This_Obj.c_TabularFormStyle          = PCCA.PCMGR.c_TabularFormStyle

This_Obj.c_AutoCalcHighlightSelected = PCCA.PCMGR.c_AutoCalcHighlightSelected
This_Obj.c_HighlightSelected         = PCCA.PCMGR.c_HighlightSelected
This_Obj.c_NoHighlightSelected       = PCCA.PCMGR.c_NoHighlightSelected

This_Obj.c_NoResizeDW                = PCCA.PCMGR.c_NoResizeDW
This_Obj.c_ResizeDW                  = PCCA.PCMGR.c_ResizeDW

This_Obj.c_NoAutoConfigMenus         = PCCA.PCMGR.c_NoAutoConfigMenus
This_Obj.c_AutoConfigMenus           = PCCA.PCMGR.c_AutoConfigMenus

This_Obj.c_ShowEmpty                 = PCCA.PCMGR.c_ShowEmpty
This_Obj.c_NoShowEmpty               = PCCA.PCMGR.c_NoShowEmpty

This_Obj.c_AutoFocus                 = PCCA.PCMGR.c_AutoFocus
This_Obj.c_NoAutoFocus               = PCCA.PCMGR.c_NoAutoFocus

This_Obj.c_CCErrorRed                = PCCA.PCMGR.c_CCErrorRed
This_Obj.c_CCErrorBlack              = PCCA.PCMGR.c_CCErrorBlack
This_Obj.c_CCErrorWhite              = PCCA.PCMGR.c_CCErrorWhite
This_Obj.c_CCErrorGray               = PCCA.PCMGR.c_CCErrorGray
This_Obj.c_CCErrorLightGray          = PCCA.PCMGR.c_CCErrorLightGray
This_Obj.c_CCErrorDarkGray           = PCCA.PCMGR.c_CCErrorDarkGray
This_Obj.c_CCErrorDarkRed            = PCCA.PCMGR.c_CCErrorDarkRed
This_Obj.c_CCErrorGreen              = PCCA.PCMGR.c_CCErrorGreen
This_Obj.c_CCErrorDarkGreen          = PCCA.PCMGR.c_CCErrorDarkGreen
This_Obj.c_CCErrorBlue               = PCCA.PCMGR.c_CCErrorBlue
This_Obj.c_CCErrorDarkBlue           = PCCA.PCMGR.c_CCErrorDarkBlue
This_Obj.c_CCErrorMagenta            = PCCA.PCMGR.c_CCErrorMagenta
This_Obj.c_CCErrorDarkMagenta        = PCCA.PCMGR.c_CCErrorDarkMagenta
This_Obj.c_CCErrorCyan               = PCCA.PCMGR.c_CCErrorCyan
This_Obj.c_CCErrorDarkCyan           = PCCA.PCMGR.c_CCErrorDarkCyan
This_Obj.c_CCErrorYellow             = PCCA.PCMGR.c_CCErrorYellow
This_Obj.c_CCErrorBrown              = PCCA.PCMGR.c_CCErrorBrown

end subroutine

public subroutine fu_buildactivestrings (uo_dw_main this_obj, long lnumobjects, string lpszobjectnames[], long lobjecttypes[], long lobjectcolors[], boolean binactivecol, boolean binactivetext, boolean binactiveline, string lpszinactivedwcolor, string lpszviewmodecolor, long lmaxlength, ref string lpszactivenonemptycolors, ref string lpszactiveemptycolors, ref string lpszinactivenonemptycolors, ref string lpszinactiveemptycolors);//******************************************************************
// Code is in the descendent.
//******************************************************************
end subroutine

public subroutine fu_buildtabstrings (uo_dw_main this_obj, long lnumcolumns, long ltaborder[], boolean icolvisible[], ref long lfirsttabcolumn, ref long lfirstviscolumn, long lmaxlength, ref string lpszenabletabs, ref string lpszdisabletabs, ref string lpszquerytabs);//******************************************************************
// Code is in the descendent.
//******************************************************************
end subroutine

public subroutine fu_psbuildactivestrings (uo_dw_main this_obj, long lnumobjects, string lpszobjectnames[], long lobjecttypes[], long lobjectcolors[], long lcolbcolors[], boolean binactivecol, boolean binactivetext, boolean binactiveline, string lpszinactivedwcolor, string lpszviewmodecolor, long lmaxlength, ref string lpszactivenonemptycolors, ref string lpszactiveemptycolors, ref string lpszinactivenonemptycolors, ref string lpszinactiveemptycolors);//******************************************************************
//  PC Module     : n_DLL_Main
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN  l_SetTab,      l_DoView, l_Doit
INTEGER  l_ObjNbr,      l_ColCnt
STRING   l_Tab,         l_Attrib
STRING   l_ActiveEmpty, l_InactiveEmpty

lpszActiveNonEmptyColors   = ""
lpszActiveEmptyColors      = ""
lpszInactiveNonEmptyColors = ""
lpszInactiveEmptyColors    = ""

IF Len(lpszInactiveDWColor) = 0 THEN
   RETURN
END IF

l_DoView = (Len(lpszViewModeColor) > 0)

l_SetTab = FALSE
l_Tab    = ""
l_ColCnt = 0

FOR l_ObjNbr = 1 TO lNumObjects

   IF lObjectColors[l_ObjNbr] <> -1 THEN

      CHOOSE CASE lObjectTypes[l_ObjNbr]
         CASE This_Obj.c_ObjTypeColumn
            l_ColCnt = l_ColCnt + 1

            l_Doit   = bInactiveCol
            l_Attrib = ".Color="

            IF l_DoView THEN
               l_ActiveEmpty = lpszViewModeColor
            ELSE
               l_ActiveEmpty = String(lColBColors[l_ColCnt])
            END IF
            l_InactiveEmpty = l_ActiveEmpty

         CASE This_Obj.c_ObjTypeText
            l_Doit          = bInactiveText
            l_Attrib        = ".Color="
            l_ActiveEmpty   = String(lObjectColors[l_ObjNbr])
            l_InactiveEmpty = lpszInactiveDWColor

         CASE This_Obj.c_ObjTypeLine
            l_Doit          = bInactiveLine
            l_Attrib        = ".Pen.Color="
            l_ActiveEmpty   = String(lObjectColors[l_ObjNbr])
            l_InactiveEmpty = lpszInactiveDWColor

         CASE ELSE
            l_Doit = FALSE
      END CHOOSE

      IF l_Doit THEN
         lpszActiveNonEmptyColors      = &
            lpszActiveNonEmptyColors   + &
            l_Tab                      + &
            lpszObjectNames[l_ObjNbr]  + &
            l_Attrib                   + &
            String(lObjectColors[l_ObjNbr])

         lpszActiveEmptyColors         = &
            lpszActiveEmptyColors      + &
            l_Tab                      + &
            lpszObjectNames[l_ObjNbr]  + &
            l_Attrib                   + &
            l_ActiveEmpty

         lpszInactiveNonEmptyColors    = &
            lpszInactiveNonEmptyColors + &
            l_Tab                      + &
            lpszObjectNames[l_ObjNbr]  + &
            l_Attrib                   + &
            lpszInactiveDWColor

         lpszInactiveEmptyColors       = &
            lpszInactiveEmptyColors    + &
            l_Tab                      + &
            lpszObjectNames[l_ObjNbr]  + &
            l_Attrib                   + &
            l_InactiveEmpty
      END IF

      IF NOT l_SetTab THEN
         l_SetTab = TRUE
         l_Tab    = "~t"
      END IF
   END IF
NEXT

end subroutine

public subroutine fu_psbuildtabstrings (uo_dw_main this_obj, long lnumcolumns, long ltaborder[], boolean icolvisible[], ref long lfirsttabcolumn, ref long lfirstviscolumn, long lmaxlength, ref string lpszenabletabs, ref string lpszdisabletabs, ref string lpszquerytabs);//******************************************************************
//  PC Module     : n_DLL_Main
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN  l_SetTab, l_displayonly
INTEGER  l_ColNbr
LONG     l_LowTab, l_num_chars
STRING   l_ColPrefix, l_ColNum, l_ColName, l_ColType, ls_result

l_displayonly   = FALSE
lFirstTabColumn = 0
lFirstVisColumn = 0
lpszEnableTabs  = ""
lpszDisableTabs = ""
lpszQueryTabs   = ""

l_LowTab        = 0

l_SetTab        = FALSE
l_ColPrefix     = "#"

FOR l_ColNbr = 1 TO lNumColumns
   IF iColVisible[l_ColNbr] THEN

      IF lFirstVisColumn = 0 THEN
         lFirstVisColumn = l_ColNbr
      END IF

      IF lTabOrder[l_ColNbr] >= 0 THEN

         IF lTabOrder[l_ColNbr] > 0 THEN
            IF lTabOrder [l_ColNbr] < l_LowTab OR l_LowTab = 0 THEN
               lFirstTabColumn = l_ColNbr
               l_LowTab        = lTabOrder[l_ColNbr]
            END IF
         END IF
         //---------------------------------------------------------

         l_ColName       = String(l_ColNbr)
         l_ColNum        = l_ColPrefix + l_ColName

			//---------------------------------------------------------
			// Leggo il tipo di dato; se la stringa è maggiore di 100 metto le scrollbar
			l_displayonly = false
			ls_result = upper(this_obj.describe("#" + l_colname + ".Edit.DisplayOnly"))
			if ls_result <> "?" then		// è di tipo edit

				l_ColType       = this_obj.Describe("#" + l_colname + ".ColType")
				if upper(left(l_ColType,4)) = "CHAR" then
					l_num_chars     = long( mid(l_ColType,6,len(l_ColType) - 6 ) )
					if l_num_chars > 100 then l_displayonly = true
				end if
			end if
			
			if l_displayonly then
				lpszEnableTabs  = lpszEnableTabs  + &
										l_ColNum        + &
										".edit.DisplayOnly=No~t"
				lpszEnableTabs  = lpszEnableTabs  + &
										l_ColNum        + &
										".edit.AutoVScroll=Yes~t"
				lpszEnableTabs  = lpszEnableTabs  + &
										l_ColNum        + &
										".edit.AutoHScroll=No~t"
				lpszEnableTabs  = lpszEnableTabs  + &
										l_ColNum        + &
										".edit.HScrollBar=No~t"
				lpszEnableTabs  = lpszEnableTabs  + &
										l_ColNum        + &
										".edit.VScrollBar=Yes"
										
				lpszDisableTabs = lpszDisableTabs + &
										l_ColNum        + &
										".edit.DisplayOnly=Yes~t"
				lpszDisableTabs = lpszDisableTabs + &
										l_ColNum        + &
										".edit.AutoHScroll=No~t"
				lpszDisableTabs = lpszDisableTabs + &
										l_ColNum        + &
										".edit.AutoVScroll=Yes~t"
				lpszDisableTabs = lpszDisableTabs + &
										l_ColNum        + &
										".edit.VScrollBar=Yes~t"
				lpszDisableTabs = lpszDisableTabs + &
										l_ColNum        + &
										".edit.HScrollBar=No"
			else
				//---------------------------------------------------------
	
				lpszEnableTabs  = lpszEnableTabs  + &
										l_ColNum        + &
										".TabSequence=" + &
										String(lTabOrder[l_ColNbr])
	
				//---------------------------------------------------------
	
				lpszDisableTabs = lpszDisableTabs + &
										l_ColNum        + &
										".TabSequence=0"
			end if
         //---------------------------------------------------------

         lpszQueryTabs   = lpszQueryTabs   + &
                           l_ColNum        + &
                           ".TabSequence=" + l_ColName

         //---------------------------------------------------------

         IF NOT l_SetTab THEN
            l_SetTab    = TRUE
            l_ColPrefix = "~t#"
         END IF
      END IF
   END IF
NEXT

end subroutine

on n_dll_main.create
call super::create
TriggerEvent( this, "constructor" )
end on

on n_dll_main.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

