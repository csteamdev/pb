﻿$PBExportHeader$n_dll_other.sru
$PBExportComments$Provides PC DLL interface emulation in PowerScript for all other platforms.
forward
global type n_dll_other from n_dll_main
end type
end forward

global type n_dll_other from n_dll_main
end type
global n_dll_other n_dll_other

type prototypes

end prototypes

type variables

end variables

forward prototypes
public subroutine fu_buildgettabstring (uo_dw_main this_obj, long lnumcolumns, long lmaxlength, ref string lpsztabstring)
public subroutine fu_parsetabstring (uo_dw_main this_obj, long lnumcolumns, string lpszdwdescribe, ref long ltaborder[], ref boolean icolvisible[])
public subroutine fu_buildtabstrings (uo_dw_main this_obj, long lnumcolumns, long ltaborder[], boolean icolvisible[], ref long lfirsttabcolumn, ref long lfirstviscolumn, long lmaxlength, ref string lpszenabletabs, ref string lpszdisabletabs, ref string lpszquerytabs)
public subroutine fu_buildgetattribstring (uo_dw_main this_obj, long lnumcolumns, boolean icolvisible[], long lmaxlength, ref string lpszattribstring)
public subroutine fu_parseattribstring (uo_dw_main this_obj, long lnumcolumns, boolean icolvisible[], string lpszdwdescribe, ref long lcolborders[], ref long lcolcolors[], ref long lcolbcolors[])
public subroutine fu_buildattribstrings (uo_dw_main this_obj, long lnumcolumns, long ltaborder[], boolean icolvisible[], long lcolborders[], long lcolcolors[], long lcolbcolors[], string lpszviewmodeborder, string lpszviewmodecolor, long lmaxlength, ref string lpszmodifyborders, ref string lpszviewborders, ref string lpszmodifycolors, ref string lpszviewcolors, ref string lpsznonemptytextcolors, ref string lpszemptytextcolors)
public subroutine fu_buildgetobjtypestring (uo_dw_main this_obj, string lpszobjnames, long lmaxlength, ref string lpszobjtypestring, ref long lnumobjects)
public subroutine fu_parseobjtypestring (uo_dw_main this_obj, long lnumobjects, string lpszobjnames, string lpszdwdescribe, long lmaxlength, ref string lpszobjcolorstring, ref string lpszobjectnames[], ref long lobjecttypes[])
public subroutine fu_parseobjcolorstring (uo_dw_main this_obj, long lnumobjects, long lobjecttypes[], string lpszdwdescribe, ref long lobjectcolors[])
public subroutine fu_buildactivestrings (uo_dw_main this_obj, long lnumobjects, string lpszobjectnames[], long lobjecttypes[], long lobjectcolors[], boolean binactivecol, boolean binactivetext, boolean binactiveline, string lpszinactivedwcolor, string lpszviewmodecolor, long lmaxlength, ref string lpszactivenonemptycolors, ref string lpszactiveemptycolors, ref string lpszinactivenonemptycolors, ref string lpszinactiveemptycolors)
end prototypes

public subroutine fu_buildgettabstring (uo_dw_main this_obj, long lnumcolumns, long lmaxlength, ref string lpsztabstring);//******************************************************************
//  PC Module     : n_DLL_Other
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

fu_PSBuildGetTabString(This_Obj, &
                       lNumColumns, &
                       lMaxLength, &
                       lpszTabString)

RETURN
end subroutine

public subroutine fu_parsetabstring (uo_dw_main this_obj, long lnumcolumns, string lpszdwdescribe, ref long ltaborder[], ref boolean icolvisible[]);//******************************************************************
//  PC Module     : n_DLL_Other
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

fu_PSParseTabString(This_Obj,        &
                    lNumColumns,     &
                    lpszdwDescribe,  &
                    lTabOrder[],     &
                    iColVisible[])

RETURN
end subroutine

public subroutine fu_buildtabstrings (uo_dw_main this_obj, long lnumcolumns, long ltaborder[], boolean icolvisible[], ref long lfirsttabcolumn, ref long lfirstviscolumn, long lmaxlength, ref string lpszenabletabs, ref string lpszdisabletabs, ref string lpszquerytabs);//******************************************************************
//  PC Module     : n_DLL_Other
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

fu_PSBuildTabStrings(This_Obj,         &
                     lNumColumns,      &
                     lTabOrder[],      &
                     iColVisible[],    &
                     lFirstTabColumn,  &
                     lFirstVisColumn,  &
                     lMaxLength,       &
                     lpszEnableTabs,   &
                     lpszDisableTabs,  &
                     lpszQueryTabs)

RETURN
end subroutine

public subroutine fu_buildgetattribstring (uo_dw_main this_obj, long lnumcolumns, boolean icolvisible[], long lmaxlength, ref string lpszattribstring);//******************************************************************
//  PC Module     : n_DLL_Other
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

fu_PSBuildGetAttribString(This_Obj,       &
                          lNumColumns,    &
                          iColVisible[],  &
                          lMaxLength,     &
                          lpszAttribString)

RETURN
end subroutine

public subroutine fu_parseattribstring (uo_dw_main this_obj, long lnumcolumns, boolean icolvisible[], string lpszdwdescribe, ref long lcolborders[], ref long lcolcolors[], ref long lcolbcolors[]);//******************************************************************
//  PC Module     : n_DLL_Other
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

fu_PSParseAttribString(This_Obj,        &
                       lNumColumns,     &
                       iColVisible[],   &
                       lpszdwDescribe,  &
                       lColBorders[],   &
                       lColColors[],    &
                       lColBColors[])

RETURN
end subroutine

public subroutine fu_buildattribstrings (uo_dw_main this_obj, long lnumcolumns, long ltaborder[], boolean icolvisible[], long lcolborders[], long lcolcolors[], long lcolbcolors[], string lpszviewmodeborder, string lpszviewmodecolor, long lmaxlength, ref string lpszmodifyborders, ref string lpszviewborders, ref string lpszmodifycolors, ref string lpszviewcolors, ref string lpsznonemptytextcolors, ref string lpszemptytextcolors);//******************************************************************
//  PC Module     : n_DLL_Other
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

fu_PSBuildAttribStrings(This_Obj,                &
                        lNumColumns,             &
                        lTabOrder[],             &
                        iColVisible[],           &
                        lColBorders[],           &
                        lColColors[],            &
                        lColBColors[],           &
                        lpszViewModeBorder,      &
                        lpszViewModeColor,       &
                        lMaxLength,              &
                        lpszModifyBorders,       &
                        lpszViewBorders,         &
                        lpszModifyColors,        &
                        lpszViewColors,          &
                        lpszNonEmptyTextColors,  &
                        lpszEmptyTextColors)

RETURN
end subroutine

public subroutine fu_buildgetobjtypestring (uo_dw_main this_obj, string lpszobjnames, long lmaxlength, ref string lpszobjtypestring, ref long lnumobjects);//******************************************************************
//  PC Module     : n_DLL_Other
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

fu_PSBuildGetObjTypeString(This_Obj,           &
                           lpszObjNames,       &
                           lMaxLength,         &
                           lpszObjTypeString,  &
                           lNumObjects)

RETURN
end subroutine

public subroutine fu_parseobjtypestring (uo_dw_main this_obj, long lnumobjects, string lpszobjnames, string lpszdwdescribe, long lmaxlength, ref string lpszobjcolorstring, ref string lpszobjectnames[], ref long lobjecttypes[]);//******************************************************************
//  PC Module     : n_DLL_Other
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

fu_PSParseObjTypeString(This_Obj,            &
                        lNumObjects,         &
                        lpszObjNames,        &
                        lpszdwDescribe,      &
                        lMaxLength,          &
                        lpszObjColorString,  &
                        lpszObjectNames[],   &
                        lObjectTypes[])

RETURN
end subroutine

public subroutine fu_parseobjcolorstring (uo_dw_main this_obj, long lnumobjects, long lobjecttypes[], string lpszdwdescribe, ref long lobjectcolors[]);//******************************************************************
//  PC Module     : n_DLL_Other
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

fu_PSParseObjColorString(This_Obj,        &
                         lNumObjects,     &
                         lObjectTypes[],  &
                         lpszdwDescribe,  &
                         lObjectColors[])

RETURN
end subroutine

public subroutine fu_buildactivestrings (uo_dw_main this_obj, long lnumobjects, string lpszobjectnames[], long lobjecttypes[], long lobjectcolors[], boolean binactivecol, boolean binactivetext, boolean binactiveline, string lpszinactivedwcolor, string lpszviewmodecolor, long lmaxlength, ref string lpszactivenonemptycolors, ref string lpszactiveemptycolors, ref string lpszinactivenonemptycolors, ref string lpszinactiveemptycolors);//******************************************************************
//  PC Module     : n_DLL_Other
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_ColNbr,   l_Idx, l_Jdx
LONG     l_ColBColors[]
STRING   l_Describe, l_Result

l_Jdx = 0
FOR l_Idx = 1 TO lNumObjects
   IF lObjectTypes[l_Idx] = This_Obj.c_ObjTypeColumn THEN
      l_Describe = lpszObjectNames[l_Idx] + ".ID"
      l_Result   = This_Obj.Describe(l_Describe)
      l_ColNbr   = Integer(l_Result)

      l_Jdx      = l_Jdx + 1
      l_ColBColors[l_Jdx] = This_Obj.i_ColBColors[l_ColNbr]
   END IF
NEXT

fu_PSBuildActiveStrings(This_Obj,                    &
                        lNumObjects,                 &
                        lpszObjectNames[],           &
                        lObjectTypes[],              &
                        lObjectColors[],             &
                        l_ColBColors[],              &
                        bInactiveCol,                &
                        bInactiveText,               &
                        bInactiveLine,               &
                        lpszInactiveDWColor,         &
                        lpszViewModeColor,           &
                        lMaxLength,                  &
                        lpszActiveNonEmptyColors,    &
                        lpszActiveEmptyColors,       &
                        lpszInactiveNonEmptyColors,  &
                        lpszInactiveEmptyColors)

RETURN
end subroutine

on n_dll_other.create
TriggerEvent( this, "constructor" )
end on

on n_dll_other.destroy
TriggerEvent( this, "destructor" )
end on

