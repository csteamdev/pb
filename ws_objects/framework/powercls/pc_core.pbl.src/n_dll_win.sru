﻿$PBExportHeader$n_dll_win.sru
$PBExportComments$Provides PC DLL interface for WIN-16.
forward
global type n_dll_win from n_dll_main
end type
end forward

global type n_dll_win from n_dll_main
end type
global n_dll_win n_dll_win

type prototypes
SUBROUTINE PCDLL_BuildGetTabString		&
      (LONG		lNumColumns,		&
       LONG		lMaxLength,		&
       ref STRING	lpszTabString)			&
LIBRARY "PC041.DLL" alias for "PCDLL_BuildGetTabString;Ansi"

SUBROUTINE PCDLL_ParseTabString			&
      (LONG		lNumColumns,		&
       STRING		lpszdwDescribe,		&
       ref LONG		lTabOrder[],		&
       ref BOOLEAN		iColVisible[])		&
LIBRARY "PC041.DLL" alias for "PCDLL_ParseTabString;Ansi"

SUBROUTINE PCDLL_BuildTabStrings			&
      (LONG		lNumColumns,		&
       LONG		lTabOrder[],		&
       BOOLEAN		iColVisible[],		&
       ref LONG		lFirstTabColumn,		&
       ref LONG		lFirstVisColumn,		&
       LONG		iMaxLength,		&
       ref STRING		lpszEnableTabs,		&
       ref STRING		lpszDisableTabs,		&
       ref STRING		lpszQueryTabs)		&
LIBRARY "PC041.DLL" alias for "PCDLL_BuildTabStrings;Ansi"

SUBROUTINE PCDLL_BuildGetAttribString		&
      (LONG		lNumColumns,		&
       BOOLEAN		iColVisible[],		&
       LONG		iMaxLength,		&
       ref STRING		lpszAttribString)		&
LIBRARY "PC041.DLL" alias for "PCDLL_BuildGetAttribString;Ansi"

SUBROUTINE PCDLL_ParseAttribString		&
      (LONG		lNumColumns,		&
       BOOLEAN		iColVisible[],		&
       STRING		lpszdwDescribe,		&
       ref LONG		lColBorders[],		&
       ref LONG		lColColors[],		&
       ref LONG		lColBColors[])		&
LIBRARY "PC041.DLL" alias for "PCDLL_ParseAttribString;Ansi"

SUBROUTINE PCDLL_BuildAttribStrings		&
      (LONG		lNumColumns,		&
       LONG		lTabOrder[],		&
       BOOLEAN		iColVisible[],		&
       LONG		lColBorders[],		&
       LONG		lColColors[],		&
       LONG		lColBColors[],		&
       STRING		lpszViewModeBorder,	&
       STRING		lpszViewModeColor,	&
       LONG		iMaxLength,		&
       ref STRING		lpszModifyBorders,		&
       ref STRING		lpszViewBorders,		&
       ref STRING		lpszModifyColors,		&
       ref STRING		lpszViewColors,		&
       ref STRING		lpszNonEmptyTextColors,	&
       ref STRING		lpszEmptyTextColors)	&
LIBRARY "PC041.DLL" alias for "PCDLL_BuildAttribStrings;Ansi"

SUBROUTINE PCDLL_BuildGetObjTypeString		&
      (STRING		lpszObjNames,		&
       LONG		iMaxLength,		&
       ref STRING		lpszObjTypeString,		&
       ref LONG		lNumObjects)		&
LIBRARY "PC041.DLL" alias for "PCDLL_BuildGetObjTypeString;Ansi"

SUBROUTINE PCDLL_ParseObjTypeString		&
      (LONG		lNumObjects,		&
       STRING		lpszObjNames,		&
       STRING		lpszdwDescribe,		&
       LONG		iMaxLength,		&
       ref STRING		lpszObjColorString,		&
       ref STRING		lpszObjectNames[],		&
       ref LONG		lObjectTypes[])		&
LIBRARY "PC041.DLL" alias for "PCDLL_ParseObjTypeString;Ansi"

SUBROUTINE PCDLL_ParseObjColorString		&
      (LONG		lNumObjects,		&
       LONG		lObjectTypes[],		&
       STRING		lpszdwDescribe,		&
       ref LONG		lObjectColors[])		&
LIBRARY "PC041.DLL" alias for "PCDLL_ParseObjColorString;Ansi"

SUBROUTINE PCDLL_BuildActiveStrings		&
      (LONG		lNumObjects,		&
       STRING		lpszObjectNames[],		&
       LONG		lObjectTypes[],		&
       LONG		lObjectColors[],		&
       LONG		lColBColors[],		&
       BOOLEAN		bInactiveCol,		&
       BOOLEAN		bInactiveText,		&
       BOOLEAN		bInactiveLine,		&
       STRING		lpszInactiveDWColor,	&
       STRING		lpszViewModeColor,	&
       LONG		iMaxLength,		&
       ref STRING		lpszActiveNonEmptyColors,	&
       ref STRING		lpszActiveEmptyColors,	&
       ref STRING		lpszInactiveNonEmptyColors,	&
       ref STRING		lpszInactiveEmptyColors)	&
LIBRARY "PC041.DLL" alias for "PCDLL_BuildActiveStrings;Ansi"
end prototypes

type variables

end variables

forward prototypes
public subroutine fu_buildgettabstring (uo_dw_main this_obj, long lnumcolumns, long lmaxlength, ref string lpsztabstring)
public subroutine fu_parsetabstring (uo_dw_main this_obj, long lnumcolumns, string lpszdwdescribe, ref long ltaborder[], ref boolean icolvisible[])
public subroutine fu_buildtabstrings (uo_dw_main this_obj, long lnumcolumns, long ltaborder[], boolean icolvisible[], ref long lfirsttabcolumn, ref long lfirstviscolumn, long lmaxlength, ref string lpszenabletabs, ref string lpszdisabletabs, ref string lpszquerytabs)
public subroutine fu_buildgetattribstring (uo_dw_main this_obj, long lnumcolumns, boolean icolvisible[], long lmaxlength, ref string lpszattribstring)
public subroutine fu_parseattribstring (uo_dw_main this_obj, long lnumcolumns, boolean icolvisible[], string lpszdwdescribe, ref long lcolborders[], ref long lcolcolors[], ref long lcolbcolors[])
public subroutine fu_buildattribstrings (uo_dw_main this_obj, long lnumcolumns, long ltaborder[], boolean icolvisible[], long lcolborders[], long lcolcolors[], long lcolbcolors[], string lpszviewmodeborder, string lpszviewmodecolor, long lmaxlength, ref string lpszmodifyborders, ref string lpszviewborders, ref string lpszmodifycolors, ref string lpszviewcolors, ref string lpsznonemptytextcolors, ref string lpszemptytextcolors)
public subroutine fu_buildgetobjtypestring (uo_dw_main this_obj, string lpszobjnames, long lmaxlength, ref string lpszobjtypestring, ref long lnumobjects)
public subroutine fu_parseobjtypestring (uo_dw_main this_obj, long lnumobjects, string lpszobjnames, string lpszdwdescribe, long lmaxlength, ref string lpszobjcolorstring, ref string lpszobjectnames[], ref long lobjecttypes[])
public subroutine fu_parseobjcolorstring (uo_dw_main this_obj, long lnumobjects, long lobjecttypes[], string lpszdwdescribe, ref long lobjectcolors[])
public subroutine fu_buildactivestrings (uo_dw_main this_obj, long lnumobjects, string lpszobjectnames[], long lobjecttypes[], long lobjectcolors[], boolean binactivecol, boolean binactivetext, boolean binactiveline, string lpszinactivedwcolor, string lpszviewmodecolor, long lmaxlength, ref string lpszactivenonemptycolors, ref string lpszactiveemptycolors, ref string lpszinactivenonemptycolors, ref string lpszinactiveemptycolors)
end prototypes

public subroutine fu_buildgettabstring (uo_dw_main this_obj, long lnumcolumns, long lmaxlength, ref string lpsztabstring);//******************************************************************
//  PC Module     : n_DLL_Win
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

IF PCCA.EXT.i_UseDLL THEN
   PCDLL_BuildGetTabString(lNumColumns, lMaxLength, lpszTabString)
ELSE
   fu_PSBuildGetTabString(This_Obj, &
                          lNumColumns, lMaxLength, lpszTabString)
END IF

RETURN
end subroutine

public subroutine fu_parsetabstring (uo_dw_main this_obj, long lnumcolumns, string lpszdwdescribe, ref long ltaborder[], ref boolean icolvisible[]);//******************************************************************
//  PC Module     : n_DLL_Win
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

IF PCCA.EXT.i_UseDLL THEN
   PCDLL_ParseTabString(lNumColumns,    &
                        lpszdwDescribe, &
                        lTabOrder[],    &
                        iColVisible[])
ELSE
   fu_PSParseTabString(This_Obj,        &
                       lNumColumns,     &
                       lpszdwDescribe,  &
                       lTabOrder[],     &
                       iColVisible[])
END IF

RETURN
end subroutine

public subroutine fu_buildtabstrings (uo_dw_main this_obj, long lnumcolumns, long ltaborder[], boolean icolvisible[], ref long lfirsttabcolumn, ref long lfirstviscolumn, long lmaxlength, ref string lpszenabletabs, ref string lpszdisabletabs, ref string lpszquerytabs);//******************************************************************
//  PC Module     : n_DLL_Win
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

IF PCCA.EXT.i_UseDLL THEN
   PCDLL_BuildTabStrings(lNumColumns,     &
                         lTabOrder[],     &
                         iColVisible[],   &
                         lFirstTabColumn, &
                         lFirstVisColumn, &
                         lMaxLength,      &
                         lpszEnableTabs,  &
                         lpszDisableTabs, &
                         lpszQueryTabs)
ELSE
   fu_PSBuildTabStrings(This_Obj,         &
                        lNumColumns,      &
                        lTabOrder[],      &
                        iColVisible[],    &
                        lFirstTabColumn,  &
                        lFirstVisColumn,  &
                        lMaxLength,       &
                        lpszEnableTabs,   &
                        lpszDisableTabs,  &
                        lpszQueryTabs)
END IF

RETURN
end subroutine

public subroutine fu_buildgetattribstring (uo_dw_main this_obj, long lnumcolumns, boolean icolvisible[], long lmaxlength, ref string lpszattribstring);//******************************************************************
//  PC Module     : n_DLL_Win
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

IF PCCA.EXT.i_UseDLL THEN
   PCDLL_BuildGetAttribString(lNumColumns,   &
                              iColVisible[], &
                              lMaxLength,    &
                              lpszAttribString)
ELSE
   fu_PSBuildGetAttribString(This_Obj,       &
                             lNumColumns,    &
                             iColVisible[],  &
                             lMaxLength,     &
                             lpszAttribString)
END IF

RETURN
end subroutine

public subroutine fu_parseattribstring (uo_dw_main this_obj, long lnumcolumns, boolean icolvisible[], string lpszdwdescribe, ref long lcolborders[], ref long lcolcolors[], ref long lcolbcolors[]);//******************************************************************
//  PC Module     : n_DLL_Win
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

IF PCCA.EXT.i_UseDLL THEN
   PCDLL_ParseAttribString(lNumColumns,    &
                           iColVisible[],  &
                           lpszdwDescribe, &
                           lColBorders[],  &
                           lColColors[],   &
                           lColBColors[])
ELSE
   fu_PSParseAttribString(This_Obj,        &
                          lNumColumns,     &
                          iColVisible[],   &
                          lpszdwDescribe,  &
                          lColBorders[],   &
                          lColColors[],    &
                          lColBColors[])
END IF

RETURN
end subroutine

public subroutine fu_buildattribstrings (uo_dw_main this_obj, long lnumcolumns, long ltaborder[], boolean icolvisible[], long lcolborders[], long lcolcolors[], long lcolbcolors[], string lpszviewmodeborder, string lpszviewmodecolor, long lmaxlength, ref string lpszmodifyborders, ref string lpszviewborders, ref string lpszmodifycolors, ref string lpszviewcolors, ref string lpsznonemptytextcolors, ref string lpszemptytextcolors);//******************************************************************
//  PC Module     : n_DLL_Win
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

IF PCCA.EXT.i_UseDLL THEN
   PCDLL_BuildAttribStrings(lNumColumns,            &
                            lTabOrder[],            &
                            iColVisible[],          &
                            lColBorders[],          &
                            lColColors[],           &
                            lColBColors[],          &
                            lpszViewModeBorder,     &
                            lpszViewModeColor,      &
                            lMaxLength,             &
                            lpszModifyBorders,      &
                            lpszViewBorders,        &
                            lpszModifyColors,       &
                            lpszViewColors,         &
                            lpszNonEmptyTextColors, &
                            lpszEmptyTextColors)
ELSE
   fu_PSBuildAttribStrings(This_Obj,                &
                           lNumColumns,             &
                           lTabOrder[],             &
                           iColVisible[],           &
                           lColBorders[],           &
                           lColColors[],            &
                           lColBColors[],           &
                           lpszViewModeBorder,      &
                           lpszViewModeColor,       &
                           lMaxLength,              &
                           lpszModifyBorders,       &
                           lpszViewBorders,         &
                           lpszModifyColors,        &
                           lpszViewColors,          &
                           lpszNonEmptyTextColors,  &
                           lpszEmptyTextColors)
END IF

RETURN
end subroutine

public subroutine fu_buildgetobjtypestring (uo_dw_main this_obj, string lpszobjnames, long lmaxlength, ref string lpszobjtypestring, ref long lnumobjects);//******************************************************************
//  PC Module     : n_DLL_Win
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

IF PCCA.EXT.i_UseDLL THEN
   PCDLL_BuildGetObjTypeString(lpszObjNames,      &
                               lMaxLength,        &
                               lpszObjTypeString, &
                               lNumObjects)
ELSE
   fu_PSBuildGetObjTypeString(This_Obj,           &
                              lpszObjNames,       &
                              lMaxLength,         &
                              lpszObjTypeString,  &
                              lNumObjects)
END IF

RETURN
end subroutine

public subroutine fu_parseobjtypestring (uo_dw_main this_obj, long lnumobjects, string lpszobjnames, string lpszdwdescribe, long lmaxlength, ref string lpszobjcolorstring, ref string lpszobjectnames[], ref long lobjecttypes[]);//******************************************************************
//  PC Module     : n_DLL_Win
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

IF PCCA.EXT.i_UseDLL THEN
   PCDLL_ParseObjTypeString(lNumObjects,        &
                            lpszObjNames,       &
                            lpszdwDescribe,     &
                            lMaxLength,         &
                            lpszObjColorString, &
                            lpszObjectNames[],  &
                            lObjectTypes[])
ELSE
   fu_PSParseObjTypeString(This_Obj,            &
                           lNumObjects,         &
                           lpszObjNames,        &
                           lpszdwDescribe,      &
                           lMaxLength,          &
                           lpszObjColorString,  &
                           lpszObjectNames[],   &
                           lObjectTypes[])
END IF

RETURN
end subroutine

public subroutine fu_parseobjcolorstring (uo_dw_main this_obj, long lnumobjects, long lobjecttypes[], string lpszdwdescribe, ref long lobjectcolors[]);//******************************************************************
//  PC Module     : n_DLL_Win
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

IF PCCA.EXT.i_UseDLL THEN
   PCDLL_ParseObjColorString(lNumObjects,    &
                             lObjectTypes[], &
                             lpszdwDescribe, &
                             lObjectColors[])
ELSE
   fu_PSParseObjColorString(This_Obj,        &
                            lNumObjects,     &
                            lObjectTypes[],  &
                            lpszdwDescribe,  &
                            lObjectColors[])
END IF

RETURN
end subroutine

public subroutine fu_buildactivestrings (uo_dw_main this_obj, long lnumobjects, string lpszobjectnames[], long lobjecttypes[], long lobjectcolors[], boolean binactivecol, boolean binactivetext, boolean binactiveline, string lpszinactivedwcolor, string lpszviewmodecolor, long lmaxlength, ref string lpszactivenonemptycolors, ref string lpszactiveemptycolors, ref string lpszinactivenonemptycolors, ref string lpszinactiveemptycolors);//******************************************************************
//  PC Module     : n_DLL_Win
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_ColNbr,   l_Idx, l_Jdx
LONG     l_ColBColors[]
STRING   l_Describe, l_Result

l_Jdx = 0
FOR l_Idx = 1 TO lNumObjects
   IF lObjectTypes[l_Idx] = This_Obj.c_ObjTypeColumn THEN
      l_Describe = lpszObjectNames[l_Idx] + ".ID"
      l_Result   = This_Obj.Describe(l_Describe)
      l_ColNbr   = Integer(l_Result)

      l_Jdx      = l_Jdx + 1
      l_ColBColors[l_Jdx] = This_Obj.i_ColBColors[l_ColNbr]
   END IF
NEXT

IF PCCA.EXT.i_UseDLL THEN
   PCDLL_BuildActiveStrings(lNumObjects,                &
                            lpszObjectNames[],          &
                            lObjectTypes[],             &
                            lObjectColors[],            &
                            l_ColBColors[],             &
                            bInactiveCol,               &
                            bInactiveText,              &
                            bInactiveLine,              &
                            lpszInactiveDWColor,        &
                            lpszViewModeColor,          &
                            lMaxLength,                 &
                            lpszActiveNonEmptyColors,   &
                            lpszActiveEmptyColors,      &
                            lpszInactiveNonEmptyColors, &
                            lpszInactiveEmptyColors)
ELSE
   fu_PSBuildActiveStrings(This_Obj,                    &
                           lNumObjects,                 &
                           lpszObjectNames[],           &
                           lObjectTypes[],              &
                           lObjectColors[],             &
                           l_ColBColors[],              &
                           bInactiveCol,                &
                           bInactiveText,               &
                           bInactiveLine,               &
                           lpszInactiveDWColor,         &
                           lpszViewModeColor,           &
                           lMaxLength,                  &
                           lpszActiveNonEmptyColors,    &
                           lpszActiveEmptyColors,       &
                           lpszInactiveNonEmptyColors,  &
                           lpszInactiveEmptyColors)
END IF

RETURN
end subroutine

on n_dll_win.create
TriggerEvent( this, "constructor" )
end on

on n_dll_win.destroy
TriggerEvent( this, "destructor" )
end on

