﻿$PBExportHeader$n_pc_mb_main.sru
$PBExportComments$PowerClass Ancestor MessageBox object
forward
global type n_pc_mb_main from n_mb_main
end type
end forward

global type n_pc_mb_main from n_mb_main
end type
global n_pc_mb_main n_pc_mb_main

type variables
//------------------------------------------------------------------------------
//  The following parameters are available for formatting
//  application error messages:
//------------------------------------------------------------------------------

UNSIGNEDLONG	c_MBC_ApplicationError = 2

UNSIGNEDLONG	c_MBI_NoDLL = 2
UNSIGNEDLONG	c_MBI_OldVersionDLL = 3
UNSIGNEDLONG	c_MBI_BadEditionDLL = 4
UNSIGNEDLONG	c_MBI_DLLFailed = 5

//------------------------------------------------------------------------------
//  The following parameters are available for formatting
//  user error messages:
//------------------------------------------------------------------------------

UNSIGNEDLONG	c_MBC_UserError = 3

UNSIGNEDLONG	c_MBI_CloseNotAllowed = 6

//------------------------------------------------------------------------------
//  The following parameters are available for formatting
//  validation messages:
//------------------------------------------------------------------------------

UNSIGNEDLONG	c_MBC_ValidationError = 4

UNSIGNEDLONG	c_MBI_DOMValError = 7
UNSIGNEDLONG	c_MBI_DOWValError = 8
UNSIGNEDLONG	c_MBI_DateValError = 9
UNSIGNEDLONG	c_MBI_DecValError = 10
UNSIGNEDLONG	c_MBI_FMTValError = 11
UNSIGNEDLONG	c_MBI_GEValError = 12
UNSIGNEDLONG	c_MBI_GTValError = 13
UNSIGNEDLONG	c_MBI_IntValError = 14
UNSIGNEDLONG	c_MBI_LEValError = 15
UNSIGNEDLONG	c_MBI_LTValError = 16
UNSIGNEDLONG	c_MBI_LengthValError = 17
UNSIGNEDLONG	c_MBI_MaxLenValError = 18
UNSIGNEDLONG	c_MBI_MinLenValError = 19
UNSIGNEDLONG	c_MBI_MonValError = 20
UNSIGNEDLONG	c_MBI_PhoneValError = 21
UNSIGNEDLONG	c_MBI_TimeValError = 22
UNSIGNEDLONG	c_MBI_YearValError = 23
UNSIGNEDLONG	c_MBI_ZipValError = 24
UNSIGNEDLONG	c_MBI_RequiredField = 25
UNSIGNEDLONG	c_MBI_NonVisRequiredField = 26
UNSIGNEDLONG	c_MBI_OKDevValError = 27
UNSIGNEDLONG	c_MBI_YesNoDevValError = 28
UNSIGNEDLONG	c_MBI_OKFMTDevValError = 29
UNSIGNEDLONG	c_MBI_YesNoFMTDevValError = 30

//------------------------------------------------------------------------------
//  The following parameters are available for formatting
//  DataWindow database error messages:
//------------------------------------------------------------------------------

UNSIGNEDLONG	c_MBC_DW_DBError = 5

UNSIGNEDLONG	c_MBI_DW_RetrieveError = 31
UNSIGNEDLONG	c_MBI_DW_UpdateError = 32
UNSIGNEDLONG	c_MBI_DW_CommitError = 33
UNSIGNEDLONG	c_MBI_DW_LockError = 34
UNSIGNEDLONG	c_MBI_DW_RollbackError = 35
UNSIGNEDLONG	c_MBI_DW_SaveAfterError = 36
UNSIGNEDLONG	c_MBI_DW_SaveBeforeError = 37
UNSIGNEDLONG	c_MBI_DW_SetKeyError = 38
UNSIGNEDLONG	c_MBI_DW_CC_ExceededRetry = 39

//------------------------------------------------------------------------------
//  The following parameters are available for formatting
//  DataWindow open error messages:
//------------------------------------------------------------------------------

UNSIGNEDLONG	c_MBC_DW_OpenError = 6

UNSIGNEDLONG	c_MBI_DW_PickedRowError = 40

//------------------------------------------------------------------------------
//  The following parameters are available for formatting
//  DataWindow validation messages:
//------------------------------------------------------------------------------

UNSIGNEDLONG	c_MBC_DW_ValidationError = 7

UNSIGNEDLONG	c_MBI_DW_DOMValError = 41
UNSIGNEDLONG	c_MBI_DW_DOWValError = 42
UNSIGNEDLONG	c_MBI_DW_DateValError = 43
UNSIGNEDLONG	c_MBI_DW_DecValError = 44
UNSIGNEDLONG	c_MBI_DW_FMTValError = 45
UNSIGNEDLONG	c_MBI_DW_GEValError = 46
UNSIGNEDLONG	c_MBI_DW_GTValError = 47
UNSIGNEDLONG	c_MBI_DW_IntValError = 48
UNSIGNEDLONG	c_MBI_DW_LEValError = 49
UNSIGNEDLONG	c_MBI_DW_LTValError = 50
UNSIGNEDLONG	c_MBI_DW_LengthValError = 51
UNSIGNEDLONG	c_MBI_DW_MaxLenValError = 52
UNSIGNEDLONG	c_MBI_DW_MinLenValError = 53
UNSIGNEDLONG	c_MBI_DW_MonValError = 54
UNSIGNEDLONG	c_MBI_DW_PhoneValError = 55
UNSIGNEDLONG	c_MBI_DW_TimeValError = 56
UNSIGNEDLONG	c_MBI_DW_YearValError = 57
UNSIGNEDLONG	c_MBI_DW_ZipValError = 58
UNSIGNEDLONG	c_MBI_DW_RequiredField = 59
UNSIGNEDLONG	c_MBI_DW_NonVisRequiredField = 60
UNSIGNEDLONG	c_MBI_DW_DWValError = 61
UNSIGNEDLONG	c_MBI_DW_OKDevValError = 62
UNSIGNEDLONG	c_MBI_DW_YesNoDevValError = 63
UNSIGNEDLONG	c_MBI_DW_OKFMTDevValError = 64
UNSIGNEDLONG	c_MBI_DW_YesNoFMTDevValError = 65
UNSIGNEDLONG	c_MBI_DW_PBValidation = 66

//------------------------------------------------------------------------------
//  The following parameters are available for formatting
//  user input messages:
//------------------------------------------------------------------------------

UNSIGNEDLONG	c_MBC_DW_UserInput = 8

UNSIGNEDLONG	c_MBI_DW_OneHighlightAnyChanges = 67
UNSIGNEDLONG	c_MBI_DW_HighlightAnyChanges = 68
UNSIGNEDLONG	c_MBI_DW_OneAnyChanges = 69
UNSIGNEDLONG	c_MBI_DW_AnyChanges = 70
UNSIGNEDLONG	c_MBI_DW_OneAskDeleteOk = 71
UNSIGNEDLONG	c_MBI_DW_AskDeleteOk = 72
UNSIGNEDLONG	c_MBI_DW_AskSure = 73

//------------------------------------------------------------------------------
//  The following parameters are available for formatting
//  DataWindow user error messages:
//------------------------------------------------------------------------------

UNSIGNEDLONG	c_MBC_DW_UserError = 9

UNSIGNEDLONG	c_MBI_DW_DeleteNotAllowed = 74
UNSIGNEDLONG	c_MBI_DW_ZeroToDelete = 75
UNSIGNEDLONG	c_MBI_DW_QueryNotAllowed = 76
UNSIGNEDLONG	c_MBI_DW_OnlyOneNew = 77

//------------------------------------------------------------------------------
//  The following parameters are available for formatting
//  DataWindow user information messages:
//------------------------------------------------------------------------------

UNSIGNEDLONG	c_MBC_DW_UserInformation = 10

UNSIGNEDLONG	c_MBI_DW_ZeroToDeleteAfterSave = 78
UNSIGNEDLONG	c_MBI_DW_ZeroSearchRows = 79
UNSIGNEDLONG	c_MBI_DW_ZeroFilterRows = 80
UNSIGNEDLONG	c_MBI_DW_ReselectedChanged = 81
UNSIGNEDLONG	c_MBI_DW_ReselectedNone = 82

//------------------------------------------------------------------------------
//  The following parameters are available for formatting
//  concurrency warnings/errors in the DataWindow:
//------------------------------------------------------------------------------

UNSIGNEDLONG	c_MBC_DW_ConcurrencyWarning = 11

UNSIGNEDLONG	c_MBI_DW_CC_NoError = 83
UNSIGNEDLONG	c_MBI_DW_CC_NonOverLapRow = 84
UNSIGNEDLONG	c_MBI_DW_CC_NonOverLapRowUser = 85
UNSIGNEDLONG	c_MBI_DW_CC_NonOverLapValue = 86
UNSIGNEDLONG	c_MBI_DW_CC_NonOverLapValueUser = 87
UNSIGNEDLONG	c_MBI_DW_CC_NonErrorHelp = 88
UNSIGNEDLONG	c_MBI_DW_CC_NonOverLapHelp = 89
UNSIGNEDLONG	c_MBI_DW_CC_OverLapHelp = 90
UNSIGNEDLONG	c_MBI_DW_CC_KeyConflictHelp = 91

UNSIGNEDLONG	c_MBC_DW_ConcurrencyError = 12

UNSIGNEDLONG	c_MBI_DW_CC_OverLapRow = 92
UNSIGNEDLONG	c_MBI_DW_CC_OverLapRowUser = 93
UNSIGNEDLONG	c_MBI_DW_CC_OverLapValue = 94
UNSIGNEDLONG	c_MBI_DW_CC_OverLapValueUser = 95
UNSIGNEDLONG	c_MBI_DW_CC_NonExistent = 96
UNSIGNEDLONG	c_MBI_DW_CC_NonExistentUser = 97
UNSIGNEDLONG	c_MBI_DW_CC_DropNonExistent = 98
UNSIGNEDLONG	c_MBI_DW_CC_DropNonExistentUser = 99
UNSIGNEDLONG	c_MBI_DW_CC_DeleteModified = 100
UNSIGNEDLONG	c_MBI_DW_CC_DeleteModifiedUser = 101
UNSIGNEDLONG	c_MBI_DW_CC_KeyConflictRow = 102
UNSIGNEDLONG	c_MBI_DW_CC_KeyConflictValue = 103

//------------------------------------------------------------------------------
//  The following parameters are available for formatting
//  messages for Developer generated errors:
//------------------------------------------------------------------------------

UNSIGNEDLONG	c_MBC_DeveloperError = 13

UNSIGNEDLONG	c_MBI_DW_SameParent = 104
UNSIGNEDLONG	c_MBI_DW_BadTransObject = 105
UNSIGNEDLONG	c_MBI_DW_BadShare = 106
UNSIGNEDLONG	c_MBI_DW_NoKeysInDW = 107
UNSIGNEDLONG	c_MBI_DW_NoKeysInParent = 108
UNSIGNEDLONG	c_MBI_DW_MismatchedKeysInParent = 109
UNSIGNEDLONG	c_MBI_DW_BadUpdateWhere = 110
UNSIGNEDLONG	c_MBI_DW_BadCCValue = 111
UNSIGNEDLONG	c_MBI_WindowOpenError = 112

//------------------------------------------------------------------------------
//  The following parameters are available for formatting
//  PowerClass DataWindow errors:
//------------------------------------------------------------------------------

UNSIGNEDLONG	c_MBC_DW_PowerClassError = 14

UNSIGNEDLONG	c_MBI_DW_ChangesClobbered = 113
UNSIGNEDLONG	c_MBI_DW_UnrecognizedDB = 114


end variables

forward prototypes
public function string format_string (string fmt_string, integer num_numbers, real number_parms[], integer num_strings, string string_parms[])
public subroutine get_database_type (transaction trans_obj, ref unsignedlong db_type, ref boolean is_odbc)
public function string get_database_descrip (transaction trans_obj)
public subroutine get_mb_info (unsignedlong mb_id, integer num_numbers, real number_parms[], integer num_strings, string string_parms[], ref string title, ref string text, ref icon icon, ref button button_set, ref integer default_button, ref boolean enabled)
public function boolean in_mb ()
public function integer message_box (unsignedlong mb_id, integer num_numbers, real number_parms[], integer num_strings, string string_parms[])
public function string quote_char (string fix_string, string quote_chr)
public subroutine set_in_mb (boolean in_or_out)
end prototypes

public function string format_string (string fmt_string, integer num_numbers, real number_parms[], integer num_strings, string string_parms[]);return fu_FormatString(Fmt_String, Num_Numbers, Number_Parms[], Num_Strings, String_Parms[])
end function

public subroutine get_database_type (transaction trans_obj, ref unsignedlong db_type, ref boolean is_odbc);fu_GetDatabaseType(Trans_Obj, DB_Type, Is_ODBC)
end subroutine

public function string get_database_descrip (transaction trans_obj);return fu_GetDatabaseDescrip(Trans_Obj)

end function

public subroutine get_mb_info (unsignedlong mb_id, integer num_numbers, real number_parms[], integer num_strings, string string_parms[], ref string title, ref string text, ref icon icon, ref button button_set, ref integer default_button, ref boolean enabled);fu_GetInfo (MB_ID, Num_Numbers, Number_Parms[], Num_Strings, String_Parms[], Title, Text, Icon, Button_Set, Default_Button, Enabled)
end subroutine

public function boolean in_mb ();return fu_InMB()

end function

public function integer message_box (unsignedlong mb_id, integer num_numbers, real number_parms[], integer num_strings, string string_parms[]);return fu_MessageBox(MB_ID, Num_Numbers, Number_Parms[], Num_Strings, String_Parms[])

end function

public function string quote_char (string fix_string, string quote_chr);return fu_QuoteChar(Fix_String, Quote_Chr)

end function

public subroutine set_in_mb (boolean in_or_out);fu_SetInMB(In_Or_Out)
end subroutine

on constructor;call n_mb_main::constructor;//******************************************************************
//  PC Module     : n_PC_MB_Main
//  Event         : Constructor
//  Description   : Initializes the UO_MB_Main constants.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Idx

//------------------------------------------------------------------
//  Initialize the Class titles.
//------------------------------------------------------------------

c_MBC_Undefined   = 1
c_MBI_Undefined   = 1
c_MB_MaxBaseClass = 14
c_MB_MaxBaseID    = 114

FOR l_Idx = c_MB_MaxBaseClass TO 1 STEP -1
   c_ClassTitle[l_Idx]   = c_MB_UseDefaultTitle
   c_DefaultTitle[l_Idx] = PCCA.Application_Name
NEXT

//------------------------------------------------------------------
//  Define the title for the "Undefined" Class.
//------------------------------------------------------------------

c_ClassTitle[c_MBC_Undefined] = &
   "Undefined Class (%1d) for Message ID #%2d"

//------------------------------------------------------------------
//  Initialize the message IDs.
//------------------------------------------------------------------

FOR l_Idx = c_MB_MaxBaseID TO 1 STEP -1
   c_MessageTitle[l_Idx]     = c_MB_UseClassTitle
   c_MessageIcon[l_Idx]      = c_MB_UseClassIcon
   c_MessageButtonSet[l_Idx] = Ok!
   c_MessageDefButton[l_Idx] = 1
   c_MessageEnabled[l_Idx]   = TRUE
NEXT

c_ClassIcon[1]  = StopSign!    /* c_MBC_Undefined */
c_ClassIcon[2]  = StopSign!    /* c_MBC_ApplicationError */
c_ClassIcon[3]  = Exclamation! /* c_MBC_UserError */
c_ClassIcon[4]  = Exclamation! /* c_MBC_ValidationError */
c_ClassIcon[5]  = StopSign!    /* c_MBC_DW_DBError */
c_ClassIcon[6]  = Exclamation! /* c_MBC_DW_OpenError */
c_ClassIcon[7]  = Exclamation! /* c_MBC_DW_ValidationError */
c_ClassIcon[8]  = Question!    /* c_MBC_DW_UserInput */
c_ClassIcon[9]  = Exclamation! /* c_MBC_DW_UserError */
c_ClassIcon[10] = Information! /* c_MBC_DW_UserInformation */
c_ClassIcon[11] = Information! /* c_MBC_DW_ConcurrencyWarning */
c_ClassIcon[12] = Exclamation! /* c_MBC_DW_ConcurrencyError */
c_ClassIcon[13] = StopSign!    /* c_MBC_DeveloperError */
c_ClassIcon[14] = StopSign!    /* c_MBC_DW_PowerClassError */

c_MessageClass[1]  = 1         /* c_MBI_Undefined */
c_MessageClass[2]  = 2         /* c_MBI_NoDLL */
c_MessageClass[3]  = 2         /* c_MBI_OldVersionDLL */
c_MessageClass[4]  = 2         /* c_MBI_BadEditionDLL */
c_MessageClass[5]  = 2         /* c_MBI_DLLFailed */
c_MessageClass[6]  = 3         /* c_MBI_CloseNotAllowed */
c_MessageClass[7]  = 4         /* c_MBI_DOMValError */
c_MessageClass[8]  = 4         /* c_MBI_DOWValError */
c_MessageClass[9]  = 4         /* c_MBI_DateValError */
c_MessageClass[10] = 4         /* c_MBI_DecValError */
c_MessageClass[11] = 4         /* c_MBI_FMTValError */
c_MessageClass[12] = 4         /* c_MBI_GEValError */
c_MessageClass[13] = 4         /* c_MBI_GTValError */
c_MessageClass[14] = 4         /* c_MBI_IntValError */
c_MessageClass[15] = 4         /* c_MBI_LEValError */
c_MessageClass[16] = 4         /* c_MBI_LTValError */
c_MessageClass[17] = 4         /* c_MBI_LengthValError */
c_MessageClass[18] = 4         /* c_MBI_MaxLenValError */
c_MessageClass[19] = 4         /* c_MBI_MinLenValError */
c_MessageClass[20] = 4         /* c_MBI_MonValError */
c_MessageClass[21] = 4         /* c_MBI_PhoneValError */
c_MessageClass[22] = 4         /* c_MBI_TimeValError */
c_MessageClass[23] = 4         /* c_MBI_YearValError */
c_MessageClass[24] = 4         /* c_MBI_ZipValError */
c_MessageClass[25] = 4         /* c_MBI_RequiredField */
c_MessageClass[26] = 4         /* c_MBI_NonVisRequiredField */
c_MessageClass[27] = 4         /* c_MBI_OKDevValError */
c_MessageClass[28] = 4         /* c_MBI_YesNoDevValError */
c_MessageClass[29] = 4         /* c_MBI_OKFMTDevValError */
c_MessageClass[30] = 4         /* c_MBI_YesNoFMTDevValError */
c_MessageClass[31] = 5         /* c_MBI_DW_RetrieveError */
c_MessageClass[32] = 5         /* c_MBI_DW_UpdateError */
c_MessageClass[33] = 5         /* c_MBI_DW_CommitError */
c_MessageClass[34] = 5         /* c_MBI_DW_LockError */
c_MessageClass[35] = 5         /* c_MBI_DW_RollbackError */
c_MessageClass[36] = 5         /* c_MBI_DW_SaveAfterError */
c_MessageClass[37] = 5         /* c_MBI_DW_SaveBeforeError */
c_MessageClass[38] = 5         /* c_MBI_DW_SetKeyError */
c_MessageClass[39] = 5         /* c_MBI_DW_CC_ExceededRetry */
c_MessageClass[40] = 6         /* c_MBI_DW_PickedRowError */
c_MessageClass[41] = 7         /* c_MBI_DW_DOMValError */
c_MessageClass[42] = 7         /* c_MBI_DW_DOWValError */
c_MessageClass[43] = 7         /* c_MBI_DW_DateValError */
c_MessageClass[44] = 7         /* c_MBI_DW_DecValError */
c_MessageClass[45] = 7         /* c_MBI_DW_FMTValError */
c_MessageClass[46] = 7         /* c_MBI_DW_GEValError */
c_MessageClass[47] = 7         /* c_MBI_DW_GTValError */
c_MessageClass[48] = 7         /* c_MBI_DW_IntValError */
c_MessageClass[49] = 7         /* c_MBI_DW_LEValError */
c_MessageClass[50] = 7         /* c_MBI_DW_LTValError */
c_MessageClass[51] = 7         /* c_MBI_DW_LengthValError */
c_MessageClass[52] = 7         /* c_MBI_DW_MaxLenValError */
c_MessageClass[53] = 7         /* c_MBI_DW_MinLenValError */
c_MessageClass[54] = 7         /* c_MBI_DW_MonValError */
c_MessageClass[55] = 7         /* c_MBI_DW_PhoneValError */
c_MessageClass[56] = 7         /* c_MBI_DW_TimeValError */
c_MessageClass[57] = 7         /* c_MBI_DW_YearValError */
c_MessageClass[58] = 7         /* c_MBI_DW_ZipValError */
c_MessageClass[59] = 7         /* c_MBI_DW_RequiredField */
c_MessageClass[60] = 7         /* c_MBI_DW_NonVisRequiredField */
c_MessageClass[61] = 7         /* c_MBI_DW_DWValError */
c_MessageClass[62] = 7         /* c_MBI_DW_OKDevValError */
c_MessageClass[63] = 7         /* c_MBI_DW_YesNoDevValError */
c_MessageClass[64] = 7         /* c_MBI_DW_OKFMTDevValError */
c_MessageClass[65] = 7         /* c_MBI_DW_YesNoFMTDevValError */
c_MessageClass[66] = 7         /* c_MBI_DW_PBValidation */
c_MessageClass[67] = 8         /* c_MBI_DW_OneHighlightAnyChanges */
c_MessageClass[68] = 8         /* c_MBI_DW_HighlightAnyChanges */
c_MessageClass[69] = 8         /* c_MBI_DW_OneAnyChanges */
c_MessageClass[70] = 8         /* c_MBI_DW_AnyChanges */
c_MessageClass[71] = 8         /* c_MBI_DW_OneAskDeleteOk */
c_MessageClass[72] = 8         /* c_MBI_DW_AskDeleteOk */
c_MessageClass[73] = 8         /* c_MBI_DW_AskSure */
c_MessageClass[74] = 9         /* c_MBI_DW_DeleteNotAllowed */
c_MessageClass[75] = 9         /* c_MBI_DW_ZeroToDelete */
c_MessageClass[76] = 9         /* c_MBI_DW_QueryNotAllowed */
c_MessageClass[77] = 9         /* c_MBI_DW_OnlyOneNew */
c_MessageClass[78] = 10        /* c_MBI_DW_ZeroToDeleteAfterSave */
c_MessageClass[79] = 10        /* c_MBI_DW_ZeroSearchRows */
c_MessageClass[80] = 10        /* c_MBI_DW_ZeroFilterRows */
c_MessageClass[81] = 10        /* c_MBI_DW_ReselectedChanged */
c_MessageClass[82] = 10        /* c_MBI_DW_ReselectedNone */
c_MessageClass[83] = 11        /* c_MBI_DW_CC_NoError */
c_MessageClass[84] = 11        /* c_MBI_DW_CC_NonOverLapRow */
c_MessageClass[85] = 11        /* c_MBI_DW_CC_NonOverLapRowUser */
c_MessageClass[86] = 11        /* c_MBI_DW_CC_NonOverLapValue */
c_MessageClass[87] = 11        /* c_MBI_DW_CC_NonOverLapValueUser */
c_MessageClass[88] = 11        /* c_MBI_DW_CC_NonErrorHelp */
c_MessageClass[89] = 11        /* c_MBI_DW_CC_NonOverLapHelp */
c_MessageClass[90] = 11        /* c_MBI_DW_CC_OverLapHelp */
c_MessageClass[91] = 11        /* c_MBI_DW_CC_KeyConflictHelp */
c_MessageClass[92] = 12        /* c_MBI_DW_CC_OverLapRow */
c_MessageClass[93] = 12        /* c_MBI_DW_CC_OverLapRowUser */
c_MessageClass[94] = 12        /* c_MBI_DW_CC_OverLapValue */
c_MessageClass[95] = 12        /* c_MBI_DW_CC_OverLapValueUser */
c_MessageClass[96] = 12        /* c_MBI_DW_CC_NonExistent */
c_MessageClass[97] = 12        /* c_MBI_DW_CC_NonExistentUser */
c_MessageClass[98] = 12        /* c_MBI_DW_CC_DropNonExistent */
c_MessageClass[99] = 12        /* c_MBI_DW_CC_DropNonExistentUser */
c_MessageClass[100] = 12       /* c_MBI_DW_CC_DeleteModified */
c_MessageClass[101] = 12       /* c_MBI_DW_CC_DeleteModifiedUser */
c_MessageClass[102] = 12       /* c_MBI_DW_CC_KeyConflictRow */
c_MessageClass[103] = 12       /* c_MBI_DW_CC_KeyConflictValue */
c_MessageClass[104] = 13       /* c_MBI_DW_SameParent */
c_MessageClass[105] = 13       /* c_MBI_DW_BadTransObject */
c_MessageClass[106] = 13       /* c_MBI_DW_BadShare */
c_MessageClass[107] = 13       /* c_MBI_DW_NoKeysInDW */
c_MessageClass[108] = 13       /* c_MBI_DW_NoKeysInParent */
c_MessageClass[109] = 13       /* c_MBI_DW_MismatchedKeysInParent */
c_MessageClass[110] = 13       /* c_MBI_DW_BadUpdateWhere */
c_MessageClass[111] = 13       /* c_MBI_DW_BadCCValue */
c_MessageClass[112] = 13       /* c_MBI_WindowOpenError */
c_MessageClass[113] = 14       /* c_MBI_DW_ChangesClobbered */
c_MessageClass[114] = 14       /* c_MBI_DW_UnrecognizedDB */

c_MessageText[1]   = "Undefined Message ID (%1d)."               /* c_MBI_Undefined */
c_MessageText[2]   = "The PowerClass DLL could not be loaded."   /* c_MBI_NoDLL */
c_MessageText[3]   = "An incorrect version of the PowerClass DLL was loaded.~n~n" + &
                     "Required version: %3s~n      " + &
                     "DLL Version: %4s"                          /* c_MBI_OldVersionDLL */
c_MessageText[4]   = "An incorrect edition of the PowerClass DLL was loaded.~n~n" + &
                     "Required DLL Edition: %3s~n" + &
                     "Actual DLL Edition  : %4s"                 /* c_MBI_BadEditionDLL */
c_MessageText[5]   = "The PowerClass DLL failed initialization." /* c_MBI_DLLFailed */
c_MessageText[6]   = "Use the %5 buttons to exit this window."   /* c_MBI_CloseNotAllowed */
c_MessageText[7]   = "Field contains an illegal~n" + &
                     "day of the month (%3s)."                   /* c_MBI_DOMValError */
c_MessageText[8]   = "Field contains an illegal day~n" + &
                     "of the week (%3s)."                        /* c_MBI_DOWValError */
c_MessageText[9]   = "Field contains an invalid~n" + &
                     "date value (%3s)."                         /* c_MBI_DateValError */
c_MessageText[10]  = "Field contains an invalid~n" + &
                     "decimal value (%3s)."                      /* c_MBI_DecValError */
c_MessageText[11]  = "Field contains a value~n" + &
                     "that is not in the correct~n" + &
                     "format of ~"%4s~"."                        /* c_MBI_FMTValError */
c_MessageText[12]  = "Field contains a value~nless than %1d."    /* c_MBI_GEValError */
c_MessageText[13]  = "Field contains a value less than~n" + &
                     "or equal to %1d."                          /* c_MBI_GTValError */
c_MessageText[14]  = "Field contains an invalid~n" + &
                     "integer value (%3s)."                      /* c_MBI_IntValError */
c_MessageText[15]  = "Field contains a value~n" + &
                     "greater than %1d."                         /* c_MBI_LEValError */
c_MessageText[16]  = "Field contains a value greater~n" + &
                     "than or equal to %1d."                     /* c_MBI_LTValError */
c_MessageText[17]  = "Field should be %1d~ncharacters long."     /* c_MBI_LengthValError */
c_MessageText[18]  = "Field must be less than %1d~n" + &
                     "characters long."                          /* c_MBI_MaxLenValError */
c_MessageText[19]  = "Field must be at least %1d~n" + &
                     "characters long."                          /* c_MBI_MinLenValError */
c_MessageText[20]  = "Field contains an illegal~nmonth (%3s)."   /* c_MBI_MonValError */
c_MessageText[21]  = "Field contains an invalid phone~n" + &
                     "number (%3s)."                             /* c_MBI_PhoneValError */
c_MessageText[22]  = "Field contains an invalid~n" + &
                     "time value (%3s)."                         /* c_MBI_TimeValError */
c_MessageText[23]  = "Field contains an illegal~nyear (%3s)."    /* c_MBI_YearValError */
c_MessageText[24]  = "Field contains an invalid~n" + &
                     "zip code (%3s)."                           /* c_MBI_ZipValError */
c_MessageText[25]  = "Field is a required field."                /* c_MBI_RequiredField */
c_MessageText[26]  = "Non-visible field is a required field."    /* c_MBI_NonVisRequiredField */
c_MessageText[27]  = "%1s"                                       /* c_MBI_OKDevValError */
c_MessageText[28]  = "%1s"                                       /* c_MBI_YesNoDevValError */
c_MessageText[29]  = "%1s"                                       /* c_MBI_OKFMTDevValError */
c_MessageText[30]  = "%1s"                                       /* c_MBI_YesNoFMTDevValError */
c_MessageText[31]  = "There was an error while retrieving data." /* c_MBI_DW_RetrieveError */
c_MessageText[32]  = "The changed record(s) could not be~n" + &
                     "updated to the database.  Try to~n" + &
                     "fix the error and save again."             /* c_MBI_DW_UpdateError */
c_MessageText[33]  = "There was an error while commiting~n" + &
                     "changes to the database.  Try to fix~n" + &
                     "the error and save again."                 /* c_MBI_DW_CommitError */
c_MessageText[34]  = "There was an error while trying to~n" + &
                     "lock the requested record(s).  The records~n" + &
                     "will be put into VIEW mode."               /* c_MBI_DW_LockError */
c_MessageText[35]  = "There was an error while rolling~n" + &
                     "back changes to the database."             /* c_MBI_DW_RollbackError */
c_MessageText[36]  = "There was an error after saving~n" + &
                     "changes to the database.  Try to~n" + &
                     "fix the error and save again."             /* c_MBI_DW_SaveAfterError */
c_MessageText[37]  = "There was an error before attempting to~n" + &
                     "save changes to the database.  Try to fix~n" + &
                     "the error and save again."                 /* c_MBI_DW_SaveBeforeError */
c_MessageText[38]  = "There was an error while setting~n" + &
                     "keys for the new record(s).  Try to~n" + &
                     "fix the error and save again."             /* c_MBI_DW_SetKeyError */
c_MessageText[39]  = "The maximum number of retries was " + &
                     "exceeded while attempting to update " + &
                     "the database.  Please try to fix the " + &
                     "error and save again."                     /* c_MBI_DW_CC_ExceededRetry */
c_MessageText[40]  = "There was an error while opening " + &
                     "window(s)."                                /* c_MBI_DW_PickedRowError */
c_MessageText[41]  = "Field contains an illegal~n" + &
                     "day of the month (%6s)."                   /* c_MBI_DW_DOMValError */
c_MessageText[42]  = "Field contains an illegal day~n" + &
                     "of the week (%6s)."                        /* c_MBI_DW_DOWValError */
c_MessageText[43]  = "Field contains an invalid~n" + &
                     "date value (%6s)."                         /* c_MBI_DW_DateValError */
c_MessageText[44]  = "Field contains an invalid~n" + &
                     "decimal value (%6s)."                      /* c_MBI_DW_DecValError */
c_MessageText[45]  = "Field contains a value~n" + &
                     "that is not in the correct~n" + &
                     "format of ~"%9s~"."                        /* c_MBI_DW_FMTValError */
c_MessageText[46]  = "Field contains a value~nless than %1d."    /* c_MBI_DW_GEValError */
c_MessageText[47]  = "Field contains a value less than~n" + &
                     "or equal to %1d."                          /* c_MBI_DW_GTValError */
c_MessageText[48]  = "Field contains an invalid~n" + &
                     "integer value (%6s)."                      /* c_MBI_DW_IntValError */
c_MessageText[49]  = "Field contains a value~ngreater than %1d." /* c_MBI_DW_LEValError */
c_MessageText[50]  = "Field contains a value greater~n" + &
                     "than or equal to %1d."                     /* c_MBI_DW_LTValError */
c_MessageText[51]  = "Field should be %1d~ncharacters long."     /* c_MBI_DW_LengthValError */
c_MessageText[52]  = "Field must be less than %1d~n" + &
                     "characters long."                          /* c_MBI_DW_MaxLenValError */
c_MessageText[53]  = "Field must be at least %1d~n" + &
                     "characters long."                          /* c_MBI_DW_MinLenValError */
c_MessageText[54]  = "Field contains an illegal~nmonth (%6s)."   /* c_MBI_DW_MonValError */
c_MessageText[55]  = "Field contains an invalid phone~n" + &
                     "number (%6s)."                             /* c_MBI_DW_PhoneValError */
c_MessageText[56]  = "Field contains an invalid~n" + &
                     "time value (%6s)."                         /* c_MBI_DW_TimeValError */
c_MessageText[57]  = "Field contains an illegal~nyear (%6s)."    /* c_MBI_DW_YearValError */
c_MessageText[58]  = "Field contains an invalid~n" + &
                     "zip code (%6s)."                           /* c_MBI_DW_ZipValError */
c_MessageText[59]  = "Field is a required field."                /* c_MBI_DW_RequiredField */
c_MessageText[60]  = "Non-visible field ~"%7s~" is a " + &
                     "required field."                           /* c_MBI_DW_NonVisRequiredField */
c_MessageText[61]  = "%8s"                                       /* c_MBI_DW_DWValError */
c_MessageText[62]  = "%8s"                                       /* c_MBI_DW_OKDevValError */
c_MessageText[63]  = "%8s"                                       /* c_MBI_DW_YesNoDevValError */
c_MessageText[64]  = "%8s"                                       /* c_MBI_DW_OKFMTDevValError */
c_MessageText[65]  = "%8s"                                       /* c_MBI_DW_YesNoFMTDevValError */
c_MessageText[66]  = "Validation Error"                          /* c_MBI_DW_PBValidation */
c_MessageText[67]  = "There have been changes in the~n" + &
                     "highlighted window.~n~n" + &
                     "Do you want to save these changes?"        /* c_MBI_DW_OneHighlightAnyChanges */
c_MessageText[68]  = "There have been changes in the~n" + &
                     "%1d highlighted windows.~n~n" + &
                     "Do you want to save these changes?"        /* c_MBI_DW_HighlightAnyChanges */
c_MessageText[69]  = "There have been changes in one window.~n~n" + &
                     "Do you want to save these changes?"        /* c_MBI_DW_OneAnyChanges */
c_MessageText[70]  = "There have been changes in %1d windows.~n~n" + &
                     "Do you want to save these changes?"        /* c_MBI_DW_AnyChanges */
c_MessageText[71]  = "There is one record selected for deletion.~n~n" + &
                     "Do you want to delete this record?"        /* c_MBI_DW_OneAskDeleteOk */
c_MessageText[72]  = "There are %1d records selected for deletion.~n~n" + &
                     "Do you want to delete these records?"      /* c_MBI_DW_AskDeleteOk */
c_MessageText[73]  = "Are you sure that you are ready to " + &
                     "exit this window?"                         /* c_MBI_DW_AskSure */
c_MessageText[74]  = "Delete is not allowed for this window."    /* c_MBI_DW_DeleteNotAllowed */
c_MessageText[75]  = "There are not any records selected for " + &
                     "deletion."                                 /* c_MBI_DW_ZeroToDelete */
c_MessageText[76]  = "Query is not allowed for this window."     /* c_MBI_DW_QueryNotAllowed */
c_MessageText[77]  = "Changes must be saved before a~n" + &
                     "new record can be inserted."               /* c_MBI_DW_OnlyOneNew */
c_MessageText[78]  = "After refreshing, there are not any " + &
                     "records selected~for deletion."            /* c_MBI_DW_ZeroToDeleteAfterSave */
c_MessageText[79]  = "There are not any records which meet the~n" + &
                     "search criteria."                          /* c_MBI_DW_ZeroSearchRows */
c_MessageText[80]  = "There are not any records which meet the~n" + &
                     "filter criteria."                          /* c_MBI_DW_ZeroFilterRows */
c_MessageText[81]  = "There were %1d record(s) selected before the~n" + &
                     "retrieve.  There are %2d record(s) selected~n" + &
                     "after the retrieve."                       /* c_MBI_DW_ReselectedChanged */
c_MessageText[82]  = "There are not any records selected after~n" + &
                     "the retrieve.  Please select a record."    /* c_MBI_DW_ReselectedNone */
c_MessageText[83]  = "No error -- this field matches the current " + &
                     "value in the database."                    /* c_MBI_DW_CC_NoError */
c_MessageText[84]  = "Some of the fields being displayed have " + &
                     "been updated in the database by another " + &
                     "user."                                     /* c_MBI_DW_CC_NonOverLapRow */
c_MessageText[85]  = "Some of the fields being displayed have " + &
                     "been updated in the database by user " + &
                     "~"%6s~"."                                  /* c_MBI_DW_CC_NonOverLapRowUser */
c_MessageText[86]  = "This field has been updated in the database " + &
                     "by another user."                          /* c_MBI_DW_CC_NonOverLapValue */
c_MessageText[87]  = "This field has been updated in the database " + &
                     "by user ~"%6s~"."                          /* c_MBI_DW_CC_NonOverLapValueUser */
c_MessageText[88]  = "The field that is highlighted does not " + &
                     "have a concurrency error."                 /* c_MBI_DW_CC_NonErrorHelp */
c_MessageText[89]  = "The field that is highlighted was not " + &
                     "modified by you.  However, it has been " + &
                     "modified by another user and saved to " + &
                     "the database.  It is generally advisable " + &
                     "to keep the new database value.  If you " + &
                     "are unsure, use the ~"CANCEL~" button to " + &
                     "stop the save process.  You will then be " + &
                     "able to make modifications and save later." /* c_MBI_DW_CC_NonOverLapHelp */
c_MessageText[90]  = "You modified the field that is highlighted.  " + &
                     "This field has also been modified by another " + &
                     "user and saved to the database.  You need to " + &
                     "decide which of the two modified values is " + &
                     "correct.  If you are unsure, use the ~"CANCEL~" " + &
                     "button to stop the save process.  You will " + &
                     "then be able to make modifications and save " + &
                     "later."                                    /* c_MBI_DW_CC_OverLapHelp */
c_MessageText[91]  = "The field that is highlighted is required " + &
                     "to be unique in the database.  The value " + &
                     "that was entered in this field has already " + &
                     "been used in the database.  You must choose " + &
                     "a unique value for this field."            /* c_MBI_DW_CC_KeyConflictHelp */
c_MessageText[92]  = "Some of the fields that you modified have " + &
                     "been updated in the database by another " + &
                     "user."                                     /* c_MBI_DW_CC_OverLapRow */
c_MessageText[93]  = "Some of the fields that you modified have " + &
                     "been updated in the database by user " + &
                     "~"%6s~"."                                  /* c_MBI_DW_CC_OverLapRowUser */
c_MessageText[94]  = "You modified this value and it has been " + &
                     "updated in the database by another user."  /* c_MBI_DW_CC_OverLapValue */
c_MessageText[95]  = "You modified this value and it has been " + &
                     "updated in the database by user ~"%6s~"."  /* c_MBI_DW_CC_OverLapValueUser */
c_MessageText[96]  = "You modified this row, but another user " + &
                     "has deleted it from the database.~n~n" + &
                     "Do you want to reinsert this record into " + &
                     "the database with your changes?  If you " + &
                     "choose ~"NO~", the record will remain " + &
                     "deleted and your changes will be dropped." /* c_MBI_DW_CC_NonExistent */
c_MessageText[97]  = "You modified this row, but user ~"%6s~" " + &
                     "has deleted it from the database.~n~n" + &
                     "Do you want to reinsert this record " + &
                     "into the database with your changes?  " + &
                     "If you choose ~"NO~", the record will " + &
                     "remain deleted and your changes will be " + &
                     "dropped."                                  /* c_MBI_DW_CC_NonExistentUser */
c_MessageText[98]  = "You modified this row, but another user " + &
                     "has deleted it from the database.~n~n" + &
                     "This row is unable to be reinserted into " + &
                     "the database.  If you choose ~"NO~", the " + &
                     "record will remain deleted and your changes " + &
                     "will be dropped.  If you choose ~"CANCEL~", " + &
                     "the save process will be stopped."         /* c_MBI_DW_CC_DropNonExistent */
c_MessageText[99]  = "You modified this row, but user ~"%6s~" " + &
                     "has deleted it from the database.~n~n" + &
                     "This row is unable to be reinserted into " + &
                     "the database.  If you choose ~"NO~", the " + &
                     "record will remain deleted and your changes " + &
                     "will be dropped.  If you choose ~"CANCEL~", " + &
                     "the save process will be stopped."         /* c_MBI_DW_CC_DropNonExistentUser */
c_MessageText[100] = "You deleted this row, but another user has " + &
                     "modified it in the database.~n~n" + &
                     "Do you still want to delete this " + &
                     "record from the database?  If you choose " + &
                     "~"NO~", the record will not be deleted."   /* c_MBI_DW_CC_DeleteModified */
c_MessageText[101] = "You deleted this row, but user ~"%6s~" " + &
                     "has modified it in the database.~n~n" + &
                     "Do you still want to delete this record " + &
                     "from the database?  If you choose ~"NO~", " + &
                     "the record will not be deleted."           /* c_MBI_DW_CC_DeleteModifiedUser */
c_MessageText[102] = "Some of the fields on this row are " + &
                     "required to be unique in the database."    /* c_MBI_DW_CC_KeyConflictRow */
c_MessageText[103] = "This value is required to be unique " + &
                     "in the database."                          /* c_MBI_DW_CC_KeyConflictValue */
c_MessageText[104] = "%1s:~n~n      DataWindow ~"%3s~" " + &
                     "specified itself as its~n      parent " + &
                     "DataWindow."                               /* c_MBI_DW_SameParent */
c_MessageText[105] = "%1s:~n~n      Unable to set the " + &
                     "transaction object~n      for " + &
                     "DataWindow ~"%3s~"."                       /* c_MBI_DW_BadTransObject */
c_MessageText[106] = "%1s:~n~n      Unable to to establish " + &
                     "share for~n      DataWindow ~"%3s~"."      /* c_MBI_DW_BadShare */
c_MessageText[107] = "%1s:~n~n      DataWindow ~"%3s~" does " + &
                     "not~n      have any keys."                 /* c_MBI_DW_NoKeysInDW */
c_MessageText[108] = "%1s:~n~n      The parent of DataWindow" + &
                     " ~"%3s~"~n      does not have any keys."   /* c_MBI_DW_NoKeysInParent */
c_MessageText[109] = "%1s:~n~n      The keys in the parent of " + &
                     "DataWindow~n      ~"%3s~" do not match."   /* c_MBI_DW_MismatchedKeysInParent */
c_MessageText[110] = "%1s:~n~n      The ~"Update WHERE~" " + &
                     "option is not configured correctly " + &
                     "for concurrency checking for DataObject " + &
                     "~"%5s~" in DataWindow ~"%3s~"."            /* c_MBI_DW_BadUpdateWhere */
c_MessageText[111] = "%1s:~n~nThe following unknown codes were " + &
                     "encountered after a pcd_ConcurrentError " + &
                     "event:~n~n%6s"                             /* c_MBI_DW_BadCCValue */
c_MessageText[112] = "An error occurred in the %5s~n" + &
                     "event of Window ~"%3s~"."                  /* c_MBI_WindowOpenError */
c_MessageText[113] = "%1s:~n~n      Internal Error.  There are " + &
                     "changes in~n      the following DataWindow " + &
                     "that have not~n      been saved:~n~n" + &
                     "      ~"%3s~""                             /* c_MBI_DW_ChangesClobbered */
c_MessageText[114] = "%1s:~n~n      PowerClass concurrency " + &
                     "checking does~n      not recognize the " + &
                     "type of your database:~n~n            " + &
                     "~"%6s~""                                   /* c_MBI_DW_UnrecognizedDB */

c_MessageButtonSet[28]  = YesNo!       /* c_MBI_YesNoDevValError */
c_MessageButtonSet[30]  = YesNo!       /* c_MBI_YesNoFMTDevValError */
c_MessageButtonSet[63]  = YesNo!       /* c_MBI_DW_YesNoDevValError */
c_MessageButtonSet[65]  = YesNo!       /* c_MBI_DW_YesNoFMTDevValError */
c_MessageButtonSet[67]  = YesNoCancel! /* c_MBI_DW_OneHighlightAnyChanges */
c_MessageButtonSet[68]  = YesNoCancel! /* c_MBI_DW_HighlightAnyChanges */
c_MessageButtonSet[69]  = YesNoCancel! /* c_MBI_DW_OneAnyChanges */
c_MessageButtonSet[70]  = YesNoCancel! /* c_MBI_DW_AnyChanges */
c_MessageButtonSet[71]  = YesNo!       /* c_MBI_DW_OneAskDeleteOk */
c_MessageButtonSet[72]  = YesNo!       /* c_MBI_DW_AskDeleteOk */
c_MessageButtonSet[73]  = YesNo!       /* c_MBI_DW_AskSure */
c_MessageButtonSet[96]  = YesNoCancel! /* c_MBI_DW_CC_NonExistent */
c_MessageButtonSet[97]  = YesNoCancel! /* c_MBI_DW_CC_NonExistentUser */
c_MessageButtonSet[98]  = OkCancel!    /* c_MBI_DW_CC_DropNonExistent */
c_MessageButtonSet[99]  = OkCancel!    /* c_MBI_DW_CC_DropNonExistentUser */
c_MessageButtonSet[100] = YesNoCancel! /* c_MBI_DW_CC_DeleteModified */
c_MessageButtonSet[101] = YesNoCancel! /* c_MBI_DW_CC_DeleteModifiedUser */

c_MessageDefButton[71]  = 2            /* c_MBI_DW_OneAskDeleteOk */
c_MessageDefButton[72]  = 2            /* c_MBI_DW_AskDeleteOk */

c_MessageEnabled[81]    = FALSE        /* c_MBI_DW_ReselectedChanged */
c_MessageEnabled[82]    = FALSE        /* c_MBI_DW_ReselectedNone */

//------------------------------------------------------------------
//  Define the title for the "Undefined" Message ID.
//------------------------------------------------------------------

c_MessageTitle[c_MBI_Undefined] = "Undefined Message ID!"
c_MessageIcon[c_MBI_Undefined]  = StopSign!

TriggerEvent("pc_Check")
end on

on n_pc_mb_main.create
TriggerEvent( this, "constructor" )
end on

on n_pc_mb_main.destroy
TriggerEvent( this, "destructor" )
end on

