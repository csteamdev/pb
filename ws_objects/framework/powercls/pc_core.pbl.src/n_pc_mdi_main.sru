﻿$PBExportHeader$n_pc_mdi_main.sru
$PBExportComments$PowerClass Ancestor MicroHelp manager object
forward
global type n_pc_mdi_main from nonvisualobject
end type
end forward

global type n_pc_mdi_main from nonvisualobject
end type
global n_pc_mdi_main n_pc_mdi_main

type variables
//------------------------------------------------------------------------------
//  Instance Variables in sorted order.
//------------------------------------------------------------------------------

LONG		i_LastMDIID

REAL		i_MDI_Numbers[]
STRING		i_MDI_Strings[]
INTEGER		i_MDICount
STRING		i_MDICurMessage
LONG		i_MDIID[]
STRING		i_MDIStack[]
BOOLEAN	i_MDIVisible[]

BOOLEAN	i_Redraw = TRUE

//------------------------------------------------------------------------------
//  Constant Variables in logical order.
//------------------------------------------------------------------------------

UNSIGNEDLONG	c_MDI_Undefined = 1

//------------------------------------------------------------------------------
//  The following parameters are available for formatting
//  Database connect MicroHelp prompts:
//
//     Numbers   - (None)
//
//     String #1 - Database type
//------------------------------------------------------------------------------

UNSIGNEDLONG	c_MDI_DB_EnterDBMS = 2
UNSIGNEDLONG	c_MDI_DB_Connect = 3

//------------------------------------------------------------------------------
//  The following parameters are available for formatting
//  DataWindow MicroHelp prompts:
//
//     Numbers - (None)
//
//     Strings - (None)
//------------------------------------------------------------------------------

UNSIGNEDLONG	c_MDI_DW_Append = 4
UNSIGNEDLONG	c_MDI_DW_Delete = 5
UNSIGNEDLONG	c_MDI_DW_Insert = 6
UNSIGNEDLONG	c_MDI_DW_Modify = 7
UNSIGNEDLONG	c_MDI_DW_Query = 8
UNSIGNEDLONG	c_MDI_DW_RefreshRows = 9
UNSIGNEDLONG	c_MDI_DW_Retrieve = 10
UNSIGNEDLONG	c_MDI_DW_ReselectRows = 11
UNSIGNEDLONG	c_MDI_DW_ScrollFirst = 12
UNSIGNEDLONG	c_MDI_DW_ScrollLast = 13
UNSIGNEDLONG	c_MDI_DW_ScrollNext = 14
UNSIGNEDLONG	c_MDI_DW_ScrollPrevious = 15
UNSIGNEDLONG	c_MDI_DW_Search = 16
UNSIGNEDLONG	c_MDI_DW_Filter = 17
UNSIGNEDLONG	c_MDI_DW_Validate = 18
UNSIGNEDLONG	c_MDI_DW_View = 19

//------------------------------------------------------------------------------
//  The following parameters are available for formatting
//  general MicroHelp prompts:
//
//     Numbers - (None)
//
//     Strings - (None)
//------------------------------------------------------------------------------

UNSIGNEDLONG	c_MDI_Close = 20
UNSIGNEDLONG	c_MDI_Open = 21
UNSIGNEDLONG	c_MDI_Print = 22
UNSIGNEDLONG	c_MDI_Save = 23
UNSIGNEDLONG	c_MDI_SaveRowsAs = 24

UNSIGNEDLONG	c_MDI_MaxBaseID = 24

//------------------------------------------------------------------------------

UNSIGNEDLONG	c_Invisible 		= 0
UNSIGNEDLONG	c_Show 			= 1
UNSIGNEDLONG	c_Mark 			= 2
UNSIGNEDLONG	c_ShowMark 		= 3

UNSIGNEDLONG	c_ColorUndefined 		= 0
UNSIGNEDLONG	c_Black 			= 1
UNSIGNEDLONG	c_White 			= 2
UNSIGNEDLONG	c_Gray 			= 3
UNSIGNEDLONG	c_DarkGray 		= 4
UNSIGNEDLONG	c_Red 			= 5
UNSIGNEDLONG	c_DarkRed 		= 6
UNSIGNEDLONG	c_Green 			= 7
UNSIGNEDLONG	c_DarkGreen 		= 8
UNSIGNEDLONG	c_Blue 			= 9
UNSIGNEDLONG	c_DarkBlue 		= 10
UNSIGNEDLONG	c_Magenta 		= 11
UNSIGNEDLONG	c_DarkMagenta 		= 12
UNSIGNEDLONG	c_Cyan 			= 13
UNSIGNEDLONG	c_DarkCyan 		= 14
UNSIGNEDLONG	c_Yellow 			= 15
UNSIGNEDLONG	c_Brown 			= 16

UNSIGNEDLONG	c_ColorFirst 		= 1
UNSIGNEDLONG	c_ColorLast 		= 16

UNSIGNEDLONG	c_DW_BorderUndefined 	= 0
UNSIGNEDLONG	c_DW_BorderNone 	= 1
UNSIGNEDLONG	c_DW_BorderShadowBox 	= 2
UNSIGNEDLONG	c_DW_BorderBox 		= 3
UNSIGNEDLONG	c_DW_BorderResize 	= 4
UNSIGNEDLONG	c_DW_BorderUnderLine 	= 5
UNSIGNEDLONG	c_DW_BorderLowered 	= 6
UNSIGNEDLONG	c_DW_BorderRaised 	= 7

UNSIGNEDLONG	c_DW_BorderFirst 		= 1
UNSIGNEDLONG	c_DW_BorderLast 		= 7

INTEGER		c_AcceptCB 		= 1
INTEGER		c_CancelCB 		= 2
INTEGER		c_CloseCB 		= 3
INTEGER		c_DeleteCB 		= 4
INTEGER		c_FilterCB 		= 5
INTEGER		c_FirstCB 		= 6
INTEGER		c_InsertCB 		= 7
INTEGER		c_LastCB 		= 8
INTEGER		c_MainCB 		= 9
INTEGER		c_ModifyCB 		= 10
INTEGER		c_NewCB 		= 11
INTEGER		c_NextCB 		= 12
INTEGER		c_PreviousCB 		= 13
INTEGER		c_PrintCB 		= 14
INTEGER		c_QueryCB 		= 15
INTEGER		c_ResetQueryCB 		= 16
INTEGER		c_RetrieveCB 		= 17
INTEGER		c_SaveCB 		= 18
INTEGER		c_SaveRowsAsCB 		= 19
INTEGER		c_SearchCB 		= 20
INTEGER		c_ViewCB 		= 21

BOOLEAN	c_MDI_Enabled 		= TRUE
BOOLEAN	c_MDI_Disabled 		= FALSE

BOOLEAN	c_DrawNow 		= TRUE
BOOLEAN	c_BufferDraw 		= FALSE

STRING		c_MDI_MenuLabelFile       	= "&File"
STRING		c_MDI_MenuLabelEdit       	= "&Edit"

STRING		c_MDI_MenuLabelNewFile    	= "&New"
STRING		c_MDI_MenuLabelOpen       	= "&Open"
STRING		c_MDI_MenuLabelClose      	= "&Close"
STRING		c_MDI_MenuLabelCancel     	= "&Cancel"
STRING		c_MDI_MenuLabelAccept     	= "&Accept"
STRING		c_MDI_MenuLabelSave       	= "&Save"
STRING		c_MDI_MenuLabelSaveRowsAs = "Save Ro&ws As..."
STRING		c_MDI_MenuLabelPrint      	= "&Print"
STRING		c_MDI_MenuLabelPrintSetup = "Prin&ter Setup..."
STRING		c_MDI_MenuLabelExit       	= "E&xit"

STRING		c_MDI_MenuLabelNew        	= "&New"
STRING		c_MDI_MenuLabelView       	= "&View"
STRING		c_MDI_MenuLabelModify     	= "&Modify"
STRING		c_MDI_MenuLabelInsert     	= "&Insert"
STRING		c_MDI_MenuLabelDelete     	= "&Delete"
STRING		c_MDI_MenuLabelFirst      	= "&First"
STRING		c_MDI_MenuLabelPrev       	= "P&revious"
STRING		c_MDI_MenuLabelNext       	= "Ne&xt"
STRING		c_MDI_MenuLabelLast       	= "&Last"
STRING		c_MDI_MenuLabelQuery      	= "&Query"
STRING		c_MDI_MenuLabelSearch     	= "S&earch"
STRING		c_MDI_MenuLabelFilter     	= "Fil&ter"

STRING		c_MDI_MenuLabelSeperator	= "-"

STRING		c_MHPromptText[]
BOOLEAN	c_MHPromptEnabled[]
STRING		c_ColorNames[] 
STRING		c_RGBStrings[]
LONG		c_RGBValues[]
STRING		c_DW_BorderNames[] 
STRING		c_DW_Borders[] 

end variables

forward prototypes
public subroutine fu_getinternational ()
public function long fu_getmdiserialid ()
public subroutine fu_pop ()
public subroutine fu_popserialid (long serial_id)
public function long fu_push (string prompt, unsignedlong visible_option)
public function long fu_pushid (unsignedlong mdi_id, unsignedlong visible_option)
public function long fu_pushidformat (unsignedlong mdi_id, unsignedlong visible_option, integer num_numbers, real number_parms[], integer num_strings, string string_parms[])
public subroutine fu_refresh ()
public subroutine fu_setredraw (boolean on_or_off)
public subroutine fu_visible (unsignedlong visible_option)
public subroutine get_international ()
public function long get_mdi_serial_id ()
public subroutine pop ()
public subroutine pop_serial_id (long serial_id)
public function long push (string prompt, unsignedlong visible_option)
public function long push_id (unsignedlong mdi_id, unsignedlong visible_option)
public function long push_id_format (unsignedlong mdi_id, unsignedlong visible_option, integer num_numbers, real number_parms[], integer num_strings, string string_parms[])
public subroutine refresh ()
public subroutine setredraw (boolean on_or_off)
public subroutine visible (unsignedlong visible_option)
end prototypes

public subroutine fu_getinternational ();//******************************************************************
//  PC Module     : n_PC_MDI_Main
//  Subroutine    : fu_GetInternational
//  Description   : Obsolete.
//
//  Parameters    : (None)
//  Return Value  : (None)
//
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

end subroutine

public function long fu_getmdiserialid ();//******************************************************************
//  PC Module     : n_PC_MDI_Main
//  Function      : fu_GetMDISerialID
//  Description   : Returns the serial identifier for the
//                  prompt that is currently on the top.
//                  of the MDI prompt stack.
//
//  Parameters    : (None)
//
//  Return Value  : LONG -
//                       The serial identifier for the prompt
//                       currently on top of the prompt stack.
//
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

LONG  l_Return

l_Return = i_MDIID[i_MDICount]

RETURN l_Return
end function

public subroutine fu_pop ();//******************************************************************
//  PC Module     : n_PC_MDI_Main
//  Subroutine    : fu_Pop
//  Description   : Pops the current MDI prompt.
//
//  Parameters    : (None)
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

//------------------------------------------------------------------
//  Make sure that there is a prompt to pop.  The first prompt
//  in the stack is "Ready" and we don't allow that to be poped.
//------------------------------------------------------------------

IF i_MDICount > 1 THEN
   i_MDICount = i_MDICount - 1
END IF

//------------------------------------------------------------------
//  Make sure redraw is on for the MicroHelp line.
//------------------------------------------------------------------

IF i_Redraw THEN

   //---------------------------------------------------------------
   //  See if the prompt that is now at the top of the stack
   //  is visible.  If it is not, then we will leave the current
   //  prompt displayed on the MicroHelp line.
   //---------------------------------------------------------------

   IF i_MDIVisible[i_MDICount] THEN

      //------------------------------------------------------------
      //  This prompt is to become visible.  Make sure that it
      //  is not already being displayed.
      //------------------------------------------------------------

      IF i_MDICurMessage <> i_MDIStack[i_MDICount] THEN

         //---------------------------------------------------------
         //  Make sure there is an MDI Frame with a MicroHelp line.
         //---------------------------------------------------------

         IF PCCA.MDI_Frame_Valid THEN
            IF PCCA.MDI_Frame.WindowType = MDIHelp! THEN
               i_MDICurMessage = i_MDIStack[i_MDICount]
               PCCA.MDI_Frame.SetMicroHelp(i_MDICurMessage)
            END IF
         END IF
      END IF
   END IF
END IF

RETURN
end subroutine

public subroutine fu_popserialid (long serial_id);//******************************************************************
//  PC Module     : n_PC_MDI_Main
//  Subroutine    : fu_PopSerialID
//  Description   : Pops the current MDI prompt if it matches
//                  the specified serial ID.
//
//  Parameters    : (None)
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

IF i_MDIID[i_MDICount] = Serial_ID THEN
   fu_Pop()
END IF

RETURN
end subroutine

public function long fu_push (string prompt, unsignedlong visible_option);//******************************************************************
//  PC Module     : n_PC_MDI_Main
//  Function      : fu_Push
//  Description   : Pushs an MDI prompt and optionally shows it
//                  and marks it as visible.
//
//  Parameters    : STRING Prompt -
//                        The prompt that is to be pushed onto
//                        the MDI prompt stack.
//
//                  UNSIGNEDLONG Visible_Option -
//                        The Visible_Option should be specified
//                        one of the following values:
//
//                           c_Invisible -
//                              Put the prompt on the MDI prompt
//                              stack, but do not display it on
//                              the MicroHelp line.
//
//                           c_Show -
//                              Put the prompt on the MDI prompt
//                              stack and display it.  Mark it as
//                              invisible so that if it gets
//                              overwritten on the display at a
//                              later time by another MDI prompt,
//                              it will not be re-displayed when
//                              that prompt is popped (i.e. the
//                              popped prompt will remain displayed
//                              on the MicroHelp line).
//
//                           c_Mark -
//                              Put the prompt on the MDI prompt
//                              stack, but do not display it.  Mark
//                              it as visible so that when another
//                              prompt is popped, it will be
//                              displayed.
//
//                           c_ShowMark -
//                              Put the prompt on the MDI prompt
//                              stack and display it.  If it gets
//                              overwritten on the display at a
//                              later time by another MDI prompt,
//                              re-display it when that prompt is
//                              popped.
//
//  Return Value  : LONG -
//                       A serial identifier for the prompt that
//                       was pushed.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

//------------------------------------------------------------------
//  Bump the prompt identifier and make sure that it doesn't
//  overflow.
//------------------------------------------------------------------

i_LastMDIID = i_LastMDIID + 1

IF i_LastMDIID > 2000000000 THEN
   i_LastMDIID = 1
END IF

//------------------------------------------------------------------
//  Add the new prompt and its ID to the stack of prompts.
//------------------------------------------------------------------

i_MDICount             = i_MDICount + 1
i_MDIStack[i_MDICount] = Prompt
i_MDIID[i_MDICount]    = i_LastMDIID

fu_Visible(Visible_Option)

RETURN i_LastMDIID
end function

public function long fu_pushid (unsignedlong mdi_id, unsignedlong visible_option);//******************************************************************
//  PC Module     : n_PC_MDI_Main
//  Function      : fu_PushID
//  Description   : Pushs an MDI prompt specified by MDI_ID
//                  and optionally shows it and marks it as
//                  visible.
//
//  Parameters    : UNSIGNEDLONG MDI_ID -
//                        The prompt identifier that is to be
//                        pushed onto the MDI prompt stack.
//
//                  UNSIGNEDLONG Visible_Option -
//                        The Visible_Option should be specified
//                        one of the following values:
//
//                           c_Invisible -
//                              Put the prompt on the MDI prompt
//                              stack, but do not display it on
//                              the MicroHelp line.
//
//                           c_Show -
//                              Put the prompt on the MDI prompt
//                              stack and display it.  Mark it as
//                              invisible so that if it gets
//                              overwritten on the display at a
//                              later time by another MDI prompt,
//                              it will not be re-displayed when
//                              that prompt is popped (i.e. the
//                              popped prompt will remain displayed
//                              on the MicroHelp line).
//
//                           c_Mark -
//                              Put the prompt on the MDI prompt
//                              stack, but do not display it.  Mark
//                              it as visible so that when another
//                              prompt is popped, it will be
//                              displayed.
//
//                           c_ShowMark -
//                              Put the prompt on the MDI prompt
//                              stack and display it.  If it gets
//                              overwritten on the display at a
//                              later time by another MDI prompt,
//                              re-display it when that prompt is
//                              popped.
//
//  Return Value  : LONG -
//                        A serial identifier for the prompt that
//                        was pushed.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

LONG  l_Return

l_Return = fu_PushIDFormat(MDI_ID, Visible_Option,  &
                           0,      i_MDI_Numbers[], &
                           0,      i_MDI_Strings[])

RETURN l_Return
end function

public function long fu_pushidformat (unsignedlong mdi_id, unsignedlong visible_option, integer num_numbers, real number_parms[], integer num_strings, string string_parms[]);//******************************************************************
//  PC Module     : n_PC_MDI_Main
//  Function      : fu_PushIDFormat
//  Description   : Pushs an MDI prompt specified by MDI_ID,
//                  formats it, and optionally shows it and
//                  marks it as visible.
//
//  Parameters    : UNSIGNEDLONG MDI_ID -
//                        The prompt identifier that is to be
//                        pushed onto the MDI prompt stack.
//
//                  UNSIGNEDLONG Visible_Option -
//                        The Visible_Option should be specified
//                        one of the following values:
//
//                           c_Invisible -
//                              Put the prompt on the MDI prompt
//                              stack, but do not display it on
//                              the MicroHelp line.
//
//                           c_Show -
//                              Put the prompt on the MDI prompt
//                              stack and display it.  Mark it as
//                              invisible so that if it gets
//                              overwritten on the display at a
//                              later time by another MDI prompt,
//                              it will not be re-displayed when
//                              that prompt is popped (i.e. the
//                              popped prompt will remain displayed
//                              on the MicroHelp line).
//
//                           c_Mark -
//                              Put the prompt on the MDI prompt
//                              stack, but do not display it.  Mark
//                              it as visible so that when another
//                              prompt is popped, it will be
//                              displayed.
//
//                           c_ShowMark -
//                              Put the prompt on the MDI prompt
//                              stack and display it.  If it gets
//                              overwritten on the display at a
//                              later time by another MDI prompt,
//                              re-display it when that prompt is
//                              popped.
//
//                  INTEGER Num_Numbers -
//                        The number of REAL parameters to be
//                        substituted into the string.
//
//                  REAL Number_Parms[] -
//                        An array containing the REAL
//                        parameters to be substituted into the
//                        string.
//
//                  INTEGER Num_Strings -
//                        The number of string parameters to be
//                        substituted into the string.
//
//                  STRING String_Parms[] -
//                        An array containing the string
//                        parameters to be substituted into the
//                        string.
//
//  Return Value  : LONG -
//                        A serial identifier for the prompt that
//                        was pushed.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN  Enabled
STRING   Prompt
REAL     l_Number[]

//------------------------------------------------------------------
//  Make sure that we have a valid Prompt ID.
//------------------------------------------------------------------

IF MDI_ID > 0                             AND &
   MDI_ID <= UpperBound(c_MHPromptText[]) AND &
   MDI_ID <> C_MDI_Undefined              THEN

   Prompt  = c_MHPromptText[MDI_ID]
   Enabled = c_MHPromptEnabled[MDI_ID]

   //---------------------------------------------------------------
   //  If Prompt is NULL, then we can't do formatting.
   //---------------------------------------------------------------

   IF IsNull(Prompt) THEN
      Prompt = ""
   ELSE

      //------------------------------------------------------------
      //  Do the formatting of the prompt.
      //------------------------------------------------------------

      Prompt = PCCA.MB.fu_FormatString                  &
                  (Prompt, Num_Numbers, Number_Parms[], &
                           Num_Strings, String_Parms[])
   END IF
ELSE

   //---------------------------------------------------------------
   //  Invalid Prompt ID.
   //---------------------------------------------------------------

   l_Number[1] = MDI_ID
   Prompt      = PCCA.MB.fu_FormatString              &
                    (c_MHPromptText[c_MDI_Undefined], &
                     1, l_Number[],                   &
                     0, i_MDI_Strings[])
   Enabled     = c_MHPromptEnabled[c_MDI_Undefined]
END IF

//------------------------------------------------------------------
//  Bump the prompt identifier and make sure that it doesn't
//  overflow.
//------------------------------------------------------------------

i_LastMDIID = i_LastMDIID + 1

IF i_LastMDIID > 2000000000 THEN
   i_LastMDIID = 1
END IF

//------------------------------------------------------------------
//  If this prompt is not enabled then we will just make a copy
//   of the top-most prompt.
//------------------------------------------------------------------

i_MDICount = i_MDICount + 1

IF NOT Enabled THEN

   i_MDIStack[i_MDICount]   = i_MDIStack[i_MDICount   - 1]
   i_MDIVisible[i_MDICount] = i_MDIVisible[i_MDICount - 1]

ELSE

   //---------------------------------------------------------------
   //  Add the new prompt and its ID to the stack of prompts.
   //---------------------------------------------------------------

   i_MDIStack[i_MDICount] = Prompt
   i_MDIID[i_MDICount]    = i_LastMDIID

   fu_Visible(Visible_Option)
END IF

RETURN i_LastMDIID
end function

public subroutine fu_refresh ();//******************************************************************
//  PC Module     : n_PC_MDI_Main
//  Subroutine    : fu_Refresh
//  Description   : Refreshs the MicroHelp line.
//
//  Parameters    : (None)
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

//------------------------------------------------------------------
//  Make sure redraw is on for the MicroHelp line.
//------------------------------------------------------------------

IF i_Redraw THEN

   //---------------------------------------------------------------
   //  Make sure there is an MDI Frame with a MicroHelp line.
   //---------------------------------------------------------------

   IF PCCA.MDI_Frame_Valid THEN
      IF PCCA.MDI_Frame.WindowType = MDIHelp! THEN
         PCCA.MDI_Frame.SetMicroHelp(i_MDICurMessage)
      END IF
   END IF
END IF

RETURN
end subroutine

public subroutine fu_setredraw (boolean on_or_off);//******************************************************************
//  PC Module     : n_PC_MDI_Main
//  Subroutine    : fu_SetRedraw
//  Description   : Turns redraw on or off for the MDI object.
//
//  Parameters    : BOOLEAN On_Or_Off -
//                       Specifies if the MDI object is allowed
//                       to draw on the MicroHelp line.
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

i_Redraw = On_Or_Off

IF i_Redraw THEN

   //---------------------------------------------------------------
   //  Redraw was turned on.  Make sure the current prompt is
   //  visible, if necessary.
   //---------------------------------------------------------------

   IF i_MDIVisible[i_MDICount] THEN

      //------------------------------------------------------------
      //  Make sure that this prompt is not currently being
      //  displayed.
      //------------------------------------------------------------

      IF i_MDICurMessage <> i_MDIStack[i_MDICount] THEN

         //---------------------------------------------------------
         //  Make sure there is an MDI Frame with a MicroHelp line.
         //---------------------------------------------------------

         IF PCCA.MDI_Frame_Valid THEN
            IF PCCA.MDI_Frame.WindowType = MDIHelp! THEN
               i_MDICurMessage = i_MDIStack[i_MDICount]
               PCCA.MDI_Frame.SetMicroHelp(i_MDICurMessage)
            END IF
         END IF
      END IF
   END IF
END IF

RETURN
end subroutine

public subroutine fu_visible (unsignedlong visible_option);//******************************************************************
//  PC Module     : n_PC_MDI_Main
//  Subroutine    : fu_Visible
//  Description   : Changes the visibility of the current MDI
//                  prompt.
//
//  Parameters    : UNSIGNEDLONG Visible_Option -
//                        The Visible_Option should be specified
//                        one of the following values:
//
//                           c_Invisible -
//                              Mark the current prompt as
//                              invisible so that if it gets
//                              overwritten on the display at
//                              a later time by another MDI
//                              prompt, it will not be
//                              re-displayed when that prompt
//                              is popped (i.e. the popped prompt
//                              will remain displayed on the
//                              MicroHelp line).
//
//                           c_Show -
//                              Put the prompt on the MDI prompt
//                              stack and display it.  Mark it
//                              as invisible so that if it gets
//                              overwritten on the display at
//                              a later time by another MDI prompt,
//                              it will not be re-displayed when
//                              that prompt is popped (i.e. the
//                              popped prompt will remain displayed
//                              on the MicroHelp line).
//
//                           c_Mark -
//                              Mark the current prompt as
//                              visible.  If it is not currently
//                              displayed it will not be
//                              re-displayed.  However, if at a
//                              later time it is overwritten by
//                              another prompt, it will be
//                              re-displayed when that prompt is
//                              popped.
//
//                           c_ShowMark -
//                              Mark the current prompt as visible
//                              and display it if it is not
//                              currently displayed.  If at a later
//                              time it is overwritten by another
//                              prompt, it will be re-displayed
//                              when that prompt is popped.
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

//------------------------------------------------------------------
//  Store whether the prompt is to be re-displayed if it gets
//  overwritten by another prompt.
//------------------------------------------------------------------

i_MDIVisible[i_MDICount] = (Visible_Option = c_Mark OR &
                            Visible_Option = c_ShowMark)

//------------------------------------------------------------------
//  Make sure redraw is on for the MicroHelp line.
//------------------------------------------------------------------

IF i_Redraw THEN

   //---------------------------------------------------------------
   //  See if the prompt is to be displayed immediately.
   //---------------------------------------------------------------

   IF Visible_Option = c_Show OR Visible_Option = c_ShowMark THEN

      //------------------------------------------------------------
      //  Make sure that this prompt is not currently being
      //  displayed.
      //------------------------------------------------------------

      IF i_MDICurMessage <> i_MDIStack[i_MDICount] THEN

         //---------------------------------------------------------
         //  Make sure there is an MDI Frame with a MicroHelp line.
         //---------------------------------------------------------

         IF PCCA.MDI_Frame_Valid THEN
            IF PCCA.MDI_Frame.WindowType = MDIHelp! THEN
               i_MDICurMessage = i_MDIStack[i_MDICount]
               PCCA.MDI_Frame.SetMicroHelp(i_MDICurMessage)
            END IF
         END IF
      END IF
   END IF
END IF

RETURN
end subroutine

public subroutine get_international ();fu_GetInternational()
end subroutine

public function long get_mdi_serial_id ();return fu_GetMDISerialId()
end function

public subroutine pop ();fu_Pop()
end subroutine

public subroutine pop_serial_id (long serial_id);fu_PopSerialID(Serial_Id)
end subroutine

public function long push (string prompt, unsignedlong visible_option);return fu_Push(Prompt, Visible_Option)
end function

public function long push_id (unsignedlong mdi_id, unsignedlong visible_option);return fu_PushID (MDI_ID, Visible_Option)
end function

public function long push_id_format (unsignedlong mdi_id, unsignedlong visible_option, integer num_numbers, real number_parms[], integer num_strings, string string_parms[]);return fu_PushIDFormat(MDI_ID, Visible_Option, Num_Numbers, Number_Parms[], Num_Strings, String_Parms[])
end function

public subroutine refresh ();fu_Refresh()
end subroutine

public subroutine setredraw (boolean on_or_off);fu_SetRedraw(on_or_off)
end subroutine

public subroutine visible (unsignedlong visible_option);fu_Visible(visible_option)
end subroutine

on constructor;//******************************************************************
//  PC Module     : n_PC_MDI_Main
//  Event         : Constructor
//  Description   : Initializes the MDI prompt stack and pushes
//                  the "Ready" prompt.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

UNSIGNEDLONG  l_Idx

//------------------------------------------------------------------
//  Set i_LastMDIID to -1.  This will cause the "Ready" prompt to
//  get a serial number of 0 - the only prompt that will be able
//  to do that.
//------------------------------------------------------------------

i_MDICount  = 0
i_LastMDIID = -1


c_MHPromptText[1] = 		"Undefined MicroHelp ID (%1d)."
c_MHPromptText[2] = 		"Please enter DBMS information..."
c_MHPromptText[3] = 		"Connecting to %1s database..."
c_MHPromptText[4] = 		"Appending a new record..."
c_MHPromptText[5] = 		"Deleting the selected record(s)..."
c_MHPromptText[6] = 		"Inserting a new record..."
c_MHPromptText[7] = 		"Putting record(s) into Modify mode..."
c_MHPromptText[8] = 		"Setting Query mode..."
c_MHPromptText[9] = 		"Refreshing records..."
c_MHPromptText[10] = 	"Retrieving records..."
c_MHPromptText[11] = 	"Re-Selecting records..."
c_MHPromptText[12] = 	"Scrolling to the first record..."
c_MHPromptText[13] = 	"Scrolling to the last record..."
c_MHPromptText[14] = 	"Scrolling to the next record..."
c_MHPromptText[15] = 	"Scrolling to the previous record..."
c_MHPromptText[16] = 	"Searching for records..."
c_MHPromptText[17] = 	"Filtering records..."
c_MHPromptText[18] = 	"Validating record(s)..."
c_MHPromptText[19] = 	"Putting record(s) into View mode..."
c_MHPromptText[20] = 	"Attempting to close..."
c_MHPromptText[21] = 	"Opening a new window..."
c_MHPromptText[22] = 	"Printing..."
c_MHPromptText[23] = 	"Saving changes..."
c_MHPromptText[24] = 	"Saving rows as..." 

c_MHPromptEnabled[1] = 	TRUE
c_MHPromptEnabled[2] = 	TRUE
c_MHPromptEnabled[3] = 	TRUE
c_MHPromptEnabled[4] = 	TRUE
c_MHPromptEnabled[5] = 	TRUE
c_MHPromptEnabled[6] = 	TRUE
c_MHPromptEnabled[7] = 	TRUE
c_MHPromptEnabled[8] = 	TRUE
c_MHPromptEnabled[9] = 	TRUE
c_MHPromptEnabled[10] = TRUE
c_MHPromptEnabled[11] = TRUE
c_MHPromptEnabled[12] = FALSE
c_MHPromptEnabled[13] = FALSE
c_MHPromptEnabled[14] = FALSE
c_MHPromptEnabled[15] = FALSE
c_MHPromptEnabled[16] = TRUE
c_MHPromptEnabled[17] = TRUE
c_MHPromptEnabled[18] = TRUE
c_MHPromptEnabled[19] = TRUE
c_MHPromptEnabled[20] = TRUE
c_MHPromptEnabled[21] = TRUE
c_MHPromptEnabled[22] = TRUE
c_MHPromptEnabled[23] = TRUE
c_MHPromptEnabled[24] = TRUE

c_ColorNames[1] = 	"Black"
c_ColorNames[2] = 	"White"
c_ColorNames[3] = 	"Gray"
c_ColorNames[4] = 	"Dark Gray"
c_ColorNames[5] = 	"Red"
c_ColorNames[6] = 	"Dark Red"
c_ColorNames[7] = 	"Green"
c_ColorNames[8] = 	"Dark Green"
c_ColorNames[9] = 	"Blue"
c_ColorNames[10] = 	"Dark Blue"
c_ColorNames[11] = 	"Magenta"
c_ColorNames[12] = 	"Dark Magenta"
c_ColorNames[13] = 	"Cyan"
c_ColorNames[14] = 	"Dark Cyan"
c_ColorNames[15] = 	"Yellow"
c_ColorNames[16] = 	"Brown" 

c_DW_BorderNames[1] = "No Border"
c_DW_BorderNames[2] = "Shadow Box"
c_DW_BorderNames[3] = "Box"
c_DW_BorderNames[4] = "Resize"
c_DW_BorderNames[5] = "Underline"
c_DW_BorderNames[6] = "Lowered"
c_DW_BorderNames[7] = "Raised"

c_DW_Borders[1] = "0"
c_DW_Borders[2] = "1"
c_DW_Borders[3] = "2"
c_DW_Borders[4] = "3"
c_DW_Borders[5] = "4"
c_DW_Borders[6] = "5"
c_DW_Borders[7] = "6"
	
//------------------------------------------------------------------
//  Initialize the RGB color values.
//------------------------------------------------------------------

c_RGBValues[c_Black]       = RGB(  0,   0,   0)
c_RGBValues[c_White]       = RGB(255, 255, 255)
c_RGBValues[c_Gray]        = RGB(192, 192, 192)
c_RGBValues[c_DarkGray]    = RGB(128, 128, 128)
c_RGBValues[c_Red]         = RGB(255,   0,   0)
c_RGBValues[c_DarkRed]     = RGB(128,   0,   0)
c_RGBValues[c_Green]       = RGB(  0, 255,   0)
c_RGBValues[c_DarkGreen]   = RGB(  0, 128,   0)
c_RGBValues[c_Blue]        = RGB(  0,   0, 255)
c_RGBValues[c_DarkBlue]    = RGB(  0,   0, 128)
c_RGBValues[c_Magenta]     = RGB(255,   0, 255)
c_RGBValues[c_DarkMagenta] = RGB(128,   0, 128)
c_RGBValues[c_Cyan]        = RGB(  0, 255, 255)
c_RGBValues[c_DarkCyan]    = RGB(  0, 128, 128)
c_RGBValues[c_Yellow]      = RGB(255, 255,   0)
c_RGBValues[c_Brown]       = RGB(128, 128,   0)

FOR l_Idx = c_ColorFirst TO c_ColorLast
   c_RGBStrings[l_Idx] = String(c_RGBValues[l_Idx])
NEXT

//------------------------------------------------------------------
//  Push the "Ready" prompt.
//------------------------------------------------------------------

fu_Push(PCCA.Application_Obj.MicroHelpDefault, c_ShowMark)
end on

event destructor;//******************************************************************
//  PC Module     : n_PC_MDI_Main
//  Event         : Destructor
//  Description   : Performs sanity checking when the MDI object
//                  is destroyed.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

IF i_MDICount <> 1 THEN
	//DOnato 12/12/2008
	//commentato alert perchè dà impressione di errore in chiusura
	//modifica provvisoria in attesa che qualche anima pia prenda in carico seriamente la cosa
	
	/*
	MessageBox(PCCA.Application_Name,                          &
              "n_PC_MDI_Main::Destructor error:~n" +          &
              "      All MicroHelp prompts were not popped.", &
              Exclamation!, Ok!, 1)
	*/
	
	  //fine modifica------------
END IF
end event

on n_pc_mdi_main.create
call super::create
TriggerEvent( this, "constructor" )
end on

on n_pc_mdi_main.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

