﻿$PBExportHeader$uo_cb_close.sru
$PBExportComments$Command button for closing a window
forward
global type uo_cb_close from uo_cb_main
end type
end forward

global type uo_cb_close from uo_cb_main
string Text="&Close"
end type
global uo_cb_close uo_cb_close

on constructor;call uo_cb_main::constructor;//******************************************************************
//  PC Module     : uo_CB_Close
//  Event         : Constructor
//  Description   : Sets up the command button.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

i_ButtonType = PCCA.MDI.c_CloseCB
i_ObjectType = "uo_CB_Close"
i_TrigEvent  = "Close"

////------------------------------------------------------------------
////  If you want this button to operate on a specific DataWindow,
////  then use the following lines of code to wire it.  You can
////  also make the Wire_Button call in the pc_SetWindow event of
////  w_Main if you want to keep the initialization of all the
////  window objects in the same place.
////------------------------------------------------------------------
////  NOTE: Generally, the developer will not want to wire the
////        Close button to a sepcific DataWindow.
////------------------------------------------------------------------
//
//IF IsValid(<window>.<DataWindow>) THEN
//   <window>.<DataWindow>.Wire_Button(THIS)
//END IF
end on

