﻿$PBExportHeader$uo_cb_filter.sru
$PBExportComments$Command button for filtering data
forward
global type uo_cb_filter from uo_cb_main
end type
end forward

global type uo_cb_filter from uo_cb_main
string Text="Fil&ter"
end type
global uo_cb_filter uo_cb_filter

on constructor;call uo_cb_main::constructor;//******************************************************************
//  PC Module     : uo_CB_Filter
//  Event         : Constructor
//  Description   : Sets up the command button.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

i_ButtonType = PCCA.MDI.c_FilterCB
i_ObjectType = "uo_CB_Filter"
i_TrigEvent  = "Filter"

////------------------------------------------------------------------
////  If you want this button to operate on a specific DataWindow,
////  then use the following lines of code to wire it.  You can
////  also make the Wire_Button call in the pc_SetWindow event of
////  w_Main if you want to keep the initialization of all the
////  window objects in the same place.
////------------------------------------------------------------------
//
//IF IsValid(<window>.<DataWindow>) THEN
//   <window>.<DataWindow>.Wire_Button(THIS)
//END IF
end on

