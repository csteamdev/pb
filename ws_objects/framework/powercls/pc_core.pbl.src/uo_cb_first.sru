﻿$PBExportHeader$uo_cb_first.sru
$PBExportComments$Command button for scrolling to the first row
forward
global type uo_cb_first from uo_cb_main
end type
end forward

global type uo_cb_first from uo_cb_main
int Width=161
string Text="|<--"
end type
global uo_cb_first uo_cb_first

on constructor;call uo_cb_main::constructor;//******************************************************************
//  PC Module     : uo_CB_First
//  Event         : Constructor
//  Description   : Sets up the command button.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

i_ButtonType = PCCA.MDI.c_FirstCB
i_ObjectType = "uo_CB_First"
i_TrigEvent  = "First"

////------------------------------------------------------------------
////  If you want this button to operate on a specific DataWindow,
////  then use the following lines of code to wire it.  You can
////  also make the Wire_Button call in the pc_SetWindow event of
////  w_Main if you want to keep the initialization of all the
////  window objects in the same place.
////------------------------------------------------------------------
//
//IF IsValid(<window>.<DataWindow>) THEN
//   <window>.<DataWindow>.Wire_Button(THIS)
//END IF
end on

