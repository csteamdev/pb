﻿$PBExportHeader$uo_cb_main.sru
$PBExportComments$Ancestor command button
forward
global type uo_cb_main from commandbutton
end type
end forward

global type uo_cb_main from commandbutton
int Width=311
int Height=109
int TabOrder=1
end type
global uo_cb_main uo_cb_main

type variables
//------------------------------------------------------------------------------
//  Instance Variables in sorted order.
//------------------------------------------------------------------------------

UNSIGNEDLONG	i_ButtonType

STRING		i_ClassName

STRING		i_ObjectType 	= "uo_CB_Main"

STRING		i_TrigEvent
UO_DW_MAIN	i_TrigObject

WINDOW		i_Window

end variables

on destructor;//******************************************************************
//  PC Module     : uo_CB_Main
//  Event         : Destructor
//  Description   : Tears down the command button.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

IF NOT IsNull(i_TrigObject) THEN
   IF i_TrigObject.i_Window <> i_Window THEN
      i_TrigObject.Unwire_Button(THIS)
   END IF
END IF
end on

on constructor;//******************************************************************
//  PC Module     : uo_CB_Main
//  Event         : Constructor
//  Description   : Initializes the command button.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

UO_CONTAINER_MAIN  l_Container

IF PARENT.TypeOf() = UserObject! THEN
   l_Container = PARENT
   i_Window    = l_Container.i_Window      
ELSE
   i_Window = PARENT
END IF

i_ButtonType = PCCA.MDI.c_MainCB
i_ClassName  = i_Window.ClassName() + "." + ClassName()
Tag          = "PowerClass Control"

SetNull(i_TrigObject)
end on

on clicked;//******************************************************************
//  PC Module     : uo_CB_Main
//  Event         : Clicked
//  Description   : Command button to trigger a PowerClass event.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

STRING  l_TrigEvent

//------------------------------------------------------------------
//  Trigger the event!
//------------------------------------------------------------------

IF IsNull(i_TrigObject) THEN
   IF IsValid(PCCA.Window_Current) THEN
      l_TrigEvent = "pc_" + i_TrigEvent
      PCCA.Window_Current.TriggerEvent(l_TrigEvent)
   END IF
ELSE
   IF IsValid(i_TrigObject) THEN
      l_TrigEvent = "pcd_" + i_TrigEvent
      i_TrigObject.TriggerEvent(l_TrigEvent)
   END IF
END IF
end on

