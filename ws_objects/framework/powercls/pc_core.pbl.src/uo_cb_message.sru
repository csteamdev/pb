﻿$PBExportHeader$uo_cb_message.sru
$PBExportComments$Command button for viewing/modifying text in a window
forward
global type uo_cb_message from commandbutton
end type
end forward

global type uo_cb_message from commandbutton
int Width=311
int Height=109
int TabOrder=1
string Text="Messa&ge"
end type
global uo_cb_message uo_cb_message

type variables
//------------------------------------------------------------------------------
//  Instance Variables in sorted order.
//------------------------------------------------------------------------------

STRING		i_ClassName

STRING		i_MessageColumn
UO_DW_MAIN	i_MessageDW
STRING		i_MessageTitle

STRING		i_ObjectType 	= "uo_CB_Message"

WINDOW		i_Window

end variables

on constructor;//******************************************************************
//  PC Module     : uo_CB_Message
//  Event         : Constructor
//  Description   : Initializes the message button.
//
//  Note: Use the uo_DW_Main::Wire_Message() routine to wire
//        the message button to the DataWindow.  Example:
//
//           <DataWindow>.Wire_Message(<Message_Button>, &
//                                     "<DW_Column>",    &
//                                     "<Title>")
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

UO_CONTAINER_MAIN  l_Container

IF PARENT.TypeOf() = UserObject! THEN
   l_Container = PARENT
   i_Window    = l_Container.i_Window      
ELSE
   i_Window = PARENT
END IF

i_ClassName  = i_Window.ClassName() + "." + ClassName()
Enabled      = FALSE

SetNull(i_MessageDW)
end on

on clicked;//******************************************************************
//  PC Module     : uo_CB_Message
//  Event         : Clicked
//  Description   : Command button to open a window to view or
//                  modify a message or comment.
//
//  Note: Use the uo_DW_Main::Wire_Message() routine to wire
//        the message button to the DataWindow.  Example:
//
//           <DataWindow>.Wire_Message(<Message_Button>, &
//                                     "<DW_Column>",    &
//                                     "<Title>")
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN  l_DisplayOnly, l_TextChanged
INTEGER  l_ColNbr,      l_Idx, l_Jdx
LONG     l_TextColor,   l_BackColor
STRING   l_Text

//------------------------------------------------------------------
//  Make sure our DataWindow is valid.
//------------------------------------------------------------------

IF IsValid(i_MessageDW) THEN
ELSE
   GOTO Finished
END IF

i_MessageDW.TriggerEvent("pcd_Active")

//------------------------------------------------------------------
//  Make sure that we have a valid row.
//------------------------------------------------------------------

IF i_MessageDW.i_ShowRow < 1 THEN
   MessageBox(PCCA.Application_Name,             &
              "There is not a record selected.", &
              Information!, Ok!)
   GOTO Finished
END IF

//------------------------------------------------------------------
//  Get the text to display from the selected DataWindow.
//------------------------------------------------------------------

l_Text = i_MessageDW.GetItemString( &
                       i_MessageDW.i_ShowRow, i_MessageColumn)

//------------------------------------------------------------------
//  If the selected DataWindow is in VIEW mode then open the
//  message window in display-only mode.  Pass the background
//  color in the fifth parameter.
//------------------------------------------------------------------

l_ColNbr     = i_MessageDW.Column_Nbr(i_MessageColumn)
l_TextColor  = Long(i_MessageDW.i_ModifyTextColor)

IF l_ColNbr > 0 THEN
   IF i_MessageDW.i_ColColors[l_ColNbr] <> -1 THEN
      l_TextColor = i_MessageDW.i_ColColors[l_ColNbr]
   END IF
END IF

IF i_MessageDW.i_CurrentMode = i_MessageDW.c_ModeView THEN
   IF Len(i_MessageDW.i_ViewModeColor) > 0 THEN
      l_BackColor = Long(i_MessageDW.i_ViewModeColor)
   ELSE
      l_BackColor = Long(i_MessageDW.i_ModifyModeColor)
      IF l_ColNbr > 0 THEN
         IF i_MessageDW.i_ColBColors[l_ColNbr] <> -1 THEN
            l_BackColor = i_MessageDW.i_ColBColors[l_ColNbr]
         END IF
      END IF
   END IF
   l_DisplayOnly = TRUE
ELSE
   l_BackColor = Long(i_MessageDW.i_ModifyModeColor)
   IF l_ColNbr > 0 THEN
      IF i_MessageDW.i_ColBColors[l_ColNbr] <> -1 THEN
         l_BackColor = i_MessageDW.i_ColBColors[l_ColNbr]
      END IF
   END IF
   l_DisplayOnly = FALSE
END IF

//------------------------------------------------------------------
//  Open the message window.
//------------------------------------------------------------------

l_TextChanged = f_PO_Message(i_MessageTitle, &
                             l_Text, &
                             l_DisplayOnly, &
                             l_BackColor, &
                             l_TextColor)

//------------------------------------------------------------------
//  If the returned text has changed, put the text back into the
//  DataWindow.
//------------------------------------------------------------------

IF l_TextChanged THEN
   i_MessageDW.SetItem(i_MessageDW.i_ShowRow, &
                       i_MessageColumn, l_Text)
END IF

Finished:
end on

on destructor;//******************************************************************
//  PC Module     : uo_CB_Message
//  Event         : Destructor
//  Description   : Tears down the message button.
//
//  Note: Use the uo_DW_Main::Wire_Message() routine to wire
//        the message button to the DataWindow.  Example:
//
//           <DataWindow>.Wire_Message(<Message_Button>, &
//                                     "<DW_Column>",    &
//                                     "<Title>")
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

IF NOT IsNull(i_MessageDW) THEN
   IF i_MessageDW.i_Window <> i_Window THEN
      i_MessageDW.Unwire_Message(THIS)
   END IF
END IF
end on

