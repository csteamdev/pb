﻿$PBExportHeader$uo_cb_ok.sru
$PBExportComments$Command button for accepting an operation
forward
global type uo_cb_ok from uo_cb_main
end type
end forward

global type uo_cb_ok from uo_cb_main
string Text="&OK"
end type
global uo_cb_ok uo_cb_ok

on constructor;call uo_cb_main::constructor;//******************************************************************
//  PC Module     : uo_CB_Ok
//  Event         : Constructor
//  Description   : Sets up the command button.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

i_ButtonType = PCCA.MDI.c_AcceptCB
i_ObjectType = "uo_CB_Ok"
i_TrigEvent  = "Accept"

////------------------------------------------------------------------
////  If you want this button to operate on a specific DataWindow,
////  then use the following lines of code to wire it.  You can
////  also make the Wire_Button call in the pc_SetWindow event of
////  w_Main if you want to keep the initialization of all the
////  window objects in the same place.
////------------------------------------------------------------------
////  NOTE: PowerClass does not provide a "pcd_Accept" event on
////        the DataWindow.
////------------------------------------------------------------------

//IF IsValid(<window>.<DataWindow>) THEN
//   <window>.<DataWindow>.Wire_Button(THIS)
//END IF
end on

