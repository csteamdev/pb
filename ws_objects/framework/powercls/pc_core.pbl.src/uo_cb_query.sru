﻿$PBExportHeader$uo_cb_query.sru
$PBExportComments$Command button for putting DataWindow into query mode
forward
global type uo_cb_query from uo_cb_main
end type
end forward

global type uo_cb_query from uo_cb_main
string Text="&Query"
end type
global uo_cb_query uo_cb_query

on constructor;call uo_cb_main::constructor;//******************************************************************
//  PC Module     : uo_CB_Query
//  Event         : Constructor
//  Description   : Sets up the command button.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

i_ButtonType = PCCA.MDI.c_QueryCB
i_ObjectType = "uo_CB_Query"
i_TrigEvent  = "Query"

////------------------------------------------------------------------
////  If you want this button to operate on a specific DataWindow,
////  then use the following lines of code to wire it.  You can
////  also make the Wire_Button call in the pc_SetWindow event of
////  w_Main if you want to keep the initialization of all the
////  window objects in the same place.
////------------------------------------------------------------------
////  NOTE: Only the root-level DataWindows support "QUERY" mode.
////        The pcd_Query event of the DataWindow will make sure
////        that this event is routed to the root-level DataWindow.
////------------------------------------------------------------------
//
//IF IsValid(<window>.<DataWindow>) THEN
//   <window>.<DataWindow>.Wire_Button(THIS)
//END IF
end on

