﻿$PBExportHeader$uo_cb_resetquery.sru
$PBExportComments$Command button for resetting DataWindow Query-By-Example qualifications
forward
global type uo_cb_resetquery from uo_cb_main
end type
end forward

global type uo_cb_resetquery from uo_cb_main
int Width=471
string Text="Rese&t Query"
end type
global uo_cb_resetquery uo_cb_resetquery

on clicked;//******************************************************************
//  PC Module     : uo_CB_ResetQuery
//  Event         : Clicked
//  Description   : Command button to trigger a PowerClass event.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

INTEGER     l_Error
LONG        l_Idx, l_Jdx
UO_DW_MAIN  l_RootDW

IF IsNull(i_TrigObject) THEN
   l_RootDW = PCCA.Window_CurrentDW
ELSE
   l_RootDW = i_TrigObject
END IF

IF IsValid(l_RootDW) THEN

   IF l_RootDW.i_InUse THEN

      //------------------------------------------------------------
      //  "QUERY" should be on the root-level DataWindow...
      //------------------------------------------------------------

      l_RootDW = l_RootDW.i_RootDW

      //------------------------------------------------------------
      //  If the DataWindow allows "QUERY" mode, reset the
      //  query-by-example fields.
      //------------------------------------------------------------

      IF l_RootDW.i_AllowQuery THEN

         l_Error = l_RootDW.Check_DW_Save(l_RootDW.c_NoRefresh)

         IF l_Error = l_RootDW.c_Success THEN

            l_RootDW.Push_DW_Redraw(l_RootDW.c_BufferDraw)

            //------------------------------------------------------
            //  If the DataWindow is not in "QUERY" mode, we need
            //  to pop into it, reset the queries, and pop back
            //  out.
            //------------------------------------------------------

            IF l_RootDW.i_DWState <> l_RootDW.c_DWStateQuery THEN
               l_RootDW.Modify("DataWindow.QueryMode=Yes")
            END IF

            l_RootDW.DataObject = l_RootDW.DataObject
            l_RootDW.SetTransObject(l_RootDW.i_DBCA)

            //------------------------------------------------------
            //  If we were in "QUERY" mode, we need to get back
            //  in since the DataWindow object got clobbered.  If
            //  we were not in "QUERY" mode, we need to refresh
            //  the DataWindows since the search criteria has
            //  changed.
            //------------------------------------------------------

            IF l_RootDW.i_DWState <> l_RootDW.c_DWStateQuery THEN
               l_RootDW.TriggerEvent("pcd_Search")
            ELSE
               l_RootDW.Modify(l_RootDW.i_QueryTabs)

               IF l_RootDW.i_FirstVisColumn > 0 THEN
                  l_RootDW.SetColumn(l_RootDW.i_FirstVisColumn)
               END IF

               l_RootDW.Modify("DataWindow.QueryMode=Yes")

               l_RootDW.Change_DW_Focus(l_RootDW)
               l_RootDW.SetRowFocusIndicator(Off!)
            END IF

            l_RootDW.Pop_DW_Redraw()
         END IF
      END IF
   END IF
END IF
end on

on constructor;call uo_cb_main::constructor;//******************************************************************
//  PC Module     : uo_CB_ResetQuery
//  Event         : Constructor
//  Description   : Sets up the command button.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

i_ButtonType = PCCA.MDI.c_ResetQueryCB
i_ObjectType = "uo_CB_ResetQuery"
i_TrigEvent  = "Reset"

////------------------------------------------------------------------
////  If you want this button to operate on a specific DataWindow,
////  then use the following lines of code to wire it.  You can
////  also make the Wire_Button call in the pc_SetWindow event of
////  w_Main if you want to keep the initialization of all the
////  window objects in the same place.
////------------------------------------------------------------------
////  NOTE: Only the root-level DataWindows support "QUERY" mode.
////        The clicked event of this object will send the event
////        to i_TrigObject's root-level DataWindow.
////------------------------------------------------------------------
//
//IF IsValid(<window>.<DataWindow>) THEN
//   <window>.<DataWindow>.Wire_Button(THIS)
//END IF
end on

