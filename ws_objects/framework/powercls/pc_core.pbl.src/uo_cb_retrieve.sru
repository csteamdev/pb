﻿$PBExportHeader$uo_cb_retrieve.sru
$PBExportComments$Command button for starting data retrieval
forward
global type uo_cb_retrieve from uo_cb_main
end type
end forward

global type uo_cb_retrieve from uo_cb_main
string Text="&Retrieve"
end type
global uo_cb_retrieve uo_cb_retrieve

on constructor;call uo_cb_main::constructor;//******************************************************************
//  PC Module     : uo_CB_Retrieve
//  Event         : Constructor
//  Description   : Sets up the command button.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

i_ButtonType = PCCA.MDI.c_RetrieveCB
i_ObjectType = "uo_CB_Retrieve"
i_TrigEvent  = "Retrieve"

////------------------------------------------------------------------
////  If you want this button to operate on a specific DataWindow,
////  then use the following lines of code to wire it.  You can
////  also make the Wire_Button call in the pc_SetWindow event of
////  w_Main if you want to keep the initialization of all the
////  window objects in the same place.
////------------------------------------------------------------------
////  NOTE: The window must handle the processing of this event.
////        If this object is wired to a DataWindow, the
////        clicked event of this object will make i_TrigObject
////        the current DataWindow and trigger the event on
////        i_TrigObject's parent window.
////------------------------------------------------------------------
//
//IF IsValid(<window>.<DataWindow>) THEN
//   <window>.<DataWindow>.Wire_Button(THIS)
//END IF
end on

on clicked;//****************************************************************
//  PC Module     : uo_CB_Retrieve
//  Event         : Clicked
//  Description   : Command button to trigger a PowerClass event.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- ------------------------------------------
//
//****************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//****************************************************************

STRING  l_TrigEvent

//------------------------------------------------------------------
//  Trigger the event!
//------------------------------------------------------------------

IF IsNull(i_TrigObject) THEN
   IF IsValid(PCCA.Window_Current) THEN
      l_TrigEvent = "pc_" + i_TrigEvent
      PCCA.Window_Current.TriggerEvent(l_TrigEvent)
   END IF
ELSE
   IF IsValid(i_TrigObject) THEN

      //----------------------------------------------------------
      //  NOTE: The Retrieve button requires processing by the
      //        pc_Retrieve event on w_Main.  If it is wired to
      //        a DataWindow, then make the DataWindow current
      //        using pcd_Active.  Then, trigger the event on
      //        DataWindow's parent window.
      //----------------------------------------------------------

      i_TrigObject.TriggerEvent("pcd_Active")
      l_TrigEvent = "pc_" + i_TrigEvent
      i_TrigObject.i_Window.TriggerEvent(l_TrigEvent)
   END IF
END IF
end on

