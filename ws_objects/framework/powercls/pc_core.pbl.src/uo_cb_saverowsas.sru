﻿$PBExportHeader$uo_cb_saverowsas.sru
$PBExportComments$Command button to save DataWindow contents in a specific file format
forward
global type uo_cb_saverowsas from uo_cb_main
end type
end forward

global type uo_cb_saverowsas from uo_cb_main
int Width=485
string Text="Save Ro&ws As"
end type
global uo_cb_saverowsas uo_cb_saverowsas

on constructor;call uo_cb_main::constructor;//******************************************************************
//  PC Module     : uo_CB_SaveRowsAs
//  Event         : Constructor
//  Description   : Sets up the command button.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

i_ButtonType = PCCA.MDI.c_SaveRowsAsCB
i_ObjectType = "uo_CB_SaveRowsAs"
i_TrigEvent  = "SaveRowsAs"

////------------------------------------------------------------------
////  If you want this button to operate on a specific DataWindow,
////  then use the following lines of code to wire it.  You can
////  also make the Wire_Button call in the pc_SetWindow event of
////  w_Main if you want to keep the initialization of all the
////  window objects in the same place.
////------------------------------------------------------------------
//
//IF IsValid(<window>.<DataWindow>) THEN
//   <window>.<DataWindow>.Wire_Button(THIS)
//END IF
end on

