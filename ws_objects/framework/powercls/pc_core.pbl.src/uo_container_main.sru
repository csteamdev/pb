﻿$PBExportHeader$uo_container_main.sru
$PBExportComments$Ancestor container object to inherit from
forward
global type uo_container_main from UserObject
end type
end forward

global type uo_container_main from UserObject
int Width=1998
int Height=1181
boolean Border=true
long BackColor=12632256
event pc_close pbm_custom69
event pc_closequery pbm_custom70
event pc_open pbm_custom71
event pc_resize pbm_custom72
event pc_setddlb pbm_custom73
event pc_setvariables pbm_custom74
event pc_setwindow pbm_custom75
end type
global uo_container_main uo_container_main

type variables
//----------------------------------------------------------------------------
//  Instance Variables in sorted order.
//----------------------------------------------------------------------------

STRING		i_ClassName
INTEGER		i_ContHeight
INTEGER		i_ContWidth
INTEGER		i_ContX
INTEGER		i_ContY
BOOLEAN	i_CT_BringToTop
UNSIGNEDLONG	i_CT_ControlWord
BOOLEAN	i_CT_LoadCodeTable
BOOLEAN	i_CT_ResizeCont
UNSIGNEDLONG	i_CT_SaveOnClose
BOOLEAN	i_CT_ZoomCont

BOOLEAN	i_DisplayError
UO_DW_MAIN	i_DWs[]

BOOLEAN	i_IsStatic

MENU		i_mPopup

INTEGER		i_NumDWs

STRING		i_ObjectType                    = "uo_Container_Main"
POWEROBJECT	i_OpenParm
BOOLEAN	i_OptionsInit

INTEGER		i_PO_Controls[]

WINDOW		i_Window
UNSIGNEDLONG	i_WindowID
W_MAIN		i_WindowMain
W_RESPONSE	i_WindowResp

//----------------------------------------------------------------------------
//  Constant Variables in logical order.
//----------------------------------------------------------------------------

UNSIGNEDLONG    c_MasterList
UNSIGNEDLONG    c_MasterEdit
UNSIGNEDLONG    c_DetailList
UNSIGNEDLONG    c_DetailEdit

UNSIGNEDLONG    c_ScrollSelf
UNSIGNEDLONG    c_ScrollParent

UNSIGNEDLONG    c_IsNotInstance
UNSIGNEDLONG    c_IsInstance

UNSIGNEDLONG    c_RequiredOnSave
UNSIGNEDLONG    c_AlwaysCheckRequired

UNSIGNEDLONG    c_NoNew
UNSIGNEDLONG    c_NewOk

UNSIGNEDLONG    c_NoModify
UNSIGNEDLONG    c_ModifyOk

UNSIGNEDLONG    c_NoDelete
UNSIGNEDLONG    c_DeleteOk

UNSIGNEDLONG    c_NoQuery
UNSIGNEDLONG    c_QueryOk

UNSIGNEDLONG    c_SelectOnDoubleClick
UNSIGNEDLONG    c_SelectOnClick
UNSIGNEDLONG    c_SelectOnRowFocusChange

UNSIGNEDLONG    c_NoDrillDown
UNSIGNEDLONG    c_DrillDown

UNSIGNEDLONG    c_SameModeOnSelect
UNSIGNEDLONG    c_ViewOnSelect
UNSIGNEDLONG    c_ModifyOnSelect

UNSIGNEDLONG    c_NoMultiSelect
UNSIGNEDLONG    c_MultiSelect

UNSIGNEDLONG    c_RefreshOnSelect
UNSIGNEDLONG    c_NoAutoRefresh
UNSIGNEDLONG    c_RefreshOnMultiSelect

UNSIGNEDLONG    c_ParentModeOnOpen
UNSIGNEDLONG    c_ViewOnOpen
UNSIGNEDLONG    c_ModifyOnOpen
UNSIGNEDLONG    c_NewOnOpen

UNSIGNEDLONG    c_SameModeAfterSave
UNSIGNEDLONG    c_ViewAfterSave

UNSIGNEDLONG    c_NoEnableModifyOnOpen
UNSIGNEDLONG    c_EnableModifyOnOpen

UNSIGNEDLONG    c_NoEnableNewOnOpen
UNSIGNEDLONG    c_EnableNewOnOpen

UNSIGNEDLONG    c_RetrieveOnOpen
UNSIGNEDLONG    c_AutoRetrieveOnOpen // Same as c_RetrieveOnOpen
UNSIGNEDLONG    c_NoRetrieveOnOpen

UNSIGNEDLONG    c_AutoRefresh
UNSIGNEDLONG    c_SkipRefresh
UNSIGNEDLONG    c_TriggerRefresh
UNSIGNEDLONG    c_PostRefresh

UNSIGNEDLONG    c_RetrieveAll
UNSIGNEDLONG    c_RetrieveAsNeeded

UNSIGNEDLONG    c_NoShareData
UNSIGNEDLONG    c_ShareData

UNSIGNEDLONG    c_NoRefreshParent
UNSIGNEDLONG    c_RefreshParent

UNSIGNEDLONG    c_NoRefreshChild
UNSIGNEDLONG    c_RefreshChild

UNSIGNEDLONG    c_IgnoreNewRows
UNSIGNEDLONG    c_NoIgnoreNewRows

UNSIGNEDLONG    c_NoNewModeOnEmpty
UNSIGNEDLONG    c_NewModeOnEmpty

UNSIGNEDLONG    c_MultipleNewRows
UNSIGNEDLONG    c_OnlyOneNewRow

UNSIGNEDLONG    c_DisableCC
UNSIGNEDLONG    c_EnableCC

UNSIGNEDLONG    c_DisableCCInsert
UNSIGNEDLONG    c_EnableCCInsert

UNSIGNEDLONG    c_ViewModeBorderUnchanged
UNSIGNEDLONG    c_ViewModeNoBorder
UNSIGNEDLONG    c_ViewModeShadowBox
UNSIGNEDLONG    c_ViewModeBox
UNSIGNEDLONG    c_ViewModeResize
UNSIGNEDLONG    c_ViewModeUnderline
UNSIGNEDLONG    c_ViewModeLowered
UNSIGNEDLONG    c_ViewModeRaised

UNSIGNEDLONG    c_ViewModeColorUnchanged
UNSIGNEDLONG    c_ViewModeBlack
UNSIGNEDLONG    c_ViewModeWhite
UNSIGNEDLONG    c_ViewModeGray
UNSIGNEDLONG    c_ViewModeLightGray // Same as c_ViewModeGray
UNSIGNEDLONG    c_ViewModeDarkGray
UNSIGNEDLONG    c_ViewModeRed
UNSIGNEDLONG    c_ViewModeDarkRed
UNSIGNEDLONG    c_ViewModeGreen
UNSIGNEDLONG    c_ViewModeDarkGreen
UNSIGNEDLONG    c_ViewModeBlue
UNSIGNEDLONG    c_ViewModeDarkBlue
UNSIGNEDLONG    c_ViewModeMagenta
UNSIGNEDLONG    c_ViewModeDarkMagenta
UNSIGNEDLONG    c_ViewModeCyan
UNSIGNEDLONG    c_ViewModeDarkCyan
UNSIGNEDLONG    c_ViewModeYellow
UNSIGNEDLONG    c_ViewModeBrown

UNSIGNEDLONG    c_InactiveDWColorUnchanged
UNSIGNEDLONG    c_InactiveDWBlack
UNSIGNEDLONG    c_InactiveDWWhite
UNSIGNEDLONG    c_InactiveDWGray
UNSIGNEDLONG    c_InactiveDWLightGray // Same as c_InActiveDWGray
UNSIGNEDLONG    c_InactiveDWDarkGray
UNSIGNEDLONG    c_InactiveDWRed
UNSIGNEDLONG    c_InactiveDWDarkRed
UNSIGNEDLONG    c_InactiveDWGreen
UNSIGNEDLONG    c_InactiveDWDarkGreen
UNSIGNEDLONG    c_InactiveDWBlue
UNSIGNEDLONG    c_InactiveDWDarkBlue
UNSIGNEDLONG    c_InactiveDWMagenta
UNSIGNEDLONG    c_InactiveDWDarkMagenta
UNSIGNEDLONG    c_InactiveDWCyan
UNSIGNEDLONG    c_InactiveDWDarkCyan
UNSIGNEDLONG    c_InactiveDWYellow
UNSIGNEDLONG    c_InactiveDWBrown

UNSIGNEDLONG    c_InactiveTextLineCol
UNSIGNEDLONG    c_NoInactiveText
UNSIGNEDLONG    c_InactiveText
UNSIGNEDLONG    c_NoInactiveLine
UNSIGNEDLONG    c_InactiveLine
UNSIGNEDLONG    c_NoInactiveCol
UNSIGNEDLONG    c_InactiveCol

UNSIGNEDLONG    c_CursorRowFocusRect
UNSIGNEDLONG    c_NoCursorRowFocusRect

UNSIGNEDLONG    c_CursorRowPointer
UNSIGNEDLONG    c_NoCursorRowPointer

UNSIGNEDLONG    c_CalcDWStyle
UNSIGNEDLONG    c_FreeFormStyle
UNSIGNEDLONG    c_TabularFormStyle

UNSIGNEDLONG    c_AutoCalcHighlightSelected
UNSIGNEDLONG    c_HighlightSelected
UNSIGNEDLONG    c_NoHighlightSelected

UNSIGNEDLONG    c_NoResizeDW
UNSIGNEDLONG    c_ResizeDW

UNSIGNEDLONG    c_NoAutoConfigMenus
UNSIGNEDLONG    c_AutoConfigMenus

UNSIGNEDLONG    c_ShowEmpty
UNSIGNEDLONG    c_NoShowEmpty

UNSIGNEDLONG    c_AutoFocus
UNSIGNEDLONG    c_NoAutoFocus

UNSIGNEDLONG    c_CCErrorRed
UNSIGNEDLONG    c_CCErrorBlack
UNSIGNEDLONG    c_CCErrorWhite
UNSIGNEDLONG    c_CCErrorGray
UNSIGNEDLONG    c_CCErrorLightGray // Same as c_InActiveDWGray
UNSIGNEDLONG    c_CCErrorDarkGray
UNSIGNEDLONG    c_CCErrorDarkRed
UNSIGNEDLONG    c_CCErrorGreen
UNSIGNEDLONG    c_CCErrorDarkGreen
UNSIGNEDLONG    c_CCErrorBlue
UNSIGNEDLONG    c_CCErrorDarkBlue
UNSIGNEDLONG    c_CCErrorMagenta
UNSIGNEDLONG    c_CCErrorDarkMagenta
UNSIGNEDLONG    c_CCErrorCyan
UNSIGNEDLONG    c_CCErrorDarkCyan
UNSIGNEDLONG    c_CCErrorYellow
UNSIGNEDLONG    c_CCErrorBrown

UNSIGNEDLONG    c_CT_LoadCodeTable
UNSIGNEDLONG    c_CT_NoLoadCodeTable

UNSIGNEDLONG    c_CT_NoResizeCont
UNSIGNEDLONG    c_CT_ResizeCont
UNSIGNEDLONG    c_CT_ZoomCont

UNSIGNEDLONG    c_CT_ClosePromptUser
UNSIGNEDLONG    c_CT_ClosePromptUserOnce
UNSIGNEDLONG    c_CT_CloseSave
UNSIGNEDLONG    c_CT_CloseNoSave

UNSIGNEDLONG    c_CT_BringToTop
UNSIGNEDLONG    c_CT_NoBringToTop

INTEGER		c_Fatal			= -1
INTEGER		c_Success		= 0
UNSIGNEDLONG	c_Default			= 0

UO_DW_MAIN	c_NullDW
UO_CB_MAIN	c_NullCB
MENU		c_NullMenu
PICTURE		c_NullPicture

STRING		c_INIUndefined		= "[INI_Undefined]"
STRING          	c_XKey			= "X"
STRING          	c_YKey			= "Y"
STRING          	c_WidthKey		= "Width"
STRING          	c_HeightKey		= "Height"
STRING          	c_WinStateKey		= "WindowState"
INTEGER		c_MinimumOverlap		= 25

UNSIGNEDLONG    c_SOCPromptUser		= 100
UNSIGNEDLONG    c_SOCPromptUserOnce	= 101
UNSIGNEDLONG    c_SOCSave		= 102
UNSIGNEDLONG    c_SOCNoSave		= 103

UNSIGNEDLONG    c_Invisible		= 0
UNSIGNEDLONG    c_Show			= 1
UNSIGNEDLONG    c_Mark			= 2
UNSIGNEDLONG    c_ShowMark		= 3

UNSIGNEDLONG    c_ColorUndefined                = 0
UNSIGNEDLONG    c_Black                         	= 1
UNSIGNEDLONG    c_White                         	= 2
UNSIGNEDLONG    c_Gray                  	= 3
UNSIGNEDLONG    c_DarkGray              	= 4
UNSIGNEDLONG    c_Red                   	= 5
UNSIGNEDLONG    c_DarkRed              	= 6
UNSIGNEDLONG    c_Green                         	= 7
UNSIGNEDLONG    c_DarkGreen             	= 8
UNSIGNEDLONG    c_Blue                  	= 9 
UNSIGNEDLONG    c_DarkBlue              	= 10
UNSIGNEDLONG    c_Magenta               	= 11
UNSIGNEDLONG    c_DarkMagenta           	= 12
UNSIGNEDLONG    c_Cyan                  	= 13
UNSIGNEDLONG    c_DarkCyan              	= 14
UNSIGNEDLONG    c_Yellow                        	= 15
UNSIGNEDLONG    c_Brown                         	= 16

UNSIGNEDLONG    c_ColorFirst            	= 1
UNSIGNEDLONG    c_ColorLast             	= 16

UNSIGNEDLONG    c_DW_BorderUndefined    	= 0
UNSIGNEDLONG    c_DW_BorderNone         	= 1
UNSIGNEDLONG    c_DW_BorderShadowBox    	= 2
UNSIGNEDLONG    c_DW_BorderBox          	= 3
UNSIGNEDLONG    c_DW_BorderResize       	= 4
UNSIGNEDLONG    c_DW_BorderUnderLine    	= 5
UNSIGNEDLONG    c_DW_BorderLowered      	= 6
UNSIGNEDLONG    c_DW_BorderRaised       	= 7

UNSIGNEDLONG    c_DW_BorderFirst		= 1
UNSIGNEDLONG    c_DW_BorderLast         	= 7

INTEGER         c_AcceptCB              		= 1
INTEGER         c_CancelCB              		= 2
INTEGER         c_CloseCB               		= 3
INTEGER         c_DeleteCB              		= 4
INTEGER         c_FilterCB              		= 5
INTEGER         c_FirstCB               		= 6
INTEGER         c_InsertCB              		= 7
INTEGER         c_LastCB                		= 8
INTEGER         c_MainCB                		= 9
INTEGER         c_ModifyCB              		= 10
INTEGER         c_NewCB                 		= 11
INTEGER         c_NextCB                		= 12
INTEGER         c_PreviousCB            		= 13
INTEGER         c_PrintCB               		= 14
INTEGER         c_QueryCB               		= 15
INTEGER         c_ResetQueryCB          	= 16
INTEGER         c_RetrieveCB            		= 17
INTEGER         c_SaveCB                		= 18
INTEGER         c_SaveRowsAsCB          	= 19
INTEGER         c_SearchCB              		= 20
INTEGER         c_ViewCB                		= 21

UNSIGNEDLONG    c_ModeUndefined		= 0
UNSIGNEDLONG    c_ModeView              	= 100
UNSIGNEDLONG    c_ModeModify            	= 101
UNSIGNEDLONG    c_ModeNew               	= 102

UNSIGNEDLONG    c_DWStateUndefined      	= 0
UNSIGNEDLONG    c_DWStateEnabled       	= 200
UNSIGNEDLONG    c_DWStateDisabled       	= 201
UNSIGNEDLONG    c_DWStateQuery          	= 202

UNSIGNEDLONG    c_ObjTypeUndefined      	= 0
UNSIGNEDLONG    c_ObjTypeColumn		= 300
UNSIGNEDLONG    c_ObjTypeText           	= 301
UNSIGNEDLONG    c_ObjTypeLine           	= 302

UNSIGNEDLONG    c_ColTypeUndefined      	= 0
UNSIGNEDLONG    c_ColTypeString                 	= 400
UNSIGNEDLONG    c_ColTypeDate           	= 401
UNSIGNEDLONG    c_ColTypeDateTime       	= 402
UNSIGNEDLONG    c_ColTypeDecimal		= 403
UNSIGNEDLONG    c_ColTypeNumber		= 404
UNSIGNEDLONG    c_ColTypeTime           	= 405
UNSIGNEDLONG    c_ColTypeTimeStamp      	= 406
UNSIGNEDLONG    c_ColTypeBlob           	= 407

UNSIGNEDLONG    c_RFC_Undefined		= 0
UNSIGNEDLONG    c_RFC_NoSelect          	= 500
UNSIGNEDLONG    c_RFC_DoubleClicked     	= 501
UNSIGNEDLONG    c_RFC_Clicked           	= 502
UNSIGNEDLONG    c_RFC_RowFocusChanged   = 503

UNSIGNEDLONG    c_RFC_RefreshOnSelect   	= 600
UNSIGNEDLONG    c_RFC_NoAutoRefresh     	= 601
UNSIGNEDLONG    c_RFC_RefreshOnMultiSelect= 602

UNSIGNEDLONG    c_AutoRefreshOnOpen     	= 700
UNSIGNEDLONG    c_SkipRefreshOnOpen     	= 701
UNSIGNEDLONG    c_TriggerRefreshOnOpen  	= 702
UNSIGNEDLONG    c_PostRefreshOnOpen     	= 703

UNSIGNEDLONG    c_ShareNone             	= 800
UNSIGNEDLONG    c_ShareEnabled          	= 801

UNSIGNEDLONG    c_No                    	= 900
UNSIGNEDLONG    c_Yes                   	= 901

UNSIGNEDLONG    c_ControlUndefined      	= 0
UNSIGNEDLONG    c_ControlActiveDW       	= 1000
UNSIGNEDLONG    c_ControlRows           	= 1001
UNSIGNEDLONG    c_ControlMainMenu       	= 1002
UNSIGNEDLONG    c_ControlPopupMenu      	= 1003

LONG            	c_DoNew                 	= 1100
UNSIGNEDINT	c_NewAppend             	= 1101
UNSIGNEDINT	c_NewInsert             	= 1102

UNSIGNEDLONG    c_AcceptProcessErrors   	= 1200
UNSIGNEDLONG    c_AcceptIgnoreErrors    	= 1201

UNSIGNEDLONG    c_GetCurrentValues      	= 1310
UNSIGNEDLONG    c_GetOriginalValues     	= 1301

UNSIGNEDLONG    c_CascadeFirst          	= 1400
UNSIGNEDLONG    c_CascadeLast           	= 1401

UNSIGNEDLONG    c_CascadePost           	= 1500
UNSIGNEDLONG    c_CascadeTrigger		= 1501

UNSIGNEDLONG    c_CheckTree             	= 1600
UNSIGNEDLONG    c_CheckChildren                 = 1601
UNSIGNEDLONG    c_CheckThisDW           	= 1602

UNSIGNEDLONG    c_EventGotoRoot		= 1700
UNSIGNEDLONG    c_EventNoTriggerUp      	= 1701

UNSIGNEDLONG    c_FindRootDW            	= 1800
UNSIGNEDLONG    c_FindScrollDW          	= 1801
UNSIGNEDLONG    c_ShareNotEmpty		= 1900
UNSIGNEDLONG    c_ShareEmpty            	= 1901
UNSIGNEDLONG    c_SharePushRedraw       	= 1902
UNSIGNEDLONG    c_SharePopRedraw        	= 1903
UNSIGNEDLONG    c_SharePushRFC          	= 1904
UNSIGNEDLONG    c_SharePopRFC           	= 1905
UNSIGNEDLONG    c_SharePushValidate     	= 1906
UNSIGNEDLONG    c_SharePopValidate      	= 1907
UNSIGNEDLONG    c_SharePushVScroll      	= 1908
UNSIGNEDLONG    c_SharePopVScroll       	= 1909

UNSIGNEDLONG    c_OpenChildAuto		= 2000
UNSIGNEDLONG    c_OpenChildForce		= 2001
UNSIGNEDLONG    c_OpenChildAlwaysForce  	= 2002
UNSIGNEDLONG    c_OpenChildPrevent      	= 2003
UNSIGNEDLONG    c_OpenChildAlwaysPrevent	= 2004

UNSIGNEDLONG    c_RefreshUndefined      	= 0
UNSIGNEDLONG    c_RefreshView           	= 2100
UNSIGNEDLONG    c_RefreshModify		= 2101
UNSIGNEDLONG    c_RefreshNew            	= 2102
UNSIGNEDLONG    c_RefreshRFC            	= 2103
UNSIGNEDLONG    c_RefreshDrillDown      	= 2104
UNSIGNEDLONG    c_RefreshSame           	= 2105
UNSIGNEDLONG    c_RefreshSave           	= 2106

UNSIGNEDLONG    c_OnlyIfModified                	= 2200
UNSIGNEDLONG    c_RetrieveAllDWs		= 2201

UNSIGNEDLONG    c_ReselectRows          	= 2300
UNSIGNEDLONG    c_NoReselectRows        	= 2301

UNSIGNEDLONG    c_CheckForChanges       	= 2400
UNSIGNEDLONG    c_IgnoreChanges		= 2401
UNSIGNEDLONG    c_SaveChanges           	= 2402

UNSIGNEDLONG    c_DoRefresh             	= 2500
UNSIGNEDLONG    c_RefreshChildren              	= 2500
UNSIGNEDLONG    c_NoRefresh             	= 2501
UNSIGNEDLONG    c_NoRefreshChildren     	= 2501

UNSIGNEDLONG    c_NoPromptUser          	= 2600
UNSIGNEDLONG    c_PromptUser            	= 2601

UNSIGNEDLONG    c_ResetChildren                 	= 2700
UNSIGNEDLONG    c_NoResetChildren       	= 2701

UNSIGNEDLONG    c_ProcessSort           	= 2800
UNSIGNEDLONG    c_ProcessGroupCalc      	= 2801
UNSIGNEDLONG    c_ProcessFilter                 	= 2802
UNSIGNEDLONG    c_ProcessSync           	= 2803

UNSIGNEDLONG    c_CCErrorNone           	= 2900
UNSIGNEDLONG    c_CCErrorNonOverlap     	= 2901
UNSIGNEDLONG    c_CCErrorOverlap                = 2902
UNSIGNEDLONG    c_CCErrorOverlapMatch   	= 2903
UNSIGNEDLONG    c_CCErrorNonExistent    	= 2904
UNSIGNEDLONG    c_CCErrorDeleteModified	= 2905
UNSIGNEDLONG    c_CCErrorDeleteNonExistent	= 2906
UNSIGNEDLONG    c_CCErrorKeyConflict    	= 2907

UNSIGNEDLONG    c_CCUserNone            	= 3000
UNSIGNEDLONG    c_CCUserSpecify		= 3001

UNSIGNEDLONG    c_CCProcUseOldDB        	= 3100
UNSIGNEDLONG    c_CCProcUseNewDB        	= 3101
UNSIGNEDLONG    c_CCProcUseDWValue      	= 3102
UNSIGNEDLONG    c_CCProcUseSpecial      	= 3103

UNSIGNEDLONG    c_CCValidateNone		= 3200
UNSIGNEDLONG    c_CCValidateRow		= 3201

UNSIGNEDLONG    c_CCUserOK              	= 3300
UNSIGNEDLONG    c_CCUserCancel          	= 3301

UNSIGNEDLONG    c_CCSetIfModified               = 3400
UNSIGNEDLONG    c_CCSetForce            	= 3401

UNSIGNEDLONG    c_ValidateAllCols               	= 3500
UNSIGNEDLONG    c_ValidateCurCol                	= 3501

UNSIGNEDLONG    c_SetRowIndicatorFocus  	= 3600
UNSIGNEDLONG    c_SetRowIndicatorNotFocus	= 3601
UNSIGNEDLONG    c_SetRowIndicatorNormal	= 3602
UNSIGNEDLONG    c_SetRowIndicatorEmpty  	= 3603
UNSIGNEDLONG    c_SetRowIndicatorQuery  	= 3604

UNSIGNEDLONG    c_CapabilityEnable      	= 3700
UNSIGNEDLONG    c_CapabilityDisable     	= 3701

UNSIGNEDLONG    c_AllowControlEnable    	= 3800
UNSIGNEDLONG    c_AllowControlDisable   	= 3801

UNSIGNEDLONG    c_CapabilityAll                 	= 0
UNSIGNEDLONG    c_CapabilityNew                 	= 1
UNSIGNEDLONG    c_CapabilityModify              	= 2
UNSIGNEDLONG    c_CapabilityDelete              	= 4
UNSIGNEDLONG    c_CapabilityQuery              	= 8

UNSIGNEDLONG    c_AllowControlAll               	= 0
UNSIGNEDLONG    c_AllowControlAccept    	= 1
UNSIGNEDLONG    c_AllowControlCancel    	= 2
UNSIGNEDLONG    c_AllowControlClose     	= 4
UNSIGNEDLONG    c_AllowControlDelete    	= 8
UNSIGNEDLONG    c_AllowControlFirst     	= 16
UNSIGNEDLONG    c_AllowControlFilter    	= 32
UNSIGNEDLONG    c_AllowControlInsert    	= 64
UNSIGNEDLONG    c_AllowControlLast      	= 128
UNSIGNEDLONG    c_AllowControlMain      	= 256
UNSIGNEDLONG    c_AllowControlModify    	= 512
UNSIGNEDLONG    c_AllowControlNew       	= 1024
UNSIGNEDLONG    c_AllowControlNext      	= 2048
UNSIGNEDLONG    c_AllowControlPrevious  	= 4096
UNSIGNEDLONG    c_AllowControlPrint     	= 8192
UNSIGNEDLONG    c_AllowControlQuery     	= 16384
UNSIGNEDLONG    c_AllowControlResetQuery    = 32768
UNSIGNEDLONG    c_AllowControlRetrieve  	= 65536
UNSIGNEDLONG    c_AllowControlSave      	= 131072
UNSIGNEDLONG    c_AllowControlSaveRowsAs	= 262144
UNSIGNEDLONG    c_AllowControlSearch    	= 524288
UNSIGNEDLONG    c_AllowControlView      	= 1048576

UNSIGNEDLONG    c_ShareModified		= 1
UNSIGNEDLONG    c_ShareAllModified	= 2
UNSIGNEDLONG    c_ShareAllDoSetKey	= 4

UNSIGNEDLONG    c_NoSyncShare		= 0
UNSIGNEDLONG    c_SyncShare		= 1

INTEGER		c_VisibleCol		= -3
INTEGER		c_InvisibleCol		= -2
INTEGER		c_SameTab		= -1
INTEGER		c_SameBorde		= -1
LONG		c_SameColor		= -1
LONG		c_SameBColor		= -1

BOOLEAN	c_BatchAttrib		= TRUE
BOOLEAN	c_ProcessAttrib		= FALSE

BOOLEAN	c_DrawNow		= TRUE
BOOLEAN	c_BufferDraw		= FALSE

BOOLEAN	c_ProcessRFC		= TRUE
BOOLEAN	c_IgnoreRFC		= FALSE

BOOLEAN	c_ProcessVal		= TRUE
BOOLEAN	c_IgnoreVal		= FALSE

BOOLEAN	c_ProcessVScroll		= TRUE
BOOLEAN	c_IgnoreVScroll		= FALSE

BOOLEAN	c_StateUnused		= FALSE
end variables

forward prototypes
public function integer container_close ()
public subroutine save_on_close (unsignedlong close_option)
public subroutine set_ct_options (unsignedlong control_word)
end prototypes

on pc_close;//******************************************************************
//  PC Module     : uo_Container_Main
//  Event         : pc_CloseQuery
//  Description   : Makes sure that it is ok to close the
//                  user object.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

TriggerEvent("pc_CloseQuery")

IF PCCA.Error = c_Success THEN
   IF i_Window.WindowType = Main! THEN
      i_WindowMain.CloseUserObject(THIS)
   ELSE
      i_WindowResp.CloseUserObject(THIS)
   END IF
END IF
end on

on pc_closequery;//******************************************************************
//  PC Module     : uo_Container_Main
//  Event         : pc_CloseQuery
//  Description   : Makes sure that it is ok to close the
//                  user object.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN       l_HaveChanges
INTEGER       l_NumChangedDWs,  l_Answer,       l_Idx
UNSIGNEDLONG  l_SaveOnClose
UNSIGNEDLONG  l_RefreshCmd,     l_RefCmd[]
UO_DW_MAIN    l_RootDW,         l_ChangedDWs[], l_TmpDW

PCCA.Error = c_Success

IF i_NumDWs > 0 THEN

   //---------------------------------------------------------------
   //  Check to see if all of the DataWindows on this window are
   //  ready to close.
   //---------------------------------------------------------------

   PCCA.MDI.fu_PushID(PCCA.MDI.c_MDI_Close, c_ShowMark)

   //---------------------------------------------------------------
   //  Make sure that all of the DataWindows are saved.
   //---------------------------------------------------------------

   FOR l_Idx = 1 TO i_NumDWs
      l_RefCmd[l_Idx] = c_RefreshUndefined
   NEXT

   l_SaveOnClose = i_CT_SaveOnClose

   //---------------------------------------------------------------
   //  If we are to prompt the user once, figure out if any
   //  DataWindows have changed and prompt them once.
   //---------------------------------------------------------------

   IF l_SaveOnClose = c_SOCPromptUserOnce THEN

      l_HaveChanges = FALSE
      FOR l_Idx = 1 TO i_NumDWs

         l_TmpDW = i_DWs[l_Idx]

         IF l_TmpDW.i_InUse THEN
            IF l_TmpDW.Check_DW_Modified(c_CheckChildren) THEN

               l_HaveChanges = TRUE

               l_RefCmd[l_Idx] = l_RefreshCmd
               l_TmpDW.Build_Changed_List(l_NumChangedDWs, &
                                          l_ChangedDWs[])
            END IF
         END IF
      NEXT

      IF l_HaveChanges THEN
         l_Answer = i_DWs[1].Ask_User(l_NumChangedDWs, &
                                      l_ChangedDWs[])
         CHOOSE CASE l_Answer
            CASE 2
              l_SaveOnClose = c_SOCNoSave

            CASE 3

               //---------------------------------------------
               //  The user canceled.
               //---------------------------------------------

               PCCA.MDI.fu_Pop()
               PCCA.Error = c_Fatal
               GOTO Finished

            CASE ELSE
              l_SaveOnClose = c_SOCSave
         END CHOOSE
      END IF
   END IF

   CHOOSE CASE l_SaveOnClose

      //------------------------------------------------------------
      //  Cycle through each of the DataWindows on this window and
      //  reset the modification flags.
      //------------------------------------------------------------

      CASE c_SOCNoSave

         FOR l_Idx = 1 TO i_NumDWs

            l_TmpDW = i_DWs[l_Idx]

            IF l_TmpDW.i_InUse THEN
               IF l_TmpDW.Check_DW_Modified(c_CheckChildren) THEN

                  //------------------------------------------------
                  //  Clear the modification flags.
                  //------------------------------------------------

                  l_TmpDW.Reset_DW_Modified(c_ResetChildren)

                  //------------------------------------------------
                  //  If the close aborts, we'll need to refresh
                  //  the DataWindows since the modification flags
                  //  have been cleard.
                  //------------------------------------------------

                  l_RefCmd[l_Idx]            = c_RefreshSame
                  l_TmpDW.i_RetrieveMySelf   = TRUE
                  l_TmpDW.i_RetrieveChildren = TRUE
               END IF
            END IF
         NEXT

      //------------------------------------------------------------
      //  Cycle through each of the DataWindows on this window and
      //  save any DataWindows which have been modified.
      //------------------------------------------------------------

      CASE c_SOCSave

         FOR l_Idx = 1 TO i_NumDWs

            l_TmpDW = i_DWs[l_Idx]

            IF l_TmpDW.i_InUse THEN
               IF l_TmpDW.Check_DW_Modified(c_CheckChildren) THEN

                  //------------------------------------------------
                  //  Save the DataWindow.
                  //------------------------------------------------

                  l_RefCmd[l_Idx] = c_RefreshSave

                  l_RootDW = l_TmpDW.i_RootDW
                  l_RootDW.is_EventControl.No_Refresh_After_Save = &
                     TRUE
                  l_RootDW.TriggerEvent("pcd_Save")

                  //------------------------------------------------
                  //  If PCCA.Error does not indicate success, then
                  //  pcd_Save failed (e.g. validation error).
                  //------------------------------------------------

                  IF PCCA.Error <> c_Success THEN
                     l_RefCmd[l_Idx] = c_RefreshUndefined

                     //---------------------------------------------
                     //  Pop the close prompt off and exit the
                     //  event.
                     //---------------------------------------------

                     PCCA.MDI.fu_Pop()
                     PCCA.Error = c_Fatal
                     GOTO Finished
                  END IF

                  //------------------------------------------------
                  //  If a save happened, the DataWindow hierarchy
                  //  may need refreshing.
                  //------------------------------------------------

                  IF l_RootDW         <> l_TmpDW            THEN
                  IF l_RefreshCmd     <> c_RefreshUndefined THEN
                     l_RootDW.is_EventControl.Skip_DW_Valid = TRUE
                     l_RootDW.is_EventControl.Skip_DW       = l_TmpDW
                     l_RootDW.is_EventControl.Refresh_Cmd   = l_RefreshCmd
                     l_RootDW.TriggerEvent("pcd_Refresh")

                     //---------------------------------------------
                     //  If an error occurred during the refresh,
                     //  don't allow the close to happen.
                     //---------------------------------------------

                     IF PCCA.Error <> c_Success THEN
                        PCCA.MDI.fu_Pop()
                        PCCA.Error = c_Fatal
                        GOTO Finished
                     END IF
                  END IF
                  END IF
               END IF
            END IF
         NEXT

      //------------------------------------------------------------
      //  Cycle through each of the DataWindows on this window and
      //  prompt the user for any that have been modified.
      //------------------------------------------------------------

      CASE c_SOCPromptUser

         FOR l_Idx = 1 TO i_NumDWs

            l_TmpDW = i_DWs[l_Idx]

            IF l_TmpDW.i_InUse THEN
               l_TmpDW.Check_Save(l_RefreshCmd)
               l_RefCmd[l_Idx] = l_RefreshCmd

               //---------------------------------------------------
               //  If PCCA.Error does not indicate success, then
               //  the user hit cancel or pcd_Save failed (e.g.
               //  validation error).
               //---------------------------------------------------

               IF PCCA.Error <> c_Success THEN

                  //------------------------------------------------
                  //  Pop the close prompt off and exit the event.
                  //------------------------------------------------

                  PCCA.MDI.fu_Pop()
                  PCCA.Error = c_Fatal
                  GOTO Finished
               END IF

               //---------------------------------------------------
               //  If a save, the DataWindow hierarchy may need
               //  refreshing.
               //---------------------------------------------------

               l_RootDW = l_TmpDW.i_RootDW

               IF l_RootDW         <> l_TmpDW            THEN
               IF l_RefreshCmd     <> c_RefreshUndefined THEN
                  l_RootDW.is_EventControl.Skip_DW_Valid = TRUE
                  l_RootDW.is_EventControl.Skip_DW       = l_TmpDW
                  l_RootDW.is_EventControl.Refresh_Cmd   = l_RefreshCmd
                  l_RootDW.TriggerEvent("pcd_Refresh")

                  //------------------------------------------------------
                  //  If an error occurred during the refresh, don't
                  //  allow the close to happen.
                  //------------------------------------------------------

                  IF PCCA.Error <> c_Success THEN
                     PCCA.MDI.fu_Pop()
                     PCCA.Error = c_Fatal
                     GOTO Finished
                  END IF
               END IF
               END IF
            END IF
         NEXT

      //------------------------------------------------------------
      //  This case was already taken care of.  If we get to here
      //  there are no changes to be saved.
      //------------------------------------------------------------

      CASE c_SOCPromptUserOnce
   END CHOOSE

   //---------------------------------------------------------------
   //  Now we can close them.
   //---------------------------------------------------------------

   FOR l_Idx = 1 TO i_NumDWs

      //------------------------------------------------------------
      //  Set the No_Close flag to tell the DataWindow that it is
      //  being triggered by CloseQuery.
      //------------------------------------------------------------

      l_TmpDW = i_DWs[l_Idx]
      IF l_TmpDW.i_InUse THEN
         l_TmpDW.is_EventControl.No_Close = TRUE
         l_TmpDW.TriggerEvent("pcd_Close")

         //---------------------------------------------------------
         //  If there was an error, abort the close process.
         //---------------------------------------------------------

         IF PCCA.Error <> c_Success THEN
            PCCA.MDI.fu_Pop()
            PCCA.Error = c_Fatal
            GOTO Finished
         END IF
      END IF
   NEXT

   PCCA.MDI.fu_Pop()
END IF

Finished:

//------------------------------------------------------------------
//  If we are not closing the DataWindow, we need to make sure
//  that any DataWindows get refreshed correctly.
//------------------------------------------------------------------

IF PCCA.Error <> c_Success THEN
IF i_NumDWs   > 0          THEN
   FOR l_Idx = 1 TO i_NumDWs
      IF l_RefCmd[l_Idx] <> c_RefreshUndefined THEN
         l_TmpDW = i_DWs[l_Idx]
         l_TmpDW.is_EventControl.Refresh_Cmd = l_RefCmd[l_Idx]
         l_TmpDW.TriggerEvent("pcd_Refresh")
      END IF
   NEXT
   PCCA.Error = c_Fatal
END IF
END IF
end on

on pc_open;//******************************************************************
//  PC Module     : uo_Container_Main
//  Event         : pc_Open
//  Description   : Initializes the DataWindows that sit on
//                  this container object.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN     l_FirstDWIsValid,  l_EnablePopup
BOOLEAN     l_FileMenuIsValid, l_EditMenuIsValid
BOOLEAN     l_PopupMenuIsValid
INTEGER     l_LowTab,          l_Idx,        l_Jdx
INTEGER     l_OrigWidth,       l_OrigHeight, l_NewWidth, l_NewHeight
OBJECT      l_TypeOf
MENU        l_PopupMenu,       l_FileMenu
MENU        l_EditMenu,        l_DWMenu
UO_DW_MAIN  l_FirstDW,         l_ToDW,       l_TmpDW
DRAGOBJECT  l_TmpObj,          l_TabObj

//------------------------------------------------------------------
//  Grab the window's open parm.
//------------------------------------------------------------------

IF i_Window.WindowType = Main! THEN
   l_FirstDWIsValid    = i_WindowMain.i_FirstDWIsValid
   l_FirstDW           = i_WindowMain.i_FirstDW
   l_EnablePopup       = i_WindowMain.i_EnablePopup
   l_PopupMenu         = i_WindowMain.i_PopupMenu
   l_FileMenu          = i_WindowMain.i_FileMenu
   l_EditMenu          = i_WindowMain.i_EditMenu
   l_PopupMenuIsValid  = i_WindowMain.i_PopupMenuIsValid
   l_FileMenuIsValid   = i_WindowMain.i_FileMenuIsValid
   l_EditMenuIsValid   = i_WindowMain.i_EditMenuIsValid
   i_OpenParm          = i_WindowMain.i_OpenParm
ELSE
   l_FirstDWIsValid    = i_WindowResp.i_FirstDWIsValid
   l_FirstDW           = i_WindowResp.i_FirstDW
   l_EnablePopup       = i_WindowResp.i_EnablePopup
   l_PopupMenu         = i_WindowResp.i_PopupMenu
   l_FileMenu          = i_WindowResp.i_FileMenu
   l_EditMenu          = i_WindowResp.i_EditMenu
   l_PopupMenuIsValid  = i_WindowResp.i_PopupMenuIsValid
   l_FileMenuIsValid   = i_WindowResp.i_FileMenuIsValid
   l_EditMenuIsValid   = i_WindowResp.i_EditMenuIsValid
   i_OpenParm          = i_WindowResp.i_OpenParm
END IF

l_Idx = PCCA.PCMGR.i_DW_NumGlobalDW
DO WHILE l_Idx > 0

   l_TmpDW = PCCA.PCMGR.i_DW_GlobalDWList[l_Idx]

   //---------------------------------------------------------------
   //  Once we have found a DataWindow with a different window ID
   //  than the current window ID, we know we have found all of
   //  the DataWindows on this window.
   //---------------------------------------------------------------

   IF l_TmpDW.i_WindowID < i_WindowID THEN
      EXIT
   END IF

   //---------------------------------------------------------------
   //  We found a PowerClass DataWindow on this window and/or
   //  container object.  If this it is on this container object,
   //  then we need to set it up.
   //---------------------------------------------------------------

   IF l_TmpDW.i_WindowID  = i_WindowID THEN
   IF l_TmpDW.i_Container = THIS THEN

      //------------------------------------------------------------
      //  If this is the first DataWindow we found for this window,
      //  then we need to remember it.
      //------------------------------------------------------------

      IF NOT l_FirstDWIsValid THEN
         l_FirstDWIsValid = TRUE
         l_FirstDW        = l_TmpDW

         //---------------------------------------------------------
         //  Copy the default menus.  Default menus are used for
         //  DataWindows which may be initialized after the
         //  window is open.
         //---------------------------------------------------------

         IF NOT l_FirstDW.i_DefaultMenusDefined THEN
            IF l_EnablePopup THEN
               l_DWMenu = l_PopupMenu
            ELSE
               l_DWMenu = c_NullMenu
            END IF
            l_FirstDW.Set_DW_Default_Menus &
               (l_FileMenu, l_EditMenu, l_DWMenu)
         END IF
      END IF

      //------------------------------------------------------------
      //  Remember the DataWindow in case this container object
      //  gets destroyed before the window.
      //------------------------------------------------------------

      i_NumDWs        = i_NumDWs + 1
      i_DWs[i_NumDWs] = l_TmpDW
   END IF
   END IF

   //---------------------------------------------------------------
   //  Get ready to look at the next DataWindow.
   //---------------------------------------------------------------

   l_Idx = l_Idx - 1
LOOP

//------------------------------------------------------------------
//  Bump the window ID for next time.
//------------------------------------------------------------------

IF NOT i_IsStatic THEN
   PCCA.PCMGR.i_WindowID = PCCA.PCMGR.i_WindowID + 1
END IF

//------------------------------------------------------------------
//  We now have to add this DataWindow to the list of DataWindows
//  currently living on the window.
//------------------------------------------------------------------

IF i_NumDWs > 0 THEN
   IF l_FirstDW <> i_DWs[1] THEN

      l_TmpDW = l_FirstDW

      FOR l_Idx = 1 TO i_NumDWs

         l_TmpDW.i_NumWindowDWs = l_TmpDW.i_NumWindowDWs + 1
         l_TmpDW.i_WindowDWs[l_TmpDW.i_NumWindowDWs] = &
             i_DWs[l_Idx]

         FOR l_Jdx = 1 TO l_TmpDW.i_NumWindowDWs
            i_DWs[l_Idx].i_WindowDWs[l_Jdx] = &
               l_TmpDW.i_WindowDWs[l_Jdx]

            i_DWs[l_Idx].i_WindowDWs[l_Jdx].i_NumWindowDWs = &
               l_TmpDW.i_NumWindowDWs

            i_DWs[l_Idx]. &
               i_WindowDWs[l_Jdx]. &
                  i_WindowDWs[l_TmpDW.i_NumWindowDWs] = i_DWs[l_Idx]
         NEXT
      NEXT
   END IF
END IF

f_PO_TimerMark("Construction of Container and DWs")

//------------------------------------------------------------------
//  Initialize PCCA.Error and i_DisplayError for the developer.
//------------------------------------------------------------------

PCCA.Error     = c_Success
i_DisplayError = TRUE

//------------------------------------------------------------------
//  The pc_SetVariables event is used if data sent to the window
//  needs to be extracted from global variables.  The data is
//  usually loaded into local or instance variables.  This event
//  is coded by the developer.
//------------------------------------------------------------------

TriggerEvent("pc_SetVariables")

IF PCCA.Error <> c_Success THEN
   IF i_DisplayError THEN
      PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::pc_Open"
      PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
      PCCA.MB.i_MB_Strings[3] = i_ClassName
      PCCA.MB.i_MB_Strings[4] = i_Window.Title
      PCCA.MB.i_MB_Strings[5] = "pc_SetVariables"
      PCCA.MB.fu_MessageBox(PCCA.MB.c_MBI_WindowOpenError, &
                         0, PCCA.MB.i_MB_Numbers[],     &
                         5, PCCA.MB.i_MB_Strings[])
   END IF
   GOTO Finished
END IF

//------------------------------------------------------------------
//  Grab the time.
//------------------------------------------------------------------

f_PO_TimerMark("pc_SetVariables")

//------------------------------------------------------------------
//  Initialize i_DisplayError for the developer.
//------------------------------------------------------------------

i_DisplayError = TRUE

//------------------------------------------------------------------
//  The pc_SetWindow event is used to do any window setup
//  processing just before the window is open.  This event
//  is coded by the developer.
//------------------------------------------------------------------

TriggerEvent("pc_SetWindow")

IF PCCA.Error <> c_Success THEN
   IF i_DisplayError THEN
      PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::pc_Open"
      PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
      PCCA.MB.i_MB_Strings[3] = i_ClassName
      PCCA.MB.i_MB_Strings[4] = i_Window.Title
      PCCA.MB.i_MB_Strings[5] = "pc_SetWindow"
      PCCA.MB.fu_MessageBox(PCCA.MB.c_MBI_WindowOpenError, &
                         0, PCCA.MB.i_MB_Numbers[],     &
                         5, PCCA.MB.i_MB_Strings[])
   END IF
   GOTO Finished
END IF

//------------------------------------------------------------------
//  Grab the time.
//------------------------------------------------------------------

f_PO_TimerMark("pc_SetWindow")

//------------------------------------------------------------------
//  If the developer did not run Set_CT_Options(), run it for them
//  so that they get any default options they have defined.
//------------------------------------------------------------------

IF NOT i_OptionsInit THEN
   Set_CT_Options(c_Default)
END IF

//------------------------------------------------------------------
//  Initialize i_DisplayError for the developer.
//------------------------------------------------------------------

i_DisplayError = TRUE

//------------------------------------------------------------------
//  The pc_SetDDLB event is used to load any code tables located
//  on the window.  If the developer specified that code tables
//  were to be loaded upfront and then trigger this event now to
//  do it.  This event is coded by the developer.
//------------------------------------------------------------------

IF i_CT_LoadCodeTable THEN

   TriggerEvent("pc_SetDDLB")

   IF PCCA.Error <> c_Success THEN
      IF i_DisplayError THEN
         PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::pc_Open"
         PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
         PCCA.MB.i_MB_Strings[3] = i_ClassName
         PCCA.MB.i_MB_Strings[4] = i_Window.Title
         PCCA.MB.i_MB_Strings[5] = "pc_SetDDLB"
         PCCA.MB.fu_MessageBox(PCCA.MB.c_MBI_WindowOpenError, &
                            0, PCCA.MB.i_MB_Numbers[],     &
                            5, PCCA.MB.i_MB_Strings[])
      END IF
      GOTO Finished
   END IF

   //---------------------------------------------------------------
   //  Grab the time.
   //---------------------------------------------------------------

   f_PO_TimerMark("pc_SetDDLB")
END IF

IF i_NumDWs > 0 THEN

   //---------------------------------------------------------------
   //  We will make the lowest tab order DataWindow the current
   //  DataWindow.
   //---------------------------------------------------------------

   l_ToDW   = PCCA.Null_Object
   l_LowTab = 0

   FOR l_Idx = 1 TO i_NumDWs
      l_TmpDW = i_DWs[l_Idx]

      //------------------------------------------------------------
      //  Copy the menu information to the DataWindow.
      //------------------------------------------------------------

      IF NOT l_TmpDW.i_FileMenuIsValid THEN
         l_TmpDW.i_FileMenu        = l_FileMenu
         l_TmpDW.i_FileMenuIsValid = l_FileMenuIsValid
      END IF

      IF NOT l_TmpDW.i_EditMenuIsValid THEN
         l_TmpDW.i_EditMenu        = l_EditMenu
         l_TmpDW.i_EditMenuIsValid = l_EditMenuIsValid
      END IF

      IF NOT l_TmpDW.i_PopupMenuIsValid THEN
         IF l_EnablePopup THEN
            l_TmpDW.i_PopupMenu        = l_PopupMenu
            l_TmpDW.i_PopupMenuIsValid = l_PopupMenuIsValid
         END IF
      END IF

      //------------------------------------------------------------
      //  If this DataWindow is in use, then look to see if it has
      //  the lowest tab order.  If so, it will be the DataWindow
      //  that will become the first current DataWindow for this
      //  window.
      //------------------------------------------------------------

      IF l_TmpDW.i_InUse      THEN
      IF l_TmpDW.TabOrder > 0 THEN
      IF (l_TmpDW.TabOrder < l_LowTab OR l_LowTab = 0) THEN
         l_ToDW   = l_TmpDW
         l_LowTab = l_TmpDW.TabOrder
      END IF
      END IF
      END IF
   NEXT

   //---------------------------------------------------------------
   //  If we have a valid DataWindow to be the first current
   //  DataWindow for this window, set focus to it.
   //---------------------------------------------------------------

   IF IsValid(l_ToDW) THEN
      l_ToDW.Set_Auto_Focus(l_ToDW)
   END IF
END IF

//------------------------------------------------------------------
//  Set focus if it has not been done yet.
//------------------------------------------------------------------

l_LowTab = 0
FOR l_Idx = 1 TO UpperBound(Control[])
   l_TypeOf = Control[l_Idx].TypeOf()
   IF l_TypeOf <> Line!           AND &
      l_TypeOf <> Oval!           AND &
      l_TypeOf <> Rectangle!      AND &
      l_TypeOf <> RoundRectangle! AND &
      l_TypeOf <> MDIClient!      THEN
      l_TmpObj = Control[l_Idx]
      IF l_TmpObj.TabOrder > 0 THEN
      IF (l_TmpObj.TabOrder < l_LowTab OR l_LowTab = 0) THEN
         l_TabObj = l_TmpObj
         l_LowTab = l_TabObj.TabOrder
      END IF
      END IF
   END IF
NEXT
IF l_LowTab > 0 THEN
   l_TabObj.SetFocus()
END IF

//------------------------------------------------------------------
//  Set the inactive colors, if any, for the DataWindows that are
//  not current.  Doing the action here prevents the flashing
//  effect.
//------------------------------------------------------------------

IF l_FirstDWIsValid THEN
   FOR l_Idx = 1 TO l_FirstDW.i_NumWindowDWs
      l_TmpDW = l_FirstDW.i_WindowDWs[l_Idx]
      IF NOT l_TmpDW.i_Current THEN
         l_TmpDW.is_EventControl.Highlight_Only = TRUE
         l_TmpDW.TriggerEvent("pcd_Inactive")
      END IF
   NEXT
END IF

//------------------------------------------------------------------
//  If the window this container is opened on has been resized then
//  we need to resize the controls on this container as well.
//------------------------------------------------------------------

IF NOT i_IsStatic THEN
   IF i_Window.WindowType = Main! THEN 
      l_OrigWidth  = i_WindowMain.i_PO_Controls[1]
      l_OrigHeight = i_WindowMain.i_PO_Controls[2]
   ELSE
      l_OrigWidth  = i_WindowResp.i_PO_Controls[1]
      l_OrigHeight = i_WindowResp.i_PO_Controls[2]
   END IF

   l_NewWidth  = i_Window.WorkSpaceWidth()
   l_NewHeight = i_Window.WorkSpaceHeight()

   IF l_NewWidth <> l_OrigWidth OR l_NewHeight <> l_OrigHeight THEN
      TriggerEvent("pc_Resize")
   END IF

   IF i_Window.WindowType = Main! THEN 
      i_WindowMain.SetRedraw(TRUE)
   ELSE
      i_WindowResp.SetRedraw(TRUE)
   END IF   
END IF

//------------------------------------------------------------------
//  Update the window's parms.
//------------------------------------------------------------------

IF i_Window.WindowType = Main! THEN
   i_WindowMain.i_FirstDWIsValid   = l_FirstDWIsValid    
   i_WindowMain.i_FirstDW          = l_FirstDW            
   i_WindowMain.i_EnablePopup      = l_EnablePopup     
   i_WindowMain.i_PopupMenu        = l_PopupMenu         
   i_WindowMain.i_FileMenu         = l_FileMenu           
   i_WindowMain.i_EditMenu         = l_EditMenu           
   i_WindowMain.i_PopupMenuIsValid = l_PopupMenuIsValid
   i_WindowMain.i_FileMenuIsValid  = l_FileMenuIsValid           
   i_WindowMain.i_EditMenuIsValid  = l_EditMenuIsValid    
ELSE
   i_WindowResp.i_FirstDWIsValid   = l_FirstDWIsValid   
   i_WindowResp.i_FirstDW          = l_FirstDW           
   i_WindowResp.i_EnablePopup      = l_EnablePopup
   i_WindowResp.i_PopupMenu        = l_PopupMenu         
   i_WindowResp.i_FileMenu         = l_FileMenu           
   i_WindowResp.i_EditMenu         = l_EditMenu          
   i_WindowResp.i_PopupMenuIsValid = l_PopupMenuIsValid
   i_WindowResp.i_FileMenuIsValid  = l_FileMenuIsValid
   i_WindowResp.i_EditMenuIsValid  = l_EditMenuIsValid
END IF

f_PO_TimerMark("Exit pc_Open Event")

Finished:
end on

on pc_resize;//******************************************************************
//  PC Module     : uo_Container_Main
//  Event         : pc_Resize
//  Description   : Handles the resizing of the container.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

IF i_CT_ResizeCont THEN
   f_PO_ResizeControls(THIS, i_PO_Controls[], i_CT_ZoomCont)
END IF
end on

on pc_setwindow;//******************************************************************
//  PC Module     : uo_Container_Main
//  Event         : pc_SetWindow
//  Description   : Initializes some pre-defined constants
//                  used with Set_DW_Options() and
//                  Set_DW_Defaults().
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

c_MasterList = c_EnableNewOnOpen        + &
               c_EnableModifyOnOpen     + &
               c_DeleteOk               + &
               c_SelectOnRowFocusChange + &
               c_DrillDown

c_MasterEdit = c_NewOk                  + &
               c_ModifyOk               + &
               c_SelectOnRowFocusChange + &
               c_DrillDown              + &
               c_ScrollParent           + &
               c_RefreshParent

c_DetailList = c_EnableNewOnOpen        + &
               c_EnableModifyOnOpen     + &
               c_DeleteOk               + &
               c_SelectOnClick          + &
               c_DrillDown              + &
               c_RefreshParent          + &
               c_RefreshChild

c_DetailEdit = c_NewOk                  + &
               c_ModifyOk               + &
               c_SelectOnRowFocusChange + &
               c_DrillDown              + &
               c_ModifyOnSelect         + &
               c_ScrollParent           + &
               c_RefreshParent          + &
               c_RefreshChild

end on

public function integer container_close ();//******************************************************************
//  PC Module     : uo_Container_Main
//  Function      : Container_Close
//  Description   : Attempts to close the container object and
//                  optionally performs checks for changes and
//                  saves.
//
//  Parameters    : (None)
//
//  Return Value  : INTEGER -
//                        Error return - c_Success or c_Fatal.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Error

TriggerEvent("pc_CloseQuery")

l_Error    = PCCA.Error
PCCA.Error = c_Success

IF l_Error = c_Success THEN
   IF i_Window.WindowType = Main! THEN
      i_WindowMain.CloseUserObject(THIS)
   ELSE
      i_WindowResp.CloseUserObject(THIS)
   END IF
   RETURN PCCA.PCMGR.c_Success
ELSE
   RETURN PCCA.PCMGR.c_Fatal
END IF
end function

public subroutine save_on_close (unsignedlong close_option);//******************************************************************
//  PC Module     : uo_Container_Main
//  Subroutine    : Save_On_Close
//  Description   : Allows the developer to specify what
//                  should happen to modifications in the
//                  DataWindows when the window is closed.
//
//  Parameters    : UNSIGNEDLONG Close_Option -
//                       Should be specified as one of the
//                       following:
//
//                          - c_SOCPromptUser
//                              Indicates that PowerClass is to
//                              prompt the user for each
//                              DataWindow hierarchy which has
//                              been modified when the window
//                              is closed.
//
//                          - c_SOCPromptUserOnce
//                              Indicates that PowerClass is to
//                              prompt the user once if any of
//                              the DataWindow hierarchies have
//                              been modified when the window
//                              is closed.
//
//                          - c_SOCSave
//                              Indicates that PowerClass is
//                              to automatically save all of
//                              DataWindows which contain
//                              modifications when the window
//                              is closed.
//
//                          - c_SOCNoSave
//                              Indicates that PowerClass is
//                              NOT to save any DataWindows.
//                              when the window is closed.
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

i_CT_SaveOnClose = Close_Option

RETURN
end subroutine

public subroutine set_ct_options (unsignedlong control_word);//******************************************************************
//  PC Module     : uo_Container_Main
//  Subroutine    : Set_CT_Options
//  Description   : Looks at the passed words and sets the
//                  container options.
//
//  Parameters    : UNSIGNEDLONG Control_Word -
//                        Specifies options for how this
//                        container is to behave.
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Idx

//------------------------------------------------------------------
//  Indicate that Set_CT_Options() has run, so that the pc_Open
//  event will not run it again.
//------------------------------------------------------------------

i_OptionsInit = TRUE

//------------------------------------------------------------------
//  Remember the developer's specifications
//------------------------------------------------------------------

i_CT_ControlWord = Control_Word

//------------------------------------------------------------------
//  Have the DLL calculate the bits.
//------------------------------------------------------------------

PCCA.DLL.fu_SetCTOptions (THIS, Control_Word)

//------------------------------------------------------------------
//  If the developer wanted the container on top, do it now.
//------------------------------------------------------------------

BringToTop = i_CT_BringToTop

RETURN
end subroutine

on constructor;//******************************************************************
//  PC Module     : uo_Container_Main
//  Event         : Constructor
//  Description   : Initializes container instance variables.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER l_OrigWidth, l_OrigHeight, l_NewWidth, l_NewHeight

//------------------------------------------------------------------
//  Set up i_ClassName.
//  See if the window variables are initialzed yet.  If there are
//  not, then this is a static container object (i.e. the
//  constructor is runing before the open event.
//------------------------------------------------------------------

i_Window = PARENT
IF i_Window.WindowType = Main! THEN
   i_WindowMain = i_Window
   i_ClassName  = i_WindowMain.ClassName() + "." + ClassName()
   i_IsStatic   = (i_WindowMain.i_ClassName = "")
ELSE
   i_WindowResp = i_Window
   i_ClassName  = i_WindowResp.ClassName() + "." + ClassName()
   i_IsStatic   = (i_WindowResp.i_ClassName = "")
END IF

//------------------------------------------------------------------
//  Save the original position and size.
//------------------------------------------------------------------

i_ContX      = X
i_ContY      = Y
i_ContWidth  = Width
i_ContHeight = Height

//------------------------------------------------------------------
//  Bump the window ID.
//------------------------------------------------------------------

IF NOT i_IsStatic THEN
   PCCA.PCMGR.i_WindowID = PCCA.PCMGR.i_WindowID + 1
   IF i_Window.WindowType = Main! THEN
      i_WindowMain.SetRedraw(FALSE)
   ELSE
      i_WindowResp.SetRedraw(FALSE)
   END IF
END IF

//------------------------------------------------------------------
//  Remember the current window ID.
//------------------------------------------------------------------

i_WindowID = PCCA.PCMGR.i_WindowID

//------------------------------------------------------------------
//  Add ourselves to the parent window's container list.
//------------------------------------------------------------------

IF i_Window.WindowType = Main! THEN
   i_WindowMain.i_NumContainers = i_WindowMain.i_NumContainers + 1
   i_WindowMain.i_Containers[i_WindowMain.i_NumContainers] = THIS
ELSE
   i_WindowResp.i_NumContainers = i_WindowResp.i_NumContainers + 1
   i_WindowResp.i_Containers[i_WindowResp.i_NumContainers] = THIS
END IF

//------------------------------------------------------------------
//  Initialize the container options.
//------------------------------------------------------------------

PCCA.DLL.fu_InitCTBits (THIS)

//------------------------------------------------------------------
//  Initialize the DataWindow options.
//------------------------------------------------------------------

PCCA.DLL.fu_CTInitDWBits (THIS)

//------------------------------------------------------------------
//  Initialize NULL objects for use by the developer.
//------------------------------------------------------------------

c_NullDW      = PCCA.Null_Object
c_NullCB      = PCCA.Null_Object
c_NullMenu    = PCCA.Null_Object
c_NullPicture = PCCA.Null_Object

//------------------------------------------------------------------
//  Kick off the first resize so that the PO variables get
//  initialized.
//------------------------------------------------------------------

f_PO_ResizeControls(THIS, i_PO_Controls[], FALSE)
   
//------------------------------------------------------------------
//  If the window this container is opened on has been resized then
//  we need to resize the controls on this container as well.
//------------------------------------------------------------------

IF NOT i_IsStatic THEN
   IF i_Window.WindowType = Main! THEN 
      l_OrigWidth  = i_WindowMain.i_PO_Controls[1]
      l_OrigHeight = i_WindowMain.i_PO_Controls[2]
   ELSE
      l_OrigWidth  = i_WindowResp.i_PO_Controls[1]
      l_OrigHeight = i_WindowResp.i_PO_Controls[2]
   END IF

   l_NewWidth  = i_Window.WorkSpaceWidth()
   l_NewHeight = i_Window.WorkSpaceHeight()

   IF l_NewWidth  <> l_OrigWidth OR l_NewHeight <> l_OrigHeight THEN
      Width  = Width  * (l_NewWidth / l_OrigWidth)
      Height = Height * (l_NewHeight / l_OrigHeight)
   END IF
END IF

//------------------------------------------------------------------
//  Need to wait for the construction of the DataWindows.
//------------------------------------------------------------------

PostEvent("pc_Open")

end on

on destructor;//******************************************************************
//  PC Module     : uo_Container_Main
//  Event         : Destructor
//  Description   : Performs termination cleanup of container
//                  instance variables.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN            l_IsFirst,      l_Found, l_FirstDWIsValid
INTEGER            l_NumLeft,      l_NumContainers
INTEGER            l_Idx,          l_Jdx
UO_DW_MAIN         l_FirstDW,      l_TmpDW
UO_CONTAINER_MAIN  l_Containers[], l_TmpCont

IF i_Window.WindowType = Main! THEN
   l_FirstDWIsValid = i_WindowMain.i_FirstDWIsValid
   l_FirstDW        = i_WindowMain.i_FirstDW
   l_NumContainers  = i_WindowMain.i_NumContainers
   l_Containers[]   = i_WindowMain.i_Containers[]
ELSE
   l_FirstDWIsValid = i_WindowResp.i_FirstDWIsValid
   l_FirstDW        = i_WindowResp.i_FirstDW
   l_NumContainers  = i_WindowResp.i_NumContainers
   l_Containers[]   = i_WindowResp.i_Containers[]
END IF

//------------------------------------------------------------------
//  If there is still a valid DataWindow on the window, we need
//  to make sure that we're not closing the one that the window
//  is dependent on.
//------------------------------------------------------------------

IF l_FirstDWIsValid THEN
   FOR l_Idx = 1 TO i_NumDWs
      IF l_FirstDW = i_DWs[l_Idx] THEN
         l_IsFirst = TRUE
         EXIT
      END IF
   NEXT
END IF

IF l_IsFirst THEN
   l_FirstDWIsValid = FALSE
   FOR l_Idx = 1 TO l_FirstDW.i_NumWindowDWs

      l_TmpDW = l_FirstDW.i_WindowDWs[l_Idx]
      l_Found = FALSE

      FOR l_Jdx = 1 TO i_NumDWs
         IF l_TmpDW = i_DWs[l_Jdx] THEN
            l_Found      = TRUE
            EXIT
         END IF
      NEXT

      IF NOT l_Found THEN

         //---------------------------------------------------------
         //  Copy the default menus.  Default menus are used for
         //  DataWindows which may be initialized after the
         //  window is open.
         //---------------------------------------------------------

         l_TmpDW.Set_DW_Default_Menus     &
            (l_FirstDW.i_DefaultFileMenu, &
             l_FirstDW.i_DefaultEditMenu, &
             l_FirstDW.i_DefaultPopupMenu)

         l_FirstDWIsValid = TRUE
         l_FirstDW        = l_TmpDW
         EXIT
      END IF
   NEXT
END IF

IF l_FirstDWIsValid THEN
   FOR l_Idx = 1 TO i_NumDWs
      FOR l_Jdx = l_FirstDW.i_NumWindowDWs TO 1 STEP -1
         l_TmpDW = l_FirstDW.i_WindowDWs[l_Jdx]
         l_TmpDW.Remove_Obj(i_DWs[l_Idx],                 &
                                  l_TmpDW.i_NumWindowDWs, &
                                  l_TmpDW.i_WindowDWs[])
      NEXT
   NEXT
END IF

//------------------------------------------------------------------
//  Delete ourselves from the parent window's container list.
//------------------------------------------------------------------

l_NumLeft = 0
FOR l_Idx = 1 TO l_NumContainers
   l_TmpCont = l_Containers[l_Idx]

   IF l_TmpCont <> THIS THEN
      l_NumLeft = l_NumLeft + 1

      IF l_Idx <> l_NumLeft THEN
         l_Containers[l_NumLeft] = l_TmpCont
      END IF
   END IF
NEXT

l_NumContainers = l_NumLeft

//------------------------------------------------------------------
//  Update the window's parms.
//------------------------------------------------------------------

IF i_Window.WindowType = Main! THEN
  i_WindowMain.i_FirstDWIsValid  = l_FirstDWIsValid 
  i_WindowMain.i_FirstDW         = l_FirstDW 
  i_WindowMain.i_NumContainers   = l_NumContainers 
  i_WindowMain.i_Containers[]    = l_Containers[]   
ELSE
   i_WindowResp.i_FirstDWIsValid = l_FirstDWIsValid 
   i_WindowResp.i_FirstDW        = l_FirstDW       
   i_WindowResp.i_NumContainers  = l_NumContainers   
   i_WindowResp.i_Containers[]   = l_Containers[]
END IF

//------------------------------------------------------------------
//  Destroy the POPUP menu if we created it.
//------------------------------------------------------------------

IF IsValid(i_mPopup) THEN
   DESTROY i_mPopup
END IF
end on

on rbuttondown;//******************************************************************
//  PC Module     : uo_Container_Main
//  Event         : RButtonDown
//  Description   : Display POPUP menu for operating on the
//                  current DataWindow.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

i_Window.TriggerEvent("RButtonDown")
end on

on uo_container_main.create
end on

on uo_container_main.destroy
end on

