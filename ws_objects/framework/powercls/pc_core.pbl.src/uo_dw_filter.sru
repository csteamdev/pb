﻿$PBExportHeader$uo_dw_filter.sru
$PBExportComments$DW object for filtering data
forward
global type uo_dw_filter from u_dw_filter
end type
end forward

global type uo_dw_filter from u_dw_filter
event pc_validate pbm_custom74
end type
global uo_dw_filter uo_dw_filter

type variables
//------------------------------------------------------------------------------
//  Instance Variables in sorted order.
//------------------------------------------------------------------------------

STRING		i_ClassName

STRING		i_ObjectType 	= "uo_DW_Filter"

UO_DW_MAIN	i_PCFilterDW

WINDOW		i_Window

end variables

forward prototypes
public function integer load_code (string filter_column, string table_name, string column_code, string column_desc, string where_clause, string all_keyword)
public function integer refresh_code (string filter_column)
public function string select_code (string filter_column)
public function integer set_code (string filter_column, string default_code)
public function integer disable_column (string filter_column)
public function integer enable_column (string filter_column)
public function integer hide_column (string filter_column)
public function integer show_column (string filter_column)
public subroutine reset_column ()
end prototypes

on pc_validate;call u_dw_filter::pc_validate;////******************************************************************
////  PO Module     : uo_DW_Filter
////  Event         : pc_Validate
////  Description   : Provides the opportunity for the developer to
////                  write code to validate the fields in this
////                  DataWindow.
////
////  Return Value  : INTEGER PCCA.Error 
////
////                  If the developer codes the validation 
////                  testing, then the developer should set 
////                  this variable to one of the following
////                  validation return codes:
////
////                  c_ValOK     = The validation test passed and 
////                                the data is to be accepted.
////                  c_ValFailed = The validation test failed and 
////                                the data is to be rejected.  
////                                Do not allow the user to move 
////                                off of this field.
////                  c_ValFixed  = The validation test failed.  
////                                However, the data was able to be
////                                modified by developer to pass the
////                                validation test.  If a developer
////                                returns this code, they are 
////                                responsible for doing a SetItem()
////                                to place the fixed value into the
////                                column.  Note that if the 
////                                DataWindow validation routines 
////                                are used, they will do the 
////                                SetItem(), if necessary.
////
////  Change History:
////
////  Date     Person     Description of Change
////  -------- ---------- --------------------------------------------
////
////******************************************************************
////  Copyright ServerLogic 1992-1995.  All Rights Reserved.
////******************************************************************
//
////------------------------------------------------------------------
////  The following values are set for the developer before this
////  event is triggered:
////
////      STRING i_FilterValue -
////            The input the user has made.
////
////      STRING i_FilterColumn -
////            The name of the column being validated, folded
////            to lower case.
////
////      STRING i_FilterRow -
////            The row number of this DataWindow.  This value is
////            always equal to 1.
////
////      STRING i_FilterValMsg -
////            The validation error message provided by
////            PowerBuilder's extended column definition.
////
////      BOOLEAN i_FilterRequiredError -
////            This column is a required field and the user has
////            not entered anything.
////------------------------------------------------------------------
//
////------------------------------------------------------------------
////  Determine which column to validate.
////------------------------------------------------------------------
//
//CHOOSE CASE i_FilterColumn
//
////   //-------------------------------------------------------------
////   //  Enter the code for each column you want to validate.
////   //  <column> should be replaced by the name of the column
////   //  from the DataWindow.  Use the i_FilterError variable to
////   //  return the status of the validation.  Some sample
////   //  validation functions are listed below.  
////   //-------------------------------------------------------------
//
////   CASE "<column1>"
//
////      PCCA.Error = f_PO_ValInt(i_FilterValue, "##,##0", TRUE)
//
////   CASE "<column2>"
//
////      PCCA.Error = f_PO_ValDec(i_FilterValue, "##,##0.00", TRUE)
////      IF PCCA.Error <> c_ValFailed THEN
////         IF Real(i_FilterValue) < 0 THEN
////            MessageBox("Filter Object Error", "<error_message>")
////            PCCA.Error = c_ValFailed
////         END IF
////      END IF
//
////   CASE "<column3>"
//
////      IF Upper(Left(i_FilterValue, 1)) = "Y"  THEN
////         SetItem(i_FilterRow, i_FilterColumn, "Yes")
////         PCCA.Error = c_ValFixed
////      ELSE
////         IF Upper(Left(i_FilterValue, 1)) = "N"  THEN
////            SetItem(i_FilterRow, i_FilterColumn, "No")
////            PCCA.Error = c_ValFixed
////         ELSE
////            MessageBox("Filter Object Error", &
////                "Valid responses are 'Y' and 'N'")
////            PCCA.Error = c_ValFailed
////         END IF
////      END IF
//
////   CASE "<column4>"
//
////      IF i_FilterRequiredError THEN
////         IF MessageBox("Filter Object Error", &
////                "Do you want a default value of 'Active'?", &
////                Question!, YesNo!, 1) = 1 THEN
////            SetItem(i_FilterRow, i_FilterColumn, "Active")
////            PCCA.Error = c_ValFixed
////         ELSE
////            PCCA.Error = c_ValFailed
////         END IF
////      END IF
//
////   CASE "<column5>"  
////
////      Generate a default value for a CheckBox
////
////      IF i_FilterRequiredError THEN
////         SetItem(i_FilterRow, i_FilterColumn, "N")
////         PCCA.Error = c_ValFixed
////      END IF
//
//END CHOOSE
end on

public function integer load_code (string filter_column, string table_name, string column_code, string column_desc, string where_clause, string all_keyword);//******************************************************************
//  PC Module     : uo_DW_Filter
//  Function      : Load_Code
//  Description   : Load a code table using codes and descriptions
//                  from the database.
//
//  Parameters    : STRING Filter_Column -
//                        The column name on this DataWindow to
//                        load.
//                  STRING Table_Name -
//                        Table from where the column with the
//                        code values resides.
//                  STRING Column_Code -
//                        Column name with the code code values.
//                  STRING Column_Desc -
//                        Column name with the code descriptions.
//                  STRING Where_Clause -
//                        WHERE clause statement to restrict the
//                        code values.
//                  STRING All_Keyword -
//                        Keyword to denote select all values
//                        (e.g. "(All)").
//
//  Return Value  : 0 = OK, -1 = Error
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

RETURN fu_LoadCode(filter_column, table_name, column_code, &
                   column_desc, where_clause, all_keyword)

end function

public function integer refresh_code (string filter_column);//******************************************************************
//  PC Module     : uo_DW_Filter
//  Function      : Refresh_Code
//  Description   : Re-retrieves the data into the code tables for
//                  a column on this DataWindow.  Note that the 
//                  Load_Code() routine does the first retrieve.
//
//  Parameters    : STRING Filter_Column -
//                         The column in this DataWindow that is
//                         to be re-loaded with data.
//
//  Return Value  : 0 = OK, -1 = Error
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

RETURN fu_RefreshCode(filter_column)

end function

public function string select_code (string filter_column);//******************************************************************
//  PC Module     : uo_DW_Filter
//  Function      : Select_Code
//  Description   : Select the code associated with the given column
//                  in this DataWindow.
//
//  Parameters    : STRING Filter_Column -
//                         Column in this DataWindow to select a
//                         code from.
//
//  Return Value  : STRING -  <code value> = OK, "" = NULL
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

RETURN fu_SelectCode(filter_column)

end function

public function integer set_code (string filter_column, string default_code);//******************************************************************
//  PC Module     : uo_DW_Filter
//  Subroutine    : Set_Code
//  Description   : Sets a default value for a column in this
//                  DataWindow.
//
//  Parameters    : STRING Filter_Column -
//                     The column in this DataWindow to set a
//                     default code for.
//                  STRING Default_Code -
//                     The value to set for this column.
//
//  Return Value  : 0 = set OK   -1 = set failed
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

RETURN fu_SetCode(filter_column, default_code)

end function

public function integer disable_column (string filter_column);//******************************************************************
//  PO Module     : uo_DW_Filter
//  Subroutine    : Disable_Column
//  Description   : Make a column on this DataWindow disabled.
//
//  Parameters    : STRING Filter_Column -
//                     The column in this DataWindow to set to
//                     disable.
//
//  Return Value  : 0 = OK     -1 = invalid column
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

RETURN fu_DisableColumn(filter_column)


end function

public function integer enable_column (string filter_column);//******************************************************************
//  PO Module     : uo_DW_Filter
//  Subroutine    : Enable_Column
//  Description   : Make a column on this DataWindow enabled.
//
//  Parameters    : STRING Filter_Column -
//                     The column in this DataWindow to set to
//                     enable.
//
//  Return Value  : 0 = OK     -1 = invalid column
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

RETURN fu_EnableColumn(filter_column)


end function

public function integer hide_column (string filter_column);//******************************************************************
//  PO Module     : uo_DW_Filter
//  Subroutine    : Hide_Column
//  Description   : Make a column on this DataWindow invisible.
//
//  Parameters    : STRING Filter_Column -
//                     The column in this DataWindow to set to
//                     invisible.
//
//  Return Value  : 0 = OK     -1 = invalid column
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

RETURN fu_HideColumn(filter_column)


end function

public function integer show_column (string filter_column);//******************************************************************
//  PO Module     : uo_DW_Filter
//  Subroutine    : Show_Column
//  Description   : Make a column on this DataWindow visible.
//
//  Parameters    : STRING Filter_Column -
//                     The column in this DataWindow to set to
//                     visible.
//
//  Return Value  : 0 = OK     -1 = invalid column
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

RETURN fu_ShowColumn(filter_column)


end function

public subroutine reset_column ();//******************************************************************
//  PC Module     : uo_DW_Filter
//  Subroutine    : Reset_Column
//  Description   : Clears the selections in this DataWindow.
//
//  Parameters    : (None)
//
//  Return Value  : 0 = OK,  -1 = insert error
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

fu_Reset()

end subroutine

on destructor;call u_dw_filter::destructor;//******************************************************************
//  PC Module     : uo_DW_Filter
//  Event         : Destructor
//  Description   : Tears down the PowerClass Filter Object.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

IF NOT IsNull(i_PCFilterDW) THEN
   IF i_PCFilterDW.i_Window <> i_Window THEN
      i_PCFilterDW.Unwire_Filter(THIS)
   END IF
END IF
end on

on constructor;call u_dw_filter::constructor;//******************************************************************
//  PC Module     : uo_DW_Filter
//  Event         : Constructor
//  Description   : Initializes the PowerClass Filter Object.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

UO_CONTAINER_MAIN  l_Container

IF PARENT.TypeOf() = UserObject! THEN
   l_Container = PARENT
   i_Window    = l_Container.i_Window      
ELSE
   i_Window = PARENT
END IF

i_ClassName = i_Window.ClassName() + "." + ClassName()
SetNull(i_PCFilterDW)
end on

on getfocus;call u_dw_filter::getfocus;//******************************************************************
//  PC Module     : uo_DW_Filter
//  Event         : GetFocus
//  Description   : Make the DataWindow we are wired to the
//                  active DataWindow.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

IF IsValid(i_PCFilterDW) THEN
   i_PCFilterDW.TriggerEvent("pcd_Active")
END IF
end on

on po_validate;//******************************************************************
//  PC Module     : uo_DW_Filter
//  Event         : po_Validate
//  Description   : Override the PowerObjects validation event and
//                  trigger the corresponding PowerClass validation
//                  event for backward compatibility.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

PCCA.Error = 0
TriggerEvent("pc_Validate")
i_FilterError = PCCA.Error
end on

