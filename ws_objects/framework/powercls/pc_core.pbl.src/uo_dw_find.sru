﻿$PBExportHeader$uo_dw_find.sru
$PBExportComments$Object to scroll to a string in a DataWindow
forward
global type uo_dw_find from u_dw_find
end type
end forward

global type uo_dw_find from u_dw_find
event pc_validate pbm_custom74
end type
global uo_dw_find uo_dw_find

type variables
//------------------------------------------------------------------------------
//  Instance Variables in sorted order.
//------------------------------------------------------------------------------

BOOLEAN	i_Ascending

STRING		i_ClassName
STRING		i_ColType

STRING		i_GetText
BOOLEAN	i_GotOrder

STRING		i_ObjectType 	= "uo_DW_Find"

UO_DW_MAIN	i_PCFindDW

WINDOW		i_Window

end variables

forward prototypes
end prototypes

on pc_validate;call u_dw_find::pc_validate;//******************************************************************
//  PC Module     : uo_DW_Find
//  Event         : pc_Validate
//  Description   : Provides validation for the find object.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Sep
STRING   l_Date, l_Time

CHOOSE CASE i_ColType
   CASE "CHAR"

   CASE "DECIMAL", "NUMBER"
      IF Len(i_GetText) = 0 THEN
         i_GetText = "0"
      ELSE
         IF NOT IsNumber(i_GetText) THEN
            PCCA.Error = c_ValFailed
         END IF
      END IF

   CASE "DATE"
      IF NOT IsDate(i_GetText) THEN
         PCCA.Error = c_ValFailed
      END IF

   CASE "DATETIME"
      l_Sep = Pos(i_GetText, " ")
      IF l_Sep = 0 THEN
         l_Sep = Pos(i_GetText, ",")
      END IF
      IF l_Sep = 0 THEN
         l_Sep = Pos(i_GetText, ";")
      END IF

      IF l_Sep = 0 THEN
         IF NOT IsDate(i_GetText) THEN
            PCCA.Error = c_ValFailed
         END IF
      ELSE
         l_Date = Mid(i_GetText, 1, l_Sep - 1)
         l_Time = Mid(i_GetText, l_Sep + 1)
         IF NOT IsDate(l_Date) THEN
            PCCA.Error = c_ValFailed
         ELSE
            IF NOT IsTime(l_Time) THEN
               PCCA.Error = c_ValFailed
            ELSE
               i_GetText = l_Date + ";" + l_Time
            END IF
         END IF
      END IF

   CASE "TIME", "TIMESTAMP"
      IF NOT IsTime(i_GetText) THEN
         PCCA.Error = c_ValFailed
      END IF

   CASE ELSE
END CHOOSE
end on

on getfocus;//******************************************************************
//  PC Module     : uo_DW_Find
//  Event         : GetFocus
//  Description   : Make the DataWindow we are wired to the
//                  active DataWindow.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

IF IsValid(i_PCFindDW) THEN
   i_PCFindDW.TriggerEvent("pcd_Active")
END IF
end on

on destructor;//******************************************************************
//  PC Module     : uo_DW_Find
//  Event         : Destructor
//  Description   : Tears down the PowerClass Find Object.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

IF NOT IsNull(i_PCFindDW) THEN
   IF i_PCFindDW.i_Window <> i_Window THEN
      i_PCFindDW.Unwire_Find(THIS)
   END IF
END IF
end on

on po_validate;//******************************************************************
//  PC Module     : uo_DW_Find
//  Event         : po_Validate
//  Description   : Override the PowerObjects validation event and
//                  trigger the corresponding PowerClass validation
//                  event for backward compatibility.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

PCCA.Error = 0
TriggerEvent("pc_Validate")
i_FindError = PCCA.Error
end on

on constructor;call u_dw_find::constructor;//******************************************************************
//  PC Module     : uo_DW_Find
//  Event         : Constructor
//  Description   : Initializes the PowerClass Find Object.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

UO_CONTAINER_MAIN  l_Container

IF PARENT.TypeOf() = UserObject! THEN
   l_Container = PARENT
   i_Window    = l_Container.i_Window      
ELSE
   i_Window = PARENT
END IF

i_ClassName = i_Window.ClassName() + "." + ClassName()
SetNull(i_PCFindDW)
end on

