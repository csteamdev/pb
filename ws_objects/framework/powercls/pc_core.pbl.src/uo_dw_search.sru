﻿$PBExportHeader$uo_dw_search.sru
$PBExportComments$DW object for qualifying a data retrieval
forward
global type uo_dw_search from u_dw_search
end type
end forward

global type uo_dw_search from u_dw_search
event pc_validate pbm_custom74
end type
global uo_dw_search uo_dw_search

type variables
//------------------------------------------------------------------------------
//  Instance Variables in sorted order.
//------------------------------------------------------------------------------

STRING		i_ClassName

STRING		i_ObjectType 	= "uo_DW_Search"

UO_DW_MAIN	i_PCSearchDW

WINDOW		i_Window

end variables

forward prototypes
public function integer load_code (string search_column, string table_name, string column_code, string column_desc, string where_clause, string all_keyword)
public function integer refresh_code (string search_column)
public function string select_code (string search_column)
public function integer set_code (string search_column, string default_code)
public function integer disable_column (string search_column)
public function integer enable_column (string search_column)
public function integer hide_column (string search_column)
public function integer show_column (string search_column)
public subroutine reset_column ()
end prototypes

on pc_validate;call u_dw_search::pc_validate;////******************************************************************
////  PO Module     : uo_DW_Search
////  Event         : pc_Validate
////  Description   : Provides the opportunity for the developer to
////                  write code to validate the fields in this
////                  DataWindow.
////
////  Return Value  : INTEGER PCCA.Error
////
////                  If the developer codes the validation 
////                  testing, then the developer should set 
////                  this variable to one of the following
////                  validation return codes:
////
////                  c_ValOK     = The validation test passed and 
////                                the data is to be accepted.
////                  c_ValFailed = The validation test failed and 
////                                the data is to be rejected.  
////                                Do not allow the user to move 
////                                off of this field.
////                  c_ValFixed  = The validation test failed.  
////                                However, the data was able to be
////                                modified by developer to pass the
////                                validation test.  If a developer
////                                returns this code, they are 
////                                responsible for doing a SetItem()
////                                to place the fixed value into the
////                                column.  Note that if the 
////                                DataWindow validation routines 
////                                are used, they will do the 
////                                SetItem(), if necessary.
////
////  Change History:
////
////  Date     Person     Description of Change
////  -------- ---------- --------------------------------------------
////
////******************************************************************
////  Copyright ServerLogic 1992-1995.  All Rights Reserved.
////******************************************************************
//
////------------------------------------------------------------------
////  The following values are set for the developer before this
////  event is triggered:
////
////      STRING i_SearchValue -
////            The input the user has made.
////
////      STRING i_SearchColumn -
////            The name of the column being validated, folded
////            to lower case.
////
////      STRING i_SearchRow -
////            The row number of this DataWindow.  This value is
////            always equal to 1.
////
////      STRING i_SearchValMsg -
////            The validation error message provided by
////            PowerBuilder's extended column definition.
////
////      BOOLEAN i_SearchRequiredError -
////            This column is a required field and the user has
////            not entered anything.
////------------------------------------------------------------------
//
////------------------------------------------------------------------
////  Determine which column to validate.
////------------------------------------------------------------------
//
//CHOOSE CASE i_SearchColumn
//
////   //-------------------------------------------------------------
////   //  Enter the code for each column you want to validate.
////   //  <column> should be replaced by the name of the column
////   //  from the DataWindow.  Use the i_SearchError variable to
////   //  return the status of the validation.  Some sample
////   //  validation functions are listed below.  
////   //-------------------------------------------------------------
//
////   CASE "<column1>"
//
////      PCCA.Error = f_PO_ValInt(i_SearchValue, "##,##0", TRUE)
//
////   CASE "<column2>"
//
////      i_SearchError = f_PO_ValDec(i_SearchValue, "##,##0.00", TRUE)
////      IF PCCA.Error <> c_ValFailed THEN
////         IF Real(i_SearchValue) < 0 THEN
////            MessageBox("Search Object Error", "<error_message>")
////            PCCA.Error = c_ValFailed
////         END IF
////      END IF
//
////   CASE "<column3>"
//
////      IF Upper(Left(i_SearchValue, 1)) = "Y"  THEN
////         SetItem(i_SearchRow, i_SearchColumn, "Yes")
////         PCCA.Error = c_ValFixed
////      ELSE
////         IF Upper(Left(i_SearchValue, 1)) = "N"  THEN
////            SetItem(i_SearchRow, i_SearchColumn, "No")
////            PCCA.Error = c_ValFixed
////         ELSE
////            MessageBox("Search Object Error", &
////                "Valid responses are 'Y' and 'N'")
////            PCCA.Error = c_ValFailed
////         END IF
////      END IF
//
////   CASE "<column4>"
//
////      IF i_SearchRequiredError THEN
////         IF MessageBox("Search Object Error", &
////                "Do you want a default value of 'Active'?", &
////                Question!, YesNo!, 1) = 1 THEN
////            SetItem(i_SearchRow, i_SearchColumn, "Active")
////            PCCA.Error = c_ValFixed
////         ELSE
////            PCCA.Error = c_ValFailed
////         END IF
////      END IF
//
////   CASE "<column5>"  
////
////      Generate a default value for a CheckBox
////
////      IF i_SearchRequiredError THEN
////         SetItem(i_SearchRow, i_SearchColumn, "N")
////         PCCA.Error = c_ValFixed
////      END IF
//
//END CHOOSE
end on

public function integer load_code (string search_column, string table_name, string column_code, string column_desc, string where_clause, string all_keyword);//******************************************************************
//  PC Module     : uo_DW_Search
//  Function      : Load_Code
//  Description   : Load a code table using codes and descriptions
//                  from the database.
//
//  Parameters    : STRING Search_Column -
//                        The column name on this DataWindow to
//                        load.
//                  STRING Table_Name -
//                        Table from where the column with the
//                        code values resides.
//                  STRING Column_Code -
//                        Column name with the code code values.
//                  STRING Column_Desc -
//                        Column name with the code descriptions.
//                  STRING Where_Clause -
//                        WHERE clause statement to restrict the
//                        code values.
//                  STRING All_Keyword -
//                        Keyword to denote select all values
//                        (e.g. "(All)").
//
//  Return Value  : 0 = OK, -1 = Error
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

RETURN fu_LoadCode(search_column, table_name, column_code, &
                   column_desc, where_clause, all_keyword)

end function

public function integer refresh_code (string search_column);//******************************************************************
//  PC Module     : uo_DW_Search
//  Function      : Refresh_Code
//  Description   : Re-retrieves the data into the code tables for
//                  a column on this DataWindow.  Note that the 
//                  Load_Code() routine does the first retrieve.
//
//  Parameters    : STRING Search_Column -
//                         The column in this DataWindow that is
//                         to be re-loaded with data.
//
//  Return Value  : 0 = OK, -1 = Error
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

RETURN fu_RefreshCode(search_column)

end function

public function string select_code (string search_column);//******************************************************************
//  PC Module     : uo_DW_Search
//  Function      : Select_Code
//  Description   : Select the code associated with the given column
//                  in this DataWindow.
//
//  Parameters    : STRING Search_Column -
//                         Column in this DataWindow to select a
//                         code from.
//
//  Return Value  : STRING -  <code value> = OK, "" = NULL
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

RETURN fu_SelectCode(search_column)

end function

public function integer set_code (string search_column, string default_code);//******************************************************************
//  PC Module     : uo_DW_Search
//  Subroutine    : Set_Code
//  Description   : Sets a default value for a column in this
//                  DataWindow.
//
//  Parameters    : STRING Search_Column -
//                     The column in this DataWindow to set a
//                     default code for.
//                  STRING Default_Code -
//                     The value to set for this column.
//
//  Return Value  : 0 = set OK   -1 = set failed
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

RETURN fu_SetCode(search_column, default_code)

end function

public function integer disable_column (string search_column);//******************************************************************
//  PO Module     : uo_DW_Search
//  Subroutine    : Disable_Column
//  Description   : Make a column on this DataWindow disabled.
//
//  Parameters    : STRING Search_Column -
//                     The column in this DataWindow to set to
//                     disable.
//
//  Return Value  : 0 = OK     -1 = invalid column
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

RETURN fu_DisableColumn(search_column)


end function

public function integer enable_column (string search_column);//******************************************************************
//  PO Module     : uo_DW_Search
//  Subroutine    : Enable_Column
//  Description   : Make a column on this DataWindow enabled.
//
//  Parameters    : STRING Search_Column -
//                     The column in this DataWindow to set to
//                     enable.
//
//  Return Value  : 0 = OK     -1 = invalid column
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

RETURN fu_EnableColumn(search_column)


end function

public function integer hide_column (string search_column);//******************************************************************
//  PO Module     : uo_DW_Search
//  Subroutine    : Hide_Column
//  Description   : Make a column on this DataWindow invisible.
//
//  Parameters    : STRING Search_Column -
//                     The column in this DataWindow to set to
//                     invisible.
//
//  Return Value  : 0 = OK     -1 = invalid column
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

RETURN fu_HideColumn(search_column)


end function

public function integer show_column (string search_column);//******************************************************************
//  PO Module     : uo_DW_Search
//  Subroutine    : Show_Column
//  Description   : Make a column on this DataWindow visible.
//
//  Parameters    : STRING Search_Column -
//                     The column in this DataWindow to set to
//                     visible.
//
//  Return Value  : 0 = OK     -1 = invalid column
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

RETURN fu_ShowColumn(search_column)


end function

public subroutine reset_column ();//******************************************************************
//  PC Module     : uo_DW_Search
//  Subroutine    : Reset_Column
//  Description   : Clears the selections in this DataWindow.
//
//  Parameters    : (None)
//
//  Return Value  : 0 = OK,  -1 = insert error
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

fu_Reset()

end subroutine

on getfocus;call u_dw_search::getfocus;//******************************************************************
//  PC Module     : uo_DW_Search
//  Event         : GetFocus
//  Description   : Make the DataWindow we are wired to the
//                  active DataWindow.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

IF IsValid(i_PCSearchDW) THEN
   i_PCSearchDW.TriggerEvent("pcd_Active")
END IF
end on

on destructor;call u_dw_search::destructor;//******************************************************************
//  PC Module     : uo_DW_Search
//  Event         : Destructor
//  Description   : Tears down the PowerClass Search Object.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

IF NOT IsNull(i_PCSearchDW) THEN
   IF i_PCSearchDW.i_Window <> i_Window THEN
      i_PCSearchDW.Unwire_Search(THIS)
   END IF
END IF
end on

on constructor;call u_dw_search::constructor;//******************************************************************
//  PC Module     : uo_DW_Search
//  Event         : Constructor
//  Description   : Initializes the PowerClass Search Object.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

UO_CONTAINER_MAIN  l_Container

IF PARENT.TypeOf() = UserObject! THEN
   l_Container = PARENT
   i_Window    = l_Container.i_Window      
ELSE
   i_Window = PARENT
END IF

i_ClassName = i_Window.ClassName() + "." + ClassName()
SetNull(i_PCSearchDW)
end on

on po_validate;//******************************************************************
//  PC Module     : uo_DW_Search
//  Event         : po_Validate
//  Description   : Override the PowerObjects validation event and
//                  trigger the corresponding PowerClass validation
//                  event for backward compatibility.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

PCCA.Error = 0
TriggerEvent("pc_Validate")
i_SearchError = PCCA.Error
end on

