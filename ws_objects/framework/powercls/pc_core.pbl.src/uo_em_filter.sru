﻿$PBExportHeader$uo_em_filter.sru
$PBExportComments$Edit mask object for filtering data
forward
global type uo_em_filter from u_em_filter
end type
end forward

global type uo_em_filter from u_em_filter
int TextSize=-8
int Weight=700
string FaceName="System"
event pc_valempty pbm_custom72
event pc_validate pbm_custom73
end type
global uo_em_filter uo_em_filter

type variables
//------------------------------------------------------------------------------
//  Instance Variables in sorted order.
//------------------------------------------------------------------------------

STRING		i_ClassName

STRING		i_ObjectType 	= "uo_EM_Filter"

UO_DW_MAIN	i_PCFilterDW

WINDOW		i_Window

end variables

forward prototypes
public function string select_code ()
public function integer load_code (string table_name, string column_code, string column_desc, string where_clause, string all_keyword)
public function integer set_code (string default_code)
public function integer refresh_code ()
end prototypes

on pc_valempty;call u_em_filter::pc_valempty;////******************************************************************
////  PC Module     : uo_EM_Filter
////  Event         : pc_ValEmpty
////  Description   : Provides the opportunity for the developer to
////                  generate errors when the user has not made a
////                  selection in the EM object.
////
////  Return Value  : c_ValOK     = OK
////                  c_ValFailed = Error, criteria is required.
////
////  Change History:
////
////  Date     Person     Description of Change
////  -------- ---------- --------------------------------------------
////
////******************************************************************
////  Copyright ServerLogic 1994-1995.  All Rights Reserved.
////******************************************************************
//
////-------------------------------------------------------------------
////  Set the PCCA.Error variable to c_ValFailed to indicate that
////  this filter object is required.
////-------------------------------------------------------------------
//
////SetFocus()
////MessageBox(PCCA.Application_Name, &
////           "This is a required filter field", &
////           Exclamation!, Ok!)
////PCCA.Error = c_ValFailed
end on

on pc_validate;call u_em_filter::pc_validate;////******************************************************************
////  PC Module     : uo_EM_Filter
////  Event         : po_Validate
////  Description   : Provides the opportunity for the developer to
////                  write code to validate the fields in this
////                  object.
////
////  Return Value  : INTEGER PCCA.Error
////
////                  If the developer codes the validation 
////                  testing, then the developer should set 
////                  this variable to one of the following
////                  validation return codes:
////
////                  c_ValOK     = The validation test passed and 
////                                the data is to be accepted.
////                  c_ValFailed = The validation test failed and 
////                                the data is to be rejected.  
////                                Do not allow the user to move 
////                                off of this field.
////
////  Change History:
////
////  Date     Person     Description of Change
////  -------- ---------- --------------------------------------------
////
////******************************************************************
////  Copyright ServerLogic 1992-1995.  All Rights Reserved.
////******************************************************************
//
////------------------------------------------------------------------
////  The following values are set for the developer before this
////  event is triggered:
////
////      STRING i_FilterValue -
////            The input the user has made.
////------------------------------------------------------------------
//
////PCCA.Error = f_PO_ValMaxLen(i_FilterValue, 10, TRUE)

end on

public function string select_code ();//******************************************************************
//  PC Module     : uo_EM_Filter
//  Function      : Select_Code
//  Description   : Select the code associated with the description
//                  from a non-DataWindow edit mask spin box.
//
//  Parameters    : (None)
//
//  Return Value  : STRING         - <code value> = OK, "" = NULL
//
//  Change History:  
//    
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

RETURN fu_SelectCode()

end function

public function integer load_code (string table_name, string column_code, string column_desc, string where_clause, string all_keyword);//******************************************************************
//  PC Module     : uo_EM_Filter
//  Function      : Load_Codes
//  Description   : Load a non-DataWindow edit mask spin box using
//                  codes and descriptions from the database.
//
//  Parameters    : STRING  Table_Name   - Table from where the
//                                         column with the code
//                                         values resides.
//                  STRING  Column_Code  - column name with the code
//                                         values.
//                  STRING  Column_Desc  - column name with the code
//                                         descriptions.
//                  STRING  Where_Clause - WHERE clause statement to 
//                                         restrict the code values.
//                  STRING  All_Keyword  - Keyword to denote select
//                                         all values (e.g. "(All)").
//
//  Return Value  : 0 = OK, -1 = Error
//
//  Change History:  
//    
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

RETURN fu_LoadCode(table_name, column_code, column_desc, &
                   where_clause, all_keyword)

end function

public function integer set_code (string default_code);//******************************************************************
//  PC Module     : uo_EM_Filter
//  Subroutine    : Set_Code
//  Description   : Sets a default value for this object.
//
//  Parameters    : STRING Default_Code -
//                     The value to set for this object.
//
//  Return Value  : 0 = set OK   -1 = set failed
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

RETURN fu_SetCode(default_code)

end function

public function integer refresh_code ();//******************************************************************
//  PC Module     : uo_EM_Filter
//  Function      : Refresh_Code
//  Description   : Refresh an edit mask using codes and 
//                  descriptions from the database.
//
//  Parameters    : (None)
//
//  Return Value  : 0 = OK, -1 = Error
//
//  Change History:  
//    
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

RETURN fu_RefreshCode()
end function

on getfocus;//******************************************************************
//  PC Module     : uo_EM_Filter
//  Event         : GetFocus
//  Description   : Make the DataWindow we are wired to the
//                  active DataWindow.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

IF IsValid(i_PCFilterDW) THEN
   i_PCFilterDW.TriggerEvent("pcd_Active")
END IF
end on

on destructor;//******************************************************************
//  PC Module     : uo_EM_Filter
//  Event         : Destructor
//  Description   : Tears down the PowerClass Filter Object.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

IF NOT IsNull(i_PCFilterDW) THEN
   IF i_PCFilterDW.i_Window <> i_Window THEN
      i_PCFilterDW.Unwire_Filter(THIS)
   END IF
END IF
end on

on constructor;call u_em_filter::constructor;//******************************************************************
//  PC Module     : uo_EM_Filter
//  Event         : Constructor
//  Description   : Initializes the PowerClass Filter Object.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

UO_CONTAINER_MAIN  l_Container

IF PARENT.TypeOf() = UserObject! THEN
   l_Container = PARENT
   i_Window    = l_Container.i_Window      
ELSE
   i_Window = PARENT
END IF

i_ClassName = i_Window.ClassName() + "." + ClassName()
SetNull(i_PCFilterDW)
end on

on po_validate;//******************************************************************
//  PC Module     : uo_EM_Filter
//  Event         : po_Validate
//  Description   : Override the PowerObjects validation event and
//                  trigger the corresponding PowerClass validation
//                  event for backward compatibility.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

PCCA.Error = 0
TriggerEvent("pc_Validate")
i_FilterError = PCCA.Error
end on

on po_valempty;//******************************************************************
//  PC Module     : uo_EM_Filter
//  Event         : po_ValEmpty
//  Description   : Override the PowerObjects validation event and
//                  trigger the corresponding PowerClass validation
//                  event for backward compatibility.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

PCCA.Error = 0
TriggerEvent("pc_ValEmpty")
i_FilterError = PCCA.Error
end on

