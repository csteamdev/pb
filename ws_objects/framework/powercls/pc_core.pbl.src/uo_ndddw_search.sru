﻿$PBExportHeader$uo_ndddw_search.sru
$PBExportComments$String w/ Numeric Code DDDW object for qualifying a data retrieval
forward
global type uo_ndddw_search from uo_dddw_search_main
end type
end forward

global type uo_ndddw_search from uo_dddw_search_main
string DataObject="d_uodddw_ns"
end type
global uo_ndddw_search uo_ndddw_search

