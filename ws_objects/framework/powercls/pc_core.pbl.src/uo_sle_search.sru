﻿$PBExportHeader$uo_sle_search.sru
$PBExportComments$Single line edit object for qualifying a data retrieval
forward
global type uo_sle_search from u_sle_search
end type
end forward

global type uo_sle_search from u_sle_search
event pc_valempty pbm_custom72
event pc_validate pbm_custom73
end type
global uo_sle_search uo_sle_search

type variables
//------------------------------------------------------------------------------
//  Instance Variables in sorted order.
//------------------------------------------------------------------------------

STRING		i_ClassName

STRING		i_ObjectType 	= "uo_SLE_Search"

UO_DW_MAIN	i_PCSearchDW

WINDOW		i_Window

end variables

forward prototypes
public function integer set_code (string default_code)
end prototypes

on pc_valempty;call u_sle_search::pc_valempty;////******************************************************************
////  PC Module     : uo_SLE_Search
////  Event         : pc_ValEmpty
////  Description   : Provides the opportunity for the developer to
////                  generate errors when the user has not made a
////                  selection in the SLE object.
////
////  Return Value  : c_ValOK     = OK
////                  c_ValFailed = Error, criteria is required.
////
////  Change History:
////
////  Date     Person     Description of Change
////  -------- ---------- --------------------------------------------
////
////******************************************************************
////  Copyright ServerLogic 1994-1995.  All Rights Reserved.
////******************************************************************
//
////-------------------------------------------------------------------
////  Set the PCCA.Error variable to c_ValFailed to indicate that
////  this search object is required.
////-------------------------------------------------------------------
//
////SetFocus()
////MessageBox(PCCA.Application_Name, &
////           "This is a required search field", &
////           Exclamation!, Ok!)
////PCCA.Error = c_ValFailed

end on

on pc_validate;call u_sle_search::pc_validate;////******************************************************************
////  PC Module     : uo_SLE_Search
////  Event         : pc_Validate
////  Description   : Provides the opportunity for the developer to
////                  write code to validate the fields in this
////                  object.
////
////  Return Value  : INTEGER PCCA.Error
////
////                  If the developer codes the validation 
////                  testing, then the developer should set 
////                  this variable to one of the following
////                  validation return codes:
////
////                  c_ValOK     = The validation test passed and 
////                                the data is to be accepted.
////                  c_ValFailed = The validation test failed and 
////                                the data is to be rejected.  
////                                Do not allow the user to move 
////                                off of this field.
////
////  Change History:
////
////  Date     Person     Description of Change
////  -------- ---------- --------------------------------------------
////
////******************************************************************
////  Copyright ServerLogic 1992-1995.  All Rights Reserved.
////******************************************************************
//
////------------------------------------------------------------------
////  The following values are set for the developer before this
////  event is triggered:
////
////      STRING i_SearchValue -
////            The input the user has made.
////------------------------------------------------------------------
//
////PCCA.Error = f_PO_ValMaxLen(i_SearchValue, 10, TRUE)
end on

public function integer set_code (string default_code);//******************************************************************
//  PC Module     : uo_SLE_Search
//  Subroutine    : Set_Code
//  Description   : Sets a default value for this object..
//
//  Parameters    : STRING Default_Code -
//                     The value to set for this object.
//
//  Return Value  : 0 = set OK   -1 = set failed
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

RETURN fu_SetCode(default_code)
end function

on getfocus;call u_sle_search::getfocus;//******************************************************************
//  PC Module     : uo_SLE_Search
//  Event         : GetFocus
//  Description   : Make the DataWindow we are wired to the
//                  active DataWindow.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

IF IsValid(i_PCSearchDW) THEN
   i_PCSearchDW.TriggerEvent("pcd_Active")
END IF
end on

on destructor;call u_sle_search::destructor;//******************************************************************
//  PC Module     : uo_SLE_Search
//  Event         : Destructor
//  Description   : Tears down the PowerClass Search Object.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

IF NOT IsNull(i_PCSearchDW) THEN
   IF i_PCSearchDW.i_Window <> i_Window THEN
      i_PCSearchDW.Unwire_Search(THIS)
   END IF
END IF
end on

on constructor;call u_sle_search::constructor;//******************************************************************
//  PC Module     : uo_SLE_Search
//  Event         : Constructor
//  Description   : Initializes the PowerClass Search Object.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

UO_CONTAINER_MAIN  l_Container

IF PARENT.TypeOf() = UserObject! THEN
   l_Container = PARENT
   i_Window    = l_Container.i_Window      
ELSE
   i_Window = PARENT
END IF

i_ClassName = i_Window.ClassName() + "." + ClassName()
SetNull(i_PCSearchDW)
end on

on po_validate;//******************************************************************
//  PC Module     : uo_SLE_Search
//  Event         : po_Validate
//  Description   : Override the PowerObjects validation event and
//                  trigger the corresponding PowerClass validation
//                  event for backward compatibility.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

PCCA.Error = 0
TriggerEvent("pc_Validate")
i_SearchError = PCCA.Error
end on

on po_valempty;//******************************************************************
//  PC Module     : uo_SLE_Search
//  Event         : po_ValEmpty
//  Description   : Override the PowerObjects validation event and
//                  trigger the corresponding PowerClass validation
//                  event for backward compatibility.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

PCCA.Error = 0
TriggerEvent("pc_ValEmpty")
i_SearchError = PCCA.Error
end on

