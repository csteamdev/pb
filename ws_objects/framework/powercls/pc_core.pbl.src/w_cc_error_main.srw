﻿$PBExportHeader$w_cc_error_main.srw
$PBExportComments$PowerClass Ancestor window for providing user intervention during concurrency errors
forward
global type w_cc_error_main from w_response
end type
type p_exclamation from picture within w_cc_error_main
end type
type p_information from picture within w_cc_error_main
end type
type p_question from picture within w_cc_error_main
end type
type p_stopsign from picture within w_cc_error_main
end type
type st_errortype from statictext within w_cc_error_main
end type
type gb_errortype from groupbox within w_cc_error_main
end type
type cb_first from uo_cb_first within w_cc_error_main
end type
type cb_prev from uo_cb_previous within w_cc_error_main
end type
type cb_next from uo_cb_next within w_cc_error_main
end type
type cb_last from uo_cb_last within w_cc_error_main
end type
type gb_movement from groupbox within w_cc_error_main
end type
type rb_saveolddb from radiobutton within w_cc_error_main
end type
type rb_savenewdb from radiobutton within w_cc_error_main
end type
type rb_saveentered from radiobutton within w_cc_error_main
end type
type rb_savespecial from radiobutton within w_cc_error_main
end type
type gb_proccode from groupbox within w_cc_error_main
end type
type cb_ok from uo_cb_ok within w_cc_error_main
end type
type cb_cancel_cl from uo_cb_cancel within w_cc_error_main
end type
type cb_help from commandbutton within w_cc_error_main
end type
type cb_yes from commandbutton within w_cc_error_main
end type
type cb_no from commandbutton within w_cc_error_main
end type
type cb_cancel_rl from uo_cb_cancel within w_cc_error_main
end type
type dw_1 from uo_dw_main within w_cc_error_main
end type
end forward

global type w_cc_error_main from w_response
int Width=2195
int Height=1713
boolean TitleBar=true
string Title=""
boolean ControlMenu=false
p_exclamation p_exclamation
p_information p_information
p_question p_question
p_stopsign p_stopsign
st_errortype st_errortype
gb_errortype gb_errortype
cb_first cb_first
cb_prev cb_prev
cb_next cb_next
cb_last cb_last
gb_movement gb_movement
rb_saveolddb rb_saveolddb
rb_savenewdb rb_savenewdb
rb_saveentered rb_saveentered
rb_savespecial rb_savespecial
gb_proccode gb_proccode
cb_ok cb_ok
cb_cancel_cl cb_cancel_cl
cb_help cb_help
cb_yes cb_yes
cb_no cb_no
cb_cancel_rl cb_cancel_rl
dw_1 dw_1
end type
global w_cc_error_main w_cc_error_main

type variables
//------------------------------------------------------------------------------
//  Instance Variables in sorted order.
//------------------------------------------------------------------------------

BOOLEAN	i_Abort     	= FALSE

UO_DW_MAIN	i_CCDW
LONG		i_CCError 	= 0
INTEGER		i_CCList[]
STRING		i_CCRectVis
STRING		i_CCRectInvis

INTEGER		i_NumCCList

BOOLEAN	i_OkToClose 	= FALSE

BOOLEAN	i_RectOn 	= FALSE
INTEGER		i_RowHeight
BOOLEAN	i_RowLevelError

//------------------------------------------------------------------------------
//  Constant Variables in logical order.
//------------------------------------------------------------------------------

UNSIGNEDLONG	c_DarkGray
UNSIGNEDLONG	c_Black

REAL		c_CCBlinkTime 	= 0.5

PICTURE		c_Icons[]

end variables

forward prototypes
public subroutine show_error (long error_col)
public subroutine show_err_msg (unsignedlong error_nbr)
public subroutine set_rect_size (integer col_nbr)
end prototypes

public subroutine show_error (long error_col);//******************************************************************
//  PC Module     : w_CC_Error_Main
//  Subroutine    : Show_Error
//  Description   : Show_Error() subroutine for the Concurrency
//                  error window.
//
//  Parameters    : LONG Error_Col -
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

RADIOBUTTON  l_RB

IF Error_Col < 1 THEN
   Error_Col = 1
END IF
IF Error_Col > i_NumCCList THEN
   Error_Col = i_NumCCList
END IF

i_CCError        = Error_Col

cb_First.Enabled = (i_CCError > 1)
cb_Prev.Enabled  = cb_First.Enabled
cb_Next.Enabled  = (i_CCError <> i_NumCCList)
cb_Last.Enabled  = cb_Next.Enabled

//------------------------------------------------------------------

IF i_CCError > 0 THEN

   Error_Col = i_CCList[i_CCError]

   Set_Rect_Size(Error_Col)

   Show_Err_Msg(i_CCDW.i_CCInfo[Error_Col].Error_Code)

   //---------------------------------------------------------------

   CHOOSE CASE i_CCDW.i_CCStatus[Error_Col].Process_Code
      CASE c_CCProcUseOldDB
         l_RB = rb_SaveOldDB
      CASE c_CCProcUseNewDB
         l_RB = rb_SaveNewDB
      CASE c_CCProcUseDWValue
         l_RB = rb_SaveEntered
      CASE c_CCProcUseSpecial
         l_RB = rb_SaveSpecial
   END CHOOSE

   //---------------------------------------------------------------

   l_RB.Checked = TRUE
   TriggerEvent(l_RB, "Clicked")
END IF

//------------------------------------------------------------------

//gb_ProcCode.Enabled = (i_CCDW.i_CCInfo[Error_Col].Error_Code <> &
//                       c_CCErrorNone)

rb_SaveOldDB.Enabled   = gb_ProcCode.Enabled
rb_SaveNewDB.Enabled   = gb_ProcCode.Enabled
rb_SaveEntered.Enabled =                              &
   ((i_CCDW.i_CCInfo[Error_Col].Old_DB_Value       <> &
     i_CCDW.i_CCInfo[Error_Col].DW_Value)          OR &
    (i_CCDW.i_CCStatus[Error_Col].Process_Code   =    &
     c_CCProcUseDWValue))                         AND &
    gb_ProcCode.Enabled
IF IsNull(i_CCDW.i_CCStatus[Error_Col].Special_Value) THEN
   rb_SaveSpecial.Enabled = FALSE
ELSE
   rb_SaveSpecial.Enabled = gb_ProcCode.Enabled
END IF

RETURN
end subroutine

public subroutine show_err_msg (unsignedlong error_nbr);//******************************************************************
//  PC Module     : w_CC_Error_Main
//  Subroutine    : Show_Err_Msg
//  Description   : Show_Err_Msg() subroutine for the Concurrency
//                  error window.
//
//  Parameters    : UNSIGNEDLONG Error_Nbr -
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN       l_Enabled
INTEGER       l_DefaultButton, l_Idx
UNSIGNEDLONG  l_MBID
STRING        l_Title,         l_Text
BUTTON        l_ButtonSet
ICON          l_Icon
PICTURE       l_Picture

CHOOSE CASE Error_Nbr
   CASE c_CCErrorNone
      l_MBID = PCCA.MB.c_MBI_DW_CC_NoError
   CASE c_CCErrorNonOverlap
      IF IsNull(i_CCDW.i_CCUser) THEN
         l_MBID = PCCA.MB.c_MBI_DW_CC_NonOverLapValue
      ELSE
         l_MBID = PCCA.MB.c_MBI_DW_CC_NonOverLapValueUser
      END IF
   CASE c_CCErrorOverlap, c_CCErrorOverlapMatch
      IF IsNull(i_CCDW.i_CCUser) THEN
         l_MBID = PCCA.MB.c_MBI_DW_CC_OverLapValue
      ELSE
         l_MBID = PCCA.MB.c_MBI_DW_CC_OverLapValueUser
      END IF
   CASE c_CCErrorNonExistent
      IF i_CCDW.i_EnableCCInsert THEN
         IF IsNull(i_CCDW.i_CCUser) THEN
            l_MBID = PCCA.MB.c_MBI_DW_CC_NonExistent
         ELSE
            l_MBID = PCCA.MB.c_MBI_DW_CC_NonExistentUser
         END IF
      ELSE
         IF IsNull(i_CCDW.i_CCUser) THEN
            l_MBID = PCCA.MB.c_MBI_DW_CC_DropNonExistent
         ELSE
            l_MBID = PCCA.MB.c_MBI_DW_CC_DropNonExistentUser
         END IF
      END IF
   CASE c_CCErrorDeleteModified
      IF IsNull(i_CCDW.i_CCUser) THEN
         l_MBID = PCCA.MB.c_MBI_DW_CC_DeleteModified
      ELSE
         l_MBID = PCCA.MB.c_MBI_DW_CC_DeleteModifiedUser
      END IF
   CASE c_CCErrorKeyConflict
      l_MBID = PCCA.MB.c_MBI_DW_CC_KeyConflictValue
   CASE ELSE
      l_MBID = 0
END CHOOSE

//------------------------------------------------------------------

IF l_MBID = 0 THEN
   st_ErrorType.Text = "Undefined error"
   l_Picture         = p_StopSign
ELSE
   PCCA.MB.i_MB_Strings[1] = "Show_Err_Msg()"
   PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
   PCCA.MB.i_MB_Strings[3] = i_CCDW.i_ClassName
   PCCA.MB.i_MB_Strings[4] = i_CCDW.i_Window.Title
   PCCA.MB.i_MB_Strings[5] = i_CCDW.DataObject
   PCCA.MB.i_MB_Strings[6] = i_CCDW.i_CCUser

   IF IsNull(i_CCDW.i_CCUser) THEN
      l_Idx = 5
   ELSE
      l_Idx = 6
   END IF

   PCCA.MB.fu_GetInfo(l_MBID,                               &
                   0,           PCCA.MB.i_MB_Numbers[],     &
                   l_Idx,       PCCA.MB.i_MB_Strings[],     &
                   l_Title,     l_Text,          l_Icon, &
                   l_ButtonSet, l_DefaultButton, l_Enabled)

   st_ErrorType.Text = l_Text

   IF IsNull(Icon) THEN
      l_Picture = c_NullPicture
   ELSE
      CHOOSE CASE l_Icon
         CASE Information!
            l_Picture = p_Information
         CASE StopSign!
            l_Picture = p_StopSign
         CASE Exclamation!
            l_Picture = p_Exclamation
         CASE Question!
            l_Picture = p_Question
         //CASE None!
         CASE ELSE
            l_Picture = c_NullPicture
      END CHOOSE
   END IF
END IF

FOR l_Idx = 1 TO UpperBound(c_Icons[])
   IF IsNull(l_Picture) THEN
      IF c_Icons[l_Idx].Visible THEN
         c_Icons[l_Idx].Visible = FALSE
      END IF
   ELSE
      IF c_Icons[l_Idx] = l_Picture THEN
         IF NOT c_Icons[l_Idx].Visible THEN
            c_Icons[l_Idx].Visible = TRUE
         END IF
      ELSE
         IF c_Icons[l_Idx].Visible THEN
            c_Icons[l_Idx].Visible = FALSE
         END IF
      END IF
   END IF
NEXT

RETURN
end subroutine

public subroutine set_rect_size (integer col_nbr);//******************************************************************
//  PC Module     : w_CC_Error_Main
//  Subroutine    : Set_Rect_Size
//  Description   : Set_Rect_Size() subroutine for the Concurrency
//                  error window.
//
//  Parameters    : INTEGER  Col_Nbr -
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN  l_First
INTEGER  l_X1,  l_Y1,  l_X2,     l_Y2
INTEGER  l_X,          l_Y
INTEGER  l_Pixels,     l_Idx
INTEGER  l_Width,      l_Height
STRING   l_Describe,   l_Modify, l_Result
STRING   l_SX1, l_SY1, l_SX2,    l_SY2

IF c_CCBlinkTime > 0 THEN
   Timer(0, THIS)
END IF

//------------------------------------------------------------------

IF Col_Nbr < 1 OR Col_Nbr > i_CCDW.i_NumColumns THEN
   l_First = TRUE
   FOR l_Idx = 1 TO i_CCDW.i_NumColumns
      IF i_CCDW.i_ColVisible[l_Idx] THEN
         l_Describe = "#" + String(l_Idx) + ".X"
         l_Result   = dw_1.Describe(l_Describe)
         l_X        = Integer(l_Result)

         l_Describe = "#" + String(l_Idx) + ".Y"
         l_Result   = dw_1.Describe(l_Describe)
         l_Y        = Integer(l_Result)

         l_Describe = "#" + String(l_Idx) + ".Width"
         l_Result   = dw_1.Describe(l_Describe)
         l_Width    = Integer(l_Result)

         l_Describe = "#" + String(l_Idx) + ".Height"
         l_Result   = dw_1.Describe(l_Describe)
         l_Height   = Integer(l_Result)

         IF l_First THEN
            l_First = FALSE
            l_X1    = l_X
            l_Y1    = l_Y
            l_X2    = l_X + l_Width
            l_Y2    = l_Y + l_Height
         ELSE
            IF l_X1 > l_X THEN
               l_X1 = l_X
            END IF
            IF l_X2 < l_X + l_Width THEN
               l_X2 = l_X + l_Width
            END IF

            IF l_Y1 > l_Y THEN
               l_Y1 = l_Y
            END IF
            IF l_Y2 < l_Y + l_Height THEN
               l_Y2 = l_Y + l_Height
            END IF
         END IF
      END IF
   NEXT
   l_X      = l_X1
   l_Y      = l_Y1
   l_Width  = l_X2 - l_X1
   l_Height = l_Y2 - l_Y1
ELSE
   l_Describe = "#" + String(Col_Nbr) + ".X"
   l_Result   = dw_1.Describe(l_Describe)
   l_X        = Integer(l_Result)

   l_Describe = "#" + String(Col_Nbr) + ".Y"
   l_Result   = dw_1.Describe(l_Describe)
   l_Y        = Integer(l_Result)

   l_Describe = "#" + String(Col_Nbr) + ".Width"
   l_Result   = dw_1.Describe(l_Describe)
   l_Width    = Integer(l_Result)

   l_Describe = "#" + String(Col_Nbr) + ".Height"
   l_Result   = dw_1.Describe(l_Describe)
   l_Height   = Integer(l_Result)
END IF

//------------------------------------------------------------------

l_Result = dw_1.Modify(i_CCRectInvis)

//------------------------------------------------------------------

l_Pixels = 1
l_SX1    = String(l_X - PixelsToUnits &
                          (l_Pixels, XPixelsToUnits!))
l_SX2    = String(l_X + l_Width + PixelsToUnits &
                                    (l_Pixels, XPixelsToUnits!))
l_SY1    = String(l_Y - PixelsToUnits &
                          (l_Pixels, YPixelsToUnits!))
l_SY2    = String(l_Y + l_Height + PixelsToUnits &
                                      (l_Pixels, YPixelsToUnits!))

//------------------------------------------------------------------

l_Modify = "CC_Line_1.X1="  + l_SX1 + &
           " CC_Line_1.Y1=" + l_SY1 + &
           " CC_Line_1.X2=" + l_SX2 + &
           " CC_Line_1.Y2=" + l_SY1
l_Result = dw_1.Modify(l_Modify)

//------------------------------------------------------------------

l_Modify = "CC_Line_2.X1="  + l_SX2 + &
           " CC_Line_2.Y1=" + l_SY1 + &
           " CC_Line_2.X2=" + l_SX2 + &
           " CC_Line_2.Y2=" + l_SY2
l_Result = dw_1.Modify(l_Modify)

//------------------------------------------------------------------

l_Modify = "CC_Line_3.X1="  + l_SX2 + &
           " CC_Line_3.Y1=" + l_SY2 + &
           " CC_Line_3.X2=" + l_SX1 + &
           " CC_Line_3.Y2=" + l_SY2
l_Result = dw_1.Modify(l_Modify)

//------------------------------------------------------------------

l_Modify = "CC_Line_4.X1="  + l_SX1 + &
           " CC_Line_4.Y1=" + l_SY2 + &
           " CC_Line_4.X2=" + l_SX1 + &
           " CC_Line_4.Y2=" + l_SY1
l_Result = dw_1.Modify(l_Modify)

//------------------------------------------------------------------

i_RectOn = TRUE
l_Result = dw_1.Modify(i_CCRectVis)

INTEGER  l_Min, l_Max, l_New

IF Col_Nbr >= 1 AND Col_Nbr <= i_CCDW.i_NumColumns THEN
   l_Describe  = "DataWindow.HorizontalScrollPosition"
   l_Result    = dw_1.Describe(l_Describe)
   l_Min       = Integer(l_Result)
   l_Max       = l_Min + dw_1.Width
   l_New       = l_Min
   l_X1        = Integer(l_SX1)
   l_X2        = Integer(l_SX2)

   IF l_X1 + l_X2 > 2 * l_Max OR &
      l_X1 + l_X2 < 2 * l_Min THEN
      IF l_X1 < 10 THEN
         l_New = 0
      ELSE
         l_New = l_X1 - 10
      END IF

      l_Modify = "DataWindow.HorizontalScrollPosition=" + &
                 String(l_New)
      l_Result = dw_1.Modify(l_Modify)
   END IF

   l_Describe  = "DataWindow.VerticalScrollPosition"
   l_Result    = dw_1.Describe(l_Describe)
   l_Min       = Integer(l_Result)
   l_Max       = l_Min + i_RowHeight
   l_New       = l_Min
   l_Y1        = Integer(l_SY1)
   l_Y2        = Integer(l_SY2)

   IF l_Y1 + l_Y2 > 2 * l_Max OR &
      l_Y1 + l_Y2 < 2 * l_Min THEN
      IF l_Y1 < 10 THEN
         l_New = 0
      ELSE
         l_New = l_Y1 - 10
      END IF

      l_Modify = "DataWindow.VerticalScrollPosition=" + &
                 String(l_New)
      l_Result = dw_1.Modify(l_Modify)
   END IF

END IF

//------------------------------------------------------------------

IF c_CCBlinkTime > 0 THEN
   Timer(c_CCBlinkTime, THIS)
END IF

RETURN
end subroutine

on pc_setwindow;call w_response::pc_setwindow;//******************************************************************
//  PC Module     : w_CC_Error_Main
//  Event         : pc_SetWindow
//  Description   : pc_SetWindow script for the Concurrency
//                  error window.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER          l_TitleHeight
INTEGER          l_ControlMenuWidth, l_SBWidth
INTEGER          l_OrgDWWidth
INTEGER          l_Header,      l_Detail
INTEGER          l_Summary,     l_Footer
INTEGER          l_DWHeight
INTEGER          l_Width,       l_Height
INTEGER          l_DeltaWidth,  l_DeltaHeight
INTEGER          l_Check,       l_NumStyles
INTEGER          l_Idx,         l_Jdx,              l_Kdx
LONG             l_ScreenWidth, l_ScreenHeight
STRING           l_CCRectColor, l_CreateLine,       l_Col
STRING           l_Describe,    l_Modify,           l_Result
STRING           l_EditStyles[]
DATAWINDOWCHILD  l_DWC1,        l_DWC2

Set_W_Options(c_NoLoadCodeTable + &
              c_NoEnablePopup   + &
              c_NoResizeWin     + &
              c_NoAutoMinimize  + &
              c_NoAutoSize      + &
              c_AutoPosition    + &
              c_NoSavePosition  + &
              c_CloseNoSave     + &
              c_ToolBarNone)

//------------------------------------------------------------------

dw_1.DataObject   = i_CCDW.DataObject
dw_1.i_NumColumns = i_CCDW.i_NumColumns

FOR l_Idx = 1 TO i_CCDW.i_NumColumns
   IF i_CCDW.i_TabOrder[l_Idx] >= 0 THEN
      IF i_CCDW.i_ColVisible[l_Idx] THEN
         l_Modify = "#" + String(l_Idx) + ".Visible=1"
      ELSE
         l_Modify = "#" + String(l_Idx) + ".Visible=0"
      END IF

      l_Result = dw_1.Modify(l_Modify)
   END IF
NEXT

dw_1.Modify(i_CCDW.i_DisableTabs)

IF i_CCDW.i_ViewModeBorderIdx <> PCCA.MDI.c_DW_BorderUndefined THEN
   dw_1.Modify(i_CCDW.i_ViewBorders)
END IF

IF i_CCDW.i_ViewModeColorIdx <> PCCA.MDI.c_ColorUndefined THEN
   dw_1.Modify(i_CCDW.i_ViewColors)
END IF

IF i_CCDW.i_InactiveDWColorIdx <> PCCA.MDI.c_ColorUndefined THEN
   dw_1.Modify(i_CCDW.i_ActiveNonEmptyColors)
END IF

//------------------------------------------------------------------
//  We can not find out what edit style the column is from
//  PowerBuilder.  Therefore, we have to cycle through all the
//  possible types seeing if we get a valid response.  Set up
//  an array containing all known styles.
//------------------------------------------------------------------

l_EditStyles[1] = ".Edit"
l_EditStyles[2] = ".EditMask"
l_EditStyles[3] = ".DDLB"
l_EditStyles[4] = ".DDDW"
//l_EditStyles[5] = ".CheckBox"
//l_EditStyles[6] = ".RadioButton"
l_NumStyles     = UpperBound(l_EditStyles[])

//------------------------------------------------------------------
//  For each edit style that we know about, see if this column
//  will respond to a DESCRIBE().
//------------------------------------------------------------------

FOR l_Idx = 1 TO i_CCDW.i_NumColumns

   l_Col = "#" + String(l_Idx)

   FOR l_Jdx = 1 TO l_NumStyles
      l_Describe = l_Col + l_EditStyles[l_Jdx] + ".Required"
      l_Result   = Upper(i_CCDW.Describe(l_Describe))
      IF l_Result <> "?" THEN

         //---------------------------------------------------------
         //  The column responded.  Now, load the code tables for
         //  it.
         //---------------------------------------------------------

         CHOOSE CASE l_EditStyles[l_Jdx]

            //------------------------------------------------------
            //  Share the code tables for DDDW's.
            //------------------------------------------------------

            CASE ".DDDW"
               i_CCDW.GetChild(i_CCDW.i_xColumnNames[l_Idx], l_DWC1)
               dw_1.GetChild(i_CCDW.i_xColumnNames[l_Idx],   l_DWC2)
               l_DWC2.SetTransObject(i_CCDW.i_DBCA)
               l_DWC1.ShareData(l_DWC2)

            //------------------------------------------------------
            //  Copy the code tables for DDLB's.
            //------------------------------------------------------

            CASE ".DDLB"
               l_Kdx    = 1
               l_Result = i_CCDW.GetValue(l_Idx, l_Kdx)
               DO WHILE l_Result <> ""
                  dw_1.SetValue(l_Idx, l_Kdx, l_Result)
                  l_Kdx    = l_Kdx + 1
                  l_Result = i_CCDW.GetValue(l_Idx, l_Kdx)
               LOOP

            //------------------------------------------------------
            //  EditMasks have code tables, but PowerBuilder does
            //  not support loading them dynamically.
            //------------------------------------------------------

            CASE ".EditMask"

            //------------------------------------------------------
            //  Edit, CheckBox, and RadioButton styles don't
            //  support code tables.
            //------------------------------------------------------

            //CASE ".Edit"
            CASE ELSE
         END CHOOSE
      END IF
   NEXT
NEXT

//------------------------------------------------------------------
//  We need to create a rectangle on the DataWindow that is used
//  to highlight the current column.
//------------------------------------------------------------------

l_CreateLine = 'Create Line ('         + &
               ' Band=Detail'          + &
               ' X1="-2" Y1="-2"'      + &
               ' X2="-1" Y2="-1"'      + &
               ' Pen.Style="0"'        + &
               ' Pen.Width="5"'        + &
               ' Background.Mode="2"'  + &
               ' Background.Color="0"' + &
               ' Name=CC_Line_'

l_Modify     = ""
FOR l_Idx = 1 TO 4
   l_Modify = l_Modify + l_CreateLine + String(l_Idx) + ') '
NEXT

l_Result = dw_1.Modify(l_Modify)

i_CCRectVis   = ""
i_CCRectInvis = ""
l_CCRectColor = PCCA.MDI.c_RGBStrings[i_CCDW.i_CCErrorColorIdx]

FOR l_Idx = 1 TO 4
   l_Result      = "CC_Line_"  + String(l_Idx)
   i_CCRectVis   = i_CCRectVis + l_Result + &
                   ".Visible='0~tIf(GetRow()=1,1,0)' "
   i_CCRectInvis = i_CCRectInvis + &
                   l_Result + ".Visible=0 "

   //---------------------------------------------------------------
   //  Just in case the color changed, send it down again.
   //---------------------------------------------------------------

   l_Modify = l_Result + ".Pen.Color=" + l_CCRectColor
   l_Result = dw_1.Modify(l_Modify)
NEXT

l_Check = i_CCDW.RowsCopy                                       &
             (i_CCDW.i_CCUserRow, i_CCDW.i_CCUserRow, Primary!, &
              dw_1,               1,                  Primary!)

dw_1.SetItemStatus(1, 0, Primary!, DataModified!)
dw_1.SetItemStatus(1, 0, Primary!, NotModified!)

//------------------------------------------------------------------
//  Delete the row that was used for user intervention.
//------------------------------------------------------------------

i_CCDW.DeleteRow(i_CCDW.i_CCUserRow)

//------------------------------------------------------------------
//  Make sure that redraw is on for the DataWindow that contains
//  the concurrency error.
//------------------------------------------------------------------

i_CCDW.Push_DW_Redraw(c_DrawNow)

//------------------------------------------------------------------

PCCA.EXT.fu_GetScreenSize(l_ScreenWidth, l_ScreenHeight)

l_Result   = dw_1.Describe("DataWindow.Header.Height")
l_Header   = Integer(l_Result)
l_Result   = dw_1.Describe("DataWindow.Detail.Height")
l_Detail   = Integer(l_Result) + PixelsToUnits(3, YPixelsToUnits!)
l_Result   = dw_1.Describe("DataWindow.Summary.Height")
l_Summary  = Integer(l_Result)
l_Result   = dw_1.Describe("DataWindow.Footer.Height")
l_Footer   = Integer(l_Result)
l_DWHeight = l_Header + l_Detail + l_Summary + l_Footer

PCCA.EXT.fu_GetWindowInfo(l_TitleHeight, l_ControlMenuWidth)
l_SBWidth     = l_ControlMenuWidth

l_Height      = Height
l_DeltaHeight = (l_DWHeight + l_SBWidth) - dw_1.Height
l_Height      = l_Height    + l_DeltaHeight

l_OrgDWWidth  = dw_1.Width
l_Width       = Width
l_DeltaWidth  = i_CCDW.Width - dw_1.Width

IF l_Height > l_ScreenHeight THEN
   l_DeltaHeight = l_DeltaHeight + (l_ScreenHeight - l_Height)
   l_DeltaWidth  = l_DeltaWidth  + l_SBWidth
   l_Height      = l_ScreenHeight
END IF

IF l_DeltaWidth > 0 THEN
   l_Width = l_Width + l_DeltaWidth
END IF

IF l_Width > l_ScreenWidth THEN
   IF l_DeltaWidth < 0 THEN
      IF l_DeltaWidth > l_ScreenWidth - l_Width THEN
         l_DeltaWidth = l_ScreenWidth - l_Width
      END IF
   ELSE
      l_DeltaWidth = l_DeltaWidth + (l_ScreenWidth - l_Width)
   END IF
   l_Width = l_ScreenWidth
END IF

Resize(l_Width, l_Height)

dw_1.Width  = dw_1.Width  + l_DeltaWidth
dw_1.Height = dw_1.Height + l_DeltaHeight
i_RowHeight = dw_1.Height - (l_Header + l_Summary + l_Footer)

IF dw_1.Width < l_OrgDWWidth THEN
   dw_1.Move(dw_1.X + (l_OrgDWWidth - dw_1.Width) / 2, dw_1.Y)
END IF

IF i_RowLevelError THEN

   cb_First.Visible       = FALSE
   cb_Prev.Visible        = FALSE
   cb_Next.Visible        = FALSE
   cb_Last.Visible        = FALSE
   gb_Movement.Visible    = FALSE
   rb_SaveOldDB.Visible   = FALSE
   rb_SaveNewDB.Visible   = FALSE
   rb_SaveEntered.Visible = FALSE
   rb_SaveSpecial.Visible = FALSE
   gb_ProcCode.Visible    = FALSE
   cb_Ok.Visible          = FALSE
   cb_Cancel_CL.Visible   = FALSE
   cb_Help.Visible        = FALSE

   st_ErrorType.Height    = st_ErrorType.Height + &
                               (dw_1.Y - gb_Movement.Y)
   gb_ErrorType.Height    = gb_ErrorType.Height + &
                               (dw_1.Y - gb_Movement.Y)

   IF i_CCDW.i_CCErrorCode = c_CCErrorNonExistent THEN
   IF NOT i_CCDW.i_EnableCCInsert                 THEN
      cb_Yes.Enabled = FALSE
   END IF
   END IF
ELSE
   cb_Yes.Visible       = FALSE
   cb_No.Visible        = FALSE
   cb_Cancel_RL.Visible = FALSE
END IF
end on

on open;call w_response::open;//******************************************************************
//  PC Module     : w_CC_Error_Main
//  Event         : Open
//  Description   : Open script for the Concurrency
//                  error window.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_LowIdx, l_Idx, l_Jdx, l_Tmp
LONG     l_I_X,    l_I_Y
LONG     l_S_X,    l_S_Y
LONG     l_E_X,    l_E_Y
LONG     l_Q_X,    l_Q_Y
BLOB     l_Information
BLOB     l_StopSign
BLOB     l_Exclamation
BLOB     l_Question

IF PCCA.Error <> c_Success THEN
   i_Abort = TRUE
   Close(THIS)
   RETURN
END IF

Title = i_CCDW.i_Window.Title

IF NOT i_RowLevelError THEN

   //---------------------------------------------------------------
   //  Make a list of all the columns that require user
   //  intervention.
   //---------------------------------------------------------------

   FOR l_Idx = 1 TO i_CCDW.i_NumColumns
      IF i_CCDW.i_CCStatus[l_Idx].User_Code = &
         c_CCUserSpecify THEN
         i_NumCCList           = i_NumCCList + 1
         i_CCList[i_NumCCList] = l_Idx
      END IF
   NEXT

   //---------------------------------------------------------------
   //  Sort the columns in error by their tab order.
   //---------------------------------------------------------------

   FOR l_Idx = 1 TO i_NumCCList - 1
      l_LowIdx = l_Idx
      FOR l_Jdx = l_Idx + 1 TO i_NumCCList
         IF i_CCDW.i_TabOrder[i_CCList[l_LowIdx]] > &
            i_CCDW.i_TabOrder[i_CCList[l_Jdx]] THEN
            l_LowIdx = l_Jdx
         END IF
      NEXT
      l_Tmp              = i_CCList[l_Idx]
      i_CCList[l_Idx]    = i_CCList[l_LowIdx]
      i_CCList[l_LowIdx] = l_Tmp
   NEXT

   //---------------------------------------------------------------

END IF

//------------------------------------------------------------------
//  Set up the Icons (e.g. Stopsign).
//------------------------------------------------------------------

c_Icons[1]                = p_Information
c_Icons[2]                = p_StopSign
c_Icons[3]                = p_Exclamation
c_Icons[4]                = p_Question

c_Icons[1].Enabled        = FALSE
c_Icons[1].Visible        = FALSE
c_Icons[1].FocusRectangle = FALSE

FOR l_Idx = 2 TO UpperBound(c_Icons[])
   c_Icons[l_Idx].X              = c_Icons[1].X
   c_Icons[l_Idx].Y              = c_Icons[1].Y
   c_Icons[l_Idx].Enabled        = c_Icons[1].Enabled
   c_Icons[l_Idx].Visible        = c_Icons[1].Visible
   c_Icons[l_Idx].Border         = c_Icons[1].Border
   c_Icons[l_Idx].BorderStyle    = c_Icons[1].BorderStyle
   c_Icons[l_Idx].FocusRectangle = c_Icons[1].FocusRectangle
NEXT

//------------------------------------------------------------------

PCCA.EXT.fu_GetBitmap(PCCA.EXT.c_InformationBitmap, &
                      l_Information, l_I_X, l_I_Y)
p_Information.Resize(PixelsToUnits(l_I_X, XPixelsToUnits!), &
                     PixelsToUnits(l_I_Y, YPixelsToUnits!))
p_Information.SetPicture(l_Information)

//------------------------------------------------------------------

PCCA.EXT.fu_GetBitmap(PCCA.EXT.c_StopSignBitmap, &
                      l_StopSign, l_S_X, l_S_Y)
p_StopSign.Resize(PixelsToUnits(l_S_X, XPixelsToUnits!), &
                  PixelsToUnits(l_S_Y, YPixelsToUnits!))
p_StopSign.SetPicture(l_StopSign)

//------------------------------------------------------------------

PCCA.EXT.fu_GetBitmap(PCCA.EXT.c_ExclamationBitmap, &
                      l_Exclamation, l_E_X, l_E_Y)
p_Exclamation.Resize(PixelsToUnits(l_E_X, XPixelsToUnits!), &
                     PixelsToUnits(l_E_Y, YPixelsToUnits!))
p_Exclamation.SetPicture(l_Exclamation)

//------------------------------------------------------------------

PCCA.EXT.fu_GetBitmap(PCCA.EXT.c_QuestionBitmap, &
                      l_Question, l_Q_X, l_Q_Y)
p_Question.Resize(PixelsToUnits(l_Q_X, XPixelsToUnits!), &
                  PixelsToUnits(l_Q_Y, YPixelsToUnits!))
p_Question.SetPicture(l_Question)

//------------------------------------------------------------------

c_DarkGray = RGB(127, 127, 127)
c_Black    = RGB(  0,   0,   0)

//------------------------------------------------------------------
//  Show the first column in error.
//------------------------------------------------------------------

IF i_RowLevelError THEN
   Set_Rect_Size(0)
   Show_Err_Msg(i_CCDW.i_CCErrorCode)
ELSE
   Show_Error(1)
END IF
end on

on closequery;call w_response::closequery;//******************************************************************
//  PC Module     : w_CC_Error_Main
//  Event         : CloseQuery
//  Description   : CloseQuery script for the Concurrency
//                  error window.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

IF i_Abort THEN
   IF IsValid(i_CCDW) THEN
      i_CCDW.i_CCUserStatus = c_CCUserCancel
   END IF
   RETURN
END IF

IF NOT i_OkToClose THEN
   PCCA.MB.i_MB_Strings[1] = "CloseQuery"
   PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
   PCCA.MB.i_MB_Strings[3] = i_ClassName
   PCCA.MB.i_MB_Strings[4] = i_CCDW.i_Window.Title
   IF i_RowLevelError THEN
      PCCA.MB.i_MB_Strings[5] = "~"YES~", ~"NO~", ~"CANCEL~""
   ELSE
      PCCA.MB.i_MB_Strings[5] = "~"YES~", ~"NO~", ~"CANCEL~""
   END IF
   PCCA.MB.fu_MessageBox(PCCA.MB.c_MBI_CloseNotAllowed, &
                      0, PCCA.MB.i_MB_Numbers[],     &
                      5, PCCA.MB.i_MB_Strings[])
   Message.ReturnValue = 1
   RETURN
END IF
end on

on close;call w_response::close;//******************************************************************
//  PC Module     : w_CC_Error_Main
//  Event         : Close
//  Description   : Close script for the Concurrency
//                  error window.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

STRING  l_Result

IF c_CCBlinkTime > 0 THEN
   Timer(0, THIS)
END IF

//------------------------------------------------------------------
//  Turn off the rectangle used for highlighting the columns in
//  error.
//------------------------------------------------------------------

IF IsValid(i_CCDW) THEN
   l_Result = dw_1.Modify(i_CCRectInvis)
END IF

end on

on timer;call w_response::timer;//******************************************************************
//  PC Module     : w_CC_Error_Main
//  Event         : Timer
//  Description   : Timer script for the Concurrency
//                  error window.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

STRING  l_Result

IF i_RectOn THEN
   l_Result = dw_1.Modify(i_CCRectInvis)
ELSE
   l_Result = dw_1.Modify(i_CCRectVis)
END IF

i_RectOn = (NOT i_RectOn)
end on

on pc_cancel;//******************************************************************
//  PC Module     : w_CC_Error_Main
//  Event         : pc_Cancel
//  Description   : pc_Cancel script for the Concurrency
//                  error window.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

i_CCDW.i_CCUserStatus = c_CCUserCancel
i_OkToClose           = TRUE

Close(THIS)
end on

on pc_first;//******************************************************************
//  PC Module     : w_CC_Error_Main
//  Event         : pc_First
//  Description   : pc_First script for the Concurrency
//                  error window.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

Show_Error(1)
end on

on pc_last;//******************************************************************
//  PC Module     : w_CC_Error_Main
//  Event         : pc_Last
//  Description   : pc_Last script for the Concurrency
//                  error window.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

Show_Error(i_CCDW.i_NumColumns)
end on

on pc_next;//******************************************************************
//  PC Module     : w_CC_Error_Main
//  Event         : pc_Next
//  Description   : pc_Next script for the Concurrency
//                  error window.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

Show_Error(i_CCError + 1)
end on

on pc_previous;//******************************************************************
//  PC Module     : w_CC_Error_Main
//  Event         : pc_Previous
//  Description   : pc_Previous script for the Concurrency
//                  error window.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

Show_Error(i_CCError - 1)
end on

on pc_setvariables;call w_response::pc_setvariables;//******************************************************************
//  PC Module     : w_CC_Error_Main
//  Event         : pc_SetVariables
//  Description   : pc_SetVariables script for the Concurrency
//                  error window.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

i_CCDW = i_OpenParm

IF IsValid(i_CCDW) THEN
ELSE
   PCCA.Error = c_Fatal
   RETURN
END IF

i_RowLevelError = (i_CCDW.i_CCErrorCode = c_CCErrorNonExistent OR &
                   i_CCDW.i_CCErrorCode = c_CCErrorDeleteModified)
end on

on w_cc_error_main.create
int iCurrent
call w_response::create
this.p_exclamation=create p_exclamation
this.p_information=create p_information
this.p_question=create p_question
this.p_stopsign=create p_stopsign
this.st_errortype=create st_errortype
this.gb_errortype=create gb_errortype
this.cb_first=create cb_first
this.cb_prev=create cb_prev
this.cb_next=create cb_next
this.cb_last=create cb_last
this.gb_movement=create gb_movement
this.rb_saveolddb=create rb_saveolddb
this.rb_savenewdb=create rb_savenewdb
this.rb_saveentered=create rb_saveentered
this.rb_savespecial=create rb_savespecial
this.gb_proccode=create gb_proccode
this.cb_ok=create cb_ok
this.cb_cancel_cl=create cb_cancel_cl
this.cb_help=create cb_help
this.cb_yes=create cb_yes
this.cb_no=create cb_no
this.cb_cancel_rl=create cb_cancel_rl
this.dw_1=create dw_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=p_exclamation
this.Control[iCurrent+2]=p_information
this.Control[iCurrent+3]=p_question
this.Control[iCurrent+4]=p_stopsign
this.Control[iCurrent+5]=st_errortype
this.Control[iCurrent+6]=gb_errortype
this.Control[iCurrent+7]=cb_first
this.Control[iCurrent+8]=cb_prev
this.Control[iCurrent+9]=cb_next
this.Control[iCurrent+10]=cb_last
this.Control[iCurrent+11]=gb_movement
this.Control[iCurrent+12]=rb_saveolddb
this.Control[iCurrent+13]=rb_savenewdb
this.Control[iCurrent+14]=rb_saveentered
this.Control[iCurrent+15]=rb_savespecial
this.Control[iCurrent+16]=gb_proccode
this.Control[iCurrent+17]=cb_ok
this.Control[iCurrent+18]=cb_cancel_cl
this.Control[iCurrent+19]=cb_help
this.Control[iCurrent+20]=cb_yes
this.Control[iCurrent+21]=cb_no
this.Control[iCurrent+22]=cb_cancel_rl
this.Control[iCurrent+23]=dw_1
end on

on w_cc_error_main.destroy
call w_response::destroy
destroy(this.p_exclamation)
destroy(this.p_information)
destroy(this.p_question)
destroy(this.p_stopsign)
destroy(this.st_errortype)
destroy(this.gb_errortype)
destroy(this.cb_first)
destroy(this.cb_prev)
destroy(this.cb_next)
destroy(this.cb_last)
destroy(this.gb_movement)
destroy(this.rb_saveolddb)
destroy(this.rb_savenewdb)
destroy(this.rb_saveentered)
destroy(this.rb_savespecial)
destroy(this.gb_proccode)
destroy(this.cb_ok)
destroy(this.cb_cancel_cl)
destroy(this.cb_help)
destroy(this.cb_yes)
destroy(this.cb_no)
destroy(this.cb_cancel_rl)
destroy(this.dw_1)
end on

type p_exclamation from picture within w_cc_error_main
int X=106
int Y=137
int Width=165
int Height=145
boolean Visible=false
boolean Enabled=false
boolean FocusRectangle=false
end type

type p_information from picture within w_cc_error_main
int X=101
int Y=137
int Width=165
int Height=145
boolean Enabled=false
boolean FocusRectangle=false
end type

type p_question from picture within w_cc_error_main
int X=101
int Y=137
int Width=165
int Height=145
boolean Visible=false
boolean Enabled=false
boolean FocusRectangle=false
end type

type p_stopsign from picture within w_cc_error_main
int X=106
int Y=137
int Width=165
int Height=145
boolean Visible=false
boolean Enabled=false
boolean FocusRectangle=false
end type

type st_errortype from statictext within w_cc_error_main
int X=311
int Y=137
int Width=1354
int Height=145
boolean Enabled=false
boolean FocusRectangle=false
long BackColor=12632256
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type gb_errortype from groupbox within w_cc_error_main
int X=46
int Y=25
int Width=1660
int Height=337
string Text="Concurrency Error"
BorderStyle BorderStyle=StyleRaised!
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_first from uo_cb_first within w_cc_error_main
int X=83
int Y=485
int Width=138
int Height=105
int TabOrder=70
string Text="|<"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_prev from uo_cb_previous within w_cc_error_main
int X=247
int Y=485
int Width=138
int Height=105
int TabOrder=80
string Text="<"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_next from uo_cb_next within w_cc_error_main
int X=417
int Y=485
int Width=138
int Height=105
int TabOrder=90
string Text=">"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_last from uo_cb_last within w_cc_error_main
int X=581
int Y=485
int Width=138
int Height=105
int TabOrder=100
string Text=">|"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type gb_movement from groupbox within w_cc_error_main
int X=46
int Y=385
int Width=709
int Height=269
string Text="Column Selection"
BorderStyle BorderStyle=StyleRaised!
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type rb_saveolddb from radiobutton within w_cc_error_main
int X=833
int Y=465
int Width=727
int Height=73
string Text="Original Database Value"
BorderStyle BorderStyle=StyleLowered!
long BackColor=12632256
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;//******************************************************************
//  PC Module     : w_CC_Error_Main.rb_SaveOldDB
//  Event         : Clicked
//  Description   : Clicked script for the Concurrency
//                  error window.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_ErrorNbr

IF i_CCError > 0 THEN
   l_ErrorNbr = i_CCList[i_CCError]

   i_CCDW.i_CCStatus[l_ErrorNbr].Process_Code = c_CCProcUseOldDB

   dw_1.Set_Col_Data(1, l_ErrorNbr, &
                     i_CCDW.i_CCInfo[l_ErrorNbr].Old_DB_Value)
END IF
end on

type rb_savenewdb from radiobutton within w_cc_error_main
int X=833
int Y=549
int Width=645
int Height=73
string Text="New Database Value"
BorderStyle BorderStyle=StyleLowered!
long BackColor=12632256
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;//******************************************************************
//  PC Module     : w_CC_Error_Main.rb_SaveNewDB
//  Event         : Clicked
//  Description   : Clicked script for the Concurrency
//                  error window.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_ErrorNbr

IF i_CCError > 0 THEN
   l_ErrorNbr = i_CCList[i_CCError]

   i_CCDW.i_CCStatus[l_ErrorNbr].Process_Code = c_CCProcUseNewDB

   dw_1.Set_Col_Data(1, l_ErrorNbr, &
                     i_CCDW.i_CCInfo[l_ErrorNbr].New_DB_Value)
END IF
end on

type rb_saveentered from radiobutton within w_cc_error_main
int X=1578
int Y=465
int Width=467
int Height=73
string Text="Entered Value"
BorderStyle BorderStyle=StyleLowered!
long BackColor=12632256
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;//******************************************************************
//  PC Module     : w_CC_Error_Main.rb_SaveEntered
//  Event         : Clicked
//  Description   : Clicked script for the Concurrency
//                  error window.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_ErrorNbr

IF i_CCError > 0 THEN
   l_ErrorNbr = i_CCList[i_CCError]

   i_CCDW.i_CCStatus[l_ErrorNbr].Process_Code = c_CCProcUseDWValue

   dw_1.Set_Col_Data(1, l_ErrorNbr, &
                     i_CCDW.i_CCInfo[l_ErrorNbr].DW_Value)
END IF
end on

type rb_savespecial from radiobutton within w_cc_error_main
int X=1578
int Y=549
int Width=471
int Height=73
string Text="Alternate Value"
BorderStyle BorderStyle=StyleLowered!
long BackColor=12632256
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;//******************************************************************
//  PC Module     : w_CC_Error_Main.rb_SaveSpecial
//  Event         : Clicked
//  Description   : Clicked script for the Concurrency
//                  error window.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_ErrorNbr

IF i_CCError > 0 THEN
   l_ErrorNbr = i_CCList[i_CCError]

   i_CCDW.i_CCStatus[l_ErrorNbr].Process_Code = c_CCProcUseSpecial

   dw_1.Set_Col_Data(1, l_ErrorNbr, &
                     i_CCDW.i_CCStatus[l_ErrorNbr].Special_Value)
END IF
end on

type gb_proccode from groupbox within w_cc_error_main
int X=778
int Y=385
int Width=1303
int Height=269
string Text="Value to be Saved"
BorderStyle BorderStyle=StyleRaised!
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_ok from uo_cb_ok within w_cc_error_main
int X=1765
int Y=53
int Height=93
int TabOrder=10
string Text="Ok"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;call uo_cb_ok::clicked;//******************************************************************
//  PC Module     : w_CC_Error_Main.CB_Ok
//  Event         : Clicked
//  Description   : Clicked script for the Concurrency
//                  error window.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1995.  All Rights Reserved.
//******************************************************************

i_CCDW.i_CCUserStatus = c_CCUserOK
i_OkToClose           = TRUE

Close(PARENT)
end on

type cb_cancel_cl from uo_cb_cancel within w_cc_error_main
int X=1765
int Y=161
int Height=93
int TabOrder=20
string Text="Cancel"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_help from commandbutton within w_cc_error_main
int X=1765
int Y=269
int Width=311
int Height=93
int TabOrder=30
string Text="&Help"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;//******************************************************************
//  PC Module     : w_CC_Error_Main.CB_Help
//  Event         : Clicked
//  Description   : Clicked script for the Concurrency
//                  error window.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1995.  All Rights Reserved.
//******************************************************************

PCCA.MB.i_MB_Strings[1] = "Clicked"
PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
PCCA.MB.i_MB_Strings[3] = i_CCDW.i_ClassName
PCCA.MB.i_MB_Strings[4] = i_CCDW.i_Window.Title
PCCA.MB.i_MB_Strings[5] = i_CCDW.DataObject

IF i_CCError > 0 THEN
   CHOOSE CASE i_CCDW.i_CCInfo[i_CCList[i_CCError]].Error_Code
      CASE c_CCErrorNone
         PCCA.MB.fu_MessageBox                 &
            (PCCA.MB.c_MBI_DW_CC_NonErrorHelp, &
             0, PCCA.MB.i_MB_Numbers[],        &
             5, PCCA.MB.i_MB_Strings[])
      CASE c_CCErrorNonOverlap
         PCCA.MB.fu_MessageBox                   &
            (PCCA.MB.c_MBI_DW_CC_NonOverLapHelp, &
             0, PCCA.MB.i_MB_Numbers[],          &
             5, PCCA.MB.i_MB_Strings[])
      CASE c_CCErrorOverlap, c_CCErrorOverlapMatch
         PCCA.MB.fu_MessageBox                &
            (PCCA.MB.c_MBI_DW_CC_OverLapHelp, &
             0, PCCA.MB.i_MB_Numbers[],       &
             5, PCCA.MB.i_MB_Strings[])
      CASE c_CCErrorKeyConflict
         PCCA.MB.fu_MessageBox                    &
            (PCCA.MB.c_MBI_DW_CC_KeyConflictHelp, &
             0, PCCA.MB.i_MB_Numbers[],           &
             5, PCCA.MB.i_MB_Strings[])
      CASE ELSE
   END CHOOSE
END IF
end on

type cb_yes from commandbutton within w_cc_error_main
int X=1765
int Y=53
int Width=311
int Height=93
int TabOrder=40
string Text="&Yes"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;//******************************************************************
//  PC Module     : w_CC_Error_Main.CB_Yes
//  Event         : Clicked
//  Description   : Clicked script for the Concurrency
//                  error window.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1995.  All Rights Reserved.
//******************************************************************

i_CCDW.i_CCProcCode   = c_CCProcUseDWValue
i_CCDW.i_CCUserStatus = c_CCUserOK
i_OkToClose           = TRUE

Close(PARENT)
end on

type cb_no from commandbutton within w_cc_error_main
int X=1765
int Y=161
int Width=311
int Height=93
int TabOrder=50
string Text="&No"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;//******************************************************************
//  PC Module     : w_CC_Error_Main.CB_No
//  Event         : Clicked
//  Description   : Clicked script for the Concurrency
//                  error window.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1995.  All Rights Reserved.
//******************************************************************

i_CCDW.i_CCProcCode   = c_CCProcUseNewDB
i_CCDW.i_CCUserStatus = c_CCUserOK
i_OkToClose           = TRUE

Close(PARENT)
end on

type cb_cancel_rl from uo_cb_cancel within w_cc_error_main
int X=1765
int Y=269
int Height=93
int TabOrder=60
string Text="Cancel"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type dw_1 from uo_dw_main within w_cc_error_main
int X=46
int Y=701
int Width=2035
int Height=837
int TabOrder=0
string DataObject=""
boolean HScrollBar=true
boolean VScrollBar=true
end type

