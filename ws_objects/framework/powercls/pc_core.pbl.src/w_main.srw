﻿$PBExportHeader$w_main.srw
$PBExportComments$Ancestor main window to inherit
forward
global type w_main from window
end type
end forward

global type w_main from window
integer x = 672
integer y = 264
integer width = 1865
integer height = 1256
boolean titlebar = true
string title = "MAIN WINDOW"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 12632256
event pc_accept pbm_custom51
event pc_anychanges pbm_custom52
event pc_cancel pbm_custom53
event pc_close pbm_custom54
event pc_delete pbm_custom55
event pc_filter pbm_custom56
event pc_first pbm_custom57
event pc_insert pbm_custom58
event pc_last pbm_custom59
event pc_modify pbm_custom60
event pc_new pbm_custom61
event pc_next pbm_custom62
event pc_previous pbm_custom63
event pc_print pbm_custom64
event pc_query pbm_custom65
event pc_retrieve pbm_custom66
event pc_save pbm_custom67
event pc_saverowsas pbm_custom68
event pc_search pbm_custom69
event pc_setddlb pbm_custom70
event pc_setopenparm pbm_custom71
event pc_setsize pbm_custom72
event pc_setvariables pbm_custom73
event pc_setwindow pbm_custom74
event pc_view pbm_custom75
event pc_wm_initmenu pbm_initmenu
event pc_wm_initmenupopup pbm_initmenupopup
event pc_wm_move pbm_move
end type
global w_main w_main

type variables
//----------------------------------------------------------------------------
//  Instance Variables in sorted order.
//----------------------------------------------------------------------------

BOOLEAN	i_AutoMinimize
BOOLEAN	i_AutoPosition
BOOLEAN	i_AutoSize
BOOLEAN	i_AutoSize30

STRING		i_ClassName
UO_CONTAINER_MAIN	i_Containers[]

BOOLEAN	i_DisplayError

MENU		i_EditMenu
MENU		i_EditMenuDelete
UNSIGNEDLONG	i_EditMenuDeleteNum
MENU		i_EditMenuFilter
UNSIGNEDLONG	i_EditMenuFilterNum
MENU		i_EditMenuFirst
UNSIGNEDLONG	i_EditMenuFirstNum
BOOLEAN	i_EditMenuInit
MENU		i_EditMenuInsert
UNSIGNEDLONG	i_EditMenuInsertNum
BOOLEAN	i_EditMenuIsPopup
BOOLEAN	i_EditMenuIsValid
MENU		i_EditMenuLast
UNSIGNEDLONG	i_EditMenuLastNum
MENU		i_EditMenuModeSep
UNSIGNEDLONG	i_EditMenuModeSepNum
MENU		i_EditMenuModify
UNSIGNEDLONG	i_EditMenuModifyNum
MENU		i_EditMenuNew
UNSIGNEDLONG	i_EditMenuNewNum
MENU		i_EditMenuNext
UNSIGNEDLONG	i_EditMenuNextNum
MENU		i_EditMenuPrev
UNSIGNEDLONG	i_EditMenuPrevNum
MENU		i_EditMenuPrint
UNSIGNEDLONG	i_EditMenuPrintNum
MENU		i_EditMenuPrintSep
UNSIGNEDLONG	i_EditMenuPrintSepNum
MENU		i_EditMenuQuery
UNSIGNEDLONG	i_EditMenuQueryNum
MENU		i_EditMenuSave
UNSIGNEDLONG	i_EditMenuSaveNum
MENU		i_EditMenuSaveRowsAs
UNSIGNEDLONG	i_EditMenuSaveRowsAsNum
MENU		i_EditMenuSaveSep
UNSIGNEDLONG	i_EditMenuSaveSepNum
MENU		i_EditMenuSearch
UNSIGNEDLONG	i_EditMenuSearchNum
MENU		i_EditMenuSearchSep
UNSIGNEDLONG	i_EditMenuSearchSepNum
MENU		i_EditMenuView
UNSIGNEDLONG	i_EditMenuViewNum
BOOLEAN	i_EnablePopup

MENU		i_FileMenu
MENU		i_FileMenuAccept
UNSIGNEDLONG	i_FileMenuAcceptNum
MENU		i_FileMenuCancel
UNSIGNEDLONG	i_FileMenuCancelNum
MENU		i_FileMenuClose
UNSIGNEDLONG	i_FileMenuCloseNum
MENU		i_FileMenuExit
UNSIGNEDLONG	i_FileMenuExitNum
MENU		i_FileMenuExitSep
UNSIGNEDLONG	i_FileMenuExitSepNum
MENU		i_FileMenuFileSep
UNSIGNEDLONG	i_FileMenuFileSepNum
BOOLEAN	i_FileMenuInit
BOOLEAN	i_FileMenuIsValid
MENU		i_FileMenuNew
UNSIGNEDLONG	i_FileMenuNewNum
MENU		i_FileMenuOpen
UNSIGNEDLONG	i_FileMenuOpenNum
MENU		i_FileMenuPrint
UNSIGNEDLONG	i_FileMenuPrintNum
MENU		i_FileMenuPrintSep
UNSIGNEDLONG	i_FileMenuPrintSepNum
MENU		i_FileMenuPrintSetup
UNSIGNEDLONG	i_FileMenuPrintSetupNum
MENU		i_FileMenuSave
UNSIGNEDLONG	i_FileMenuSaveNum
MENU		i_FileMenuSaveRowsAs
UNSIGNEDLONG	i_FileMenuSaveRowsAsNum
MENU		i_FileMenuSaveSep
UNSIGNEDLONG	i_FileMenuSaveSepNum
UO_DW_MAIN	i_FirstDW
BOOLEAN	i_FirstDWIsValid

STRING		i_INIFileSection
BOOLEAN	i_INIGetDone
INTEGER		i_INIHeight
INTEGER		i_INILastHeight
INTEGER		i_INILastWidth
WINDOWSTATE	i_INILastWindowState
INTEGER		i_INILastXPos
INTEGER		i_INILastYPos
BOOLEAN	i_INIPutDone
BOOLEAN	i_INISaveIsValid
INTEGER		i_INISaveLastXPos
INTEGER		i_INISaveLastYPos
BOOLEAN	i_INIValidSize
BOOLEAN	i_INIValidWS
INTEGER		i_INIWidth
WINDOWSTATE	i_INIWindowState
INTEGER		i_INIXPos
INTEGER		i_INIYPos
BOOLEAN	i_IsSheet

BOOLEAN	i_LoadCodeTable

MENU		i_mPopup

INTEGER		i_NumContainers

STRING		i_ObjectType = "w_Main"
POWEROBJECT	i_OpenParm
BOOLEAN	i_OptionsInit

INTEGER		i_PO_Controls[]
MENU		i_PopupMenu
MENU		i_PopupMenuDelete
UNSIGNEDLONG	i_PopupMenuDeleteNum
MENU		i_PopupMenuFilter
UNSIGNEDLONG	i_PopupMenuFilterNum
MENU		i_PopupMenuFirst
UNSIGNEDLONG	i_PopupMenuFirstNum
BOOLEAN	i_PopupMenuInit
MENU		i_PopupMenuInsert
UNSIGNEDLONG	i_PopupMenuInsertNum
BOOLEAN	i_PopupMenuIsEdit
BOOLEAN	i_PopupMenuIsValid
MENU		i_PopupMenuLast
UNSIGNEDLONG	i_PopupMenuLastNum
MENU		i_PopupMenuModeSep
UNSIGNEDLONG	i_PopupMenuModeSepNum
MENU		i_PopupMenuModify
UNSIGNEDLONG	i_PopupMenuModifyNum
MENU		i_PopupMenuNew
UNSIGNEDLONG	i_PopupMenuNewNum
MENU		i_PopupMenuNext
UNSIGNEDLONG	i_PopupMenuNextNum
MENU		i_PopupMenuPrev
UNSIGNEDLONG	i_PopupMenuPrevNum
MENU		i_PopupMenuPrint
UNSIGNEDLONG	i_PopupMenuPrintNum
MENU		i_PopupMenuPrintSep
UNSIGNEDLONG	i_PopupMenuPrintSepNum
MENU		i_PopupMenuQuery
UNSIGNEDLONG	i_PopupMenuQueryNum
MENU		i_PopupMenuSave
UNSIGNEDLONG	i_PopupMenuSaveNum
MENU		i_PopupMenuSaveRowsAs
UNSIGNEDLONG	i_PopupMenuSaveRowsAsNum
MENU		i_PopupMenuSaveSep
UNSIGNEDLONG	i_PopupMenuSaveSepNum
MENU		i_PopupMenuSearch
UNSIGNEDLONG	i_PopupMenuSearchNum
MENU		i_PopupMenuSearchSep
UNSIGNEDLONG	i_PopupMenuSearchSepNum
MENU		i_PopupMenuView
UNSIGNEDLONG	i_PopupMenuViewNum

BOOLEAN	i_ResizeWin

UNSIGNEDLONG	i_SaveOnClose
BOOLEAN	i_SavePosition

UNSIGNEDLONG	i_ToolBarPosition
BOOLEAN	i_ToolBarText

UNSIGNEDLONG	i_W_ControlWord
LONG		i_W_DevNumber[]
INTEGER		i_WindowHeight
WINDOWSTATE	i_WindowState
INTEGER		i_WindowWidth

BOOLEAN	i_ZoomWin

//----------------------------------------------------------------------------
//  Constant Variables in logical order.
//----------------------------------------------------------------------------

UNSIGNEDLONG	c_MasterList
UNSIGNEDLONG	c_MasterEdit
UNSIGNEDLONG	c_DetailList
UNSIGNEDLONG	c_DetailEdit

UNSIGNEDLONG	c_ScrollSelf
UNSIGNEDLONG	c_ScrollParent

UNSIGNEDLONG	c_IsNotInstance
UNSIGNEDLONG	c_IsInstance

UNSIGNEDLONG	c_RequiredOnSave
UNSIGNEDLONG	c_AlwaysCheckRequired

UNSIGNEDLONG	c_NoNew
UNSIGNEDLONG	c_NewOk

UNSIGNEDLONG	c_NoModify
UNSIGNEDLONG	c_ModifyOk

UNSIGNEDLONG	c_NoDelete
UNSIGNEDLONG	c_DeleteOk

UNSIGNEDLONG	c_NoQuery
UNSIGNEDLONG	c_QueryOk

UNSIGNEDLONG	c_SelectOnDoubleClick
UNSIGNEDLONG	c_SelectOnClick
UNSIGNEDLONG	c_SelectOnRowFocusChange

UNSIGNEDLONG	c_NoDrillDown
UNSIGNEDLONG	c_DrillDown

UNSIGNEDLONG	c_SameModeOnSelect
UNSIGNEDLONG	c_ViewOnSelect
UNSIGNEDLONG	c_ModifyOnSelect

UNSIGNEDLONG	c_NoMultiSelect
UNSIGNEDLONG	c_MultiSelect

UNSIGNEDLONG	c_RefreshOnSelect
UNSIGNEDLONG	c_NoAutoRefresh
UNSIGNEDLONG	c_RefreshOnMultiSelect

UNSIGNEDLONG	c_ParentModeOnOpen
UNSIGNEDLONG	c_ViewOnOpen
UNSIGNEDLONG	c_ModifyOnOpen
UNSIGNEDLONG	c_NewOnOpen

UNSIGNEDLONG	c_SameModeAfterSave
UNSIGNEDLONG	c_ViewAfterSave

UNSIGNEDLONG	c_NoEnableModifyOnOpen
UNSIGNEDLONG	c_EnableModifyOnOpen

UNSIGNEDLONG	c_NoEnableNewOnOpen
UNSIGNEDLONG	c_EnableNewOnOpen

UNSIGNEDLONG	c_RetrieveOnOpen
UNSIGNEDLONG	c_AutoRetrieveOnOpen // Same as c_RetrieveOnOpen
UNSIGNEDLONG	c_NoRetrieveOnOpen

UNSIGNEDLONG	c_AutoRefresh
UNSIGNEDLONG	c_SkipRefresh
UNSIGNEDLONG	c_TriggerRefresh
UNSIGNEDLONG	c_PostRefresh

UNSIGNEDLONG	c_RetrieveAll
UNSIGNEDLONG	c_RetrieveAsNeeded

UNSIGNEDLONG	c_NoShareData
UNSIGNEDLONG	c_ShareData

UNSIGNEDLONG	c_NoRefreshParent
UNSIGNEDLONG	c_RefreshParent

UNSIGNEDLONG	c_NoRefreshChild
UNSIGNEDLONG	c_RefreshChild

UNSIGNEDLONG	c_IgnoreNewRows
UNSIGNEDLONG	c_NoIgnoreNewRows

UNSIGNEDLONG	c_NoNewModeOnEmpty
UNSIGNEDLONG	c_NewModeOnEmpty

UNSIGNEDLONG	c_MultipleNewRows
UNSIGNEDLONG	c_OnlyOneNewRow

UNSIGNEDLONG	c_DisableCC
UNSIGNEDLONG	c_EnableCC

UNSIGNEDLONG	c_DisableCCInsert
UNSIGNEDLONG	c_EnableCCInsert

UNSIGNEDLONG	c_ViewModeBorderUnchanged
UNSIGNEDLONG	c_ViewModeNoBorder
UNSIGNEDLONG	c_ViewModeShadowBox
UNSIGNEDLONG	c_ViewModeBox
UNSIGNEDLONG	c_ViewModeResize
UNSIGNEDLONG	c_ViewModeUnderline
UNSIGNEDLONG	c_ViewModeLowered
UNSIGNEDLONG	c_ViewModeRaised

UNSIGNEDLONG	c_ViewModeColorUnchanged
UNSIGNEDLONG	c_ViewModeBlack
UNSIGNEDLONG	c_ViewModeWhite
UNSIGNEDLONG	c_ViewModeGray
UNSIGNEDLONG	c_ViewModeLightGray // Same as c_ViewModeGray
UNSIGNEDLONG	c_ViewModeDarkGray
UNSIGNEDLONG	c_ViewModeRed
UNSIGNEDLONG	c_ViewModeDarkRed
UNSIGNEDLONG	c_ViewModeGreen
UNSIGNEDLONG	c_ViewModeDarkGreen
UNSIGNEDLONG	c_ViewModeBlue
UNSIGNEDLONG	c_ViewModeDarkBlue
UNSIGNEDLONG	c_ViewModeMagenta
UNSIGNEDLONG	c_ViewModeDarkMagenta
UNSIGNEDLONG	c_ViewModeCyan
UNSIGNEDLONG	c_ViewModeDarkCyan
UNSIGNEDLONG	c_ViewModeYellow
UNSIGNEDLONG	c_ViewModeBrown

UNSIGNEDLONG	c_InactiveDWColorUnchanged
UNSIGNEDLONG	c_InactiveDWBlack
UNSIGNEDLONG	c_InactiveDWWhite
UNSIGNEDLONG	c_InactiveDWGray
UNSIGNEDLONG	c_InactiveDWLightGray // Same as c_InActiveDWGray
UNSIGNEDLONG	c_InactiveDWDarkGray
UNSIGNEDLONG	c_InactiveDWRed
UNSIGNEDLONG	c_InactiveDWDarkRed
UNSIGNEDLONG	c_InactiveDWGreen
UNSIGNEDLONG	c_InactiveDWDarkGreen
UNSIGNEDLONG	c_InactiveDWBlue
UNSIGNEDLONG	c_InactiveDWDarkBlue
UNSIGNEDLONG	c_InactiveDWMagenta
UNSIGNEDLONG	c_InactiveDWDarkMagenta
UNSIGNEDLONG	c_InactiveDWCyan
UNSIGNEDLONG	c_InactiveDWDarkCyan
UNSIGNEDLONG	c_InactiveDWYellow
UNSIGNEDLONG	c_InactiveDWBrown

UNSIGNEDLONG	c_InactiveTextLineCol
UNSIGNEDLONG	c_NoInactiveText
UNSIGNEDLONG	c_InactiveText
UNSIGNEDLONG	c_NoInactiveLine
UNSIGNEDLONG	c_InactiveLine
UNSIGNEDLONG	c_NoInactiveCol
UNSIGNEDLONG	c_InactiveCol

UNSIGNEDLONG	c_CursorRowFocusRect
UNSIGNEDLONG	c_NoCursorRowFocusRect

UNSIGNEDLONG	c_CursorRowPointer
UNSIGNEDLONG	c_NoCursorRowPointer

UNSIGNEDLONG	c_CalcDWStyle
UNSIGNEDLONG	c_FreeFormStyle
UNSIGNEDLONG	c_TabularFormStyle

UNSIGNEDLONG	c_AutoCalcHighlightSelected
UNSIGNEDLONG	c_HighlightSelected
UNSIGNEDLONG	c_NoHighlightSelected

UNSIGNEDLONG	c_NoResizeDW
UNSIGNEDLONG	c_ResizeDW

UNSIGNEDLONG	c_NoAutoConfigMenus
UNSIGNEDLONG	c_AutoConfigMenus

UNSIGNEDLONG	c_ShowEmpty
UNSIGNEDLONG	c_NoShowEmpty

UNSIGNEDLONG	c_AutoFocus
UNSIGNEDLONG	c_NoAutoFocus

UNSIGNEDLONG	c_CCErrorRed
UNSIGNEDLONG	c_CCErrorBlack
UNSIGNEDLONG	c_CCErrorWhite
UNSIGNEDLONG	c_CCErrorGray
UNSIGNEDLONG	c_CCErrorLightGray // Same as c_InActiveDWGray
UNSIGNEDLONG	c_CCErrorDarkGray
UNSIGNEDLONG	c_CCErrorDarkRed
UNSIGNEDLONG	c_CCErrorGreen
UNSIGNEDLONG	c_CCErrorDarkGreen
UNSIGNEDLONG	c_CCErrorBlue
UNSIGNEDLONG	c_CCErrorDarkBlue
UNSIGNEDLONG	c_CCErrorMagenta
UNSIGNEDLONG	c_CCErrorDarkMagenta
UNSIGNEDLONG	c_CCErrorCyan
UNSIGNEDLONG	c_CCErrorDarkCyan
UNSIGNEDLONG	c_CCErrorYellow
UNSIGNEDLONG	c_CCErrorBrown

UNSIGNEDLONG	c_LoadCodeTable
UNSIGNEDLONG	c_NoLoadCodeTable

UNSIGNEDLONG	c_EnablePopup
UNSIGNEDLONG	c_NoEnablePopup

UNSIGNEDLONG	c_NoResizeWin
UNSIGNEDLONG	c_ResizeWin
UNSIGNEDLONG	c_ZoomWin

UNSIGNEDLONG	c_NoAutoMinimize
UNSIGNEDLONG	c_AutoMinimize

UNSIGNEDLONG	c_NoAutoSize
UNSIGNEDLONG	c_AutoSize
UNSIGNEDLONG	c_AutoSize30

UNSIGNEDLONG	c_NoAutoPosition
UNSIGNEDLONG	c_AutoPosition

UNSIGNEDLONG	c_NoSavePosition
UNSIGNEDLONG	c_SavePosition

UNSIGNEDLONG	c_ClosePromptUser
UNSIGNEDLONG	c_ClosePromptUserOnce
UNSIGNEDLONG	c_CloseSave
UNSIGNEDLONG	c_CloseNoSave

UNSIGNEDLONG	c_ToolBarNone
UNSIGNEDLONG	c_ToolBarTop
UNSIGNEDLONG	c_ToolBarBottom
UNSIGNEDLONG	c_ToolBarLeft
UNSIGNEDLONG	c_ToolBarRight
UNSIGNEDLONG	c_ToolBarFloat

UNSIGNEDLONG	c_ToolBarHideText
UNSIGNEDLONG	c_ToolBarShowText

UNSIGNEDLONG	c_CT_LoadCodeTable
UNSIGNEDLONG	c_CT_NoLoadCodeTable

UNSIGNEDLONG	c_CT_NoResizeCont
UNSIGNEDLONG	c_CT_ResizeCont
UNSIGNEDLONG	c_CT_ZoomCont

UNSIGNEDLONG	c_CT_ClosePromptUser
UNSIGNEDLONG	c_CT_ClosePromptUserOnce
UNSIGNEDLONG	c_CT_CloseSave
UNSIGNEDLONG	c_CT_CloseNoSave

UNSIGNEDLONG	c_CT_BringToTop
UNSIGNEDLONG	c_CT_NoBringToTop

INTEGER		c_Fatal 			= -1
INTEGER		c_Success 		= 0
UNSIGNEDLONG	c_Default 		= 0

UO_DW_MAIN	c_NullDW
UO_CB_MAIN	c_NullCB
MENU		c_NullMenu
PICTURE		c_NullPicture

STRING		c_INIUndefined 		= "[INI_Undefined]"
STRING		c_XKey 			= "X"
STRING		c_YKey 			= "Y"
STRING		c_WidthKey 		= "Width"
STRING		c_HeightKey 		= "Height"
STRING		c_WinStateKey 		= "WindowState"
INTEGER		c_MinimumOverlap 		= 25

UNSIGNEDLONG	c_SOCPromptUser 		= 100
UNSIGNEDLONG	c_SOCPromptUserOnce 	= 101
UNSIGNEDLONG	c_SOCSave 		= 102
UNSIGNEDLONG	c_SOCNoSave 		= 103

UNSIGNEDLONG	c_Invisible 		= 0
UNSIGNEDLONG	c_Show 			= 1
UNSIGNEDLONG	c_Mark 			= 2
UNSIGNEDLONG	c_ShowMark 		= 3

UNSIGNEDLONG	c_ColorUndefined 		= 0
UNSIGNEDLONG	c_Black 			= 1
UNSIGNEDLONG	c_White 			= 2
UNSIGNEDLONG	c_Gray 			= 3
UNSIGNEDLONG	c_DarkGray 		= 4
UNSIGNEDLONG	c_Red 			= 5
UNSIGNEDLONG	c_DarkRed 		= 6
UNSIGNEDLONG	c_Green 			= 7
UNSIGNEDLONG	c_DarkGreen 		= 8
UNSIGNEDLONG	c_Blue			= 9 
UNSIGNEDLONG	c_DarkBlue 		= 10
UNSIGNEDLONG	c_Magenta 		= 11
UNSIGNEDLONG	c_DarkMagenta 		= 12
UNSIGNEDLONG	c_Cyan 			= 13
UNSIGNEDLONG	c_DarkCyan 		= 14
UNSIGNEDLONG	c_Yellow 			= 15
UNSIGNEDLONG	c_Brown 			= 16

UNSIGNEDLONG	c_ColorFirst 		= 1
UNSIGNEDLONG	c_ColorLast 		= 16

UNSIGNEDLONG	c_DW_BorderUndefined 	= 0
UNSIGNEDLONG	c_DW_BorderNone 	= 1
UNSIGNEDLONG	c_DW_BorderShadowBox 	= 2
UNSIGNEDLONG	c_DW_BorderBox 		= 3
UNSIGNEDLONG	c_DW_BorderResize 	= 4
UNSIGNEDLONG	c_DW_BorderUnderLine 	= 5
UNSIGNEDLONG	c_DW_BorderLowered 	= 6
UNSIGNEDLONG	c_DW_BorderRaised 	= 7

UNSIGNEDLONG	c_DW_BorderFirst 		= 1
UNSIGNEDLONG	c_DW_BorderLast	 	= 7

INTEGER		c_AcceptCB 		= 1
INTEGER		c_CancelCB 		= 2
INTEGER		c_CloseCB 		= 3
INTEGER		c_DeleteCB 		= 4
INTEGER		c_FilterCB 		= 5
INTEGER		c_FirstCB 		= 6
INTEGER		c_InsertCB 		= 7
INTEGER		c_LastCB 		= 8
INTEGER		c_MainCB 		= 9
INTEGER		c_ModifyCB 		= 10
INTEGER		c_NewCB 		= 11
INTEGER		c_NextCB 		= 12
INTEGER		c_PreviousCB 		= 13
INTEGER		c_PrintCB 		= 14
INTEGER		c_QueryCB 		= 15
INTEGER		c_ResetQueryCB 		= 16
INTEGER		c_RetrieveCB 		= 17
INTEGER		c_SaveCB 		= 18
INTEGER		c_SaveRowsAsCB 		= 19
INTEGER		c_SearchCB 		= 20
INTEGER		c_ViewCB 		= 21

UNSIGNEDLONG	c_ModeUndefined 		= 0
UNSIGNEDLONG	c_ModeView 		= 100
UNSIGNEDLONG	c_ModeModify 		= 101
UNSIGNEDLONG	c_ModeNew 		= 102

UNSIGNEDLONG	c_DWStateUndefined 	= 0
UNSIGNEDLONG	c_DWStateEnabled 	= 200
UNSIGNEDLONG	c_DWStateDisabled 	= 201
UNSIGNEDLONG	c_DWStateQuery 		= 202

UNSIGNEDLONG	c_ObjTypeUndefined 	= 0
UNSIGNEDLONG	c_ObjTypeColumn 		= 300
UNSIGNEDLONG	c_ObjTypeText 		= 301
UNSIGNEDLONG	c_ObjTypeLine 		= 302

UNSIGNEDLONG	c_ColTypeUndefined 	= 0
UNSIGNEDLONG	c_ColTypeString 		= 400
UNSIGNEDLONG	c_ColTypeDate 		= 401
UNSIGNEDLONG	c_ColTypeDateTime 	= 402
UNSIGNEDLONG	c_ColTypeDecimal 		= 403
UNSIGNEDLONG	c_ColTypeNumber 		= 404
UNSIGNEDLONG	c_ColTypeTime 		= 405
UNSIGNEDLONG	c_ColTypeTimeStamp 	= 406
UNSIGNEDLONG	c_ColTypeBlob 		= 407

UNSIGNEDLONG	c_RFC_Undefined 		= 0
UNSIGNEDLONG	c_RFC_NoSelect 		= 500
UNSIGNEDLONG	c_RFC_DoubleClicked 	= 501
UNSIGNEDLONG	c_RFC_Clicked 		= 502
UNSIGNEDLONG	c_RFC_RowFocusChanged 	= 503

UNSIGNEDLONG	c_RFC_RefreshOnSelect 	= 600
UNSIGNEDLONG	c_RFC_NoAutoRefresh 	= 601
UNSIGNEDLONG	c_RFC_RefreshOnMultiSelect= 602

UNSIGNEDLONG	c_AutoRefreshOnOpen 	= 700
UNSIGNEDLONG	c_SkipRefreshOnOpen 	= 701
UNSIGNEDLONG	c_TriggerRefreshOnOpen 	= 702
UNSIGNEDLONG	c_PostRefreshOnOpen 	= 703

UNSIGNEDLONG	c_ShareNone 		= 800
UNSIGNEDLONG	c_ShareEnabled 		= 801

UNSIGNEDLONG	c_No 			= 900
UNSIGNEDLONG	c_Yes 			= 901

UNSIGNEDLONG	c_ControlUndefined 	= 0
UNSIGNEDLONG	c_ControlActiveDW 	= 1000
UNSIGNEDLONG	c_ControlRows 		= 1001
UNSIGNEDLONG	c_ControlMainMenu 	= 1002
UNSIGNEDLONG	c_ControlPopupMenu 	= 1003

LONG		c_DoNew 		= 1100
UNSIGNEDINT	c_NewAppend 		= 1101
UNSIGNEDINT	c_NewInsert 		= 1102

UNSIGNEDLONG	c_AcceptProcessErrors 	= 1200
UNSIGNEDLONG	c_AcceptIgnoreErrors 	= 1201

UNSIGNEDLONG	c_GetCurrentValues 	= 1310
UNSIGNEDLONG	c_GetOriginalValues 	= 1301

UNSIGNEDLONG	c_CascadeFirst 		= 1400
UNSIGNEDLONG	c_CascadeLast 		= 1401

UNSIGNEDLONG	c_CascadePost 		= 1500
UNSIGNEDLONG	c_CascadeTrigger 		= 1501

UNSIGNEDLONG	c_CheckTree 		= 1600
UNSIGNEDLONG	c_CheckChildren 		= 1601
UNSIGNEDLONG	c_CheckThisDW 		= 1602

UNSIGNEDLONG	c_EventGotoRoot 		= 1700
UNSIGNEDLONG	c_EventNoTriggerUp 	= 1701

UNSIGNEDLONG	c_FindRootDW 		= 1800
UNSIGNEDLONG	c_FindScrollDW 		= 1801
UNSIGNEDLONG	c_ShareNotEmpty 		= 1900
UNSIGNEDLONG	c_ShareEmpty 		= 1901
UNSIGNEDLONG	c_SharePushRedraw 	= 1902
UNSIGNEDLONG	c_SharePopRedraw 	= 1903
UNSIGNEDLONG	c_SharePushRFC 		= 1904
UNSIGNEDLONG	c_SharePopRFC 		= 1905
UNSIGNEDLONG	c_SharePushValidate 	= 1906
UNSIGNEDLONG	c_SharePopValidate 	= 1907
UNSIGNEDLONG	c_SharePushVScroll 	= 1908
UNSIGNEDLONG	c_SharePopVScroll 	= 1909

UNSIGNEDLONG	c_OpenChildAuto 		= 2000
UNSIGNEDLONG	c_OpenChildForce 		= 2001
UNSIGNEDLONG	c_OpenChildAlwaysForce 	= 2002
UNSIGNEDLONG	c_OpenChildPrevent 	= 2003
UNSIGNEDLONG	c_OpenChildAlwaysPrevent 	= 2004

UNSIGNEDLONG	c_RefreshUndefined 	= 0
UNSIGNEDLONG	c_RefreshView 		= 2100
UNSIGNEDLONG	c_RefreshModify 		= 2101
UNSIGNEDLONG	c_RefreshNew 		= 2102
UNSIGNEDLONG	c_RefreshRFC 		= 2103
UNSIGNEDLONG	c_RefreshDrillDown 	= 2104
UNSIGNEDLONG	c_RefreshSame 		= 2105
UNSIGNEDLONG	c_RefreshSave 		= 2106

UNSIGNEDLONG	c_OnlyIfModified 		= 2200
UNSIGNEDLONG	c_RetrieveAllDWs 		= 2201

UNSIGNEDLONG	c_ReselectRows 		= 2300
UNSIGNEDLONG	c_NoReselectRows 	= 2301

UNSIGNEDLONG	c_CheckForChanges 	= 2400
UNSIGNEDLONG	c_IgnoreChanges 		= 2401
UNSIGNEDLONG	c_SaveChanges 		= 2402

UNSIGNEDLONG	c_DoRefresh 		= 2500
UNSIGNEDLONG	c_RefreshChildren 		= 2500
UNSIGNEDLONG	c_NoRefresh 		= 2501
UNSIGNEDLONG	c_NoRefreshChildren 	= 2501

UNSIGNEDLONG	c_NoPromptUser 		= 2600
UNSIGNEDLONG	c_PromptUser 		= 2601

UNSIGNEDLONG	c_ResetChildren 		= 2700
UNSIGNEDLONG	c_NoResetChildren 	= 2701

UNSIGNEDLONG	c_ProcessSort 		= 2800
UNSIGNEDLONG	c_ProcessGroupCalc 	= 2801
UNSIGNEDLONG	c_ProcessFilter 		= 2802
UNSIGNEDLONG	c_ProcessSync 		= 2803

UNSIGNEDLONG	c_CCErrorNone 		= 2900
UNSIGNEDLONG	c_CCErrorNonOverlap 	= 2901
UNSIGNEDLONG	c_CCErrorOverlap 		= 2902
UNSIGNEDLONG	c_CCErrorOverlapMatch 	= 2903
UNSIGNEDLONG	c_CCErrorNonExistent 	= 2904
UNSIGNEDLONG	c_CCErrorDeleteModified 	= 2905
UNSIGNEDLONG	c_CCErrorDeleteNonExistent 	= 2906
UNSIGNEDLONG	c_CCErrorKeyConflict 	= 2907

UNSIGNEDLONG	c_CCUserNone 		= 3000
UNSIGNEDLONG	c_CCUserSpecify 		= 3001

UNSIGNEDLONG	c_CCProcUseOldDB 	= 3100
UNSIGNEDLONG	c_CCProcUseNewDB 	= 3101
UNSIGNEDLONG	c_CCProcUseDWValue 	= 3102
UNSIGNEDLONG	c_CCProcUseSpecial 	= 3103

UNSIGNEDLONG	c_CCValidateNone 		= 3200
UNSIGNEDLONG	c_CCValidateRow 		= 3201

UNSIGNEDLONG	c_CCUserOK 		= 3300
UNSIGNEDLONG	c_CCUserCancel 		= 3301

UNSIGNEDLONG	c_CCSetIfModified 		= 3400
UNSIGNEDLONG	c_CCSetForce 		= 3401

UNSIGNEDLONG	c_ValidateAllCols 		= 3500
UNSIGNEDLONG	c_ValidateCurCol		= 3501

UNSIGNEDLONG	c_SetRowIndicatorFocus 	= 3600
UNSIGNEDLONG	c_SetRowIndicatorNotFocus 	= 3601
UNSIGNEDLONG	c_SetRowIndicatorNormal 	= 3602
UNSIGNEDLONG	c_SetRowIndicatorEmpty 	= 3603
UNSIGNEDLONG	c_SetRowIndicatorQuery 	= 3604

UNSIGNEDLONG	c_CapabilityEnable 	= 3700
UNSIGNEDLONG	c_CapabilityDisable 	= 3701

UNSIGNEDLONG	c_AllowControlEnable 	= 3800
UNSIGNEDLONG	c_AllowControlDisable 	= 3801

UNSIGNEDLONG	c_CapabilityAll 		= 0
UNSIGNEDLONG	c_CapabilityNew 		= 1
UNSIGNEDLONG	c_CapabilityModify 		= 2
UNSIGNEDLONG	c_CapabilityDelete 		= 4
UNSIGNEDLONG	c_CapabilityQuery 		= 8

UNSIGNEDLONG	c_AllowControlAll 		= 0
UNSIGNEDLONG	c_AllowControlAccept 	= 1
UNSIGNEDLONG	c_AllowControlCancel 	= 2
UNSIGNEDLONG	c_AllowControlClose 	= 4
UNSIGNEDLONG	c_AllowControlDelete 	= 8
UNSIGNEDLONG	c_AllowControlFirst 	= 16
UNSIGNEDLONG	c_AllowControlFilter 	= 32
UNSIGNEDLONG	c_AllowControlInsert 	= 64
UNSIGNEDLONG	c_AllowControlLast 	= 128
UNSIGNEDLONG	c_AllowControlMain 	= 256
UNSIGNEDLONG	c_AllowControlModify 	= 512
UNSIGNEDLONG	c_AllowControlNew 	= 1024
UNSIGNEDLONG	c_AllowControlNext 	= 2048
UNSIGNEDLONG	c_AllowControlPrevious 	= 4096
UNSIGNEDLONG	c_AllowControlPrint 	= 8192
UNSIGNEDLONG	c_AllowControlQuery 	= 16384
UNSIGNEDLONG	c_AllowControlResetQuery 	= 32768
UNSIGNEDLONG	c_AllowControlRetrieve 	= 65536
UNSIGNEDLONG	c_AllowControlSave 	= 131072
UNSIGNEDLONG	c_AllowControlSaveRowsAs 	= 262144
UNSIGNEDLONG	c_AllowControlSearch 	= 524288
UNSIGNEDLONG	c_AllowControlView 	= 1048576

UNSIGNEDLONG	c_ShareModified 		= 1
UNSIGNEDLONG	c_ShareAllModified 	= 2
UNSIGNEDLONG	c_ShareAllDoSetKey 	= 4

UNSIGNEDLONG	c_NoSyncShare 		= 0
UNSIGNEDLONG	c_SyncShare 		= 1

INTEGER		c_VisibleCol 		= -3
INTEGER		c_InvisibleCol 		= -2
INTEGER		c_SameTab 		= -1
INTEGER		c_SameBorder 		= -1
LONG		c_SameColor 		= -1
LONG		c_SameBColor 		= -1

BOOLEAN	c_BatchAttrib 		= TRUE
BOOLEAN	c_ProcessAttrib 		= FALSE

BOOLEAN	c_DrawNow 		= TRUE
BOOLEAN	c_BufferDraw 		= FALSE

BOOLEAN	c_ProcessRFC 		= TRUE
BOOLEAN	c_IgnoreRFC 		= FALSE

BOOLEAN	c_ProcessVal 		= TRUE
BOOLEAN	c_IgnoreVal 		= FALSE

BOOLEAN	c_ProcessVScroll		= TRUE
BOOLEAN	c_IgnoreVScroll 		= FALSE

BOOLEAN	c_StateUnused 		= FALSE

end variables

forward prototypes
public subroutine get_ini_size ()
public subroutine put_ini_size ()
public subroutine save_on_close (unsignedlong close_option)
public subroutine set_ini_section (string file_section)
public subroutine set_tb_float_position (long x_pos, long y_pos)
public subroutine set_w_emenu (menu edit_menu)
public subroutine set_w_fmenu (menu file_menu)
public subroutine set_w_options (unsignedlong control_word)
public subroutine set_w_pmenu (menu popup_menu)
public subroutine set_w_popup (menu popup_menu)
public subroutine debug (string event_name, string prefix, integer debug_level)
public subroutine proc_entry (string proc_name, integer debug_dump_level)
public subroutine proc_exit ()
end prototypes

on pc_close;//******************************************************************
//  PC Module     : w_Main
//  Event         : pc_Close
//  Description   : Closes the current window.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

//------------------------------------------------------------------
//  Use the PowerBuilder routine to close the window.
//------------------------------------------------------------------

Close(THIS)
end on

on pc_delete;//******************************************************************
//  PC Module     : w_Main
//  Event         : pc_Delete
//  Description   : Tells the current DataWindow to delete is
//                  selected rows.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

//------------------------------------------------------------------
//  Trigger the corresponding DataWindow event on the current
//  DataWindow (if it valid).
//------------------------------------------------------------------

IF IsValid(PCCA.Window_CurrentDW) THEN
   PCCA.Window_CurrentDW.TriggerEvent("pcd_Delete")
END IF

end on

on pc_filter;//******************************************************************
//  PC Module     : w_Main
//  Event         : pc_Filter
//  Description   : Tells the current DataWindow to build up a
//                  filter based on non-DataWindow fields and
//                  execute it.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

//------------------------------------------------------------------
//  Trigger the corresponding DataWindow event on the current
//  DataWindow (if it valid).
//------------------------------------------------------------------

IF IsValid(PCCA.Window_CurrentDW) THEN
   PCCA.Window_CurrentDW.TriggerEvent("pcd_Filter")
END IF

end on

on pc_first;//******************************************************************
//  PC Module     : w_Main
//  Event         : pc_First
//  Description   : Tells the current DataWindow to scroll to
//                  the first row.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

//------------------------------------------------------------------
//  Trigger the corresponding DataWindow event on the current
//  DataWindow (if it valid).
//------------------------------------------------------------------

IF IsValid(PCCA.Window_CurrentDW) THEN
   PCCA.Window_CurrentDW.TriggerEvent("pcd_First")
END IF

end on

on pc_insert;//******************************************************************
//  PC Module     : w_Main
//  Event         : pc_Insert
//  Description   : Tells the current DataWindow to insert
//                  a row and go into new mode.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

//------------------------------------------------------------------
//  Trigger the corresponding DataWindow event on the current
//  DataWindow (if it valid).
//------------------------------------------------------------------

IF IsValid(PCCA.Window_CurrentDW) THEN
   PCCA.Window_CurrentDW.TriggerEvent("pcd_Insert")
END IF

end on

on pc_last;//******************************************************************
//  PC Module     : w_Main
//  Event         : pc_Last
//  Description   : Tells the current DataWindow to scroll to
//                  the last row.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

//------------------------------------------------------------------
//  Trigger the corresponding DataWindow event on the current
//  DataWindow (if it valid).
//------------------------------------------------------------------

IF IsValid(PCCA.Window_CurrentDW) THEN
   PCCA.Window_CurrentDW.TriggerEvent("pcd_Last")
END IF

end on

on pc_modify;//******************************************************************
//  PC Module     : w_Main
//  Event         : pc_Modify
//  Description   : Tells the current DataWindow to go into modify
//                  mode.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

//------------------------------------------------------------------
//  Trigger the corresponding DataWindow event on the current
//  DataWindow (if it valid).
//------------------------------------------------------------------

IF IsValid(PCCA.Window_CurrentDW) THEN
   PCCA.Window_CurrentDW.TriggerEvent("pcd_Modify")
END IF

end on

on pc_new;//******************************************************************
//  PC Module     : w_Main
//  Event         : pc_New
//  Description   : Tells the current DataWindow to append
//                  a row and go into new mode.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

//------------------------------------------------------------------
//  Trigger the corresponding DataWindow event on the current
//  DataWindow (if it valid).
//------------------------------------------------------------------

IF IsValid(PCCA.Window_CurrentDW) THEN
   PCCA.Window_CurrentDW.TriggerEvent("pcd_New")
END IF

end on

on pc_next;//******************************************************************
//  PC Module     : w_Main
//  Event         : pc_Next
//  Description   : Tells the current DataWindow to scroll to
//                  the next row.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

//------------------------------------------------------------------
//  Trigger the corresponding DataWindow event on the current
//  DataWindow (if it valid).
//------------------------------------------------------------------

IF IsValid(PCCA.Window_CurrentDW) THEN
   PCCA.Window_CurrentDW.TriggerEvent("pcd_Next")
END IF

end on

on pc_previous;//******************************************************************
//  PC Module     : w_Main
//  Event         : pc_Previous
//  Description   : Tells the current DataWindow to scroll to
//                  the previous row.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

//------------------------------------------------------------------
//  Trigger the corresponding DataWindow event on the current
//  DataWindow (if it valid).
//------------------------------------------------------------------

IF IsValid(PCCA.Window_CurrentDW) THEN
   PCCA.Window_CurrentDW.TriggerEvent("pcd_Previous")
END IF

end on

on pc_print;//******************************************************************
//  PC Module     : w_Main
//  Event         : pc_Print
//  Description   : Tells the current DataWindow to print its
//                  contents.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

//------------------------------------------------------------------
//  Trigger the corresponding DataWindow event on the current
//  DataWindow (if it valid).
//------------------------------------------------------------------

IF IsValid(PCCA.Window_CurrentDW) THEN
   PCCA.Window_CurrentDW.TriggerEvent("pcd_Print")
END IF

end on

on pc_query;//******************************************************************
//  PC Module     : w_Main
//  Event         : pc_Query
//  Description   : Tells the current DataWindow to go into query
//                  mode.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

//------------------------------------------------------------------
//  Trigger the corresponding DataWindow event on the current
//  DataWindow (if it valid).
//------------------------------------------------------------------

IF IsValid(PCCA.Window_CurrentDW) THEN
   PCCA.Window_CurrentDW.TriggerEvent("pcd_Query")
END IF

end on

on pc_retrieve;//******************************************************************
//  PC Module     : w_Main
//  Event         : pc_Retrieve
//  Description   : Retrieve data from the database for the
//                  current DataWindow.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

UNSIGNEDLONG  l_RefreshCmd
UO_DW_MAIN    l_CurDW

//------------------------------------------------------------------
//  Retrieve is a little more tricky than most window events
//  because we need to make sure that we don't clobber any changes
//  the user has made.
//------------------------------------------------------------------

IF IsValid(PCCA.Window_CurrentDW) THEN
   l_CurDW = PCCA.Window_CurrentDW

   IF l_CurDW.Check_Modifications(c_CheckForChanges) = c_Success THEN

      //------------------------------------------------------------
      //  Retrieve_DW() will force the retrieve to happend and
      //  will take care of refreshing the children DataWindows.
      //------------------------------------------------------------

      l_CurDW.Retrieve_DW(c_RetrieveAllDWs, &
                          c_NoReselectRows, &
                          c_RefreshSame)
   END IF
END IF

end on

on pc_save;//******************************************************************
//  PC Module     : w_Main
//  Event         : pc_Save
//  Description   : Tells the current DataWindow to save changes
//                  to the database.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

//------------------------------------------------------------------
//  Trigger the corresponding DataWindow event on the current
//  DataWindow (if it valid).
//------------------------------------------------------------------

IF IsValid(PCCA.Window_CurrentDW) THEN
   PCCA.Window_CurrentDW.TriggerEvent("pcd_Save")
END IF

end on

on pc_saverowsas;//******************************************************************
//  PC Module     : w_Main
//  Event         : pc_SaveRowsAs
//  Description   : Tells the current DataWindow to save its
//                  rows into a specified file format.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

//------------------------------------------------------------------
//  Trigger the corresponding DataWindow event on the current
//  DataWindow (if it valid).
//------------------------------------------------------------------

IF IsValid(PCCA.Window_CurrentDW) THEN
   PCCA.Window_CurrentDW.TriggerEvent("pcd_SaveRowsAs")
END IF

end on

on pc_search;//******************************************************************
//  PC Module     : w_Main
//  Event         : pc_Search
//  Description   : Tells the current DataWindow to build up the
//                  SQL statement for retrieve based on
//                  non-DataWindow fields and query mode.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

//------------------------------------------------------------------
//  Trigger the corresponding DataWindow event on the current
//  DataWindow (if it valid).
//------------------------------------------------------------------

IF IsValid(PCCA.Window_CurrentDW) THEN
   PCCA.Window_CurrentDW.TriggerEvent("pcd_Search")
END IF

end on

on pc_setopenparm;//******************************************************************
//  PC Module     : w_Main
//  Event         : pc_SetOpenParm
//  Description   : Triggered by the global Window_Open()
//                  functions to grab the PowerObject
//                  parameter passed on open.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

//------------------------------------------------------------------
//  Grab the PowerObject paramater out of the PCCA.
//------------------------------------------------------------------

i_OpenParm = PCCA.Open_Parm

end on

on pc_setsize;//******************************************************************
//  PC Module     : w_Main
//  Event         : pc_SetSize
//  Description   : Posted event which sets the window
//                  state when the window is opened.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

IF i_SavePosition THEN
IF i_INIValidWS   THEN
   IF WindowState <> i_INIWindowState THEN
      WindowState =  i_INIWindowState
   END IF
END IF
END IF

end on

event pc_setwindow;//******************************************************************
//  PC Module     : w_Main
//  Event         : pc_SetWindow
//  Description   : Initializes some pre-defined constants
//                  used with Set_DW_Options() and
//                  Set_DW_Defaults().
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

this.backcolor = s_themes.theme[ s_themes.current_theme].w_backcolor

c_MasterList = c_EnableNewOnOpen        + &
               c_EnableModifyOnOpen     + &
               c_DeleteOk               + &
               c_SelectOnRowFocusChange + &
               c_DrillDown

c_MasterEdit = c_NewOk                  + &
               c_ModifyOk               + &
               c_SelectOnRowFocusChange + &
               c_DrillDown              + &
               c_ScrollParent           + &
               c_RefreshParent

c_DetailList = c_EnableNewOnOpen        + &
               c_EnableModifyOnOpen     + &
               c_DeleteOk               + &
               c_SelectOnClick          + &
               c_DrillDown              + &
               c_RefreshParent          + &
               c_RefreshChild

c_DetailEdit = c_NewOk                  + &
               c_ModifyOk               + &
               c_SelectOnRowFocusChange + &
               c_DrillDown              + &
               c_ModifyOnSelect         + &
               c_ScrollParent           + &
               c_RefreshParent          + &
               c_RefreshChild

end event

on pc_view;//******************************************************************
//  PC Module     : w_Main
//  Event         : pc_View
//  Description   : Tells the current DataWindow to go into view
//                  mode.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

//------------------------------------------------------------------
//  Trigger the corresponding DataWindow event on the current
//  DataWindow (if it valid).
//------------------------------------------------------------------

IF IsValid(PCCA.Window_CurrentDW) THEN
   PCCA.Window_CurrentDW.TriggerEvent("pcd_View")
END IF

end on

on pc_wm_initmenu;//******************************************************************
//  PC Module     : w_Main
//  Event         : pc_WM_InitMenu
//  Description   : Sees if there is a current DataWindow and
//                  if there is not, disables the EDIT menu
//                  items.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

//------------------------------------------------------------------
//  If an pc_Activate is pending, make it happen now.
//------------------------------------------------------------------

IF PCCA.PCMGR.i_ProcessingActivate THEN
   PCCA.PCMGR.TriggerEvent("pc_Activate")
END IF

IF IsValid(PCCA.Window_CurrentDW) THEN
   IF NOT PCCA.Window_CurrentDW.i_Window.ToolBarVisible THEN
      PCCA.Window_CurrentDW.is_EventControl.Control_Mode = &
         PCCA.Window_CurrentDW.c_ControlMainMenu
      PCCA.Window_CurrentDW.TriggerEvent("pcd_SetControl")
   END IF
ELSE

   //---------------------------------------------------------------
   //  There is not a current DataWindow.  Disable all of the
   //  EDIT menu items if there is an EDIT menu.
   //---------------------------------------------------------------

   IF i_EditMenuIsValid THEN

      //------------------------------------------------------------
      //  The "New" menu item.
      //------------------------------------------------------------

      IF i_EditMenuNewNum > 0 THEN
         IF i_EditMenuNew.Enabled THEN
            i_EditMenuNew.Enabled = FALSE
         END IF
      END IF

      //------------------------------------------------------------
      //  The "View" menu item.
      //------------------------------------------------------------

      IF i_EditMenuViewNum > 0 THEN
         IF i_EditMenuView.Enabled THEN
            i_EditMenuView.Enabled = FALSE
         END IF
      END IF

      //------------------------------------------------------------
      //  The "Modify" menu item.
      //------------------------------------------------------------

      IF i_EditMenuModifyNum > 0 THEN
         IF i_EditMenuModify.Enabled THEN
            i_EditMenuModify.Enabled = FALSE
         END IF
      END IF

      //------------------------------------------------------------
      //  The "Insert" menu item.
      //------------------------------------------------------------

      IF i_EditMenuInsertNum > 0 THEN
         IF i_EditMenuInsert.Enabled THEN
            i_EditMenuInsert.Enabled = FALSE
         END IF
      END IF

      //------------------------------------------------------------
      //  The "Delete" menu item.
      //------------------------------------------------------------

      IF i_EditMenuDeleteNum > 0 THEN
         IF i_EditMenuDelete.Enabled THEN
            i_EditMenuDelete.Enabled = FALSE
         END IF
      END IF

      //------------------------------------------------------------
      //  The "First" menu item.
      //------------------------------------------------------------

      IF i_EditMenuFirstNum > 0 THEN
         IF i_EditMenuFirst.Enabled THEN
            i_EditMenuFirst.Enabled = FALSE
         END IF
      END IF

      //------------------------------------------------------------
      //  The "Prev" menu item.
      //------------------------------------------------------------

      IF i_EditMenuPrevNum > 0 THEN
         IF i_EditMenuPrev.Enabled THEN
            i_EditMenuPrev.Enabled = FALSE
         END IF
      END IF

      //------------------------------------------------------------
      //  The "Next" menu item.
      //------------------------------------------------------------

      IF i_EditMenuNextNum > 0 THEN
         IF i_EditMenuNext.Enabled THEN
            i_EditMenuNext.Enabled = FALSE
         END IF
      END IF

      //------------------------------------------------------------
      //  The "Last" menu item.
      //------------------------------------------------------------

      IF i_EditMenuLastNum > 0 THEN
         IF i_EditMenuLast.Enabled THEN
            i_EditMenuLast.Enabled = FALSE
         END IF
      END IF

      //------------------------------------------------------------
      //  The "Query" menu item.
      //------------------------------------------------------------

      IF i_EditMenuQueryNum > 0 THEN
         IF i_EditMenuQuery.Enabled THEN
            i_EditMenuQuery.Enabled = FALSE
         END IF
      END IF

      //------------------------------------------------------------
      //  The "Search" menu item.
      //------------------------------------------------------------

      IF i_EditMenuSearchNum > 0 THEN
         IF i_EditMenuSearch.Enabled THEN
            i_EditMenuSearch.Enabled = FALSE
         END IF
      END IF

      //------------------------------------------------------------
      //  The "SaveNum" menu item.
      //------------------------------------------------------------

      IF i_EditMenuSaveNum > 0 THEN
         IF i_EditMenuSave.Enabled THEN
            i_EditMenuSave.Enabled = FALSE
         END IF
      END IF

      //------------------------------------------------------------
      //  The "Save Rows As" menu item.
      //------------------------------------------------------------

      IF i_EditMenuSaveRowsAsNum > 0 THEN
         IF i_EditMenuSaveRowsAs.Enabled THEN
            i_EditMenuSaveRowsAs.Enabled = FALSE
         END IF
      END IF

      //------------------------------------------------------------
      //  The "Print" menu item.
      //------------------------------------------------------------

      IF i_EditMenuPrintNum > 0 THEN
         IF i_EditMenuPrint.Enabled THEN
            i_EditMenuPrint.Enabled = FALSE
         END IF
      END IF
   END IF
END IF

end on

on pc_wm_initmenupopup;//******************************************************************
//  PC Module     : w_Main
//  Event         : pc_WM_InitMenuPopup
//  Description   : Sees if there is a current DataWindow and
//                  if there is not, disables the POPUP menu
//                  items.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

//------------------------------------------------------------------
//  If an activate is pending, make it happen now.
//------------------------------------------------------------------

IF PCCA.PCMGR.i_ProcessingActivate THEN
   PCCA.PCMGR.TriggerEvent("pc_Activate")
END IF

IF IsValid(PCCA.Window_CurrentDW) THEN
   PCCA.Window_CurrentDW.is_EventControl.Control_Mode = &
      PCCA.Window_CurrentDW.c_ControlPopupMenu
   PCCA.Window_CurrentDW.TriggerEvent("pcd_SetControl")
ELSE

   //---------------------------------------------------------------
   //  There is not a current DataWindow.  Disable all of the
   //  POPUP menu items if there is a POPUP menu.
   //---------------------------------------------------------------

   IF NOT i_PopupMenuInit THEN
      Set_W_PMenu(i_PopupMenu)
   END IF

   IF i_PopupMenuIsValid THEN

      //------------------------------------------------------------
      //  The "New" menu item.
      //------------------------------------------------------------

      IF i_PopupMenuNewNum > 0 THEN
         IF i_PopupMenuNew.Enabled THEN
            i_PopupMenuNew.Enabled = FALSE
         END IF
      END IF

      //------------------------------------------------------------
      //  The "View" menu item.
      //------------------------------------------------------------

      IF i_PopupMenuViewNum > 0 THEN
         IF i_PopupMenuView.Enabled THEN
            i_PopupMenuView.Enabled = FALSE
         END IF
      END IF

      //------------------------------------------------------------
      //  The "Modify" menu item.
      //------------------------------------------------------------

      IF i_PopupMenuModifyNum > 0 THEN
         IF i_PopupMenuModify.Enabled THEN
            i_PopupMenuModify.Enabled = FALSE
         END IF
      END IF

      //------------------------------------------------------------
      //  The "Insert" menu item.
      //------------------------------------------------------------

      IF i_PopupMenuInsertNum > 0 THEN
         IF i_PopupMenuInsert.Enabled THEN
            i_PopupMenuInsert.Enabled = FALSE
         END IF
      END IF

      //------------------------------------------------------------
      //  The "Delete" menu item.
      //------------------------------------------------------------

      IF i_PopupMenuDeleteNum > 0 THEN
         IF i_PopupMenuDelete.Enabled THEN
            i_PopupMenuDelete.Enabled = FALSE
         END IF
      END IF

      //------------------------------------------------------------
      //  The "First" menu item.
      //------------------------------------------------------------

      IF i_PopupMenuFirstNum > 0 THEN
         IF i_PopupMenuFirst.Enabled THEN
            i_PopupMenuFirst.Enabled = FALSE
         END IF
      END IF

      //------------------------------------------------------------
      //  The "Prev" menu item.
      //------------------------------------------------------------

      IF i_PopupMenuPrevNum > 0 THEN
         IF i_PopupMenuPrev.Enabled THEN
            i_PopupMenuPrev.Enabled = FALSE
         END IF
      END IF

      //------------------------------------------------------------
      //  The "Next" menu item.
      //------------------------------------------------------------

      IF i_PopupMenuNextNum > 0 THEN
         IF i_PopupMenuNext.Enabled THEN
            i_PopupMenuNext.Enabled = FALSE
         END IF
      END IF

      //------------------------------------------------------------
      //  The "Last" menu item.
      //------------------------------------------------------------

      IF i_PopupMenuLastNum > 0 THEN
         IF i_PopupMenuLast.Enabled THEN
            i_PopupMenuLast.Enabled = FALSE
         END IF
      END IF

      //------------------------------------------------------------
      //  The "Query" menu item.
      //------------------------------------------------------------

      IF i_PopupMenuQueryNum > 0 THEN
         IF i_PopupMenuQuery.Enabled THEN
            i_PopupMenuQuery.Enabled = FALSE
         END IF
      END IF

      //------------------------------------------------------------
      //  The "Search" menu item.
      //------------------------------------------------------------

      IF i_PopupMenuSearchNum > 0 THEN
         IF i_PopupMenuSearch.Enabled THEN
            i_PopupMenuSearch.Enabled = FALSE
         END IF
      END IF

      //------------------------------------------------------------
      //  The "SaveNum" menu item.
      //------------------------------------------------------------

      IF i_PopupMenuSaveNum > 0 THEN
         IF i_PopupMenuSave.Enabled THEN
            i_PopupMenuSave.Enabled = FALSE
         END IF
      END IF

      //------------------------------------------------------------
      //  The "Save Rows As" menu item.
      //------------------------------------------------------------

      IF i_PopupMenuSaveRowsAsNum > 0 THEN
         IF i_PopupMenuSaveRowsAs.Enabled THEN
            i_PopupMenuSaveRowsAs.Enabled = FALSE
         END IF
      END IF

      //------------------------------------------------------------
      //  The "Print" menu item.
      //------------------------------------------------------------

      IF i_PopupMenuPrintNum > 0 THEN
         IF i_PopupMenuPrint.Enabled THEN
            i_PopupMenuPrint.Enabled = FALSE
         END IF
      END IF
   END IF
END IF

end on

on pc_wm_move;//******************************************************************
//  PC Module     : w_Main
//  Event         : pc_WM_Move
//  Description   : Handles the move event for the window.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

//------------------------------------------------------------------
//  Save the last position information.
//------------------------------------------------------------------

IF WindowState = Normal! THEN
   i_INISaveIsValid  = TRUE
   i_INISaveLastXPos = i_INILastXPos
   i_INISaveLastYPos = i_INILastYPos
   i_INILastXPos     = X
   i_INILastYPos     = Y
END IF
end on

public subroutine get_ini_size ();//******************************************************************
//  PC Module     : w_Main
//  Subroutine    : Get_INI_Size
//  Description   : Gets postion, size, and window state
//                  information from the INI file.
//
//  Parameters    : (None)
//
//  Return Value  : (None).  However, the following instance
//                  variables are modified:
//
//                     BOOLEAN      i_INIValidSize
//                     BOOLEAN      i_INIValidWS
//                     INTEGER      i_INIXPos
//                     INTEGER      i_INIYPos
//                     INTEGER      i_INIWidth
//                     INTEGER      i_INIHeight
//                     WINDOWSTATE  i_INIWindowState
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

STRING  l_SXPos,  l_SYPos
STRING  l_SWidth, l_SHeight, l_WinState

//------------------------------------------------------------------
//  Indictate that this routine has been called.
//------------------------------------------------------------------

i_INIGetDone = TRUE

//------------------------------------------------------------------
//  Get the window position from the INI file.
//------------------------------------------------------------------

l_SXPos    = ProfileString(PCCA.Application_INI_File, &
                           i_INIFileSection,          &
                           c_XKey,                    &
                           c_INIUndefined)
l_SYPos    = ProfileString(PCCA.Application_INI_File, &
                           i_INIFileSection,          &
                           c_YKey,                    &
                           c_INIUndefined)
l_SWidth   = ProfileString(PCCA.Application_INI_File, &
                           i_INIFileSection,          &
                           c_WidthKey,                &
                           c_INIUndefined)
l_SHeight  = ProfileString(PCCA.Application_INI_File, &
                           i_INIFileSection,          &
                           c_HeightKey,               &
                           c_INIUndefined)
l_WinState = ProfileString(PCCA.Application_INI_File, &
                           i_INIFileSection,          &
                           c_WinStateKey,             &
                           c_INIUndefined)

//------------------------------------------------------------------

i_INIValidSize = (l_SXPos   <> c_INIUndefined AND &
                  l_SYPos   <> c_INIUndefined AND &
                  l_SWidth  <> c_INIUndefined AND &
                  l_SHeight <> c_INIUndefined)

IF i_INIValidSize THEN
   i_INIXPos   = Integer(l_SXPos)
   i_INIYPos   = Integer(l_SYPos)
   i_INIWidth  = Integer(l_SWidth)
   i_INIHeight = Integer(l_SHeight)
ELSE
   i_INIXPos   = X
   i_INIYPos   = Y
   i_INIWidth  = Width
   i_INIHeight = Height
END IF

//------------------------------------------------------------------
//  Make sure that the window is not an unreasonable size and at
//  least a corner of it is on the display.
//------------------------------------------------------------------

IF i_INIWidth < c_MinimumOverlap THEN
   i_INIWidth = c_MinimumOverlap
END IF

IF i_INIHeight < c_MinimumOverlap THEN
   i_INIHeight = c_MinimumOverlap
END IF

IF i_INIXPos > PCCA.Screen_Width THEN
   i_INIXPos = PCCA.Screen_Width - c_MinimumOverlap
END IF
IF i_INIXPos + i_INIWidth < 1 THEN
   i_INIXPos = c_MinimumOverlap - i_INIWidth
END IF

IF i_INIYPos > PCCA.Screen_Height THEN
   i_INIYPos = PCCA.Screen_Height - c_MinimumOverlap
END IF
IF i_INIYPos + i_INIHeight < 1 THEN
   i_INIYPos = c_MinimumOverlap - i_INIHeight
END IF

//------------------------------------------------------------------
//  Check for window state.
//------------------------------------------------------------------

i_INIValidWS = (l_WinState <> c_INIUndefined)

IF i_INIValidWS THEN
   CHOOSE CASE l_WinState
      CASE "Maximized"
         i_INIWindowState = Maximized!
      CASE "Minimized"
         i_INIWindowState = Minimized!
      //CASE "Normal"
      CASE ELSE
         i_INIWindowState = Normal!
   END CHOOSE
ELSE
   i_INIWindowState = Normal!
END IF

RETURN
end subroutine

public subroutine put_ini_size ();//******************************************************************
//  PC Module     : w_Main
//  Subroutine    : Put_INI_Size
//  Description   : Puts the postion, size, and window state
//                  information to the INI file.
//
//  Parameters    : (None)
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

STRING  l_WinState

//------------------------------------------------------------------
//  Indictate that this routine has been called.
//------------------------------------------------------------------

i_INIPutDone = TRUE

//------------------------------------------------------------------
//  Convert WindowState to a string so that it can be stored in
//  the INI file.
//------------------------------------------------------------------

IF i_INILastWindowState = Maximized! THEN
   l_WinState = "Maximized"
ELSE
   IF i_INILastWindowState = Minimized! THEN
      l_WinState = "Minimized"
   ELSE
      l_WinState = "Normal"
   END IF
END IF

//------------------------------------------------------------------
//  Store the position, size, and window state information.
//------------------------------------------------------------------

SetProfileString(PCCA.Application_INI_File, &
                 i_INIFileSection,          &
                 c_XKey,                    &
                 String(i_INILastXPos))
SetProfileString(PCCA.Application_INI_File, &
                 i_INIFileSection,          &
                 c_YKey,                    &
                 String(i_INILastYPos))
SetProfileString(PCCA.Application_INI_File, &
                 i_INIFileSection,          &
                 c_WidthKey,                &
                 String(i_INILastWidth))
SetProfileString(PCCA.Application_INI_File, &
                 i_INIFileSection,          &
                 c_HeightKey,               &
                 String(i_INILastHeight))
SetProfileString(PCCA.Application_INI_File, &
                 i_INIFileSection,          &
                 c_WinStateKey,             &
                 l_WinState)

RETURN
end subroutine

public subroutine save_on_close (unsignedlong close_option);//******************************************************************
//  PC Module     : w_Main
//  Subroutine    : Save_On_Close
//  Description   : Allows the developer to specify what
//                  should happen to modifications in the
//                  DataWindows when the window is closed.
//
//  Parameters    : UNSIGNEDLONG Close_Option -
//                       Should be specified as one of the
//                       following:
//
//                          - c_SOCPromptUser
//                              Indicates that PowerClass is to
//                              prompt the user for each
//                              DataWindow hierarchy which has
//                              been modified when the window
//                              is closed.
//
//                          - c_SOCPromptUserOnce
//                              Indicates that PowerClass is to
//                              prompt the user once if any of
//                              the DataWindow hierarchies have
//                              been modified when the window
//                              is closed.
//
//                          - c_SOCSave
//                              Indicates that PowerClass is
//                              to automatically save all of
//                              DataWindows which contain
//                              modifications when the window
//                              is closed.
//
//                          - c_SOCNoSave
//                              Indicates that PowerClass is
//                              NOT to save any DataWindows.
//                              when the window is closed.
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

i_SaveOnClose = Close_Option

RETURN
end subroutine

public subroutine set_ini_section (string file_section);//******************************************************************
//  PC Module     : w_Main
//  Subroutine    : Set_INI_Section
//  Description   : Sets the file section that the window's
//                  size and position will be saved to in
//                  the INI file.
//
//  Parameters    : STRING File_Section -
//                        The section in the INI file where to
//                        place the window information.
//                        By default, PowerClass will use the
//                        following format if the developer does
//                        not call this routine:
//
//                           PCCA.Application_Name + &
//                              "/" + <window>.ClassName()
//
//                        Example:
//                           "PowerClass Sample/w_mdi_frame"
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

i_INIFileSection = File_Section

RETURN
end subroutine

public subroutine set_tb_float_position (long x_pos, long y_pos);//******************************************************************
//  PC Module     : w_Main
//  Subroutine    : Set_TB_Float_Position
//  Description   : Set Toolbar float position
//
//  Parameters    : LONG  X_Pos -
//                        X location for the toolbar.
//                  LONG  Y_Pos -
//                        Y location for the toolbar.
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

IF PCCA.MDI_Frame_Valid THEN
   i_ToolBarPosition = c_ToolBarFloat

   ToolBarX          = X_Pos
   ToolBarY          = Y_Pos
   ToolBarVisible    = TRUE
   ToolBarAlignment  = Floating!
END IF

RETURN
end subroutine

public subroutine set_w_emenu (menu edit_menu);//******************************************************************
//  PC Module     : w_Main
//  Subroutine    : Set_W_EMenu
//  Description   : Initializes the EDIT menu for w_Main.
//
//  Parameters    : MENU Edit_Menu -
//                        The menu that is to be used
//                        as the EDIT menu by PowerClass.
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Idx, l_Jdx
MENU     l_TmpMenu

//------------------------------------------------------------------
//  We indicate that this routine has been called by setting
//  i_EditMenuInit to TRUE.  If this routine is called by the
//  developer, this flag will be TRUE and PowerClass will know
//  not to not call it again.
//------------------------------------------------------------------

i_EditMenuInit = TRUE

//------------------------------------------------------------------
//  Set the our menu to the specified menu.
//------------------------------------------------------------------

i_EditMenu = Edit_Menu

//------------------------------------------------------------------
//  See if the EDIT Menu is valid.  If it is, save the individual
//  menu items into instance variables for speedy access.
//------------------------------------------------------------------

i_EditMenuIsValid       = IsValid(i_EditMenu)
i_EditMenuNewNum        = 0
i_EditMenuViewNum       = 0
i_EditMenuModifyNum     = 0
i_EditMenuInsertNum     = 0
i_EditMenuDeleteNum     = 0
i_EditMenuModeSepNum    = 0
i_EditMenuFirstNum      = 0
i_EditMenuPrevNum       = 0
i_EditMenuNextNum       = 0
i_EditMenuLastNum       = 0
i_EditMenuSearchSepNum  = 0
i_EditMenuQueryNum      = 0
i_EditMenuSearchNum     = 0
i_EditMenuFilterNum     = 0
i_EditMenuSaveSepNum    = 0
i_EditMenuSaveNum       = 0
i_EditMenuSaveRowsAsNum = 0
i_EditMenuPrintSepNum   = 0
i_EditMenuPrintNum      = 0

IF i_EditMenuIsValid THEN
   l_Jdx = UpperBound(i_EditMenu.Item[])
   FOR l_Idx = 1 TO l_Jdx
      l_TmpMenu = i_EditMenu.Item[l_Idx]
      CHOOSE CASE l_TmpMenu.Text
         CASE PCCA.MDI.c_MDI_MenuLabelNew
            i_EditMenuNewNum        = l_Idx
            i_EditMenuNew           = l_TmpMenu
         CASE PCCA.MDI.c_MDI_MenuLabelView
            i_EditMenuViewNum       = l_Idx
            i_EditMenuView          = l_TmpMenu
         CASE PCCA.MDI.c_MDI_MenuLabelModify
            i_EditMenuModifyNum     = l_Idx
            i_EditMenuModify        = l_TmpMenu
         CASE PCCA.MDI.c_MDI_MenuLabelInsert
            i_EditMenuInsertNum     = l_Idx
            i_EditMenuInsert        = l_TmpMenu
         CASE PCCA.MDI.c_MDI_MenuLabelDelete
            i_EditMenuDeleteNum     = l_Idx
            i_EditMenuDelete        = l_TmpMenu
            IF l_Idx < l_Jdx THEN
               IF i_EditMenu.Item[l_Idx + 1].Text = &
                  PCCA.MDI.c_MDI_MenuLabelSeperator THEN
                  i_EditMenuModeSepNum = l_Idx + 1
                  i_EditMenuModeSep    = &
                     i_EditMenu.Item[l_Idx + 1]
               END IF
            END IF
         CASE PCCA.MDI.c_MDI_MenuLabelFirst
            i_EditMenuFirstNum      = l_Idx
            i_EditMenuFirst         = l_TmpMenu
         CASE PCCA.MDI.c_MDI_MenuLabelPrev
            i_EditMenuPrevNum       = l_Idx
            i_EditMenuPrev          = l_TmpMenu
         CASE PCCA.MDI.c_MDI_MenuLabelNext
            i_EditMenuNextNum       = l_Idx
            i_EditMenuNext          = l_TmpMenu
         CASE PCCA.MDI.c_MDI_MenuLabelLast
            i_EditMenuLastNum       = l_Idx
            i_EditMenuLast          = l_TmpMenu
         CASE PCCA.MDI.c_MDI_MenuLabelQuery
            i_EditMenuQueryNum      = l_Idx
            i_EditMenuQuery         = l_TmpMenu
            IF l_Idx > 1 THEN
               IF i_EditMenu.Item[l_Idx - 1].Text = &
                  PCCA.MDI.c_MDI_MenuLabelSeperator THEN
                  i_EditMenuSearchSepNum = l_Idx - 1
                  i_EditMenuSearchSep    = &
                     i_EditMenu.Item[l_Idx - 1]
               END IF
            END IF
         CASE PCCA.MDI.c_MDI_MenuLabelSearch
            i_EditMenuSearchNum     = l_Idx
            i_EditMenuSearch        = l_TmpMenu
         CASE PCCA.MDI.c_MDI_MenuLabelFilter
            i_EditMenuFilterNum     = l_Idx
            i_EditMenuFilter        = l_TmpMenu
         CASE PCCA.MDI.c_MDI_MenuLabelSave
            i_EditMenuSaveNum       = l_Idx
            i_EditMenuSave          = l_TmpMenu
            IF l_Idx > 1 THEN
               IF i_EditMenu.Item[l_Idx - 1].Text = &
                  PCCA.MDI.c_MDI_MenuLabelSeperator THEN
                  i_EditMenuSaveSepNum = l_Idx - 1
                  i_EditMenuSaveSep    = &
                     i_EditMenu.Item[l_Idx - 1]
               END IF
            END IF
         CASE PCCA.MDI.c_MDI_MenuLabelSaveRowsAs
            i_EditMenuSaveRowsAsNum = l_Idx
            i_EditMenuSaveRowsAs    = l_TmpMenu
         CASE PCCA.MDI.c_MDI_MenuLabelPrint
            i_EditMenuPrintNum      = l_Idx
            i_EditMenuPrint         = l_TmpMenu
            IF l_Idx > 1 THEN
               IF i_EditMenu.Item[l_Idx - 1].Text = &
                  PCCA.MDI.c_MDI_MenuLabelSeperator THEN
                  i_EditMenuPrintSepNum = l_Idx - 1
                  i_EditMenuPrintSep    = &
                     i_EditMenu.Item[l_Idx - 1]
               END IF
            END IF
      END CHOOSE
   NEXT
END IF

IF i_EditMenuIsPopup THEN
   PCCA.DLL.fu_SetWMPMenu (THIS)
END IF

RETURN
end subroutine

public subroutine set_w_fmenu (menu file_menu);//******************************************************************
//  PC Module     : w_Main
//  Subroutine    : Set_W_FMenu
//  Description   : Initializes the FILE menu for w_Main.
//
//  Parameters    : MENU File_Menu -
//                        The menu that is to be used
//                        as the FILE menu by PowerClass.
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Idx, l_Jdx
MENU     l_TmpMenu

//------------------------------------------------------------------
//  We indicate that this routine has been called by setting
//  i_FileMenuInit to TRUE.  If this routine is called by the
//  developer, this flag will be TRUE and PowerClass will know
//  not to not call it again.
//------------------------------------------------------------------

i_FileMenuInit = TRUE

//------------------------------------------------------------------
//  Set the our menu to the specified menu.
//------------------------------------------------------------------

i_FileMenu = File_Menu

//------------------------------------------------------------------
//  See if the FILE Menu is valid.  If it is, save the individual
//  menu items into instance variables for speedy access.
//------------------------------------------------------------------

i_FileMenuIsValid       = IsValid(i_FileMenu)
i_FileMenuNewNum        = 0
i_FileMenuOpenNum       = 0
i_FileMenuCloseNum      = 0
i_FileMenuFileSepNum    = 0
i_FileMenuCancelNum     = 0
i_FileMenuAcceptNum     = 0
i_FileMenuSaveSepNum    = 0
i_FileMenuSaveNum       = 0
i_FileMenuSaveRowsAsNum = 0
i_FileMenuPrintSepNum   = 0
i_FileMenuPrintNum      = 0
i_FileMenuPrintSetupNum = 0
i_FileMenuExitSepNum    = 0
i_FileMenuExitNum       = 0

IF i_FileMenuIsValid THEN
   l_Jdx = UpperBound(i_FileMenu.Item[])
   FOR l_Idx = 1 TO l_Jdx
      l_TmpMenu = i_FileMenu.Item[l_Idx]
      CHOOSE CASE l_TmpMenu.Text
         CASE PCCA.MDI.c_MDI_MenuLabelNew
            i_FileMenuNewNum        = l_Idx
            i_FileMenuNew           = l_TmpMenu
         CASE PCCA.MDI.c_MDI_MenuLabelOpen
            i_FileMenuOpenNum       = l_Idx
            i_FileMenuOpen          = l_TmpMenu
         CASE PCCA.MDI.c_MDI_MenuLabelClose
            i_FileMenuCloseNum      = l_Idx
            i_FileMenuClose         = l_TmpMenu
            IF l_Idx < l_Jdx THEN
               IF i_FileMenu.Item[l_Idx + 1].Text = &
                  PCCA.MDI.c_MDI_MenuLabelSeperator THEN
                  i_FileMenuFileSepNum = l_Idx + 1
                  i_FileMenuFileSep    = &
                     i_FileMenu.Item[l_Idx + 1]
               END IF
            END IF
         CASE PCCA.MDI.c_MDI_MenuLabelCancel
            i_FileMenuCancelNum     = l_Idx
            i_FileMenuCancel        = l_TmpMenu
         CASE PCCA.MDI.c_MDI_MenuLabelAccept
            i_FileMenuAcceptNum     = l_Idx
            i_FileMenuAccept        = l_TmpMenu
         CASE PCCA.MDI.c_MDI_MenuLabelSave
            i_FileMenuSaveNum       = l_Idx
            i_FileMenuSave          = l_TmpMenu
            IF l_Idx > 1 THEN
               IF i_FileMenu.Item[l_Idx - 1].Text = &
                  PCCA.MDI.c_MDI_MenuLabelSeperator THEN
                  i_FileMenuSaveSepNum = l_Idx - 1
                  i_FileMenuSaveSep    = &
                     i_FileMenu.Item[l_Idx - 1]
               END IF
            END IF
         CASE PCCA.MDI.c_MDI_MenuLabelSaveRowsAs
            i_FileMenuSaveRowsAsNum = l_Idx
            i_FileMenuSaveRowsAs    = l_TmpMenu
         CASE PCCA.MDI.c_MDI_MenuLabelPrint
            i_FileMenuPrintNum      = l_Idx
            i_FileMenuPrint         = l_TmpMenu
            IF l_Idx > 1 THEN
               IF i_FileMenu.Item[l_Idx - 1].Text = &
                  PCCA.MDI.c_MDI_MenuLabelSeperator THEN
                  i_FileMenuPrintSepNum = l_Idx - 1
                  i_FileMenuPrintSep    = &
                     i_FileMenu.Item[l_Idx - 1]
               END IF
            END IF
         CASE PCCA.MDI.c_MDI_MenuLabelPrintSetup
            i_FileMenuPrintSetupNum = l_Idx
            i_FileMenuPrintSetup    = l_TmpMenu
         CASE PCCA.MDI.c_MDI_MenuLabelExit
            i_FileMenuExitNum       = l_Idx
            i_FileMenuExit          = l_TmpMenu
            IF l_Idx > 1 THEN
               IF i_FileMenu.Item[l_Idx - 1].Text = &
                  PCCA.MDI.c_MDI_MenuLabelSeperator THEN
                  i_FileMenuExitSepNum = l_Idx - 1
                  i_FileMenuExitSep    = &
                     i_FileMenu.Item[l_Idx - 1]
               END IF
            END IF
      END CHOOSE
   NEXT
END IF

RETURN
end subroutine

public subroutine set_w_options (unsignedlong control_word);//******************************************************************
//  PC Module     : w_Main
//  Subroutine    : Set_W_Options
//  Description   : Looks at the passed words and sets the window
//                  options.
//
//  Parameters    : UNSIGNEDLONG Control_Word -
//                        Specifies options for how this
//                        window is to behave.
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Idx

//------------------------------------------------------------------
//  Indicate that Set_W_Options() has run, so that the Open
//  event will not run it again.
//------------------------------------------------------------------

i_OptionsInit = TRUE

//------------------------------------------------------------------
//  Remember the developer's specifications
//------------------------------------------------------------------

i_W_ControlWord = Control_Word

//------------------------------------------------------------------
//  Have the DLL calculate the bits.
//------------------------------------------------------------------

PCCA.DLL.fu_SetWMOptions (THIS, Control_Word)

//------------------------------------------------------------------
//  Process how the PowerBuilder Toolbar will be handled.
//------------------------------------------------------------------

IF PCCA.MDI_Frame_Valid THEN
   IF WindowType <> Response! THEN
      CHOOSE CASE i_ToolBarPosition
         CASE c_ToolBarLeft
            ToolBarVisible   = TRUE
            ToolBarAlignment = AlignAtLeft!
         CASE c_ToolBarRight
            ToolBarVisible   = TRUE
            ToolBarAlignment = AlignAtRight!
         CASE c_ToolBarTop
            ToolBarVisible   = TRUE
            ToolBarAlignment = AlignAtTop!
         CASE c_ToolBarBottom
            ToolBarVisible   = TRUE
            ToolBarAlignment = AlignAtBottom!
         CASE c_ToolBarFloat
            ToolBarVisible   = TRUE
            ToolBarAlignment = Floating!
         //CASE c_ToolBarNone
         CASE ELSE
            ToolBarVisible   = FALSE
      END CHOOSE

      //------------------------------------------------------------
      //  Process how the PowerBuilder Toolbar Text will be
      //  handled.
      //------------------------------------------------------------

      IF PCCA.Application_Obj.ToolBarText <> i_ToolBarText THEN
         PCCA.Application_Obj.ToolBarText = i_ToolBarText
      END IF
   END IF

   //---------------------------------------------------------------
   //  Now that the ToolBar has been made visible or non-visible,
   //  turn redraw of the MDI client area back on.  This will allow
   //  the menus and toolbars to appear to let the user know that
   //  progress is being made.
   //---------------------------------------------------------------

   IF PCCA.MDI_Allow_Redraw THEN
      PCCA.MDI_Frame.SetRedraw(TRUE)
   END IF
END IF

RETURN
end subroutine

public subroutine set_w_pmenu (menu popup_menu);//******************************************************************
//  PC Module     : w_Main
//  Subroutine    : Set_W_PMenu
//  Description   : Initializes the POPUP menu for w_Main.
//
//  Parameters    : MENU Popup -
//                       The POPUP menu that is to be used
//                       by this window.  If it is
//                       specified as c_NullMenu, then a
//                       POPUP menu will be created from
//                       M_POPUP.
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Idx, l_Jdx
MENU     l_TmpMenu

//------------------------------------------------------------------
//  We indicate that this routine has been called by setting
//  i_PopupMenuInit to TRUE.  If this routine is called by the
//  developer, this flag will be TRUE and PowerClass will know
//  not to not call it again.
//------------------------------------------------------------------

i_PopupMenuInit = TRUE

//------------------------------------------------------------------
//  Set our menu to the specified menu.
//------------------------------------------------------------------

i_PopupMenu = Popup_Menu

//------------------------------------------------------------------
//  See if the POPUP Menu is valid.  If it is, save the individual
//  menu items into instance variables for speedy access.
//------------------------------------------------------------------

i_PopupMenuIsValid       = IsValid(i_PopupMenu)
i_PopupMenuNewNum        = 0
i_PopupMenuViewNum       = 0
i_PopupMenuModifyNum     = 0
i_PopupMenuInsertNum     = 0
i_PopupMenuDeleteNum     = 0
i_PopupMenuModeSepNum    = 0
i_PopupMenuFirstNum      = 0
i_PopupMenuPrevNum       = 0
i_PopupMenuNextNum       = 0
i_PopupMenuLastNum       = 0
i_PopupMenuSearchSepNum  = 0
i_PopupMenuQueryNum      = 0
i_PopupMenuSearchNum     = 0
i_PopupMenuFilterNum     = 0
i_PopupMenuSaveSepNum    = 0
i_PopupMenuSaveNum       = 0
i_PopupMenuSaveRowsAsNum = 0
i_PopupMenuPrintSepNum   = 0
i_PopupMenuPrintNum      = 0

//------------------------------------------------------------------
//  Determine if the POPUP menu is the same menu as the EDIT Menu.
//------------------------------------------------------------------

IF i_PopupMenuIsValid THEN
    IF i_EditMenuIsValid THEN
       i_PopupMenuIsEdit = (i_EditMenu = i_PopupMenu)
    ELSE
       i_PopupMenuIsEdit = FALSE
   END IF
ELSE

   //---------------------------------------------------------------
   //  The POPUP Menu is not valid.  Indicate that the EDIT Menu
   //  should be used when the POPUP Menu is needed.
   //---------------------------------------------------------------

   i_PopupMenuIsEdit = TRUE
END IF

i_EditMenuIsPopup = i_PopupMenuIsEdit

//------------------------------------------------------------------
//  If the POPUP Menu is not the same as the EDIT Menu, then
//  save the individual menu items into instance variables for
//  speedy access.
//------------------------------------------------------------------

IF i_PopupMenuIsValid THEN
   IF NOT i_PopupMenuIsEdit THEN
      l_Jdx = UpperBound(i_PopupMenu.Item[])
      FOR l_Idx = 1 TO l_Jdx
         l_TmpMenu = i_PopupMenu.Item[l_Idx]
         CHOOSE CASE l_TmpMenu.Text
            CASE PCCA.MDI.c_MDI_MenuLabelNew
               i_PopupMenuNewNum        = l_Idx
               i_PopupMenuNew           = l_TmpMenu
            CASE PCCA.MDI.c_MDI_MenuLabelView
               i_PopupMenuViewNum       = l_Idx
               i_PopupMenuView          = l_TmpMenu
            CASE PCCA.MDI.c_MDI_MenuLabelModify
               i_PopupMenuModifyNum     = l_Idx
               i_PopupMenuModify        = l_TmpMenu
            CASE PCCA.MDI.c_MDI_MenuLabelInsert
               i_PopupMenuInsertNum     = l_Idx
               i_PopupMenuInsert        = l_TmpMenu
            CASE PCCA.MDI.c_MDI_MenuLabelDelete
               i_PopupMenuDeleteNum     = l_Idx
               i_PopupMenuDelete        = l_TmpMenu
               IF l_Idx < l_Jdx THEN
                  IF i_PopupMenu.Item[l_Idx + 1].Text = &
                     PCCA.MDI.c_MDI_MenuLabelSeperator THEN
                     i_PopupMenuModeSepNum = l_Idx + 1
                     i_PopupMenuModeSep    = &
                        i_PopupMenu.Item[l_Idx + 1]
                  END IF
               END IF
            CASE PCCA.MDI.c_MDI_MenuLabelFirst
               i_PopupMenuFirstNum      = l_Idx
               i_PopupMenuFirst         = l_TmpMenu
            CASE PCCA.MDI.c_MDI_MenuLabelPrev
               i_PopupMenuPrevNum       = l_Idx
               i_PopupMenuPrev          = l_TmpMenu
            CASE PCCA.MDI.c_MDI_MenuLabelNext
               i_PopupMenuNextNum       = l_Idx
               i_PopupMenuNext          = l_TmpMenu
            CASE PCCA.MDI.c_MDI_MenuLabelLast
               i_PopupMenuLastNum       = l_Idx
               i_PopupMenuLast          = l_TmpMenu
            CASE PCCA.MDI.c_MDI_MenuLabelQuery
               i_PopupMenuQueryNum      = l_Idx
               i_PopupMenuQuery         = l_TmpMenu
               IF l_Idx > 1 THEN
                  IF i_PopupMenu.Item[l_Idx - 1].Text = &
                     PCCA.MDI.c_MDI_MenuLabelSeperator THEN
                     i_PopupMenuSearchSepNum = l_Idx - 1
                     i_PopupMenuSearchSep    = &
                        i_PopupMenu.Item[l_Idx - 1]
                  END IF
               END IF
            CASE PCCA.MDI.c_MDI_MenuLabelSearch
               i_PopupMenuSearchNum     = l_Idx
               i_PopupMenuSearch        = l_TmpMenu
            CASE PCCA.MDI.c_MDI_MenuLabelFilter
               i_PopupMenuFilterNum     = l_Idx
               i_PopupMenuFilter        = l_TmpMenu
            CASE PCCA.MDI.c_MDI_MenuLabelSave
               i_PopupMenuSaveNum       = l_Idx
               i_PopupMenuSave          = l_TmpMenu
               IF l_Idx > 1 THEN
                  IF i_PopupMenu.Item[l_Idx - 1].Text = &
                     PCCA.MDI.c_MDI_MenuLabelSeperator THEN
                     i_PopupMenuSaveSepNum = l_Idx - 1
                     i_PopupMenuSaveSep    = &
                        i_PopupMenu.Item[l_Idx - 1]
                  END IF
               END IF
            CASE PCCA.MDI.c_MDI_MenuLabelSaveRowsAs
               i_PopupMenuSaveRowsAsNum = l_Idx
               i_PopupMenuSaveRowsAs    = l_TmpMenu
            CASE PCCA.MDI.c_MDI_MenuLabelPrint
               i_PopupMenuPrintNum      = l_Idx
               i_PopupMenuPrint         = l_TmpMenu
               IF l_Idx > 1 THEN
                  IF i_PopupMenu.Item[l_Idx - 1].Text = &
                     PCCA.MDI.c_MDI_MenuLabelSeperator THEN
                     i_PopupMenuPrintSepNum = l_Idx - 1
                     i_PopupMenuPrintSep    = &
                        i_PopupMenu.Item[l_Idx - 1]
                  END IF
               END IF
         END CHOOSE
      NEXT
   ELSE
      PCCA.DLL.fu_SetWMPMenu (THIS)
   END IF
END IF

RETURN
end subroutine

public subroutine set_w_popup (menu popup_menu);//******************************************************************
//  PC Module     : w_Main
//  Subroutine    : Set_W_Popup
//  Description   : Sets the POPUP menu to be used by the window.
//
//  Parameters    : MENU Popup -
//                       The POPUP menu that is to be used
//                       by this window.  If it is
//                       specified as c_NullMenu, then a
//                       POPUP menu will be created from
//                       M_POPUP.
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Idx, l_Jdx

//------------------------------------------------------------------
//  If the passed POPUP menu is not valid, then create a popup
//  of type M_POPUP.
//------------------------------------------------------------------

IF IsValid(Popup_Menu) THEN
   i_PopupMenu = Popup_Menu.Item[1]
ELSE
   IF IsValid(i_mPopup) THEN
   ELSE
      i_PopupMenu = c_NullMenu
      i_mPopup    = CREATE M_POPUP
   END IF

   IF IsValid(i_mPopup) THEN
      i_PopupMenu         = i_mPopup.Item[1]
      i_PopupMenu.Visible = TRUE

      l_Jdx = UpperBound(i_PopupMenu.Item[])
      FOR l_Idx = 1 TO l_Jdx
          i_PopupMenu.Item[l_Idx].Visible = TRUE
          i_PopupMenu.Item[l_Idx].Enabled = FALSE
      NEXT
   END IF
END IF

IF IsValid(i_PopupMenu) THEN
   i_PopupMenuIsValid = TRUE
ELSE
   i_PopupMenuIsValid = FALSE
END IF

RETURN
end subroutine

public subroutine debug (string event_name, string prefix, integer debug_level);
end subroutine

public subroutine proc_entry (string proc_name, integer debug_dump_level);
end subroutine

public subroutine proc_exit ();
end subroutine

event resize;//******************************************************************
//  PC Module     : w_Main
//  Event         : Resize
//  Description   : Resize the DataWindow(s) when the window is
//                  resized by the user.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN            l_TriggeredEvent, l_Control, l_Visible
INTEGER            l_Idx
REAL               l_NewScaleX, l_NewScaleY, l_AspectRatio
REAL               l_NewScaleArea, l_TmpNewWidth, l_TmpNewHeight
UO_DW_MAIN         l_TmpDW
UO_CONTAINER_MAIN  l_TmpCont

IF i_WindowWidth = 0 THEN
   RETURN
END IF

//------------------------------------------------------------------
//  Save the last size and window state information.
//------------------------------------------------------------------

IF WindowState = Normal! THEN
   i_INILastWidth  = Width
   i_INILastHeight = Height
ELSE
   IF i_INISaveIsValid THEN
      i_INISaveIsValid = FALSE
      i_INILastXPos    = i_INISaveLastXPos
      i_INILastYPos    = i_INISaveLastYPos
   END IF
END IF

i_INILastWindowState = WindowState

//------------------------------------------------------------------
//  Determine if this event was triggered by another PowerClass
//  event.
//------------------------------------------------------------------

l_TriggeredEvent = PCCA.Do_Event

//------------------------------------------------------------------
//  Remember if the control key is down.
//------------------------------------------------------------------

l_Control = KeyDown(keyControl!)

//------------------------------------------------------------------
//  If we are minimized, then resizing the controls will not
//  make a difference.
//------------------------------------------------------------------

IF WindowState <> Minimized! THEN

   l_NewScaleX   = WorkSpaceWidth()  / i_WindowWidth
   l_NewScaleY   = WorkSpaceHeight() / i_WindowHeight
   l_AspectRatio = i_WindowWidth / i_WindowHeight

   IF WorkSpaceWidth()/WorkSpaceHeight() <= l_AspectRatio THEN
      l_TmpNewWidth  = WorkSpaceWidth()
      l_TmpNewHeight = WorkSpaceWidth() / l_AspectRatio
   ELSE
      l_TmpNewHeight = WorkSpaceHeight()
      l_TmpNewWidth  = WorkSpaceHeight() * l_AspectRatio
   END IF

   l_NewScaleArea = SQRT ((l_TmpNewWidth * l_TmpNewHeight) / &
                          (i_WindowWidth * i_WindowHeight))

   IF i_ResizeWin THEN

      //------------------------------------------------------------
      //  Do any containers on the window.
      //------------------------------------------------------------

      IF i_NumContainers > 0 THEN
         SetReDraw(FALSE)
      END IF

      FOR l_Idx = 1 TO i_NumContainers
         l_TmpCont = i_Containers[l_Idx]
         IF NOT l_TmpCont.i_IsStatic THEN
            IF i_ZoomWin THEN
               l_TmpCont.Move                 &
                  (l_NewScaleArea * l_TmpCont.i_ContX, &
                   l_NewScaleArea * l_TmpCont.i_ContY)
               l_TmpCont.Resize                    &
                  (l_NewScaleArea * l_TmpCont.i_ContWidth,  &
                   l_NewScaleArea * l_TmpCont.i_ContHeight)            
            ELSE
               l_TmpCont.Move                 &
                  (l_NewScaleX * l_TmpCont.i_ContX, &
                   l_NewScaleY * l_TmpCont.i_ContY)
               l_TmpCont.Resize                    &
                  (l_NewScaleX * l_TmpCont.i_ContWidth,  &
                   l_NewScaleY * l_TmpCont.i_ContHeight)
            END IF
         END IF
         IF l_TmpCont.i_CT_ResizeCont THEN
            l_TmpCont.TriggerEvent("pc_Resize")
         END IF
      NEXT

      //------------------------------------------------------------
      //  Do all other controls.
      //------------------------------------------------------------

      f_PO_ResizeControls(THIS, i_PO_Controls[], i_ZoomWin)

   ELSE

      //---------------------------------------------------------
      //  The developer did not specify that the control objects
      //  were to be resized when the window got resized.
      //  Therefore, we need to loop through each DataWindow.
      //  If the DataWindow can be resized, then resize the
      //  DataWindow to be proportionally the same as when the
      //  window was at its original size.
      //---------------------------------------------------------

      IF i_FirstDWIsValid THEN
         FOR l_Idx = 1 TO i_FirstDW.i_NumWindowDWs
            l_TmpDW = i_FirstDW.i_WindowDWs[l_Idx]

            IF l_TmpDW.i_ResizeDW THEN
               l_Visible = l_TmpDW.Visible
               IF l_Visible THEN
                  l_TmpDW.Visible = FALSE
               END IF

               l_TmpDW.Move(l_NewScaleX * l_TmpDW.i_XPos, &
                            l_NewScaleY * l_TmpDW.i_YPos)
               l_TmpDW.Resize(l_NewScaleX * l_TmpDW.i_Width, &
                              l_NewScaleY * l_TmpDW.i_Height)

               IF l_Visible THEN
                  l_TmpDW.Visible = TRUE
               END IF
            END IF
         NEXT
      END IF
   END IF
END IF

//------------------------------------------------------------------
//  See if the window state changed.
//------------------------------------------------------------------

IF WindowState <> i_WindowState THEN

   //---------------------------------------------------------------
   //  Remember the new state.
   //---------------------------------------------------------------

   i_WindowState = WindowState

   //---------------------------------------------------------------
   //  See if the window state changed to a non-Maximized! state.
   //---------------------------------------------------------------

   IF WindowState <> Maximized! THEN

      //------------------------------------------------------------
      //  Make sure the window state was not passed by a parent
      //  DataWindow.
      //------------------------------------------------------------

      IF NOT l_TriggeredEvent THEN

         //---------------------------------------------------------
         //  See if the Control key is down.
         //---------------------------------------------------------

         IF l_Control THEN

            //------------------------------------------------------
            //  If the new window state is Minimized!, then pass it
            //  to the children DataWindows.
            //------------------------------------------------------

            IF WindowState = Minimized! THEN
               IF i_FirstDWIsValid THEN
                  FOR l_Idx = 1 TO i_FirstDW.i_NumWindowDWs
                     i_FirstDW.i_WindowDWs[l_Idx].Pass_Minimize()
                  NEXT
               END IF
            ELSE

               //---------------------------------------------------
               //  If the new window state is Normal!, then pass it
               //  to the children DataWindows.
               //---------------------------------------------------

               IF WindowState = Normal! THEN
                  IF i_FirstDWIsValid THEN
                     FOR l_Idx = 1 TO i_FirstDW.i_NumWindowDWs
                        i_FirstDW.i_WindowDWs[l_Idx].Pass_Normal()
                     NEXT
                  END IF
               END IF
            END IF
         END IF
      END IF
   END IF
END IF

end event

on activate;//******************************************************************
//  PC Module     : w_Main
//  Event         : Activate
//  Description   : Sets up for a window that gets focus.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

//------------------------------------------------------------------
//  If there are DataWindow(s) on this window, we need to tell
//  them that we just got activated so that one of them can make
//  itself the current DataWindow.
//------------------------------------------------------------------

IF i_FirstDWIsValid THEN

   //---------------------------------------------------------------
   //  PostEvent() problem...
   //---------------------------------------------------------------

   IF IsValid(PCCA.Window_CurrentDW) THEN
      IF PCCA.Window_CurrentDW.i_Window <> THIS THEN
         PCCA.Window_CurrentDW.TriggerEvent("pcd_Inactive")
      END IF
   END IF

   PCCA.PCMGR.Set_DW_Activate(i_FirstDW)
END IF

//------------------------------------------------------------------
//  Because there may be multiple windows open, indicate that this
//  window is the current window.
//------------------------------------------------------------------

PCCA.Window_Current = THIS

end on

on open;//******************************************************************
//  PC Module     : w_Main
//  Event         : Open
//  Description   : Opens the window and does window processing
//                  before the window is visible.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN     l_ConfigMenus, l_DevMoved
BOOLEAN     l_DevSized,    l_DoResize
INTEGER     l_FrameWidth,  l_FrameHeight
INTEGER     l_TitleHeight, l_ControlMenuWidth
INTEGER     l_LowTab
INTEGER     l_Idx,         l_Jdx
INTEGER     l_Width,       l_Height
INTEGER     l_XPos,        l_YPos
MENU        l_DWMenu,      l_Menu
UO_DW_MAIN  l_ToDW,        l_TmpDW

//------------------------------------------------------------------
//  Grab the time.
//------------------------------------------------------------------

f_PO_TimerMark("Menu/Toolbar/Control Creation")

//------------------------------------------------------------------
//  Initialize the class name identifier.
//------------------------------------------------------------------

i_ClassName = ClassName()

//------------------------------------------------------------------
//  Grab the Open Parm.
//------------------------------------------------------------------

i_OpenParm = PCCA.Open_Parm

//------------------------------------------------------------------
//  Remember if this window is a sheet.
//------------------------------------------------------------------

i_IsSheet = PCCA.Is_Sheet

//------------------------------------------------------------------
//  Initlialize PCCA.Error.
//------------------------------------------------------------------

PCCA.Error = c_Success

//------------------------------------------------------------------
//  We are now the current window.
//------------------------------------------------------------------

PCCA.Window_Current = THIS

//------------------------------------------------------------------
//  Set the INI file section to a reasonable default.
//------------------------------------------------------------------

i_INIFileSection = PCCA.Application_Name + "/" + i_ClassName

//------------------------------------------------------------------
//  Look through the list of all DataWindows for the DataWindows
//  which reside on this window.
//------------------------------------------------------------------

l_Idx = PCCA.PCMGR.i_DW_NumGlobalDW
DO WHILE l_Idx > 0

   l_TmpDW = PCCA.PCMGR.i_DW_GlobalDWList[l_Idx]

   //---------------------------------------------------------------
   //  Once we have found a DataWindow with a different window ID
   //  than the current window ID, we know we have found all of
   //  the DataWindows on this window.
   //---------------------------------------------------------------

   IF l_TmpDW.i_WindowID < PCCA.PCMGR.i_WindowID THEN
      EXIT
   END IF

   //---------------------------------------------------------------
   //  We found a PowerClass DataWindow on this window and/or
   //  container object.  If this it is on the window, then we
   //  need to set it up.
   //---------------------------------------------------------------

   IF l_TmpDW.i_WindowID = PCCA.PCMGR.i_WindowID THEN
   IF IsNull(l_TmpDW.i_Container) THEN

      //------------------------------------------------------------
      //  If this is the first DataWindow we found for this window,
      //  then we need to remember it.
      //------------------------------------------------------------

      IF NOT i_FirstDWIsValid THEN
         i_FirstDWIsValid = TRUE
         i_FirstDW        = l_TmpDW
      END IF
   END IF
   END IF

   //---------------------------------------------------------------
   //  Get ready to look at the next DataWindow.
   //---------------------------------------------------------------

   l_Idx = l_Idx - 1
LOOP

//------------------------------------------------------------------
//  Bump the window ID for next time.
//------------------------------------------------------------------

PCCA.PCMGR.i_WindowID = PCCA.PCMGR.i_WindowID + 1

//------------------------------------------------------------------
//  Let PCCA.PCMGR know that we are using him.
//------------------------------------------------------------------

PCCA.PCMGR.Inc_Usage(THIS)

//------------------------------------------------------------------
//  Set up PowerLock security.
//------------------------------------------------------------------

f_PL_Security(THIS)

//------------------------------------------------------------------
//  Initialize the window options.
//------------------------------------------------------------------

PCCA.DLL.fu_InitWMBits (THIS)

//------------------------------------------------------------------
//  Initialize the DataWindow options.
//------------------------------------------------------------------

PCCA.DLL.fu_WMInitDWBits (THIS)

//------------------------------------------------------------------
//  Initialize NULL objects for use by the developer.
//------------------------------------------------------------------

c_NullDW      = PCCA.Null_Object
c_NullCB      = PCCA.Null_Object
c_NullMenu    = PCCA.Null_Object
c_NullPicture = PCCA.Null_Object

//------------------------------------------------------------------
//  Get the window frame width and height.
//------------------------------------------------------------------

PCCA.EXT.fu_GetFrameSize(l_FrameWidth, l_FrameHeight)

//------------------------------------------------------------------
//  Get the title and menu heights.
//------------------------------------------------------------------

PCCA.EXT.fu_GetWindowInfo(l_TitleHeight, l_ControlMenuWidth)

//------------------------------------------------------------------
//  Remember the original size and state of the window.
//------------------------------------------------------------------

l_XPos         = X
l_YPos         = Y
i_WindowWidth  = WorkSpaceWidth()
i_WindowHeight = WorkSpaceHeight()
i_WindowState  = WindowState

f_PO_ResizeControls(THIS, i_PO_Controls[], FALSE)

//------------------------------------------------------------------
//  Even if WindowState says Maximized!, the window may not have
//  been maximized yet.
//------------------------------------------------------------------

IF i_WindowState = Maximized! THEN
   i_WindowState = Normal!
END IF

//------------------------------------------------------------------
//  Initialize i_DisplayError for the developer.
//------------------------------------------------------------------

i_DisplayError = TRUE

//------------------------------------------------------------------
//  The pc_SetVariables event is used if data sent to the window
//  needs to be extracted from global variables.  The data is
//  usually loaded into local or instance variables.  This event
//  is coded by the developer.
//------------------------------------------------------------------

TriggerEvent("pc_SetVariables")

IF PCCA.Error <> c_Success THEN
   IF i_DisplayError THEN
      PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::Open"
      PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
      PCCA.MB.i_MB_Strings[3] = i_ClassName
      PCCA.MB.i_MB_Strings[4] = Title
      PCCA.MB.i_MB_Strings[5] = "pc_SetVariables"
      PCCA.MB.fu_MessageBox(PCCA.MB.c_MBI_WindowOpenError, &
                         0, PCCA.MB.i_MB_Numbers[],     &
                         5, PCCA.MB.i_MB_Strings[])
   END IF
   GOTO Finished
END IF

//------------------------------------------------------------------
//  Grab the time.
//------------------------------------------------------------------

f_PO_TimerMark("pc_SetVariables")

//------------------------------------------------------------------
//  Initialize i_DisplayError for the developer.
//------------------------------------------------------------------

i_DisplayError = TRUE

//------------------------------------------------------------------
//  The pc_SetWindow event is used to do any window setup
//  processing just before the window is open.  This event
//  is coded by the developer.
//------------------------------------------------------------------

TriggerEvent("pc_SetWindow")

IF PCCA.Error <> c_Success THEN
   IF i_DisplayError THEN
      PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::Open"
      PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
      PCCA.MB.i_MB_Strings[3] = i_ClassName
      PCCA.MB.i_MB_Strings[4] = Title
      PCCA.MB.i_MB_Strings[5] = "pc_SetWindow"
      PCCA.MB.fu_MessageBox(PCCA.MB.c_MBI_WindowOpenError, &
                         0, PCCA.MB.i_MB_Numbers[],     &
                         5, PCCA.MB.i_MB_Strings[])
   END IF
   GOTO Finished
END IF

//------------------------------------------------------------------
//  Grab the time.
//------------------------------------------------------------------

f_PO_TimerMark("pc_SetWindow")

//------------------------------------------------------------------
//  If the developer did not run Set_W_Options(), run it for them
//  so that they get any default options they have defined.  Also,
//  Set_W_Options() restores redrawing of the MDI Frame.
//------------------------------------------------------------------

IF NOT i_OptionsInit THEN
   Set_W_Options(c_Default)
END IF

//------------------------------------------------------------------
//  Initialize i_DisplayError for the developer.
//------------------------------------------------------------------

i_DisplayError = TRUE

//------------------------------------------------------------------
//  The pc_SetDDLB event is used to load any code tables located
//  on the window.  If the developer specified that code tables
//  were to be loaded upfront and then trigger this event now to
//  do it.  This event is coded by the developer.
//------------------------------------------------------------------

IF i_LoadCodeTable THEN

   TriggerEvent("pc_SetDDLB")

   IF PCCA.Error <> c_Success THEN
      IF i_DisplayError THEN
         PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::Open"
         PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
         PCCA.MB.i_MB_Strings[3] = i_ClassName
         PCCA.MB.i_MB_Strings[4] = Title
         PCCA.MB.i_MB_Strings[5] = "pc_SetDDLB"
         PCCA.MB.fu_MessageBox(PCCA.MB.c_MBI_WindowOpenError, &
                            0, PCCA.MB.i_MB_Numbers[],     &
                            5, PCCA.MB.i_MB_Strings[])
      END IF
      GOTO Finished
   END IF

   //---------------------------------------------------------------
   //  Grab the time.
   //---------------------------------------------------------------

   f_PO_TimerMark("pc_SetDDLB")
END IF

//------------------------------------------------------------------
//  See if the developer moved the window.
//------------------------------------------------------------------

l_DevMoved = (l_XPos <> X OR l_YPos <> Y)
l_DevSized = (i_WindowWidth  <> WorkSpaceWidth() OR &
              i_WindowHeight <> WorkSpaceHeight())

//------------------------------------------------------------------
//  If the developer did not size the window, then we will
//  attempt to size and/or position the window.
//------------------------------------------------------------------

IF NOT l_DevSized THEN

   //---------------------------------------------------------------
   //  See if the developer told us to get the size and position
   //  information from the INI file.
   //---------------------------------------------------------------

   IF i_SavePosition THEN

      //------------------------------------------------------------
      //  If the INI information was not already gotten by the
      //  developer, get it.
      //------------------------------------------------------------

      IF NOT i_INIGetDone THEN
         Get_INI_Size()
      END IF

      //------------------------------------------------------------
      //  Make sure that the developer did not already position
      //  the window.
      //------------------------------------------------------------

      IF NOT l_DevMoved THEN

         Move(i_INIXPos, i_INIYPos)
         Resize(i_INIWidth, i_INIHeight)
         l_DevMoved = TRUE
         l_DevSized = TRUE

         //---------------------------------------------------------
         //  It would be nice to take care of the WindowState
         //  here, but PowerBuilder won't let us.  It will be
         //  taken care of in pc_SetSize, which is a posted
         //  event.
         //---------------------------------------------------------

      END IF
   ELSE

      //------------------------------------------------------------
      //  If the developer wanted the auto-size feature, we take
      //  care of it here.
      //------------------------------------------------------------

      IF i_AutoSize OR i_AutoSize30 THEN
         l_DoResize = FALSE

         IF MenuName <> "" THEN
            IF ParentWindow() = PCCA.MDI_Frame THEN
               IF i_AutoSize30 THEN
                  l_DoResize = TRUE
                  l_Width    = Width
                  l_Height   = Height
               END IF
            ELSE
               l_DoResize = TRUE
               l_Width    = Width + PixelsToUnits(1, XPixelsToUnits!)
               l_Height   = Height + PixelsToUnits(1, YPixelsToUnits!)
            END IF
         END IF

         IF l_DoResize THEN
            Resize(l_Width, l_Height)
            l_DevSized = TRUE
         END IF
      END IF
   END IF
END IF

//------------------------------------------------------------------
//  Determine if we should cascade the window.  It should be
//  cascaded if:
//     a) It is is not a response window AND
//     b) An MDI frame exists AND
//     c) The window is being opened using Window_Open().
//------------------------------------------------------------------

IF PCCA.MDI_Frame_Valid THEN
IF PCCA.Num_Windows > 0 THEN
   IF IsNull(PCCA.Window_List[PCCA.Num_Windows]) OR &
      PCCA.Window_List[PCCA.Num_Windows] = THIS THEN

      //------------------------------------------------------------
      //  Only cascade the window if the developer did not do
      //  it.
      //------------------------------------------------------------

      IF NOT l_DevMoved THEN
         IF PCCA.Num_Windows = 1 THEN
            l_XPos = 1
            l_YPos = 1
         ELSE

            //------------------------------------------------------
            //  Find the last cascaded position.
            //------------------------------------------------------

            l_Jdx = 0
            FOR l_Idx = PCCA.Num_Windows - 1 TO 1 STEP -1
               IF PCCA.Window_X[l_Idx] > 0 THEN
                  l_Jdx = l_Idx
                  EXIT
               END IF
            NEXT

            //------------------------------------------------------
            //  Use the postion of the last cascaded window to
            //  position this window.
            //------------------------------------------------------

            IF l_Jdx > 0 THEN
               l_XPos = PCCA.Window_X[l_Jdx] + l_ControlMenuWidth
               l_YPos = PCCA.Window_Y[l_Jdx] + l_TitleHeight
               IF l_XPos + Width  > PCCA.MDI_Client.Width OR &
                  l_YPos + Height > PCCA.MDI_Client.Height THEN
                  l_XPos = 1
                  l_YPos = 1
               END IF
            ELSE
               l_XPos = 1
               l_YPos = 1
            END IF
         END IF

         //---------------------------------------------------------
         //  Move to the cascaded position.
         //---------------------------------------------------------

         PCCA.Window_X[PCCA.Num_Windows] = l_XPos
         PCCA.Window_Y[PCCA.Num_Windows] = l_YPos
         Move(l_XPos, l_YPos)
      ELSE
         PCCA.Window_X[PCCA.Num_Windows] = 0
         PCCA.Window_Y[PCCA.Num_Windows] = 0
      END IF
   END IF
END IF
END IF

//------------------------------------------------------------------
//  If the developer did not specify the menus, then we need to
//  set them up.
//------------------------------------------------------------------

IF MenuName = "" THEN
   IF PCCA.MDI_Frame_Valid THEN
      l_Menu = PCCA.MDI_Frame.MenuID
   END IF
ELSE
   l_Menu = MenuID
END IF
      
IF NOT IsNull(l_Menu) THEN
IF NOT i_FileMenuIsValid OR &
   NOT i_EditMenuIsValid OR &
   NOT i_PopupMenuIsValid THEN
   l_Jdx = UpperBound(l_Menu.Item[])
   FOR l_Idx = 1 TO l_Jdx
      CHOOSE CASE l_Menu.Item[l_Idx].Text
         CASE PCCA.MDI.c_MDI_MenuLabelFile
            IF NOT i_FileMenuIsValid THEN
               i_FileMenu        = l_Menu.Item[l_Idx]
               i_FileMenuIsValid = TRUE
            END IF

         CASE PCCA.MDI.c_MDI_MenuLabelEdit
            IF NOT i_EditMenuIsValid THEN
               i_EditMenu        = l_Menu.Item[l_Idx]
               i_EditMenuIsValid = TRUE
            END IF
            IF NOT i_PopupMenuIsValid THEN
               IF i_EditMenu.Visible THEN
                  i_PopupMenu        = l_Menu.Item[l_Idx]
                  i_PopupMenuIsValid = TRUE
               END IF
            END IF
      END CHOOSE
   NEXT
END IF
END IF

//------------------------------------------------------------------
//  We need to create the POPUP menu if:
//     a) The developer specified that the POPUP menu is to be
//        enabled AND
//     b) the developer did not already specify it using
//        Set_W_Popup() AND
//     c) 1) There is not a menu associated with this window OR
//        2) The EDIT menu is not visible.

//------------------------------------------------------------------

IF i_EnablePopup THEN
   IF IsValid(i_PopupMenu) THEN
   ELSE
      Set_W_Popup(c_NullMenu)
   END IF
END IF

//------------------------------------------------------------------
//  If there are DataWindows on this window, we need to do some
//  initialization for them.
//------------------------------------------------------------------

l_ConfigMenus = FALSE

IF i_FirstDWIsValid THEN

   //---------------------------------------------------------------
   //  If the default menus for DataWindows have not been set, set
   //  them.  Default menus are used for DataWindows which may be
   //  initialized after the window is open.
   //---------------------------------------------------------------

   IF NOT i_FirstDW.i_DefaultMenusDefined THEN
      IF i_EnablePopup THEN
         l_DWMenu = i_PopupMenu
      ELSE
         l_DWMenu = c_NullMenu
      END IF
      i_FirstDW.Set_DW_Default_Menus(i_FileMenu, i_EditMenu, l_DWMenu)
   END IF

   //---------------------------------------------------------------
   //  We will make the lowest tab order DataWindow the current
   //  DataWindow.
   //---------------------------------------------------------------

   l_ToDW   = PCCA.Null_Object
   l_LowTab = 0

   FOR l_Idx = 1 TO i_FirstDW.i_NumWindowDWs
      l_TmpDW = i_FirstDW.i_WindowDWs[l_Idx]

      //------------------------------------------------------------
      //  Copy the menu information to the DataWindow.
      //------------------------------------------------------------

      IF NOT l_TmpDW.i_FileMenuIsValid THEN
         l_TmpDW.i_FileMenu        = i_FileMenu
         l_TmpDW.i_FileMenuIsValid = i_FileMenuIsValid
      END IF

      IF NOT l_TmpDW.i_EditMenuIsValid THEN
         l_TmpDW.i_EditMenu        = i_EditMenu
         l_TmpDW.i_EditMenuIsValid = i_EditMenuIsValid
      END IF

      IF NOT l_TmpDW.i_PopupMenuIsValid THEN
         IF i_EnablePopup THEN
            l_TmpDW.i_PopupMenu        = i_PopupMenu
            l_TmpDW.i_PopupMenuIsValid = i_PopupMenuIsValid
         END IF
      END IF

      //------------------------------------------------------------
      //  See if this DataWindow has the capability to enable
      //  menu options.
      //------------------------------------------------------------

      IF l_TmpDW.i_AutoConfigMenus THEN
         l_ConfigMenus = TRUE
      END IF

      //------------------------------------------------------------
      //  If this DataWindow is in use, then look to see if it has
      //  the lowest tab order.  If so, it will be the DataWindow
      //  that will become the first current DataWindow for this
      //  window.
      //------------------------------------------------------------

      IF l_TmpDW.i_InUse THEN
      IF l_TmpDW.TabOrder > 0 THEN
      IF (l_TmpDW.TabOrder < l_LowTab OR l_LowTab = 0) THEN
         l_ToDW   = l_TmpDW
         l_LowTab = l_TmpDW.TabOrder
      END IF
      END IF
      END IF
   NEXT

   //---------------------------------------------------------------
   //  If we have a valid DataWindow to be the first current
   //  DataWindow for this window, mark it as such.
   //---------------------------------------------------------------

   IF IsValid(l_ToDW) THEN
      l_ToDW.i_Current = TRUE
   END IF
END IF

//------------------------------------------------------------------
//  See if the DataWindows are to configure the menus.
//------------------------------------------------------------------

IF l_ConfigMenus THEN
   IF PCCA.MDI_Frame_Valid  THEN
   IF PCCA.MDI_Allow_Redraw THEN
      PCCA.MDI_Frame.SetRedraw(FALSE)
   END IF
   END IF

   FOR l_Idx = 1 TO i_FirstDW.i_NumWindowDWs
      l_TmpDW = i_FirstDW.i_WindowDWs[l_Idx]

      //------------------------------------------------------------
      //  Let the DatFaWindow enable menu options if it has
      //  the capability.
      //------------------------------------------------------------

      IF l_TmpDW.i_AutoConfigMenus THEN
         l_TmpDW.Config_Menus()
      END IF
   NEXT

   IF PCCA.MDI_Frame_Valid  THEN
   IF PCCA.MDI_Allow_Redraw THEN
      PCCA.MDI_Frame.SetRedraw(TRUE)
   END IF
   END IF
END IF

//------------------------------------------------------------------
//  Set the inactive colors, if any, for the DataWindows that are
//  not current.  Doing the action here prevents the flashing
//  effect.
//------------------------------------------------------------------

IF i_FirstDWIsValid THEN
   FOR l_Idx = 1 TO i_FirstDW.i_NumWindowDWs
      l_TmpDW = i_FirstDW.i_WindowDWs[l_Idx]
      IF NOT l_TmpDW.i_Current THEN
         l_TmpDW.is_EventControl.Highlight_Only = TRUE
         l_TmpDW.TriggerEvent("pcd_Inactive")
      END IF
   NEXT
END IF

//------------------------------------------------------------------
//  Post the pc_SetSize event, which may set the window state.
//------------------------------------------------------------------

PostEvent("pc_SetSize")

//------------------------------------------------------------------
//  If the timer is on, grab the time.
//------------------------------------------------------------------

f_PO_TimerMark("Exit Open Event")

Finished:

//------------------------------------------------------------------
//  If there was an error during initialization of this window,
//  then clean up.
//------------------------------------------------------------------

IF PCCA.Error <> c_Success THEN
   IF i_FirstDWIsValid THEN
      FOR l_Idx = 1 TO i_FirstDW.i_NumWindowDWs
         l_TmpDW = i_FirstDW.i_WindowDWs[l_Idx]
         l_TmpDW.is_EventControl.No_Close = TRUE
         l_TmpDW.TriggerEvent("pcd_Close")
      NEXT
   END IF

   i_FirstDWIsValid = FALSE

   IF PCCA.MDI_Frame_Valid  THEN
   IF PCCA.MDI_Allow_Redraw THEN
      PCCA.MDI_Frame.SetRedraw(TRUE)
   END IF
   END IF
END IF

//------------------------------------------------------------------
//  Pop the "Opening Window" prompt.
//------------------------------------------------------------------

PCCA.MDI.fu_Pop()

end on

on close;//******************************************************************
//  PC Module     : w_Main
//  Event         : Close
//  Description   : Closes the window.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

INTEGER            l_Idx
UO_CONTAINER_MAIN  l_TmpCont

//------------------------------------------------------------------
//  Save our position to the INI file, if requested by the
//  developer.
//------------------------------------------------------------------

IF i_SavePosition THEN
   IF NOT i_INIPutDone THEN
      Put_INI_Size()
   END IF
END IF

//------------------------------------------------------------------
//  Destroy the POPUP menu if we created it.
//------------------------------------------------------------------

IF IsValid(i_mPopup) THEN
   DESTROY i_mPopup
END IF

//------------------------------------------------------------------
//  If this is not a response window and a MDI frame exists,
//  then call Window_Close() to take ourselves out of the global
//  list of open windows.
//------------------------------------------------------------------

IF PCCA.MDI_Frame_Valid    THEN
   Window_Close(THIS)
END IF

//------------------------------------------------------------------
//  Just to be safe, make sure the containers go away.
//------------------------------------------------------------------

FOR l_Idx = i_NumContainers TO 1 STEP -1
   l_TmpCont = i_Containers[l_Idx]
   IF NOT l_TmpCont.i_IsStatic THEN
      l_TmpCont.Save_On_Close(c_SOCNoSave)
      l_TmpCont.Container_Close()
   END IF
NEXT

//------------------------------------------------------------------
//  Indicate that there is not a current window.  If another window
//  gets activated, it will set itself to be the current window.
//------------------------------------------------------------------

PCCA.Window_Current = PCCA.Null_Object

//------------------------------------------------------------------
//  Let PCCA.PCMGR know that we are done with him.
//------------------------------------------------------------------

IF IsValid(PCCA.PCMGR) THEN
   PCCA.PCMGR.Dec_Usage(THIS)
END IF

end on

on rbuttondown;//******************************************************************
//  PC Module     : w_Main
//  Event         : RButtonDown
//  Description   : Display POPUP menu for operating on the
//                  current DataWindow.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN  l_PopupIsValid
MENU     l_Popup

//------------------------------------------------------------------
//  Make sure that this window is active before we process
//  the right mouse button event.
//------------------------------------------------------------------

IF PCCA.Window_Current <> THIS THEN
   SetFocus()
END IF

//------------------------------------------------------------------
//  If the POPUP menu was enabled, put it up.
//------------------------------------------------------------------

IF i_EnablePopup THEN

   //---------------------------------------------------------------
   //  Make sure the POPUP menus are loaded correctly.
   //---------------------------------------------------------------

   TriggerEvent("pc_WM_InitMenuPopup")

   l_PopupIsValid = FALSE

   IF i_PopupMenuIsValid THEN
      l_PopupIsValid = i_PopupMenuIsValid
      l_Popup        = i_PopupMenu
   END IF

   IF NOT l_PopupIsValid THEN
   IF IsValid(PCCA.Window_CurrentDW) THEN
      l_PopupIsValid = PCCA.Window_CurrentDW.i_PopupMenuIsValid
      l_Popup        = PCCA.Window_CurrentDW.i_PopupMenu
   END IF
   END IF

   IF l_PopupIsValid THEN
      IF PCCA.MDI_Frame_Valid AND WindowType <> Response! THEN
         l_Popup.PopMenu(PCCA.MDI_Frame.PointerX(), &
                         PCCA.MDI_Frame.PointerY())
      ELSE
         l_Popup.PopMenu(PointerX(), PointerY())
      END IF
   END IF
END IF

end on

on closequery;//******************************************************************
//  PC Module     : w_Main
//  Event         : CloseQuery
//  Description   : Verifies that it is Okay to close the window.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN       l_TriggeredEvent, l_HaveChanges
INTEGER       l_NumChangedDWs,  l_Answer,       l_Idx
UNSIGNEDLONG  l_SaveOnClose
UNSIGNEDLONG  l_RefreshCmd,     l_RefCmd[]
UO_DW_MAIN    l_RootDW,         l_ChangedDWs[], l_TmpDW

//------------------------------------------------------------------
//  Remember if we were triggered internally.  If we are, don't
//  ask any questions, just try to close.  If we are an internally
//  triggered event, we were most likely triggered by pcd_Close.
//------------------------------------------------------------------

l_TriggeredEvent = PCCA.Do_Event
PCCA.Do_Event    = FALSE
PCCA.Error       = c_Success

IF NOT l_TriggeredEvent THEN
IF i_FirstDWIsValid     THEN

   //---------------------------------------------------------------
   //  If we get here, it was because the close was triggered
   //  externally.  Check to see if all of the DataWindows on
   //  this window are ready to close.
   //---------------------------------------------------------------

   PCCA.MDI.fu_PushID(PCCA.MDI.c_MDI_Close, c_ShowMark)

   //---------------------------------------------------------------
   //  Make sure that all of the DataWindows are saved.
   //---------------------------------------------------------------

   FOR l_Idx = 1 TO i_FirstDW.i_NumWindowDWs
      l_RefCmd[l_Idx] = c_RefreshUndefined
   NEXT

   l_SaveOnClose = i_SaveOnClose

   //---------------------------------------------------------------
   //  If we are to prompt the user once, figure out if any
   //  DataWindows have changed and prompt them once.
   //---------------------------------------------------------------

   IF l_SaveOnClose = c_SOCPromptUserOnce THEN

      l_HaveChanges = FALSE
      FOR l_Idx = 1 TO i_FirstDW.i_NumWindowDWs

         l_TmpDW = i_FirstDW.i_WindowDWs[l_Idx]

         IF l_TmpDW.i_InUse THEN
            IF l_TmpDW.Check_DW_Modified(c_CheckChildren) THEN
               l_HaveChanges = TRUE
               l_RefCmd[l_Idx] = l_RefreshCmd
               l_TmpDW.Build_Changed_List(l_NumChangedDWs, &
                                          l_ChangedDWs[])
            END IF
         END IF
      NEXT

      IF l_HaveChanges THEN
         l_Answer = i_FirstDW.Ask_User(l_NumChangedDWs, &
                                       l_ChangedDWs[])
         CHOOSE CASE l_Answer
            CASE 2
              l_SaveOnClose = c_SOCNoSave

            CASE 3

               //---------------------------------------------
               //  The user canceled.
               //---------------------------------------------

               PCCA.MDI.fu_Pop()
               PCCA.Error = c_Fatal
               GOTO Finished

            CASE ELSE
              l_SaveOnClose = c_SOCSave
         END CHOOSE
      END IF
   END IF

   CHOOSE CASE l_SaveOnClose

      //------------------------------------------------------------
      //  Cycle through each of the DataWindows on this window and
      //  reset the modification flags.
      //------------------------------------------------------------

      CASE c_SOCNoSave

         FOR l_Idx = 1 TO i_FirstDW.i_NumWindowDWs
            l_TmpDW = i_FirstDW.i_WindowDWs[l_Idx]
            IF l_TmpDW.i_InUse THEN
               IF l_TmpDW.Check_DW_Modified(c_CheckChildren) THEN

                  //------------------------------------------------
                  //  Clear the modification flags.
                  //------------------------------------------------

                  l_TmpDW.Reset_DW_Modified(c_ResetChildren)

                  //------------------------------------------------
                  //  If the close aborts, we'll need to refresh
                  //  the DataWindows since the modification flags
                  //  have been cleard.
                  //------------------------------------------------

                  l_RefCmd[l_Idx]            = c_RefreshSame
                  l_TmpDW.i_RetrieveMySelf   = TRUE
                  l_TmpDW.i_RetrieveChildren = TRUE
               END IF
            END IF
         NEXT

      //------------------------------------------------------------
      //  Cycle through each of the DataWindows on this window and
      //  save any DataWindows which have been modified.
      //------------------------------------------------------------

      CASE c_SOCSave

         FOR l_Idx = 1 TO i_FirstDW.i_NumWindowDWs
            l_TmpDW = i_FirstDW.i_WindowDWs[l_Idx]
            IF l_TmpDW.i_InUse THEN
               IF l_TmpDW.Check_DW_Modified(c_CheckChildren) THEN

                  //------------------------------------------------
                  //  Save the DataWindow.
                  //------------------------------------------------

                  l_RefCmd[l_Idx] = c_RefreshSave

                  l_RootDW = l_TmpDW.i_RootDW
                  l_RootDW.is_EventControl.No_Refresh_After_Save = &
                     TRUE
                  l_RootDW.TriggerEvent("pcd_Save")

                  //------------------------------------------------
                  //  If PCCA.Error does not indicate success, then
                  //  pcd_Save failed (e.g. validation error).
                  //------------------------------------------------

                  IF PCCA.Error <> c_Success THEN
                     l_RefCmd[l_Idx] = c_RefreshUndefined

                     //---------------------------------------------
                     //  Pop the close prompt off and exit the
                     //  event.
                     //---------------------------------------------

                     PCCA.MDI.fu_Pop()
                     PCCA.Error = c_Fatal
                     GOTO Finished
                  END IF

                  //------------------------------------------------
                  //  If a save happened, the DataWindow hierarchy
                  //  may need refreshing.
                  //------------------------------------------------

                  IF l_RootDW         <> l_TmpDW            THEN
                  IF l_RefreshCmd     <> c_RefreshUndefined THEN
                     l_RootDW.is_EventControl.Skip_DW_Valid = TRUE
                     l_RootDW.is_EventControl.Skip_DW       = l_TmpDW
                     l_RootDW.is_EventControl.Refresh_Cmd   = l_RefreshCmd
                     l_RootDW.TriggerEvent("pcd_Refresh")

                     //---------------------------------------------
                     //  If an error occurred during the refresh,
                     //  don't allow the close to happen.
                     //---------------------------------------------

                     IF PCCA.Error <> c_Success THEN
                        PCCA.MDI.fu_Pop()
                        PCCA.Error = c_Fatal
                        GOTO Finished
                     END IF
                  END IF
                  END IF
               END IF
            END IF
         NEXT

      //------------------------------------------------------------
      //  Cycle through each of the DataWindows on this window and
      //  prompt the user for any that have been modified.
      //------------------------------------------------------------

      CASE c_SOCPromptUser

         FOR l_Idx = 1 TO i_FirstDW.i_NumWindowDWs
            l_TmpDW = i_FirstDW.i_WindowDWs[l_Idx]
            IF l_TmpDW.i_InUse THEN
               l_TmpDW.Check_Save(l_RefreshCmd)
               l_RefCmd[l_Idx] = l_RefreshCmd

               //---------------------------------------------------
               //  If PCCA.Error does not indicate success, then
               //  the user hit cancel or pcd_Save failed (e.g.
               //  validation error).
               //---------------------------------------------------

               IF PCCA.Error <> c_Success THEN

                  //------------------------------------------------
                  //  Pop the close prompt off and exit the event.
                  //------------------------------------------------

                  PCCA.MDI.fu_Pop()
                  PCCA.Error = c_Fatal
                  GOTO Finished
               END IF

               //---------------------------------------------------
               //  If a save, the DataWindow hierarchy may need
               //  refreshing.
               //---------------------------------------------------

               l_RootDW = l_TmpDW.i_RootDW

               IF l_RootDW         <> l_TmpDW            THEN
               IF l_RefreshCmd     <> c_RefreshUndefined THEN
                  l_RootDW.is_EventControl.Skip_DW_Valid = TRUE
                  l_RootDW.is_EventControl.Skip_DW       = l_TmpDW
                  l_RootDW.is_EventControl.Refresh_Cmd   = l_RefreshCmd
                  l_RootDW.TriggerEvent("pcd_Refresh")

                  //------------------------------------------------------
                  //  If an error occurred during the refresh, don't
                  //  allow the close to happen.
                  //------------------------------------------------------

                  IF PCCA.Error <> c_Success THEN
                     PCCA.MDI.fu_Pop()
                     PCCA.Error = c_Fatal
                     GOTO Finished
                  END IF
               END IF
               END IF
            END IF
         NEXT

      //------------------------------------------------------------
      //  This case was already taken care of.  If we get to here
      //  there are no changes to be saved.
      //------------------------------------------------------------

      CASE c_SOCPromptUserOnce
   END CHOOSE

   //---------------------------------------------------------------
   //  Now we can close them.
   //---------------------------------------------------------------

   FOR l_Idx = 1 TO i_FirstDW.i_NumWindowDWs

      //------------------------------------------------------------
      //  Set the No_Close flag to tell the DataWindow that it is
      //  being triggered by CloseQuery.
      //------------------------------------------------------------

      l_TmpDW = i_FirstDW.i_WindowDWs[l_Idx]
      IF l_TmpDW.i_InUse THEN
         l_TmpDW.is_EventControl.No_Close = TRUE
         l_TmpDW.TriggerEvent("pcd_Close")

         //---------------------------------------------------------
         //  If there was an error, abort the close process.
         //---------------------------------------------------------

         IF PCCA.Error <> c_Success THEN
            PCCA.MDI.fu_Pop()
            PCCA.Error = c_Fatal
            GOTO Finished
         END IF
      END IF
   NEXT

   i_FirstDWIsValid = FALSE

   PCCA.MDI.fu_Pop()
END IF
END IF

Finished:

//------------------------------------------------------------------
//  If we are not closing the DataWindow, we need to make sure
//  that any DataWindows get refreshed correctly.
//------------------------------------------------------------------

IF PCCA.Error <> c_Success THEN
IF i_FirstDWIsValid        THEN
   FOR l_Idx = 1 TO i_FirstDW.i_NumWindowDWs
      IF l_RefCmd[l_Idx] <> c_RefreshUndefined THEN
         l_TmpDW = i_FirstDW.i_WindowDWs[l_Idx]
         l_TmpDW.is_EventControl.Refresh_Cmd = l_RefCmd[l_Idx]
         l_TmpDW.TriggerEvent("pcd_Refresh")
      END IF
   NEXT
   PCCA.Error = c_Fatal
END IF
END IF

IF PCCA.Error <> c_Success THEN
   Message.ReturnValue = 1
   RETURN
END IF
end on

on deactivate;//******************************************************************
//  PC Module     : w_Main
//  Event         : Deactivate
//  Description   : Triggered when the window loses focus.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

//------------------------------------------------------------------
//  This window is being deactivated.  Tell the PCCA.PCMGR so
//  that it will defuse the pcd_Activate event.
//------------------------------------------------------------------

IF IsValid(PCCA.PCMGR) THEN
   PCCA.PCMGR.Set_DW_Deactivate()
END IF

//------------------------------------------------------------------
//  If we are supposed to minimize when the window gets
//  deactivated, then do it.
//------------------------------------------------------------------

IF i_AutoMinimize AND NOT PCCA.MB.fu_InMB() THEN
   WindowState = Minimized!
END IF

end on

on w_main.create
end on

on w_main.destroy
end on

