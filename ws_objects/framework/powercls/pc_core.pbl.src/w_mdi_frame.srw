﻿$PBExportHeader$w_mdi_frame.srw
$PBExportComments$Ancestor MDI window to inherit for MDI frame
forward
global type w_mdi_frame from window
end type
type mdi_1 from mdiclient within w_mdi_frame
end type
end forward

global type w_mdi_frame from window
integer width = 2917
integer height = 2004
boolean titlebar = true
string title = "MDI WINDOW"
string menuname = "m_cs_xx"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
windowtype windowtype = mdihelp!
windowstate windowstate = maximized!
long backcolor = 285212671
event pc_closeall pbm_custom72
event pc_mdiactive pbm_custom73
event pc_setsize pbm_custom74
event pc_setwindow pbm_custom75
event pc_wm_initmenu pbm_initmenu
event pc_wm_move pbm_move
mdi_1 mdi_1
end type
global w_mdi_frame w_mdi_frame

type variables
//----------------------------------------------------------------------------
//  Instance Variables in sorted order.
//----------------------------------------------------------------------------

STRING		i_ClassName

MENU		i_EditMenu
MENU		i_EditMenuDelete
UNSIGNEDLONG	i_EditMenuDeleteNum
MENU		i_EditMenuFilter
UNSIGNEDLONG	i_EditMenuFilterNum
MENU		i_EditMenuFirst
UNSIGNEDLONG	i_EditMenuFirstNum
BOOLEAN	i_EditMenuInit
MENU		i_EditMenuInsert
UNSIGNEDLONG	i_EditMenuInsertNum
BOOLEAN	i_EditMenuIsPopup
BOOLEAN	i_EditMenuIsValid
MENU		i_EditMenuLast
UNSIGNEDLONG	i_EditMenuLastNum
MENU		i_EditMenuModeSep
UNSIGNEDLONG	i_EditMenuModeSepNum
MENU		i_EditMenuModify
UNSIGNEDLONG	i_EditMenuModifyNum
MENU		i_EditMenuNew
UNSIGNEDLONG	i_EditMenuNewNum
MENU		i_EditMenuNext
UNSIGNEDLONG	i_EditMenuNextNum
MENU		i_EditMenuPrev
UNSIGNEDLONG	i_EditMenuPrevNum
MENU		i_EditMenuPrint
UNSIGNEDLONG	i_EditMenuPrintNum
MENU		i_EditMenuPrintSep
UNSIGNEDLONG	i_EditMenuPrintSepNum
MENU		i_EditMenuQuery
UNSIGNEDLONG	i_EditMenuQueryNum
MENU		i_EditMenuSave
UNSIGNEDLONG	i_EditMenuSaveNum
MENU		i_EditMenuSaveRowsAs
UNSIGNEDLONG	i_EditMenuSaveRowsAsNum
MENU		i_EditMenuSaveSep
UNSIGNEDLONG	i_EditMenuSaveSepNum
MENU		i_EditMenuSearch
UNSIGNEDLONG	i_EditMenuSearchNum
MENU		i_EditMenuSearchSep
UNSIGNEDLONG	i_EditMenuSearchSepNum
MENU		i_EditMenuView
UNSIGNEDLONG	i_EditMenuViewNum

MENU		i_FileMenu
MENU		i_FileMenuAccept
UNSIGNEDLONG	i_FileMenuAcceptNum
MENU		i_FileMenuCancel
UNSIGNEDLONG	i_FileMenuCancelNum
MENU		i_FileMenuClose
UNSIGNEDLONG	i_FileMenuCloseNum
MENU		i_FileMenuExit
UNSIGNEDLONG	i_FileMenuExitNum
MENU		i_FileMenuExitSep
UNSIGNEDLONG	i_FileMenuExitSepNum
MENU		i_FileMenuFileSep
UNSIGNEDLONG	i_FileMenuFileSepNum
BOOLEAN	i_FileMenuInit
BOOLEAN	i_FileMenuIsValid
MENU		i_FileMenuNew
UNSIGNEDLONG	i_FileMenuNewNum
MENU		i_FileMenuOpen
UNSIGNEDLONG	i_FileMenuOpenNum
MENU		i_FileMenuPrint
UNSIGNEDLONG	i_FileMenuPrintNum
MENU		i_FileMenuPrintSep
UNSIGNEDLONG	i_FileMenuPrintSepNum
MENU		i_FileMenuPrintSetup
UNSIGNEDLONG	i_FileMenuPrintSetupNum
MENU		i_FileMenuSave
UNSIGNEDLONG	i_FileMenuSaveNum
MENU		i_FileMenuSaveRowsAs
UNSIGNEDLONG	i_FileMenuSaveRowsAsNum
MENU		i_FileMenuSaveSep
UNSIGNEDLONG	i_FileMenuSaveSepNum

STRING		i_INIFileSection
BOOLEAN	i_INIGetDone
INTEGER		i_INIHeight
INTEGER		i_INILastHeight
INTEGER		i_INILastWidth
WINDOWSTATE	i_INILastWindowState
INTEGER		i_INILastXPos
INTEGER		i_INILastYPos
BOOLEAN	i_INIPutDone
BOOLEAN	i_INISaveIsValid
INTEGER		i_INISaveLastXPos
INTEGER		i_INISaveLastYPos
BOOLEAN	i_INIValidSize
BOOLEAN	i_INIValidWS
INTEGER		i_INIWidth
WINDOWSTATE	i_INIWindowState
INTEGER		i_INIXPos
INTEGER		i_INIYPos

BOOLEAN	i_MDI_AllowRedraw
BOOLEAN	i_MDI_AutoConfigMenus
BOOLEAN	i_MDI_SavePosition
BOOLEAN	i_MDI_ShowClock
BOOLEAN	i_MDI_ShowResources
UNSIGNEDLONG	i_MDI_ToolBarPosition
BOOLEAN	i_MDI_ToolBarText
W_MICROHELP	i_MHWindow
MENU		i_mPopup

STRING		i_ObjectType  		= "w_MDI_Frame"
BOOLEAN	i_OptionsInit

MENU		i_PopupMenu
MENU		i_PopupMenuDelete
UNSIGNEDLONG	i_PopupMenuDeleteNum
MENU		i_PopupMenuFilter
UNSIGNEDLONG	i_PopupMenuFilterNum
MENU		i_PopupMenuFirst
UNSIGNEDLONG	i_PopupMenuFirstNum
BOOLEAN	i_PopupMenuInit
MENU		i_PopupMenuInsert
UNSIGNEDLONG	i_PopupMenuInsertNum
BOOLEAN	i_PopupMenuIsEdit
BOOLEAN	i_PopupMenuIsValid
MENU		i_PopupMenuLast
UNSIGNEDLONG	i_PopupMenuLastNum
MENU		i_PopupMenuModeSep
UNSIGNEDLONG	i_PopupMenuModeSepNum
MENU		i_PopupMenuModify
UNSIGNEDLONG	i_PopupMenuModifyNum
MENU		i_PopupMenuNew
UNSIGNEDLONG	i_PopupMenuNewNum
MENU		i_PopupMenuNext
UNSIGNEDLONG	i_PopupMenuNextNum
MENU		i_PopupMenuPrev
UNSIGNEDLONG	i_PopupMenuPrevNum
MENU		i_PopupMenuPrint
UNSIGNEDLONG	i_PopupMenuPrintNum
MENU		i_PopupMenuPrintSep
UNSIGNEDLONG	i_PopupMenuPrintSepNum
MENU		i_PopupMenuQuery
UNSIGNEDLONG	i_PopupMenuQueryNum
MENU		i_PopupMenuSave
UNSIGNEDLONG	i_PopupMenuSaveNum
MENU		i_PopupMenuSaveRowsAs
UNSIGNEDLONG	i_PopupMenuSaveRowsAsNum
MENU		i_PopupMenuSaveSep
UNSIGNEDLONG	i_PopupMenuSaveSepNum
MENU		i_PopupMenuSearch
UNSIGNEDLONG	i_PopupMenuSearchNum
MENU		i_PopupMenuSearchSep
UNSIGNEDLONG	i_PopupMenuSearchSepNum
MENU		i_PopupMenuView
UNSIGNEDLONG	i_PopupMenuViewNum

INTEGER		i_UpdateTime  		= 60

//----------------------------------------------------------------------------
// Constant Variables in logical order.
//----------------------------------------------------------------------------

UNSIGNEDLONG	c_MDI_AllowRedraw
UNSIGNEDLONG	c_MDI_NoAllowRedraw

UNSIGNEDLONG	c_MDI_NoShowResources
UNSIGNEDLONG	c_MDI_ShowResources

UNSIGNEDLONG	c_MDI_NoShowClock
UNSIGNEDLONG	c_MDI_ShowClock

UNSIGNEDLONG	c_MDI_NoAutoConfigMenus
UNSIGNEDLONG	c_MDI_AutoConfigMenus

UNSIGNEDLONG	c_MDI_NoSavePosition
UNSIGNEDLONG	c_MDI_SavePosition

UNSIGNEDLONG	c_MDI_ToolBarNone
UNSIGNEDLONG	c_MDI_ToolBarTop
UNSIGNEDLONG	c_MDI_ToolBarBottom
UNSIGNEDLONG	c_MDI_ToolBarLeft
UNSIGNEDLONG	c_MDI_ToolBarRight
UNSIGNEDLONG	c_MDI_ToolBarFloat

UNSIGNEDLONG	c_MDI_ToolBarHideText
UNSIGNEDLONG	c_MDI_ToolBarShowText

INTEGER		c_Fatal 			= -1
INTEGER		c_Success 		= 0
UNSIGNEDLONG	c_Default 		= 0

UO_DW_MAIN	c_NullDW
UO_CB_MAIN	c_NullCB
MENU		c_NullMenu
PICTURE		c_NullPicture

STRING		c_INIUndefined 		= "[INI_Undefined]"
STRING		c_XKey 			= "X"
STRING		c_YKey 			= "Y"
STRING		c_WidthKey 		= "Width"
STRING		c_HeightKey 		= "Height"
STRING		c_WinStateKey 		= "WindowState"
INTEGER		c_MinimumOverlap 		= 25

UNSIGNEDLONG	c_SOCPromptUser 		= 100
UNSIGNEDLONG	c_SOCPromptUserOnce 	= 101
UNSIGNEDLONG	c_SOCSave 		= 102
UNSIGNEDLONG	c_SOCNoSave 		= 103

UNSIGNEDLONG	c_Invisible 		= 0
UNSIGNEDLONG	c_Show 			= 1
UNSIGNEDLONG	c_Mark 			= 2
UNSIGNEDLONG	c_ShowMark 		= 3

UNSIGNEDLONG	c_ColorUndefined 		= 0
UNSIGNEDLONG	c_Black 			= 1
UNSIGNEDLONG	c_White 			= 2
UNSIGNEDLONG	c_Gray 			= 3
UNSIGNEDLONG	c_DarkGray 		= 4
UNSIGNEDLONG	c_Red 			= 5
UNSIGNEDLONG	c_DarkRed 		= 6
UNSIGNEDLONG	c_Green 			= 7
UNSIGNEDLONG	c_DarkGreen 		= 8
UNSIGNEDLONG	c_Blue			= 9 
UNSIGNEDLONG	c_DarkBlue 		= 10
UNSIGNEDLONG	c_Magenta 		= 11
UNSIGNEDLONG	c_DarkMagenta 		= 12
UNSIGNEDLONG	c_Cyan 			= 13
UNSIGNEDLONG	c_DarkCyan 		= 14
UNSIGNEDLONG	c_Yellow 			= 15
UNSIGNEDLONG	c_Brown 			= 16

UNSIGNEDLONG	c_ColorFirst 		= 1
UNSIGNEDLONG	c_ColorLast 		= 16

UNSIGNEDLONG	c_DW_BorderUndefined 	= 0
UNSIGNEDLONG	c_DW_BorderNone 	= 1
UNSIGNEDLONG	c_DW_BorderShadowBox 	= 2
UNSIGNEDLONG	c_DW_BorderBox 		= 3
UNSIGNEDLONG	c_DW_BorderResize 	= 4
UNSIGNEDLONG	c_DW_BorderUnderLine 	= 5
UNSIGNEDLONG	c_DW_BorderLowered 	= 6
UNSIGNEDLONG	c_DW_BorderRaised 	= 7

UNSIGNEDLONG	c_DW_BorderFirst 		= 1
UNSIGNEDLONG	c_DW_BorderLast	 	= 7

INTEGER		c_AcceptCB 		= 1
INTEGER		c_CancelCB 		= 2
INTEGER		c_CloseCB 		= 3
INTEGER		c_DeleteCB 		= 4
INTEGER		c_FilterCB 		= 5
INTEGER		c_FirstCB 		= 6
INTEGER		c_InsertCB 		= 7
INTEGER		c_LastCB 		= 8
INTEGER		c_MainCB 		= 9
INTEGER		c_ModifyCB 		= 10
INTEGER		c_NewCB 		= 11
INTEGER		c_NextCB 		= 12
INTEGER		c_PreviousCB 		= 13
INTEGER		c_PrintCB 		= 14
INTEGER		c_QueryCB 		= 15
INTEGER		c_ResetQueryCB 		= 16
INTEGER		c_RetrieveCB 		= 17
INTEGER		c_SaveCB 		= 18
INTEGER		c_SaveRowsAsCB 		= 19
INTEGER		c_SearchCB 		= 20
INTEGER		c_ViewCB 		= 21

end variables

forward prototypes
public subroutine get_ini_size ()
public subroutine put_ini_size ()
public subroutine set_ini_section (string file_section)
public subroutine set_mdi_emenu (menu edit_menu)
public subroutine set_mdi_fmenu (menu file_menu)
public subroutine set_mdi_options (unsignedlong control_word)
public subroutine set_mdi_pmenu (menu popup_menu)
public subroutine set_mdi_popup (menu popup_menu)
public subroutine set_tb_float_position (long x_pos, long y_pos)
end prototypes

on pc_closeall;//******************************************************************
//  PC Module     : w_MDI_Frame
//  Event         : pc_CloseAll
//  Description   : Close all the open windows in the MDI Frame.
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Idx, l_NumWin

PCCA.Error = c_Success

PCCA.MDI.fu_PushID(PCCA.MDI.c_MDI_Close, c_ShowMark)

//------------------------------------------------------------------
//  Close every open window.
//------------------------------------------------------------------

IF PCCA.Num_Windows > 0 THEN
   l_NumWin         = PCCA.Num_Windows
   PCCA.Num_Windows = 0

   FOR l_Idx = l_NumWin TO 1 STEP -1
      IF IsValid(PCCA.Window_List[l_Idx]) THEN
         Close(PCCA.Window_List[l_Idx])
         IF PCCA.Error <> c_Success THEN
            PCCA.Num_Windows = l_Idx
            EXIT
         END IF
      END IF
   NEXT
END IF

PCCA.MDI.fu_Pop()
end on

on pc_mdiactive;//******************************************************************
//  PC Module     : w_MDI_Frame
//  Event         : pc_MDIActive
//  Description   : This event runs when all sheets have been
//                  closed.  If auto configuration for the MDI menu
//                  is on, set the standard PowerClass menu items
//                  appropriately for a MDI menu.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

//------------------------------------------------------------------
//  See if we are to Auto-Configure the menus.
//------------------------------------------------------------------

IF i_MDI_AutoConfigMenus THEN

   //---------------------------------------------------------------
   //  Since this is the MDI Frame, none of these should be
   //  visible.
   //---------------------------------------------------------------

   IF i_FileMenuIsValid THEN
      IF i_FileMenuCloseNum > 0 THEN
         i_FileMenuClose.Visible = FALSE
      END IF
      IF i_FileMenuFileSepNum > 0 THEN
         i_FileMenuFileSep.Visible = FALSE
      END IF
      IF i_FileMenuCancelNum > 0 THEN
         i_FileMenuCancel.Visible = FALSE
      END IF
      IF i_FileMenuAcceptNum > 0 THEN
         i_FileMenuAccept.Visible = FALSE
      END IF
      IF i_FileMenuSaveNum > 0 THEN
         i_FileMenuSave.Visible = FALSE
      END IF
      IF i_FileMenuSaveRowsAsNum > 0 THEN
         i_FileMenuSaveRowsAs.Visible = FALSE
      END IF
      IF i_FileMenuSaveSepNum > 0 THEN
         i_FileMenuSaveSep.Visible = FALSE
      END IF
      IF i_FileMenuPrintNum > 0 THEN
         i_FileMenuPrint.Visible = FALSE
      END IF
      IF i_FileMenuPrintSepNum > 0 THEN
         i_FileMenuPrintSep.Visible = FALSE
      END IF
   END IF

   IF i_EditMenuIsValid THEN
      i_EditMenu.Enabled = FALSE
   END IF

   IF i_PopupMenuIsValid THEN
      i_PopupMenu.Enabled = FALSE
   END IF
END IF
end on

on pc_setsize;//******************************************************************
//  PC Module     : w_MDI_Frame
//  Event         : pc_SetSize
//  Description   : Posted event which sets the window
//                  state when the window is opened.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

IF i_MDI_SavePosition THEN
IF i_INIValidWS       THEN
   IF WindowState <> i_INIWindowState THEN
      WindowState =  i_INIWindowState
   END IF
END IF
END IF

//------------------------------------------------------------------
//  If it was requested, we already opened the MicroHelp window.
//  Now, we'll configure it and get it running.
//------------------------------------------------------------------

IF WindowType = MDIHelp! AND &
   (i_MDI_ShowResources OR i_MDI_ShowClock) THEN
   IF IsValid(w_MicroHelp) THEN
      w_MicroHelp.i_ShowResources = i_MDI_ShowResources
      w_MicroHelp.i_ShowClock     = i_MDI_ShowClock
      w_MicroHelp.fw_SetPosition()
      w_MicroHelp.TriggerEvent("Timer")
      Timer(i_UpdateTime, w_MicroHelp)
   END IF
END IF
end on

on pc_wm_initmenu;//******************************************************************
//  PC Module     : w_MDI_Frame
//  Event         : pc_WM_InitMenu
//  Description   : If there is not any current DataWindow,
//                  tells the window to disable the Edit menu.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

IF IsValid(PCCA.Window_CurrentDW) THEN
   IF NOT PCCA.Window_CurrentDW.i_Window.ToolBarVisible THEN
      PCCA.Window_CurrentDW.is_EventControl.Control_Mode = &
         PCCA.Window_CurrentDW.c_ControlMainMenu
      PCCA.Window_CurrentDW.TriggerEvent("pcd_SetControl")
   END IF
ELSE
   IF IsValid(PCCA.Window_Current) THEN
      PCCA.Window_Current.TriggerEvent("pc_WM_InitMenu")
   END IF
END IF
end on

on pc_wm_move;//******************************************************************
//  PC Module     : w_MDI_Frame
//  Event         : pc_WM_Move
//  Description   : Handles the move event for the MDI Frame.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

//------------------------------------------------------------------
//  Save the last position information.
//------------------------------------------------------------------

IF WindowState = Normal! THEN
   i_INISaveIsValid  = TRUE
   i_INISaveLastXPos = i_INILastXPos
   i_INISaveLastYPos = i_INILastYPos
   i_INILastXPos     = X
   i_INILastYPos     = Y
END IF

//------------------------------------------------------------------
//  Update the position of the MicroHelp window.
//------------------------------------------------------------------

IF IsValid(w_MicroHelp) THEN
   w_MicroHelp.fw_SetPosition()
END IF
end on

public subroutine get_ini_size ();//******************************************************************
//  PC Module     : w_MDI_Frame
//  Subroutine    : Get_INI_Size
//  Description   : Gets postion, size, and window state
//                  information from the INI file.
//
//  Parameters    : (None)
//
//  Return Value  : (None).  However, the following instance
//                  variables are modified:
//
//                     BOOLEAN      i_INIValidSize
//                     BOOLEAN      i_INIValidWS
//                     INTEGER      i_INIXPos
//                     INTEGER      i_INIYPos
//                     INTEGER      i_INIWidth
//                     INTEGER      i_INIHeight
//                     WINDOWSTATE  i_INIWindowState
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

STRING  l_SXPos,  l_SYPos
STRING  l_SWidth, l_SHeight, l_WinState

//------------------------------------------------------------------
//  Indictate that this routine has been called.
//------------------------------------------------------------------

i_INIGetDone = TRUE

//------------------------------------------------------------------
//  Get the window position from the INI file.
//------------------------------------------------------------------

l_SXPos    = ProfileString(PCCA.Application_INI_File, &
                           i_INIFileSection,          &
                           c_XKey,                    &
                           c_INIUndefined)
l_SYPos    = ProfileString(PCCA.Application_INI_File, &
                           i_INIFileSection,          &
                           c_YKey,                    &
                           c_INIUndefined)
l_SWidth   = ProfileString(PCCA.Application_INI_File, &
                           i_INIFileSection,          &
                           c_WidthKey,                &
                           c_INIUndefined)
l_SHeight  = ProfileString(PCCA.Application_INI_File, &
                           i_INIFileSection,          &
                           c_HeightKey,               &
                           c_INIUndefined)
l_WinState = ProfileString(PCCA.Application_INI_File, &
                           i_INIFileSection,          &
                           c_WinStateKey,             &
                           c_INIUndefined)

//------------------------------------------------------------------

i_INIValidSize = (l_SXPos   <> c_INIUndefined AND &
                  l_SYPos   <> c_INIUndefined AND &
                  l_SWidth  <> c_INIUndefined AND &
                  l_SHeight <> c_INIUndefined)

IF i_INIValidSize THEN
   i_INIXPos   = Integer(l_SXPos)
   i_INIYPos   = Integer(l_SYPos)
   i_INIWidth  = Integer(l_SWidth)
   i_INIHeight = Integer(l_SHeight)
ELSE
   i_INIXPos   = X
   i_INIYPos   = Y
   i_INIWidth  = Width
   i_INIHeight = Height
END IF

//------------------------------------------------------------------
//  Make sure that the window is not an unreasonable size and at
//  least a corner of it is on the display.
//------------------------------------------------------------------

IF i_INIWidth < c_MinimumOverlap THEN
   i_INIWidth = c_MinimumOverlap
END IF

IF i_INIHeight < c_MinimumOverlap THEN
   i_INIHeight = c_MinimumOverlap
END IF

IF i_INIXPos > PCCA.Screen_Width THEN
   i_INIXPos = PCCA.Screen_Width - c_MinimumOverlap
END IF
IF i_INIXPos + i_INIWidth < 1 THEN
   i_INIXPos = c_MinimumOverlap - i_INIWidth
END IF

IF i_INIYPos > PCCA.Screen_Height THEN
   i_INIYPos = PCCA.Screen_Height - c_MinimumOverlap
END IF
IF i_INIYPos + i_INIHeight < 1 THEN
   i_INIYPos = c_MinimumOverlap - i_INIHeight
END IF

//------------------------------------------------------------------
//  Check for window state.
//------------------------------------------------------------------

i_INIValidWS = (l_WinState <> c_INIUndefined)

IF i_INIValidWS THEN
   CHOOSE CASE l_WinState
      CASE "Maximized"
         i_INIWindowState = Maximized!
      CASE "Minimized"
         i_INIWindowState = Minimized!
      //CASE "Normal"
      CASE ELSE
         i_INIWindowState = Normal!
   END CHOOSE
ELSE
   i_INIWindowState = Normal!
END IF

RETURN
end subroutine

public subroutine put_ini_size ();//******************************************************************
//  PC Module     : w_MDI_Frame
//  Subroutine    : Put_INI_Size
//  Description   : Puts the postion, size, and window state
//                  information to the INI file.
//
//  Parameters    : (None)
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

STRING  l_WinState

//------------------------------------------------------------------
//  Indictate that this routine has been called.
//------------------------------------------------------------------

i_INIPutDone = TRUE

//------------------------------------------------------------------
//  Convert WindowState to a string so that it can be stored in
//  the INI file.
//------------------------------------------------------------------

IF i_INILastWindowState = Maximized! THEN
   l_WinState = "Maximized"
ELSE
   IF i_INILastWindowState = Minimized! THEN
      l_WinState = "Minimized"
   ELSE
      l_WinState = "Normal"
   END IF
END IF

//------------------------------------------------------------------
//  Store the position, size, and window state information.
//------------------------------------------------------------------

SetProfileString(PCCA.Application_INI_File, &
                 i_INIFileSection,          &
                 c_XKey,                    &
                 String(i_INILastXPos))
SetProfileString(PCCA.Application_INI_File, &
                 i_INIFileSection,          &
                 c_YKey,                    &
                 String(i_INILastYPos))
SetProfileString(PCCA.Application_INI_File, &
                 i_INIFileSection,          &
                 c_WidthKey,                &
                 String(i_INILastWidth))
SetProfileString(PCCA.Application_INI_File, &
                 i_INIFileSection,          &
                 c_HeightKey,               &
                 String(i_INILastHeight))
SetProfileString(PCCA.Application_INI_File, &
                 i_INIFileSection,          &
                 c_WinStateKey,             &
                 l_WinState)

RETURN
end subroutine

public subroutine set_ini_section (string file_section);//******************************************************************
//  PC Module     : w_MDI_Frame
//  Subroutine    : Set_INI_Section
//  Description   : Sets the file section that the window's
//                  size and position will be saved to in
//                  the INI file.
//
//  Parameters    : STRING File_Section -
//                        The section in the INI file where to
//                        place the window information.
//                        By default, PowerClass will use the
//                        following format if the developer does
//                        not call this routine:
//
//                           PCCA.Application_Name + &
//                              "/" + <window>.ClassName()
//
//                        Example:
//                           "PowerClass Sample/w_mdi_frame"
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

i_INIFileSection = File_Section

RETURN
end subroutine

public subroutine set_mdi_emenu (menu edit_menu);//******************************************************************
//  PC Module     : w_MDI_Frame
//  Subroutine    : Set_MDI_EMenu
//  Description   : Initializes the EDIT menu for w_MDI_Frame
//
//  Parameters    : MENU Edit_Menu -
//                        The menu that is to be used
//                        as the EDIT menu by PowerClass.
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Idx, l_Jdx
MENU     l_TmpMenu

//------------------------------------------------------------------
//  We indicate that this routine has been called by setting
//  i_EditMenuInit to TRUE.  If this routine is called by the
//  developer, this flag will be TRUE and PowerClass will know
//  not to not call it again.
//------------------------------------------------------------------

i_EditMenuInit = TRUE

//------------------------------------------------------------------
//  Set the our menu to the specified menu.
//------------------------------------------------------------------

i_EditMenu = Edit_Menu

//------------------------------------------------------------------
//  See if the EDIT Menu is valid.  If it is, save the individual
//  menu items into instance variables for speedy access.
//------------------------------------------------------------------

i_EditMenuIsValid       = IsValid(i_EditMenu)
i_EditMenuNewNum        = 0
i_EditMenuViewNum       = 0
i_EditMenuModifyNum     = 0
i_EditMenuInsertNum     = 0
i_EditMenuDeleteNum     = 0
i_EditMenuModeSepNum    = 0
i_EditMenuFirstNum      = 0
i_EditMenuPrevNum       = 0
i_EditMenuNextNum       = 0
i_EditMenuLastNum       = 0
i_EditMenuSearchSepNum  = 0
i_EditMenuQueryNum      = 0
i_EditMenuSearchNum     = 0
i_EditMenuFilterNum     = 0
i_EditMenuSaveSepNum    = 0
i_EditMenuSaveNum       = 0
i_EditMenuSaveRowsAsNum = 0
i_EditMenuPrintSepNum   = 0
i_EditMenuPrintNum      = 0

IF i_EditMenuIsValid THEN
   l_Jdx = UpperBound(i_EditMenu.Item[])
   FOR l_Idx = 1 TO l_Jdx
      l_TmpMenu = i_EditMenu.Item[l_Idx]
      CHOOSE CASE l_TmpMenu.Text
         CASE PCCA.MDI.c_MDI_MenuLabelNew
            i_EditMenuNewNum        = l_Idx
            i_EditMenuNew           = l_TmpMenu
         CASE PCCA.MDI.c_MDI_MenuLabelView
            i_EditMenuViewNum       = l_Idx
            i_EditMenuView          = l_TmpMenu
         CASE PCCA.MDI.c_MDI_MenuLabelModify
            i_EditMenuModifyNum     = l_Idx
            i_EditMenuModify        = l_TmpMenu
         CASE PCCA.MDI.c_MDI_MenuLabelInsert
            i_EditMenuInsertNum     = l_Idx
            i_EditMenuInsert        = l_TmpMenu
         CASE PCCA.MDI.c_MDI_MenuLabelDelete
            i_EditMenuDeleteNum     = l_Idx
            i_EditMenuDelete        = l_TmpMenu
            IF l_Idx < l_Jdx THEN
               IF i_EditMenu.Item[l_Idx + 1].Text = &
                  PCCA.MDI.c_MDI_MenuLabelSeperator THEN
                  i_EditMenuModeSepNum = l_Idx + 1
                  i_EditMenuModeSep    = &
                     i_EditMenu.Item[l_Idx + 1]
               END IF
            END IF
         CASE PCCA.MDI.c_MDI_MenuLabelFirst
            i_EditMenuFirstNum      = l_Idx
            i_EditMenuFirst         = l_TmpMenu
         CASE PCCA.MDI.c_MDI_MenuLabelPrev
            i_EditMenuPrevNum       = l_Idx
            i_EditMenuPrev          = l_TmpMenu
         CASE PCCA.MDI.c_MDI_MenuLabelNext
            i_EditMenuNextNum       = l_Idx
            i_EditMenuNext          = l_TmpMenu
         CASE PCCA.MDI.c_MDI_MenuLabelLast
            i_EditMenuLastNum       = l_Idx
            i_EditMenuLast          = l_TmpMenu
         CASE PCCA.MDI.c_MDI_MenuLabelQuery
            i_EditMenuQueryNum      = l_Idx
            i_EditMenuQuery         = l_TmpMenu
            IF l_Idx > 1 THEN
               IF i_EditMenu.Item[l_Idx - 1].Text = &
                  PCCA.MDI.c_MDI_MenuLabelSeperator THEN
                  i_EditMenuSearchSepNum = l_Idx - 1
                  i_EditMenuSearchSep    = &
                     i_EditMenu.Item[l_Idx - 1]
               END IF
            END IF
         CASE PCCA.MDI.c_MDI_MenuLabelSearch
            i_EditMenuSearchNum     = l_Idx
            i_EditMenuSearch        = l_TmpMenu
         CASE PCCA.MDI.c_MDI_MenuLabelFilter
            i_EditMenuFilterNum     = l_Idx
            i_EditMenuFilter        = l_TmpMenu
         CASE PCCA.MDI.c_MDI_MenuLabelSave
            i_EditMenuSaveNum       = l_Idx
            i_EditMenuSave          = l_TmpMenu
            IF l_Idx > 1 THEN
               IF i_EditMenu.Item[l_Idx - 1].Text = &
                  PCCA.MDI.c_MDI_MenuLabelSeperator THEN
                  i_EditMenuSaveSepNum = l_Idx - 1
                  i_EditMenuSaveSep    = &
                     i_EditMenu.Item[l_Idx - 1]
               END IF
            END IF
         CASE PCCA.MDI.c_MDI_MenuLabelSaveRowsAs
            i_EditMenuSaveRowsAsNum = l_Idx
            i_EditMenuSaveRowsAs    = l_TmpMenu
         CASE PCCA.MDI.c_MDI_MenuLabelPrint
            i_EditMenuPrintNum      = l_Idx
            i_EditMenuPrint         = l_TmpMenu
            IF l_Idx > 1 THEN
               IF i_EditMenu.Item[l_Idx - 1].Text = &
                  PCCA.MDI.c_MDI_MenuLabelSeperator THEN
                  i_EditMenuPrintSepNum = l_Idx - 1
                  i_EditMenuPrintSep    = &
                     i_EditMenu.Item[l_Idx - 1]
               END IF
            END IF
      END CHOOSE
   NEXT
END IF

IF i_EditMenuIsPopup THEN
   PCCA.DLL.fu_SetMDIPMenu (THIS)
END IF

RETURN
end subroutine

public subroutine set_mdi_fmenu (menu file_menu);//******************************************************************
//  PC Module     : w_MDI_Frame
//  Subroutine    : Set_MDI_FMenu
//  Description   : Initializes the FILE menu for w_MDI_Frame
//
//  Parameters    : MENU File_Menu -
//                        The menu that is to be used
//                        as the FILE menu by PowerClass.
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Idx, l_Jdx
MENU     l_TmpMenu

//------------------------------------------------------------------
//  We indicate that this routine has been called by setting
//  i_FileMenuInit to TRUE.  If this routine is called by the
//  developer, this flag will be TRUE and PowerClass will know
//  not to not call it again.
//------------------------------------------------------------------

i_FileMenuInit = TRUE

//------------------------------------------------------------------
//  Set the our menu to the specified menu.
//------------------------------------------------------------------

i_FileMenu = File_Menu

//------------------------------------------------------------------
//  See if the FILE Menu is valid.  If it is, save the individual
//  menu items into instance variables for speedy access.
//------------------------------------------------------------------

i_FileMenuIsValid       = IsValid(i_FileMenu)
i_FileMenuNewNum        = 0
i_FileMenuOpenNum       = 0
i_FileMenuCloseNum      = 0
i_FileMenuFileSepNum    = 0
i_FileMenuCancelNum     = 0
i_FileMenuAcceptNum     = 0
i_FileMenuSaveSepNum    = 0
i_FileMenuSaveNum       = 0
i_FileMenuSaveRowsAsNum = 0
i_FileMenuPrintSepNum   = 0
i_FileMenuPrintNum      = 0
i_FileMenuPrintSetupNum = 0
i_FileMenuExitSepNum    = 0
i_FileMenuExitNum       = 0

IF i_FileMenuIsValid THEN
   l_Jdx = UpperBound(i_FileMenu.Item[])
   FOR l_Idx = 1 TO l_Jdx
      l_TmpMenu = i_FileMenu.Item[l_Idx]
      CHOOSE CASE l_TmpMenu.Text
         CASE PCCA.MDI.c_MDI_MenuLabelNew
            i_FileMenuNewNum        = l_Idx
            i_FileMenuNew           = l_TmpMenu
         CASE PCCA.MDI.c_MDI_MenuLabelOpen
            i_FileMenuOpenNum       = l_Idx
            i_FileMenuOpen          = l_TmpMenu
         CASE PCCA.MDI.c_MDI_MenuLabelClose
            i_FileMenuCloseNum      = l_Idx
            i_FileMenuClose         = l_TmpMenu
            IF l_Idx < l_Jdx THEN
               IF i_FileMenu.Item[l_Idx + 1].Text = &
                  PCCA.MDI.c_MDI_MenuLabelSeperator THEN
                  i_FileMenuFileSepNum = l_Idx + 1
                  i_FileMenuFileSep    = &
                     i_FileMenu.Item[l_Idx + 1]
               END IF
            END IF
         CASE PCCA.MDI.c_MDI_MenuLabelCancel
            i_FileMenuCancelNum     = l_Idx
            i_FileMenuCancel        = l_TmpMenu
         CASE PCCA.MDI.c_MDI_MenuLabelAccept
            i_FileMenuAcceptNum     = l_Idx
            i_FileMenuAccept        = l_TmpMenu
         CASE PCCA.MDI.c_MDI_MenuLabelSave
            i_FileMenuSaveNum       = l_Idx
            i_FileMenuSave          = l_TmpMenu
            IF l_Idx > 1 THEN
               IF i_FileMenu.Item[l_Idx - 1].Text = &
                  PCCA.MDI.c_MDI_MenuLabelSeperator THEN
                  i_FileMenuSaveSepNum = l_Idx - 1
                  i_FileMenuSaveSep    = &
                     i_FileMenu.Item[l_Idx - 1]
               END IF
            END IF
         CASE PCCA.MDI.c_MDI_MenuLabelSaveRowsAs
            i_FileMenuSaveRowsAsNum = l_Idx
            i_FileMenuSaveRowsAs    = l_TmpMenu
         CASE PCCA.MDI.c_MDI_MenuLabelPrint
            i_FileMenuPrintNum      = l_Idx
            i_FileMenuPrint         = l_TmpMenu
            IF l_Idx > 1 THEN
               IF i_FileMenu.Item[l_Idx - 1].Text = &
                  PCCA.MDI.c_MDI_MenuLabelSeperator THEN
                  i_FileMenuPrintSepNum = l_Idx - 1
                  i_FileMenuPrintSep    = &
                     i_FileMenu.Item[l_Idx - 1]
               END IF
            END IF
         CASE PCCA.MDI.c_MDI_MenuLabelPrintSetup
            i_FileMenuPrintSetupNum = l_Idx
            i_FileMenuPrintSetup    = l_TmpMenu
         CASE PCCA.MDI.c_MDI_MenuLabelExit
            i_FileMenuExitNum       = l_Idx
            i_FileMenuExit          = l_TmpMenu
            IF l_Idx > 1 THEN
               IF i_FileMenu.Item[l_Idx - 1].Text = &
                  PCCA.MDI.c_MDI_MenuLabelSeperator THEN
                  i_FileMenuExitSepNum = l_Idx - 1
                  i_FileMenuExitSep    = &
                     i_FileMenu.Item[l_Idx - 1]
               END IF
            END IF
      END CHOOSE
   NEXT
END IF

RETURN
end subroutine

public subroutine set_mdi_options (unsignedlong control_word);//******************************************************************
//  PC Module     : w_MDI_Frame
//  Subroutine    : Set_MDI_Options
//  Description   : Looks at the passed words and sets the MDI
//                  options.
//
//  Parameters    : UNSIGNEDLONG Control_Word -
//                        Specifies options for how the
//                        MDI Frame is to behave.
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

//------------------------------------------------------------------
//  Indicate that Set_MDI_Options() has run, so that the Open
//  event will not run it again.
//------------------------------------------------------------------

i_OptionsInit = TRUE

//------------------------------------------------------------------
//  Have the DLL calculate the bits.
//------------------------------------------------------------------

PCCA.DLL.fu_SetMDIOptions (THIS, Control_Word)

//------------------------------------------------------------------
//  Put the redraw specification in the PCCA.
//------------------------------------------------------------------

PCCA.MDI_Allow_Redraw = i_MDI_AllowRedraw

//------------------------------------------------------------------
//  Process how the PowerBuilder Toolbar will be handled.
//------------------------------------------------------------------

CHOOSE CASE i_MDI_ToolBarPosition
   CASE c_MDI_ToolBarLeft
      ToolBarVisible   = TRUE
      ToolBarAlignment = AlignAtLeft!
   CASE c_MDI_ToolBarRight
      ToolBarVisible   = TRUE
      ToolBarAlignment = AlignAtRight!
   CASE c_MDI_ToolBarTop
      ToolBarVisible   = TRUE
      ToolBarAlignment = AlignAtTop!
   CASE c_MDI_ToolBarBottom
      ToolBarVisible   = TRUE
      ToolBarAlignment = AlignAtBottom!
   CASE c_MDI_ToolBarFloat
      ToolBarVisible   = TRUE
      ToolBarAlignment = Floating!
   //CASE c_MDI_ToolBarNone
   CASE ELSE
      ToolBarVisible   = FALSE
END CHOOSE

//------------------------------------------------------------------
//  Process how the PowerBuilder Toolbar Text will be handled.
//------------------------------------------------------------------

PCCA.Application_Obj.ToolBarText = i_MDI_ToolBarText

RETURN
end subroutine

public subroutine set_mdi_pmenu (menu popup_menu);//******************************************************************
//  PC Module     : w_MDI_Frame
//  Subroutine    : Set_MDI_PMenu
//  Description   : Initializes the POPUP menu for w_MDI_Frame
//
//  Parameters    : MENU Popup_Menu -
//                       The POPUP menu that is to be used
//                       by the w_MDI_Frame.  If it is
//                       specified as c_NullMenu, then a
//                       POPUP menu will be created from
//                       M_POPUP.
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Idx, l_Jdx
MENU     l_TmpMenu

//------------------------------------------------------------------
//  We indicate that this routine has been called by setting
//  i_PopupMenuInit to TRUE.  If this routine is called by the
//  developer, this flag will be TRUE and PowerClass will know
//  not to not call it again.
//------------------------------------------------------------------

i_PopupMenuInit = TRUE

//------------------------------------------------------------------
//  Set our menu to the specified menu.
//------------------------------------------------------------------

i_PopupMenu = Popup_Menu

//------------------------------------------------------------------
//  See if the POPUP Menu is valid.  If it is, save the individual
//  menu items into instance variables for speedy access.
//------------------------------------------------------------------

i_PopupMenuIsValid       = IsValid(i_PopupMenu)
i_PopupMenuNewNum        = 0
i_PopupMenuViewNum       = 0
i_PopupMenuModifyNum     = 0
i_PopupMenuInsertNum     = 0
i_PopupMenuDeleteNum     = 0
i_PopupMenuModeSepNum    = 0
i_PopupMenuFirstNum      = 0
i_PopupMenuPrevNum       = 0
i_PopupMenuNextNum       = 0
i_PopupMenuLastNum       = 0
i_PopupMenuSearchSepNum  = 0
i_PopupMenuQueryNum      = 0
i_PopupMenuSearchNum     = 0
i_PopupMenuFilterNum     = 0
i_PopupMenuSaveSepNum    = 0
i_PopupMenuSaveNum       = 0
i_PopupMenuSaveRowsAsNum = 0
i_PopupMenuPrintSepNum   = 0
i_PopupMenuPrintNum      = 0

//------------------------------------------------------------------
//  Determine if the POPUP menu is the same menu as the EDIT Menu.
//------------------------------------------------------------------

IF i_PopupMenuIsValid THEN
    IF i_EditMenuIsValid THEN
       i_PopupMenuIsEdit = (i_EditMenu = i_PopupMenu)
    ELSE
       i_PopupMenuIsEdit = FALSE
   END IF
ELSE

   //---------------------------------------------------------------
   //  The POPUP Menu is not valid.  Indicate that the EDIT Menu
   //  should be used when the POPUP Menu is needed.
   //---------------------------------------------------------------

   i_PopupMenuIsEdit = TRUE
END IF

i_EditMenuIsPopup = i_PopupMenuIsEdit

//------------------------------------------------------------------
//  If the POPUP Menu is not the same as the EDIT Menu, then
//  save the individual menu items into instance variables for
//  speedy access.
//------------------------------------------------------------------

IF i_PopupMenuIsValid THEN
   IF NOT i_PopupMenuIsEdit THEN
      l_Jdx = UpperBound(i_PopupMenu.Item[])
      FOR l_Idx = 1 TO l_Jdx
         l_TmpMenu = i_PopupMenu.Item[l_Idx]
         CHOOSE CASE l_TmpMenu.Text
            CASE PCCA.MDI.c_MDI_MenuLabelNew
               i_PopupMenuNewNum        = l_Idx
               i_PopupMenuNew           = l_TmpMenu
            CASE PCCA.MDI.c_MDI_MenuLabelView
               i_PopupMenuViewNum       = l_Idx
               i_PopupMenuView          = l_TmpMenu
            CASE PCCA.MDI.c_MDI_MenuLabelModify
               i_PopupMenuModifyNum     = l_Idx
               i_PopupMenuModify        = l_TmpMenu
            CASE PCCA.MDI.c_MDI_MenuLabelInsert
               i_PopupMenuInsertNum     = l_Idx
               i_PopupMenuInsert        = l_TmpMenu
            CASE PCCA.MDI.c_MDI_MenuLabelDelete
               i_PopupMenuDeleteNum     = l_Idx
               i_PopupMenuDelete        = l_TmpMenu
               IF l_Idx < l_Jdx THEN
                  IF i_PopupMenu.Item[l_Idx + 1].Text = &
                     PCCA.MDI.c_MDI_MenuLabelSeperator THEN
                     i_PopupMenuModeSepNum = l_Idx + 1
                     i_PopupMenuModeSep    = &
                        i_PopupMenu.Item[l_Idx + 1]
                  END IF
               END IF
            CASE PCCA.MDI.c_MDI_MenuLabelFirst
               i_PopupMenuFirstNum      = l_Idx
               i_PopupMenuFirst         = l_TmpMenu
            CASE PCCA.MDI.c_MDI_MenuLabelPrev
               i_PopupMenuPrevNum       = l_Idx
               i_PopupMenuPrev          = l_TmpMenu
            CASE PCCA.MDI.c_MDI_MenuLabelNext
               i_PopupMenuNextNum       = l_Idx
               i_PopupMenuNext          = l_TmpMenu
            CASE PCCA.MDI.c_MDI_MenuLabelLast
               i_PopupMenuLastNum       = l_Idx
               i_PopupMenuLast          = l_TmpMenu
            CASE PCCA.MDI.c_MDI_MenuLabelQuery
               i_PopupMenuQueryNum      = l_Idx
               i_PopupMenuQuery         = l_TmpMenu
               IF l_Idx > 1 THEN
                  IF i_PopupMenu.Item[l_Idx - 1].Text = &
                     PCCA.MDI.c_MDI_MenuLabelSeperator THEN
                     i_PopupMenuSearchSepNum = l_Idx - 1
                     i_PopupMenuSearchSep    = &
                        i_PopupMenu.Item[l_Idx - 1]
                  END IF
               END IF
            CASE PCCA.MDI.c_MDI_MenuLabelSearch
               i_PopupMenuSearchNum     = l_Idx
               i_PopupMenuSearch        = l_TmpMenu
            CASE PCCA.MDI.c_MDI_MenuLabelFilter
               i_PopupMenuFilterNum     = l_Idx
               i_PopupMenuFilter        = l_TmpMenu
            CASE PCCA.MDI.c_MDI_MenuLabelSave
               i_PopupMenuSaveNum       = l_Idx
               i_PopupMenuSave          = l_TmpMenu
               IF l_Idx > 1 THEN
                  IF i_PopupMenu.Item[l_Idx - 1].Text = &
                     PCCA.MDI.c_MDI_MenuLabelSeperator THEN
                     i_PopupMenuSaveSepNum = l_Idx - 1
                     i_PopupMenuSaveSep    = &
                        i_PopupMenu.Item[l_Idx - 1]
                  END IF
               END IF
            CASE PCCA.MDI.c_MDI_MenuLabelSaveRowsAs
               i_PopupMenuSaveRowsAsNum = l_Idx
               i_PopupMenuSaveRowsAs    = l_TmpMenu
            CASE PCCA.MDI.c_MDI_MenuLabelPrint
               i_PopupMenuPrintNum      = l_Idx
               i_PopupMenuPrint         = l_TmpMenu
               IF l_Idx > 1 THEN
                  IF i_PopupMenu.Item[l_Idx - 1].Text = &
                     PCCA.MDI.c_MDI_MenuLabelSeperator THEN
                     i_PopupMenuPrintSepNum = l_Idx - 1
                     i_PopupMenuPrintSep    = &
                        i_PopupMenu.Item[l_Idx - 1]
                  END IF
               END IF
         END CHOOSE
      NEXT
   ELSE
      PCCA.DLL.fu_SetMDIPMenu (THIS)
   END IF
END IF

RETURN
end subroutine

public subroutine set_mdi_popup (menu popup_menu);//******************************************************************
//  PC Module     : w_MDI_Frame
//  Subroutine    : Set_MDI_PMenu
//  Description   : Sets the POPUP menu to be used by the
//                  MDI Frame.
//
//  Parameters    : MENU Popup_Menu -
//                       The POPUP menu that is to be used
//                       by the w_MDI_Frame.  If it is
//                       specified as c_NullMenu, then a
//                       POPUP menu will be created from
//                       M_POPUP.
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Idx, l_Jdx

//------------------------------------------------------------------
//  If the passed POPUP menu is not valid, then create a popup
//  of type M_POPUP.
//------------------------------------------------------------------

IF IsValid(Popup_Menu) THEN
   i_PopupMenu = Popup_Menu.Item[1]
ELSE
   IF IsValid(i_mPopup) THEN
   ELSE
      i_PopupMenu = c_NullMenu
      i_mPopup    = CREATE M_POPUP
   END IF

   IF IsValid(i_mPopup) THEN
      i_PopupMenu         = i_mPopup.Item[1]
      i_PopupMenu.Visible = TRUE

      l_Jdx = UpperBound(i_PopupMenu.Item[])
      FOR l_Idx = 1 TO l_Jdx
          i_PopupMenu.Item[l_Idx].Visible = TRUE
          i_PopupMenu.Item[l_Idx].Enabled = FALSE
      NEXT
   END IF
END IF

IF IsValid(i_PopupMenu) THEN
   i_PopupMenuIsValid = TRUE
ELSE
   i_PopupMenuIsValid = FALSE
END IF

RETURN
end subroutine

public subroutine set_tb_float_position (long x_pos, long y_pos);//******************************************************************
//  PC Module     : w_MDI_Frame
//  Subroutine    : Set_TB_Float_Position
//  Description   : Set Toolbar Attributes.
//
//  Parameters    : LONG X_Pos -
//                        X location for the toolbar.
//                  LONG Y_Pos -
//                        Y location for the toolbar.
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

i_MDI_ToolBarPosition = c_MDI_ToolBarFloat

ToolBarX              = X_Pos
ToolBarY              = Y_Pos
ToolBarVisible        = TRUE
ToolBarAlignment      = Floating!

RETURN
end subroutine

event open;//******************************************************************
//  PC Module     : w_MDI_Frame
//  Event         : Open
//  Description   : Open the MDI Frame window.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN  l_DevMoved, l_DevSized
INTEGER  l_XPos,     l_YPos
INTEGER  l_Width,    l_Height
INTEGER  l_Idx,      l_Jdx
STRING   l_FileSection
STRING   l_SXPos,    l_SYPos
STRING   l_SWidth,   l_SHeight, l_WinState

//------------------------------------------------------------------
//  Initialize the class name identifier.
//------------------------------------------------------------------

//this.backcolor = s_themes.theme[s_themes.current_theme].mdi_backcolor

i_ClassName = ClassName()

//------------------------------------------------------------------
//  Set the INI file section to a reasonable default.
//------------------------------------------------------------------

i_INIFileSection = PCCA.Application_Name + "/" + i_ClassName

//------------------------------------------------------------------
//  Set the name of the MDI frame window into the PCCA structure.
//  The MDI_Frame variable is used to determine if the MDI frame
//  exists (e.g. for setting MicroHelp).
//------------------------------------------------------------------

PCCA.MDI_Frame       = THIS
PCCA.MDI_Frame_Valid = TRUE

//------------------------------------------------------------------
//  Initialize the menu variables.
//------------------------------------------------------------------

i_mPopup           = PCCA.Null_Object
i_FileMenuIsValid  = FALSE
i_EditMenuIsValid  = FALSE
i_PopupMenuIsValid = FALSE

//------------------------------------------------------------------
//  Find the client area.
//------------------------------------------------------------------

FOR l_Idx = 1 TO UpperBound(Control[])
   IF Control[l_Idx].TypeOf() = MDIClient! THEN
      PCCA.MDI_Client = Control[l_Idx]
      EXIT
   END IF
NEXT

//------------------------------------------------------------------
//  Make sure that PCCA.PCMGR exists.
//------------------------------------------------------------------

IF IsValid(PCCA.PCMGR) THEN
ELSE
   Open(w_PCManager_Std)
END IF

//------------------------------------------------------------------
//  Let PCCA.PCMGR know that we are using him.
//------------------------------------------------------------------

PCCA.PCMGR.Inc_Usage(THIS)

//------------------------------------------------------------------
//  Set security on the objects.
//------------------------------------------------------------------

f_PL_Security(THIS)

//------------------------------------------------------------------
//  Initialize the MDI options.
//------------------------------------------------------------------

PCCA.DLL.fu_InitMDIBits (THIS)

//------------------------------------------------------------------
//  Initialize NULL objects for use by the developer.
//------------------------------------------------------------------

c_NullDW      = PCCA.Null_Object
c_NullCB      = PCCA.Null_Object
c_NullMenu    = PCCA.Null_Object
c_NullPicture = PCCA.Null_Object

//------------------------------------------------------------------
//  Remember the current window position.
//------------------------------------------------------------------

l_XPos   = X
l_YPos   = Y
l_Width  = WorkSpaceWidth()
l_Height = WorkSpaceHeight()

//------------------------------------------------------------------
//  Trigger the pc_SetWindow event to give the developer a
//  chance to specify options for the MDI Frame.
//------------------------------------------------------------------

TriggerEvent("pc_SetWindow")

//------------------------------------------------------------------
//  If the developer did not run Set_MDI_Options(), run it for
//  them so that they get any default options they have defined.
//------------------------------------------------------------------

IF NOT i_OptionsInit THEN
   Set_MDI_Options(c_Default)
END IF

//------------------------------------------------------------------
//  If the developer did not specify the menus, then we need to
//  set them up.
//------------------------------------------------------------------

IF MenuName <> "" THEN
IF NOT i_FileMenuIsValid OR NOT i_EditMenuIsValid THEN
   l_Jdx = UpperBound(MenuID.Item[])
   FOR l_Idx = 1 TO l_Jdx
      CHOOSE CASE MenuID.Item[l_Idx].Text
         CASE PCCA.MDI.c_MDI_MenuLabelFile
            IF NOT i_FileMenuIsValid THEN
               i_FileMenu        = MenuID.Item[l_Idx]
               i_FileMenuIsValid = TRUE
            END IF

         CASE PCCA.MDI.c_MDI_MenuLabelEdit
            IF NOT i_EditMenuIsValid THEN
               i_EditMenu        = MenuID.Item[l_Idx]
               i_EditMenuIsValid = TRUE
            END IF
            IF NOT i_PopupMenuIsValid THEN
               IF i_EditMenu.Visible THEN
                  i_PopupMenu        = MenuID.Item[l_Idx]
                  i_PopupMenuIsValid = TRUE
               END IF
            END IF
      END CHOOSE
   NEXT
END IF
END IF

//------------------------------------------------------------------
//  See if we are to Auto-Configure the menus.
//------------------------------------------------------------------

IF i_MDI_AutoConfigMenus THEN

   //---------------------------------------------------------------
   //  Since this is the MDI Frame, none of these should be
   //  visible.
   //---------------------------------------------------------------

   IF NOT i_FileMenuInit THEN
      Set_MDI_FMenu(i_FileMenu)
   END IF

   IF i_FileMenuIsValid THEN
      IF i_FileMenuCloseNum > 0 THEN
         i_FileMenuClose.Visible = FALSE
      END IF
      IF i_FileMenuFileSepNum > 0 THEN
         i_FileMenuFileSep.Visible = FALSE
      END IF
      IF i_FileMenuCancelNum > 0 THEN
         i_FileMenuCancel.Visible = FALSE
      END IF
      IF i_FileMenuAcceptNum > 0 THEN
         i_FileMenuAccept.Visible = FALSE
      END IF
      IF i_FileMenuSaveNum > 0 THEN
         i_FileMenuSave.Visible = FALSE
      END IF
      IF i_FileMenuSaveRowsAsNum > 0 THEN
         i_FileMenuSaveRowsAs.Visible = FALSE
      END IF
      IF i_FileMenuSaveSepNum > 0 THEN
         i_FileMenuSaveSep.Visible = FALSE
      END IF
      IF i_FileMenuPrintNum > 0 THEN
         i_FileMenuPrint.Visible = FALSE
      END IF
      IF i_FileMenuPrintSepNum > 0 THEN
         i_FileMenuPrintSep.Visible = FALSE
      END IF
   END IF

   IF NOT i_EditMenuInit THEN
      Set_MDI_EMenu(i_EditMenu)
   END IF

   IF i_EditMenuIsValid THEN
      i_EditMenu.Enabled = FALSE
   END IF

   IF NOT i_PopupMenuInit THEN
      Set_MDI_PMenu(i_PopupMenu)
   END IF

   IF i_PopupMenuIsValid THEN
      i_PopupMenu.Enabled = FALSE
   END IF
END IF

//------------------------------------------------------------------
//  See if we are to use the INI settings to size and position
//  the window.
//------------------------------------------------------------------

IF i_MDI_SavePosition THEN

   //------------------------------------------------------------
   //  If the INI information was not already gotten by the
   //  developer, get it.
   //------------------------------------------------------------

   IF NOT i_INIGetDone THEN
      Get_INI_Size()
   END IF

   //---------------------------------------------------------------
   //  See if the developer moved the window.
   //---------------------------------------------------------------

   l_DevMoved = (l_XPos <> X OR l_YPos <> Y)
   l_DevSized = (l_Width  <> WorkSpaceWidth() OR &
                 l_Height <> WorkSpaceHeight())

   //---------------------------------------------------------------
   //  If the developer did not move the window, then we will
   //  attempt to size and/or position the window.
   //---------------------------------------------------------------

   IF NOT l_DevMoved AND NOT l_DevSized THEN

      //------------------------------------------------------------
      //  Set the window position and size.
      //------------------------------------------------------------

      Move(i_INIXPos, i_INIYPos)
      Resize(i_INIWidth, i_INIHeight)
      l_DevMoved = TRUE
      l_DevSized = TRUE

      //------------------------------------------------------------
      //  It would be nice to take care of the WindowState here,
      //  but PowerBuilder won't let us.  It will be taken care
      //  of in pc_SetSize, which is a posted event.
      //------------------------------------------------------------

   END IF
END IF

//------------------------------------------------------------------
//  Open the MicroHelp window, if requested.  For the moment, tell
//  it that no services are requested to prevent it from updating
//  while we finish initializing the MDI frame.
//------------------------------------------------------------------

IF WindowType = MDIHelp! AND &
   (i_MDI_ShowResources OR i_MDI_ShowClock) THEN
   f_PO_MicroHelp(THIS, i_UpdateTime, FALSE, FALSE)
END IF

//------------------------------------------------------------------
//  If there is not a title on the MDI Frame, then change it to
//  the application name.
//------------------------------------------------------------------

IF Title = "MDI WINDOW" THEN
   Title = PCCA.Application_Name
END IF

//------------------------------------------------------------------
//  If there is a plaque window, make sure that it stays on top.
//------------------------------------------------------------------

// stefanop: disabilito placca
//IF IsValid(w_Plaque) THEN
//   Open(w_Plaque, THIS)
//END IF

//------------------------------------------------------------------
//  Post the pc_SetSize event, which may set the window state.
//------------------------------------------------------------------

PostEvent("pc_SetSize")
end event

on close;//******************************************************************
//  PC Module     : w_MDI_Frame
//  Event         : Close
//  Description   : Closes the MicroHelp window, if any.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

//------------------------------------------------------------------
//  Save our position to the INI file, if requested by the
//  developer.
//------------------------------------------------------------------

IF i_MDI_SavePosition THEN
   IF NOT i_INIPutDone THEN
      Put_INI_Size()
   END IF
END IF

//------------------------------------------------------------------
//  Destroy the POPUP menu if we created it.
//------------------------------------------------------------------

IF IsValid(i_mPopup) THEN
   DESTROY i_mPopup
END IF

//------------------------------------------------------------------
//  Close the MicroHelp indicator.
//------------------------------------------------------------------

IF IsValid(w_MicroHelp) THEN
   Close(w_MicroHelp)
END IF

//------------------------------------------------------------------
//  Indicate that the MDI frame is no longer there.
//------------------------------------------------------------------

PCCA.MDI_Frame_Valid = FALSE

//------------------------------------------------------------------
//  Let PCCA.PCMGR know that we are done with him.
//------------------------------------------------------------------

IF IsValid(PCCA.PCMGR) THEN
   PCCA.PCMGR.Dec_Usage(THIS)
END IF
end on

on closequery;//******************************************************************
//  PC Module     : w_MDI_Frame
//  Event         : CloseQuery
//  Description   : Make sure it is OK to close.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

TriggerEvent("pc_CloseAll")

IF PCCA.Error <> c_Success THEN
   Message.ReturnValue = 1
   RETURN
END IF
end on

on resize;//******************************************************************
//  PC Module     : w_MDI_Frame
//  Event         : Resize
//  Description   : Handles the resize event for the MDI Frame.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

//------------------------------------------------------------------
//  Save the last position, size, and window state information.
//------------------------------------------------------------------

IF WindowState = Normal! THEN
   i_INILastWidth  = Width
   i_INILastHeight = Height
ELSE
   IF i_INISaveIsValid THEN
      i_INISaveIsValid = FALSE
      i_INILastXPos    = i_INISaveLastXPos
      i_INILastYPos    = i_INISaveLastYPos
   END IF
END IF

i_INILastWindowState = WindowState

//------------------------------------------------------------------
//  Reposition the MicroHelp indicator.
//------------------------------------------------------------------

IF IsValid(w_MicroHelp) THEN
   w_MicroHelp.fw_SetPosition()
END IF

PCCA.MDI.fu_Refresh()
end on

on w_mdi_frame.create
if this.MenuName = "m_cs_xx" then this.MenuID = create m_cs_xx
this.mdi_1=create mdi_1
this.Control[]={this.mdi_1}
end on

on w_mdi_frame.destroy
if IsValid(MenuID) then destroy(MenuID)
destroy(this.mdi_1)
end on

type mdi_1 from mdiclient within w_mdi_frame
long BackColor=285212671
end type

