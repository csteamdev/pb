﻿$PBExportHeader$w_pcmanager_main.srw
$PBExportComments$PowerClass Ancestor Manager Window for defaults and activation control.
forward
global type w_pcmanager_main from Window
end type
type st_1 from statictext within w_pcmanager_main
end type
end forward

global type w_pcmanager_main from Window
int X=673
int Y=265
int Width=755
int Height=333
boolean Visible=false
boolean Enabled=false
boolean TitleBar=true
string Title="PowerClass Manager"
long BackColor=12632256
event pc_activate pbm_custom71
event pc_checkmdi pbm_custom72
event pc_exitapp pbm_custom73
event pc_postdw pbm_custom74
event pc_setwindow pbm_custom75
st_1 st_1
end type
global w_pcmanager_main w_pcmanager_main

type variables
//----------------------------------------------------------------------------
//  Instance Variables in sorted order.
//----------------------------------------------------------------------------

UO_DW_MAIN	i_ActivateDW
UO_DW_MAIN	i_ActivateFocusDW

STRING		i_ClassName

LONG		i_DW_Counter
BOOLEAN	i_DW_FocusDone
UO_DW_MAIN	i_DW_GlobalDWList[]
INTEGER		i_DW_InRefresh
BOOLEAN	i_DW_NeedRetrieve
UO_DW_MAIN	i_DW_NeedRetrieveDW
BOOLEAN	i_DW_NoBufferDrawing
INTEGER		i_DW_NumGlobalDW
STRING		i_DWEventName[]

INTEGER		i_EndOfPostedQueue
WINDOW		i_ExitWindow

STRING		i_INIFileSection
BOOLEAN	i_INIGetDone
INTEGER		i_INIHeight
INTEGER		i_INILastHeight
INTEGER		i_INILastWidth
WINDOWSTATE	i_INILastWindowState
INTEGER		i_INILastXPos
INTEGER		i_INILastYPos
BOOLEAN	i_INIPutDone
BOOLEAN	i_INISaveIsValid
INTEGER		i_INISaveLastXPos
INTEGER		i_INISaveLastYPos
BOOLEAN	i_INIValidSize
BOOLEAN	i_INIValidWS
INTEGER		i_INIWidth
WINDOWSTATE	i_INIWindowState
INTEGER		i_INIXPos
INTEGER		i_INIYPos

BOOLEAN	i_JustActivated

INTEGER		i_NumWindows

STRING		i_ObjectType 		= "w_PCManager_Main"

UO_DW_MAIN	i_PostDW[]
BOOLEAN	i_ProcessingActivate

INTEGER		i_StartOfPostedQueue

UNSIGNEDLONG	i_WindowID
WINDOW		i_WindowList[]

//----------------------------------------------------------------------------
//  Constant Variables in logical order.
//----------------------------------------------------------------------------

UNSIGNEDLONG	c_MasterList
UNSIGNEDLONG	c_MasterEdit
UNSIGNEDLONG	c_DetailList
UNSIGNEDLONG	c_DetailEdit

UNSIGNEDLONG	c_ScrollSelf
UNSIGNEDLONG	c_ScrollParent

UNSIGNEDLONG	c_IsNotInstance
UNSIGNEDLONG	c_IsInstance

UNSIGNEDLONG	c_RequiredOnSave
UNSIGNEDLONG	c_AlwaysCheckRequired

UNSIGNEDLONG	c_NoNew
UNSIGNEDLONG	c_NewOk

UNSIGNEDLONG	c_NoModify
UNSIGNEDLONG	c_ModifyOk

UNSIGNEDLONG	c_NoDelete
UNSIGNEDLONG	c_DeleteOk

UNSIGNEDLONG	c_NoQuery
UNSIGNEDLONG	c_QueryOk

UNSIGNEDLONG	c_SelectOnDoubleClick
UNSIGNEDLONG	c_SelectOnClick
UNSIGNEDLONG	c_SelectOnRowFocusChange

UNSIGNEDLONG	c_NoDrillDown
UNSIGNEDLONG	c_DrillDown

UNSIGNEDLONG	c_SameModeOnSelect
UNSIGNEDLONG	c_ViewOnSelect
UNSIGNEDLONG	c_ModifyOnSelect

UNSIGNEDLONG	c_NoMultiSelect
UNSIGNEDLONG	c_MultiSelect

UNSIGNEDLONG	c_RefreshOnSelect
UNSIGNEDLONG	c_NoAutoRefresh
UNSIGNEDLONG	c_RefreshOnMultiSelect

UNSIGNEDLONG	c_ParentModeOnOpen
UNSIGNEDLONG	c_ViewOnOpen
UNSIGNEDLONG	c_ModifyOnOpen
UNSIGNEDLONG	c_NewOnOpen

UNSIGNEDLONG	c_SameModeAfterSave
UNSIGNEDLONG	c_ViewAfterSave

UNSIGNEDLONG	c_NoEnableModifyOnOpen
UNSIGNEDLONG	c_EnableModifyOnOpen

UNSIGNEDLONG	c_NoEnableNewOnOpen
UNSIGNEDLONG	c_EnableNewOnOpen

UNSIGNEDLONG	c_RetrieveOnOpen
UNSIGNEDLONG	c_AutoRetrieveOnOpen // Same as c_RetrieveOnOpen
UNSIGNEDLONG	c_NoRetrieveOnOpen

UNSIGNEDLONG	c_AutoRefresh
UNSIGNEDLONG	c_SkipRefresh
UNSIGNEDLONG	c_TriggerRefresh
UNSIGNEDLONG	c_PostRefresh

UNSIGNEDLONG	c_RetrieveAll
UNSIGNEDLONG	c_RetrieveAsNeeded

UNSIGNEDLONG	c_NoShareData
UNSIGNEDLONG	c_ShareData

UNSIGNEDLONG	c_NoRefreshParent
UNSIGNEDLONG	c_RefreshParent

UNSIGNEDLONG	c_NoRefreshChild
UNSIGNEDLONG	c_RefreshChild

UNSIGNEDLONG	c_IgnoreNewRows
UNSIGNEDLONG	c_NoIgnoreNewRows

UNSIGNEDLONG	c_NoNewModeOnEmpty
UNSIGNEDLONG	c_NewModeOnEmpty

UNSIGNEDLONG	c_MultipleNewRows
UNSIGNEDLONG	c_OnlyOneNewRow

UNSIGNEDLONG	c_DisableCC
UNSIGNEDLONG	c_EnableCC

UNSIGNEDLONG	c_DisableCCInsert
UNSIGNEDLONG	c_EnableCCInsert

UNSIGNEDLONG	c_ViewModeBorderUnchanged
UNSIGNEDLONG	c_ViewModeNoBorder
UNSIGNEDLONG	c_ViewModeShadowBox
UNSIGNEDLONG	c_ViewModeBox
UNSIGNEDLONG	c_ViewModeResize
UNSIGNEDLONG	c_ViewModeUnderline
UNSIGNEDLONG	c_ViewModeLowered
UNSIGNEDLONG	c_ViewModeRaised

UNSIGNEDLONG	c_ViewModeColorUnchanged
UNSIGNEDLONG	c_ViewModeBlack
UNSIGNEDLONG	c_ViewModeWhite
UNSIGNEDLONG	c_ViewModeGray
UNSIGNEDLONG	c_ViewModeLightGray // Same as c_ViewModeGray
UNSIGNEDLONG	c_ViewModeDarkGray
UNSIGNEDLONG	c_ViewModeRed
UNSIGNEDLONG	c_ViewModeDarkRed
UNSIGNEDLONG	c_ViewModeGreen
UNSIGNEDLONG	c_ViewModeDarkGreen
UNSIGNEDLONG	c_ViewModeBlue
UNSIGNEDLONG	c_ViewModeDarkBlue
UNSIGNEDLONG	c_ViewModeMagenta
UNSIGNEDLONG	c_ViewModeDarkMagenta
UNSIGNEDLONG	c_ViewModeCyan
UNSIGNEDLONG	c_ViewModeDarkCyan
UNSIGNEDLONG	c_ViewModeYellow
UNSIGNEDLONG	c_ViewModeBrown

UNSIGNEDLONG	c_InactiveDWColorUnchanged
UNSIGNEDLONG	c_InactiveDWBlack
UNSIGNEDLONG	c_InactiveDWWhite
UNSIGNEDLONG	c_InactiveDWGray
UNSIGNEDLONG	c_InactiveDWLightGray // Same as c_InActiveDWGray
UNSIGNEDLONG	c_InactiveDWDarkGray
UNSIGNEDLONG	c_InactiveDWRed
UNSIGNEDLONG	c_InactiveDWDarkRed
UNSIGNEDLONG	c_InactiveDWGreen
UNSIGNEDLONG	c_InactiveDWDarkGreen
UNSIGNEDLONG	c_InactiveDWBlue
UNSIGNEDLONG	c_InactiveDWDarkBlue
UNSIGNEDLONG	c_InactiveDWMagenta
UNSIGNEDLONG	c_InactiveDWDarkMagenta
UNSIGNEDLONG	c_InactiveDWCyan
UNSIGNEDLONG	c_InactiveDWDarkCyan
UNSIGNEDLONG	c_InactiveDWYellow
UNSIGNEDLONG	c_InactiveDWBrown

UNSIGNEDLONG	c_NoInactiveText
UNSIGNEDLONG	c_InactiveText
UNSIGNEDLONG	c_NoInactiveLine
UNSIGNEDLONG	c_InactiveLine
UNSIGNEDLONG	c_NoInactiveCol
UNSIGNEDLONG	c_InactiveCol

UNSIGNEDLONG	c_CursorRowFocusRect
UNSIGNEDLONG	c_NoCursorRowFocusRect

UNSIGNEDLONG	c_CursorRowPointer
UNSIGNEDLONG	c_NoCursorRowPointer

UNSIGNEDLONG	c_CalcDWStyle
UNSIGNEDLONG	c_FreeFormStyle
UNSIGNEDLONG	c_TabularFormStyle

UNSIGNEDLONG	c_AutoCalcHighlightSelected
UNSIGNEDLONG	c_HighlightSelected
UNSIGNEDLONG	c_NoHighlightSelected

UNSIGNEDLONG	c_NoResizeDW
UNSIGNEDLONG	c_ResizeDW

UNSIGNEDLONG	c_NoAutoConfigMenus
UNSIGNEDLONG	c_AutoConfigMenus

UNSIGNEDLONG	c_ShowEmpty
UNSIGNEDLONG	c_NoShowEmpty

UNSIGNEDLONG	c_AutoFocus
UNSIGNEDLONG	c_NoAutoFocus

UNSIGNEDLONG	c_CCErrorRed
UNSIGNEDLONG	c_CCErrorBlack
UNSIGNEDLONG	c_CCErrorWhite
UNSIGNEDLONG	c_CCErrorGray
UNSIGNEDLONG	c_CCErrorLightGray // Same as c_InActiveDWGray
UNSIGNEDLONG	c_CCErrorDarkGray
UNSIGNEDLONG	c_CCErrorDarkRed
UNSIGNEDLONG	c_CCErrorGreen
UNSIGNEDLONG	c_CCErrorDarkGreen
UNSIGNEDLONG	c_CCErrorBlue
UNSIGNEDLONG	c_CCErrorDarkBlue
UNSIGNEDLONG	c_CCErrorMagenta
UNSIGNEDLONG	c_CCErrorDarkMagenta
UNSIGNEDLONG	c_CCErrorCyan
UNSIGNEDLONG	c_CCErrorDarkCyan
UNSIGNEDLONG	c_CCErrorYellow
UNSIGNEDLONG	c_CCErrorBrown

UNSIGNEDLONG	c_CT_LoadCodeTable
UNSIGNEDLONG	c_CT_NoLoadCodeTable

UNSIGNEDLONG	c_CT_NoResizeCont
UNSIGNEDLONG	c_CT_ResizeCont
UNSIGNEDLONG	c_CT_ZoomCont

UNSIGNEDLONG	c_CT_ClosePromptUser
UNSIGNEDLONG	c_CT_ClosePromptUserOnce
UNSIGNEDLONG	c_CT_CloseSave
UNSIGNEDLONG	c_CT_CloseNoSave

UNSIGNEDLONG	c_CT_BringToTop
UNSIGNEDLONG	c_CT_NoBringToTop

UNSIGNEDLONG	c_MDI_AllowRedraw
UNSIGNEDLONG	c_MDI_NoAllowRedraw

UNSIGNEDLONG	c_MDI_NoShowResources
UNSIGNEDLONG	c_MDI_ShowResources

UNSIGNEDLONG	c_MDI_NoShowClock
UNSIGNEDLONG	c_MDI_ShowClock

UNSIGNEDLONG	c_MDI_NoAutoConfigMenus
UNSIGNEDLONG	c_MDI_AutoConfigMenus

UNSIGNEDLONG	c_MDI_NoSavePosition
UNSIGNEDLONG	c_MDI_SavePosition

UNSIGNEDLONG	c_MDI_ToolBarNone
UNSIGNEDLONG	c_MDI_ToolBarTop
UNSIGNEDLONG	c_MDI_ToolBarBottom
UNSIGNEDLONG	c_MDI_ToolBarLeft
UNSIGNEDLONG	c_MDI_ToolBarRight
UNSIGNEDLONG	c_MDI_ToolBarFloat

UNSIGNEDLONG	c_MDI_ToolBarHideText
UNSIGNEDLONG	c_MDI_ToolBarShowText

UNSIGNEDLONG	c_LoadCodeTable
UNSIGNEDLONG	c_NoLoadCodeTable

UNSIGNEDLONG	c_EnablePopup
UNSIGNEDLONG	c_NoEnablePopup

UNSIGNEDLONG	c_NoResizeWin
UNSIGNEDLONG	c_ResizeWin
UNSIGNEDLONG	c_ZoomWin

UNSIGNEDLONG	c_NoAutoMinimize
UNSIGNEDLONG	c_AutoMinimize

UNSIGNEDLONG	c_NoAutoSize
UNSIGNEDLONG	c_AutoSize
UNSIGNEDLONG	c_AutoSize30

UNSIGNEDLONG	c_NoAutoPosition
UNSIGNEDLONG	c_AutoPosition

UNSIGNEDLONG	c_NoSavePosition
UNSIGNEDLONG	c_SavePosition

UNSIGNEDLONG	c_ClosePromptUser
UNSIGNEDLONG	c_ClosePromptUserOnce
UNSIGNEDLONG	c_CloseSave
UNSIGNEDLONG	c_CloseNoSave

UNSIGNEDLONG	c_ToolBarNone
UNSIGNEDLONG	c_ToolBarTop
UNSIGNEDLONG	c_ToolBarBottom
UNSIGNEDLONG	c_ToolBarLeft
UNSIGNEDLONG	c_ToolBarRight
UNSIGNEDLONG	c_ToolBarFloat

UNSIGNEDLONG	c_ToolBarHideText
UNSIGNEDLONG	c_ToolBarShowText

INTEGER		c_Fatal 			= -1
INTEGER		c_Success 		= 0
UNSIGNEDLONG	c_Default 		= 0

UO_DW_MAIN	c_NullDW
UO_CB_MAIN	c_NullCB
MENU		c_NullMenu
PICTURE		c_NullPicture

STRING		c_INIUndefined 		= "[INI_Undefined]"
STRING		c_XKey 			= "X"
STRING		c_YKey 			= "Y"
STRING		c_WidthKey 		= "Width"
STRING		c_HeightKey 		= "Height"
STRING		c_WinStateKey 		= "WindowState"
INTEGER		c_MinimumOverlap 		= 25

UNSIGNEDLONG	c_Invisible 		= 0
UNSIGNEDLONG	c_Show 			= 1
UNSIGNEDLONG	c_Mark 			= 2
UNSIGNEDLONG	c_ShowMark 		= 3

LONG		c_CCDuplicateDBMSAllBase		=  0
LONG		c_CCDuplicateDBMSGupta		=  0
LONG		c_CCDuplicateDBMSDecRdb		=  0
LONG		c_CCDuplicateDBMSIBMDRDA	=  0
LONG		c_CCDuplicateDBMSInformix4	=  0
LONG		c_CCDuplicateDBMSInformix5	=  0
LONG		c_CCDuplicateDBMSMDGDB2	=  0
LONG		c_CCDuplicateDBMSOracle6		=  1
LONG		c_CCDuplicateDBMSOracle7		=  1
LONG		c_CCDuplicateDBMSSQLServer	=  2601
LONG		c_CCDuplicateDBMSSybase		=  2601
LONG		c_CCDuplicateDBMSWatcom		=  -193
LONG		c_CCDuplicateDBMSXDB		=  0
LONG		c_CCErrorDBMSAllBase		=  -3
LONG		c_CCErrorDBMSGupta		=  -3
LONG		c_CCErrorDBMSDecRdb		=  -3
LONG		c_CCErrorDBMSIBMDRDA		=  -3
LONG		c_CCErrorDBMSInformix4		=  -3
LONG		c_CCErrorDBMSInformix5		=  -3
LONG		c_CCErrorDBMSMDGDB2		=  -3
LONG		c_CCErrorDBMSOracle6		=  -3
LONG		c_CCErrorDBMSOracle7		=  -3
LONG		c_CCErrorDBMSSQLServer_1		=  -3
LONG		c_CCErrorDBMSSQLServer_2		=  532
LONG		c_CCErrorDBMSSybase_1		=  -3
LONG		c_CCErrorDBMSSybase_2		=  532
LONG		c_CCErrorDBMSWatcom		=  -3
LONG		c_CCErrorDBMSXDB		=  -3
end variables

forward prototypes
public subroutine dec_usage (window dead_window)
public subroutine dec_usage_noclose (window dead_window)
public subroutine do_posted ()
public subroutine get_ini_size ()
public subroutine inc_usage (window new_window)
public function boolean post_dw_event (uo_dw_main post_dw, string event_name)
public subroutine put_ini_size ()
public subroutine repost_events ()
public subroutine set_ct_defaults (unsignedlong control_word)
public subroutine set_dw_activate (uo_dw_main resident_dw)
public subroutine set_dw_deactivate ()
public subroutine set_dw_defaults (unsignedlong control_word, unsignedlong visual_word)
public subroutine set_ini_section (string file_section)
public subroutine set_mdi_defaults (unsignedlong control_word)
public subroutine set_w_defaults (unsignedlong control_word)
public function unsignedlong bit_and_mask (unsignedlong operand1, unsignedlong operand2)
public subroutine proc_entry (string proc_name, integer debug_dump_level)
public subroutine proc_exit ()
end prototypes

on pc_activate;//******************************************************************
//  PC Module     : w_PCManager_Main
//  Event         : pc_Activate
//  Description   : This is a posted event which takes care
//                  of setting the correct DataWindow to be
//                  the current DataWindow after all of the
//                  dust has settled from the window Activate
//                  event.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

UO_DW_MAIN  l_RequestedDW
UO_DW_MAIN  l_FocusDW

//------------------------------------------------------------------
//  Save the information we need before we reset the activation
//  information.
//------------------------------------------------------------------

l_RequestedDW = i_ActivateDW
l_FocusDW     = i_ActivateFocusDW

//------------------------------------------------------------------
//  Since we are in this event, it must have been pulled out
//  of the message queue.  Indicate that since it has been
//  received that it no longer needs processing.
//------------------------------------------------------------------

Set_DW_Deactivate()

//------------------------------------------------------------------
//  See if l_RequestedDW is still valid.  If it is not, then some
//  other PowerClass event handled setting focus to the correct
//  current DataWindow (e.g. the Clicked! event) or the window
//  was closed.
//------------------------------------------------------------------

IF IsValid(l_RequestedDW) THEN

   //---------------------------------------------------------------
   //  Find the DataWindow on the window that is to be the
   //  current DataWindow.
   //---------------------------------------------------------------

   l_RequestedDW = l_RequestedDW.Find_DW_Current()

   //---------------------------------------------------------------
   //  If we have a valid DataWindow, then we need to make it the
   //  current DataWindow.
   //---------------------------------------------------------------

   IF IsValid(l_RequestedDW) THEN

      //------------------------------------------------------------
      //  When the window got activated, it set focus to the first
      //  tab object, which may be a DataWindow.  This DataWindow
      //  was stored in i_ActivateFocusDW by the GetFocus event.
      //  If the focus has been changed (i.e. the user clicked on
      //  a command button), then we don't want to change the
      //  focus.  If it has not been changed, then we do want to
      //  change focus to the DataWindow that is to become the
      //  current DataWindow.
      //------------------------------------------------------------

      IF IsValid(l_FocusDW) THEN
         IF GetFocus() = l_FocusDW THEN

            //------------------------------------------------------
            //  Focus has not been changed from the original
            //  DataWindow that got focus when the window recieved
            //  the Activate event.  If the focus has not been set
            //  to the DataWindow that is to become the current
            //  DataWindow, we can just set focus to that
            //  DataWindow.  However, if it already has focus,
            //  SetFocus() will not do anything.  Therefore, we
            //  must trigger the GetFocus event ourselves.
            //------------------------------------------------------

            IF GetFocus() <> l_RequestedDW THEN
               l_RequestedDW.SetFocus()
            ELSE
               l_RequestedDW.TriggerEvent("GetFocus")
            END IF
         ELSE

            //------------------------------------------------------
            //  The focus has been changed from the first-tab order
            //  control to some other control.  We don't want to be
            //  rude and grab focus back.  Just make the correct
            //  DataWindow become the current DataWindow.
            //------------------------------------------------------

            l_RequestedDW.TriggerEvent("pcd_Active")
         END IF
      ELSE

         //---------------------------------------------------------
         //  The first-tab order object is not a DataWindow.
         //  Therefore we don't want to grab focus from it.
         //  Just make the correct DataWindow become the current
         //  DataWindow.
         //---------------------------------------------------------

         l_RequestedDW.TriggerEvent("pcd_Active")
      END IF
   ELSE

      //------------------------------------------------------------
      //  We couldn't find a DataWindow on this window to make
      //  the current DataWindow.  Make sure that there is not a
      //  current DataWindow.
      //------------------------------------------------------------

      IF IsValid(PCCA.Window_CurrentDW) THEN
         PCCA.Window_CurrentDW.TriggerEvent("pcd_Inactive")
      END IF
   END IF
END IF

end on

on pc_checkmdi;//******************************************************************
//  PC Module     : w_PCManager_Main
//  Event         : pc_CheckMDI
//  Description   : This event is posted by Dec_Usage().  It
//                  checks to see if the MDI Client has the
//                  focus.  If MDI Client has focus, then there
//                  are not any sheets open on the MDI Frame and
//                  the pc_MDIActive event is triggered on the
//                  MDI Frame.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

IF GetFocus() = PCCA.MDI_Client THEN
   PCCA.MDI_Frame.TriggerEvent("pc_MDIActive")
END IF

end on

on pc_exitapp;//******************************************************************
//  PC Module     : w_PCManager_Main
//  Event         : pc_ExitApp
//  Description   : This event is posted by m_Main when
//                  the user executes the EXIT menu.
//                  This event will close the window
//                  whose EXIT menu was clicked, close
//                  all other open windows, and do a
//                  HALT CLOSE.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Idx, l_NumWin
WINDOW   l_Window

//------------------------------------------------------------------
//  Bump our increment count so that this window is not closed.
//------------------------------------------------------------------

Inc_Usage(THIS)

//------------------------------------------------------------------
//  Close the Exit Window and all other known windows.
//------------------------------------------------------------------

PCCA.Error = c_Success

PCCA.MDI.fu_PushID(PCCA.MDI.c_MDI_Close, c_ShowMark)

IF IsValid(i_ExitWindow) THEN
   Close(i_ExitWindow)
END IF

IF PCCA.Error = c_Success THEN
IF i_NumWindows > 0       THEN

   //---------------------------------------------------------------
   //  Close every open window, except this one.
   //---------------------------------------------------------------

   l_NumWin = i_NumWindows - 1

   FOR l_Idx = l_NumWin TO 1 STEP -1
      IF IsValid(i_WindowList[l_Idx]) THEN
         Close(i_WindowList[l_Idx])
         IF PCCA.Error <> c_Success THEN
            i_NumWindows = l_Idx
            EXIT
         END IF
      END IF
   NEXT

   i_NumWindows = 0
END IF
END IF

PCCA.MDI.fu_Pop()

IF PCCA.Error = c_Success THEN
   Close(THIS)
   RETURN
END IF
end on

on pc_postdw;//******************************************************************
//  PC Module     : w_PCManager_Main
//  Event         : pc_PostDW
//  Description   : This is a posted event that is responsible for
//                  executing posted DataWindow events.  It pulls
//                  the DataWindow event information from the
//                  w_PCManager event queue and sets up the
//                  EventControl structure for the DataWindow
//                  and triggers the event.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

UO_DW_MAIN  l_TriggerDW

//------------------------------------------------------------------
//  Make sure that the posted event has not already been taken
//  care of.
//------------------------------------------------------------------

IF i_EndOfPostedQueue <> i_StartOfPostedQueue THEN
   i_StartOfPostedQueue = i_StartOfPostedQueue + 1
   l_TriggerDW          = i_PostDW[i_StartOfPostedQueue]

   IF IsValid(l_TriggerDW) THEN

      //------------------------------------------------------------
      //  Set up the Control structures and trigger the
      //  event.
      //------------------------------------------------------------

      l_TriggerDW.is_EventControl = &
         l_TriggerDW.is_PostEControl[i_StartOfPostedQueue]
      l_TriggerDW.is_DevControl   = &
         l_TriggerDW.is_PostDControl[i_StartOfPostedQueue]
      l_TriggerDW.TriggerEvent(i_DWEventName[i_StartOfPostedQueue])

      //------------------------------------------------------------
      //  Reset the Event Control In_Cascade_Event variable.
      //------------------------------------------------------------

      IF IsValid(l_TriggerDW) THEN
         l_TriggerDW.is_EventControl.In_Cascade_Event = FALSE
      END IF
   END IF
END IF

//------------------------------------------------------------------
//  If there are not any more events in the queue, we can reset
//  queue pointers so that the post arrays do not grow forever.
//------------------------------------------------------------------

IF i_StartOfPostedQueue = i_EndOfPostedQueue THEN
   i_StartOfPostedQueue = 0
   i_EndOfPostedQueue   = 0
END IF

end on

on pc_setwindow;//******************************************************************
//  PC Module     : w_PCManager_Main
//  Event         : pc_SetWindow
//  Description   : Initializes some pre-defined constants
//                  used with Set_DW_Options() and Set_DW_Defaults().
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

c_MasterList = c_EnableNewOnOpen        + &
               c_EnableModifyOnOpen     + &
               c_DeleteOk               + &
               c_SelectOnRowFocusChange + &
               c_DrillDown

c_MasterEdit = c_NewOk                  + &
               c_ModifyOk               + &
               c_SelectOnRowFocusChange + &
               c_DrillDown              + &
               c_ScrollParent           + &
               c_RefreshParent

c_DetailList = c_EnableNewOnOpen        + &
               c_EnableModifyOnOpen     + &
               c_DeleteOk               + &
               c_SelectOnClick          + &
               c_DrillDown              + &
               c_RefreshParent          + &
               c_RefreshChild

c_DetailEdit = c_NewOk                  + &
               c_ModifyOk               + &
               c_SelectOnRowFocusChange + &
               c_DrillDown              + &
               c_ModifyOnSelect         + &
               c_ScrollParent           + &
               c_RefreshParent          + &
               c_RefreshChild

end on

public subroutine dec_usage (window dead_window);//******************************************************************
//  PC Module     : w_PCManager_Main
//  Subroutine    : Dec_Usage
//  Description   : Informs w_PCManager that a window has closed
//                  and will no longer be accessing w_PCManager.
//                  w_PCManager decrements a usage count and when
//                  the usage count reaches zero (0), w_PCManager
//                  will close.  Dec_Usage() also checks to see if
//                  there are any open sheets left on the MDI
//                  Frame.  If there are none, then Dec_Usage()
//                  will trigger the pc_MDIActive event on the
//                  MDI Frame.
//
//  Parameters    : WINDOW Dead_Window -
//                       The window that is detaching from
//                       w_PCManager.
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Idx, l_Jdx
WINDOW   l_Window

l_Jdx = 0
FOR l_Idx = 1 TO i_NumWindows
   l_Window = i_WindowList[l_Idx]
   IF Dead_Window <> l_Window THEN
      l_Jdx  = l_Jdx + 1
      IF l_Idx <> l_Jdx THEN
         i_WindowList[l_Jdx] = l_Window
      END IF
   END IF
NEXT

i_NumWindows = l_Jdx

IF i_NumWindows = 1 AND PCCA.MDI_Frame_Valid THEN
   PostEvent("pc_CheckMDI")
ELSE
   IF i_NumWindows < 1 THEN
      Close(THIS)
      RETURN
   END IF
END IF

RETURN
end subroutine

public subroutine dec_usage_noclose (window dead_window);//******************************************************************
//  PC Module     : w_PCManager_Main
//  Subroutine    : Dec_Usage_NoClose
//  Description   : Informs w_PCManager that a window has closed
//                  and will no longer be accessing w_PCManager.
//                  w_PCManager decrements the usage count, but
//                  does not close.
//
//  Parameters    : WINDOW Dead_Window -
//                       The window that is detaching from
//                       w_PCManager.
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Idx, l_Jdx
WINDOW   l_Window

l_Jdx = 0
FOR l_Idx = 1 TO i_NumWindows
   l_Window = i_WindowList[l_Idx]
   IF Dead_Window <> l_Window THEN
      l_Jdx  = l_Jdx + 1
      IF l_Idx <> l_Jdx THEN
         i_WindowList[l_Jdx] = l_Window
      END IF
   END IF
NEXT

i_NumWindows = l_Jdx

RETURN
end subroutine

public subroutine do_posted ();//******************************************************************
//  PC Module     : w_PCManager_Main
//  Subroutine    : Do_Posted
//  Description   : Causes the events which have been posted
//                  to the DataWindow to be executed immediately.
//
//  Parameters    : (None)
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Idx

FOR l_Idx = i_StartOfPostedQueue + 1 TO i_EndOfPostedQueue
   TriggerEvent("pc_PostDW")
NEXT

RETURN
end subroutine

public subroutine get_ini_size ();//******************************************************************
//  PC Module     : w_PCManager_Main
//  Subroutine    : Get_INI_Size
//  Description   : Gets postion, size, and window state
//                  information from the INI file.
//
//  Parameters    : (None)
//
//  Return Value  : (None).  However, the following instance
//                  variables are modified:
//
//                     BOOLEAN      i_INIValidSize
//                     BOOLEAN      i_INIValidWS
//                     INTEGER      i_INIXPos
//                     INTEGER      i_INIYPos
//                     INTEGER      i_INIWidth
//                     INTEGER      i_INIHeight
//                     WINDOWSTATE  i_INIWindowState
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

STRING  l_SXPos,  l_SYPos
STRING  l_SWidth, l_SHeight, l_WinState

//------------------------------------------------------------------
//  Indictate that this routine has been called.
//------------------------------------------------------------------

i_INIGetDone = TRUE

//------------------------------------------------------------------
//  Get the window position from the INI file.
//------------------------------------------------------------------

l_SXPos    = ProfileString(PCCA.Application_INI_File, &
                           i_INIFileSection,          &
                           c_XKey,                    &
                           c_INIUndefined)
l_SYPos    = ProfileString(PCCA.Application_INI_File, &
                           i_INIFileSection,          &
                           c_YKey,                    &
                           c_INIUndefined)
l_SWidth   = ProfileString(PCCA.Application_INI_File, &
                           i_INIFileSection,          &
                           c_WidthKey,                &
                           c_INIUndefined)
l_SHeight  = ProfileString(PCCA.Application_INI_File, &
                           i_INIFileSection,          &
                           c_HeightKey,               &
                           c_INIUndefined)
l_WinState = ProfileString(PCCA.Application_INI_File, &
                           i_INIFileSection,          &
                           c_WinStateKey,             &
                           c_INIUndefined)

//------------------------------------------------------------------

i_INIValidSize = (l_SXPos   <> c_INIUndefined AND &
                  l_SYPos   <> c_INIUndefined AND &
                  l_SWidth  <> c_INIUndefined AND &
                  l_SHeight <> c_INIUndefined)

IF i_INIValidSize THEN
   i_INIXPos   = Integer(l_SXPos)
   i_INIYPos   = Integer(l_SYPos)
   i_INIWidth  = Integer(l_SWidth)
   i_INIHeight = Integer(l_SHeight)
ELSE
   i_INIXPos   = X
   i_INIYPos   = Y
   i_INIWidth  = Width
   i_INIHeight = Height
END IF

//------------------------------------------------------------------
//  Make sure that the window is not an unreasonable size and at
//  least a corner of it is on the display.
//------------------------------------------------------------------

IF i_INIWidth < c_MinimumOverlap THEN
   i_INIWidth = c_MinimumOverlap
END IF

IF i_INIHeight < c_MinimumOverlap THEN
   i_INIHeight = c_MinimumOverlap
END IF

IF i_INIXPos > PCCA.Screen_Width THEN
   i_INIXPos = PCCA.Screen_Width - c_MinimumOverlap
END IF
IF i_INIXPos + i_INIWidth < 1 THEN
   i_INIXPos = c_MinimumOverlap - i_INIWidth
END IF

IF i_INIYPos > PCCA.Screen_Height THEN
   i_INIYPos = PCCA.Screen_Height - c_MinimumOverlap
END IF
IF i_INIYPos + i_INIHeight < 1 THEN
   i_INIYPos = c_MinimumOverlap - i_INIHeight
END IF

//------------------------------------------------------------------
//  Check for window state.
//------------------------------------------------------------------

i_INIValidWS = (l_WinState <> c_INIUndefined)

IF i_INIValidWS THEN
   CHOOSE CASE l_WinState
      CASE "Maximized"
         i_INIWindowState = Maximized!
      CASE "Minimized"
         i_INIWindowState = Minimized!
      //CASE "Normal"
      CASE ELSE
         i_INIWindowState = Normal!
   END CHOOSE
ELSE
   i_INIWindowState = Normal!
END IF

RETURN
end subroutine

public subroutine inc_usage (window new_window);//******************************************************************
//  PC Module     : w_PCManager_Main
//  Subroutine    : Inc_Usage
//  Description   : Informs w_PCManager_Main that a window has opened
//                  and will be using w_PCManager_Main.  
//                  w_PCManager_Main increments a usage count to 
//                  indicate that it is being used by a new window.
//
//  Parameters    : WINDOW New_Window -
//                       The window that is attaching to
//                       w_PCManager.
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

i_NumWindows = i_NumWindows + 1
i_WindowList[i_NumWindows] = New_Window

RETURN
end subroutine

public function boolean post_dw_event (uo_dw_main post_dw, string event_name);//******************************************************************
//  PC Module     : w_PCManager_Main
//  Subroutine    : Post_DW_Event
//  Description   : Manages the posting of an event to a
//                  DataWindow.  Many DataWindow events require
//                  information from the EventControl structure.
//                  If another event runs on the DataWindow before
//                  the posted event runs, the EventControl
//                  structure will be overwritten by the time the
//                  posted event happens.  Note that the DataWindow
//                  provides a PostEvent() function so that the
//                  developer will generally not have to call this
//                  routine.
//
//  Parameters    : UO_DW_MAIN Post_DW -
//                       The DataWindow that is to be posted to.
//
//                  STRING Event_Name -
//                       The name of the DataWindow event that is
//                       to be posted.
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN  l_Return

//------------------------------------------------------------------
//  Assume failure.
//------------------------------------------------------------------

l_Return = FALSE

//------------------------------------------------------------------
//  Make sure that we got passed a valid DataWindow.
//------------------------------------------------------------------

IF IsValid(Post_DW) THEN

   //---------------------------------------------------------------
   //  Save the event information.
   //---------------------------------------------------------------

   i_EndOfPostedQueue                 = i_EndOfPostedQueue + 1
   i_DWEventName[i_EndOfPostedQueue]  = Event_Name
   i_PostDW[i_EndOfPostedQueue]       = Post_DW
   Post_DW.is_PostEControl[i_EndOfPostedQueue] = &
      Post_DW.is_EventControl
   Post_DW.is_PostDControl[i_EndOfPostedQueue] = &
      Post_DW.is_DevControl

   //---------------------------------------------------------------
   //  Reset the Control Structures.
   //---------------------------------------------------------------

   Post_DW.is_EventControl = Post_DW.is_ResetControl
   Post_DW.Reset_Dev_Control()

   //---------------------------------------------------------------
   //  Post an event to w_PCManager_Main that will set up the
   //  is_EventControl and is_DevControl for the DataWindow
   //  and trigger the event.
   //---------------------------------------------------------------

   l_Return = PostEvent("pc_PostDW")
END IF

RETURN l_Return
end function

public subroutine put_ini_size ();//******************************************************************
//  PC Module     : w_PCManager_Main
//  Subroutine    : Put_INI_Size
//  Description   : Puts the postion, size, and window state
//                  information to the INI file.
//
//  Parameters    : STRING File_Section -
//                        The section in the INI file where to
//                        place the window information.
//                        PowerClass will uses the following
//                        format to get and save window
//                        information:
//
//                           PCCA.Application_Name + &
//                              "/" + <window>.ClassName()
//
//                        Example:
//                           "PowerClass Sample/w_mdi_frame"
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

STRING  l_WinState

//------------------------------------------------------------------
//  Indictate that this routine has been called.
//------------------------------------------------------------------

i_INIPutDone = TRUE

//------------------------------------------------------------------
//  Convert WindowState to a string so that it can be stored in
//  the INI file.
//------------------------------------------------------------------

IF i_INILastWindowState = Maximized! THEN
   l_WinState = "Maximized"
ELSE
   IF i_INILastWindowState = Minimized! THEN
      l_WinState = "Minimized"
   ELSE
      l_WinState = "Normal"
   END IF
END IF

//------------------------------------------------------------------
//  Store the position, size, and window state information.
//------------------------------------------------------------------

SetProfileString(PCCA.Application_INI_File, &
                 i_INIFileSection,          &
                 c_XKey,                    &
                 String(i_INILastXPos))
SetProfileString(PCCA.Application_INI_File, &
                 i_INIFileSection,          &
                 c_YKey,                    &
                 String(i_INILastYPos))
SetProfileString(PCCA.Application_INI_File, &
                 i_INIFileSection,          &
                 c_WidthKey,                &
                 String(i_INILastWidth))
SetProfileString(PCCA.Application_INI_File, &
                 i_INIFileSection,          &
                 c_HeightKey,               &
                 String(i_INILastHeight))
SetProfileString(PCCA.Application_INI_File, &
                 i_INIFileSection,          &
                 c_WinStateKey,             &
                 l_WinState)

RETURN
end subroutine

public subroutine repost_events ();//******************************************************************
//  PC Module     : w_PCManager_Main
//  Subroutine    : Repost_Events
//  Description   : PowerBuilder may sometimes lose posted events.
//                  This routine should be called when when this
//                  occurs.
//
//  Parameters    : (None)
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Idx

IF i_ProcessingActivate THEN
   PostEvent("pc_Activate")
END IF

FOR l_Idx = i_StartOfPostedQueue + 1 TO i_EndOfPostedQueue
   PostEvent("pc_PostDW")
NEXT

RETURN
end subroutine

public subroutine set_ct_defaults (unsignedlong control_word);//******************************************************************
//  PC Module     : w_PCManager_Main
//  Subroutine    : Set_CT_Defaults
//  Description   : Sets the default options for all
//                  uo_Container_Main ancestor objects.  The
//                  developer can add additional options or
//                  override the defaults on each
//                  uo_Container_Main::Set_CT_Options() call.
//                  This routine can be called either after the
//                  Init_PCCA() call in the Application Open
//                  event, in which case all of the constants
//                  passed must be fully qualified by using
//                  "PCCA.PCMGR.<constant>".  Or the developer
//                  can call this routine in the pc_SetWindow
//                  event of w_PCManager_Std, in which case the
//                  constants do not need to be fully qualified.
//
//  Parameters    : UNSIGNEDLONG Control_Word -
//                        Specifies default options for how
//                        all objects are to behave.  These
//                        are the same options as are specifed
//                        for uo_Container_Main::Set_CT_Options().
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

PCCA.DLL.fu_SetCTDefaults (THIS, Control_Word)

RETURN
end subroutine

public subroutine set_dw_activate (uo_dw_main resident_dw);//******************************************************************
//  PC Module     : w_PCManager_Main
//  Subroutine    : Set_DW_Activate
//  Description   : When a window is activated, PowerClass will
//                  make the DataWindow which was current the
//                  last time the window was active become current
//                  again.  This routine handles the setup
//                  processing necessary for this functionality.
//                  Typically, this routine will not be called
//                  by the developer.
//
//  Parameters    : UO_DW_MAIN Resident_DW -
//                        A DataWindow which is resident on the
//                        window that is being activated.
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

//------------------------------------------------------------------
//  Our window just recieved an Activate event.  We need to
//  do the following:
//     a) Indicate that we are now processing an activate event.
//     b) Indicate that the activate event has just happened.
//     c) Remember the DataWindow that that resides on the
//        window that is being activated.
//     d) Indicate that SetFocus() has not been called on any
//        DataWindow.  The window will try later to set focus
//        to the first tab order object.
//------------------------------------------------------------------

i_ProcessingActivate = TRUE
i_JustActivated      = TRUE
i_ActivateDW         = Resident_DW
i_ActivateFocusDW    = PCCA.Null_Object

//------------------------------------------------------------------
//  Post the initial pc_Activate event.  Others may get posted
//  later, but that's Ok.
//------------------------------------------------------------------

PostEvent("pc_Activate")

RETURN
end subroutine

public subroutine set_dw_deactivate ();//******************************************************************
//  PC Module     : w_PCManager_Main
//  Subroutine    : Set_DW_Deactivate
//  Description   : When a window is activated, PowerClass will
//                  make the DataWindow which was current the
//                  last time the window was active become current
//                  again.  However, sometimes a window will
//                  become activated and then de-activated without
//                  any DataWindows ever being made current.  A
//                  common example of this is when the application
//                  closes.  Another example is when the user
//                  clicks on a DataWindow and changes the current
//                  DataWindow directly.  This routine will defuse
//                  the processing that PowerClass does to
//                  restore the current DataWindow.  Typically,
//                  this routine will not be called by the
//                  developer.
//
//  Parameters    : (None)
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

//------------------------------------------------------------------
//  Clear the activate variables.
//------------------------------------------------------------------

i_ProcessingActivate = FALSE
i_JustActivated      = FALSE
i_ActivateDW         = PCCA.Null_Object
i_ActivateFocusDW    = PCCA.Null_Object

RETURN
end subroutine

public subroutine set_dw_defaults (unsignedlong control_word, unsignedlong visual_word);//******************************************************************
//  PC Module     : w_PCManager_Main
//  Subroutine    : Set_DW_Defaults
//  Description   : Sets the default options for all calls
//                  to uo_DW_Main::Set_DW_Options.  The
//                  developer can specify additional options
//                  or override the defaults on each
//                  individual Set_DW_Options() call.  This
//                  routine can be called either after the
//                  Init_PCCA() call in the Application Open
//                  event, in which case all of the constants
//                  passed must be fully qualified by using
//                  "PCCA.PCMGR.<constant>".  Or the developer
//                  can call this routine in the pc_SetWindow
//                  event of w_PCManager_Std, in which case the
//                  constants do not need to be fully qualified.
//
//  Parameters    : UNSIGNEDLONG Control_Word -
//                        Specifies default options for how
//                        all DataWindows are to behave.  These
//                        are the same options as are specifed
//                        for uo_DW_Main::Set_DW_Options().
//
//                  UNSIGNEDLONG Visual_Word -
//                        Specifies default options for how
//                        all DataWindows are to be displayed.
//                        These are the same options as are
//                        specifed for
//                        uo_DW_Main::Set_DW_Options().
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

PCCA.DLL.fu_SetDWDefaults (THIS, Control_Word, Visual_Word)

RETURN
end subroutine

public subroutine set_ini_section (string file_section);//******************************************************************
//  PC Module     : w_PCManager_Main
//  Subroutine    : Set_INI_Section
//  Description   : Sets the file section that the window's
//                  size and position will be saved to in
//                  the INI file.
//
//  Parameters    : STRING File_Section -
//                        The section in the INI file where to
//                        place the window information.
//                        By default, PowerClass will use the
//                        following format if the developer does
//                        not call this routine:
//
//                           PCCA.Application_Name + &
//                              "/" + <window>.ClassName()
//
//                        Example:
//                           "PowerClass Sample/w_mdi_frame"
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

i_INIFileSection = File_Section

RETURN
end subroutine

public subroutine set_mdi_defaults (unsignedlong control_word);//******************************************************************
//  PC Module     : w_PCManager_Main
//  Subroutine    : Set_MDI_Defaults
//  Description   : Sets the default options for all
//                  w_MDI_Frame ancestor windows (probably a
//                  total of one in the application).  The
//                  developer can add additional options or
//                  override the defaults on each individual
//                  w_MDI_Frame::Set_MDI_Options() call.  This
//                  routine can be called either after the
//                  Init_PCCA() call in the Application Open
//                  event, in which case all of the constants
//                  passed must be fully qualified by using
//                  "PCCA.PCMGR.<constant>".  Or the developer
//                  can call this routine in the pc_SetWindow
//                  event of w_PCManager_Std, in which case the
//                  constants do not need to be fully qualified.
//
//  Parameters    : UNSIGNEDLONG Control_Word -
//                        Specifies default options for how
//                        all MDI Frames are to behave. These
//                        are the same options as are specifed
//                        for w_MDI_Frame::Set_MDI_Options().
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

PCCA.DLL.fu_SetMDIDefaults (THIS, Control_Word)

RETURN
end subroutine

public subroutine set_w_defaults (unsignedlong control_word);//******************************************************************
//  PC Module     : w_PCManager_Main
//  Subroutine    : Set_W_Defaults
//  Description   : Sets the default options for all w_Main/
//                  w_Response ancestor windows.  The developer
//                  can add additional options or override the
//                  defaults on each w_Main::Set_W_Options() or
//                  w_Response::Set_W_Options() call.  This
//                  routine can be called either after the
//                  Init_PCCA() call in the Application Open
//                  event, in which case all of the constants
//                  passed must be fully qualified by using
//                  "PCCA.PCMGR.<constant>".  Or the developer
//                  can call this routine in the pc_SetWindow
//                  event of w_PCManager_Std, in which case the
//                  constants do not need to be fully qualified.
//
//  Parameters    : UNSIGNEDLONG Control_Word -
//                        Specifies default options for how
//                        all windows are to behave.  These
//                        are the same options as are specifed
//                        for w_Main::Set_W_Options().
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

PCCA.DLL.fu_SetWDefaults (THIS, Control_Word)

RETURN
end subroutine

public function unsignedlong bit_and_mask (unsignedlong operand1, unsignedlong operand2);RETURN 0
end function

public subroutine proc_entry (string proc_name, integer debug_dump_level);
end subroutine

public subroutine proc_exit ();
end subroutine

on open;//******************************************************************
//  PC Module     : w_PCManager_Main
//  Event         : Open
//  Description   : This event performs initialization of
//                  variables and triggers the pc_SetWindow
//                  event to allow the developer to set default
//                  options for PowerClass objects.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

//------------------------------------------------------------------
//  Initialize the class name identifier.
//------------------------------------------------------------------

i_ClassName = ClassName()

//------------------------------------------------------------------
//  Set ourselves into the PCCA.
//------------------------------------------------------------------

PCCA.PCMGR = THIS

//------------------------------------------------------------------
//  Make sure that we are invisible.
//------------------------------------------------------------------

Visible = FALSE

//------------------------------------------------------------------
//  Set the INI file section to a reasonable default.
//------------------------------------------------------------------

i_INIFileSection = PCCA.Application_Name + "/" + i_ClassName

//------------------------------------------------------------------
//  Initialize the control words.
//------------------------------------------------------------------

PCCA.DLL.fu_InitDefaultBits()

//------------------------------------------------------------------
//  Initialize NULL objects for use by the developer.
//------------------------------------------------------------------

c_NullDW      = PCCA.Null_Object
c_NullCB      = PCCA.Null_Object
c_NullMenu    = PCCA.Null_Object
c_NullPicture = PCCA.Null_Object

//------------------------------------------------------------------
//  Initialize the activation variables.
//------------------------------------------------------------------

i_ActivateDW      = PCCA.Null_Object
i_ActivateFocusDW = PCCA.Null_Object

//------------------------------------------------------------------
//  The pc_SetWindow event of w_PCManager is used to do set
//  the PowerClass defaults for the application.  This event
//  is coded by the developer.
//------------------------------------------------------------------

TriggerEvent("pc_SetWindow")

end on

on close;//******************************************************************
//  PC Module     : w_PCManager_Main
//  Event         : Close
//  Description   : Closes this window and halts the
//                  application via HALT CLOSE.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

HALT CLOSE
end on

on w_pcmanager_main.create
this.st_1=create st_1
this.Control[]={ this.st_1}
end on

on w_pcmanager_main.destroy
destroy(this.st_1)
end on

type st_1 from statictext within w_pcmanager_main
int X=23
int Y=25
int Width=691
int Height=201
boolean Enabled=false
boolean Border=true
BorderStyle BorderStyle=StyleRaised!
string Text="PowerClass Manager"
Alignment Alignment=Center!
boolean FocusRectangle=false
long BackColor=12632256
int TextSize=-16
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

