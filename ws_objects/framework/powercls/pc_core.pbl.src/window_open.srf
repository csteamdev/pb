﻿$PBExportHeader$window_open.srf
$PBExportComments$Opens or refreshes a window
global type window_open from function_object
end type

forward prototypes
global function integer window_open (ref window window_name, integer menu_position)
end prototypes

global function integer window_open (ref window window_name, integer menu_position);//******************************************************************
//  PC Module     : Window_Open
//  Function      : Window_Open
//  Description   : Open or refresh a window.  Do NOT use this
//                  to open a MDI frame or an initial window.
//
//  Parameters    : WINDOW Window_Name -
//                       Name of the window to open function or
//                       refresh.
//
//                  INTEGER Menu_Position-
//                        Number of the menu item (in the menu
//                        associated with the window) to which
//                        you want to append the name of the
//                        open window.  The ranges of
//                        Menu_Position have the following
//                        meanings:
//                           > 0: Use OpenSheet*() passing
//                                Menu_Position as the menu
//                                position.
//                           = 0: Use Open*().
//                           < 0: Use OpenSheet*() passing
//                                0 as the menu position.
//                           
//  Return Value  : INTEGER -
//                        Error return: 0 = OK, -1 = Error
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1993.  All Rights Reserved.
//******************************************************************

BOOLEAN  l_WindowExists
INTEGER  l_Error,    l_NumWin, l_Idx
LONG     l_PromptID
STRING   c_ThisProcName = "::Window_Open()"


if menu_position<0 then menu_position=6

//------------------------------------------------------------------
//  Initialize the error return and turn the hour glass on.
//------------------------------------------------------------------

l_Error = 0
SetPointer(HourGlass!)

//------------------------------------------------------------------
//  There is not a PowerObject parameter, so set PCCA.Open_Parm
//  to NULL.
//------------------------------------------------------------------

PCCA.Open_Parm = PCCA.Null_Object

//------------------------------------------------------------------
//  Loop through the array of windows to see if the current window
//  is already open.
//------------------------------------------------------------------

l_WindowExists = FALSE
FOR l_Idx = 1 TO PCCA.Num_Windows
   IF Window_Name = PCCA.Window_List[l_Idx] THEN

      //------------------------------------------------------------
      //  Make sure that the window is valid.  If it is not,
      //  remove it from our array of open windows and re-open
      //  it.
      //------------------------------------------------------------

      IF IsValid(Window_Name) THEN
         l_WindowExists = TRUE
      ELSE
         Window_Close(Window_Name)
      END IF
      EXIT
   END IF
NEXT

IF NOT l_WindowExists THEN

   //---------------------------------------------------------------
   //  Grab the time.
   //---------------------------------------------------------------

   f_PO_TimerMark("Enter " + c_ThisProcName)

   //---------------------------------------------------------------
   //  The Open event of the window is should pop this prompt.
   //  This is especially true if this is a Response! window
   //  because you shouldn't have to wait until the user has
   //  done all their work and closed the window before the
   //  "Open Window" prompt gets removed.  However, we will keep
   //  the serial number for the pushed message and if it isn't
   //  gone by the time we return from the Open, we'll clean it
   //  up for the developer.
   //---------------------------------------------------------------

   l_PromptID = PCCA.MDI.fu_PushID &
                   (PCCA.MDI.c_MDI_Open, PCCA.MDI.c_Show)

   //---------------------------------------------------------------
   //  We are opening a new window, so generate a new window ID.
   //---------------------------------------------------------------

   PCCA.PCMGR.i_WindowID = PCCA.PCMGR.i_WindowID + 1

   //---------------------------------------------------------------
   //  If we are in an MDI environment, then turn redraw off so
   //  that the toolbar does not flash as we open the window.  If
   //  the developer is using this routine with a non-w_Main
   //  window, they will probably want to turn redraw on inside
   //  their Open event.  This is especially true if the window is
   //  a Response! window.
   //---------------------------------------------------------------

   IF PCCA.MDI_Frame_Valid  THEN
   IF PCCA.MDI_Allow_Redraw THEN
      PCCA.MDI_Frame.SetRedraw(FALSE)
   END IF
   END IF

   //---------------------------------------------------------------
   //  It would nice to see if the WindowType is not Response!,
   //  so that we wouldn't have to put it in the array of windows.
   //  But we can't since the window has not yet been created.
   //---------------------------------------------------------------

   IF PCCA.MDI_Frame_Valid AND Menu_Position <> 0 THEN

      //------------------------------------------------------------
      //  Current window is NOT open.  Increment the global window
      //  count by 1.  Because the array must be the specified
      //  size, put a NULL object in.
      //------------------------------------------------------------

      l_NumWin                           = PCCA.Num_Windows
      PCCA.Num_Windows                   = PCCA.Num_Windows + 1
      PCCA.Window_List[PCCA.Num_Windows] = PCCA.Null_Object
      PCCA.Window_X[PCCA.Num_Windows]    = 0
      PCCA.Window_Y[PCCA.Num_Windows]    = 0

      //------------------------------------------------------------
      //  If Menu_Position is less than 0, then the developer
      //  wanted the window opened as sheet, but without a
      //  a menu position.
      //------------------------------------------------------------

      IF Menu_Position < 0 THEN
         Menu_Position = 0
      END IF

      //------------------------------------------------------------
      //  If an MDI environment is used, OpenSheet the current
      //  window, else Open it.
      //------------------------------------------------------------

      PCCA.Is_Sheet = TRUE

      l_Error = OpenSheet           &
                   (Window_Name,    &
                    PCCA.MDI_Frame, &
                    Menu_Position,  &
                    Original!)

      IF l_Error <> 1 THEN
         l_Error          = -1
         PCCA.Num_Windows = PCCA.Num_Windows - 1
         FOR l_Idx = PCCA.Num_Windows TO l_NumWin
            PCCA.Window_List[l_Idx] = PCCA.Window_List[l_Idx + 1]
            PCCA.Window_X[l_Idx]    = PCCA.Window_X[l_Idx + 1]
            PCCA.Window_Y[l_Idx]    = PCCA.Window_Y[l_Idx + 1]
         NEXT
      ELSE
         l_Error = 0

         IF IsValid(Window_Name) THEN

            //------------------------------------------------------
            //  Add the current window to the global window array.
            //  This will be used to close the window at a later
            //  time.
            //------------------------------------------------------

           PCCA.Window_List[PCCA.Num_Windows] = Window_Name

           //-------------------------------------------------------
           //  If the window is a Response! type (it shouldn't be
           //  if the developer is doing things correctly), then
           //  get it out of the global array of windows.
           //-------------------------------------------------------

           IF Window_Name.WindowType = Response! THEN
              Window_Close(Window_Name)
           END IF
         ELSE

            //------------------------------------------------------
            //  In case the window closed itself.
            //------------------------------------------------------

            PCCA.Num_Windows = PCCA.Num_Windows - 1
            FOR l_Idx = PCCA.Num_Windows TO l_NumWin
               PCCA.Window_List[l_Idx] = PCCA.Window_List[l_Idx + 1]
               PCCA.Window_X[l_Idx]    = PCCA.Window_X[l_Idx + 1]
               PCCA.Window_Y[l_Idx]    = PCCA.Window_Y[l_Idx + 1]
            NEXT
         END IF
      END IF
   ELSE

      //------------------------------------------------------------
      //  If the menu position is 0 or no MDI frame exists, use an
      //  Open instead of an OpenSheet.
      //------------------------------------------------------------

      PCCA.Is_Sheet = FALSE

      l_Error = Open(Window_Name, PCCA.PCMGR)

      IF l_Error <> 1 THEN
         l_Error = -1
      ELSE
         l_Error = 0
      END IF
   END IF

   //---------------------------------------------------------------
   //  Make sure that the "Opening a new window" prompt got
   //  removed.
   //---------------------------------------------------------------

   PCCA.MDI.fu_PopSerialID(l_PromptID)

   //---------------------------------------------------------------
   //  If we are in an MDI environment, restore redraw.  Note that
   //  the window may have already done this.
   //---------------------------------------------------------------

   IF PCCA.MDI_Frame_Valid  THEN
   IF PCCA.MDI_Allow_Redraw THEN
      PCCA.MDI_Frame.SetRedraw(TRUE)
   END IF
   END IF

ELSE
   IF PCCA.Raise_Open_Window THEN
      IF Window_Name.WindowState = Minimized! THEN
         Window_Name.WindowState = Normal!
      ELSE
         Window_Name.SetFocus()
      END IF
   END IF
   Window_Name.TriggerEvent("pc_SetVariables")
END IF

//------------------------------------------------------------------
//  Return the error code.
//------------------------------------------------------------------

RETURN l_Error
end function

