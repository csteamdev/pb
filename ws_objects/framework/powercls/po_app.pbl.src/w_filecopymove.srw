﻿$PBExportHeader$w_filecopymove.srw
$PBExportComments$Window to indicate that a copy or move is processing
forward
global type w_filecopymove from Window
end type
type st_to_file_t from statictext within w_filecopymove
end type
type st_from_file_t from statictext within w_filecopymove
end type
type st_to_file from statictext within w_filecopymove
end type
type st_from_file from statictext within w_filecopymove
end type
type cb_cancel from commandbutton within w_filecopymove
end type
end forward

shared variables

end variables

global type w_filecopymove from Window
int X=485
int Y=621
int Width=1806
int Height=521
boolean TitleBar=true
string Title="Copying..."
long BackColor=12632256
boolean ControlMenu=true
WindowType WindowType=response!
event po_copymove pbm_custom75
st_to_file_t st_to_file_t
st_from_file_t st_from_file_t
st_to_file st_to_file
st_from_file st_from_file
cb_cancel cb_cancel
end type
global w_filecopymove w_filecopymove

type variables
BOOLEAN	i_CancelCopyMove = FALSE

end variables

on po_copymove;//******************************************************************
//  PO Module     : w_FileCopyMove
//  Event         : po_CopyMove
//  Description   : Copies/Moves files.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

INTEGER l_Idx, l_NumFiles
STRING  l_FromFile, l_ToFile

//------------------------------------------------------------------
//  Before each loop, check to see if the user pressed the Cancel
//  button.
//------------------------------------------------------------------

l_NumFiles = UpperBound(w_POManager_STD.PARM.Files[])

FOR l_Idx = 1 TO l_NumFiles
   Yield()
   IF i_CancelCopyMove THEN
      w_POManager_STD.PARM.Answer = -1
      Close(THIS)
   END IF

   l_FromFile = w_POManager_STD.PARM.From_Directory + &
                w_POManager_STD.PARM.Files[l_Idx]
   l_ToFile   = w_POManager_STD.PARM.To_Directory + &
                w_POManager_STD.PARM.Files[l_Idx]
   st_From_File_t.text = UPPER(l_FromFile)
   st_To_File_t.text   = UPPER(l_ToFile)
   IF w_POManager_STD.PARM.Copy_File THEN
      w_POManager_STD.EXT.fu_FileCopy(l_FromFile, l_ToFile)
   ELSE
      w_POManager_STD.EXT.fu_FileMove(l_FromFile, l_ToFile)
   END IF
NEXT

w_POManager_STD.PARM.Answer = 0
Close(THIS)
end on

on open;//******************************************************************
//  PO Module     : w_FileCopyMove
//  Event         : Open
//  Description   : Allows processing before window becomes visible.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

w_POManager_STD.PARM.Answer = -1

//------------------------------------------------------------------
//  Set the window attributes.
//------------------------------------------------------------------

BackColor                = w_POManager_STD.i_WindowColor

st_From_File.BackColor   = w_POManager_STD.i_WindowColor
st_From_File.TextColor   = w_POManager_STD.i_WindowTextColor
st_From_File.FaceName    = w_POManager_STD.i_WindowTextFont
st_From_File.TextSize    = w_POManager_STD.i_WindowTextSize

st_From_File_t.BackColor = w_POManager_STD.i_WindowColor
st_From_File_t.FaceName  = w_POManager_STD.i_WindowTextFont
st_From_File_t.TextSize  = w_POManager_STD.i_WindowTextSize

st_To_File.BackColor     = w_POManager_STD.i_WindowColor
st_To_File.TextColor     = w_POManager_STD.i_WindowTextColor
st_To_File.FaceName      = w_POManager_STD.i_WindowTextFont
st_To_File.TextSize      = w_POManager_STD.i_WindowTextSize

st_To_File_t.BackColor   = w_POManager_STD.i_WindowColor
st_To_File_t.FaceName    = w_POManager_STD.i_WindowTextFont
st_To_File_t.TextSize    = w_POManager_STD.i_WindowTextSize

cb_cancel.FaceName       = w_POManager_STD.i_WindowTextFont
cb_cancel.TextSize       = w_POManager_STD.i_WindowTextSize

//------------------------------------------------------------------
//  Set the title and from file text to indicate a move is to be
//  done instead of a copy.
//------------------------------------------------------------------

IF NOT w_POManager_STD.PARM.Copy_File THEN
   Title = "Moving..."
   st_From_File.text = " Moving:"
END IF

//------------------------------------------------------------------
//  Position the window in the center.
//------------------------------------------------------------------

f_PO_SetWindowPosition(THIS)

//------------------------------------------------------------------
//  Copy/Move files.
//------------------------------------------------------------------

IF w_POManager_STD.PARM.display THEN
   PostEvent("po_CopyMove")
ELSE
   TriggerEvent("po_CopyMove")
END IF
end on

on w_filecopymove.create
this.st_to_file_t=create st_to_file_t
this.st_from_file_t=create st_from_file_t
this.st_to_file=create st_to_file
this.st_from_file=create st_from_file
this.cb_cancel=create cb_cancel
this.Control[]={ this.st_to_file_t,&
this.st_from_file_t,&
this.st_to_file,&
this.st_from_file,&
this.cb_cancel}
end on

on w_filecopymove.destroy
destroy(this.st_to_file_t)
destroy(this.st_from_file_t)
destroy(this.st_to_file)
destroy(this.st_from_file)
destroy(this.cb_cancel)
end on

type st_to_file_t from statictext within w_filecopymove
int X=412
int Y=141
int Width=1317
int Height=77
boolean Enabled=false
string Text="none"
boolean FocusRectangle=false
long BackColor=12632256
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_from_file_t from statictext within w_filecopymove
int X=412
int Y=53
int Width=1317
int Height=77
boolean Enabled=false
string Text="none"
boolean FocusRectangle=false
long BackColor=12632256
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_to_file from statictext within w_filecopymove
int X=65
int Y=141
int Width=339
int Height=77
boolean Enabled=false
boolean DragAuto=true
string Text=" To:"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_from_file from statictext within w_filecopymove
int X=65
int Y=53
int Width=339
int Height=77
boolean Enabled=false
boolean DragAuto=true
string Text=" Copying:"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_cancel from commandbutton within w_filecopymove
int X=1340
int Y=281
int Width=330
int Height=93
int TabOrder=10
string Text=" Cancel"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;//******************************************************************
//  PO Module     : w_FileCopyMove.cb_Cancel
//  Event         : Clicked
//  Description   : Cancel file move/copy operations.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

i_CancelCopyMove = TRUE

end on

