﻿$PBExportHeader$w_timer.srw
$PBExportComments$Window to display the results of a timing session
forward
global type w_timer from Window
end type
type cb_printtimes from commandbutton within w_timer
end type
type rb_calls from radiobutton within w_timer
end type
type rb_seq from radiobutton within w_timer
end type
type cb_savetimes from commandbutton within w_timer
end type
type cb_close from commandbutton within w_timer
end type
type dw_calls from datawindow within w_timer
end type
type dw_seq from datawindow within w_timer
end type
type gb_summary from groupbox within w_timer
end type
end forward

global type w_timer from Window
int X=567
int Y=301
int Width=1788
int Height=1309
boolean TitleBar=true
string Title="Timer Report"
long BackColor=12632256
WindowType WindowType=response!
cb_printtimes cb_printtimes
rb_calls rb_calls
rb_seq rb_seq
cb_savetimes cb_savetimes
cb_close cb_close
dw_calls dw_calls
dw_seq dw_seq
gb_summary gb_summary
end type
global w_timer w_timer

type variables
STRING	i_Directory = ""
end variables

on open;//******************************************************************
//  PO Module     : w_Timer
//  Event         : Open
//  Description   : Displays timing information.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER     l_RowCnt,     l_NumRet,     l_Idx,   l_Jdx
DECIMAL{2}  l_SubElapsed, l_TotElapsed, l_Total, l_Percent
DECIMAL{2}  l_RetTime[],  l_Sum
STRING      l_RetText[]
TIME        l_FirstT,     l_SecondT

//------------------------------------------------------------------
//  Set the title for the window.
//------------------------------------------------------------------

IF w_POManager_STD.PARM.Title <> "" THEN
   Title = w_POManager_STD.PARM.Title
END IF

//------------------------------------------------------------------
//  Loop through the timer array and calculate the intervals and
//  sub-intervals.  Then load the datawindows with the timings.
//------------------------------------------------------------------

IF w_POManager_Std.i_TimerCnt > 0 THEN
   l_Total  = f_po_TimerElapsed(w_POManager_Std.i_TimerTime[1], &
                 w_POManager_Std.i_TimerTime[w_POManager_Std.i_TimerCnt])
   l_RowCnt = 0
   l_Sum    = 0

   l_Idx = 2
   DO WHILE l_Idx <= w_POManager_Std.i_TimerCnt

      IF Len(Trim(w_POManager_Std.i_TimerLabel[l_Idx])) > 0 THEN
         l_NumRet     = 0
         l_TotElapsed = 0

         l_FirstT     = w_POManager_Std.i_TimerTime[l_Idx - 1]
         l_SecondT    = w_POManager_Std.i_TimerTime[l_Idx]
         l_SubElapsed = f_po_TimerElapsed(l_FirstT, l_SecondT)
         l_TotElapsed = l_TotElapsed + l_SubElapsed
         l_Sum        = l_Sum        + l_SubElapsed

         IF w_POManager_Std.i_TimerLabel[l_Idx] = "SUBHEAD" THEN
            DO WHILE w_POManager_Std.i_TimerLabel[l_Idx] = "SUBHEAD"
               l_Idx     = l_Idx + 1
               l_FirstT  = w_POManager_Std.i_TimerTime[l_Idx - 1]
               l_SecondT = w_POManager_Std.i_TimerTime[l_Idx]
               l_NumRet  = l_NumRet + 1
               l_RetTime[l_NumRet] = f_po_TimerElapsed(l_FirstT, l_SecondT)
               l_RetText[l_NumRet] = w_POManager_Std.i_TimerLabel[l_Idx]

               l_Idx        = l_Idx + 1
               l_FirstT     = w_POManager_Std.i_TimerTime[l_Idx - 1]
               l_SecondT    = w_POManager_Std.i_TimerTime[l_Idx]
               l_SubElapsed = f_po_TimerElapsed(l_FirstT, l_SecondT)
               l_TotElapsed = l_TotElapsed + l_SubElapsed
               l_Sum        = l_Sum        + l_SubElapsed
            LOOP
         END IF               

         dw_seq.InsertRow(0)
         dw_calls.InsertRow(0)

         l_RowCnt = l_RowCnt + 1
         IF l_Total > 0 THEN
            l_Percent = 100.0 * l_TotElapsed / l_Total
         ELSE
            l_Percent = 0.0
         END IF

         dw_seq.SetItem(l_RowCnt, "process", &
                        " " + TRIM(w_POManager_Std.i_TimerLabel[l_Idx]))
         dw_seq.SetItem(l_RowCnt, "elapsed", l_TotElapsed)
         dw_seq.SetItem(l_RowCnt, "percent", l_Percent)
         dw_seq.SetItem(l_RowCnt, "total", l_Sum)

         dw_calls.SetItem(l_RowCnt, "process", &
                          " " + TRIM(w_POManager_Std.i_TimerLabel[l_Idx]))
         dw_calls.SetItem(l_RowCnt, "elapsed", l_TotElapsed)

         FOR l_Jdx = 1 TO l_NumRet         
            dw_seq.InsertRow(0)
            dw_calls.InsertRow(0)

            l_RowCnt     = l_RowCnt + 1
            l_SubElapsed = l_RetTime[l_Jdx]
            l_TotElapsed = l_SubElapsed
            l_Sum        = l_Sum + l_SubElapsed
            IF l_Total > 0 THEN
               l_Percent = 100.0 * l_TotElapsed / l_Total
            ELSE
               l_Percent = 0
            END IF

            dw_seq.SetItem(l_RowCnt, "process", "    " + TRIM(l_RetText[l_Jdx]))
            dw_seq.SetItem(l_RowCnt, "elapsed", l_TotElapsed)
            dw_seq.SetItem(l_RowCnt, "percent", l_Percent)
            dw_seq.SetItem(l_RowCnt, "total", l_Sum)

            dw_calls.SetItem(l_RowCnt, "process", "    " + TRIM(l_RetText[l_Jdx]))
            dw_calls.SetItem(l_RowCnt, "elapsed", l_TotElapsed)
         NEXT      
      END IF

      l_Idx = l_Idx + 1
   LOOP

   w_POManager_Std.i_TimerCnt = 0
END IF

//------------------------------------------------------------------
//  Reposition the window in the center of the screen.
//------------------------------------------------------------------

f_PO_SetWindowPosition(THIS)

end on

on w_timer.create
this.cb_printtimes=create cb_printtimes
this.rb_calls=create rb_calls
this.rb_seq=create rb_seq
this.cb_savetimes=create cb_savetimes
this.cb_close=create cb_close
this.dw_calls=create dw_calls
this.dw_seq=create dw_seq
this.gb_summary=create gb_summary
this.Control[]={ this.cb_printtimes,&
this.rb_calls,&
this.rb_seq,&
this.cb_savetimes,&
this.cb_close,&
this.dw_calls,&
this.dw_seq,&
this.gb_summary}
end on

on w_timer.destroy
destroy(this.cb_printtimes)
destroy(this.rb_calls)
destroy(this.rb_seq)
destroy(this.cb_savetimes)
destroy(this.cb_close)
destroy(this.dw_calls)
destroy(this.dw_seq)
destroy(this.gb_summary)
end on

type cb_printtimes from commandbutton within w_timer
int X=901
int Y=1065
int Width=366
int Height=93
int TabOrder=50
string Text=" &Print Timing"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;//******************************************************************
//  PO Module     : w_Timer.cb_PrintTimes
//  Event         : Clicked
//  Description   : Print the timings to the current printer.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

IF rb_seq.Checked THEN
   dw_seq.SetReDraw(FALSE)
   dw_seq.TitleBar = TRUE
   dw_seq.Title = Parent.Title + " - " + String(Today(), "m/d/yy")
   dw_seq.Print()
   dw_seq.TitleBar = FALSE
   dw_seq.SetReDraw(TRUE)
ELSE
   dw_calls.SetReDraw(FALSE)
   dw_calls.TitleBar = TRUE
   dw_calls.Title = Parent.Title + " - " + String(Today(), "m/d/yy")
   dw_calls.Print()
   dw_calls.TitleBar = FALSE
   dw_calls.SetReDraw(TRUE)
END IF
end on

type rb_calls from radiobutton within w_timer
int X=540
int Y=1025
int Width=261
int Height=73
string Text="By Call"
BorderStyle BorderStyle=StyleLowered!
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;//******************************************************************
//  PO Module     : w_Timer.rb_Calls
//  Event         : Clicked
//  Description   : Display timing information by the number of
//                  times an interval was called.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

dw_calls.Show()
dw_seq.Hide()
Parent.Width = 1926
end on

type rb_seq from radiobutton within w_timer
int X=83
int Y=1025
int Width=435
int Height=73
string Text="By Sequence"
BorderStyle BorderStyle=StyleLowered!
boolean Checked=true
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;//******************************************************************
//  PO Module     : w_Timer.rb_Seq
//  Event         : Clicked
//  Description   : Display timing information by sequence.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

dw_seq.Show()
dw_calls.Hide()
Parent.Width = 1788
end on

type cb_savetimes from commandbutton within w_timer
int X=901
int Y=957
int Width=366
int Height=93
int TabOrder=40
string Text=" &Save Timing"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;//******************************************************************
//  PO Module     : w_Timer.cb_SaveTimes
//  Event         : Clicked
//  Description   : Save the timings into a file.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

IF rb_seq.Checked THEN
   f_PO_FileSaveAs(dw_seq, i_Directory)
ELSE
   f_PO_FileSaveAs(dw_calls, i_Directory)
END IF
end on

type cb_close from commandbutton within w_timer
int X=1335
int Y=1009
int Width=366
int Height=93
int TabOrder=30
string Text="&Close"
boolean Default=true
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;//******************************************************************
//  PO Module     : w_Timer.cb_Close
//  Event         : Clicked
//  Description   : Closes the window.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

Close(PARENT)
end on

type dw_calls from datawindow within w_timer
int X=42
int Y=33
int Width=1802
int Height=841
int TabOrder=10
boolean Visible=false
string DataObject="d_timer_call"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

type dw_seq from datawindow within w_timer
int X=42
int Y=33
int Width=1655
int Height=841
int TabOrder=20
string DataObject="d_timer_seq"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

type gb_summary from groupbox within w_timer
int X=37
int Y=929
int Width=810
int Height=229
int TabOrder=60
string Text="Summary"
BorderStyle BorderStyle=StyleLowered!
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

