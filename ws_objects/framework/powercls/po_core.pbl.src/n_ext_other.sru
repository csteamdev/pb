﻿$PBExportHeader$n_ext_other.sru
$PBExportComments$Provides OS interface for all other supported platforms
forward
global type n_ext_other from n_ext_main
end type
end forward

type rect from structure
    integer left
    integer top
    integer right
    integer bottom
end type

global type n_ext_other from n_ext_main
end type
global n_ext_other n_ext_other

type prototypes

end prototypes

type variables
//------------------------------------------------------------------------------
//  Temporary handle for doing work - don't want to 
//  spread handles all over the code.
//------------------------------------------------------------------------------

UNSIGNEDINT  i_TmpHandle


end variables

forward prototypes
public function unsignedlong fu_bitandmask (unsignedlong lmask1, unsignedlong lmask2)
public subroutine fu_getbitmap (long lbitmapnumber, ref blob lpucbitmap, ref long lwidth, ref long lheight)
public function integer fu_filecopy (string from_file, string to_file)
public function integer fu_filemove (string from_file, string to_file)
public subroutine fu_getframesize (ref integer frame_width, ref integer frame_height)
public subroutine fu_getscreensize (ref long screen_width, ref long screen_height)
public subroutine fu_releasecapture ()
public subroutine fu_getwindowinfo (ref integer title_height, ref integer control_menu_width)
public subroutine fu_getresources (ref unsignedinteger resources[])
public function integer fu_defineresources (ref string resources[])
public subroutine fu_setcapture (datawindow capture_window)
end prototypes

public function unsignedlong fu_bitandmask (unsignedlong lmask1, unsignedlong lmask2);//******************************************************************
//  PO Module     : n_Ext_Other
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1995.  All Rights Reserved.
//******************************************************************

BOOLEAN       l_Bit1, l_Bit2
INTEGER       l_Idx
UNSIGNEDLONG  l_Divided, l_Return

//------------------------------------------------------------------
//  Initialize the return value to cleared state.
//------------------------------------------------------------------

l_Return = 0

//------------------------------------------------------------------
//  Step through each bit.  If both bits are set then set the
//  corresponding bit in the return value.
//------------------------------------------------------------------

FOR l_Idx = 1 TO 32

   //---------------------------------------------------------------
   //  Right  the return value.
   //---------------------------------------------------------------

   l_Return /= 2

   //---------------------------------------------------------------
   //  See if the lMask1 is odd.  If it is, then we know the low
   //  order bit is set.  Since dividing by 2 shifts a value
   //  right by one bit, we can save l_Divided into lMask1 so
   //  that the next time through the loop we'll be ready to
   //  test the next bit.
   //---------------------------------------------------------------

   l_Divided = lMask1 / 2
   l_Bit1    = (2 * l_Divided <> lMask1)
   lMask1    = l_Divided

   //---------------------------------------------------------------
   //  See if the LMask2 is odd.  If it is, then we know the low
   //  order bit is set.  Since dividing by 2 shifts a value
   //  right by one bit, we can save l_Divided into lMask2 so
   //  that the next time through the loop we'll be ready to
   //  test the next bit.
   //---------------------------------------------------------------

   l_Divided = lMask2 / 2
   l_Bit2    = (2 * l_Divided <> lMask2)
   lMask2    = l_Divided

   //---------------------------------------------------------------
   //  If both bits are set, then set the high bit in the return
   //  value.  It will get shifted to the appropriate position by
   //  the time we are done.
   //---------------------------------------------------------------

   IF l_Bit1 AND l_Bit2 THEN
      l_Return += 2147483648
   END IF
NEXT

RETURN l_Return
end function

public subroutine fu_getbitmap (long lbitmapnumber, ref blob lpucbitmap, ref long lwidth, ref long lheight);//******************************************************************
//  PO Module     : n_Ext_Other
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1995.  All Rights Reserved.
//******************************************************************

INTEGER       l_Size, l_Idx
UNSIGNEDLONG  l_Bitmap[]

CHOOSE CASE lBitmapNumber
   CASE c_ExclamationBitmap
      l_Bitmap[] = c_ExclamationBMPData[]
      lWidth     = 35
      lHeight    = 34
   CASE c_InformationBitmap
      l_Bitmap[] = c_InformationBMPData[]
      lWidth     = 35
      lHeight    = 34
   CASE c_PC_TabBitmap
      l_Bitmap[] = c_PC_TabBMPData[]
      lWidth     = 122
      lHeight    = 29
   CASE c_PC_Tab_CBitmap
      l_Bitmap[] = c_PC_Tab_CBMPData[]
      lWidth     = 122
      lHeight    = 26
   CASE c_QuestionBitmap
      l_Bitmap[] = c_QuestionBMPData[]
      lWidth     = 35
      lHeight    = 34
   CASE c_StopSignBitmap
      l_Bitmap[] = c_StopSignBMPData[]
      lWidth     = 35
      lHeight    = 34
   CASE ELSE
END CHOOSE

l_Size     = UpperBound(l_Bitmap[])
lpucBitmap = Blob(Fill(" ", 4 * l_Size))
FOR l_Idx  = 1 TO l_Size
   BlobEdit(lpucBitmap, 4 * (l_Idx - 1) + 1, l_Bitmap[l_Idx])
NEXT

RETURN
end subroutine

public function integer fu_filecopy (string from_file, string to_file);//******************************************************************
//  PO Module     : n_EXT_Other
//  Function      : fu_FileCopy
//  Description   : Copies a file.
//
//  Parameters    : STRING From_File -
//                        File to copy from.
//
//                  STRING To_File -
//                        File to copy to.
//
//  Return Value  : INTEGER
//                         0 = file copy OK
//                        -1 = file copy failed
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_FileRead, l_FileWrite, l_Return
INTEGER  l_FileLen,  l_Loops,     l_Idx
BLOB     l_Bytes

//------------------------------------------------------------------
//  Determine the number of bytes for the file.
//------------------------------------------------------------------

l_FileLen = FileLength(From_File)
IF l_FileLen = -1 THEN
   RETURN -1
END IF

//------------------------------------------------------------------
//  Determine the number of loops that have to be performed to
//  read/write the file.
//------------------------------------------------------------------

IF l_FileLen > 32765 THEN
        IF Mod(l_FileLen, 32765) = 0 THEN
      l_Loops = l_FileLen/32765
        ELSE
      l_Loops = (l_FileLen/32765) + 1
        END IF
ELSE
        l_Loops = 1
END IF

//------------------------------------------------------------------
//  Open the file to read from.
//------------------------------------------------------------------

l_FileRead = FileOpen(From_File, StreamMode!, Read!, LockRead!)
IF l_FileRead = -1 THEN
   RETURN -1
END IF

//------------------------------------------------------------------
//  Open the file to write to.
//------------------------------------------------------------------

l_FileWrite = FileOpen(To_File, StreamMode!, Write!, &
                                LockWrite!,  Replace!)
IF l_FileWrite = -1 THEN
   FileClose(l_FileRead)
   RETURN -1
END IF

//------------------------------------------------------------------
//  Copy the contents from one file to the other.
//------------------------------------------------------------------

FOR l_Idx = 1 TO l_Loops
   l_Return = FileRead(l_FileRead, l_Bytes)
   IF l_Return = -1 THEN
      FileClose(l_FileRead)
      FileClose(l_FileWrite)
      RETURN -1
   ELSE
      l_Return = FileWrite(l_FileWrite, l_Bytes)
      IF l_Return = -1 THEN
         FileClose(l_FileRead)
         FileClose(l_FileWrite)
         RETURN -1
      END IF
   END IF
NEXT

//------------------------------------------------------------------
//  Close the files.
//------------------------------------------------------------------

FileClose(l_FileRead)
FileClose(l_FileWrite)

RETURN 0
end function

public function integer fu_filemove (string from_file, string to_file);//******************************************************************
//  PO Module     : N_EXT_Other
//  Function      : fu_FileMove
//  Description   : Moves a file.
//
//  Parameters    : STRING From_File -
//                     File to move from.
//                  STRING To_File   -
//                     File to move to.
//
//  Return Value  : INTEGER
//                      0 = file move OK
//                     -1 = file move failed
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN  l_OK
INTEGER  l_Return

//------------------------------------------------------------------
//  Copy the file.
//------------------------------------------------------------------

l_Return = fu_FileCopy(From_File, To_File)
IF l_Return <> 0 THEN
   RETURN l_Return
END IF

//------------------------------------------------------------------
//  Remove the old file.
//------------------------------------------------------------------

l_OK = FileDelete(From_File)
IF NOT l_OK THEN
   RETURN -1
END IF

RETURN 0
end function

public subroutine fu_getframesize (ref integer frame_width, ref integer frame_height);//******************************************************************
//  PO Module     : n_Ext_Other
//  Function      : fu_GetFrameSize
//  Description   :
//
//  Parameters    : ref INTEGER  Frame_Width -
//                  ref INTEGER  Frame_Height -
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1995.  All Rights Reserved.
//******************************************************************

Frame_Width  = 4
Frame_Height = 4

Frame_Width  = PixelsToUnits(Frame_Width,  XPixelsToUnits!)
Frame_Height = PixelsToUnits(Frame_Height, YPixelsToUnits!)

RETURN
end subroutine

public subroutine fu_getscreensize (ref long screen_width, ref long screen_height);//******************************************************************
//  PO Module     : n_Ext_Other
//  Function      : fu_GetScreenSize
//  Description   :
//
//  Parameters    : ref LONG  Screen_Width -
//                  ref LONG  Screen_Height -
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1995.  All Rights Reserved.
//******************************************************************

Screen_Width  = PixelsToUnits(w_POManager_Std.ENV.ScreenWidth,  &
                              XPixelsToUnits!)
Screen_Height = PixelsToUnits(w_POManager_Std.ENV.ScreenHeight, &
                              YPixelsToUnits!)

RETURN
end subroutine

public subroutine fu_releasecapture ();//******************************************************************
//  PO Module     : n_Ext_Other
//  Function      : fu_ReleaseCapture
//  Description   :
//
//  Parameters    : (None)
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1995.  All Rights Reserved.
//******************************************************************

end subroutine

public subroutine fu_getwindowinfo (ref integer title_height, ref integer control_menu_width);//******************************************************************
//  PO Module     : n_Ext_Other
//  Subroutine    : fu_GetWindowInfo
//  Description   : Returns sizing of window components.
//
//  Parameters    :
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_BorderHeight, l_BorderWidth, l_FrameWidth
INTEGER  l_TitleHeight,  l_MenuHeight,  l_FrameHeight

//------------------------------------------------------------------
//  Get the title and menu heights.  Assume that the control
//  menu is square.
//------------------------------------------------------------------

l_BorderWidth      = 1
l_BorderHeight     = 1
l_TitleHeight      = 20
l_FrameWidth       = 4
l_FrameHeight      = 4

Control_Menu_Width = PixelsToUnits(l_TitleHeight     - &
                                   2 * l_BorderWidth + &
                                   l_FrameWidth,    &
                                   XPixelsToUnits!)
Title_Height       = PixelsToUnits(l_TitleHeight      - &
                                   2 * l_BorderHeight + &
                                   l_FrameHeight,    &
                                   YPixelsToUnits!)

RETURN
end subroutine

public subroutine fu_getresources (ref unsignedinteger resources[]);//******************************************************************
//  PO Module     : n_Ext_Other
//  Function      : fu_GetResources
//  Description   :
//
//  Parameters    : ref UNSIGNEDINTEGER  Resources[] -
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1995.  All Rights Reserved.
//******************************************************************



end subroutine

public function integer fu_defineresources (ref string resources[]);//******************************************************************
//  PO Module     : n_Ext_Other
//  Function      : fu_DefineResources
//  Description   :
//
//  Parameters    : ref STRING  Resources[] -
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1995.  All Rights Reserved.
//******************************************************************

RETURN 0

end function

public subroutine fu_setcapture (datawindow capture_window);//******************************************************************
//  PO Module     : n_Ext_Other
//  Function      : fu_SetCapture
//  Description   :
//
//  Parameters    : DATAWINDOW  Capture_Window -
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1995.  All Rights Reserved.
//******************************************************************

i_TmpHandle = 0

RETURN
end subroutine

on constructor;call n_ext_main::constructor;//******************************************************************
//  PO Module     : n_EXT_Other
//  Event         : Constructor
//  Description   : Initialize variables for other platforms.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

CHOOSE CASE w_POMANAGER_Std.ENV.OSType
   CASE Macintosh!
      c_DirDelim     = ":"
      c_ODBCFile     = "ODBC Preferences"
      c_SystemFile   = "pb.ini"

   CASE ELSE
      c_DirDelim 		= "/"
      c_ODBCFile 		= "odbc.ini"
      c_SystemFile 	= "pb.ini"

END CHOOSE

end on

on n_ext_other.create
TriggerEvent( this, "constructor" )
end on

on n_ext_other.destroy
TriggerEvent( this, "destructor" )
end on

