﻿$PBExportHeader$n_ext_win.sru
$PBExportComments$Provides OS interface for Windows 3.1 and Windows 95
forward
global type n_ext_win from n_ext_main
end type
type rect from structure within n_ext_win
end type
end forward

type rect from structure
    integer left
    integer top
    integer right
    integer bottom
end type

global type n_ext_win from n_ext_main
end type
global n_ext_win n_ext_win

type prototypes
//******************************************************************
//  Externals	: PowerClass DLL Functions
//  Description	: Provides the global external functions
//  		  definitions for the PowerClass DLL.
//
//  Change History	:
//
//  Date	Person	      Description of Change
//  --------	----------    --------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

FUNCTION UNSIGNEDINT PCDLL_Init			&
      (    STRING		lpszCode,			&
           INTEGER		iMaxLength,		&
       ref STRING		lpszVersion,		&
       ref STRING		lpszRequiredEdition,	&
       ref STRING		lpszActualEdition,		&
       ref UNSIGNEDINT	iUnused)			&
LIBRARY "PC041.DLL" alias for "PCDLL_Init;Ansi"

SUBROUTINE PCDLL_Term				&
      (ref UNSIGNEDINT	iUnused)			&
LIBRARY "PC041.DLL" alias for "PCDLL_Term;Ansi"

FUNCTION UNSIGNEDLONG PCDLL_BitANDMask	&
      (UNSIGNEDLONG  	lMask1,			&
       UNSIGNEDLONG  	lMask2)			&
LIBRARY "PC041.DLL" alias for "PCDLL_BitANDMask;Ansi"

FUNCTION UNSIGNEDLONG PCDLL_BitORMask		&
      (UNSIGNEDLONG  	lMask1,			&
       UNSIGNEDLONG  	lMask2)			&
LIBRARY "PC041.DLL" alias for "PCDLL_BitORMask;Ansi"

//******************************************************************
//  Externals	: ServerLogic Windows Functions
//  Description	: Provides the global external functions
//  		  definitions for Windows functions
//  		  used by the ServerLogic Libraries.
//
//  Change History	:
//
//  Date	Person	      Description of Change
//  --------	----------    --------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

SUBROUTINE FreeLibrary				&
      (UNSIGNEDINT	hLibHandle)		&
LIBRARY "KERNEL.EXE" alias for "FreeLibrary;Ansi"

SUBROUTINE GetClientRect				&
   (UNSIGNEDINT		hWnd, 			&
    ref RECT		lpRC)			&
LIBRARY "USER.DLL" alias for "GetClientRect;Ansi"

FUNCTION UNSIGNEDINT GetFreeSystemResources	&
      (UNSIGNEDINT	nSysResource)		&
LIBRARY "USER.DLL" alias for "GetFreeSystemResources;Ansi"

FUNCTION INTEGER GetSystemMetrics		&
      (INTEGER		nOption)			&
LIBRARY "USER.DLL" alias for "GetSystemMetrics;Ansi"

FUNCTION UNSIGNEDINT LoadLibrary			&
      (STRING		lpszLibName)		&
LIBRARY "KERNEL.EXE" alias for "LoadLibrary;Ansi"

FUNCTION UNSIGNEDINT ReleaseCapture		&
      ()						&
LIBRARY "USER.DLL" alias for "ReleaseCapture;Ansi"

FUNCTION UNSIGNEDINT SetCapture			&
      (UNSIGNEDINT	hWnd)			&
LIBRARY "USER.DLL" alias for "SetCapture;Ansi"
end prototypes

type variables
//------------------------------------------------------------------------------
//  For use with GetFreeSystemResources()
//------------------------------------------------------------------------------

UNSIGNEDINT	GFSR_SYSTEMRESOURCES = 0
UNSIGNEDINT	GFSR_GDIRESOURCES 	= 1
UNSIGNEDINT	GFSR_USERRESOURCES 	= 2

//------------------------------------------------------------------------------
//  For use with GetSystemMetrics()
//------------------------------------------------------------------------------

INTEGER		SM_CXBORDER 		= 5
INTEGER		SM_CYBORDER 		= 6
INTEGER		SM_CYCAPTION 		= 4
INTEGER		SM_CYMENU 		= 15
INTEGER		SM_CXFRAME 		= 32
INTEGER		SM_CYFRAME 		= 33

//------------------------------------------------------------------------------
//  Handle array for fu_LoadLibrary/fu_FreeLibrary.
//------------------------------------------------------------------------------

INTEGER		i_NumLibHandles
UNSIGNEDINT	i_LibHandles[]

//------------------------------------------------------------------------------
//  Temporary handle for doing work - don't want to 
//  spread handles all over the code.
//------------------------------------------------------------------------------

UNSIGNEDINT	i_TmpHandle


end variables

forward prototypes
public function unsignedinteger fu_extinit (string lpszcode, integer imaxlength, ref string lpszversion, ref string lpszrequirededition, ref string lpszactualedition)
public subroutine fu_extterm ()
public function unsignedlong fu_bitandmask (unsignedlong lmask1, unsignedlong lmask2)
public subroutine fu_getbitmap (long lbitmapnumber, ref blob lpucbitmap, ref long lwidth, ref long lheight)
public function integer fu_filecopy (string from_file, string to_file)
public function integer fu_filemove (string from_file, string to_file)
public subroutine fu_freelibrary (integer pc_handle)
public subroutine fu_getframesize (ref integer frame_width, ref integer frame_height)
public subroutine fu_getscreensize (ref long screen_width, ref long screen_height)
public function integer fu_loadlibrary (string library_name)
public subroutine fu_releasecapture ()
public subroutine fu_getwindowinfo (ref integer title_height, ref integer control_menu_width)
public subroutine fu_getresources (ref unsignedinteger resources[])
public function integer fu_defineresources (ref string resources[])
public subroutine fu_setcapture (datawindow capture_window)
end prototypes

public function unsignedinteger fu_extinit (string lpszcode, integer imaxlength, ref string lpszversion, ref string lpszrequirededition, ref string lpszactualedition);//******************************************************************
//  PO Module     : n_Ext_Win
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1995.  All Rights Reserved.
//******************************************************************

UNSIGNEDINT  l_Unused
UNSIGNEDINT  l_Return

IF i_UseDLL THEN
   l_Return = PCDLL_Init(lpszCode,            &
                         iMaxLength,          &
                         lpszVersion,         &
                         lpszRequiredEdition, &
                         lpszActualEdition,   &
                         l_Unused)
   RETURN l_Return
END IF

//------------------------------------------------------------------
//  This section never should be reached.
//------------------------------------------------------------------

l_Return            = -5
lpszVersion         = "Ver 0.0:"
lpszRequiredEdition = "Non-DLL"
lpszActualEdition   = lpszRequiredEdition

RETURN l_Return
end function

public subroutine fu_extterm ();//******************************************************************
//  PO Module     : n_Ext_Win/n_Ext_NT
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1995.  All Rights Reserved.
//******************************************************************

UNSIGNEDINT  l_Unused

IF i_UseDLL THEN
   PCDLL_Term(l_Unused)
   RETURN
END IF

//------------------------------------------------------------------
//  This section never should be reached.
//------------------------------------------------------------------

RETURN
end subroutine

public function unsignedlong fu_bitandmask (unsignedlong lmask1, unsignedlong lmask2);//******************************************************************
//  PO Module     : n_Ext_Win
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1995.  All Rights Reserved.
//******************************************************************

BOOLEAN       l_Bit1, l_Bit2
INTEGER       l_Idx
UNSIGNEDLONG  l_Divided
UNSIGNEDLONG  l_Return

IF i_UseDLL THEN
   l_Return = PCDLL_BitANDMask(lMask1, lMask2)
   RETURN l_Return
END IF

//------------------------------------------------------------------
//  Initialize the return value to cleared state.
//------------------------------------------------------------------

l_Return = 0

//------------------------------------------------------------------
//  Step through each bit.  If both bits are set then set the
//  corresponding bit in the return value.
//------------------------------------------------------------------

FOR l_Idx = 1 TO 32

   //---------------------------------------------------------------
   //  Right  the return value.
   //---------------------------------------------------------------

   l_Return /= 2

   //---------------------------------------------------------------
   //  See if the lMask1 is odd.  If it is, then we know the low
   //  order bit is set.  Since dividing by 2 shifts a value
   //  right by one bit, we can save l_Divided into lMask1 so
   //  that the next time through the loop we'll be ready to
   //  test the next bit.
   //---------------------------------------------------------------

   l_Divided = lMask1 / 2
   l_Bit1    = (2 * l_Divided <> lMask1)
   lMask1    = l_Divided

   //---------------------------------------------------------------
   //  See if the LMask2 is odd.  If it is, then we know the low
   //  order bit is set.  Since dividing by 2 shifts a value
   //  right by one bit, we can save l_Divided into lMask2 so
   //  that the next time through the loop we'll be ready to
   //  test the next bit.
   //---------------------------------------------------------------

   l_Divided = lMask2 / 2
   l_Bit2    = (2 * l_Divided <> lMask2)
   lMask2    = l_Divided

   //---------------------------------------------------------------
   //  If both bits are set, then set the high bit in the return
   //  value.  It will get shifted to the appropriate position by
   //  the time we are done.
   //---------------------------------------------------------------

   IF l_Bit1 AND l_Bit2 THEN
      l_Return += 2147483648
   END IF
NEXT

RETURN l_Return
end function

public subroutine fu_getbitmap (long lbitmapnumber, ref blob lpucbitmap, ref long lwidth, ref long lheight);//******************************************************************
//  PO Module     : n_Ext_Win
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1995.  All Rights Reserved.
//******************************************************************

INTEGER       l_Size, l_Idx
UNSIGNEDLONG  l_Bitmap[]

CHOOSE CASE lBitmapNumber
   CASE c_ExclamationBitmap
      l_Bitmap[] = c_ExclamationBMPData[]
      lWidth     = 35
      lHeight    = 34
   CASE c_InformationBitmap
      l_Bitmap[] = c_InformationBMPData[]
      lWidth     = 35
      lHeight    = 34
   CASE c_PC_TabBitmap
      l_Bitmap[] = c_PC_TabBMPData[]
      lWidth     = 122
      lHeight    = 29
   CASE c_PC_Tab_CBitmap
      l_Bitmap[] = c_PC_Tab_CBMPData[]
      lWidth     = 122
      lHeight    = 26
   CASE c_QuestionBitmap
      l_Bitmap[] = c_QuestionBMPData[]
      lWidth     = 35
      lHeight    = 34
   CASE c_StopSignBitmap
      l_Bitmap[] = c_StopSignBMPData[]
      lWidth     = 35
      lHeight    = 34
   CASE ELSE
END CHOOSE

l_Size     = UpperBound(l_Bitmap[])
lpucBitmap = Blob(Fill(" ", 4 * l_Size))
FOR l_Idx  = 1 TO l_Size
   BlobEdit(lpucBitmap, 4 * (l_Idx - 1) + 1, l_Bitmap[l_Idx])
NEXT

RETURN
end subroutine

public function integer fu_filecopy (string from_file, string to_file);//******************************************************************
//  PO Module     : n_EXT_Win
//  Function      : fu_FileCopy
//  Description   : Copies a file.
//
//  Parameters    : STRING From_File -
//                        File to copy from.
//
//                  STRING To_File -
//                        File to copy to.
//
//  Return Value  : INTEGER
//                         0 = file copy OK
//                        -1 = file copy failed
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_FileRead, l_FileWrite, l_Return
INTEGER  l_FileLen,  l_Loops,     l_Idx
BLOB     l_Bytes

//------------------------------------------------------------------
//  Determine the number of bytes for the file.
//------------------------------------------------------------------

l_FileLen = FileLength(From_File)
IF l_FileLen = -1 THEN
   RETURN -1
END IF

//------------------------------------------------------------------
//  Determine the number of loops that have to be performed to
//  read/write the file.
//------------------------------------------------------------------

IF l_FileLen > 32765 THEN
	IF Mod(l_FileLen, 32765) = 0 THEN
      l_Loops = l_FileLen/32765
	ELSE
      l_Loops = (l_FileLen/32765) + 1
	END IF
ELSE
	l_Loops = 1
END IF

//------------------------------------------------------------------
//  Open the file to read from.
//------------------------------------------------------------------

l_FileRead = FileOpen(From_File, StreamMode!, Read!, LockRead!)
IF l_FileRead = -1 THEN
   RETURN -1
END IF

//------------------------------------------------------------------
//  Open the file to write to.
//------------------------------------------------------------------

l_FileWrite = FileOpen(To_File, StreamMode!, Write!, &
                                LockWrite!,  Replace!)
IF l_FileWrite = -1 THEN
   FileClose(l_FileRead)
   RETURN -1
END IF

//------------------------------------------------------------------
//  Copy the contents from one file to the other.
//------------------------------------------------------------------

FOR l_Idx = 1 TO l_Loops
	l_Return = FileRead(l_FileRead, l_Bytes)
	IF l_Return = -1 THEN
      FileClose(l_FileRead)
      FileClose(l_FileWrite)
      RETURN -1
   ELSE
      l_Return = FileWrite(l_FileWrite, l_Bytes)
	   IF l_Return = -1 THEN
         FileClose(l_FileRead)
         FileClose(l_FileWrite)
         RETURN -1
      END IF
   END IF
NEXT

//------------------------------------------------------------------
//  Close the files.
//------------------------------------------------------------------

FileClose(l_FileRead)
FileClose(l_FileWrite)

RETURN 0
end function

public function integer fu_filemove (string from_file, string to_file);//******************************************************************
//  PO Module     : N_EXT_Win
//  Function      : fu_FileMove
//  Description   : Moves a file.
//
//  Parameters    : STRING From_File -
//                     File to move from.
//                  STRING To_File   -
//                     File to move to.
//
//  Return Value  : INTEGER
//                      0 = file move OK
//                     -1 = file move failed
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN  l_OK
INTEGER  l_Return

//------------------------------------------------------------------
//  Copy the file.
//------------------------------------------------------------------

l_Return = fu_FileCopy(From_File, To_File)
IF l_Return <> 0 THEN
   RETURN l_Return
END IF

//------------------------------------------------------------------
//  Remove the old file.
//------------------------------------------------------------------

l_OK = FileDelete(From_File)
IF NOT l_OK THEN
   RETURN -1
END IF

RETURN 0
end function

public subroutine fu_freelibrary (integer pc_handle);//******************************************************************
//  PO Module     : n_Ext_Win
//  Function      : fu_FreeLibrary
//  Description   : Frees a DLL.
//
//  Parameters    : INTEGER  PC_Handle -
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Idx

IF PC_Handle > 0 THEN
   i_TmpHandle = i_LibHandles[PC_Handle]
   FreeLibrary(i_TmpHandle)

   FOR l_Idx = PC_Handle + 1 TO i_NumLibHandles
      i_LibHandles[l_Idx - 1] = i_LibHandles[l_Idx]
   NEXT
   i_NumLibHandles = i_NumLibHandles - 1
END IF

RETURN
end subroutine

public subroutine fu_getframesize (ref integer frame_width, ref integer frame_height);//******************************************************************
//  PO Module     : n_Ext_Win
//  Function      : fu_GetFrameSize
//  Description   :
//
//  Parameters    : ref INTEGER  Frame_Width -
//                  ref INTEGER  Frame_Height -
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1995.  All Rights Reserved.
//******************************************************************

Frame_Width  = GetSystemMetrics(SM_CXFRAME)
Frame_Height = GetSystemMetrics(SM_CYFRAME)

Frame_Width  = PixelsToUnits(Frame_Width,  XPixelsToUnits!)
Frame_Height = PixelsToUnits(Frame_Height, YPixelsToUnits!)

RETURN
end subroutine

public subroutine fu_getscreensize (ref long screen_width, ref long screen_height);//******************************************************************
//  PO Module     : n_Ext_Win
//  Function      : fu_GetScreenSize
//  Description   :
//
//  Parameters    : ref LONG  Screen_Width -
//                  ref LONG  Screen_Height -
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1995.  All Rights Reserved.
//******************************************************************

Screen_Width  = PixelsToUnits(w_POManager_Std.ENV.ScreenWidth,  &
                              XPixelsToUnits!)
Screen_Height = PixelsToUnits(w_POManager_Std.ENV.ScreenHeight, &
                              YPixelsToUnits!)

RETURN
end subroutine

public function integer fu_loadlibrary (string library_name);//******************************************************************
//  PO Module     : n_Ext_Win
//  Function      : fu_LoadLibrary
//  Description   : Loads a DLL.
//
//  Parameters    : STRING Library_Name -
//                        The name of the DLL to load.
//
//  Return Value  : INTEGER -
//                        A handle to the DLL that was loaded.
//                        If the return value is 0, then there
//                        was an error while loading the DLL.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN      l_Success
INTEGER      l_PCHandle
STRING       l_LibError

i_TmpHandle = LoadLibrary(Library_Name)
l_Success   = (i_TmpHandle >= 32)

IF NOT l_Success THEN

   l_PCHandle = 0

   CHOOSE CASE i_TmpHandle
      CASE 0
         l_LibError = "      Out of memory."
      CASE 2
         l_LibError = "      File not found."
      CASE 3
         l_LibError = "      Path not found."
      CASE 5
         l_LibError = "      Attempt to dynamically link to a task."
      CASE 6
         l_LibError = "      Library requires separate data~n" + &
                      "      segments for each task."
      CASE 10
         l_LibError = "      Incorrect Windows version."
      CASE 11
         l_LibError = "      Invalid .EXE file (non-Windows~n" + &
                      "      .EXE or error in .EXE image)."
      CASE 12
         l_LibError = "      OS/2 application."
      CASE 13
         l_LibError = "      DOS 4.0 application."
      CASE 14
         l_LibError = "      Unknown .EXE type."
      CASE 15
         l_LibError = "      Attempt in protected (standard~n" + &
                      "      or 386 enhanced) mode to load~n"  + &
                      "      an .EXE created for an earlier~n" + &
                      "      version of Windows."
      CASE 16
         l_LibError = "      Attempt to load a second~n"       + &
                      "      instance of an .EXE containing~n" + &
                      "      multiple, writeable data segments."
      CASE 17
         l_LibError = "      Attempt in large-frame EMS~n"      + &
                      "      mode to load a second instance~n"  + &
                      "      of an application that links to~n" + &
                      "      certain nonshareable DLLs~n"       + &
                      "      already in use."
      CASE 18
         l_LibError = "      Attempt in real mode to load an~n"  + &
                      "      application marked for protected~n" + &
                      "      mode only."
      CASE ELSE
         l_LibError = "      Unknown error while loading the DLL."
   END CHOOSE

   MessageBox("n_Ext_Win::fu_LoadLibrary()", &
              Library_Name + ": ~n~n" + l_LibError, StopSign!, Ok!)
ELSE
   i_NumLibHandles               = i_NumLibHandles + 1
   i_LibHandles[i_NumLibHandles] = i_TmpHandle
   l_PCHandle                    = i_NumLibHandles
END IF

RETURN l_PCHandle
end function

public subroutine fu_releasecapture ();//******************************************************************
//  PO Module     : n_Ext_Win
//  Function      : fu_ReleaseCapture
//  Description   :
//
//  Parameters    : (None)
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1995.  All Rights Reserved.
//******************************************************************

ReleaseCapture()

RETURN
end subroutine

public subroutine fu_getwindowinfo (ref integer title_height, ref integer control_menu_width);//******************************************************************
//  PO Module     : n_Ext_Win
//  Subroutine    : fu_GetWindowInfo
//  Description   : Returns sizing of window components.
//
//  Parameters    :
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_BorderHeight, l_BorderWidth, l_FrameWidth
INTEGER  l_TitleHeight,  l_MenuHeight,  l_FrameHeight

//------------------------------------------------------------------
//  Get the title and menu heights.  Assume that the control
//  menu is square.
//------------------------------------------------------------------

l_BorderWidth      = GetSystemMetrics(SM_CXBORDER)
l_BorderHeight     = GetSystemMetrics(SM_CYBORDER)
l_TitleHeight      = GetSystemMetrics(SM_CYCAPTION)
l_FrameWidth       = GetSystemMetrics(SM_CXFRAME)
l_FrameHeight      = GetSystemMetrics(SM_CYFRAME)

Control_Menu_Width = PixelsToUnits(l_TitleHeight     - &
                                   2 * l_BorderWidth + &
                                   l_FrameWidth,    &
                                   XPixelsToUnits!)
Title_Height       = PixelsToUnits(l_TitleHeight      - &
                                   2 * l_BorderHeight + &
                                   l_FrameHeight,    &
                                   YPixelsToUnits!)

RETURN
end subroutine

public subroutine fu_getresources (ref unsignedinteger resources[]);//******************************************************************
//  PO Module     : n_Ext_Win
//  Function      : fu_GetResources
//  Description   :
//
//  Parameters    : ref UNSIGNEDINTEGER  Resources[] -
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1995.  All Rights Reserved.
//******************************************************************

Resources[3] = GetFreeSystemResources(GFSR_USERRESOURCES)
Resources[2] = GetFreeSystemResources(GFSR_GDIRESOURCES)
Resources[1] = GetFreeSystemResources(GFSR_SYSTEMRESOURCES)

end subroutine

public function integer fu_defineresources (ref string resources[]);//******************************************************************
//  PO Module     : n_Ext_Win
//  Function      : fu_DefineResources
//  Description   :
//
//  Parameters    : ref STRING  Resources[] -
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1995.  All Rights Reserved.
//******************************************************************

Resources[1] = "S"
Resources[2] = "G"
Resources[3] = "U"

RETURN 3

end function

public subroutine fu_setcapture (datawindow capture_window);//******************************************************************
//  PO Module     : n_Ext_Win
//  Function      : fu_SetCapture
//  Description   :
//
//  Parameters    : DATAWINDOW  Capture_Window -
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1995.  All Rights Reserved.
//******************************************************************

i_TmpHandle = SetCapture(Handle(Capture_Window))

RETURN
end subroutine

on constructor;call n_ext_main::constructor;//******************************************************************
//  PO Module     : n_EXT_Win
//  Event         : Constructor
//  Description   : Initialize variables for the Windows platform.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

c_DirDelim 		= "\"
c_ODBCFile 		= "odbc.ini"
c_SystemFile 	= "pb.ini"
end on

on n_ext_win.create
call super::create
end on

on n_ext_win.destroy
call super::destroy
end on

