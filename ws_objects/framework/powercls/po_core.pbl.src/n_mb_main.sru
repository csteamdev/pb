﻿$PBExportHeader$n_mb_main.sru
$PBExportComments$Ancestor messaging object
forward
global type n_mb_main from nonvisualobject
end type
end forward

shared variables
BOOLEAN	s_InMB
end variables

global type n_mb_main from nonvisualobject
event pc_check pbm_custom75
end type
global n_mb_main n_mb_main

type variables
//-------------------------------------------------------------------------------
//  Enumerated constants for database names.  
//  These are returned by fu_GetDatabaseType() 
//  and are used to index into c_DBNames[].
//-------------------------------------------------------------------------------

UNSIGNEDLONG	c_DB_Unknown 		= 1
UNSIGNEDLONG	c_DB_AllBase 		= 2
UNSIGNEDLONG	c_DB_MDG_DB2 		= 3
UNSIGNEDLONG	c_DB_Gupta 		= 4
UNSIGNEDLONG	c_DB_IBMDRDA 		= 5
UNSIGNEDLONG	c_DB_Informix4 		= 6
UNSIGNEDLONG	c_DB_Informix5 		= 7
UNSIGNEDLONG	c_DB_ODBC		= 8
UNSIGNEDLONG	c_DB_Oracle6 		= 9
UNSIGNEDLONG	c_DB_Oracle7 		= 10
UNSIGNEDLONG	c_DB_SQLServer 		= 11
UNSIGNEDLONG	c_DB_Sybase 		= 12
UNSIGNEDLONG	c_DB_SybaseNet 		= 13
UNSIGNEDLONG	c_DB_XDB 		= 14

UNSIGNEDLONG	c_DB_Watcom 		= 15
UNSIGNEDLONG	c_DB_DecRdb 		= 16
UNSIGNEDLONG	c_DB_Sybase49 		= 17

//------------------------------------------------------------------------------
//  String constants for the database names.
//------------------------------------------------------------------------------

STRING		c_DB_Names[] = { &
		"Unknown", &
		"HP Allbase/SQL", &
		"Micro Decisionware Gateway/DB2", &
		"Gutpa SQLBase", &
		"IBM DRDA", &
		"Informix 4 SQL", &
		"Informix 5 SQL", &
		"ODBC", &
		"Oracle 6 SQL", &
		"Oracle 7 SQL", &
		"SQL Server", &
		"Sybase SQL", &
		"Sybase Net-Gateway/DB2", &
		"XDB", &
		"Watcom SQL", &
		"Digital RDB", &
		"Sybase SQL 4.9" &
		}

//------------------------------------------------------------------------------
//  Message constants.
//------------------------------------------------------------------------------

REAL		i_MB_Numbers[]
STRING		i_MB_Strings[]

STRING		c_MB_UseDefaultTitle
STRING		c_MB_UseClassTitle
ICON		c_MB_UseClassIcon
ICON		c_MB_NoClassIcon
BOOLEAN	c_MB_Enabled 		= TRUE
BOOLEAN	c_MB_Disabled 		= FALSE

UNSIGNEDLONG	c_MBC_Undefined		= 1
UNSIGNEDLONG	c_MBI_Undefined		= 1

UNSIGNEDLONG	c_MB_MaxBaseClass
UNSIGNEDLONG	c_MB_MaxBaseID

STRING		c_DefaultTitle[]
STRING		c_ClassTitle[]
ICON		c_ClassIcon[]

UNSIGNEDLONG	c_MessageClass[]
STRING		c_MessageTitle[]
STRING		c_MessageText[]
ICON		c_MessageIcon[]
BUTTON		c_MessageButtonSet[]
INTEGER		c_MessageDefButton[]
BOOLEAN	c_MessageEnabled[]
end variables

forward prototypes
public function string fu_formatstring (string fmt_string, integer num_numbers, real number_parms[], integer num_strings, string string_parms[])
public subroutine fu_getdatabasetype (transaction trans_obj, ref unsignedlong db_type, ref boolean is_odbc)
public subroutine fu_getinfo (unsignedlong mb_id, integer num_numbers, real number_parms[], integer num_strings, string string_parms[], ref string title, ref string text, ref icon icon, ref button button_set, ref integer default_button, ref boolean enabled)
public function boolean fu_inmb ()
public function integer fu_messagebox (unsignedlong mb_id, integer num_numbers, real number_parms[], integer num_strings, string string_parms[])
public function string fu_quotechar (string fix_string, string quote_chr)
public subroutine fu_setinmb (boolean in_or_out)
public function string fu_getdatabasedescrip (transaction trans_obj)
end prototypes

on pc_check;//******************************************************************
//  PO Module     : n_MB_Main
//  Event         : pc_Check
//  Description   : Performs some sanity tests on the MB object.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Idx

//------------------------------------------------------------------
//  Sanity checks.
//------------------------------------------------------------------

l_Idx = UpperBound(c_ClassTitle[])
IF l_Idx <> c_MB_MaxBaseClass THEN
   MessageBox("Developer Error",                  &
              "n_MB_Main::Constructor error:~n" + &
              "      c_ClassTitle[]",             &
              Exclamation!, Ok!, 1)
END IF

l_Idx = UpperBound(c_ClassIcon[])
IF l_Idx <> c_MB_MaxBaseClass THEN
   MessageBox("Developer Error",                  &
              "n_MB_Main::Constructor error:~n" + &
              "      c_ClassIcon[]",              &
              Exclamation!, Ok!, 1)
END IF

l_Idx = UpperBound(c_MessageClass[])
IF l_Idx <> c_MB_MaxBaseID THEN
   MessageBox("Developer Error",                  &
              "n_MB_Main::Constructor error:~n" + &
              "      c_MessageClass[]",           &
              Exclamation!, Ok!, 1)
END IF

l_Idx = UpperBound(c_MessageTitle[])
IF l_Idx <> c_MB_MaxBaseID THEN
   MessageBox("Developer Error",                  &
              "n_MB_Main::Constructor error:~n" + &
              "      c_MessageTitle[]",           &
              Exclamation!, Ok!, 1)
END IF

l_Idx = UpperBound(c_MessageText[])
IF l_Idx <> c_MB_MaxBaseID THEN
   MessageBox("Developer Error",                  &
              "n_MB_Main::Constructor error:~n" + &
              "      c_MessageText[]",            &
              Exclamation!, Ok!, 1)
END IF

l_Idx = UpperBound(c_MessageIcon[])
IF l_Idx <> c_MB_MaxBaseID THEN
   MessageBox("Developer Error",                  &
              "n_MB_Main::Constructor error:~n" + &
              "      c_MessageIcon[]",            &
              Exclamation!, Ok!, 1)
END IF

l_Idx = UpperBound(c_MessageButtonSet[])
IF l_Idx <> c_MB_MaxBaseID THEN
   MessageBox("Developer Error",                  &
              "n_MB_Main::Constructor error:~n" + &
              "      c_MessageButtonSet[]",       &
              Exclamation!, Ok!, 1)
END IF

l_Idx = UpperBound(c_MessageDefButton[])
IF l_Idx <> c_MB_MaxBaseID THEN
   MessageBox("Developer Error",                  &
              "n_MB_Main::Constructor error:~n" + &
              "      c_MessageDefButton[]",       &
              Exclamation!, Ok!, 1)
END IF

l_Idx = UpperBound(c_MessageEnabled[])
IF l_Idx <> c_MB_MaxBaseID THEN
   MessageBox("Developer Error",                  &
              "n_MB_Main::Constructor error:~n" + &
              "      c_MessageEnabled[]",         &
              Exclamation!, Ok!, 1)
END IF
end on

public function string fu_formatstring (string fmt_string, integer num_numbers, real number_parms[], integer num_strings, string string_parms[]);//******************************************************************
//  PO Module     : n_MB_Main
//  Function      : fu_FormatString
//  Description   : Substitues values into a string.  Number
//                  substitutions are made when "%<pos>d" is
//                  found and string substitutions are made
//                  when "%<pos>s" is found.  If there are not
//                  enough parameters (e.g. %2d was specified,
//                  but no numbers were passed), the format
//                  string and all spaces following it  will
//                  be deleted from the output. An example
//                  string passed as Fmt_String would be:
//                     "Str #2: %2s; Str #1: %1s, Num: %1d"
//
//  Parameters    : STRING  Fmt_String -
//                        The format string which specifies
//                        the substitutions are to be done.
//
//                  INTEGER Num_Numbers -
//                        The number of real parameters to
//                        be substituted into the Fmt_String.
//
//                  REAL Number_Parms[] -
//                        An array containing the REAL
//                        parameters to be substituted into
//                        the Fmt_String.
//
//                  INTEGER Num_Strings -
//                        The number of string parameters to
//                        be substituted into the Fmt_String.
//
//                  STRING String_Parms[] -
//                        An array containing the string
//                        parameters to be substituted into
//                        the Fmt_String.
//
//  Return Value  : STRING -
//                        The string produced by the formatting.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

STRING   c_FormatChar = "%"

BOOLEAN  l_Quoted,    l_DoSub,     l_KillFormat
INTEGER  l_Length,    l_FormatPos, l_ParmNum, l_EndPos, l_Idx
INTEGER  l_Zero,      l_Nine,      l_TmpAsc
STRING   l_FormatStr, l_SubStr,    l_Num,     l_TmpChar

//------------------------------------------------------------------
//  See if there are any formatting characters in Fmt_String.
//  If there are not, then we can just return Fmt_String.
//------------------------------------------------------------------

l_FormatPos = Pos(Fmt_String, c_FormatChar)
IF l_FormatPos = 0 THEN
   l_FormatStr = Fmt_String
   GOTO Finished
END IF

//------------------------------------------------------------------
//  Find every format character in Fmt_String and determine
//  its type and substitution number and then substitute it.
//------------------------------------------------------------------

l_FormatStr = Fmt_String
l_Length    = Len(l_FormatStr)
l_Zero      = Asc("0")
l_Nine      = Asc("9")

DO WHILE l_FormatPos > 0

   //---------------------------------------------------------------
   //  We found a character that may begin a format.  See if it
   //  is quoted.
   //---------------------------------------------------------------

   l_Quoted = FALSE
   IF l_FormatPos > 1 THEN
      l_Idx     = l_FormatPos - 1
      l_TmpChar = Mid(l_FormatStr, l_Idx, 1)

      DO WHILE l_TmpChar = "~~"
         l_Quoted = (NOT l_Quoted)
         l_Idx    = l_Idx - 1
         IF l_Idx > 0 THEN
            l_TmpChar = Mid(l_FormatStr, l_Idx, 1)
         ELSE
            l_TmpChar = ""
         END IF
      LOOP
   END IF

   //---------------------------------------------------------------
   //  If the character has not already been quoted, then figure
   //  its position parameter.
   //---------------------------------------------------------------

   IF NOT l_Quoted THEN
      IF l_FormatPos < l_Length THEN
         l_Num     = "0"

         l_EndPos  = l_FormatPos + 1
         l_TmpChar = Mid(l_FormatStr, l_EndPos, 1)
         l_TmpAsc  = Asc(l_TmpChar)

         DO WHILE l_TmpAsc >= l_Zero AND l_TmpAsc <= l_Nine
            l_Num    = l_Num    + l_TmpChar
            l_EndPos = l_EndPos + 1
            IF l_EndPos <= l_Length THEN
               l_TmpChar = Mid(l_FormatStr, l_EndPos, 1)
               l_TmpAsc  = Asc(l_TmpChar)
            ELSE
               l_TmpChar = ""
               l_TmpAsc  = 0
            END IF
         LOOP

         //---------------------------------------------------------
         //  If not parameter position was specified, assume 1.
         //---------------------------------------------------------

         l_ParmNum = Integer(l_Num)

         IF l_ParmNum < 1 THEN
            l_ParmNum = 1
         END IF

         //---------------------------------------------------------
         //  See if we have a string substitution.
         //---------------------------------------------------------

         l_DoSub      = FALSE
         l_KillFormat = FALSE

         IF l_TmpChar = "s" THEN

            //------------------------------------------------------
            //  Is it a valid parameter number?
            //------------------------------------------------------

            l_DoSub = (l_ParmNum <= Num_Strings)
            IF l_DoSub THEN
               l_SubStr = String_Parms[l_ParmNum]
            ELSE
               l_KillFormat = TRUE
            END IF
         ELSE

            //------------------------------------------------------
            //  See if we have a number substitution.
            //------------------------------------------------------

            IF l_TmpChar = "d" THEN

               //---------------------------------------------------
               //  Is it a valid parameter number?
               //---------------------------------------------------

               l_DoSub = (l_ParmNum <= Num_Numbers)

               IF l_DoSub THEN
                  l_SubStr = String(Number_Parms[l_ParmNum])
               ELSE
                  l_KillFormat = TRUE
               END IF
            END IF
         END IF

         //---------------------------------------------------------
         //  Nothing to substitute - kill the format.
         //---------------------------------------------------------

         IF l_KillFormat THEN
            l_DoSub = TRUE

            IF l_EndPos < l_Length THEN
               l_EndPos  = l_EndPos + 1
               l_TmpChar = Mid(l_FormatStr, l_EndPos, 1)

               DO WHILE l_TmpChar = " "
                  l_EndPos = l_EndPos + 1
                  IF l_EndPos <= l_Length THEN
                     l_TmpChar = Mid(l_FormatStr, l_EndPos, 1)
                  ELSE
                     l_TmpChar = ""
                  END IF
               LOOP

               l_EndPos = l_EndPos - 1
            END IF

            l_SubStr = ""
         END IF

         IF l_DoSub THEN
            l_FormatStr = Replace(l_FormatStr,                &
                                  l_FormatPos,                &
                                  l_EndPos - l_FormatPos + 1, &
                                  l_SubStr)
            l_Length    = Len(l_FormatStr)
            l_FormatPos = l_FormatPos + Len(l_SubStr) - 1
         END IF
      END IF
   END IF

   //---------------------------------------------------------------
   //  Find the next format character.
   //---------------------------------------------------------------

   l_FormatPos = Pos(l_FormatStr, c_FormatChar, l_FormatPos + 1)
LOOP

Finished:

RETURN l_FormatStr
end function

public subroutine fu_getdatabasetype (transaction trans_obj, ref unsignedlong db_type, ref boolean is_odbc);//******************************************************************
//  PO Module     : n_MB_Main
//  Function      : fu_GetDatabaseType
//  Description   : Returns a unsignedlong identifying the type
//                  of database.
//
//  Parameters    : TRANSACTION  Trans_Obj -
//                        The transaction object used to connect
//                        the database.
//
//                  UNSIGNEDLONG DB_Type -
//                       Returns an enumerated value that
//                       identifies the type of database being
//                       accessed by Trans_Obj.
//
//                  BOOLEAN Is_ODBC -
//                       Indicates if the database being accessed
//                       by Trans_Obj is using ODBC.
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1995.  All Rights Reserved.
//******************************************************************

STRING  l_DBMS, l_ODBC, l_Source, l_Parm
INTEGER l_Pos

//------------------------------------------------------------------
//  Find the database we are connected to from the transaction
//  object.
//------------------------------------------------------------------

l_DBMS = Upper(Trans_Obj.DBMS)

//------------------------------------------------------------------
//  See if we are using ODBC to access the database.
//------------------------------------------------------------------

Is_ODBC = (l_DBMS = "ODBC")

//------------------------------------------------------------------
//  If we are connected to some sort of ODBC database, look in the
//  "Data Sources" section of the ODBC INI file to determine what
//  database we are actually connected to.
//------------------------------------------------------------------

IF Is_ODBC THEN

   l_Parm = Trans_Obj.DBParm
   l_Pos  = Pos(Upper(l_Parm), "DSN=")
   IF l_Pos > 0 THEN
      l_Parm = Mid(l_Parm, l_Pos + 4)
      l_Pos  = Pos(l_Parm, ";")
      IF l_Pos > 0 THEN
         l_Source = Mid(l_Parm, 1, l_Pos - 1)
      ELSE
         l_Pos = Pos(l_Parm, "'")
         IF l_Pos > 0 THEN
            l_Source = Mid(l_Parm, 1, l_Pos - 1)
         ELSE
            l_Source = l_Parm
         END IF
      END IF
   ELSE
      l_Source = Trans_Obj.Database
   END IF

   l_ODBC = (ProfileString(w_POManager_Std.EXT.c_ODBCFile, &
                 "ODBC Data Sources", &
                 l_Source,  &
                 "[NO_ODBC_DSN]"))
   IF l_ODBC = "[NO_ODBC_DSN]" THEN
      l_ODBC = (ProfileString(w_POManager_Std.EXT.c_ODBCFile, &
                    "ODBC 32 bit Data Sources", &
                    l_Source,  &
                    "[NO_ODBC_DSN]"))
   END IF

   //---------------------------------------------------------------
   //  ODBC Databases.
   //---------------------------------------------------------------

   IF Pos(Upper(l_ODBC), "WATCOM") > 0 OR &
      Pos(Upper(l_ODBC), "ANYWHERE") > 0 THEN
         DB_Type = c_DB_Watcom
   ELSEIF Pos(Upper(l_ODBC), "DEC") > 0 OR &
      Pos(Upper(l_ODBC), "RDB") > 0 THEN
         DB_Type = c_DB_DecRdb
   ELSE
         DB_Type = c_DB_ODBC
   END IF
ELSE

   //---------------------------------------------------------------
   //  Now that we know that this is not an ODBC database, we can
   //  set DB_Type to the description of the database.
   //---------------------------------------------------------------

   CHOOSE CASE Left(l_DBMS, 3)

      //------------------------------------------------------------
      //  HP Allbase/SQL.
      //------------------------------------------------------------

      CASE "HPA"
         DB_Type = c_DB_AllBase

      //------------------------------------------------------------
      //  Micro Decisionware Gateway for DB2.
      //------------------------------------------------------------

      CASE "MDI"
         DB_Type = c_DB_MDG_DB2

      //------------------------------------------------------------
      //  Gupta SQLBase.
      //------------------------------------------------------------

      CASE "GUP"
         DB_Type = c_DB_Gupta

      //------------------------------------------------------------
      //  IBM Distributed Relational Database Architecture.
      //------------------------------------------------------------

      CASE "IBM"
         DB_Type = c_DB_IBMDRDA

      //------------------------------------------------------------
      //  Informix 4.
      //------------------------------------------------------------

      CASE "IN4"
         DB_Type = c_DB_Informix4

      //------------------------------------------------------------
      //  Informix 5.
      //------------------------------------------------------------

      CASE "IN5"
         DB_Type = c_DB_Informix5

      //------------------------------------------------------------
      //  Oracle 6.
      //------------------------------------------------------------

      CASE "OR6"
         DB_Type = c_DB_Oracle6

      //------------------------------------------------------------
      //  Oracle 7.
      //------------------------------------------------------------

      CASE "OR7"
         DB_Type = c_DB_Oracle7

      //------------------------------------------------------------
      //  Oracle 7.2
      //------------------------------------------------------------

      CASE "O72"
         DB_Type = c_DB_Oracle7

      //------------------------------------------------------------
      //  Oracle 7.3
      //------------------------------------------------------------

      CASE "O73"
         DB_Type = c_DB_Oracle7

      //------------------------------------------------------------
      //  SQL Server.
      //------------------------------------------------------------

      CASE "SYB","SNC"
         DB_Type = c_DB_SQLServer

      //------------------------------------------------------------
      //  Sybase SQL Server System 10.
      //------------------------------------------------------------

      CASE "SYC","OLE"
         DB_Type = c_DB_Sybase

      //------------------------------------------------------------
      //  Sybase SQL Server System 4.9.
      //------------------------------------------------------------

      CASE "SYT"
         DB_Type = c_DB_Sybase49

      //------------------------------------------------------------
      //  Sybase Net-Gateway for DB2.
      //------------------------------------------------------------

      CASE "NET"
         DB_Type = c_DB_SybaseNet

      //------------------------------------------------------------
      //  XDB.
      //------------------------------------------------------------

      CASE "XDB"
         DB_Type = c_DB_XDB

      //------------------------------------------------------------
      //  Don't know what what type of database this is.
      //------------------------------------------------------------

      CASE ELSE
         DB_Type = c_DB_Unknown
   END CHOOSE
END IF

RETURN
end subroutine

public subroutine fu_getinfo (unsignedlong mb_id, integer num_numbers, real number_parms[], integer num_strings, string string_parms[], ref string title, ref string text, ref icon icon, ref button button_set, ref integer default_button, ref boolean enabled);//******************************************************************
//  PO Module     : n_MB_Main
//  Subroutine    : fu_GetInfo
//  Description   : Gets the information ready for the
//                  fu_MessageBox() function.
//
//  Parameters    : UNSIGNEDLONG MB_ID -
//                        The ID of the message to be formatted
//                        and displayed.
//                  INTEGER Num_Numbers -
//                        The number of REAL parameters to be
//                        substituted into the string.
//                  REAL Number_Parms[] -
//                        An array containing the REAL
//                        parameters to be substituted into the
//                        string.
//                  INTEGER Num_Strings -
//                        The number of string parameters to be
//                        substituted into the string.
//                  STRING String_Parms[] -
//                        An array containing the string
//                        parameters to be substituted into the
//                        string.
//                  ref STRING Title -
//                        The title that should be used for the
//                        message box.
//                  ref STRING Text -
//                        The message that should be used for
//                        the message box.
//                  ref ICON Icon -
//                        The icon that should be used for
//                        the message box.
//                  ref BUTTON Button_Set -
//                        The button set that should be used for
//                        the message box.
//                  ref INTEGER Default_Button -
//                        The default button that should be
//                        specified for the message box.
//                  ref BOOLEAN Enabled -
//                        Indicates if the message is enabled
//                        and should be displayed.
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

UNSIGNEDLONG  l_Class
REAL          l_Number[]

//------------------------------------------------------------------
//  Initialize the return values to used in the fu_MessageBox()
//  call.
//------------------------------------------------------------------

SetNull(Title)
SetNull(Text)
SetNull(Icon)
SetNull(Button_Set)
Default_Button = 0
Enabled        = FALSE

//------------------------------------------------------------------
//  Make sure that we have a valid Message ID.
//------------------------------------------------------------------

IF MB_ID > 0                             AND &
   MB_ID <= UpperBound(c_MessageTitle[]) AND &
   MB_ID <> C_MBI_Undefined              THEN

   //---------------------------------------------------------------
   //  For this Message ID, get the title, text, icon, button
   //  set, default button, and enabled flag to be used in the
   //  fu_MessageBox() call.
   //---------------------------------------------------------------

   Title          = c_MessageTitle[MB_ID]
   Text           = c_MessageText[MB_ID]
   Icon           = c_MessageIcon[MB_ID]
   Button_Set     = c_MessageButtonSet[MB_ID]
   Default_Button = c_MessageDefButton[MB_ID]
   Enabled        = c_MessageEnabled[MB_ID]

   //---------------------------------------------------------------
   //  Get the class for this Message ID.
   //---------------------------------------------------------------

   l_Class = c_MessageClass[MB_ID]

   //---------------------------------------------------------------
   //  Make sure that we have a valid Class.
   //---------------------------------------------------------------

   IF l_Class > 0                           AND &
      l_Class <= UpperBound(c_ClassTitle[]) AND &
      l_Class <> c_MBC_Undefined            THEN

      //------------------------------------------------------------
      //  If the title has not been set then use the title of the
      //  class.
      //------------------------------------------------------------

      IF IsNull(Title) THEN
         Title = c_ClassTitle[l_Class]
      END IF

      //------------------------------------------------------------
      //  If the title was not set, then use the default title.
      //------------------------------------------------------------

      IF IsNull(Title) THEN
         IF l_Class <= UpperBound(c_DefaultTitle[]) THEN
            Title = c_DefaultTitle[l_Class]
         ELSE
            Title = ""
         END IF
      END IF

      //------------------------------------------------------------
      //  If an icon has not been specified, then use the icon
      //  specified for this class.
      //------------------------------------------------------------

      IF IsNull(Icon) THEN
         Icon = c_ClassIcon[l_Class]
      END IF
   ELSE

      //------------------------------------------------------------
      //  Invalid class.
      //------------------------------------------------------------

      l_Number[1] = l_Class
      l_Number[2] = MB_ID
      Title       = fu_FormatString                    &
                       (c_ClassTitle[c_MBC_Undefined], &
                        2, l_Number[],                 &
                        0, i_MB_Strings[])
   END IF
ELSE

   //---------------------------------------------------------------
   //  Invalid message ID.
   //---------------------------------------------------------------

   Title         = c_MessageTitle[c_MBI_Undefined]
   l_Number[1]   = MB_ID
   Text          = fu_FormatString                     &
                      (c_MessageText[c_MBI_Undefined], &
                       1, l_Number[],                  &
                       0, i_MB_Strings[])
   Icon           = c_MessageIcon[c_MBI_Undefined]
   Button_Set     = c_MessageButtonSet[c_MBI_Undefined]
   Default_Button = c_MessageDefButton[c_MBI_Undefined]
   Enabled        = c_MessageEnabled[c_MBI_Undefined]
END IF

//------------------------------------------------------------------
//  If the title was not set, then blank it out.
//------------------------------------------------------------------

IF IsNull(Title) THEN
   Title = ""
ELSE

   //---------------------------------------------------------------
   //  Do the formatting of the title.
   //---------------------------------------------------------------

   Title = fu_FormatString                         &
              (Title, Num_Numbers, Number_Parms[], &
                      Num_Strings, String_Parms[])
END IF

//------------------------------------------------------------------
//  If Text is NULL, then we can't do formatting.
//------------------------------------------------------------------

IF IsNull(Text) THEN
   Text = ""
ELSE

   //---------------------------------------------------------------
   //  Do the formatting of the message.
   //---------------------------------------------------------------

   Text = fu_FormatString                        &
             (Text, Num_Numbers, Number_Parms[], &
                    Num_Strings, String_Parms[])
END IF

//------------------------------------------------------------------
//  If the icon was not set, then use None!.
//------------------------------------------------------------------

IF IsNull(Icon) THEN
   Icon = None!
END IF

//------------------------------------------------------------------
//  If the button was not set, then use Ok!.
//------------------------------------------------------------------

IF IsNull(Button_Set) THEN
   Button_Set = Ok!
END IF

RETURN
end subroutine

public function boolean fu_inmb ();//******************************************************************
//  PO Module     : n_MB_Main
//  Function      : fu_InMB
//  Description   : Indicates if a message box is currently
//                  execuing.
//
//  Parameters    : (None)
//  Return Value  : BOOLEAN -
//                        TRUE indicates that a message box
//                        is executing.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

RETURN s_InMB
end function

public function integer fu_messagebox (unsignedlong mb_id, integer num_numbers, real number_parms[], integer num_strings, string string_parms[]);//******************************************************************
//  PO Module     : n_MB_Main
//  Function      : fu_MessageBox
//  Description   : Formats and displays the message specified
//                  by MB_ID in a MESSAGEBOX() call.
//
//  Parameters    : UNSIGNEDLONG MB_ID -
//                        The ID of the message to be formatted
//                        and displayed.
//                  INTEGER Num_Numbers -
//                        The number of REAL parameters to be
//                        substituted into the string.
//                  REAL Number_Parms[] -
//                        An array containing the REAL
//                        parameters to be substituted into the
//                        string.
//                  INTEGER Num_Strings -
//                        The number of string parameters to be
//                        substituted into the string.
//                  STRING String_Parms[] -
//                        An array containing the string
//                        parameters to be substituted into the
//                        string.
//  Return Value  : INTEGER -
//                        The result returned by the call to
//                        MESSAGEBOX() call.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Answer
STRING   l_Text
STRING   l_Title
ICON     l_Icon
BUTTON   l_ButtonSet
INTEGER  l_DefButton
BOOLEAN  l_Enabled

//------------------------------------------------------------------
//  Get the parameters for the message box.
//------------------------------------------------------------------

fu_GetInfo(MB_ID,                       &
           Num_Numbers, Number_Parms[], &
           Num_Strings, String_Parms[], &
           l_Title,     l_Text,         &
           l_Icon,      l_ButtonSet,    &
           l_DefButton, l_Enabled)

//------------------------------------------------------------------
//  If the message is enabled, then do the MESSAGEBOX().
//------------------------------------------------------------------

IF l_Enabled THEN
   fu_SetInMB(TRUE)
   l_Answer = g_mb.MessageBox(l_Title,     l_Text,     l_Icon, l_ButtonSet, l_DefButton)
   fu_SetInMB(FALSE)
ELSE
   l_Answer = l_DefButton
END IF

RETURN l_Answer
end function

public function string fu_quotechar (string fix_string, string quote_chr);//******************************************************************
//  PO Module     : n_MB_Main
//  Function      : fu_QuoteChar
//  Description   : Adds the PowerBuilder escape character ("~")
//                  to prevent PowerBuilder from doing special
//                  interpetion of the character specifed by
//                  Quote_Chr.
//
//  Parameters    : STRING Fix_String -
//                     The string that needs to be quoted.
//
//                  STRING Quote_Chr
//                     The character to be quoted.
//
//  Return Value  : STRING -
//                     The result of the quoting operation.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN  l_Quoted
INTEGER  l_QuotePos,   l_StartPos, l_Idx
STRING   l_QuotedChar, l_FixedStr, l_TmpChar

//------------------------------------------------------------------
//  See if there are any characters to be quoted in Fix_String.
//  If there are not, then we can just return Fix_String.
//------------------------------------------------------------------

l_QuotePos = Pos(Fix_String, Quote_Chr)
IF l_QuotePos = 0 THEN
   l_FixedStr = Fix_String
   GOTO Finished
END IF

//------------------------------------------------------------------
//  For every character to be quoted in Fix_String, we need to
//  precede it with the PowerBuilder quote character (~), if it
//  has not already been quoted.
//------------------------------------------------------------------

l_FixedStr   = ""
l_StartPos   = 1
l_QuotedChar = "~~" + Quote_Chr

DO WHILE l_QuotePos > 0

   //---------------------------------------------------------------
   //  We found a character that needs to be quoted.  Make sure
   //  that it is not already quoted.
   //---------------------------------------------------------------

   l_Quoted = FALSE
   IF l_QuotePos > 1 THEN
      l_Idx     = l_QuotePos - 1
      l_TmpChar = Mid(Fix_String, l_Idx, 1)

      DO WHILE l_TmpChar = "~~"
         l_Quoted = (NOT l_Quoted)
         l_Idx    = l_Idx - 1
         IF l_Idx > 0 THEN
            l_TmpChar = Mid(Fix_String, l_Idx, 1)
         ELSE
            l_TmpChar = ""
         END IF
      LOOP
   END IF

   //---------------------------------------------------------------
   //  If the character has not already been quoted, then add
   //  the string and the quoted character.
   //---------------------------------------------------------------

   IF NOT l_Quoted THEN
      l_FixedStr = l_FixedStr +                   &
                   Mid(Fix_String, l_StartPos,    &
                       l_QuotePos - l_StartPos) + &
                   l_QuotedChar
      l_StartPos = l_QuotePos + 1
   END IF

   //---------------------------------------------------------------
   //  Find the next character to be quoted.
   //---------------------------------------------------------------

   l_QuotePos = Pos(Fix_String, Quote_Chr, l_QuotePos + 1)
LOOP

//------------------------------------------------------------------
//  Add what remains of the string after the last quoted character.
//------------------------------------------------------------------

l_FixedStr = l_FixedStr + Mid(Fix_String, l_StartPos)

Finished:

RETURN l_FixedStr
end function

public subroutine fu_setinmb (boolean in_or_out);//******************************************************************
//  PO Module     : n_MB_Main
//  Function      : fu_SetInMB
//  Description   : Sets the shared s_InMB flag.
//
//  Parameters    : BOOLEAN  In_Or_Out -
//                        Indicates if a message box is starting
//                        executing or finishing execution.
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

s_InMB = In_Or_Out
end subroutine

public function string fu_getdatabasedescrip (transaction trans_obj);//******************************************************************
//  PO Module     : n_MB_Main
//  Function      : fu_GetDatabaseDescrip
//  Description   : Returns a string identifying the type
//                  of database.
//
//  Parameters    : TRANSACTION  Trans_Obj -
//                        The transaction object used to connect
//                        the database.
//
//  Return Value  : STRING -
//                       Specifies the type of database that
//                       is being accessed by Trans_Obj.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1995.  All Rights Reserved.
//******************************************************************

BOOLEAN       l_IsODBC
UNSIGNEDLONG  l_DBType
STRING        l_ODBC, l_Return

//------------------------------------------------------------------
//  Get what type of database we are connected to.
//------------------------------------------------------------------

fu_GetDatabaseType(Trans_Obj, l_DBType, l_IsODBC)

//------------------------------------------------------------------
//  See if we are connected to some sort of ODBC database.
//------------------------------------------------------------------

IF l_IsODBC THEN

   //---------------------------------------------------------------
   //  If we don't know what type of ODBC database that we are
   //  connected to, look in the "Data Sources" section of the
   //  ODBC INI file to determine the type of database.
   //---------------------------------------------------------------

   IF l_DBType = c_DB_ODBC THEN
      l_ODBC = (ProfileString(w_POManager_Std.EXT.c_ODBCFile, &
                    "ODBC Data Sources", &
                    Trans_Obj.DataBase,  &
                    "[NO_ODBC_DSN]"))
      IF l_ODBC = "[NO_ODBC_DSN]" THEN
         l_ODBC = (ProfileString(w_POManager_Std.EXT.c_ODBCFile, &
                       "ODBC 32 bit Data Sources", &
                       Trans_Obj.DataBase,  &
                       "[NO_ODBC_DSN]"))
      END IF

      l_Return = c_DB_Names[c_DB_ODBC] + "/" + l_ODBC
   ELSE
      l_Return = c_DB_Names[c_DB_ODBC] + "/" + c_DB_Names[l_DBType]
   END IF
ELSE
   IF l_DBType = c_DB_Unknown THEN
      l_Return = Trans_Obj.DBMS
   ELSE
      l_Return = c_DB_Names[l_DBType]
   END IF
END IF

RETURN l_Return
end function

on constructor;//******************************************************************
//  PO Module     : n_MB_Main
//  Event         : Constructor
//  Description   : Initializes the n_MB_Main constants.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

SetNull(c_MB_UseDefaultTitle)
SetNull(c_MB_UseClassTitle)
SetNull(c_MB_UseClassIcon)
SetNull(c_MB_NoClassIcon)

c_DB_Names[1] = "Unknown"
c_DB_Names[2] = "HP Allbase/SQL"
c_DB_Names[3] = "Micro Decisionware Gateway/DB2"
c_DB_Names[4] = "Gutpa SQLBase"
c_DB_Names[5] = "IBM DRDA"
c_DB_Names[6] = "Informix 4 SQL"
c_DB_Names[7] = "Informix 5 SQL"
c_DB_Names[8] = "ODBC"
c_DB_Names[9] = "Oracle 6 SQL"
c_DB_Names[10] = "Oracle 7 SQL"
c_DB_Names[11] = "SQL Server"
c_DB_Names[12] = "Sybase SQL"
c_DB_Names[13] = "Sybase Net-Gateway/DB2"
c_DB_Names[14] = "XDB"
c_DB_Names[15] = "Watcom SQL"
c_DB_Names[16] = "Digital RDB"
c_DB_Names[17] = "Sybase SQL 4.9" 

end on

on n_mb_main.create
call super::create
TriggerEvent( this, "constructor" )
end on

on n_mb_main.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

