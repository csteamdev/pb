﻿$PBExportHeader$n_po_mb_main.sru
$PBExportComments$Descendant messaging object for PowerObjects
forward
global type n_po_mb_main from n_mb_main
end type
end forward

global type n_po_mb_main from n_mb_main
end type
global n_po_mb_main n_po_mb_main

type variables
//------------------------------------------------------------------------------
//  PowerObjects core error messages:
//------------------------------------------------------------------------------

UNSIGNEDLONG	c_MBC_CoreError 		= 2

//------------------------------------------------------------------------------
//  PowerObjects folder error messages:
//------------------------------------------------------------------------------

UNSIGNEDLONG	c_MBC_FolderError 	= 3

//------------------------------------------------------------------------------
//  PowerObjects outliner error messages:
//------------------------------------------------------------------------------

UNSIGNEDLONG	c_MBC_OutlinerError 	= 4

UNSIGNEDLONG	c_MBI_DataTypeError 	= 2

//------------------------------------------------------------------------------
//  PowerObjects database connection error messages:
//------------------------------------------------------------------------------

UNSIGNEDLONG	c_MBC_DatabaseError 	= 5

UNSIGNEDLONG	c_MBI_ConnectError 	= 3
UNSIGNEDLONG	c_MBI_INIFileError 		= 4
UNSIGNEDLONG	c_MBI_GraceError 		= 5

//------------------------------------------------------------------------------
//  PowerObjects file save as error messages:
//------------------------------------------------------------------------------

UNSIGNEDLONG	c_MBC_FileSaveAsError 	= 6

UNSIGNEDLONG	c_MBI_FileMissingError 	= 6
UNSIGNEDLONG	c_MBI_FileExists 		= 7

//------------------------------------------------------------------------------
//  PowerObjects database cursor error messages:
//------------------------------------------------------------------------------

UNSIGNEDLONG	c_MBC_DBCursorError 	= 7

UNSIGNEDLONG	c_MBI_CursorDeclareError 	= 8
UNSIGNEDLONG	c_MBI_CursorPrepareError 	= 9
UNSIGNEDLONG	c_MBI_CursorOpenError 	= 10
UNSIGNEDLONG	c_MBI_CursorFetchError 	= 11
UNSIGNEDLONG	c_MBI_CursorCloseError 	= 12

//------------------------------------------------------------------------------
//  The following parameters are available for formatting
//  validation messages:
//
//     Numbers   - (None)
//
//     String #1 - <calling procedure's name>
//     String #2 - <application name>
//     String #3 - <user's input>
//
//  Additional parameters are available for formatting
//  these messages:
//
//     c_MBI_FMTValError:
//        String #4 - <pattern specification>
//
//     c_MBI_GEValError:
//     c_MBI_GTValError:
//     c_MBI_LEValError:
//     c_MBI_LengthValError:
//     c_MBI_LTValError:
//     c_MBI_MaxLenValError:
//     c_MBI_MinLenValError:
//        Number #1 - <comparison value>
//
//  The following messages do not provide the standard
//  set of message parameters:
//
//     c_MBI_RequiredField:
//     c_MBI_NonVisRequiredField:
//     c_MBI_OKDevValError:
//     c_MBI_YesNoDevValError:
//     c_MBI_OKFMTDevValError:
//     c_MBI_YesNoFMTDevValError:
//        Numbers   - <developer specified>
//        Strings   - <developer specified>
//-----------------------------------------------------------------------------

UNSIGNEDLONG	c_MBC_ValidationError 	= 8

UNSIGNEDLONG	c_MBI_DOMValError 	= 13
UNSIGNEDLONG	c_MBI_DOWValError 	= 14
UNSIGNEDLONG	c_MBI_DateValError 	= 15
UNSIGNEDLONG	c_MBI_DecValError 	= 16
UNSIGNEDLONG	c_MBI_FMTValError 	= 17
UNSIGNEDLONG	c_MBI_GEValError 		= 18
UNSIGNEDLONG	c_MBI_GTValError 		= 19
UNSIGNEDLONG	c_MBI_IntValError 		= 20
UNSIGNEDLONG	c_MBI_LEValError 		= 21
UNSIGNEDLONG	c_MBI_LTValError 		= 22
UNSIGNEDLONG	c_MBI_LengthValError 	= 23
UNSIGNEDLONG	c_MBI_MaxLenValError 	= 24
UNSIGNEDLONG	c_MBI_MinLenValError 	= 25
UNSIGNEDLONG	c_MBI_MonValError 	= 26
UNSIGNEDLONG	c_MBI_PhoneValError 	= 27
UNSIGNEDLONG	c_MBI_TimeValError 	= 28
UNSIGNEDLONG	c_MBI_YearValError 	= 29
UNSIGNEDLONG	c_MBI_ZipValError 		= 30
UNSIGNEDLONG	c_MBI_RequiredField 	= 31
UNSIGNEDLONG	c_MBI_NonVisRequiredField 	= 32
UNSIGNEDLONG	c_MBI_OKDevValError 	= 33
UNSIGNEDLONG	c_MBI_YesNoDevValError 	= 34
UNSIGNEDLONG	c_MBI_OKFMTDevValError 	= 35
UNSIGNEDLONG	c_MBI_YesNoFMTDevValError = 36

//------------------------------------------------------------------------------
//  PowerObjects search object error messages:
//------------------------------------------------------------------------------

UNSIGNEDLONG	c_MBC_SearchError 	= 9

UNSIGNEDLONG	c_MBI_DDDWChildFindError 	= 37
UNSIGNEDLONG	c_MBI_DDDWInsertRowError = 38
UNSIGNEDLONG   c_MBI_DDDWRetrieveError 	= 39
UNSIGNEDLONG	c_MBI_DDDWTransactionError = 40
UNSIGNEDLONG	c_MBI_DDDWModifySQLError = 41

//------------------------------------------------------------------------------
//  PowerObjects evaluation error messages:
//------------------------------------------------------------------------------

UNSIGNEDLONG	c_MBC_EvaluationError 	= 10

UNSIGNEDLONG	c_MBI_EvalExpired 	= 42
UNSIGNEDLONG	c_MBI_BadEvalCode 	= 43


end variables

on constructor;call n_mb_main::constructor;//******************************************************************
//  PO Module     : n_PO_MB_Main
//  Event         : Constructor
//  Description   : Initializes the n_PO_MB_Main constants.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Idx

c_MB_MaxBaseClass = 10
c_MB_MaxBaseID 	= 43

//------------------------------------------------------------------
//  Initialize the Class titles.
//------------------------------------------------------------------

FOR l_Idx = c_MB_MaxBaseClass TO 1 STEP -1
   c_ClassTitle[l_Idx] = c_MB_UseDefaultTitle
NEXT

//------------------------------------------------------------------
//  Initialize the message IDs.
//------------------------------------------------------------------

FOR l_Idx = c_MB_MaxBaseID TO 1 STEP -1
   c_MessageTitle[l_Idx] = c_MB_UseClassTitle
   c_MessageIcon[l_Idx]  = c_MB_UseClassIcon
NEXT

c_DefaultTitle[1]  = "Undefined Class Title"
c_DefaultTitle[2]  = "PowerObjects"
c_DefaultTitle[3]  = "PowerObjects Folder"
c_DefaultTitle[4]  = "PowerObjects Outliner"
c_DefaultTitle[5]  = "Database Connection"
c_DefaultTitle[6]  = "File Save As"
c_DefaultTitle[7]  = "Database Cursor"
c_DefaultTitle[8]  = "Validation Error"
c_DefaultTitle[9]  = "PowerObjects Search Object"
c_DefaultTitle[10] = "Evaluation Error"

c_ClassIcon[1]     = StopSign!
c_ClassIcon[2]     = StopSign!
c_ClassIcon[3]     = StopSign!
c_ClassIcon[4]     = Exclamation!
c_ClassIcon[5]     = Exclamation!
c_ClassIcon[6]     = Exclamation!
c_ClassIcon[7]     = Exclamation!
c_ClassIcon[8]     = Exclamation!
c_ClassIcon[9]     = Exclamation!
c_ClassIcon[10]    = StopSign!

c_MessageClass[1]  = 1
c_MessageClass[2]  = 4
c_MessageClass[3]  = 5 
c_MessageClass[4]  = 5 
c_MessageClass[5]  = 5
c_MessageClass[6]  = 6 
c_MessageClass[7]  = 6
c_MessageClass[8]  = 7 
c_MessageClass[9]  = 7 
c_MessageClass[10] = 7 
c_MessageClass[11] = 7 
c_MessageClass[12] = 7
c_MessageClass[13] = 8 
c_MessageClass[14] = 8 
c_MessageClass[15] = 8 
c_MessageClass[16] = 8 
c_MessageClass[17] = 8 
c_MessageClass[18] = 8 
c_MessageClass[19] = 8 
c_MessageClass[20] = 8 
c_MessageClass[21] = 8 
c_MessageClass[22] = 8 
c_MessageClass[23] = 8 
c_MessageClass[24] = 8 
c_MessageClass[25] = 8
c_MessageClass[26] = 8 
c_MessageClass[27] = 8 
c_MessageClass[28] = 8 
c_MessageClass[29] = 8 
c_MessageClass[30] = 8 
c_MessageClass[31] = 8 
c_MessageClass[32] = 8 
c_MessageClass[33] = 8 
c_MessageClass[34] = 8 
c_MessageClass[35] = 8 
c_MessageClass[36] = 8
c_MessageClass[37] = 9 
c_MessageClass[38] = 9 
c_MessageClass[39] = 9 
c_MessageClass[40] = 9 
c_MessageClass[41] = 9 
c_MessageClass[42] = 10 
c_MessageClass[43] = 10

c_MessageText[1]   =	"Undefined"
c_MessageText[2]   =	"Could not determine the type of database that is being used.  If the database connection is through ODBC, the problem could be the database type could not be determined from DBParm."
c_MessageText[3]   =	"Unable to connect to the database:~n~n%1s"
c_MessageText[4]   =	"Unable to find the '%1s' INI file."
c_MessageText[5]   =	"The number of grace logins has been exceded."
c_MessageText[6]   =	"No file name has been entered."
c_MessageText[7]   =	"Replace existing '%1s' file?"
c_MessageText[8]   =	"Could not declare a dynamic cursor:~n~n%1s"
c_MessageText[9]   =	"Could not prepare the dynamic cusor:~n~n%1s"
c_MessageText[10]  =	"Could not open the dynamic cursor:~n~n%1s"
c_MessageText[11]  =	"Could not fetch a record from the dynamic cursor:~n~n%1s"
c_MessageText[12]  =	"Could not close the dynamic cursor:~n~n%1s"
c_MessageText[13]  =	"Field contains an illegal~nday of the month (%3s)."
c_MessageText[14]  =	"Field contains an illegal day~nof the week (%3s)."
c_MessageText[15]  =	"Field contains an invalid~ndate value (%3s)."
c_MessageText[16]  =	"Field contains an invalid~ndecimal value (%3s)."
c_MessageText[17]  =	"Field contains a value~nthat is not in the correct~nformat of ~"%4s~"."
c_MessageText[18]  =	"Field contains a value~nless than %1d."
c_MessageText[19]  =	"Field contains a value less than~nor equal to %1d."
c_MessageText[20]  =	"Field contains an invalid~ninteger value (%3s)."
c_MessageText[21]  =	"Field contains a value~ngreater than %1d."
c_MessageText[22]  =	"Field contains a value greater~nthan or equal to %1d."
c_MessageText[23]  =	"Field should be %1d~ncharacters long."
c_MessageText[24]  =	"Field must be less than %1d~ncharacters long."
c_MessageText[25]  =	"Field must be at least %1d~ncharacters long."
c_MessageText[26]  =	"Field contains an illegal~nmonth (%3s)."
c_MessageText[27]  =	"Field contains an invalid phone~nnumber (%3s)."
c_MessageText[28]  =	"Field contains an invalid~ntime value (%3s)."
c_MessageText[29]  =	"Field contains an illegal~nyear (%3s)."
c_MessageText[30]  =	"Field contains an invalid~nzip code (%3s)."
c_MessageText[31]  =	"Field is a required field."
c_MessageText[32]  =	"Non-visible field is a required field."
c_MessageText[33]  =	"%1s"
c_MessageText[34]  =	"%1s"
c_MessageText[35]  =	"%1s"
c_MessageText[36]  =	"%1s"
c_MessageText[37]  =	"Could not get child Drop Down DataWindow."
c_MessageText[38]  =	"Could not insert row into Drop Down DataWindow."
c_MessageText[39]  =	"Could not retrieve data into Drop Down DataWindow."
c_MessageText[40]  =	"Could not set the transaction object for Drop Down DataWindow."
c_MessageText[41]  =	"Could not modify the SQL for the Drop Down DataWindow."
c_MessageText[42]  =	"Your evaluation period has expired.  Please~ncontact your ServerLogic representative to~norder the product."
c_MessageText[43]  =	"An evaluation code could not be found.~nPlease contact your ServerLogic representative for help."

c_MessageButtonSet[1]  = Ok!
c_MessageButtonSet[2]  = Ok!
c_MessageButtonSet[3]  = Ok! 
c_MessageButtonSet[4]  = Ok! 
c_MessageButtonSet[5]  = Ok!
c_MessageButtonSet[6]  = Ok!
c_MessageButtonSet[7]  = YesNo!
c_MessageButtonSet[8]  = Ok!
c_MessageButtonSet[9]  = Ok! 
c_MessageButtonSet[10] = Ok! 
c_MessageButtonSet[11] = Ok! 
c_MessageButtonSet[12] = Ok!
c_MessageButtonSet[13] = Ok! 
c_MessageButtonSet[14] = Ok! 
c_MessageButtonSet[15] = Ok! 
c_MessageButtonSet[16] = Ok! 
c_MessageButtonSet[17] = Ok! 
c_MessageButtonSet[18] = Ok!
c_MessageButtonSet[19] = Ok! 
c_MessageButtonSet[20] = Ok! 
c_MessageButtonSet[21] = Ok! 
c_MessageButtonSet[22] = Ok! 
c_MessageButtonSet[23] = Ok! 
c_MessageButtonSet[24] = Ok!
c_MessageButtonSet[25] = Ok! 
c_MessageButtonSet[26] = Ok! 
c_MessageButtonSet[27] = Ok! 
c_MessageButtonSet[28] = Ok! 
c_MessageButtonSet[29] = Ok! 
c_MessageButtonSet[30] = Ok!
c_MessageButtonSet[31] = Ok! 
c_MessageButtonSet[32] = Ok! 
c_MessageButtonSet[33] = Ok! 
c_MessageButtonSet[34] = Ok! 
c_MessageButtonSet[35] = Ok! 
c_MessageButtonSet[36] = Ok!
c_MessageButtonSet[37] = Ok! 
c_MessageButtonSet[38] = Ok! 
c_MessageButtonSet[39] = Ok! 
c_MessageButtonSet[40] = Ok! 
c_MessageButtonSet[41] = Ok! 
c_MessageButtonSet[42] = Ok! 
c_MessageButtonSet[43] = Ok!

c_MessageDefButton[1]  = 1
c_MessageDefButton[2]  = 1
c_MessageDefButton[3]  = 1 
c_MessageDefButton[4]  = 1 
c_MessageDefButton[5]  = 1
c_MessageDefButton[6]  = 1 
c_MessageDefButton[7]  = 2
c_MessageDefButton[8]  = 1 
c_MessageDefButton[9]  = 1 
c_MessageDefButton[10] = 1
c_MessageDefButton[11] = 1 
c_MessageDefButton[12] = 1
c_MessageDefButton[13] = 1 
c_MessageDefButton[14] = 1 
c_MessageDefButton[15] = 1 
c_MessageDefButton[16] = 1 
c_MessageDefButton[17] = 1 
c_MessageDefButton[18] = 1 
c_MessageDefButton[19] = 1 
c_MessageDefButton[20] = 1 
c_MessageDefButton[21] = 1 
c_MessageDefButton[22] = 1 
c_MessageDefButton[23] = 1 
c_MessageDefButton[24] = 1
c_MessageDefButton[25] = 1 
c_MessageDefButton[26] = 1 
c_MessageDefButton[27] = 1 
c_MessageDefButton[28] = 1 
c_MessageDefButton[29] = 1 
c_MessageDefButton[30] = 1 
c_MessageDefButton[31] = 1 
c_MessageDefButton[32] = 1 
c_MessageDefButton[33] = 1 
c_MessageDefButton[34] = 1 
c_MessageDefButton[35] = 1 
c_MessageDefButton[36] = 1
c_MessageDefButton[37] = 1 
c_MessageDefButton[38] = 1 
c_MessageDefButton[39] = 1 
c_MessageDefButton[40] = 1 
c_MessageDefButton[41] = 1 
c_MessageDefButton[42] = 1 
c_MessageDefButton[43] = 1

c_MessageEnabled[1]  = TRUE
c_MessageEnabled[2]  = TRUE
c_MessageEnabled[3]  = TRUE 
c_MessageEnabled[4]  = TRUE 
c_MessageEnabled[5]  = TRUE
c_MessageEnabled[6]  = TRUE 
c_MessageEnabled[7]  = TRUE
c_MessageEnabled[8]  = TRUE 
c_MessageEnabled[9]  = TRUE 
c_MessageEnabled[10] = TRUE 
c_MessageEnabled[11] = TRUE 
c_MessageEnabled[12] = TRUE
c_MessageEnabled[13] = TRUE 
c_MessageEnabled[14] = TRUE 
c_MessageEnabled[15] = TRUE 
c_MessageEnabled[16] = TRUE 
c_MessageEnabled[17] = TRUE 
c_MessageEnabled[18] = TRUE
c_MessageEnabled[19] = TRUE 
c_MessageEnabled[20] = TRUE 
c_MessageEnabled[21] = TRUE 
c_MessageEnabled[22] = TRUE 
c_MessageEnabled[23] = TRUE 
c_MessageEnabled[24] = TRUE
c_MessageEnabled[25] = TRUE 
c_MessageEnabled[26] = TRUE 
c_MessageEnabled[27] = TRUE 
c_MessageEnabled[28] = TRUE 
c_MessageEnabled[29] = TRUE 
c_MessageEnabled[30] = TRUE
c_MessageEnabled[31] = TRUE 
c_MessageEnabled[32] = TRUE 
c_MessageEnabled[33] = TRUE 
c_MessageEnabled[34] = TRUE 
c_MessageEnabled[35] = TRUE 
c_MessageEnabled[36] = TRUE
c_MessageEnabled[37] = TRUE 
c_MessageEnabled[38] = TRUE 
c_MessageEnabled[39] = TRUE 
c_MessageEnabled[40] = TRUE 
c_MessageEnabled[41] = TRUE 
c_MessageEnabled[42] = TRUE
c_MessageEnabled[43] = TRUE

//------------------------------------------------------------------
//  Define the title for the "Undefined" Message ID.
//------------------------------------------------------------------

c_MessageTitle[c_MBI_Undefined] = "Undefined Message ID!"
c_MessageIcon[c_MBI_Undefined]  = StopSign!

TriggerEvent("pc_Check")

end on

on n_po_mb_main.create
TriggerEvent( this, "constructor" )
end on

on n_po_mb_main.destroy
TriggerEvent( this, "destructor" )
end on

