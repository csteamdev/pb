﻿$PBExportHeader$n_po_mb_std.sru
$PBExportComments$Standards descendant object for customizing PowerObjects messages
forward
global type n_po_mb_std from n_po_mb_main
end type
end forward

global type n_po_mb_std from n_po_mb_main
end type
global n_po_mb_std n_po_mb_std

on n_po_mb_std.create
TriggerEvent( this, "constructor" )
end on

on n_po_mb_std.destroy
TriggerEvent( this, "destructor" )
end on

