﻿$PBExportHeader$w_pomanager_main.srw
$PBExportComments$Ancestor PowerObjects manager
forward
global type w_pomanager_main from Window
end type
type st_1 from statictext within w_pomanager_main
end type
end forward

type s_parms from structure
    string application_name
    string application_rev
    string application_bitmap
    string application_copyright
    string replace_file
    string with_file
    boolean multiple
    integer answer
    string from_directory
    string to_directory
    string files[]
    boolean copy_file
    boolean display
    datawindow dw_name
    string current_directory
    string login_title
    integer grace_logins
    boolean attempt_connection
    string login_bitmap
    transaction trans_object
    string message_title
    string message_text
    boolean display_only
    unsignedlong back_color
    unsignedlong text_color
    window mdi_frame
    integer update_seconds
    boolean show_clock
    boolean show_resources
    string title
    string label
    string action
end type

global type w_pomanager_main from Window
int X=682
int Y=481
int Width=723
int Height=361
boolean Visible=false
boolean TitleBar=true
string Title="PowerObject Manager"
long BackColor=12632256
st_1 st_1
end type
global w_pomanager_main w_pomanager_main

type prototypes

end prototypes

type variables
//-------------------------------------------------------------------------------
//  Non-visual objects.
//-------------------------------------------------------------------------------

n_PO_MB_Main	MB
n_EXT_Main	EXT

//-------------------------------------------------------------------------------
//  System-wide instance variables.
//-------------------------------------------------------------------------------

ENVIRONMENT	ENV
S_PARMS	PARM

BOOLEAN	i_TimerOn	= FALSE
INTEGER		i_TimerCnt
STRING		i_TimerLabel[]
TIME		i_TimerTime[]

STRING		i_ApplicationName

//-------------------------------------------------------------------------------
//  Utility window instance variables.
//-------------------------------------------------------------------------------

STRING		i_WindowTextFont		= "Arial"
INTEGER		i_WindowTextSize		= -9
ULONG		i_WindowColor		= 12632256
ULONG		i_WindowTextColor		= 8388608

//-------------------------------------------------------------------------------
//  Constants that are used across all objects.
//-------------------------------------------------------------------------------

ULONG		c_DefaultVisual		= 0
STRING		c_DefaultFont		= ""
INTEGER		c_DefaultSize		= 0

ULONG		c_Black			= 0
ULONG		c_White			= 16777215
ULONG		c_Gray			= 12632256
ULONG		c_DarkGray		= 8421504
ULONG		c_Red			= 255
ULONG		c_DarkRed		= 128
ULONG		c_Green			= 65280
ULONG		c_DarkGreen		= 32768
ULONG		c_Blue			= 16711680
ULONG		c_DarkBlue		= 8388608
ULONG		c_Magenta		= 16711935
ULONG		c_DarkMagenta		= 8388736
ULONG		c_Cyan			= 16776960
ULONG		c_DarkCyan		= 8421376
ULONG		c_Yellow			= 65535
ULONG		c_DarkYellow		= 32896

//-------------------------------------------------------------------------------
//  Constants for setting options for the Folder object.
//-------------------------------------------------------------------------------

ULONG		c_FolderTabTop		= 0
ULONG		c_FolderTabBottom		= 1
ULONG		c_FolderTabLeft		= 2
ULONG		c_FolderTabRight		= 3

ULONG		c_Folder3D		= 0
ULONG		c_Folder2D		= 4

ULONG		c_FolderGray		= 0
ULONG		c_FolderBlack		= 8
ULONG		c_FolderWhite		= 16
ULONG		c_FolderDarkGray		= 24
ULONG		c_FolderRed		= 32
ULONG		c_FolderDarkRed		= 40
ULONG		c_FolderGreen		= 48
ULONG		c_FolderDarkGreen		= 56
ULONG		c_FolderBlue		= 64
ULONG		c_FolderDarkBlue		= 72
ULONG		c_FolderMagenta		= 80
ULONG		c_FolderDarkMagenta	= 88
ULONG		c_FolderCyan		= 96
ULONG		c_FolderDarkCyan		= 104
ULONG		c_FolderYellow		= 112
ULONG		c_FolderDarkYellow	= 120

ULONG		c_FolderBGGray		= 0
ULONG		c_FolderBGBlack		= 128
ULONG		c_FolderBGWhite		= 256
ULONG		c_FolderBGDarkGray	= 384
ULONG		c_FolderBGRed		= 512
ULONG		c_FolderBGDarkRed	= 640
ULONG		c_FolderBGGreen		= 768
ULONG		c_FolderBGDarkGreen	= 896
ULONG		c_FolderBGBlue		= 1024
ULONG		c_FolderBGDarkBlue	= 1152
ULONG		c_FolderBGMagenta	= 1280
ULONG		c_FolderBGDarkMagenta	= 1408
ULONG		c_FolderBGCyan		= 1536
ULONG		c_FolderBGDarkCyan	= 1664
ULONG		c_FolderBGYellow		= 1792
ULONG		c_FolderBGDarkYellow	= 1920

ULONG		c_FolderBorderOn		= 0
ULONG		c_FolderBorderOff		= 2048

ULONG		c_FolderResizeOn		= 0
ULONG		c_FolderResizeOff		= 4096

ULONG		c_TextRegular		= 0
ULONG		c_TextBold		= 1
ULONG		c_TextItalic		= 2
ULONG		c_TextUnderline		= 3

ULONG		c_TextCenter		= 0
ULONG		c_TextLeft		= 4
ULONG		c_TextRight		= 8

ULONG		c_TextBlack		= 0
ULONG		c_TextWhite		= 16
ULONG		c_TextGray		= 32
ULONG		c_TextDarkGray		= 48
ULONG		c_TextRed		= 64
ULONG		c_TextDarkRed		= 80
ULONG		c_TextGreen		= 96
ULONG		c_TextDarkGreen		= 112
ULONG		c_TextBlue		= 128
ULONG		c_TextDarkBlue		= 144
ULONG		c_TextMagenta		= 160
ULONG		c_TextDarkMagenta	= 176
ULONG		c_TextCyan		= 192
ULONG		c_TextDarkCyan		= 208
ULONG		c_TextYellow		= 224
ULONG		c_TextDarkYellow		= 240

ULONG		c_TextRotationOff		= 0
ULONG		c_TextRotationOn		= 256

ULONG		c_TextHighlightWhite	= 0
ULONG		c_TextHighlightBlack	= 512
ULONG		c_TextHighlightGray	= 1024
ULONG		c_TextHighlightDarkGray	= 1536
ULONG		c_TextHighlightRed	= 2048
ULONG		c_TextHighlightDarkRed	= 2560
ULONG		c_TextHighlightGreen	= 3072
ULONG		c_TextHighlightDarkGreen	= 3584
ULONG		c_TextHighlightBlue	= 4096
ULONG		c_TextHighlightDarkBlue	= 4608
ULONG		c_TextHighlightMagenta	= 5120
ULONG		c_TextHighlightDarkMagenta	= 5632
ULONG		c_TextHighlightCyan	= 6144
ULONG		c_TextHighlightDarkCyan	= 6656
ULONG		c_TextHighlightYellow	= 7168
ULONG		c_TextHighlightDarkYellow	= 7680

ULONG		c_TextCurrentBold		= 0
ULONG		c_TextCurrentRegular	= 1
ULONG		c_TextCurrentItalic		= 2
ULONG		c_TextCurrentUnderline	= 3

ULONG		c_TextCurrentBlack	= 0
ULONG		c_TextCurrentWhite	= 4
ULONG		c_TextCurrentGray		= 8
ULONG		c_TextCurrentDarkGray	= 12
ULONG		c_TextCurrentRed		= 16
ULONG		c_TextCurrentDarkRed	= 20
ULONG		c_TextCurrentGreen	= 24
ULONG		c_TextCurrentDarkGreen	= 28
ULONG		c_TextCurrentBlue		= 32
ULONG		c_TextCurrentDarkBlue	= 36
ULONG		c_TextCurrentMagenta	= 40
ULONG		c_TextCurrentDarkMagenta	= 44
ULONG		c_TextCurrentCyan		= 48
ULONG		c_TextCurrentDarkCyan	= 52
ULONG		c_TextCurrentYellow	= 56
ULONG		c_TextCurrentDarkYellow	= 60

ULONG		c_TabCurrentGray		= 0
ULONG		c_TabCurrentBlack		= 64
ULONG		c_TabCurrentWhite		= 128
ULONG		c_TabCurrentDarkGray	= 192
ULONG		c_TabCurrentRed		= 256
ULONG		c_TabCurrentDarkRed	= 320
ULONG		c_TabCurrentGreen		= 384
ULONG		c_TabCurrentDarkGreen	= 448
ULONG		c_TabCurrentBlue		= 512
ULONG		c_TabCurrentDarkBlue	= 576
ULONG		c_TabCurrentMagenta	= 640
ULONG		c_TabCurrentDarkMagenta	= 704
ULONG		c_TabCurrentCyan		= 768
ULONG		c_TabCurrentDarkCyan	= 832
ULONG		c_TabCurrentYellow	= 896
ULONG		c_TabCurrentDarkYellow	= 960

ULONG		c_TabCurrentColorFolder 	= 0
ULONG		c_TabCurrentColorBorder 	= 1024
ULONG		c_TabCurrentColorTab	= 2048

ULONG		c_TextDisableItalic		= 0
ULONG		c_TextDisableBold		= 1
ULONG		c_TextDisableRegular	= 2
ULONG		c_TextDisableUnderline	= 3

ULONG		c_TextDisableDarkGray	= 0
ULONG		c_TextDisableWhite	= 4
ULONG		c_TextDisableGray		= 8
ULONG		c_TextDisableBlack	= 12
ULONG		c_TextDisableRed		= 16
ULONG		c_TextDisableDarkRed	= 20
ULONG		c_TextDisableGreen	= 24
ULONG		c_TextDisableDarkGreen	= 28
ULONG		c_TextDisableBlue		= 32
ULONG		c_TextDisableDarkBlue	= 36
ULONG		c_TextDisableMagenta	= 40
ULONG		c_TextDisableDarkMagenta	= 44
ULONG		c_TextDisableCyan		= 48
ULONG		c_TextDisableDarkCyan	= 52
ULONG		c_TextDisableYellow	= 56
ULONG		c_TextDisableDarkYellow	= 60

//-------------------------------------------------------------------------------
//  Constants for setting options for the Outliner object.
//-------------------------------------------------------------------------------

ULONG		c_HLWhite		= 0
ULONG		c_HLBlack		= 1
ULONG		c_HLGray		= 2
ULONG		c_HLDarkGray		= 3
ULONG		c_HLRed			= 4
ULONG		c_HLDarkRed		= 5
ULONG		c_HLGreen		= 6
ULONG		c_HLDarkGreen		= 7
ULONG		c_HLBlue			= 8
ULONG		c_HLDarkBlue		= 9
ULONG		c_HLMagenta		= 10
ULONG		c_HLDarkMagenta		= 11
ULONG		c_HLCyan		= 12
ULONG		c_HLDarkCyan		= 13
ULONG		c_HLYellow		= 14
ULONG		c_HLDarkYellow		= 15

ULONG		c_LineBlack		= 0
ULONG		c_LineWhite		= 16
ULONG		c_LineGray		= 32
ULONG		c_LineDarkGray		= 48
ULONG		c_LineRed		= 64
ULONG		c_LineDarkRed		= 80
ULONG		c_LineGreen		= 96
ULONG		c_LineDarkGreen		= 112
ULONG		c_LineBlue		= 128
ULONG		c_LineDarkBlue		= 144
ULONG		c_LineMagenta		= 160
ULONG		c_LineDarkMagenta	= 176
ULONG		c_LineCyan		= 192
ULONG		c_LineDarkCyan		= 208
ULONG		c_LineYellow		= 224
ULONG		c_LineDarkYellow		= 240

ULONG		c_RowFocusIndicatorBlack		= 256
ULONG		c_RowFocusIndicatorWhite		= 512
ULONG		c_RowFocusIndicatorGray		= 0
ULONG		c_RowFocusIndicatorDarkGray	= 768
ULONG		c_RowFocusIndicatorRed		= 1024
ULONG		c_RowFocusIndicatorDarkRed	= 1280
ULONG		c_RowFocusIndicatorGreen		= 1536
ULONG		c_RowFocusIndicatorDarkGreen	= 1792
ULONG		c_RowFocusIndicatorBlue		= 2048
ULONG		c_RowFocusIndicatorDarkBlue	= 2304
ULONG		c_RowFocusIndicatorMagenta	= 2560
ULONG		c_RowFocusIndicatorDarkMagenta	= 2816
ULONG		c_RowFocusIndicatorCyan		= 3072
ULONG		c_RowFocusIndicatorDarkCyan	= 3328
ULONG		c_RowFocusIndicatorYellow		= 3584
ULONG		c_RowFocusIndicatorDarkYellow	= 3840

ULONG		c_HighlightDarkBlue	= 0
ULONG		c_HighlightBlack		= 4096
ULONG		c_HighlightGray		= 8192
ULONG		c_HighlightDarkGray	= 12288
ULONG		c_HighlightRed		= 16384
ULONG		c_HighlightDarkRed	= 20480
ULONG		c_HighlightGreen		= 24576
ULONG		c_HighlightDarkGreen	= 28672
ULONG		c_HighlightBlue		= 32768
ULONG		c_HighlightWhite		= 36864
ULONG		c_HighlightMagenta	= 40960
ULONG		c_HighlightDarkMagenta	= 45056
ULONG		c_HighlightCyan		= 49152
ULONG		c_HighlightDarkCyan	= 53248
ULONG		c_HighlightYellow		= 57344
ULONG		c_HighlightDarkYellow	= 61440

ULONG		c_ShowLines		= 0
ULONG		c_HideLines		= 65536

ULONG		c_ShowBoxes		= 0
ULONG		c_HideBoxes		= 131072

ULONG		c_ShowBMP		= 0
ULONG		c_HideBMP		= 262144

ULONG		c_RetrieveAll		= 0
ULONG		c_RetrieveAsNeeded	= 524288

ULONG		c_RetainOnCollapse	= 0
ULONG		c_DeleteOnCollapse	= 1048576

ULONG		c_CollapseOnOpen		= 0
ULONG		c_ExpandOnOpen		= 2097152

ULONG		c_ShowRowFocusIndicator	= 0
ULONG		c_HideRowFocusIndicator	= 4194304

ULONG		c_ShowHighlight		= 0
ULONG		c_HideHighlight		= 8388608

ULONG		c_DrillDownOnDoubleClick	= 0
ULONG		c_DrillDownOnClick	= 16777216

ULONG		c_SingleSelect		= 0
ULONG		c_MultiSelect		= 33554432

ULONG		c_NoAllowDragDrop	= 0
ULONG		c_AllowDragDrop		= 67108864

ULONG		c_DragMoveRows		= 0
ULONG		c_DragCopyRows		= 134217728

ULONG		c_DrillDownOnLastLevel	= 0
ULONG		c_DrillDownOnLastData	= 268435456

//-------------------------------------------------------------------------------
//  Constants for setting options for the Progress Bar object.
//-------------------------------------------------------------------------------

ULONG		c_ProgressBGGray		= 0
ULONG		c_ProgressBGBlack	= 1
ULONG		c_ProgressBGWhite	= 2
ULONG		c_ProgressBGDarkGray	= 3
ULONG		c_ProgressBGRed		= 4
ULONG		c_ProgressBGDarkRed	= 5
ULONG		c_ProgressBGGreen	= 6
ULONG		c_ProgressBGDarkGreen	= 7
ULONG		c_ProgressBGBlue		= 8
ULONG		c_ProgressBGDarkBlue	= 9
ULONG		c_ProgressBGMagenta	= 10
ULONG		c_ProgressBGDarkMagenta	= 11
ULONG		c_ProgressBGCyan		= 12
ULONG		c_ProgressBGDarkCyan	= 13
ULONG		c_ProgressBGYellow	= 14
ULONG		c_ProgressBGDarkYellow	= 15

ULONG		c_ProgressGray		= 0
ULONG		c_ProgressBlack		= 16
ULONG		c_ProgressWhite		= 32
ULONG		c_ProgressDarkGray	= 48
ULONG		c_ProgressRed		= 64
ULONG		c_ProgressDarkRed	= 80
ULONG		c_ProgressGreen		= 96
ULONG		c_ProgressDarkGreen	= 112
ULONG		c_ProgressBlue		= 128
ULONG		c_ProgressDarkBlue   	= 144
ULONG		c_ProgressMagenta    	= 160
ULONG		c_ProgressDarkMagenta	= 176
ULONG		c_ProgressCyan       	= 192
ULONG		c_ProgressDarkCyan   	= 208
ULONG		c_ProgressYellow		= 224
ULONG		c_ProgressDarkYellow	= 240

ULONG		c_ProgressTextBlack	= 0
ULONG		c_ProgressTextWhite	= 256
ULONG		c_ProgressTextGray	= 512
ULONG		c_ProgressTextDarkGray	= 768
ULONG		c_ProgressTextRed		= 1024
ULONG		c_ProgressTextDarkRed	= 1280
ULONG		c_ProgressTextGreen	= 1536
ULONG		c_ProgressTextDarkGreen	= 1792
ULONG		c_ProgressTextBlue	= 2048
ULONG		c_ProgressTextDarkBlue	= 2304
ULONG		c_ProgressTextMagenta	= 2560
ULONG		c_ProgressTextDarkMagenta	= 2816
ULONG		c_ProgressTextCyan	= 3072
ULONG		c_ProgressTextDarkCyan	= 3328
ULONG		c_ProgressTextYellow	= 3584
ULONG		c_ProgressTextDarkYellow	= 3840

ULONG		c_ProgressMeterBlue	= 49152
ULONG		c_ProgressMeterWhite	= 4096
ULONG		c_ProgressMeterGray	= 8192
ULONG		c_ProgressMeterDarkGray	= 12288
ULONG		c_ProgressMeterRed	= 16384
ULONG		c_ProgressMeterDarkRed	= 20480
ULONG		c_ProgressMeterGreen	= 24576
ULONG		c_ProgressMeterDarkGreen	= 28672
ULONG		c_ProgressMeterBlack	= 32768
ULONG		c_ProgressMeterDarkBlue	= 36864
ULONG		c_ProgressMeterMagenta	= 40960
ULONG		c_ProgressMeterDarkMagenta = 45056
ULONG		c_ProgressMeterCyan	= 0
ULONG		c_ProgressMeterDarkCyan	= 53248
ULONG		c_ProgressMeterYellow	= 57344
ULONG		c_ProgressMeterDarkYellow	= 61440

ULONG		c_ProgressBorderNone	= 196608
ULONG		c_ProgressBorderShadowBox	= 65536
ULONG		c_ProgressBorderBox	= 131072
ULONG		c_ProgressBorderLowered	= 0
ULONG		c_ProgressBorderRaised	= 262144

ULONG		c_ProgressDirectionHoriz	= 0
ULONG		c_ProgressDirectionVert	= 524288

ULONG		c_ProgressPercentShow	= 0
ULONG		c_ProgressPercentHide	= 1048576

//-------------------------------------------------------------------------------
//  Constants for setting options for the Slider Bar object.
//-------------------------------------------------------------------------------

ULONG		c_SliderBGGray		= 0
ULONG		c_SliderBGBlack		= 1
ULONG		c_SliderBGWhite		= 2
ULONG		c_SliderBGDarkGray	= 3
ULONG		c_SliderBGRed		= 4
ULONG		c_SliderBGDarkRed	= 5
ULONG		c_SliderBGGreen		= 6
ULONG		c_SliderBGDarkGreen	= 7
ULONG		c_SliderBGBlue		= 8
ULONG		c_SliderBGDarkBlue	= 9
ULONG		c_SliderBGMagenta	= 10
ULONG		c_SliderBGDarkMagenta	= 11
ULONG		c_SliderBGCyan		= 12
ULONG		c_SliderBGDarkCyan	= 13
ULONG		c_SliderBGYellow		= 14
ULONG		c_SliderBGDarkYellow	= 15

ULONG		c_SliderFrameGray		= 0
ULONG		c_SliderFrameBlack	= 16
ULONG		c_SliderFrameWhite	= 32
ULONG		c_SliderFrameDarkGray	= 48
ULONG		c_SliderFrameRed		= 64
ULONG		c_SliderFrameDarkRed	= 80
ULONG		c_SliderFrameGreen	= 96
ULONG		c_SliderFrameDarkGreen	= 112
ULONG		c_SliderFrameBlue		= 128
ULONG		c_SliderFrameDarkBlue   	= 144
ULONG		c_SliderFrameMagenta    	= 160
ULONG		c_SliderFrameDarkMagenta	= 176
ULONG		c_SliderFrameCyan       	= 192
ULONG		c_SliderFrameDarkCyan   	= 208
ULONG		c_SliderFrameYellow	= 224
ULONG		c_SliderFrameDarkYellow	= 240

ULONG		c_SliderBarBlack		= 2304
ULONG		c_SliderBarWhite		= 256
ULONG		c_SliderBarGray		= 0
ULONG		c_SliderBarDarkGray	= 768
ULONG		c_SliderBarRed		= 1024
ULONG		c_SliderBarDarkRed	= 1280
ULONG		c_SliderBarGreen		= 1536
ULONG		c_SliderBarDarkGreen	= 1792
ULONG		c_SliderBarBlue		= 2048
ULONG		c_SliderBarDarkBlue	= 512
ULONG		c_SliderBarMagenta	= 2560
ULONG		c_SliderBarDarkMagenta	= 2816
ULONG		c_SliderBarCyan		= 3072
ULONG		c_SliderBarDarkCyan	= 3328
ULONG		c_SliderBarYellow		= 3584
ULONG		c_SliderBarDarkYellow	= 3840

ULONG		c_SliderCenterLineBlue	= 32768
ULONG		c_SliderCenterLineWhite	= 4096
ULONG		c_SliderCenterLineGray	= 8192
ULONG		c_SliderCenterLineDarkGray	= 12288
ULONG		c_SliderCenterLineRed	= 16384
ULONG		c_SliderCenterLineDarkRed	= 20480
ULONG		c_SliderCenterLineGreen	= 24576
ULONG		c_SliderCenterLineDarkGreen	= 28672
ULONG		c_SliderCenterLineBlack	= 0
ULONG		c_SliderCenterLineDarkBlue	= 36864
ULONG		c_SliderCenterLineMagenta	= 40960
ULONG		c_SliderCenterLineDarkMagenta	 = 45056
ULONG		c_SliderCenterLineCyan	= 49152
ULONG		c_SliderCenterLineDarkCyan	= 53248
ULONG		c_SliderCenterLineYellow	= 57344
ULONG		c_SliderCenterLineDarkYellow	= 61440


ULONG		c_SliderFrameNone		= 131072
ULONG		c_SliderFrameShadowBox	= 65536
ULONG		c_SliderFrameBox		= 0
ULONG		c_SliderFrameLowered	= 196608
ULONG		c_SliderFrameRaised	= 262144

ULONG		c_SliderBorderNone	= 1572864
ULONG		c_SliderBorderShadowBox	= 524288
ULONG		c_SliderBorderBox		= 1048576
ULONG		c_SliderBorderLowered	= 0
ULONG		c_SliderBorderRaised	= 2097152

ULONG		c_SliderCenterLineShow	= 4194304
ULONG		c_SliderCenterLineHide	= 0

ULONG		c_SliderDirectionHoriz	= 0
ULONG		c_SliderDirectionVert	= 8388608

ULONG		c_SliderIndicatorOut	= 0
ULONG		c_SliderIndicatorIn		= 16777216

ULONG		c_SliderTextGray		= 1
ULONG		c_SliderTextBlack		= 0
ULONG		c_SliderTextWhite		= 2
ULONG		c_SliderTextDarkGray	= 3
ULONG		c_SliderTextRed		= 4
ULONG		c_SliderTextDarkRed	= 5
ULONG		c_SliderTextGreen		= 6
ULONG		c_SliderTextDarkGreen	= 7
ULONG		c_SliderTextBlue		= 8
ULONG		c_SliderTextDarkBlue	= 9
ULONG		c_SliderTextMagenta	= 10
ULONG		c_SliderTextDarkMagenta	= 11
ULONG		c_SliderTextCyan		= 12
ULONG		c_SliderTextDarkCyan	= 13
ULONG		c_SliderTextYellow		= 14
ULONG		c_SliderTextDarkYellow	= 15

ULONG		c_SliderTextShow		= 32
ULONG		c_SliderTextHide		= 0

ULONG		c_SliderTickGray		= 2
ULONG		c_SliderTickBlack		= 1
ULONG		c_SliderTickWhite		= 0
ULONG		c_SliderTickDarkGray	= 3
ULONG		c_SliderTickRed		= 4
ULONG		c_SliderTickDarkRed	= 5
ULONG		c_SliderTickGreen		= 6
ULONG		c_SliderTickDarkGreen	= 7
ULONG		c_SliderTickBlue		= 8
ULONG		c_SliderTickDarkBlue	= 9
ULONG		c_SliderTickMagenta	= 10
ULONG		c_SliderTickDarkMagenta	= 11
ULONG		c_SliderTickCyan		= 12
ULONG		c_SliderTickDarkCyan	= 13
ULONG		c_SliderTickYellow		= 14
ULONG		c_SliderTickDarkYellow	= 15

ULONG		c_SliderTickPlacementTopLeft	= 0
ULONG		c_SliderTickPlacementBotRight	= 32

//-------------------------------------------------------------------------------
//  Constants for setting options for the Calendar object.
//-------------------------------------------------------------------------------

ULONG 		c_CalGray		= 0
ULONG 		c_CalBlack		= 1
ULONG 		c_CalWhite		= 2
ULONG 		c_CalDarkGray		= 3
ULONG 		c_CalRed			= 4
ULONG 		c_CalDarkRed		= 5
ULONG 		c_CalGreen		= 6
ULONG 		c_CalDarkGreen		= 7
ULONG 		c_CalBlue		= 8
ULONG 		c_CalDarkBlue		= 9
ULONG 		c_CalMagenta		= 10
ULONG 		c_CalDarkMagenta		= 11
ULONG		c_CalCyan		= 12
ULONG		c_CalDarkCyan		= 13
ULONG 		c_CalYellow		= 14
ULONG 		c_CalDarkYellow		= 15

ULONG 		c_CalWEGray		= 0
ULONG 		c_CalWEBlack		= 16
ULONG 		c_CalWEWhite		= 32
ULONG 		c_CalWEDarkGray		= 48
ULONG 		c_CalWERed		= 64
ULONG 		c_CalWEDarkRed		= 80
ULONG 		c_CalWEGreen		= 96
ULONG 		c_CalWEDarkGreen	= 112
ULONG 		c_CalWEBlue		= 128
ULONG 		c_CalWEDarkBlue		= 144
ULONG		c_CalWEMagenta		= 160
ULONG 		c_CalWEDarkMagenta	= 176
ULONG 		c_CalWECyan		= 192
ULONG 		c_CalWEDarkCyan		= 208
ULONG 		c_CalWEYellow		= 224
ULONG 		c_CalWEDarkYellow	= 240

ULONG 		c_CalSortAsc		= 0
ULONG 		c_CalSortDesc		= 256
ULONG 		c_CalNoSort		= 512

ULONG 		c_CalStyle3D		= 0
ULONG		c_CalStyle2D		= 1024

ULONG 		c_CalHeadingBlack		= 0
ULONG 		c_CalHeadingWhite	= 1
ULONG 		c_CalHeadingGray		= 2
ULONG 		c_CalHeadingDarkGray	= 3
ULONG 		c_CalHeadingRed		= 4
ULONG 		c_CalHeadingDarkRed	= 5
ULONG 		c_CalHeadingGreen	= 6
ULONG 		c_CalHeadingDarkGreen	= 7
ULONG 		c_CalHeadingBlue		= 8
ULONG 		c_CalHeadingDarkBlue	= 9
ULONG 		c_CalHeadingMagenta	= 10
ULONG 		c_CalHeadingDarkMagenta	= 11
ULONG 		c_CalHeadingCyan		= 12
ULONG 		c_CalHeadingDarkCyan	= 13
ULONG 		c_CalHeadingYellow	= 14
ULONG 		c_CalHeadingDarkYellow	= 15

ULONG 		c_CalHeadingAuto		= 0
ULONG 		c_CalHeading1		= 16
ULONG 		c_CalHeading2		= 32
ULONG 		c_CalHeading3		= 48
ULONG 		c_CalHeadingFull		= 64

ULONG 		c_CalHeadingBold		= 0
ULONG 		c_CalHeadingRegular	= 128
ULONG 		c_CalHeadingItalic		= 256
ULONG 		c_CalHeadingUnderline	= 384

ULONG 		c_CalHeadingCenter	= 0
ULONG 		c_CalHeadingLeft		= 512
ULONG 		c_CalHeadingRight		= 1024

ULONG 		c_CalDayBlack		= 0
ULONG 		c_CalDayWhite		= 1
ULONG 		c_CalDayGray		= 2
ULONG 		c_CalDayDarkGray		= 3
ULONG 		c_CalDayRed		= 4
ULONG 		c_CalDayDarkRed		= 5
ULONG 		c_CalDayGreen		= 6 
ULONG 		c_CalDayDarkGreen	= 7
ULONG 		c_CalDayBlue		= 8
ULONG 		c_CalDayDarkBlue		= 9
ULONG 		c_CalDayMagenta		= 10
ULONG 		c_CalDayDarkMagenta	= 11
ULONG 		c_CalDayCyan		= 12
ULONG 		c_CalDayDarkCyan		= 13
ULONG 		c_CalDayYellow		= 14
ULONG 		c_CalDayDarkYellow	= 15

ULONG 		c_CalWEDayBlack		= 0
ULONG 		c_CalWEDayWhite		= 16
ULONG 		c_CalWEDayGray		= 32
ULONG		c_CalWEDayDarkGray	= 48
ULONG 		c_CalWEDayRed		= 64
ULONG 		c_CalWEDayDarkRed	= 80
ULONG 		c_CalWEDayGreen		= 96
ULONG 		c_CalWEDayDarkGreen	= 112
ULONG 		c_CalWEDayBlue		= 128
ULONG 		c_CalWEDayDarkBlue	= 144
ULONG 		c_CalWEDayMagenta	= 160
ULONG 		c_CalWEDayDarkMagenta	= 176
ULONG 		c_CalWEDayCyan		= 192
ULONG 		c_CalWEDayDarkCyan	= 208
ULONG 		c_CalWEDayYellow		= 224
ULONG 		c_CalWEDayDarkYellow	= 240

ULONG 		c_CalDayRight		= 0
ULONG 		c_CalDayLeft		= 256
ULONG 		c_CalDayCenter		= 512

ULONG 		c_CalDayRegular		= 0
ULONG 		c_CalDayBold		= 1024
ULONG 		c_CalDayItalic		= 2048
ULONG 		c_CalDayUnderline		= 3072

ULONG 		c_CalWEDayRegular	= 0
ULONG 		c_CalWEDayBold		= 4096
ULONG		c_CalWEDayItalic		= 8192
ULONG 		c_CalWEDayUnderline	= 12288

ULONG 		c_CalSelectWhite		= 0
ULONG 		c_CalSelectBlack		= 1
ULONG 		c_CalSelectGray		= 2
ULONG 		c_CalSelectDarkGray	= 3
ULONG 		c_CalSelectRed		= 4
ULONG 		c_CalSelectDarkRed	= 5
ULONG 		c_CalSelectGreen		= 6
ULONG 		c_CalSelectDarkGreen	= 7
ULONG 		c_CalSelectBlue		= 8
ULONG 		c_CalSelectDarkBlue	= 9
ULONG 		c_CalSelectMagenta	= 10
ULONG 		c_CalSelectDarkMagenta	= 11
ULONG 		c_CalSelectCyan		= 12
ULONG 		c_CalSelectDarkCyan	= 13
ULONG 		c_CalSelectYellow		= 14
ULONG 		c_CalSelectDarkYellow	= 15

ULONG 		c_CalSelectBGDarkGray	= 0
ULONG 		c_CalSelectBGBlack	= 16
ULONG 		c_CalSelectBGWhite	= 32
ULONG 		c_CalSelectBGGray		= 48
ULONG 		c_CalSelectBGRed		= 64
ULONG 		c_CalSelectBGDarkRed	= 80
ULONG 		c_CalSelectBGGreen	= 96
ULONG 		c_CalSelectBGDarkGreen	= 112
ULONG 		c_CalSelectBGBlue		= 128
ULONG 		c_CalSelectBGDarkBlue	= 144
ULONG 		c_CalSelectBGMagenta	= 160
ULONG 		c_CalSelectBGDarkMagenta	= 176
ULONG 		c_CalSelectBGCyan	= 192
ULONG 		c_CalSelectBGDarkCyan	= 208
ULONG 		c_CalSelectBGYellow	= 224
ULONG 		c_CalSelectBGDarkYellow	= 240

ULONG 		c_CalSelectRegular	= 0
ULONG 		c_CalSelectBold		= 256
ULONG 		c_CalSelectItalic		= 512
ULONG 		c_CalSelectUnderline	= 768

ULONG 		c_CalDisableDarkGray	= 0
ULONG 		c_CalDisableWhite		= 1
ULONG 		c_CalDisableBlack		= 2
ULONG 		c_CalDisableGray		= 3
ULONG 		c_CalDisableRed		= 4
ULONG 		c_CalDisableDarkRed	= 5
ULONG 		c_CalDisableGreen		= 6
ULONG 		c_CalDisableDarkGreen	= 7
ULONG 		c_CalDisableBlue		= 8
ULONG 		c_CalDisableDarkBlue	= 9
ULONG 		c_CalDisableMagenta	= 10
ULONG 		c_CalDisableDarkMagenta	= 11
ULONG 		c_CalDisableCyan		= 12
ULONG 		c_CalDisableDarkCyan	= 13
ULONG 		c_CalDisableYellow		= 14
ULONG 		c_CalDisableDarkYellow	= 15

ULONG 		c_CalDisableBGGray	= 0
ULONG 		c_CalDisableBGBlack	= 16
ULONG 		c_CalDisableBGWhite	= 32
ULONG 		c_CalDisableBGDarkGray	= 48
ULONG 		c_CalDisableBGRed	= 64
ULONG 		c_CalDisableBGDarkRed	= 80
ULONG 		c_CalDisableBGGreen	= 96
ULONG 		c_CalDisableBGDarkGreen	= 112
ULONG 		c_CalDisableBGBlue	= 128
ULONG 		c_CalDisableBGDarkBlue	= 144
ULONG 		c_CalDisableBGMagenta	= 160
ULONG 		c_CalDisableBGDarkMagenta	= 176
ULONG 		c_CalDisableBGCyan	= 192
ULONG 		c_CalDisableBGDarkCyan	= 208
ULONG 		c_CalDisableBGYellow	= 224
ULONG 		c_CalDisableBGDarkYellow	= 240

ULONG 		c_CalDisableRegular	= 0
ULONG 		c_CalDisableBold		= 256
ULONG 		c_CalDisableItalic		= 512
ULONG 		c_CalDisableUnderline	= 768

ULONG 		c_CalMonthBlack		= 0
ULONG 		c_CalMonthWhite		= 1
ULONG 		c_CalMonthGray		= 2
ULONG 		c_CalMonthDarkGray	= 3
ULONG 		c_CalMonthRed		= 4
ULONG 		c_CalMonthDarkRed	= 5
ULONG 		c_CalMonthGreen		= 6
ULONG		c_CalMonthDarkGreen	= 7
ULONG 		c_CalMonthBlue		= 8
ULONG 		c_CalMonthDarkBlue	= 9
ULONG 		c_CalMonthMagenta	= 10
ULONG 		c_CalMonthDarkMagenta	= 11
ULONG 		c_CalMonthCyan		= 12
ULONG 		c_CalMonthDarkCyan	= 13
ULONG 		c_CalMonthYellow		= 14
ULONG 		c_CalMonthDarkYellow	= 15

ULONG 		c_CalMonthBold		= 0
ULONG 		c_CalMonthRegular		= 16
ULONG 		c_CalMonthItalic		= 32
ULONG 		c_CalMonthUnderline	= 48

ULONG		c_CalYearBlack		= 0
ULONG 		c_CalYearWhite		= 1
ULONG 		c_CalYearGray		= 2
ULONG 		c_CalYearDarkGray		= 3
ULONG 		c_CalYearRed		= 4
ULONG 		c_CalYearDarkRed		= 5
ULONG 		c_CalYearGreen		= 6
ULONG 		c_CalYearDarkGreen	= 7
ULONG 		c_CalYearBlue		= 8
ULONG 		c_CalYearDarkBlue		= 9
ULONG 		c_CalYearMagenta		= 10
ULONG 		c_CalYearDarkMagenta	= 11
ULONG 		c_CalYearCyan		= 12
ULONG 		c_CalYearDarkCyan	= 13
ULONG 		c_CalYearYellow		= 14
ULONG 		c_CalYearDarkYellow	= 15

ULONG 		c_CalYearBold		= 0
ULONG 		c_CalYearRegular		= 16
ULONG 		c_CalYearItalic		= 32
ULONG 		c_CalYearUnderline	= 48

ULONG 		c_CalYearShow		= 0
ULONG 		c_CalYearHide		= 64

//-------------------------------------------------------------------------------
//  Constants for setting options for the Progress Bar object.
//-------------------------------------------------------------------------------

ULONG		c_WindowGray		= 0
ULONG		c_WindowBlack		= 1
ULONG		c_WindowWhite		= 2
ULONG		c_WindowDarkGray	= 3
ULONG		c_WindowRed		= 4
ULONG		c_WindowDarkRed		= 5
ULONG		c_WindowGreen		= 6
ULONG		c_WindowDarkGreen	= 7
ULONG		c_WindowBlue		= 8
ULONG		c_WindowDarkBlue		= 9
ULONG		c_WindowMagenta		= 10
ULONG		c_WindowDarkMagenta	= 11
ULONG		c_WindowCyan		= 12
ULONG		c_WindowDarkCyan	= 13
ULONG		c_WindowYellow		= 14
ULONG		c_WindowDarkYellow	= 15

ULONG		c_WindowTextDarkBlue	= 0
ULONG		c_WindowTextBlack	= 16
ULONG		c_WindowTextWhite	= 32
ULONG		c_WindowTextDarkGray	= 48
ULONG		c_WindowTextRed		= 64
ULONG		c_WindowTextDarkRed	= 80
ULONG		c_WindowTextGreen	= 96
ULONG		c_WindowTextDarkGreen	= 112
ULONG		c_WindowTextBlue		= 128
ULONG		c_WindowTextGray   	= 144
ULONG		c_WindowTextMagenta    	= 160
ULONG		c_WindowTextDarkMagenta	= 176
ULONG		c_WindowTextCyan       	= 192
ULONG		c_WindowTextDarkCyan   	= 208
ULONG		c_WindowTextYellow	= 224
ULONG		c_WindowTextDarkYellow	= 240



end variables

forward prototypes
public subroutine fw_folderdefaults (unsignedlong visualword)
public subroutine fw_tabdefaults (unsignedlong visualword)
public subroutine fw_tabcurrentdefaults (integer visualword)
public subroutine fw_tabdisabledefaults (unsignedlong visualword)
public function unsignedlong fw_bitmask (unsignedlong operand1, unsignedlong operand2)
public subroutine fw_hltextdefaults (unsignedlong visualword)
public subroutine fw_hldefaults (unsignedlong visualword)
public function integer fw_progressdefaults (unsignedlong visualword)
public function integer fw_setlogin (ref transaction trans_object, string usr_login, string usr_password)
public function string fw_getlogin (transaction trans_object)
public subroutine fw_windowdefaults (string textfont, integer textsize, unsignedlong visualword)
public subroutine fw_caldefaults (unsignedlong visualword)
public subroutine fw_sliderdefaults (unsignedlong visualword)
public subroutine fw_calheadingdefaults (unsignedlong visualword)
public subroutine fw_caldaydefaults (unsignedlong visualword)
public subroutine fw_calselectdefaults (unsignedlong visualword)
public subroutine fw_caldisabledefaults (unsignedlong visualword)
public subroutine fw_calmonthdefaults (unsignedlong visualword)
public subroutine fw_calyeardefaults (unsignedlong visualword)
end prototypes

public subroutine fw_folderdefaults (unsignedlong visualword);//******************************************************************
//  PO Module     : w_POManager_Main
//  Function      : fw_FolderDefaults
//  Description   : Establishes the default characteristics of the
//                  folder portion of the tab folder object.
//
//  Parameters    : UNSIGNEDLONG VisualWord
//
//  Return Value  : (None)
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

ULONG      l_BitMask
ULONG      c_FolderTabMask     = 3
ULONG      c_FolderStyleMask   = 4
ULONG      c_FolderColorMask   = 120
ULONG      c_FolderBGColorMask = 1920
ULONG      c_FolderBorderMask  = 2048
ULONG      c_FolderResizeMask  = 4096

//------------------------------------------------------------------
//  Sets the default location of the tab on the folder.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_FolderTabMask)
  
CHOOSE CASE l_BitMask
   CASE c_FolderTabTop
      c_FolderTabTop = 0
   CASE c_FolderTabBottom
      c_FolderTabTop = c_FolderTabBottom
      c_FolderTabBottom = 0
   CASE c_FolderTabLeft
      c_FolderTabTop = c_FolderTabLeft
      c_FolderTabLeft = 0
   CASE c_FolderTabRight
      c_FolderTabTop = c_FolderTabRight
      c_FolderTabRight = 0
END CHOOSE

//------------------------------------------------------------------
//  Sets the default color of the folder.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_FolderColorMask)

CHOOSE CASE l_BitMask
   CASE c_FolderGray
      c_FolderGray = 0
   CASE c_FolderBlack
      c_FolderGray = c_FolderBlack
      c_FolderBlack = 0
   CASE c_FolderWhite
      c_FolderGray = c_FolderWhite
      c_FolderWhite = 0
   CASE c_FolderDarkGray
      c_FolderGray = c_FolderDarkGray
      c_FolderDarkGray = 0
   CASE c_FolderRed
      c_FolderGray = c_FolderRed
      c_FolderRed = 0
   CASE c_FolderDarkRed
      c_FolderGray = c_FolderDarkRed
      c_FolderDarkRed = 0
   CASE c_FolderGreen
      c_FolderGray = c_FolderGreen
      c_FolderGreen = 0
   CASE c_FolderDarkGreen
      c_FolderGray = c_FolderDarkGreen
      c_FolderDarkGreen = 0
   CASE c_FolderBlue
      c_FolderGray = c_FolderBlue
      c_FolderBlue = 0
   CASE c_FolderDarkBlue
      c_FolderGray = c_FolderDarkBlue
      c_FolderDarkBlue = 0
   CASE c_FolderMagenta
      c_FolderGray = c_FolderMagenta
      c_FolderMagenta = 0
   CASE c_FolderDarkMagenta
      c_FolderGray = c_FolderDarkMagenta
      c_FolderDarkMagenta = 0
   CASE c_FolderCyan
      c_FolderGray = c_FolderCyan
      c_FolderCyan = 0
   CASE c_FolderDarkCyan
      c_FolderGray = c_FolderDarkCyan
      c_FolderDarkCyan = 0
   CASE c_FolderYellow
      c_FolderGray = c_FolderYellow
      c_FolderYellow = 0
   CASE c_FolderDarkYellow
      c_FolderGray = c_FolderDarkYellow
      c_FolderDarkYellow = 0
END CHOOSE

//------------------------------------------------------------------
//  Sets the default style of the folder to 2D or 3D.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_FolderStyleMask)

CHOOSE CASE l_BitMask
   CASE c_Folder3D
      c_Folder3D = 0
   CASE c_Folder2D
      c_Folder3D = c_Folder2D
      c_Folder2D = 0
END CHOOSE

//------------------------------------------------------------------
//  Sets the default background color of the folder.  This color 
//  should be set to the same color as the parent (i.e. window).
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_FolderBGColorMask)

CHOOSE CASE l_BitMask
   CASE c_FolderBGGray
      c_FolderBGGray = 0
   CASE c_FolderBGBlack
      c_FolderBGGray= c_FolderBGBlack
      c_FolderBGBlack = 0
   CASE c_FolderBGWhite
      c_FolderBGGray= c_FolderBGWhite
      c_FolderBGWhite = 0
   CASE c_FolderBGDarkGray
      c_FolderBGGray= c_FolderBGDarkGray
      c_FolderBGDarkGray = 0
   CASE c_FolderBGRed
      c_FolderBGGray= c_FolderBGRed
      c_FolderBGRed = 0
   CASE c_FolderBGDarkRed
      c_FolderBGGray= c_FolderBGDarkRed
      c_FolderBGDarkRed = 0
   CASE c_FolderBGGreen
      c_FolderBGGray= c_FolderBGGreen
      c_FolderBGGreen = 0
   CASE c_FolderBGDarkGreen
      c_FolderBGGray= c_FolderBGDarkGreen
      c_FolderBGDarkGreen = 0
   CASE c_FolderBGBlue
      c_FolderBGGray= c_FolderBGBlue
      c_FolderBGBlue = 0
   CASE c_FolderBGDarkBlue
      c_FolderBGGray= c_FolderBGDarkBlue
      c_FolderBGDarkBlue = 0
   CASE c_FolderBGMagenta
      c_FolderBGGray= c_FolderBGMagenta
      c_FolderBGMagenta = 0
   CASE c_FolderBGDarkMagenta
      c_FolderBGGray= c_FolderBGDarkMagenta
      c_FolderBGDarkMagenta = 0
   CASE c_FolderBGCyan
      c_FolderBGGray= c_FolderBGCyan
      c_FolderBGCyan = 0
   CASE c_FolderBGDarkCyan
      c_FolderBGGray= c_FolderBGDarkCyan
      c_FolderBGDarkCyan = 0
   CASE c_FolderBGYellow
      c_FolderBGGray= c_FolderBGYellow
      c_FolderBGYellow = 0
   CASE c_FolderBGDarkYellow
      c_FolderBGGray= c_FolderBGDarkYellow
      c_FolderBGDarkYellow = 0
END CHOOSE

//------------------------------------------------------------------
//  Sets the default border style of the folder.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_FolderBorderMask)

CHOOSE CASE l_BitMask
   CASE c_FolderBorderOn
      c_FolderBorderOn = 0
   CASE c_FolderBorderOff
      c_FolderBorderOn = c_FolderBorderOff
      c_FolderBorderOff = 0
END CHOOSE

//------------------------------------------------------------------
//  Sets the default for allowing the folder to be resized when the
//  parent is resized.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_FolderResizeMask)

CHOOSE CASE l_BitMask
   CASE c_FolderResizeOn
      c_FolderResizeOn = 0
   CASE c_FolderResizeOff
      c_FolderResizeOn = c_FolderResizeOff
      c_FolderResizeOff = 0
END CHOOSE

end subroutine

public subroutine fw_tabdefaults (unsignedlong visualword);//******************************************************************
//  PO Module     : w_POManager_Main
//  Function      : fw_TabDefaults
//  Description   : Establishes the default characteristics of the
//                  tab portion of the tab folder object.
//
//  Parameters    : UNSIGNEDLONG VisualWord
//
//  Return Value  : (None)
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

ULONG l_BitMask
ULONG c_TextStyleMask    = 3
ULONG c_TextAlignMask    = 12
ULONG c_TextColorMask    = 240
ULONG c_TextRotationMask = 256

//------------------------------------------------------------------
//  Sets the default text style for the tabs.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_TextStyleMask)

CHOOSE CASE l_BitMask
   CASE c_TextRegular
      c_TextRegular = 0
   CASE c_TextBold
      c_TextRegular = c_TextBold
      c_TextBold = 0
   CASE c_TextItalic
      c_TextRegular = c_TextItalic
      c_TextItalic= 0
   CASE c_TextUnderline
      c_TextRegular = c_TextUnderLine
      c_TextUnderLine = 0
END CHOOSE

//------------------------------------------------------------------
//  Sets the default text alignment for the tabs.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_TextAlignMask)

CHOOSE CASE l_BitMask
   CASE c_TextCenter
      c_TextCenter = 0
   CASE c_TextLeft
      c_TextCenter = c_TextLeft
      c_TextLeft = 0
   CASE c_TextRight
      c_TextCenter = c_TextRight
      c_TextRight = 0
END CHOOSE

//------------------------------------------------------------------
//  Sets the default text color for the tabs.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_TextColorMask)

CHOOSE CASE l_BitMask
   CASE c_TextBlack
      c_TextBlack = 0
   CASE c_TextWhite
      c_TextBlack = c_TextWhite
      c_TextWhite = 0
   CASE c_TextGray
      c_TextBlack = c_TextGray
      c_TextGray = 0
   CASE c_TextDarkGray
      c_TextBlack = c_TextDarkGray
      c_TextDarkGray = 0
   CASE c_TextRed
      c_TextBlack = c_TextRed
      c_TextRed = 0
   CASE c_TextDarkRed
      c_TextBlack = c_TextDarkRed
      c_TextDarkRed = 0
   CASE c_TextGreen
      c_TextBlack = c_TextGreen
      c_TextGreen = 0
   CASE c_TextDarkGreen
      c_TextBlack = c_TextDarkGreen
      c_TextDarkGreen = 0
   CASE c_TextBlue
      c_TextBlack = c_TextBlue
      c_TextBlue = 0
   CASE c_TextDarkBlue
      c_TextBlack = c_TextDarkBlue
      c_TextDarkBlue = 0
   CASE c_TextMagenta
      c_TextBlack = c_TextMagenta
      c_TextMagenta = 0
   CASE c_TextDarkMagenta
      c_TextBlack = c_TextDarkMagenta
      c_TextDarkMagenta = 0
   CASE c_TextCyan
      c_TextBlack = c_TextCyan
      c_TextCyan = 0
   CASE c_TextDarkCyan
      c_TextBlack = c_TextDarkCyan
      c_TextDarkCyan = 0
   CASE c_TextYellow
      c_TextBlack = c_TextYellow
      c_TextYellow = 0
   CASE c_TextDarkYellow
      c_TextBlack = c_TextDarkYellow
      c_TextDarkYellow = 0
END CHOOSE

//------------------------------------------------------------------
//  Sets the default for indicating if the text for the tabs
//  should be rotated when the tab location is on the right or left
//  side of the folder.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_TextRotationMask)

CHOOSE CASE l_BitMask
   CASE c_TextRotationOn
      c_TextRotationOn = 0
   CASE c_TextRotationOff
      c_TextRotationOff = c_TextRotationOn
      c_TextRotationOff = 0
END CHOOSE
end subroutine

public subroutine fw_tabcurrentdefaults (integer visualword);//******************************************************************
//  PO Module     : w_POManager_Main
//  Function      : fw_TabCurrentDefaults
//  Description   : Establishes the default characteristics of the
//                  current tab portion of the tab folder object.
//
//  Parameters    : UNSIGNEDLONG VisualWord
//
//  Return Value  : (None)
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

ULONG l_BitMask
ULONG c_TextCurrentStyleMask = 3
ULONG c_TextCurrentColorMask = 60
ULONG c_TabCurrentColorMask  = 960
ULONG c_TabCurrentStyleMask  = 3072

//------------------------------------------------------------------
//  Sets the default text style for the current tab.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_TextCurrentStyleMask)

CHOOSE CASE l_BitMask
   CASE c_TextCurrentBold
      c_TextCurrentBold = 0
   CASE c_TextCurrentRegular
      c_TextCurrentBold = c_TextCurrentRegular
      c_TextCurrentRegular = 0
   CASE c_TextCurrentItalic
      c_TextCurrentBold = c_TextCurrentItalic
      c_TextCurrentItalic = 0
   CASE c_TextCurrentUnderline
      c_TextCurrentBold = c_TextCurrentUnderline
      c_TextCurrentUnderline = 0
END CHOOSE

//------------------------------------------------------------------
//  Sets the default text color for the current tab.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_TextCurrentColorMask)

CHOOSE CASE l_BitMask
   CASE c_TextCurrentBlack
      c_TextCurrentBlack = 0
   CASE c_TextCurrentWhite
      c_TextCurrentBlack = c_TextCurrentWhite
      c_TextCurrentWhite = 0
   CASE c_TextCurrentGray
      c_TextCurrentBlack = c_TextCurrentGray
      c_TextCurrentGray = 0
   CASE c_TextCurrentDarkGray
      c_TextCurrentBlack = c_TextCurrentDarkGray
      c_TextCurrentDarkGray = 0
   CASE c_TextCurrentRed
      c_TextCurrentBlack = c_TextCurrentRed
      c_TextCurrentRed = 0
   CASE c_TextCurrentDarkRed
      c_TextCurrentBlack = c_TextCurrentDarkRed
      c_TextCurrentDarkRed = 0
   CASE c_TextCurrentGreen
      c_TextCurrentBlack = c_TextCurrentGreen
      c_TextCurrentGreen = 0
   CASE c_TextCurrentDarkGreen
      c_TextCurrentBlack = c_TextCurrentDarkGreen
      c_TextCurrentDarkGreen = 0
   CASE c_TextCurrentBlue
      c_TextCurrentBlack = c_TextCurrentBlue
      c_TextCurrentBlue = 0
   CASE c_TextCurrentDarkBlue
      c_TextCurrentBlack = c_TextCurrentDarkBlue
      c_TextCurrentDarkBlue = 0
   CASE c_TextCurrentMagenta
      c_TextCurrentBlack = c_TextCurrentMagenta
      c_TextCurrentMagenta = 0
   CASE c_TextCurrentDarkMagenta
      c_TextCurrentBlack = c_TextCurrentDarkMagenta
      c_TextCurrentDarkMagenta = 0
   CASE c_TextCurrentCyan
      c_TextCurrentBlack = c_TextCurrentCyan
      c_TextCurrentCyan = 0
   CASE c_TextCurrentDarkCyan
      c_TextCurrentBlack = c_TextCurrentDarkCyan
      c_TextCurrentDarkCyan = 0
   CASE c_TextCurrentYellow
      c_TextCurrentBlack = c_TextCurrentYellow
      c_TextCurrentYellow = 0
   CASE c_TextCurrentDarkYellow
      c_TextCurrentBlack = c_TextCurrentDarkYellow
      c_TextCurrentDarkYellow = 0
END CHOOSE

//------------------------------------------------------------------
//  Sets the default tab color for the current tab.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_TabCurrentColorMask)

CHOOSE CASE l_BitMask
   CASE c_TabCurrentGray
      c_TabCurrentGray = 0
   CASE c_TabCurrentBlack
      c_TabCurrentBlack = c_TabCurrentGray
      c_TabCurrentGray = 0
   CASE c_TabCurrentWhite
      c_TabCurrentGray = c_TabCurrentWhite
      c_TabCurrentWhite = 0
   CASE c_TabCurrentDarkGray
      c_TabCurrentGray = c_TabCurrentDarkGray
      c_TabCurrentDarkGray = 0
   CASE c_TabCurrentRed
      c_TabCurrentGray = c_TabCurrentRed
      c_TabCurrentRed = 0
   CASE c_TabCurrentDarkRed
      c_TabCurrentGray = c_TabCurrentDarkRed
      c_TabCurrentDarkRed = 0
   CASE c_TabCurrentGreen
      c_TabCurrentGray = c_TabCurrentGreen
      c_TabCurrentGreen = 0
   CASE c_TabCurrentDarkGreen
      c_TabCurrentGray = c_TabCurrentDarkGreen
      c_TabCurrentDarkGreen = 0
   CASE c_TabCurrentBlue
      c_TabCurrentGray = c_TabCurrentBlue
      c_TabCurrentBlue = 0
   CASE c_TabCurrentDarkBlue
      c_TabCurrentGray = c_TabCurrentDarkBlue
      c_TabCurrentDarkBlue = 0
   CASE c_TabCurrentMagenta
      c_TabCurrentGray = c_TabCurrentMagenta
      c_TabCurrentMagenta = 0
   CASE c_TabCurrentDarkMagenta
      c_TabCurrentGray = c_TabCurrentDarkMagenta
      c_TabCurrentDarkMagenta = 0
   CASE c_TabCurrentCyan
      c_TabCurrentGray = c_TabCurrentCyan
      c_TabCurrentCyan = 0
   CASE c_TabCurrentDarkCyan
      c_TabCurrentGray = c_TabCurrentDarkCyan
      c_TabCurrentDarkCyan = 0
   CASE c_TabCurrentYellow
      c_TabCurrentGray = c_TabCurrentYellow
      c_TabCurrentYellow = 0
   CASE c_TabCurrentDarkYellow
      c_TabCurrentGray = c_TabCurrentDarkYellow
      c_TabCurrentDarkYellow = 0
END CHOOSE

//------------------------------------------------------------------
//  Sets the default tab color style for the current tab.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_TabCurrentStyleMask)

CHOOSE CASE l_BitMask
   CASE c_TabCurrentColorFolder
      c_TabCurrentColorFolder = 0
   CASE c_TabCurrentColorBorder
      c_TabCurrentColorFolder = c_TabCurrentColorBorder
      c_TabCurrentColorBorder = 0
   CASE c_TabCurrentColorTab
      c_TabCurrentColorFolder = c_TabCurrentColorTab
      c_TabCurrentColorTab = 0
END CHOOSE
end subroutine

public subroutine fw_tabdisabledefaults (unsignedlong visualword);//******************************************************************
//  PO Module     : w_POManager_Main
//  Function      : fw_TabDisableDefaults
//  Description   : Establishes the default characteristics of the
//                  disabled tab portion of the tab folder object.
//
//  Parameters    : UNSIGNEDLONG VisualWord
//
//  Return Value  : (None)
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

ULONG l_BitMask
ULONG c_TextDisableStyleMask = 3
ULONG c_TextDisableColorMask = 60

//------------------------------------------------------------------
//  Sets the default text style for disabled tabs.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_TextDisableStyleMask)

CHOOSE CASE l_BitMask
   CASE c_TextDisableItalic
      c_TextDisableItalic = 0
   CASE c_TextDisableRegular
      c_TextDisableItalic = c_TextDisableRegular
      c_TextDisableRegular = 0
   CASE c_TextDisableBold
      c_TextDisableItalic = c_TextDisableBold
      c_TextDisableBold = 0
   CASE c_TextDisableUnderline
      c_TextDisableItalic = c_TextDisableUnderline
      c_TextDisableUnderline = 0
END CHOOSE

//------------------------------------------------------------------
//  Sets the default text color for disabled tabs.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_TextDisableColorMask)

CHOOSE CASE l_BitMask
   CASE c_TextDisableDarkGray
      c_TextDisableDarkGray = 0
   CASE c_TextDisableBlack
      c_TextDisableDarkGray = c_TextDisableBlack
      c_TextDisableBlack = 0
   CASE c_TextDisableWhite
      c_TextDisableDarkGray = c_TextDisableWhite
      c_TextDisableWhite = 0
   CASE c_TextDisableGray
      c_TextDisableDarkGray = c_TextDisableGray
      c_TextDisableGray = 0
   CASE c_TextDisableRed
      c_TextDisableDarkGray = c_TextDisableRed
      c_TextDisableRed = 0
   CASE c_TextDisableDarkRed
      c_TextDisableDarkGray = c_TextDisableDarkRed
      c_TextDisableDarkRed = 0
   CASE c_TextDisableGreen
      c_TextDisableDarkGray = c_TextDisableGreen
      c_TextDisableGreen = 0
   CASE c_TextDisableDarkGreen
      c_TextDisableDarkGray = c_TextDisableDarkGreen
      c_TextDisableDarkGreen = 0
   CASE c_TextDisableBlue
      c_TextDisableDarkGray = c_TextDisableBlue
      c_TextDisableBlue = 0
   CASE c_TextDisableDarkBlue
      c_TextDisableDarkGray = c_TextDisableDarkBlue
      c_TextDisableDarkBlue = 0
   CASE c_TextDisableMagenta
      c_TextDisableDarkGray = c_TextDisableMagenta
      c_TextDisableMagenta = 0
   CASE c_TextDisableDarkMagenta
      c_TextDisableDarkGray = c_TextDisableDarkMagenta
      c_TextDisableDarkMagenta = 0
   CASE c_TextDisableCyan
      c_TextDisableDarkGray = c_TextDisableCyan
      c_TextDisableCyan = 0
   CASE c_TextDisableDarkCyan
      c_TextDisableDarkGray = c_TextDisableDarkCyan
      c_TextDisableDarkCyan = 0
   CASE c_TextDisableYellow
      c_TextDisableDarkGray = c_TextDisableYellow
      c_TextDisableYellow = 0
   CASE c_TextDisableDarkYellow
      c_TextDisableDarkGray = c_TextDisableDarkYellow
      c_TextDisableDarkYellow = 0
END CHOOSE
end subroutine

public function unsignedlong fw_bitmask (unsignedlong operand1, unsignedlong operand2);//******************************************************************
//  PO Module     : w_POManager_Main
//  Function      : fw_BitMask
//  Description   : Returns the logical AND of the parameters.
//
//  Parameters    : UNSIGNEDLONG Operand1
//                  UNSIGNEDLONG Operand2
//
//  Return Value  : UNSIGNEDLONG
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN      l_Bit1,   l_Bit2
INTEGER      l_Idx
UNSIGNEDLONG l_Return, l_Divided

//------------------------------------------------------------------
//  Initialize the return value to cleared state.
//------------------------------------------------------------------

l_Return = 0

//------------------------------------------------------------------
//  Step through each bit.  If both bits are set then set the
//  corresponding bit in the return value.
//------------------------------------------------------------------

FOR l_Idx = 1 TO 32

   //---------------------------------------------------------------
   //  Right shift the return value.
   //---------------------------------------------------------------

   l_Return /= 2

   //---------------------------------------------------------------
   //  See if the the first operand is odd.  If it is, then we know
   //  the low order bit is set.  Since dividing by 2 shifts a value
   //  right by one bit, we can save l_Divided into the operand so
   //  that the next time through the loop we'll be ready to test
   //  the next bit.
   //---------------------------------------------------------------

   l_Divided = Operand1 / 2
   l_Bit1    = (2 * l_Divided <> Operand1)
   Operand1  = l_Divided

   //---------------------------------------------------------------
   //  See if the the second operand is odd.  If it is, then we know
   //  the low order bit is set.  Since dividing by 2 shifts a value
   //  right by one bit, we can save l_Divided into the operand so
   //  that the next time through the loop we'll be ready to test
   //  the next bit.
   //---------------------------------------------------------------

   l_Divided = Operand2 / 2
   l_Bit2    = (2 * l_Divided <> Operand2)
   Operand2  = l_Divided

   //---------------------------------------------------------------
   //  If both bits are set, then set the high bit in the return
   //  value.  It will get shifted to the appropriate position by
   //  the time we are done.
   //---------------------------------------------------------------

   IF l_Bit1 AND l_Bit2 THEN
      l_Return += 2147483648
   END IF
NEXT

RETURN l_Return
end function

public subroutine fw_hltextdefaults (unsignedlong visualword);//******************************************************************
//  PO Module     : w_POManager_Main
//  Function      : fw_HLTextDefaults
//  Description   : Establishes the default characteristics of the
//                  text portion of the outliner object.
//
//  Parameters    : UNSIGNEDLONG VisualWord
//
//  Return Value  : (None)
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

ULONG l_BitMask
ULONG c_TextStyleMask = 3
ULONG c_TextColorMask = 240
ULONG c_TextHighMask  = 7680

//------------------------------------------------------------------
//  Sets the default text style of the outliner.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_TextStyleMask)

CHOOSE CASE l_BitMask
   CASE c_TextRegular
      c_TextRegular = 0
   CASE c_TextBold
      c_TextRegular = c_TextBold
      c_TextBold = 0
   CASE c_TextItalic
      c_TextRegular = c_TextItalic
      c_TextItalic= 0
   CASE c_TextUnderline
      c_TextRegular = c_TextUnderLine
      c_TextUnderLine = 0
END CHOOSE

//------------------------------------------------------------------
//  Sets the default text color of the outliner.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_TextColorMask)

CHOOSE CASE l_BitMask
   CASE c_TextBlack
      c_TextBlack = 0
   CASE c_TextWhite
      c_TextBlack = c_TextWhite
      c_TextWhite = 0
   CASE c_TextGray
      c_TextBlack = c_TextGray
      c_TextGray = 0
   CASE c_TextDarkGray
      c_TextBlack = c_TextDarkGray
      c_TextDarkGray = 0
   CASE c_TextRed
      c_TextBlack = c_TextRed
      c_TextRed = 0
   CASE c_TextDarkRed
      c_TextBlack = c_TextDarkRed
      c_TextDarkRed = 0
   CASE c_TextGreen
      c_TextBlack = c_TextGreen
      c_TextGreen = 0
   CASE c_TextDarkGreen
      c_TextBlack = c_TextDarkGreen
      c_TextDarkGreen = 0
   CASE c_TextBlue
      c_TextBlack = c_TextBlue
      c_TextBlue = 0
   CASE c_TextDarkBlue
      c_TextBlack = c_TextDarkBlue
      c_TextDarkBlue = 0
   CASE c_TextMagenta
      c_TextBlack = c_TextMagenta
      c_TextMagenta = 0
   CASE c_TextDarkMagenta
      c_TextBlack = c_TextDarkMagenta
      c_TextDarkMagenta = 0
   CASE c_TextCyan
      c_TextBlack = c_TextCyan
      c_TextCyan = 0
   CASE c_TextDarkCyan
      c_TextBlack = c_TextDarkCyan
      c_TextDarkCyan = 0
   CASE c_TextYellow
      c_TextBlack = c_TextYellow
      c_TextYellow = 0
   CASE c_TextDarkYellow
      c_TextBlack = c_TextDarkYellow
      c_TextDarkYellow = 0
END CHOOSE

//------------------------------------------------------------------
//  Sets the default text color when the record is highlighted.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_TextHighMask)

CHOOSE CASE l_BitMask
   CASE c_TextHighlightBlack
      c_TextHighlightWhite = c_TextHighlightBlack
      c_TextHighlightBlack = 0
   CASE c_TextHighlightWhite
      c_TextHighlightWhite = 0
   CASE c_TextHighlightGray
      c_TextHighlightWhite = c_TextHighlightGray
      c_TextHighlightGray = 0
   CASE c_TextHighlightDarkGray
      c_TextHighlightWhite = c_TextHighlightDarkGray
      c_TextHighlightDarkGray = 0
   CASE c_TextHighlightRed
      c_TextHighlightWhite = c_TextHighlightRed
      c_TextHighlightRed = 0
   CASE c_TextHighlightDarkRed
      c_TextHighlightWhite = c_TextHighlightDarkRed
      c_TextHighlightDarkRed = 0
   CASE c_TextHighlightGreen
      c_TextHighlightWhite = c_TextHighlightGreen
      c_TextHighlightGreen = 0
   CASE c_TextHighlightDarkGreen
      c_TextHighlightWhite = c_TextHighlightDarkGreen
      c_TextHighlightDarkGreen = 0
   CASE c_TextHighlightBlue
      c_TextHighlightWhite = c_TextHighlightBlue
      c_TextHighlightBlue = 0
   CASE c_TextHighlightDarkBlue
      c_TextHighlightWhite = c_TextHighlightDarkBlue
      c_TextHighlightDarkBlue = 0
   CASE c_TextHighlightMagenta
      c_TextHighlightWhite = c_TextHighlightMagenta
      c_TextHighlightMagenta = 0
   CASE c_TextHighlightDarkMagenta
      c_TextHighlightWhite = c_TextHighlightDarkMagenta
      c_TextHighlightDarkMagenta = 0
   CASE c_TextHighlightCyan
      c_TextHighlightWhite = c_TextHighlightCyan
      c_TextHighlightCyan = 0
   CASE c_TextHighlightDarkCyan
      c_TextHighlightWhite = c_TextHighlightDarkCyan
      c_TextHighlightDarkCyan = 0
   CASE c_TextHighlightYellow
      c_TextHighlightWhite = c_TextHighlightYellow
      c_TextHighlightYellow = 0
   CASE c_TextHighlightDarkYellow
      c_TextHighlightWhite = c_TextHighlightDarkYellow
      c_TextHighlightDarkYellow = 0
END CHOOSE
end subroutine

public subroutine fw_hldefaults (unsignedlong visualword);//******************************************************************
//  PO Module     : w_POManager_Main
//  Function      : fw_HLDefaults
//  Description   : Establishes the default characteristics of the
//                  outliner portion of the outliner object.
//
//  Parameters    : UNSIGNEDLONG VisualWord
//
//  Return Value  : (None)
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

ULONG l_BitMask

ULONG c_HLColorMask       = 15
ULONG c_LineColorMask     = 240
ULONG c_RowFocusColorMask = 3840
ULONG c_HighColorMask     = 61440
ULONG c_LineMask          = 65536
ULONG c_BoxMask           = 131072
ULONG c_BMPMask           = 262144
ULONG c_RetrieveMask      = 524288
ULONG c_RetainMask        = 1048576
ULONG c_ExpandMask        = 2097152
ULONG c_RowFocusMask      = 4194304
ULONG c_HighlightMask     = 8388608
ULONG c_DrillDownMask     = 16777216
ULONG c_SelectMask        = 33554432
ULONG c_DragMask          = 67108864
ULONG c_DragModeMask      = 134217728

//------------------------------------------------------------------
//  Sets the default background color of the outliner.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_HLColorMask)
  
CHOOSE CASE l_BitMask
   CASE c_HLWhite
      c_HLWhite = 0
   CASE c_HLBlack
      c_HLWhite = c_HLBlack
      c_HLBlack = 0
   CASE c_HLGray
      c_HLWhite = c_HLGray
      c_HLGray = 0
   CASE c_HLDarkGray
      c_HLWhite = c_HLDarkGray
      c_HLDarkGray = 0
   CASE c_HLRed
      c_HLWhite = c_HLRed
      c_HLRed = 0
   CASE c_HLDarkRed
      c_HLWhite = c_HLDarkRed
      c_HLDarkRed = 0
   CASE c_HLGreen
      c_HLWhite = c_HLGreen
      c_HLGreen = 0
   CASE c_HLDarkGreen
      c_HLWhite = c_HLDarkGreen
      c_HLDarkGreen = 0
   CASE c_HLBlue
      c_HLWhite = c_HLBlue
      c_HLBlue = 0
   CASE c_HLDarkBlue
      c_HLWhite = c_HLDarkBlue
      c_HLDarkBlue = 0
   CASE c_HLMagenta
      c_HLWhite = c_HLMagenta
      c_HLMagenta = 0
   CASE c_HLDarkMagenta
      c_HLWhite = c_HLDarkMagenta
      c_HLDarkMagenta = 0
   CASE c_HLCyan
      c_HLWhite = c_HLCyan
      c_HLCyan = 0
   CASE c_HLDarkCyan
      c_HLWhite = c_HLDarkCyan
      c_HLDarkCyan = 0
   CASE c_HLYellow
      c_HLWhite = c_HLYellow
      c_HLYellow = 0
   CASE c_HLDarkYellow
      c_HLWhite = c_HLDarkYellow
      c_HLDarkYellow = 0
END CHOOSE

//------------------------------------------------------------------
//  Sets the default line color of the outliner.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_LineColorMask)
  
CHOOSE CASE l_BitMask
   CASE c_LineBlack
      c_LineBlack = 0
   CASE c_LineWhite
      c_LineBlack = c_LineWhite
      c_LineWhite = 0
   CASE c_LineGray
      c_LineBlack = c_LineGray
      c_LineGray = 0
   CASE c_LineDarkGray
      c_LineBlack = c_LineDarkGray
      c_LineDarkGray = 0
   CASE c_LineRed
      c_LineBlack = c_LineRed
      c_LineRed = 0
   CASE c_LineDarkRed
      c_LineBlack = c_LineDarkRed
      c_LineDarkRed = 0
   CASE c_LineGreen
      c_LineBlack = c_LineGreen
      c_LineGreen = 0
   CASE c_LineDarkGreen
      c_LineBlack = c_LineDarkGreen
      c_LineDarkGreen = 0
   CASE c_LineBlue
      c_LineBlack = c_LineBlue
      c_LineBlue = 0
   CASE c_LineDarkBlue
      c_LineBlack = c_LineDarkBlue
      c_LineDarkBlue = 0
   CASE c_LineMagenta
      c_LineBlack = c_LineMagenta
      c_LineMagenta = 0
   CASE c_LineDarkMagenta
      c_LineBlack = c_LineDarkMagenta
      c_LineDarkMagenta = 0
   CASE c_LineCyan
      c_LineBlack = c_LineCyan
      c_LineCyan = 0
   CASE c_LineDarkCyan
      c_LineBlack = c_LineDarkCyan
      c_LineDarkCyan = 0
   CASE c_LineYellow
      c_LineBlack = c_LineYellow
      c_LineYellow = 0
   CASE c_LineDarkYellow
      c_LineBlack = c_LineDarkYellow
      c_LineDarkYellow = 0
END CHOOSE

//------------------------------------------------------------------
//  Sets the default color of the row focus indicator rectangle.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_RowFocusColorMask)
  
CHOOSE CASE l_BitMask
   CASE c_RowFocusIndicatorGray
      c_RowFocusIndicatorGray = 0
   CASE c_RowFocusIndicatorWhite
      c_RowFocusIndicatorGray = c_RowFocusIndicatorWhite
      c_RowFocusIndicatorWhite = 0
   CASE c_RowFocusIndicatorBlack
      c_RowFocusIndicatorGray = c_RowFocusIndicatorBlack
      c_RowFocusIndicatorBlack = 0
   CASE c_RowFocusIndicatorDarkGray
      c_RowFocusIndicatorGray = c_RowFocusIndicatorDarkGray
      c_RowFocusIndicatorDarkGray = 0
   CASE c_RowFocusIndicatorRed
      c_RowFocusIndicatorGray = c_RowFocusIndicatorRed
      c_RowFocusIndicatorRed = 0
   CASE c_RowFocusIndicatorDarkRed
      c_RowFocusIndicatorGray = c_RowFocusIndicatorDarkRed
      c_RowFocusIndicatorDarkRed = 0
   CASE c_RowFocusIndicatorGreen
      c_RowFocusIndicatorGray = c_RowFocusIndicatorGreen
      c_RowFocusIndicatorGreen = 0
   CASE c_RowFocusIndicatorDarkGreen
      c_RowFocusIndicatorGray = c_RowFocusIndicatorDarkGreen
      c_RowFocusIndicatorDarkGreen = 0
   CASE c_RowFocusIndicatorBlue
      c_RowFocusIndicatorGray = c_RowFocusIndicatorBlue
      c_RowFocusIndicatorBlue = 0
   CASE c_RowFocusIndicatorDarkBlue
      c_RowFocusIndicatorGray = c_RowFocusIndicatorDarkBlue
      c_RowFocusIndicatorDarkBlue = 0
   CASE c_RowFocusIndicatorMagenta
      c_RowFocusIndicatorGray = c_RowFocusIndicatorMagenta
      c_RowFocusIndicatorMagenta = 0
   CASE c_RowFocusIndicatorDarkMagenta
      c_RowFocusIndicatorGray = c_RowFocusIndicatorDarkMagenta
      c_RowFocusIndicatorDarkMagenta = 0
   CASE c_RowFocusIndicatorCyan
      c_RowFocusIndicatorGray = c_RowFocusIndicatorCyan
      c_RowFocusIndicatorCyan = 0
   CASE c_RowFocusIndicatorDarkCyan
      c_RowFocusIndicatorGray = c_RowFocusIndicatorDarkCyan
      c_RowFocusIndicatorDarkCyan = 0
   CASE c_RowFocusIndicatorYellow
      c_RowFocusIndicatorGray = c_RowFocusIndicatorYellow
      c_RowFocusIndicatorYellow = 0
   CASE c_RowFocusIndicatorDarkYellow
      c_RowFocusIndicatorGray = c_RowFocusIndicatorDarkYellow
      c_RowFocusIndicatorDarkYellow = 0
END CHOOSE

//------------------------------------------------------------------
//  Sets the default color of highlighed records.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_HighColorMask)
  
CHOOSE CASE l_BitMask
   CASE c_HighlightDarkBlue
      c_HighlightDarkBlue = 0
   CASE c_HighlightBlack
      c_HighlightDarkBlue = c_HighlightBlack
      c_HighlightBlack = 0
   CASE c_HighlightWhite
      c_HighlightDarkBlue = c_HighlightWhite
      c_HighlightWhite = 0
   CASE c_HighlightGray
      c_HighlightDarkBlue = c_HighlightGray
      c_HighlightGray = 0
   CASE c_HighlightDarkGray
      c_HighlightDarkBlue = c_HighlightDarkGray
      c_HighlightDarkGray = 0
   CASE c_HighlightRed
      c_HighlightDarkBlue = c_HighlightRed
      c_HighlightRed = 0
   CASE c_HighlightDarkRed
      c_HighlightDarkBlue = c_HighlightDarkRed
      c_HighlightDarkRed = 0
   CASE c_HighlightGreen
      c_HighlightDarkBlue = c_HighlightGreen
      c_HighlightGreen = 0
   CASE c_HighlightDarkGreen
      c_HighlightDarkBlue = c_HighlightDarkGreen
      c_HighlightDarkGreen = 0
   CASE c_HighlightBlue
      c_HighlightDarkBlue = c_HighlightBlue
      c_HighlightBlue = 0
   CASE c_HighlightMagenta
      c_HighlightDarkBlue = c_HighlightMagenta
      c_HighlightMagenta = 0
   CASE c_HighlightDarkMagenta
      c_HighlightDarkBlue = c_HighlightDarkMagenta
      c_HighlightDarkMagenta = 0
   CASE c_HighlightCyan
      c_HighlightDarkBlue = c_HighlightCyan
      c_HighlightCyan = 0
   CASE c_HighlightDarkCyan
      c_HighlightDarkBlue = c_HighlightDarkCyan
      c_HighlightDarkCyan = 0
   CASE c_HighlightYellow
      c_HighlightDarkBlue = c_HighlightYellow
      c_HighlightYellow = 0
   CASE c_HighlightDarkYellow
      c_HighlightDarkBlue = c_HighlightDarkYellow
      c_HighlightDarkYellow = 0
END CHOOSE

//------------------------------------------------------------------
//  Sets the default for indicating whether connecting lines are to
//  be shown on the outliner.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_LineMask)

CHOOSE CASE l_BitMask
   CASE c_ShowLines
      c_ShowLines = 0
   CASE c_HideLines
      c_ShowLines = c_HideLines
      c_HideLines = 0
END CHOOSE

//------------------------------------------------------------------
//  Sets the default for indicating whether boxes that indicate
//  lower level data are to be shown on the outliner.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_BoxMask)

CHOOSE CASE l_BitMask
   CASE c_ShowBoxes
      c_ShowBoxes = 0
   CASE c_HideBoxes
      c_ShowBoxes = c_HideBoxes
      c_HideBoxes = 0
END CHOOSE

//------------------------------------------------------------------
//  Sets the default for indicating whether bitmaps are to
//  be shown on the outliner.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_BMPMask)

CHOOSE CASE l_BitMask
   CASE c_ShowBMP
      c_ShowBMP = 0
   CASE c_HideBMP
      c_ShowBMP = c_HideBMP
      c_HideBMP = 0
END CHOOSE

//------------------------------------------------------------------
//  Sets the default for indicating whether data for all levels are
//  to be retrieved up front or the retrieval at each branch is
//  done as needed.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_RetrieveMask)

CHOOSE CASE l_BitMask
   CASE c_RetrieveAll
      c_RetrieveAll = 0
   CASE c_RetrieveAsNeeded
      c_RetrieveAll = c_RetrieveAsNeeded
      c_RetrieveAsNeeded = 0
END CHOOSE

//------------------------------------------------------------------
//  Sets the default for indicating whether data is to be retained
//  when a branch is collapsed.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_RetainMask)

CHOOSE CASE l_BitMask
   CASE c_RetainOnCollapse
      c_RetainOnCollapse = 0
   CASE c_DeleteOnCollapse
      c_RetainOnCollapse = c_DeleteOnCollapse
      c_DeleteOnCollapse = 0
END CHOOSE

//------------------------------------------------------------------
//  Sets the default for indicating whether the outliner should be
//  created with all levels expanded.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_ExpandMask)

CHOOSE CASE l_BitMask
   CASE c_CollapseOnOpen
      c_CollapseOnOpen = 0
   CASE c_ExpandOnOpen
      c_CollapseOnOpen = c_ExpandOnOpen
      c_ExpandOnOpen = 0
END CHOOSE

//------------------------------------------------------------------
//  Sets the default for indicating whether a row focus indicator
//  rectangle should be shown.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_RowFocusMask)

CHOOSE CASE l_BitMask
   CASE c_ShowRowFocusIndicator
      c_ShowRowFocusIndicator = 0
   CASE c_HideRowFocusIndicator
      c_ShowRowFocusIndicator = c_HideRowFocusIndicator
      c_HideRowFocusIndicator = 0
END CHOOSE

//------------------------------------------------------------------
//  Sets the default for indicating whether selected records should
//  be highlighted.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_HighlightMask)

CHOOSE CASE l_BitMask
   CASE c_ShowHighlight
      c_ShowHighlight = 0
   CASE c_HideHighlight
      c_ShowHighlight = c_HideHighlight
      c_HideHighlight = 0
END CHOOSE

//------------------------------------------------------------------
//  Sets the default for indicating whether multiple records may be
//  selected.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_SelectMask)

CHOOSE CASE l_BitMask
   CASE c_SingleSelect
      c_SingleSelect = 0
   CASE c_Multiselect
      c_SingleSelect = c_Multiselect
      c_Multiselect = 0
END CHOOSE

//------------------------------------------------------------------
//  Sets the default for indicating whether selection occurs on a
//  doubleclick or a single click.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_DrillDownMask)

CHOOSE CASE l_BitMask
   CASE c_DrillDownOnDoubleClick
      c_DrillDownOnDoubleClick = 0
   CASE c_DrillDownOnClick
      c_DrillDownOnDoubleClick = c_DrillDownOnClick
      c_DrillDownOnClick = 0
END CHOOSE

//------------------------------------------------------------------
//  Sets the default for indicating whether drag and drop is allowed.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_DragMask)

CHOOSE CASE l_BitMask
   CASE c_NoAllowDragDrop
      c_NoAllowDragDrop = 0
   CASE c_AllowDragDrop
      c_NoAllowDragDrop = c_AllowDragDrop
      c_AllowDragDrop = 0
END CHOOSE

//------------------------------------------------------------------
//  Sets the default for indicating whether to move or copy records
//  when dragging records within the outliner.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_DragModeMask)

CHOOSE CASE l_BitMask
   CASE c_DragMoveRows
      c_DragMoveRows = 0
   CASE c_DragCopyRows
      c_DragMoveRows = c_DragCopyRows
      c_DragMoveRows = 0
END CHOOSE
end subroutine

public function integer fw_progressdefaults (unsignedlong visualword);//******************************************************************
//  PO Module     : w_POManager
//  Function      : fw_ProgressDefaults
//  Description   : Establishes the default characteristics of the
//                  progress meter object.
//
//  Parameters    : UNSIGNEDLONG VisualWord
//
//  Return Value  : (None)
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

ULONG      l_BitMask
ULONG      c_ProgressBGColorMask		= 15
ULONG      c_ProgressColorMask		= 240
ULONG      c_ProgressTextColorMask	= 3840
ULONG      c_ProgressMeterColorMask	= 61440
ULONG      c_ProgressBorderMask		= 458752
ULONG      c_ProgressDirectionMask	= 524288
ULONG      c_ProgressPercentMask		= 1048576

//------------------------------------------------------------------
//  Sets the default background color of the progress bar.  This 
//  color should be set to the same color as the parent (i.e. window).
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_ProgressBGColorMask)
  
CHOOSE CASE l_BitMask
   CASE c_ProgressBGGray
      c_ProgressBGWhite = c_ProgressBGGray
		c_ProgressBGGray = 0
   CASE c_ProgressBGBlack
      c_ProgressBGWhite = c_ProgressBGBlack
		c_ProgressBGBlack = 0
   CASE c_ProgressBGWhite
      c_ProgressBGWhite = 0
   CASE c_ProgressBGDarkGray
      c_ProgressBGWhite = c_ProgressBGDarkGray
		c_ProgressBGDarkGray = 0
   CASE c_ProgressBGRed
      c_ProgressBGWhite = c_ProgressBGRed
		c_ProgressBGRed = 0
   CASE c_ProgressDarkRed
      c_ProgressBGWhite = c_ProgressBGDarkRed
		c_ProgressBGDarkRed = 0
   CASE c_ProgressBGGreen
      c_ProgressBGWhite = c_ProgressBGGreen
		c_ProgressBGGreen = 0
   CASE c_ProgressBGDarkGreen
      c_ProgressBGWhite = c_ProgressBGDarkGreen
		c_ProgressBGDarkGreen = 0
   CASE c_ProgressBGBlue
      c_ProgressBGWhite = c_ProgressBGBlue
		c_ProgressBGBlue = 0
   CASE c_ProgressBGDarkBlue
      c_ProgressBGWhite = c_ProgressBGDarkBlue
		c_ProgressBGDarkBlue = 0
   CASE c_ProgressBGMagenta
      c_ProgressBGWhite = c_ProgressBGMagenta
		c_ProgressBGMagenta = 0
   CASE c_ProgressBGDarkMagenta
      c_ProgressBGWhite = c_ProgressBGDarkMagenta
		c_ProgressBGDarkMagenta = 0
   CASE c_ProgressBGCyan
      c_ProgressBGWhite = c_ProgressBGCyan
		c_ProgressBGCyan = 0
   CASE c_ProgressBGDarkCyan
      c_ProgressBGWhite = c_ProgressBGDarkCyan
		c_ProgressBGDarkCyan = 0
   CASE c_ProgressBGYellow
      c_ProgressBGWhite = c_ProgressBGYellow
		c_ProgressBGYellow = 0
   CASE c_ProgressBGDarkYellow
      c_ProgressBGWhite = c_ProgressBGDarkYellow
		c_ProgressBGDarkYellow = 0
END CHOOSE

//------------------------------------------------------------------
//  Sets the default color of the progress bar.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_ProgressColorMask)

CHOOSE CASE l_BitMask
   CASE c_ProgressGray
		c_ProgressGray = 0
   CASE c_ProgressBlack
      c_ProgressGray = c_ProgressBlack
		c_ProgressBlack = 0
   CASE c_ProgressWhite
      c_ProgressGray = c_ProgressWhite
      c_ProgressWhite = 0
   CASE c_ProgressDarkGray
      c_ProgressGray = c_ProgressDarkGray
		c_ProgressDarkGray = 0
   CASE c_ProgressRed
      c_ProgressGray = c_ProgressRed
		c_ProgressRed = 0
   CASE c_ProgressDarkRed
      c_ProgressGray = c_ProgressDarkRed
		c_ProgressDarkRed = 0
   CASE c_ProgressGreen
      c_ProgressGray = c_ProgressGreen
		c_ProgressGreen = 0
   CASE c_ProgressDarkGreen
      c_ProgressGray = c_ProgressDarkGreen
		c_ProgressDarkGreen = 0
   CASE c_ProgressBlue
      c_ProgressGray = c_ProgressBlue
		c_ProgressBlue = 0
   CASE c_ProgressDarkBlue
      c_ProgressGray = c_ProgressDarkBlue
		c_ProgressDarkBlue = 0
   CASE c_ProgressMagenta
      c_ProgressGray = c_ProgressMagenta
		c_ProgressMagenta = 0
   CASE c_ProgressDarkMagenta
      c_ProgressGray = c_ProgressDarkMagenta
		c_ProgressDarkMagenta = 0
   CASE c_ProgressCyan
      c_ProgressGray = c_ProgressCyan
		c_ProgressCyan = 0
   CASE c_ProgressDarkCyan
      c_ProgressGray = c_ProgressDarkCyan
		c_ProgressDarkCyan = 0
   CASE c_ProgressYellow
      c_ProgressGray = c_ProgressYellow
		c_ProgressYellow = 0
   CASE c_ProgressDarkYellow
      c_ProgressGray = c_ProgressDarkYellow
		c_ProgressDarkYellow = 0
END CHOOSE

//------------------------------------------------------------------
//  Sets the default color of the progress bar meter text.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_ProgressTextColorMask)

CHOOSE CASE l_BitMask
   CASE c_ProgressTextGray
      c_ProgressTextBlack = c_ProgressTextGray
		c_ProgressTextGray = 0
   CASE c_ProgressTextBlack
		c_ProgressTextBlack = 0
   CASE c_ProgressTextWhite
      c_ProgressTextBlack = c_ProgressTextWhite
		c_ProgressTextWhite = 0
   CASE c_ProgressTextDarkGray
      c_ProgressTextBlack = c_ProgressTextDarkGray
		c_ProgressTextDarkGray = 0
   CASE c_ProgressTextRed
      c_ProgressTextBlack = c_ProgressTextRed
		c_ProgressTextRed = 0
   CASE c_ProgressTextDarkRed
      c_ProgressTextBlack = c_ProgressTextDarkRed
		c_ProgressTextDarkRed = 0
   CASE c_ProgressTextGreen
      c_ProgressTextBlack = c_ProgressTextGreen
		c_ProgressTextGreen = 0
   CASE c_ProgressTextDarkGreen
      c_ProgressTextBlack = c_ProgressTextDarkGreen
		c_ProgressTextDarkGreen = 0
   CASE c_ProgressTextBlue
      c_ProgressTextBlack = c_ProgressTextBlue
		c_ProgressTextBlue = 0
   CASE c_ProgressTextDarkBlue
      c_ProgressTextBlack = c_ProgressTextDarkBlue
		c_ProgressTextDarkBlue = 0
   CASE c_ProgressTextMagenta
      c_ProgressTextBlack = c_ProgressTextMagenta
		c_ProgressTextMagenta = 0
   CASE c_ProgressTextDarkMagenta
      c_ProgressTextBlack = c_ProgressTextDarkMagenta
		c_ProgressTextDarkMagenta = 0
   CASE c_ProgressTextCyan
      c_ProgressTextBlack = c_ProgressTextCyan
		c_ProgressTextCyan = 0
   CASE c_ProgressTextDarkCyan
      c_ProgressTextBlack = c_ProgressTextDarkCyan
		c_ProgressTextDarkCyan = 0
   CASE c_ProgressTextYellow
      c_ProgressTextBlack = c_ProgressTextYellow
		c_ProgressTextYellow = 0
   CASE c_ProgressTextDarkYellow
      c_ProgressTextBlack = c_ProgressTextDarkYellow
		c_ProgressTextDarkYellow = 0
END CHOOSE

//------------------------------------------------------------------
//  Sets the default color of the progress bar meter.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_ProgressMeterColorMask)

CHOOSE CASE l_BitMask
   CASE c_ProgressMeterGray
      c_ProgressMeterBlue = c_ProgressMeterGray
		c_ProgressMeterGray = 0
   CASE c_ProgressMeterBlack
      c_ProgressMeterBlue = c_ProgressMeterBLack
		c_ProgressMeterBlack = 0
   CASE c_ProgressMeterWhite
      c_ProgressMeterBlue = c_ProgressMeterWhite
		c_ProgressMeterWhite = 0
   CASE c_ProgressMeterDarkGray
      c_ProgressMeterBlue = c_ProgressMeterDarkGray
		c_ProgressMeterDarkGray = 0
   CASE c_ProgressMeterRed
      c_ProgressMeterBlue = c_ProgressMeterRed
		c_ProgressMeterRed = 0
   CASE c_ProgressMeterDarkRed
      c_ProgressMeterBlue = c_ProgressMeterDarkRed
		c_ProgressMeterDarkRed = 0
   CASE c_ProgressMeterGreen
      c_ProgressMeterBlue = c_ProgressMeterGreen
		c_ProgressMeterGreen = 0
   CASE c_ProgressMeterDarkGreen
      c_ProgressMeterBlue = c_ProgressMeterDarkGreen
		c_ProgressMeterDarkGreen = 0
   CASE c_ProgressMeterBlue
		c_ProgressMeterBlue = 0
   CASE c_ProgressMeterDarkBlue
      c_ProgressMeterBlue = c_ProgressMeterDarkBlue
		c_ProgressMeterDarkBlue = 0
   CASE c_ProgressMeterMagenta
      c_ProgressMeterBlue = c_ProgressMeterMagenta
		c_ProgressMeterMagenta = 0
   CASE c_ProgressMeterDarkMagenta
      c_ProgressMeterBlue = c_ProgressMeterDarkMagenta
		c_ProgressMeterDarkMagenta = 0
   CASE c_ProgressMeterCyan
      c_ProgressMeterBlue = c_ProgressMeterCyan
		c_ProgressMeterCyan = 0
  CASE c_ProgressMeterDarkCyan
      c_ProgressMeterBlue = c_ProgressMeterDarkCyan
		c_ProgressMeterDarkCyan = 0
   CASE c_ProgressMeterYellow
      c_ProgressMeterBlue = c_ProgressMeterYellow
		c_ProgressMeterYellow = 0
   CASE c_ProgressMeterDarkYellow
      c_ProgressMeterBlue = c_ProgressMeterDarkYellow
		c_ProgressMeterDarkYellow = 0
END CHOOSE

//------------------------------------------------------------------
//  Sets the default style of the progress bar border.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_ProgressBorderMask)

CHOOSE CASE l_BitMask
   CASE c_ProgressBorderNone
      c_ProgressBorderNone = 0
   CASE c_ProgressBorderShadowBox
      c_ProgressBorderNone = c_ProgressBorderShadowBox
		c_ProgressBorderShadowBox = 0
   CASE c_ProgressBorderBox
      c_ProgressBorderNone = c_ProgressBorderBox
		c_ProgressBorderBox = 0
   CASE c_ProgressBorderLowered
      c_ProgressBorderNone = c_ProgressBorderLowered
		c_ProgressBorderLowered = 0
   CASE c_ProgressBorderRaised
      c_ProgressBorderNone = c_ProgressBorderRaised
		c_ProgressBorderRaised = 0
END CHOOSE

//------------------------------------------------------------------
//  Sets the default direction of the progress bar.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_ProgressDirectionMask)

CHOOSE CASE l_BitMask
   CASE c_ProgressDirectionHoriz
      c_ProgressDirectionHoriz = 0
   CASE c_ProgressDirectionVert
      c_ProgressDirectionHoriz = c_ProgressDirectionVert
		c_ProgressDirectionVert = 0
END CHOOSE

//------------------------------------------------------------------
//  Determones whether or not the percentage should be shown as text.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_ProgressPercentMask)

CHOOSE CASE l_BitMask
   CASE c_ProgressPercentShow
      c_ProgressPercentShow = 0
   CASE c_ProgressPercentHide
      c_ProgressPercentShow = c_ProgressPercentHide
		c_ProgressPercentHide = 0
END CHOOSE

return 0
end function

public function integer fw_setlogin (ref transaction trans_object, string usr_login, string usr_password);//******************************************************************
//  PO Module     : w_POManager_Main
//  Function      : fw_SetLogin
//  Description   : Sets the user/logon ID and password into the 
//                  transaction object depending on the type of 
//                  database being used.
//
//  Parameters    : TRANSACTION Trans_Object -
//                     Transaction object.
//                  STRING      Usr_Login    -
//                     User/Logon name.
//                  STRING      Usr_Password -
//                     Log/DB password name.
//
//  Return Value  : INTEGER
//                      0 = database found
//                     -1 = database not found
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

STRING  l_DBParm, l_String, l_Char
INTEGER l_StartPos, l_EndPos, l_Idx, l_Return = 0
ULONG    l_DBMS
BOOLEAN  l_ODBC

w_POManager_STD.MB.fu_GetDatabaseType(trans_object, &
                                      l_DBMS, l_ODBC)

CHOOSE CASE l_DBMS

	CASE w_POManager_STD.MB.c_DB_Sybase, w_POManager_STD.MB.c_DB_SQLServer, &
        w_POManager_STD.MB.c_DB_MDG_DB2, w_POManager_STD.MB.c_DB_Oracle6, &
        w_POManager_STD.MB.c_DB_Oracle7, w_POManager_STD.MB.c_DB_XDB, &
        w_POManager_STD.MB.c_DB_SybaseNet, w_POManager_STD.MB.c_DB_Sybase49

		trans_object.LogID   = Trim(usr_login)
		trans_object.LogPass = Trim(usr_password)

	case w_POManager_STD.MB.c_DB_Gupta, w_POManager_STD.MB.c_DB_Allbase, &
        w_POManager_STD.MB.c_DB_Informix4, w_POManager_STD.MB.c_DB_Informix5, &
        w_POManager_STD.MB.c_DB_IBMDRDA

		trans_object.UserID  = Trim(usr_login)
		trans_object.DBPass  = Trim(usr_password)

	case w_POManager_STD.MB.c_DB_ODBC, w_POManager_STD.MB.c_DB_Watcom, &
        w_POManager_STD.MB.c_DB_DecRdb

		trans_object.UserID  = Trim(usr_login)
		trans_object.DBPass  = Trim(usr_password)

      l_DBParm = trans_object.DBParm
      FOR l_Idx = 1 TO 2
         IF l_Idx = 1 THEN
            l_String = "UID="
         ELSE
            l_String = "PWD="
         END IF

         l_StartPos = POS(UPPER(l_DBParm), l_String)
         IF l_StartPos > 0 THEN
            l_StartPos = l_StartPos + 4
            l_EndPos = l_StartPos
            DO
               l_EndPos = l_EndPos + 1
               l_Char = MID(l_DBParm, l_EndPos, 1)
            LOOP UNTIL l_Char = ";" OR l_Char = "'" OR l_EndPos = Len(l_DBParm)

            IF l_Char <> ";" AND l_Char <> "'" THEN
               l_EndPos = Len(l_DBParm) + 1
            END IF

            IF l_Idx = 1 THEN
               l_DBParm = REPLACE(l_DBParm, l_StartPos, l_EndPos - l_StartPos, trans_object.UserID)
            ELSE
               l_DBParm = REPLACE(l_DBParm, l_StartPos, l_EndPos - l_StartPos, trans_object.DBPass)
            END IF
         END IF
      NEXT
      trans_object.DBParm = l_DBParm

   CASE ELSE

      l_Return = -1

END CHOOSE

RETURN l_Return
end function

public function string fw_getlogin (transaction trans_object);//******************************************************************
//  PO Module     : w_POManager_Main
//  Function      : fw_GetLogin
//  Description   : Gets the user/logon ID from the transaction
//                  object depending on the type of database being
//                  used.
//
//  Parameters    : TRANSACTION Trans_Object -
//                     Transaction object.
//
//  Return Value  : STRING
//                     User/logon name.
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

ULONG    l_DBMS
BOOLEAN  l_ODBC

w_POManager_STD.MB.fu_GetDatabaseType(trans_object, &
                                      l_DBMS, l_ODBC)

CHOOSE CASE l_DBMS

	CASE w_POManager_STD.MB.c_DB_Sybase, w_POManager_STD.MB.c_DB_SQLServer, &
        w_POManager_STD.MB.c_DB_MDG_DB2, w_POManager_STD.MB.c_DB_Oracle6, &
        w_POManager_STD.MB.c_DB_Oracle7, w_POManager_STD.MB.c_DB_XDB, &
        w_POManager_STD.MB.c_DB_SybaseNet, w_POManager_Std.MB.c_DB_Sybase49

		RETURN trans_object.LogID

	case w_POManager_STD.MB.c_DB_Gupta, w_POManager_STD.MB.c_DB_Allbase, &
        w_POManager_STD.MB.c_DB_Informix4, w_POManager_STD.MB.c_DB_Informix5, &
        w_POManager_STD.MB.c_DB_IBMDRDA

		RETURN trans_object.UserID

	case w_POManager_STD.MB.c_DB_ODBC, w_POManager_STD.MB.c_DB_Watcom, &
        w_POManager_STD.MB.c_DB_DecRdb

		RETURN trans_object.UserID

   CASE ELSE

      RETURN ""

END CHOOSE

end function

public subroutine fw_windowdefaults (string textfont, integer textsize, unsignedlong visualword);//******************************************************************
//  PO Module     : w_POManager_Main
//  Subroutine    : fw_WindowDefaults
//  Description   : Sets the default characteristics for all
//                  utility windows.  
//
//  Parameters    : STRING  TextFont   - font style of the text
//                                       controls in the window.
//                  STRING  TextSize   - text size in points.
//                  ULONG   VisualWord - visual options for the
//                                       utility windows.
//
//  Return Value  : (None)
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

ULONG l_BitMask
ULONG c_WindowColorMask     = 3
ULONG c_WindowTextColorMask = 12

//------------------------------------------------------------------
//  Set the font style of the text for the utility windows.
//------------------------------------------------------------------

IF TextFont <> c_DefaultFont THEN
   i_WindowTextFont = TextFont
END IF

//------------------------------------------------------------------
//  Set the font size of the text for the utility windows.
//------------------------------------------------------------------

IF TextSize <> c_DefaultSize THEN
   i_WindowTextSize = TextSize * -1
END IF

//------------------------------------------------------------------
//  Sets the default window color for the utility windows.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_WindowColorMask)

CHOOSE CASE l_BitMask
   CASE c_WindowGray
      c_WindowGray = 0
   CASE c_WindowBlack
      c_WindowGray = c_WindowBlack
      c_WindowBlack = 0
   CASE c_WindowWhite
      c_WindowGray = c_WindowWhite
      c_WindowWhite = 0
   CASE c_WindowDarkGray
      c_WindowGray = c_WindowDarkGray
      c_WindowDarkGray = 0
   CASE c_WindowRed
      c_WindowGray = c_WindowRed
      c_WindowRed = 0
   CASE c_WindowDarkRed
      c_WindowGray = c_WindowDarkRed
      c_WindowDarkRed = 0
   CASE c_WindowGreen
      c_WindowGray = c_WindowGreen
      c_WindowGreen = 0
   CASE c_WindowDarkGreen
      c_WindowGray = c_WindowDarkGreen
      c_WindowDarkGreen = 0
   CASE c_WindowBlue
      c_WindowGray = c_WindowBlue
      c_WindowBlue = 0
   CASE c_WindowDarkBlue
      c_WindowGray = c_WindowDarkBlue
      c_WindowDarkBlue = 0
   CASE c_WindowMagenta
      c_WindowGray = c_WindowMagenta
      c_WindowMagenta = 0
   CASE c_WindowDarkMagenta
      c_WindowGray = c_WindowDarkMagenta
      c_WindowDarkMagenta = 0
   CASE c_WindowCyan
      c_WindowGray = c_WindowCyan
      c_WindowCyan = 0
   CASE c_WindowDarkCyan
      c_WindowGray = c_WindowDarkCyan
      c_WindowDarkCyan = 0
   CASE c_WindowYellow
      c_WindowGray = c_WindowYellow
      c_WindowYellow = 0
   CASE c_WindowDarkYellow
      c_WindowGray = c_WindowDarkYellow
      c_WindowDarkYellow = 0
END CHOOSE

//------------------------------------------------------------------
//  Sets the default window text color for the utility windows.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_WindowTextColorMask)

CHOOSE CASE l_BitMask
   CASE c_WindowTextDarkBlue
      c_WindowTextDarkBlue = 0
   CASE c_WindowTextBlack
      c_WindowTextDarkBlue = c_WindowTextBlack
      c_WindowTextBlack = 0
   CASE c_WindowTextWhite
      c_WindowTextDarkBlue = c_WindowTextWhite
      c_WindowTextWhite = 0
   CASE c_WindowTextGray
      c_WindowTextDarkBlue = c_WindowTextGray
      c_WindowTextGray = 0
   CASE c_WindowTextDarkGray
      c_WindowTextDarkBlue = c_WindowTextDarkGray
      c_WindowTextDarkGray = 0
   CASE c_WindowTextRed
      c_WindowTextDarkBlue = c_WindowTextRed
      c_WindowTextRed = 0
   CASE c_WindowTextDarkRed
      c_WindowTextDarkBlue = c_WindowTextDarkRed
      c_WindowTextDarkRed = 0
   CASE c_WindowTextGreen
      c_WindowTextDarkBlue = c_WindowTextGreen
      c_WindowTextGreen = 0
   CASE c_WindowTextDarkGreen
      c_WindowTextDarkBlue = c_WindowTextDarkGreen
      c_WindowTextDarkGreen = 0
   CASE c_WindowTextBlue
      c_WindowTextDarkBlue = c_WindowTextBlue
      c_WindowTextBlue = 0
   CASE c_WindowTextMagenta
      c_WindowTextDarkBlue = c_WindowTextMagenta
      c_WindowTextMagenta = 0
   CASE c_WindowTextDarkMagenta
      c_WindowTextDarkBlue = c_WindowTextDarkMagenta
      c_WindowTextDarkMagenta = 0
   CASE c_WindowTextCyan
      c_WindowTextDarkBlue = c_WindowTextCyan
      c_WindowTextCyan = 0
   CASE c_WindowTextDarkCyan
      c_WindowTextDarkBlue = c_WindowTextDarkCyan
      c_WindowTextDarkCyan = 0
   CASE c_WindowTextYellow
      c_WindowTextDarkBlue = c_WindowTextYellow
      c_WindowTextYellow = 0
   CASE c_WindowTextDarkYellow
      c_WindowTextDarkBlue = c_WindowTextDarkYellow
      c_WindowTextDarkYellow = 0
END CHOOSE


end subroutine

public subroutine fw_caldefaults (unsignedlong visualword);//******************************************************************
//  PO Module     : w_POManager_Main
//  Function      : fw_CalDefaults
//  Description   : Establishes the default characteristics of the
//                  calendar portion of the calendar object.
//
//  Parameters    : UNSIGNEDLONG VisualWord
//
//  Return Value  : (None)
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

ULONG      l_BitMask
ULONG      c_CalColorMask   = 15
ULONG      c_CalWEColorMask = 240
ULONG      c_CalSortMask    = 768
ULONG      c_CalStyleMask   = 1024

//------------------------------------------------------------------
//  Sets the default color for the weekdays of the Calendar.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_CalColorMask)

CHOOSE CASE l_BitMask
   CASE c_CalGray
      c_CalGray = 0
   CASE c_CalBlack
      c_CalGray = c_CalBlack
      c_CalBlack = 0
   CASE c_CalWhite
      c_CalGray = c_CalWhite
      c_CalWhite = 0
   CASE c_CalDarkGray
      c_CalGray = c_CalDarkGray
      c_CalDarkGray = 0
   CASE c_CalRed
      c_CalGray = c_CalRed
      c_CalRed = 0
   CASE c_CalDarkRed
      c_CalGray = c_CalDarkRed
      c_CalDarkRed = 0
   CASE c_CalGreen
      c_CalGray = c_CalGreen
      c_CalGreen = 0
   CASE c_CalDarkGreen
      c_CalGray = c_CalDarkGreen
      c_CalDarkGreen = 0
   CASE c_CalBlue
      c_CalGray = c_CalBlue
      c_CalBlue = 0
   CASE c_CalDarkBlue
      c_CalGray = c_CalDarkBlue
      c_CalDarkBlue = 0
   CASE c_CalMagenta
      c_CalGray = c_CalMagenta
      c_CalMagenta = 0
   CASE c_CalDarkMagenta
      c_CalGray = c_CalDarkMagenta
      c_CalDarkMagenta = 0
   CASE c_CalCyan
      c_CalGray = c_CalCyan
      c_CalCyan = 0
   CASE c_CalDarkCyan
      c_CalGray = c_CalDarkCyan
      c_CalDarkCyan = 0
   CASE c_CalYellow
      c_CalGray = c_CalYellow
      c_CalYellow = 0
   CASE c_CalDarkYellow
      c_CalGray = c_CalDarkYellow
      c_CalDarkYellow = 0
END CHOOSE

//------------------------------------------------------------------
//  Sets the Calendar style.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_CalStyleMask)

CHOOSE CASE l_BitMask
   CASE c_CalStyle3D
      c_CalStyle3D = 0
   CASE c_CalStyle2D
      c_CalStyle3D = c_CalStyle2D
      c_CalStyle2D = 0
END CHOOSE

//------------------------------------------------------------------
//  Sets the default color for the weekends of the Calendar.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_CalWEColorMask)

CHOOSE CASE l_BitMask
   CASE c_CalWEGray
      c_CalWEGray = 0
   CASE c_CalWEBlack
      c_CalWEGray= c_CalWEBlack
      c_CalWEBlack = 0
   CASE c_CalWEWhite
      c_CalWEGray= c_CalWEWhite
      c_CalWEWhite = 0
   CASE c_CalWEDarkGray
      c_CalWEGray= c_CalWEDarkGray
      c_CalWEDarkGray = 0
   CASE c_CalWERed
      c_CalWEGray= c_CalWERed
      c_CalWERed = 0
   CASE c_CalWEDarkRed
      c_CalWEGray= c_CalWEDarkRed
      c_CalWEDarkRed = 0
   CASE c_CalWEGreen
      c_CalWEGray= c_CalWEGreen
      c_CalWEDarkGreen = 0
   CASE c_CalWEDarkGreen
      c_CalWEGray= c_CalWEDarkGreen
      c_CalWEDarkGreen = 0
   CASE c_CalWEBlue
      c_CalWEGray= c_CalWEBlue
      c_CalWEBlue = 0
   CASE c_CalWEDarkBlue
      c_CalWEGray= c_CalWEDarkBlue
      c_CalWEDarkBlue = 0
   CASE c_CalWEMagenta
      c_CalWEGray= c_CalWEMagenta
      c_CalWEMagenta = 0
   CASE c_CalWEDarkMagenta
      c_CalWEGray= c_CalWEDarkMagenta
      c_CalWEDarkMagenta = 0
   CASE c_CalWECyan
      c_CalWEGray= c_CalWECyan
      c_CalWECyan = 0
   CASE c_CalWEDarkCyan
      c_CalWEGray= c_CalWEDarkCyan
      c_CalWEDarkCyan = 0
   CASE c_CalWEYellow
      c_CalWEGray= c_CalWEYellow
      c_CalWEYellow = 0
   CASE c_CalWEDarkYellow
      c_CalWEGray= c_CalWEDarkYellow
      c_CalWEDarkYellow = 0
END CHOOSE

//------------------------------------------------------------------
//  Sets the sort array for the calendar.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_CalSortMask)

CHOOSE CASE l_BitMask
   CASE c_CalSortAsc
      c_CalSortAsc = 0
   CASE c_CalSortDesc
      c_CalSortAsc = c_CalSortDesc
      c_CalSortDesc = 0
   CASE c_CalNoSort
      c_CalSortAsc = c_CalNoSort
      c_CalNoSort = 0
END CHOOSE


end subroutine

public subroutine fw_sliderdefaults (unsignedlong visualword);//******************************************************************
//  PO Module     : w_POManager_Main
//  Function      : fw_SliderDefaults
//  Description   : Establishes the default characteristics of the
//                  slider object.
//
//  Parameters    : UNSIGNEDLONG VisualWord
//
//  Return Value  : (None)
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

ULONG      l_BitMask
ULONG c_SliderBGColorMask = 15
ULONG c_SliderFrameColorMask = 240
ULONG c_SliderBarColorMask = 3840
ULONG c_SliderCenterLineColorMask = 61440
ULONG	c_SliderFrameMask = 458752
ULONG	c_SliderBorderMask = 3670016
ULONG	c_SliderCenterLineMask = 4194304
ULONG	c_SliderDirectionMask = 8388608
ULONG	c_SliderIndicatorMask = 16777216

ULONG c_SliderTextColorMask = 15
ULONG	c_SliderTextMask = 32

ULONG c_SliderTickColorMask = 15
//------------------------------------------------------------------
//  Sets the default background color of the slider.  This 
//  color should be set to the same color as the parent (i.e. window).
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_SliderBGColorMask)
  
CHOOSE CASE l_BitMask
   CASE c_SliderBGGray
      c_SliderBGWhite = c_SliderBGGray
		c_SliderBGGray = 0
   CASE c_SliderBGBlack
      c_SliderBGWhite = c_SliderBGBlack
		c_SliderBGBlack = 0
   CASE c_SliderBGWhite
      c_SliderBGWhite = 0
   CASE c_SliderBGDarkGray
      c_SliderBGWhite = c_SliderBGDarkGray
		c_SliderBGDarkGray = 0
   CASE c_SliderBGRed
      c_SliderBGWhite = c_SliderBGRed
		c_SliderBGRed = 0
   CASE c_SliderBGDarkRed
      c_SliderBGWhite = c_SliderBGDarkRed
		c_SliderBGDarkRed = 0
   CASE c_SliderBGGreen
      c_SliderBGWhite = c_SliderBGGreen
		c_SliderBGGreen = 0
   CASE c_SliderBGDarkGreen
      c_SliderBGWhite = c_SliderBGDarkGreen
		c_SliderBGDarkGreen = 0
   CASE c_SliderBGBlue
      c_SliderBGWhite = c_SliderBGBlue
		c_SliderBGBlue = 0
   CASE c_SliderBGDarkBlue
      c_SliderBGWhite = c_SliderBGDarkBlue
		c_SliderBGDarkBlue = 0
   CASE c_SliderBGMagenta
      c_SliderBGWhite = c_SliderBGMagenta
		c_SliderBGMagenta = 0
   CASE c_SliderBGDarkMagenta
      c_SliderBGWhite = c_SliderBGDarkMagenta
		c_SliderBGDarkMagenta = 0
   CASE c_SliderBGCyan
      c_SliderBGWhite = c_SliderBGCyan
		c_SliderBGCyan = 0
   CASE c_SliderBGDarkCyan
      c_SliderBGWhite = c_SliderBGDarkCyan
		c_SliderBGDarkCyan = 0
   CASE c_SliderBGYellow
      c_SliderBGWhite = c_SliderBGYellow
		c_SliderBGYellow = 0
   CASE c_SliderBGDarkYellow
      c_SliderBGWhite = c_SliderBGDarkYellow
		c_SliderBGDarkYellow = 0
END CHOOSE

//------------------------------------------------------------------
//  Sets the default color of the Slider frame.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_SliderFrameColorMask)

CHOOSE CASE l_BitMask
   CASE c_SliderFrameGray
		c_SliderFrameGray = 0
   CASE c_SliderFrameBlack
      c_SliderFrameGray = c_SliderFrameBlack
		c_SliderFrameBlack = 0
   CASE c_SliderFrameWhite
      c_SliderFrameGray = c_SliderFrameWhite
      c_SliderFrameWhite = 0
   CASE c_SliderFrameDarkGray
      c_SliderFrameGray = c_SliderFrameDarkGray
		c_SliderFrameDarkGray = 0
   CASE c_SliderFrameRed
      c_SliderFrameGray = c_SliderFrameRed
		c_SliderFrameRed = 0
   CASE c_SliderFrameDarkRed
      c_SliderFrameGray = c_SliderFrameDarkRed
		c_SliderFrameDarkRed = 0
   CASE c_SliderFrameGreen
      c_SliderFrameGray = c_SliderFrameGreen
		c_SliderFrameGreen = 0
   CASE c_SliderFrameDarkGreen
      c_SliderFrameGray = c_SliderFrameDarkGreen
		c_SliderFrameDarkGreen = 0
   CASE c_SliderFrameBlue
      c_SliderFrameGray = c_SliderFrameBlue
		c_SliderFrameBlue = 0
   CASE c_SliderFrameDarkBlue
      c_SliderFrameGray = c_SliderFrameDarkBlue
		c_SliderFrameDarkBlue = 0
   CASE c_SliderFrameMagenta
      c_SliderFrameGray = c_SliderFrameMagenta
		c_SliderFrameMagenta = 0
   CASE c_SliderFrameDarkMagenta
      c_SliderFrameGray = c_SliderFrameDarkMagenta
		c_SliderFrameDarkMagenta = 0
   CASE c_SliderFrameCyan
      c_SliderFrameGray = c_SliderFrameCyan
		c_SliderFrameCyan = 0
   CASE c_SliderFrameDarkCyan
      c_SliderFrameGray = c_SliderFrameDarkCyan
		c_SliderFrameDarkCyan = 0
   CASE c_SliderFrameYellow
      c_SliderFrameGray = c_SliderFrameYellow
		c_SliderFrameYellow = 0
   CASE c_SliderFrameDarkYellow
      c_SliderFrameGray = c_SliderFrameDarkYellow
		c_SliderFrameDarkYellow = 0
END CHOOSE

//------------------------------------------------------------------
//  Sets the default color of the Slider bar.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_SliderBarColorMask)

CHOOSE CASE l_BitMask
   CASE c_SliderBarGray
      c_SliderBarBlack = c_SliderBarGray
		c_SliderBarGray = 0
   CASE c_SliderBarBlack
		c_SliderBarBlack = 0
   CASE c_SliderBarWhite
      c_SliderBarBlack = c_SliderBarWhite
		c_SliderBarWhite = 0
   CASE c_SliderBarDarkGray
      c_SliderBarBlack = c_SliderBarDarkGray
		c_SliderBarDarkGray = 0
   CASE c_SliderBarRed
      c_SliderBarBlack = c_SliderBarRed
		c_SliderBarRed = 0
   CASE c_SliderBarDarkRed
      c_SliderBarBlack = c_SliderBarDarkRed
		c_SliderBarDarkRed = 0
   CASE c_SliderBarGreen
      c_SliderBarBlack = c_SliderBarGreen
		c_SliderBarGreen = 0
   CASE c_SliderBarDarkGreen
      c_SliderBarBlack = c_SliderBarDarkGreen
		c_SliderBarDarkGreen = 0
   CASE c_SliderBarBlue
      c_SliderBarBlack = c_SliderBarBlue
		c_SliderBarBlue = 0
   CASE c_SliderBarDarkBlue
      c_SliderBarBlack = c_SliderBarDarkBlue
		c_SliderBarDarkBlue = 0
   CASE c_SliderBarMagenta
      c_SliderBarBlack = c_SliderBarMagenta
		c_SliderBarMagenta = 0
   CASE c_SliderBarDarkMagenta
      c_SliderBarBlack = c_SliderBarDarkMagenta
		c_SliderBarDarkMagenta = 0
   CASE c_SliderBarCyan
      c_SliderBarBlack = c_SliderBarCyan
		c_SliderBarCyan = 0
   CASE c_SliderBarDarkCyan
      c_SliderBarBlack = c_SliderBarDarkCyan
		c_SliderBarDarkCyan = 0
   CASE c_SliderBarYellow
      c_SliderBarBlack = c_SliderBarYellow
		c_SliderBarYellow = 0
   CASE c_SliderBarDarkYellow
      c_SliderBarBlack = c_SliderBarDarkYellow
		c_SliderBarDarkYellow = 0
END CHOOSE

//------------------------------------------------------------------
//  Sets the default color of the Slider center line.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_SliderCenterLineColorMask)

CHOOSE CASE l_BitMask
   CASE c_SliderCenterLineGray
      c_SliderCenterLineBlue = c_SliderCenterLineGray
		c_SliderCenterLineGray = 0
   CASE c_SliderCenterLineBlack
      c_SliderCenterLineBlue = c_SliderCenterLineBLack
		c_SliderCenterLineBlack = 0
   CASE c_SliderCenterLineWhite
      c_SliderCenterLineBlue = c_SliderCenterLineWhite
		c_SliderCenterLineWhite = 0
   CASE c_SliderCenterLineDarkGray
      c_SliderCenterLineBlue = c_SliderCenterLineDarkGray
		c_SliderCenterLineDarkGray = 0
   CASE c_SliderCenterLineRed
      c_SliderCenterLineBlue = c_SliderCenterLineRed
		c_SliderCenterLineRed = 0
   CASE c_SliderCenterLineDarkRed
      c_SliderCenterLineBlue = c_SliderCenterLineDarkRed
		c_SliderCenterLineDarkRed = 0
   CASE c_SliderCenterLineGreen
      c_SliderCenterLineBlue = c_SliderCenterLineGreen
		c_SliderCenterLineGreen = 0
   CASE c_SliderCenterLineDarkGreen
      c_SliderCenterLineBlue = c_SliderCenterLineDarkGreen
		c_SliderCenterLineDarkGreen = 0
   CASE c_SliderCenterLineBlue
		c_SliderCenterLineBlue = 0
   CASE c_SliderCenterLineDarkBlue
      c_SliderCenterLineBlue = c_SliderCenterLineDarkBlue
		c_SliderCenterLineDarkBlue = 0
   CASE c_SliderCenterLineMagenta
      c_SliderCenterLineBlue = c_SliderCenterLineMagenta
		c_SliderCenterLineMagenta = 0
   CASE c_SliderCenterLineDarkMagenta
      c_SliderCenterLineBlue = c_SliderCenterLineDarkMagenta
		c_SliderCenterLineDarkMagenta = 0
   CASE c_SliderCenterLineCyan
      c_SliderCenterLineBlue = c_SliderCenterLineCyan
		c_SliderCenterLineCyan = 0
  CASE c_SliderCenterLineDarkCyan
      c_SliderCenterLineBlue = c_SliderCenterLineDarkCyan
		c_SliderCenterLineDarkCyan = 0
   CASE c_SliderCenterLineYellow
      c_SliderCenterLineBlue = c_SliderCenterLineYellow
		c_SliderCenterLineYellow = 0
   CASE c_SliderCenterLineDarkYellow
      c_SliderCenterLineBlue = c_SliderCenterLineDarkYellow
		c_SliderCenterLineDarkYellow = 0
END CHOOSE

//------------------------------------------------------------------
//  Sets the default style of the Slider frame border.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_SliderFrameMask)

CHOOSE CASE l_BitMask
   CASE c_SliderFrameNone
      c_SliderFrameNone = 0
   CASE c_SliderFrameShadowBox
      c_SliderFrameNone = c_SliderFrameShadowBox
		c_SliderFrameShadowBox = 0
   CASE c_SliderFrameBox
      c_SliderFrameNone = c_SliderFrameBox
		c_SliderFrameBox = 0
   CASE c_SliderFrameLowered
      c_SliderFrameNone = c_SliderFrameLowered
		c_SliderFrameLowered = 0
   CASE c_SliderFrameRaised
      c_SliderFrameNone = c_SliderFrameRaised
		c_SliderFrameRaised = 0
END CHOOSE


//------------------------------------------------------------------
//  Sets the default style of the Slider border.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_SliderBorderMask)

CHOOSE CASE l_BitMask
   CASE c_SliderBorderNone
      c_SliderBorderNone = 0
   CASE c_SliderBorderShadowBox
      c_SliderBorderNone = c_SliderBorderShadowBox
		c_SliderBorderShadowBox = 0
   CASE c_SliderBorderBox
      c_SliderBorderNone = c_SliderBorderBox
		c_SliderBorderBox = 0
   CASE c_SliderBorderLowered
      c_SliderBorderNone = c_SliderBorderLowered
		c_SliderBorderLowered = 0
   CASE c_SliderBorderRaised
      c_SliderBorderNone = c_SliderBorderRaised
		c_SliderBorderRaised = 0
END CHOOSE

//------------------------------------------------------------------
//  Sets the default direction of the Slider.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_SliderDirectionMask)

CHOOSE CASE l_BitMask
   CASE c_SliderDirectionHoriz
      c_SliderDirectionHoriz = 0
   CASE c_SliderDirectionVert
      c_SliderDirectionHoriz = c_SliderDirectionVert
		c_SliderDirectionVert = 0
END CHOOSE

//------------------------------------------------------------------
//  Determines whether or not the center line should be shown.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_SliderCenterLineMask)

CHOOSE CASE l_BitMask
   CASE c_SliderCenterLineShow
      c_SliderCenterLineShow = 0
   CASE c_SliderCenterLineHide
      c_SliderCenterLineShow = c_SliderCenterLineHide
		c_SliderCenterLineHide = 0
END CHOOSE

//------------------------------------------------------------------
//  Determines whether the indicator should be inside or outside
//  the slider bar.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_SliderIndicatorMask)

CHOOSE CASE l_BitMask
   CASE c_SliderIndicatorIn
      c_SliderIndicatorIn = 0
   CASE c_SliderIndicatorOut
      c_SliderIndicatorIn = c_SliderIndicatorOut
		c_SliderIndicatorOut = 0
END CHOOSE

//------------------------------------------------------------------
//  Sets the default color of the Slider text.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_SliderTextColorMask)

CHOOSE CASE l_BitMask
   CASE c_SliderTextGray
		c_SliderTextGray = 0
   CASE c_SliderTextBlack
      c_SliderTextGray = c_SliderTextBlack
		c_SliderTextBlack = 0
   CASE c_SliderTextWhite
      c_SliderTextGray = c_SliderTextWhite
      c_SliderTextWhite = 0
   CASE c_SliderTextDarkGray
      c_SliderTextGray = c_SliderTextDarkGray
		c_SliderTextDarkGray = 0
   CASE c_SliderTextRed
      c_SliderTextGray = c_SliderTextRed
		c_SliderTextRed = 0
   CASE c_SliderTextDarkRed
      c_SliderTextGray = c_SliderTextDarkRed
		c_SliderTextDarkRed = 0
   CASE c_SliderTextGreen
      c_SliderTextGray = c_SliderTextGreen
		c_SliderTextGreen = 0
   CASE c_SliderTextDarkGreen
      c_SliderTextGray = c_SliderTextDarkGreen
		c_SliderTextDarkGreen = 0
   CASE c_SliderTextBlue
      c_SliderTextGray = c_SliderTextBlue
		c_SliderTextBlue = 0
   CASE c_SliderTextDarkBlue
      c_SliderTextGray = c_SliderTextDarkBlue
		c_SliderTextDarkBlue = 0
   CASE c_SliderTextMagenta
      c_SliderTextGray = c_SliderTextMagenta
		c_SliderTextMagenta = 0
   CASE c_SliderTextDarkMagenta
      c_SliderTextGray = c_SliderTextDarkMagenta
		c_SliderTextDarkMagenta = 0
   CASE c_SliderTextCyan
      c_SliderTextGray = c_SliderTextCyan
		c_SliderTextCyan = 0
   CASE c_SliderTextDarkCyan
      c_SliderTextGray = c_SliderTextDarkCyan
		c_SliderTextDarkCyan = 0
   CASE c_SliderTextYellow
      c_SliderTextGray = c_SliderTextYellow
		c_SliderTextYellow = 0
   CASE c_SliderTextDarkYellow
      c_SliderTextGray = c_SliderTextDarkYellow
		c_SliderTextDarkYellow = 0
END CHOOSE

//------------------------------------------------------------------
//  Determines whether or not the slider text should be shown.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_SliderTextMask)

CHOOSE CASE l_BitMask
   CASE c_SliderTextShow
      c_SliderTextShow = 0
   CASE c_SliderTextHide
      c_SliderTextShow = c_SliderTextHide
		c_SliderTextHide = 0
END CHOOSE

//------------------------------------------------------------------
//  Sets the default color of the Slider tick marks.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_SliderTickColorMask)

CHOOSE CASE l_BitMask
   CASE c_SliderTickGray
		c_SliderTickGray = 0
   CASE c_SliderTickBlack
      c_SliderTickGray = c_SliderTickBlack
		c_SliderTickBlack = 0
   CASE c_SliderTickWhite
      c_SliderTickGray = c_SliderTickWhite
      c_SliderTickWhite = 0
   CASE c_SliderTickDarkGray
      c_SliderTickGray = c_SliderTickDarkGray
		c_SliderTickDarkGray = 0
   CASE c_SliderTickRed
      c_SliderTickGray = c_SliderTickRed
		c_SliderTickRed = 0
   CASE c_SliderTickDarkRed
      c_SliderTickGray = c_SliderTickDarkRed
		c_SliderTickDarkRed = 0
   CASE c_SliderTickGreen
      c_SliderTickGray = c_SliderTickGreen
		c_SliderTickGreen = 0
   CASE c_SliderTickDarkGreen
      c_SliderTickGray = c_SliderTickDarkGreen
		c_SliderTickDarkGreen = 0
   CASE c_SliderTickBlue
      c_SliderTickGray = c_SliderTickBlue
		c_SliderTickBlue = 0
   CASE c_SliderTickDarkBlue
      c_SliderTickGray = c_SliderTickDarkBlue
		c_SliderTickDarkBlue = 0
   CASE c_SliderTickMagenta
      c_SliderTickGray = c_SliderTickMagenta
		c_SliderTickMagenta = 0
   CASE c_SliderTickDarkMagenta
      c_SliderTickGray = c_SliderTickDarkMagenta
		c_SliderTickDarkMagenta = 0
   CASE c_SliderTickCyan
      c_SliderTickGray = c_SliderTickCyan
		c_SliderTickCyan = 0
   CASE c_SliderTickDarkCyan
      c_SliderTickGray = c_SliderTickDarkCyan
		c_SliderTickDarkCyan = 0
   CASE c_SliderTickYellow
      c_SliderTickGray = c_SliderTickYellow
		c_SliderTickYellow = 0
   CASE c_SliderTickDarkYellow
      c_SliderTickGray = c_SliderTickDarkYellow
		c_SliderTickDarkYellow = 0
END CHOOSE

end subroutine

public subroutine fw_calheadingdefaults (unsignedlong visualword);//******************************************************************
//  PO Module     : w_POManager_Main
//  Function      : fw_CalHeadingDefaults
//  Description   : Establishes the default characteristics of the
//                  day heading portion of the calendar object.
//
//  Parameters    : UNSIGNEDLONG VisualWord
//
//  Return Value  : (None)
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

ULONG l_BitMask
ULONG c_HeadingColorMask     = 15
ULONG c_HeadingStyleMask     = 112
ULONG c_HeadingTextStyleMask = 384
ULONG c_HeadingAlignMask     = 1536

//------------------------------------------------------------------
//  Sets the default text color for the heading.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_HeadingColorMask)

CHOOSE CASE l_BitMask
   CASE c_CalHeadingBlack
      c_CalHeadingBlack = 0
   CASE c_CalHeadingWhite
      c_CalHeadingBlack = c_CalHeadingWhite
      c_CalHeadingWhite = 0
   CASE c_CalHeadingGray
      c_CalHeadingBlack = c_CalHeadingGray
      c_CalHeadingGray = 0
   CASE c_CalHeadingDarkGray
      c_CalHeadingBlack = c_CalHeadingDarkGray
      c_CalHeadingDarkGray = 0
   CASE c_CalHeadingRed
      c_CalHeadingBlack = c_CalHeadingRed
      c_CalHeadingRed = 0
   CASE c_CalHeadingDarkRed
      c_CalHeadingBlack = c_CalHeadingDarkRed
      c_CalHeadingDarkRed = 0
   CASE c_CalHeadingGreen
      c_CalHeadingBlack = c_CalHeadingGreen
      c_CalHeadingGreen = 0
   CASE c_CalHeadingDarkGreen
      c_CalHeadingBlack = c_CalHeadingDarkGreen
      c_CalHeadingDarkGreen = 0
   CASE c_CalHeadingBlue
      c_CalHeadingBlack = c_CalHeadingBlue
      c_CalHeadingBlue = 0
   CASE c_CalHeadingDarkBlue
      c_CalHeadingBlack = c_CalHeadingDarkBlue
      c_CalHeadingDarkBlue = 0
   CASE c_CalHeadingMagenta
      c_CalHeadingBlack = c_CalHeadingMagenta
      c_CalHeadingMagenta = 0
   CASE c_CalHeadingDarkMagenta
      c_CalHeadingBlack = c_CalHeadingDarkMagenta
      c_CalHeadingDarkMagenta = 0
   CASE c_CalHeadingCyan
      c_CalHeadingBlack = c_CalHeadingCyan
      c_CalHeadingCyan = 0
   CASE c_CalHeadingDarkCyan
      c_CalHeadingBlack = c_CalHeadingDarkCyan
      c_CalHeadingDarkCyan = 0
   CASE c_CalHeadingYellow
      c_CalHeadingBlack = c_CalHeadingYellow
      c_CalHeadingYellow = 0
   CASE c_CalHeadingDarkYellow
      c_CalHeadingBlack = c_CalHeadingDarkYellow
      c_CalHeadingDarkYellow = 0
END CHOOSE

//------------------------------------------------------------------
//  Sets the default style for the heading.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_HeadingStyleMask)

CHOOSE CASE l_BitMask
   CASE c_CalHeadingAuto
      c_CalHeadingAuto = 0
   CASE c_CalHeading1
      c_CalHeadingAuto = c_CalHeading1
      c_CalHeading1 = 0
   CASE c_CalHeading2
      c_CalHeadingAuto = c_CalHeading2
      c_CalHeading2 = 0
   CASE c_CalHeading3
      c_CalHeadingAuto = c_CalHeading3
      c_CalHeading3 = 0
   CASE c_CalHeadingFull
      c_CalHeadingAuto = c_CalHeadingFull
      c_CalHeadingFull = 0
END CHOOSE

//------------------------------------------------------------------
//  Sets the default text style for the heading.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_HeadingTextStyleMask)

CHOOSE CASE l_BitMask
   CASE c_CalHeadingBold
      c_CalHeadingBold = 0
   CASE c_CalHeadingRegular
      c_CalHeadingBold = c_CalHeadingRegular
      c_CalHeadingRegular = 0
   CASE c_CalHeadingItalic
      c_CalHeadingBold = c_CalHeadingItalic
      c_CalHeadingItalic= 0
   CASE c_CalHeadingUnderline
      c_CalHeadingBold = c_CalHeadingUnderLine
      c_CalHeadingUnderLine = 0
END CHOOSE

//------------------------------------------------------------------
//  Sets the default text alignment for the heading.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_HeadingAlignMask)

CHOOSE CASE l_BitMask
   CASE c_CalHeadingCenter
      c_CalHeadingCenter = 0
   CASE c_CalHeadingLeft
      c_CalHeadingCenter = c_CalHeadingLeft
      c_CalHeadingLeft = 0
   CASE c_CalHeadingRight
      c_CalHeadingCenter = c_CalHeadingRight
      c_CalHeadingRight = 0
END CHOOSE


end subroutine

public subroutine fw_caldaydefaults (unsignedlong visualword);//******************************************************************
//  PO Module     : w_POManager_Main
//  Function      : fw_CalDayDefaults
//  Description   : Establishes the default characteristics of the
//                  day portion of the calendar object.
//
//  Parameters    : UNSIGNEDLONG VisualWord
//
//  Return Value  : (None)
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

ULONG l_BitMask
ULONG c_WDTextColorMask = 15
ULONG c_WETextColorMask = 240
ULONG c_DayAlignMask    = 768
ULONG c_WDTextStyleMask = 3072
ULONG c_WETextStyleMask = 12288

//------------------------------------------------------------------
//  Sets the default text color for the weekday day.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_WDTextColorMask)

CHOOSE CASE l_BitMask
   CASE c_CalDayBlack
      c_CalDayBlack = 0
   CASE c_CalDayWhite
      c_CalDayBlack = c_CalDayWhite
      c_CalDayWhite = 0
   CASE c_CalDayGray
      c_CalDayBlack = c_CalDayGray
      c_CalDayGray = 0
   CASE c_CalDayDarkGray
      c_CalDayBlack = c_CalDayDarkGray
      c_CalDayDarkGray = 0
   CASE c_CalDayRed
      c_CalDayBlack = c_CalDayRed
      c_CalDayRed = 0
   CASE c_CalDayDarkRed
      c_CalDayBlack = c_CalDayDarkRed
      c_CalDayDarkRed = 0
   CASE c_CalDayGreen
      c_CalDayBlack = c_CalDayGreen
      c_CalDayGreen = 0
   CASE c_CalDayDarkGreen
      c_CalDayBlack = c_CalDayDarkGreen
      c_CalDayDarkGreen = 0
   CASE c_CalDayBlue
      c_CalDayBlack = c_CalDayBlue
      c_CalDayBlue = 0
   CASE c_CalDayDarkBlue
      c_CalDayBlack = c_CalDayDarkBlue
      c_CalDayDarkBlue = 0
   CASE c_CalDayMagenta
      c_CalDayBlack = c_CalDayMagenta
      c_CalDayMagenta = 0
   CASE c_CalDayDarkMagenta
      c_CalDayBlack = c_CalDayDarkMagenta
      c_CalDayDarkMagenta = 0
   CASE c_CalDayCyan
      c_CalDayBlack = c_CalDayCyan
      c_CalDayCyan = 0
   CASE c_CalDayDarkCyan
      c_CalDayBlack = c_CalDayDarkCyan
      c_CalDayDarkCyan = 0
   CASE c_CalDayYellow
      c_CalDayBlack = c_CalDayYellow
      c_CalDayYellow = 0
   CASE c_CalDayDarkYellow
      c_CalDayBlack = c_CalDayDarkYellow
      c_CalDayDarkYellow = 0
END CHOOSE

//------------------------------------------------------------------
//  Sets the default text color for the weekend day.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_WETextColorMask)

CHOOSE CASE l_BitMask
   CASE c_CalWEDayBlack
      c_CalWEDayBlack = 0
   CASE c_CalWEDayWhite
      c_CalWEDayBlack = c_CalWEDayWhite
      c_CalWEDayWhite = 0
   CASE c_CalWEDayGray
      c_CalWEDayBlack = c_CalWEDayGray
      c_CalWEDayGray = 0
   CASE c_CalWEDayDarkGray
      c_CalWEDayBlack = c_CalWEDayDarkGray
      c_CalWEDayDarkGray = 0
   CASE c_CalWEDayRed
      c_CalWEDayBlack = c_CalWEDayRed
      c_CalWEDayRed = 0
   CASE c_CalWEDayDarkRed
      c_CalWEDayBlack = c_CalWEDayDarkRed
      c_CalWEDayDarkRed = 0
   CASE c_CalWEDayGreen
      c_CalWEDayBlack = c_CalWEDayGreen
      c_CalWEDayGreen = 0
   CASE c_CalWEDayDarkGreen
      c_CalWEDayBlack = c_CalWEDayDarkGreen
      c_CalWEDayDarkGreen = 0
   CASE c_CalWEDayBlue
      c_CalWEDayBlack = c_CalWEDayBlue
      c_CalWEDayBlue = 0
   CASE c_CalWEDayDarkBlue
      c_CalWEDayBlack = c_CalWEDayDarkBlue
      c_CalWEDayDarkBlue = 0
   CASE c_CalWEDayMagenta
      c_CalWEDayBlack = c_CalWEDayMagenta
      c_CalWEDayMagenta = 0
   CASE c_CalWEDayDarkMagenta
      c_CalWEDayBlack = c_CalWEDayDarkMagenta
      c_CalWEDayDarkMagenta = 0
   CASE c_CalWEDayCyan
      c_CalWEDayBlack = c_CalWEDayCyan
      c_CalWEDayCyan = 0
   CASE c_CalWEDayDarkCyan
      c_CalWEDayBlack = c_CalWEDayDarkCyan
      c_CalWEDayDarkCyan = 0
   CASE c_CalWEDayYellow
      c_CalWEDayBlack = c_CalWEDayYellow
      c_CalWEDayYellow = 0
   CASE c_CalWEDayDarkYellow
      c_CalWEDayBlack = c_CalWEDayDarkYellow
      c_CalWEDayDarkYellow = 0
END CHOOSE

//------------------------------------------------------------------
//  Sets the default text style for the weekday day.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_WDTextStyleMask)

CHOOSE CASE l_BitMask
   CASE c_CalDayRegular
      c_CalDayRegular = 0
   CASE c_CalDayBold
      c_CalDayRegular = c_CalDayBold
      c_CalDayBold = 0
   CASE c_CalDayItalic
      c_CalDayRegular = c_CalDayItalic
      c_CalDayItalic= 0
   CASE c_CalDayUnderline
      c_CalDayRegular = c_CalDayUnderLine
      c_CalDayUnderLine = 0
END CHOOSE

//------------------------------------------------------------------
//  Sets the default text style for the weekend day.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_WETextStyleMask)

CHOOSE CASE l_BitMask
   CASE c_CalWEDayRegular
      c_CalWEDayRegular = 0
   CASE c_CalWEDayBold
      c_CalWEDayRegular = c_CalWEDayBold
      c_CalWEDayBold = 0
   CASE c_CalWEDayItalic
      c_CalWEDayRegular = c_CalWEDayItalic
      c_CalWEDayItalic= 0
   CASE c_CalWEDayUnderline
      c_CalWEDayRegular = c_CalWEDayUnderLine
      c_CalWEDayUnderLine = 0
END CHOOSE

//------------------------------------------------------------------
//  Sets the default text alignment for the day.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_DayAlignMask)

CHOOSE CASE l_BitMask
   CASE c_CalDayRight
      c_CalDayRight = 0
   CASE c_CalDayLeft
      c_CalDayRight = c_CalDayLeft
      c_CalDayLeft = 0
   CASE c_CalDayRight
      c_CalDayRight = c_CalDayCenter
      c_CalDayCenter = 0
END CHOOSE


end subroutine

public subroutine fw_calselectdefaults (unsignedlong visualword);//******************************************************************
//  PO Module     : w_POManager_Main
//  Function      : fw_CalSelectDefaults
//  Description   : Establishes the default characteristics of the
//                  selected day portion of the calendar object.
//
//  Parameters    : UNSIGNEDLONG VisualWord
//
//  Return Value  : (None)
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

ULONG l_BitMask
ULONG c_SelectColorMask   = 15
ULONG c_SelectBGColorMask = 240
ULONG c_SelectStyleMask   = 768

//------------------------------------------------------------------
//  Sets the default text color for the selected day.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_SelectColorMask)

CHOOSE CASE l_BitMask
   CASE c_CalSelectWhite
      c_CalSelectWhite = 0
   CASE c_CalSelectBlack
      c_CalSelectWhite = c_CalSelectBlack
      c_CalSelectBlack = 0
   CASE c_CalSelectGray
      c_CalSelectWhite = c_CalSelectGray
      c_CalSelectGray = 0
   CASE c_CalSelectDarkGray
      c_CalSelectWhite = c_CalSelectDarkGray
      c_CalSelectDarkGray = 0
   CASE c_CalSelectRed
      c_CalSelectWhite = c_CalSelectRed
      c_CalSelectRed = 0
   CASE c_CalSelectDarkRed
      c_CalSelectWhite = c_CalSelectDarkRed
      c_CalSelectDarkRed = 0
   CASE c_CalSelectGreen
      c_CalSelectWhite = c_CalSelectGreen
      c_CalSelectGreen = 0
   CASE c_CalSelectDarkGreen
      c_CalSelectWhite = c_CalSelectDarkGreen
      c_CalSelectDarkGreen = 0
   CASE c_CalSelectBlue
      c_CalSelectWhite = c_CalSelectBlue
      c_CalSelectBlue = 0
   CASE c_CalSelectDarkBlue
      c_CalSelectWhite = c_CalSelectDarkBlue
      c_CalSelectDarkBlue = 0
   CASE c_CalSelectMagenta
      c_CalSelectWhite = c_CalSelectMagenta
      c_CalSelectMagenta = 0
   CASE c_CalSelectDarkMagenta
      c_CalSelectWhite = c_CalSelectDarkMagenta
      c_CalSelectDarkMagenta = 0
   CASE c_CalSelectCyan
      c_CalSelectWhite = c_CalSelectCyan
      c_CalSelectCyan = 0
   CASE c_CalSelectDarkCyan
      c_CalSelectWhite = c_CalSelectDarkCyan
      c_CalSelectDarkCyan = 0
   CASE c_CalSelectYellow
      c_CalSelectWhite = c_CalSelectYellow
      c_CalSelectYellow = 0
   CASE c_CalSelectDarkYellow
      c_CalSelectWhite = c_CalSelectDarkYellow
      c_CalSelectDarkYellow = 0
END CHOOSE

//------------------------------------------------------------------
//  Sets the default background color for the selected day.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_SelectBGColorMask)

CHOOSE CASE l_BitMask
   CASE c_CalSelectBGDarkGray
      c_CalSelectBGDarkGray = 0
   CASE c_CalSelectBGWhite
      c_CalSelectBGDarkGray = c_CalSelectBGWhite
      c_CalSelectBGWhite = 0
   CASE c_CalSelectBGGray
      c_CalSelectBGDarkGray = c_CalSelectBGGray
      c_CalSelectBGGray = 0
   CASE c_CalSelectBGBlack
      c_CalSelectBGDarkGray = c_CalSelectBGBlack
      c_CalSelectBGBlack = 0
   CASE c_CalSelectBGRed
      c_CalSelectBGDarkGray = c_CalSelectBGRed
      c_CalSelectBGRed = 0
   CASE c_CalSelectBGDarkRed
      c_CalSelectBGDarkGray = c_CalSelectBGDarkRed
      c_CalSelectBGDarkRed = 0
   CASE c_CalSelectBGGreen
      c_CalSelectBGDarkGray = c_CalSelectBGGreen
      c_CalSelectBGGreen = 0
   CASE c_CalSelectBGDarkGreen
      c_CalSelectBGDarkGray = c_CalSelectBGDarkGreen
      c_CalSelectBGDarkGreen = 0
   CASE c_CalSelectBGBlue
      c_CalSelectBGDarkGray = c_CalSelectBGBlue
      c_CalSelectBGBlue = 0
   CASE c_CalSelectBGDarkBlue
      c_CalSelectBGDarkGray = c_CalSelectBGDarkBlue
      c_CalSelectBGDarkBlue = 0
   CASE c_CalSelectBGMagenta
      c_CalSelectBGDarkGray = c_CalSelectBGMagenta
      c_CalSelectBGMagenta = 0
   CASE c_CalSelectBGDarkMagenta
      c_CalSelectBGDarkGray = c_CalSelectBGDarkMagenta
      c_CalSelectBGDarkMagenta = 0
   CASE c_CalSelectBGCyan
      c_CalSelectBGDarkGray = c_CalSelectBGCyan
      c_CalSelectBGCyan = 0
   CASE c_CalSelectBGDarkCyan
      c_CalSelectBGDarkGray = c_CalSelectBGDarkCyan
      c_CalSelectBGDarkCyan = 0
   CASE c_CalSelectBGYellow
      c_CalSelectBGDarkGray = c_CalSelectBGYellow
      c_CalSelectBGYellow = 0
   CASE c_CalSelectBGDarkYellow
      c_CalSelectBGDarkGray = c_CalSelectBGDarkYellow
      c_CalSelectBGDarkYellow = 0
END CHOOSE

//------------------------------------------------------------------
//  Sets the default text style for the selected day.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_SelectStyleMask)

CHOOSE CASE l_BitMask
   CASE c_CalSelectRegular
      c_CalSelectRegular = 0
   CASE c_CalSelectBold
      c_CalSelectRegular = c_CalSelectBold
      c_CalSelectBold = 0
   CASE c_CalSelectItalic
      c_CalSelectRegular = c_CalSelectItalic
      c_CalSelectItalic= 0
   CASE c_CalSelectUnderline
      c_CalSelectRegular = c_CalSelectUnderLine
      c_CalSelectUnderLine = 0
END CHOOSE


end subroutine

public subroutine fw_caldisabledefaults (unsignedlong visualword);//******************************************************************
//  PO Module     : w_POManager_Main
//  Function      : fw_CalDisableDefaults
//  Description   : Establishes the default characteristics of the
//                  disabled day portion of the calendar object.
//
//  Parameters    : UNSIGNEDLONG VisualWord
//
//  Return Value  : (None)
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

ULONG l_BitMask
ULONG c_DisableColorMask   = 15
ULONG c_DisableBGColorMask = 240
ULONG c_DisableStyleMask   = 768

//------------------------------------------------------------------
//  Sets the default text color for the disabled day.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_DisableColorMask)

CHOOSE CASE l_BitMask
   CASE c_CalDisableDarkGray
      c_CalDisableDarkGray = 0
   CASE c_CalDisableBlack
      c_CalDisableDarkGray = c_CalDisableBlack
      c_CalDisableBlack = 0
   CASE c_CalDisableGray
      c_CalDisableDarkGray = c_CalDisableGray
      c_CalDisableGray = 0
   CASE c_CalDisableWhite
      c_CalDisableDarkGray = c_CalDisableWhite
      c_CalDisableWhite = 0
   CASE c_CalDisableRed
      c_CalDisableDarkGray = c_CalDisableRed
      c_CalDisableRed = 0
   CASE c_CalDisableDarkRed
      c_CalDisableDarkGray = c_CalDisableDarkRed
      c_CalDisableDarkRed = 0
   CASE c_CalDisableGreen
      c_CalDisableDarkGray = c_CalDisableGreen
      c_CalDisableGreen = 0
   CASE c_CalDisableDarkGreen
      c_CalDisableDarkGray = c_CalDisableDarkGreen
      c_CalDisableDarkGreen = 0
   CASE c_CalDisableBlue
      c_CalDisableDarkGray = c_CalDisableBlue
      c_CalDisableBlue = 0
   CASE c_CalDisableDarkBlue
      c_CalDisableDarkGray = c_CalDisableDarkBlue
      c_CalDisableDarkBlue = 0
   CASE c_CalDisableMagenta
      c_CalDisableDarkGray = c_CalDisableMagenta
      c_CalDisableMagenta = 0
   CASE c_CalDisableDarkMagenta
      c_CalDisableDarkGray = c_CalDisableDarkMagenta
      c_CalDisableDarkMagenta = 0
   CASE c_CalDisableCyan
      c_CalDisableDarkGray = c_CalDisableCyan
      c_CalDisableCyan = 0
   CASE c_CalDisableDarkCyan
      c_CalDisableDarkGray = c_CalDisableDarkCyan
      c_CalDisableDarkCyan = 0
   CASE c_CalDisableYellow
      c_CalDisableDarkGray = c_CalDisableYellow
      c_CalDisableYellow = 0
   CASE c_CalDisableDarkYellow
      c_CalDisableDarkGray = c_CalDisableDarkYellow
      c_CalDisableDarkYellow = 0
END CHOOSE

//------------------------------------------------------------------
//  Sets the default background color for the disabled day.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_DisableBGColorMask)

CHOOSE CASE l_BitMask
   CASE c_CalDisableBGGray
      c_CalDisableBGGray = 0
   CASE c_CalDisableBGWhite
      c_CalDisableBGGray = c_CalDisableBGWhite
      c_CalDisableBGWhite = 0
   CASE c_CalDisableBGDarkGray
      c_CalDisableBGGray = c_CalDisableBGDarkGray
      c_CalDisableBGDarkGray = 0
   CASE c_CalDisableBGBlack
      c_CalDisableBGGray = c_CalDisableBGBlack
      c_CalDisableBGBlack = 0
   CASE c_CalDisableBGRed
      c_CalDisableBGGray = c_CalDisableBGRed
      c_CalDisableBGRed = 0
   CASE c_CalDisableBGDarkRed
      c_CalDisableBGGray = c_CalDisableBGDarkRed
      c_CalDisableBGDarkRed = 0
   CASE c_CalDisableBGGreen
      c_CalDisableBGGray = c_CalDisableBGGreen
      c_CalDisableBGGreen = 0
   CASE c_CalDisableBGDarkGreen
      c_CalDisableBGGray = c_CalDisableBGDarkGreen
      c_CalDisableBGDarkGreen = 0
   CASE c_CalDisableBGBlue
      c_CalDisableBGGray = c_CalDisableBGBlue
      c_CalDisableBGBlue = 0
   CASE c_CalDisableBGDarkBlue
      c_CalDisableBGGray = c_CalDisableBGDarkBlue
      c_CalDisableBGDarkBlue = 0
   CASE c_CalDisableBGMagenta
      c_CalDisableBGGray = c_CalDisableBGMagenta
      c_CalDisableBGMagenta = 0
   CASE c_CalDisableBGDarkMagenta
      c_CalDisableBGGray = c_CalDisableBGDarkMagenta
      c_CalDisableBGDarkMagenta = 0
   CASE c_CalDisableBGCyan
      c_CalDisableBGGray = c_CalDisableBGCyan
      c_CalDisableBGCyan = 0
   CASE c_CalDisableBGDarkCyan
      c_CalDisableBGGray = c_CalDisableBGDarkCyan
      c_CalDisableBGDarkCyan = 0
   CASE c_CalDisableBGYellow
      c_CalDisableBGGray = c_CalDisableBGYellow
      c_CalDisableBGYellow = 0
   CASE c_CalDisableBGDarkYellow
      c_CalDisableBGGray = c_CalDisableBGDarkYellow
      c_CalDisableBGDarkYellow = 0
END CHOOSE

//------------------------------------------------------------------
//  Sets the default text style for the disabled day.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_DisableStyleMask)

CHOOSE CASE l_BitMask
   CASE c_CalDisableItalic
      c_CalDisableItalic = 0
   CASE c_CalDisableBold
      c_CalDisableItalic = c_CalDisableBold
      c_CalDisableBold = 0
   CASE c_CalDisableRegular
      c_CalDisableItalic = c_CalDisableRegular
      c_CalDisableRegular = 0
   CASE c_CalDisableUnderline
      c_CalDisableItalic = c_CalDisableUnderLine
      c_CalDisableUnderLine = 0
END CHOOSE


end subroutine

public subroutine fw_calmonthdefaults (unsignedlong visualword);//******************************************************************
//  PO Module     : w_POManager_Main
//  Function      : fw_CalMonthDefaults
//  Description   : Establishes the default characteristics of the
//                  month portion of the calendar object.
//
//  Parameters    : UNSIGNEDLONG VisualWord
//
//  Return Value  : (None)
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

ULONG l_BitMask
ULONG c_MonthColorMask     = 15
ULONG c_MonthStyleMask     = 48

//------------------------------------------------------------------
//  Sets the default text color for the month.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_MonthColorMask)

CHOOSE CASE l_BitMask
   CASE c_CalMonthBlack
      c_CalMonthBlack = 0
   CASE c_CalMonthWhite
      c_CalMonthBlack = c_CalMonthWhite
      c_CalMonthWhite = 0
   CASE c_CalMonthGray
      c_CalMonthBlack = c_CalMonthGray
      c_CalMonthGray = 0
   CASE c_CalMonthDarkGray
      c_CalMonthBlack = c_CalMonthDarkGray
      c_CalMonthDarkGray = 0
   CASE c_CalMonthRed
      c_CalMonthBlack = c_CalMonthRed
      c_CalMonthRed = 0
   CASE c_CalMonthDarkRed
      c_CalMonthBlack = c_CalMonthDarkRed
      c_CalMonthDarkRed = 0
   CASE c_CalMonthGreen
      c_CalMonthBlack = c_CalMonthGreen
      c_CalMonthGreen = 0
   CASE c_CalMonthDarkGreen
      c_CalMonthBlack = c_CalMonthDarkGreen
      c_CalMonthDarkGreen = 0
   CASE c_CalMonthBlue
      c_CalMonthBlack = c_CalMonthBlue
      c_CalMonthBlue = 0
   CASE c_CalMonthDarkBlue
      c_CalMonthBlack = c_CalMonthDarkBlue
      c_CalMonthDarkBlue = 0
   CASE c_CalMonthMagenta
      c_CalMonthBlack = c_CalMonthMagenta
      c_CalMonthMagenta = 0
   CASE c_CalMonthDarkMagenta
      c_CalMonthBlack = c_CalMonthDarkMagenta
      c_CalMonthDarkMagenta = 0
   CASE c_CalMonthCyan
      c_CalMonthBlack = c_CalMonthCyan
      c_CalMonthCyan = 0
   CASE c_CalMonthDarkCyan
      c_CalMonthBlack = c_CalMonthDarkCyan
      c_CalMonthDarkCyan = 0
   CASE c_CalMonthYellow
      c_CalMonthBlack = c_CalMonthYellow
      c_CalMonthYellow = 0
   CASE c_CalMonthDarkYellow
      c_CalMonthBlack = c_CalMonthDarkYellow
      c_CalMonthDarkYellow = 0
END CHOOSE

//------------------------------------------------------------------
//  Sets the default text style for the month.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_MonthStyleMask)

CHOOSE CASE l_BitMask
   CASE c_CalMonthBold
      c_CalMonthBold = 0
   CASE c_CalMonthRegular
      c_CalMonthBold = c_CalMonthRegular
      c_CalMonthRegular = 0
   CASE c_CalMonthItalic
      c_CalMonthBold = c_CalMonthItalic
      c_CalMonthItalic= 0
   CASE c_CalMonthUnderline
      c_CalMonthBold = c_CalMonthUnderLine
      c_CalMonthUnderLine = 0
END CHOOSE


end subroutine

public subroutine fw_calyeardefaults (unsignedlong visualword);//******************************************************************
//  PO Module     : w_POManager_Main
//  Function      : fw_CalYearDefaults
//  Description   : Establishes the default characteristics of the
//                  year portion of the calendar object.
//
//  Parameters    : UNSIGNEDLONG VisualWord
//
//  Return Value  : (None)
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

ULONG l_BitMask
ULONG c_YearColorMask     = 15
ULONG c_YearStyleMask     = 48
ULONG c_ShowYearMask      = 64

//------------------------------------------------------------------
//  Sets the default text color for the year.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_YearColorMask)

CHOOSE CASE l_BitMask
   CASE c_CalYearBlack
      c_CalYearBlack = 0
   CASE c_CalYearWhite
      c_CalYearBlack = c_CalYearWhite
      c_CalYearWhite = 0
   CASE c_CalYearGray
      c_CalYearBlack = c_CalYearGray
      c_CalYearGray = 0
   CASE c_CalYearDarkGray
      c_CalYearBlack = c_CalYearDarkGray
      c_CalYearDarkGray = 0
   CASE c_CalYearRed
      c_CalYearBlack = c_CalYearRed
      c_CalYearRed = 0
   CASE c_CalYearDarkRed
      c_CalYearBlack = c_CalYearDarkRed
      c_CalYearDarkRed = 0
   CASE c_CalYearGreen
      c_CalYearBlack = c_CalYearGreen
      c_CalYearGreen = 0
   CASE c_CalYearDarkGreen
      c_CalYearBlack = c_CalYearDarkGreen
      c_CalYearDarkGreen = 0
   CASE c_CalYearBlue
      c_CalYearBlack = c_CalYearBlue
      c_CalYearBlue = 0
   CASE c_CalYearDarkBlue
      c_CalYearBlack = c_CalYearDarkBlue
      c_CalYearDarkBlue = 0
   CASE c_CalYearMagenta
      c_CalYearBlack = c_CalYearMagenta
      c_CalYearMagenta = 0
   CASE c_CalYearDarkMagenta
      c_CalYearBlack = c_CalYearDarkMagenta
      c_CalYearDarkMagenta = 0
   CASE c_CalYearCyan
      c_CalYearBlack = c_CalYearCyan
      c_CalYearCyan = 0
   CASE c_CalYearDarkCyan
      c_CalYearBlack = c_CalYearDarkCyan
      c_CalYearDarkCyan = 0
   CASE c_CalYearYellow
      c_CalYearBlack = c_CalYearYellow
      c_CalYearYellow = 0
   CASE c_CalYearDarkYellow
      c_CalYearBlack = c_CalYearDarkYellow
      c_CalYearDarkYellow = 0
END CHOOSE

//------------------------------------------------------------------
//  Sets the default text style for the year.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_YearStyleMask)

CHOOSE CASE l_BitMask
   CASE c_CalYearBold
      c_CalYearBold = 0
   CASE c_CalYearRegular
      c_CalYearBold = c_CalYearRegular
      c_CalYearRegular = 0
   CASE c_CalYearItalic
      c_CalYearBold = c_CalYearItalic
      c_CalYearItalic= 0
   CASE c_CalYearUnderline
      c_CalYearBold = c_CalYearUnderLine
      c_CalYearUnderLine = 0
END CHOOSE

//------------------------------------------------------------------
//  Sets the default for showing the year on the calendar.
//------------------------------------------------------------------

l_BitMask = fw_BitMask(VisualWord, c_ShowYearMask)

CHOOSE CASE l_BitMask
   CASE c_CalYearShow
      c_CalYearShow = 0
   CASE c_CalYearHide
      c_CalYearShow = c_CalYearHide
      c_CalYearHide = 0
END CHOOSE


end subroutine

on open;//******************************************************************
//  PO Module     : w_POManager_Main
//  Event         : Open
//  Description   : Initializes the PowerObjects environment.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

//------------------------------------------------------------------
//  Create the message object.
//------------------------------------------------------------------

MB = CREATE n_PO_MB_Std

//------------------------------------------------------------------
//  Get the PowerBuilder environment.
//------------------------------------------------------------------

GetEnvironment(ENV)

CHOOSE CASE ENV.OSType
   CASE Windows!
      IF ENV.OSMajorRevision = 4 THEN
         EXT = CREATE N_EXT_NT
      ELSE
         EXT = CREATE N_EXT_WIN
      END IF

   CASE WindowsNT!
      EXT = CREATE N_EXT_NT

   CASE ELSE
      EXT = CREATE N_EXT_OTHER

END CHOOSE
end on

on close;//******************************************************************
//  PO Module     : w_POManager_Main
//  Event         : Close
//  Description   : Terminates the PowerObjects environment.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

DESTROY MB
DESTROY EXT
end on

on w_pomanager_main.create
this.st_1=create st_1
this.Control[]={ this.st_1}
end on

on w_pomanager_main.destroy
destroy(this.st_1)
end on

type st_1 from statictext within w_pomanager_main
int X=23
int Y=17
int Width=673
int Height=245
boolean Enabled=false
boolean Border=true
BorderStyle BorderStyle=StyleRaised!
string Text="PowerObject Manager"
Alignment Alignment=Center!
boolean FocusRectangle=false
long BackColor=12632256
int TextSize=-16
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

