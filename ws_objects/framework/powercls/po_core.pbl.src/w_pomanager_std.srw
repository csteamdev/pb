﻿$PBExportHeader$w_pomanager_std.srw
$PBExportComments$Standards descendant manager for setting default options
forward
global type w_pomanager_std from w_pomanager_main
end type
end forward

global type w_pomanager_std from w_pomanager_main
end type
global w_pomanager_std w_pomanager_std

on open;call w_pomanager_main::open;////******************************************************************
////                  This is a Developer coded event
////******************************************************************
////
////  PO Module     : w_POManager_Std
////  Event         : Open
////  Description   : Provides an opportunity for the developer to
////                  change defaults for objects in PowerObjects.
////
////  Change History: 
////
////  Date     Person     Description of Change
////  -------- ---------- --------------------------------------------
////
////******************************************************************
////  Copyright ServerLogic 1994-1995.  All Rights Reserved.
////******************************************************************
//
////------------------------------------------------------------------
////  Sample code for utility window defaults.    (Application Module)
////------------------------------------------------------------------
//
//fw_WindowDefaults(c_DefaultFont, c_DefaultSize, c_DefaultVisual)
//
////------------------------------------------------------------------
////  Sample code for folder defaults.        (Data Navigation Module)
////------------------------------------------------------------------
//
//fw_FolderDefaults(c_DefaultVisual)
//
//fw_TabDefaults(c_DefaultVisual)
//fw_TabCurrentDefaults(c_DefaultVisual)
//fw_TabDisableDefaults(c_DefaultVisual)
//
////------------------------------------------------------------------
////  Sample code for outliner defaults.      (Data Navigation Module)
////------------------------------------------------------------------
//
//fw_HLDefaults(c_DefaultVisual)
//fw_HLTextDefaults(c_DefaultVisual)
//
////------------------------------------------------------------------
////  Sample code for progress bar defaults.  (Data Management Module)
////------------------------------------------------------------------
//
//fw_ProgressDefaults(c_DefaultVisual)
//
////------------------------------------------------------------------
////  Sample code for slider defaults.        (Data Management Module)
////------------------------------------------------------------------
//
//fw_SliderDefaults(c_DefaultVisual)
//
////------------------------------------------------------------------
////  Sample code for calendar defaults.      (Data Management Module)
////------------------------------------------------------------------
//
//fw_CalDefaults(c_DefaultVisual)
//
//fw_CalHeadingDefaults(c_DefaultVisual)
//fw_CalDayDefaults(c_DefaultVisual)
//fw_CalMonthDefaults(c_DefaultVisual)
//fw_CalYearDefaults(c_DefaultVisual)
//
//fw_CalSelectDefaults(c_DefaultVisual)
//fw_CalDisableDefaults(c_DefaultVisual)
end on

on w_pomanager_std.create
call w_pomanager_main::create
end on

on w_pomanager_std.destroy
call w_pomanager_main::destroy
end on

