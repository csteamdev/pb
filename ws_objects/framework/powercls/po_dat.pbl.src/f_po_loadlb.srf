﻿$PBExportHeader$f_po_loadlb.srf
$PBExportComments$Function for loading a standalone list box code table
global type f_po_loadlb from function_object
end type

forward prototypes
global function integer f_po_loadlb (listbox lb_name, transaction trans_object, string table_name, string column_code, string column_desc, string where_clause, string all_keyword)
end prototypes

global function integer f_po_loadlb (listbox lb_name, transaction trans_object, string table_name, string column_code, string column_desc, string where_clause, string all_keyword);//******************************************************************
//  PO Module     : f_PO_LoadLB
//  Function      : f_PO_LoadLB
//  Description   : Load a list box using codes and descriptions
//                  from the database.
//
//  Parameters    : LB     LB_Name     - Name of the LB to load.
//                  TRANSACTION Trans_Object - Transaction object.
//                  STRING Table_Name  - Table from where the column
//                                       with the code values
//                                       resides.
//                  STRING Column_Code - Column name with the code
//                                       values.
//                  STRING Column_Desc - Column name with the code
//                                       descriptions.
//                  STRING Where_Clause- WHERE clause statement to 
//                                       restrict the code values.
//                  STRING All_Keyword - Keyword to denote select
//                                       all values (e.g. "(All)").
//
//  Return Value  : 0 = OK, -1 = Error
//
//  Change History:  
//    
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN l_IsNumber
LONG    l_Nbr
STRING  l_SQLString, l_Code, l_Desc, l_ErrorStrings[1]

//------------------------------------------------------------------
//  Declare a dynamic SQL cursor to hold the data while loading the 
//  list box.
//------------------------------------------------------------------

trans_object.SQLCode = 0
DECLARE LB_Cursor DYNAMIC CURSOR FOR SQLSA ;

IF trans_object.SQLCode <> 0 THEN
   l_ErrorStrings[1] = trans_object.SQLErrText
   w_POManager_Std.MB.fu_MessageBox( &
                    w_POManager_Std.MB.c_MBI_CursorDeclareError, &
                    0, w_POManager_Std.MB.i_MB_Numbers[], &
                    1, l_ErrorStrings[])
   RETURN -1
END IF

//------------------------------------------------------------------
//  Build the SQL SELECT statement to get the data.
//------------------------------------------------------------------

l_SQLString = "SELECT " + Column_Code + ", " + &
              Column_Desc + " FROM " + Table_Name
IF Trim(Where_Clause) <> "" THEN
   l_SQLString = l_SQLString + " WHERE " + Where_Clause
END IF

//------------------------------------------------------------------
//  Prepare the SQL statement for execution.
//------------------------------------------------------------------

PREPARE SQLSA FROM :l_SQLString USING trans_object;

IF trans_object.SQLCode <> 0 THEN
   l_ErrorStrings[1] = trans_object.SQLErrText
   w_POManager_Std.MB.fu_MessageBox( &
                    w_POManager_Std.MB.c_MBI_CursorPrepareError, &
                    0, w_POManager_Std.MB.i_MB_Numbers[], &
                    1, l_ErrorStrings[])
   RETURN -1
END IF

//------------------------------------------------------------------
//  Open the dynamic cursor.
//------------------------------------------------------------------

OPEN DYNAMIC LB_Cursor ;

IF trans_object.SQLCode <> 0 THEN
   l_ErrorStrings[1] = trans_object.SQLErrText
   w_POManager_Std.MB.fu_MessageBox( &
                    w_POManager_Std.MB.c_MBI_CursorOpenError, &
                    0, w_POManager_Std.MB.i_MB_Numbers[], &
                    1, l_ErrorStrings[])
   RETURN -1
END IF

//------------------------------------------------------------------
//  If a keyword has been given to select all, add it as a selection
//  to the list box.
//------------------------------------------------------------------

IF All_Keyword <> "" THEN
   l_Code = "(ALL)"
   l_Desc = All_Keyword
   l_Desc = String(l_Desc, Fill("@",80)) + "||" + l_Code
   LB_Name.AddItem(l_Desc)
END IF

//------------------------------------------------------------------
//  Determine if the code column is a number or a string.
//------------------------------------------------------------------

FETCH LB_Cursor INTO :l_Code, :l_Desc ;

IF trans_object.SQLCode = 0 OR trans_object.SQLCode = 100 THEN
   l_IsNumber = FALSE
ELSE
   l_IsNumber = TRUE
END IF

CLOSE LB_Cursor ;
OPEN DYNAMIC LB_Cursor ;

//------------------------------------------------------------------
//  For each row in the cursor, add the description and code to the
//  list box.
//------------------------------------------------------------------

IF l_IsNumber THEN
   DO WHILE trans_object.SQLCode = 0
      FETCH LB_Cursor INTO :l_Nbr, :l_Desc ;
      IF trans_object.SQLCode = 0 THEN
         IF IsNull(l_Desc) THEN
  		      l_Nbr = -99999
  	      END IF
         IF Column_Code <> Column_Desc THEN
            l_Desc = String(l_Desc, Fill("@",80)) + &
                     "||" + String(l_Nbr)
         END IF
         LB_Name.AddItem(l_Desc)
      ELSE
         IF trans_object.SQLCode <> 100 THEN
            l_ErrorStrings[1] = trans_object.SQLErrText
            w_POManager_Std.MB.fu_MessageBox( &
                    w_POManager_Std.MB.c_MBI_CursorFetchError, &
                    0, w_POManager_Std.MB.i_MB_Numbers[], &
                    1, l_ErrorStrings[])
            RETURN -1
         END IF
      END IF
   LOOP
ELSE
   DO WHILE trans_object.SQLCode = 0
      FETCH LB_Cursor INTO :l_Code, :l_Desc ;
      IF trans_object.SQLCode = 0 THEN
         IF IsNull(l_Desc) THEN
  		      l_Code = "NULL"
  	      END IF
         IF Column_Code <> Column_Desc THEN
	         l_Desc = String(l_Desc, Fill("@",80)) + &
                     "||" + l_Code
         END IF
			IF l_Code <> "NULL" THEN
	         LB_Name.AddItem(l_Desc)
			End If
      ELSE
         IF trans_object.SQLCode <> 100 THEN
            l_ErrorStrings[1] = trans_object.SQLErrText
            w_POManager_Std.MB.fu_MessageBox( &
                    w_POManager_Std.MB.c_MBI_CursorFetchError, &
                    0, w_POManager_Std.MB.i_MB_Numbers[], &
                    1, l_ErrorStrings[])
            RETURN -1
         END IF
      END IF
   LOOP
END IF

//------------------------------------------------------------------
//  Close the dynamic cursor.
//------------------------------------------------------------------

CLOSE LB_Cursor ;

IF trans_object.SQLCode <> 0 THEN
   l_ErrorStrings[1] = trans_object.SQLErrText
   w_POManager_Std.MB.fu_MessageBox( &
                    w_POManager_Std.MB.c_MBI_CursorCloseError, &
                    0, w_POManager_Std.MB.i_MB_Numbers[], &
                    1, l_ErrorStrings[])
   RETURN -1
END IF

//------------------------------------------------------------------
//  Do any cleanup and exit the function.
//------------------------------------------------------------------

RETURN 0
end function

