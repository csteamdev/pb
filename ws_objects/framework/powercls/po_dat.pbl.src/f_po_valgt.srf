﻿$PBExportHeader$f_po_valgt.srf
$PBExportComments$Function to validate if a number is greater than another number
global type f_po_valgt from function_object
end type

forward prototypes
global function integer f_po_valgt (string value, real value_gt, boolean display_error)
end prototypes

global function integer f_po_valgt (string value, real value_gt, boolean display_error);//**********************************************************************
//  PO Module     : f_PO_ValGT
//  Function      : f_PO_ValGT
//  Description   : Check the value to see if it greater than
//                  another number.
//
//  Parameters    : STRING Value    - String containing a number.
//                  REAL   Value_GT - Number used for the
//                                    comparison.
//						  BOOLEAN display_error - Boolean controlling 
//							       whether an error message displays. 	
//
//  Return Value  : 0 = OK, 1 = Error
//
//  Change History:  
//    
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Error

//------------------------------------------------------------------
//  Assume an error.
//------------------------------------------------------------------

l_Error = 1
Value   = Trim(Value)

//------------------------------------------------------------------
//  Check if the value is a valid number.  If so, check to see if
//  the value is greater than the second number.
//------------------------------------------------------------------

l_Error = f_PO_ValDec(Value, "", display_error)
IF l_Error = 0 THEN
   IF Real(Value) > Value_GT THEN
      l_Error = 0
   ELSE
      l_Error = 1
   END IF

   IF l_Error = 1 THEN
      IF display_error THEN
         w_POManager_Std.MB.i_MB_Numbers[1] = Value_GT
         w_POManager_Std.MB.i_MB_Strings[1] = "::f_PO_ValGT()"
         w_POManager_Std.MB.i_MB_Strings[2] = w_POManager_Std.i_ApplicationName
         w_POManager_Std.MB.i_MB_Strings[3] = Value
         w_POManager_Std.MB.fu_MessageBox          &
            (w_POManager_Std.MB.c_MBI_GTValError,  &
             1, w_POManager_Std.MB.i_MB_Numbers[], &
             3, w_POManager_Std.MB.i_MB_Strings[])
      END IF
   END IF
END IF

RETURN l_Error
end function

