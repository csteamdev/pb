﻿$PBExportHeader$f_po_valminlen.srf
$PBExportComments$Function to validate if a string is no less than a given value
global type f_po_valminlen from function_object
end type

forward prototypes
global function integer f_po_valminlen (string value, long min_len, boolean display_error)
end prototypes

global function integer f_po_valminlen (string value, long min_len, boolean display_error);//******************************************************************
//  PO Module     : f_PO_ValMinLen
//  Function      : f_PO_ValMinLen
//  Description   : Make sure that a value is at least Min_Len
//                  characters long.
//
//  Parameters    : STRING Value   - String to be tested.
//                  LONG   Min_Len - The minimum length for the
//                                   string.
//						  BOOLEAN display_error - Boolean controlling 
//							       whether an error message displays. 	
//
//  Return Value  : 0 = OK, 1 = Error
//
//  Change History:  
//    
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Error

//------------------------------------------------------------------
//  Assume an error.
//------------------------------------------------------------------

l_Error = 1
Value   = Trim(Value)

IF Len(Value) >= Min_Len THEN
   l_Error = 0
END IF

IF l_Error = 1 THEN
   IF display_error THEN
      w_POManager_Std.MB.i_MB_Numbers[1] = Min_len
      w_POManager_Std.MB.i_MB_Strings[1] = "::f_PO_ValMinLen()"
      w_POManager_Std.MB.i_MB_Strings[2] = w_POManager_Std.i_ApplicationName
      w_POManager_Std.MB.i_MB_Strings[3] = Value
      w_POManager_Std.MB.fu_MessageBox             &
         (w_POManager_Std.MB.c_MBI_MinLenValError, &
          1, w_POManager_Std.MB.i_MB_Numbers[],    &
          3, w_POManager_Std.MB.i_MB_Strings[])
   END IF
END IF

RETURN l_Error
end function

