﻿$PBExportHeader$f_po_valyear.srf
$PBExportComments$Function to validate a year
global type f_po_valyear from function_object
end type

forward prototypes
global function integer f_po_valyear (ref string value, string format, boolean display_error)
end prototypes

global function integer f_po_valyear (ref string value, string format, boolean display_error);//******************************************************************
//  PO Module     : f_PO_ValYear
//  Function      : f_PO_ValYear
//  Description   : Check the value to see if it is a valid year.
//
//  Parameters    : STRING Value  - String containing a year.
//                  STRING Format - Format to return the value in.
//						  BOOLEAN display_error - Boolean controlling 
//							       whether an error message displays. 	
//
//  Return Value  : 0 = OK, 1 = Error
//
//  Change History:  
//    
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_Error

//------------------------------------------------------------------
//  Assume an error.
//------------------------------------------------------------------

l_Error = 1
Value   = Trim(Value)

//------------------------------------------------------------------
//  Check to see if the value is a valid number.
//------------------------------------------------------------------

IF IsNumber(Value) THEN

   //---------------------------------------------------------------
   //  Check to see if the value is a valid integer.
   //---------------------------------------------------------------

   IF Match(Value, "^[0-9]+$") THEN

      //------------------------------------------------------------
      //  Make sure the length of the value is either 1, 2, or 4
      //  characters.  If a format is given, apply the format and
      //  return the value.
      //------------------------------------------------------------

      CHOOSE CASE Len(Value)
         CASE 1
            l_Error = 0
            Value   = "0" + Value

         CASE 2
            l_Error = 0
            IF Upper(Format) = "YYYY" THEN
               Value = Left(String(Year(Today())), 2) + Value
            END IF

         CASE 4
            l_Error = 0
            IF Upper(Format) = "YY" THEN
               Value = Right(Value, 2)
            END IF
      END CHOOSE
   END IF
END IF

IF l_Error = 1 THEN
   IF display_error THEN
      w_POManager_Std.MB.i_MB_Strings[1] = "::f_PO_ValYear()"
      w_POManager_Std.MB.i_MB_Strings[2] = w_POManager_Std.i_ApplicationName
      w_POManager_Std.MB.i_MB_Strings[3] = Value
      w_POManager_Std.MB.fu_MessageBox           &
         (w_POManager_Std.MB.c_MBI_YearValError, &
          0, w_POManager_Std.MB.i_MB_Numbers[],  &
          3, w_POManager_Std.MB.i_MB_Strings[])
   END IF
END IF

RETURN l_Error
end function

