﻿$PBExportHeader$n_po_date_std.sru
$PBExportComments$Non-visual object descendant of n_PO_Date_Main for customing date utilities
forward
global type n_po_date_std from n_po_date_main
end type
end forward

global type n_po_date_std from n_po_date_main
end type
global n_po_date_std n_po_date_std

on n_po_date_std.create
TriggerEvent( this, "constructor" )
end on

on n_po_date_std.destroy
TriggerEvent( this, "destructor" )
end on

