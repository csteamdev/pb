﻿$PBExportHeader$u_dddw_filter_main.sru
$PBExportComments$Ancestor user object for DDDW filter objects
forward
global type u_dddw_filter_main from datawindow
end type
end forward

global type u_dddw_filter_main from datawindow
int Width=732
int Height=93
int TabOrder=1
event po_valempty pbm_custom74
event po_validate pbm_custom75
end type
global u_dddw_filter_main u_dddw_filter_main

type variables
DATAWINDOW		i_FilterDW

TRANSACTION		i_FilterTransObj
STRING			i_FilterColumn
STRING			i_FilterValue
STRING			i_FilterOriginal

INTEGER			i_FilterError

STRING			i_LoadTable
STRING			i_LoadCode
STRING			i_LoadDesc
STRING			i_LoadWhere
STRING			i_LoadKeyword

BOOLEAN		i_DWCValid
DATAWINDOWCHILD	i_DWC
STRING			i_DWCColName

INTEGER			c_ValOK		= 0
INTEGER			c_ValFailed	= 1


end variables

forward prototypes
public function string fu_selectcode ()
public function integer fu_refreshcode ()
public function integer fu_resetcode ()
public function integer fu_setcode (string default_code)
public function integer fu_wiredw (datawindow filter_dw, string filter_column, transaction filter_transobj)
public function integer fu_loadcode (string table_name, string column_code, string column_desc, string where_clause, string all_keyword)
public function integer fu_buildfilter (boolean filter_reset)
public subroutine fu_unwiredw ()
end prototypes

on po_valempty;////******************************************************************
////  PO Module     : u_DDDW_Filter_Main
////  Event         : po_ValEmpty
////  Description   : Provides the opportunity for the developer to
////                  generate errors when the user has not made a
////                  selection in the DDDW object.
////
////  Return Value  : c_ValOK     = OK
////                  c_ValFailed = Error, criteria is required.
////
////  Change History:
////
////  Date     Person     Description of Change
////  -------- ---------- --------------------------------------------
////
////******************************************************************
////  Copyright ServerLogic 1994-1995.  All Rights Reserved.
////******************************************************************
//
////-------------------------------------------------------------------
////  Set the i_FilterError variable to c_ValFailed to indicate that
////  this filter object is required.
////-------------------------------------------------------------------
//
////SetFocus()
////MessageBox("Filter Object Error", &
////           "This is a required filter field", &
////           Exclamation!, Ok!)
////i_FilterError = c_ValFailed
end on

on po_validate;////******************************************************************
////  PO Module     : u_DDDW_Filter_Main
////  Event         : po_Validate
////  Description   : Provides the opportunity for the developer to
////                  write code to validate the fields in this
////                  object.
////
////  Return Value  : INTEGER i_FilterError 
////
////                  If the developer codes the validation 
////                  testing, then the developer should set 
////                  this variable to one of the following
////                  validation return codes:
////
////                  c_ValOK     = The validation test passed and 
////                                the data is to be accepted.
////                  c_ValFailed = The validation test failed and 
////                                the data is to be rejected.  
////                                Do not allow the user to move 
////                                off of this field.
////
////  Change History:
////
////  Date     Person     Description of Change
////  -------- ---------- --------------------------------------------
////
////******************************************************************
////  Copyright ServerLogic 1992-1995.  All Rights Reserved.
////******************************************************************
//
////------------------------------------------------------------------
////  The following values are set for the developer before this
////  event is triggered:
////
////      STRING i_FilterValue -
////            The input the user has made.
////------------------------------------------------------------------
//
////i_FilterError = f_PO_ValMaxLen(i_FilterValue, 10, TRUE)
end on

public function string fu_selectcode ();//******************************************************************
//  PO Module     : u_DDDW_Filter_Main
//  Function      : fu_SelectCode
//  Description   : Select the code associated with the
//                  description from a non-DataWindow
//                  drop-down DataWindow.
//
//  Parameters    : (None)
//
//  Return Value  : STRING -  <code value> = OK, "" = NULL
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_ColNbr
STRING   l_ReturnText,  l_ColType, l_dwDescribe, l_ColStr

//------------------------------------------------------------------
//  Set the number and name of the DDDW column.
//     Column number is always 1.
//------------------------------------------------------------------

l_ColNbr     = 1
l_dwDescribe = "#" + String(l_ColNbr) + ".Name"
l_ColStr     = Describe(l_dwDescribe)

//------------------------------------------------------------------
//  Pull the code from the drop-down DataWindow.
//------------------------------------------------------------------

l_dwDescribe = l_ColStr + ".ColType"
l_ColType    = Describe(l_dwDescribe)

IF Upper(l_ColType) = "NUMBER" THEN
   l_ReturnText = String(GetItemNumber(1, l_ColNbr))
ELSE
   l_ReturnText = GetItemString(1, l_ColNbr)
END IF

//------------------------------------------------------------------
//  If the code is -99999 or (ALL), return an empty string from the
//  function.
//------------------------------------------------------------------

IF IsNull(l_ReturnText) THEN
   l_ReturnText = ""
ELSE
   IF l_ReturnText = "-99999" OR l_ReturnText = "(ALL)" THEN
      l_ReturnText = ""
   END IF
END IF

RETURN l_ReturnText
end function

public function integer fu_refreshcode ();//******************************************************************
//  PO Module     : u_DDDW_Filter_Main
//  Function      : fu_RefreshCode
//  Description   : Re-retrieves the data into a drop down
//                  DataWindow.  Note that the fu_LoadCode()
//                  routine does the first retrieve.
//
//  Parameters    : (None)
//
//  Return Value  : 0 = OK, -1 = Error
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN          l_HaveAllKeyWord, l_IsNumber
INTEGER          l_ColNbr
LONG             l_Error
STRING           l_ColStr,         l_AllKeyWord
STRING           l_dwDescribe,     l_Attrib
DATAWINDOWCHILD  l_DWC

//------------------------------------------------------------------
//  Set the number and name of the DDDW column. Column number is 
//  always 1.
//------------------------------------------------------------------

l_ColNbr     = 1
l_dwDescribe = "#" + String(l_ColNbr) + ".Name"
l_ColStr     = Describe(l_dwDescribe)

//------------------------------------------------------------------
//  Get the child DataWindow.  If column is not a DDDW column,
//  then return with error.  Otherwise, we need to do the retrieve.
//------------------------------------------------------------------

l_Error = GetChild(l_ColStr, l_DWC)

IF l_Error <> 1 THEN
   w_POManager_Std.MB.fu_MessageBox           &
      (w_POManager_Std.MB.c_MBI_DDDWChildFindError, &
       0, w_POManager_Std.MB.i_MB_Numbers[],  &
       0, w_POManager_Std.MB.i_MB_Strings[])
   RETURN -1
END IF

//------------------------------------------------------------------
//  Get the ALL keyword from the child DataWindow.
//------------------------------------------------------------------

l_HaveAllKeyWord = FALSE
IF l_DWC.RowCount() > 0 THEN
   l_Attrib   = l_DWC.Describe("#2.ColType")
   l_IsNumber = (Upper(l_Attrib) = "NUMBER")
      
   IF l_IsNumber THEN
      l_AllKeyWord     = String(l_DWC.GetItemNumber(1, 2))
      l_HaveAllKeyWord = (l_AllKeyWord = "-99999")
   ELSE
      l_AllKeyWord     = l_DWC.GetItemString(1, 2)
      l_HaveAllKeyWord = (l_AllKeyWord = "(ALL)")
   END IF

   IF l_HaveAllKeyWord THEN
      l_AllKeyWord = l_DWC.GetItemString(1, l_ColNbr)
   END IF
END IF

//------------------------------------------------------------------
//  Insert a row for the user into the parent DataWindow.
//------------------------------------------------------------------

Reset()
l_Error = InsertRow(0)

IF l_Error < 0 THEN
   w_POManager_Std.MB.fu_MessageBox           &
      (w_POManager_Std.MB.c_MBI_DDDWInsertRowError, &
       0, w_POManager_Std.MB.i_MB_Numbers[],  &
       0, w_POManager_Std.MB.i_MB_Strings[])
   RETURN -1
END IF

//------------------------------------------------------------------
//  Retrieve the data for the child DataWindow.
//------------------------------------------------------------------

l_Error = l_DWC.Retrieve()
IF l_Error < 0 THEN
   w_POManager_Std.MB.fu_MessageBox           &
      (w_POManager_Std.MB.c_MBI_DDDWRetrieveError, &
       0, w_POManager_Std.MB.i_MB_Numbers[],  &
       0, w_POManager_Std.MB.i_MB_Strings[])
   RETURN -1
END IF

//------------------------------------------------------------------
//  If an ALL keyword is given, re-insert it into the first row
//  on the child DataWindow.
//------------------------------------------------------------------

IF l_HaveAllKeyWord THEN
   IF l_DWC.InsertRow(1) > 0 THEN
      l_DWC.SetItem(1, l_ColNbr, l_AllKeyWord)

      IF l_IsNumber THEN
         l_DWC.SetItem(1, 2, -99999)
      ELSE
         l_DWC.SetItem(1, 2, "(ALL)")
      END IF
   END IF
END IF

//------------------------------------------------------------------
//  Indicate that there not any errors.
//------------------------------------------------------------------

RETURN 0
end function

public function integer fu_resetcode ();//******************************************************************
//  PO Module     : u_DDDW_Filter_Main
//  Function      : fu_ResetCode
//  Description   : Clears the selection the DDDW filter object.
//
//  Parameters    : (None)
//
//  Return Value  : 0 = OK, -1 = Insert Error
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER l_Error

Reset()
l_Error = InsertRow(0)

IF l_Error < 0 THEN
   w_POManager_Std.MB.fu_MessageBox           &
      (w_POManager_Std.MB.c_MBI_DDDWInsertRowError, &
       0, w_POManager_Std.MB.i_MB_Numbers[],  &
       0, w_POManager_Std.MB.i_MB_Strings[])
   RETURN -1
END IF

RETURN 0
end function

public function integer fu_setcode (string default_code);//******************************************************************
//  PO Module     : u_DDDW_Filter
//  Subroutine    : fu_SetCode
//  Description   : Sets a default value for this object.
//
//  Parameters    : STRING Default_Code -
//                     The value to set for this object.
//
//  Return Value  : 0 = set OK   -1 = set failed
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

DATAWINDOWCHILD l_DWC
STRING          l_Value, l_ColType, l_ColStr, l_Describe, l_String
LONG            l_Idx, l_TotalItems
INTEGER         l_Error, l_ColNbr
DOUBLE          l_Number

//------------------------------------------------------------------
//  Set the number and name of the DDDW column. Column number is 
//  always 1.
//------------------------------------------------------------------

l_ColNbr   = 1
l_Describe = "#" + String(l_ColNbr) + ".Name"
l_ColStr   = Describe(l_Describe)

l_Error = GetChild(l_ColStr, l_DWC)

IF l_Error <> 1 THEN
   w_POManager_Std.MB.fu_MessageBox           &
      (w_POManager_Std.MB.c_MBI_DDDWChildFindError, &
       0, w_POManager_Std.MB.i_MB_Numbers[],  &
       0, w_POManager_Std.MB.i_MB_Strings[])
   RETURN -1
END IF

l_ColType    = UPPER(l_DWC.Describe("#2.ColType"))

l_TotalItems = l_DWC.RowCount()
FOR l_Idx = 1 TO l_TotalItems
   IF l_ColType = "NUMBER" THEN
      l_Number = l_DWC.GetItemNumber(l_Idx, 2)
      IF String(l_Number) = default_code THEN
         SetItem(1, 1, l_Number)
         RETURN 0
      END IF
   ELSE
      l_String = l_DWC.GetItemString(l_Idx, 2)
      IF l_String = default_code THEN
         SetItem(1, 1, l_String)
         RETURN 0
      END IF
   END IF
NEXT

RETURN -1
end function

public function integer fu_wiredw (datawindow filter_dw, string filter_column, transaction filter_transobj);//******************************************************************
//  PO Module     : u_DDDW_Filter_Main
//  Subroutine    : fu_WireDW
//  Description   : Wires a column in a DataWindow to this object.
//
//  Parameters    : DATAWINDOW Filter_DW -
//                     The DataWindow that is to be wired to
//                     this filter object.
//                  STRING Filter_Column -
//                     The column in the DataWindow that
//                     the filter object should filter through.
//                  TRANSACTION Filter_TransObj - 
//                     Transaction object associated with the
//                     filter DataWindow.
//
//  Return Value  : 0 = valid DataWindow   -1 = invalid DataWindow
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

INTEGER l_Return = -1

//------------------------------------------------------------------
//  Make sure that we were passed a valid DataWindow to be wired.
//------------------------------------------------------------------

IF IsValid(filter_dw) THEN

   //---------------------------------------------------------------
   //  Remember the DataWindow tabe and column for filtering.
   //---------------------------------------------------------------

   i_FilterDW       = filter_dw
   i_FilterColumn   = filter_column
   i_FilterTransObj = filter_transobj
   i_FilterOriginal = i_FilterDW.Describe("datawindow.table.filter")
   IF i_FilterOriginal = "?" THEN
      i_FilterOriginal = ""
   END IF
   Enabled          = TRUE
   l_Return         = 0

END IF

RETURN l_Return
end function

public function integer fu_loadcode (string table_name, string column_code, string column_desc, string where_clause, string all_keyword);//******************************************************************
//  PO Module     : u_DDDW_Filter_Main
//  Function      : fu_LoadCode
//  Description   : Load a non-DataWindow drop down DataWindow
//                  using codes and descriptions from the
//                  database.
//
//  Parameters    : STRING      Table_Name   - 
//                     Table from where the column with the code
//                     values resides.
//                  STRING      Column_Code  - 
//                     Column name with the code values.
//                  STRING      Column_Desc  - 
//                     Column name with the code descriptions.
//                  STRING      Where_Clause - 
//                     WHERE cause statement to restrict the code 
//                     values.
//                  STRING      All_Keyword  - 
//                     Keyword to denote select all values (e.g. 
//                     "(All)").
//
//  Return Value  : 0 = OK, -1 = Error
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER          l_ColNbr,     l_Idx,        l_Jdx
LONG             l_Error
STRING           l_SQLString,  l_ErrStr
STRING           l_dwDescribe, l_dwModify, l_Attrib, l_ColStr
TRANSACTION      l_Trans
DATAWINDOWCHILD  l_DWC

STRING           c_ColAttrib[]  =            &
                    { ".BackGround.Color",   & 
                      ".Background.Mode",    &
                      ".Border",             &
                      ".Color",              &
                      ".Font.CharSet",       &
                      ".Font.Face",          &
                      ".Font.Family",        &
                      ".Font.Height",        &
                      ".Font.Italic",        &
                      ".Font.Pitch",         &
                      ".Font.Strikethrough", &
                      ".Font.Underline",     &
                      ".Font.Weight",        &
                      ".Font.Width",         &
                      ".Height",             &
                      ".Width" }

i_LoadTable = table_name
i_LoadCode = column_code
i_LoadDesc = column_desc
i_LoadWhere = where_clause
i_LoadKeyword = all_keyword

//------------------------------------------------------------------
//  Set the number and name of the DDDW column. Column number is 
//  always 1.
//------------------------------------------------------------------

l_ColNbr     = 1
l_dwDescribe = "#" + String(l_ColNbr) + ".Name"
l_ColStr     = Describe(l_dwDescribe)

//------------------------------------------------------------------
//  Build the SQL SELECT statement.
//------------------------------------------------------------------

l_SQLString = "SELECT " + i_LoadDesc + ", " + &
              i_LoadCode + " FROM " + i_LoadTable

IF Trim(i_LoadWhere) <> "" THEN
   l_SQLString = l_SQLString + " WHERE " + i_LoadWhere
END IF

//------------------------------------------------------------------
//  Get the child DataWindow.  If column is not a DDDW column,
//  then return with error.  Otherwise, we need to set the
//  Select statement in the drop down DataWindow.
//------------------------------------------------------------------

l_Error = GetChild(l_ColStr, l_DWC)

IF l_Error <> 1 THEN
   w_POManager_Std.MB.fu_MessageBox           &
      (w_POManager_Std.MB.c_MBI_DDDWChildFindError, &
       0, w_POManager_Std.MB.i_MB_Numbers[],  &
       0, w_POManager_Std.MB.i_MB_Strings[])
   RETURN -1
END IF

//------------------------------------------------------------------
//  Set the transaction object.
//------------------------------------------------------------------

l_Trans = i_FilterTransObj
l_Error = l_DWC.SetTransObject(l_Trans)
IF l_Error <> 1 THEN
   w_POManager_Std.MB.fu_MessageBox           &
      (w_POManager_Std.MB.c_MBI_DDDWTransactionError, &
       0, w_POManager_Std.MB.i_MB_Numbers[],  &
       0, w_POManager_Std.MB.i_MB_Strings[])
   RETURN -1
END IF

//------------------------------------------------------------------
//  Assign the Select statement to the drop down DataWindow.
//------------------------------------------------------------------

l_dwModify = "DataWindow.Table.Select='" + &
             w_POManager_STD.MB.fu_QuoteChar(l_SQLString, "'")  + "'"
l_ErrStr   = l_DWC.Modify(l_dwModify)

IF l_ErrStr <> "" THEN
   w_POManager_Std.MB.fu_MessageBox           &
      (w_POManager_Std.MB.c_MBI_DDDWModifySQLError, &
       0, w_POManager_Std.MB.i_MB_Numbers[],  &
       0, w_POManager_Std.MB.i_MB_Strings[])
   RETURN -1
END IF

//------------------------------------------------------------------
//  Insert a row for the user into the parent DataWindow.
//------------------------------------------------------------------

Reset()
l_Error = InsertRow(0)

IF l_Error < 0 THEN
   w_POManager_Std.MB.fu_MessageBox           &
      (w_POManager_Std.MB.c_MBI_DDDWInsertRowError, &
       0, w_POManager_Std.MB.i_MB_Numbers[],  &
       0, w_POManager_Std.MB.i_MB_Strings[])
   RETURN -1
END IF

//------------------------------------------------------------------
//  Retrieve the data for the child DataWindow.
//------------------------------------------------------------------

l_Error = l_DWC.Retrieve()
IF l_Error < 0 THEN
   w_POManager_Std.MB.fu_MessageBox           &
      (w_POManager_Std.MB.c_MBI_DDDWRetrieveError, &
       0, w_POManager_Std.MB.i_MB_Numbers[],  &
       0, w_POManager_Std.MB.i_MB_Strings[])
   RETURN -1
END IF

//------------------------------------------------------------------
//  If an ALL keyword is given, insert it into the first row on the
//  child DataWindow.
//------------------------------------------------------------------

IF Trim(i_LoadKeyword) <> "" THEN
   IF l_DWC.InsertRow(1) > 0 THEN
      l_Attrib = l_DWC.Describe("#1.ColType")
      IF Upper(l_Attrib) <> "NUMBER" THEN
         l_DWC.SetItem(1, l_ColNbr, i_LoadKeyword)
      END IF

      l_Attrib = l_DWC.Describe("#2.ColType")
      IF Upper(l_Attrib) = "NUMBER" THEN
         l_DWC.SetItem(1, 2, -99999)
      ELSE
         l_DWC.SetItem(1, 2, "(ALL)")
      END IF
      l_DWC.SelectRow(0, FALSE)
      l_DWC.SelectRow(1, TRUE)
   END IF
END IF

//------------------------------------------------------------------
//  Set the attributes of the column to be the same as the
//  parent column.
//------------------------------------------------------------------

l_Jdx = UpperBound(c_ColAttrib[])
FOR l_Idx = 1 TO l_Jdx

   l_dwDescribe = l_ColStr + c_ColAttrib[l_Idx]
   l_Attrib     = Describe(l_dwDescribe)

   IF c_ColAttrib[l_Idx] = ".Font.Face" THEN
      IF l_Attrib = "?" OR l_Attrib = "!" THEN
         l_Attrib = "Arial"
      END IF
      l_Attrib = "'" + l_Attrib + "'"
   ELSE
      IF l_Attrib = "?" OR l_Attrib = "!" THEN
         l_Attrib = "0"
      END IF
   END IF

   l_dwModify = "#1" + c_ColAttrib[l_Idx] + "=" + l_Attrib
   l_ErrStr   = l_DWC.Modify(l_dwModify)

NEXT

//------------------------------------------------------------------
//  Make sure the border for the child column is off.
//------------------------------------------------------------------

l_dwModify = "#1" + ".Border=0"
l_ErrStr   = l_DWC.Modify(l_dwModify)

//------------------------------------------------------------------
//  Indicate that there not any errors.
//------------------------------------------------------------------

RETURN 0
end function

public function integer fu_buildfilter (boolean filter_reset);//******************************************************************
//  PO Module     : u_DDDW_Filter_Main
//  Function      : fu_BuildFilter
//  Description   : Extends the filter clause of a DataWindow
//                  with the values in this filter object.
//
//  Parameters    : BOOLEAN Filter_Reset -
//                     Should the DataWindow filter be reset to its
//                     original state before the building begins.
//
//  Return Value  : 0 = build OK   -1 = validation error
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN  l_First
INTEGER  l_GroupByPos, l_OrderByPos
STRING   l_Text, l_ColumnType, l_Concat, l_NewFilter
STRING   l_GroupBy, l_OrderBy, l_Quotes, l_Other

//------------------------------------------------------------------
//  See if the DataWindow filter criteria needs to be reset to its
//  original syntax.
//------------------------------------------------------------------

IF filter_reset THEN
   i_FilterDW.Modify('datawindow.table.filter="' + &
                     i_FilterOriginal + '"')
END IF

l_NewFilter = i_FilterDW.Describe("datawindow.table.filter")
IF l_NewFilter = "?" THEN
   l_NewFilter = ""
END IF

l_Text = fu_SelectCode()

//------------------------------------------------------------------
//  If this object has been un-wired, don't include in build.
//------------------------------------------------------------------

IF IsNull(i_FilterDW) THEN
   RETURN -1
END IF

l_Concat = ""
IF l_NewFilter <> "" THEN
   l_Concat = " AND "
END IF

//------------------------------------------------------------------
//  Check to see if the developer made this a required field for
//  filter criteria.
//------------------------------------------------------------------

IF l_Text = "" THEN
   i_FilterError = c_ValOK
   i_FilterValue = l_Text
   TriggerEvent("po_ValEmpty")

   //---------------------------------------------------------------
   //  If we get a validation error, abort the filter.
   //---------------------------------------------------------------

   IF i_FilterError = c_ValFailed THEN
      SetFocus()
      RETURN -1
   ELSE
      RETURN 0
   END IF
END IF

//------------------------------------------------------------------
//  Assume that it does not contain anything that needs quoting.
//------------------------------------------------------------------

l_Quotes = ""

//------------------------------------------------------------------
//  See what type of data this filter object contains.
//------------------------------------------------------------------

l_ColumnType = Upper(i_FilterDW.Describe(i_FilterColumn + ".ColType"))
IF Left(l_ColumnType, 3) = "DEC" THEN
   l_ColumnType = "NUMBER"
END IF

CHOOSE CASE l_ColumnType
   CASE "NUMBER"
   CASE "DATE"
   CASE "DATETIME"
   CASE "TIME"

      //------------------------------------------------------------
      //  We have a data type that needs to be quoted (e.g. STRING).
      //------------------------------------------------------------

   CASE ELSE
      l_Quotes = "'"
END CHOOSE

//------------------------------------------------------------------
//  Stuff the current token in and perform valdiation.
//------------------------------------------------------------------

i_FilterValue = l_Text
i_FilterError = c_ValOK
TriggerEvent("po_Validate")

//------------------------------------------------------------------
//  If we get a validation error, abort the filter.
//------------------------------------------------------------------

IF i_FilterError = c_ValFailed THEN
   SetFocus()
   RETURN -1
END IF

//------------------------------------------------------------------
//  If we got to here, validation must have been successful. 
//  Validation may have modified the token.  Grab it back.
//------------------------------------------------------------------

l_Text = i_FilterValue

//------------------------------------------------------------------
//  Add the validated value.
//------------------------------------------------------------------

l_Other = l_Quotes + l_Text + l_Quotes

//------------------------------------------------------------------
//  Add the snippet generated by the this object to our new 
//  filtr statement.
//------------------------------------------------------------------

IF Len(l_Other) > 0 THEN
   l_NewFilter = l_NewFilter + l_Concat + "(" + i_FilterColumn + &
                               " = " + l_Other + ")"
END IF

//------------------------------------------------------------------
//  Stuff the parameter with the completed filter statement.
//------------------------------------------------------------------

l_NewFilter = 'datawindow.table.filter="' + l_NewFilter + '"'
i_FilterDW.Modify(l_NewFilter)

RETURN 0
end function

public subroutine fu_unwiredw ();//******************************************************************
//  PO Module     : u_DDDW_Filter_Main
//  Subroutine    : fu_UnwireDW
//  Description   : Un-wires a DataWindow from the filter object.
//
//  Parameters    : (None)
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

SetNull(i_FilterDW)
Enabled = FALSE

end subroutine

on resize;//******************************************************************
//  PO Module     : u_DDDW_Filter_Main
//  Event         : Resize
//  Description   : Resizes the Filter Object.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_XPos,       l_YPos
LONG     l_Width,      l_Height
STRING   l_dwDescribe, l_dwModify, l_ErrStr

IF Border THEN
   IF BorderStyle = StyleBox! THEN
      Border = FALSE
   END IF
END IF

l_Width  = UnitsToPixels(Width, XUnitsToPixels!) - 2
l_Height = UnitsToPixels(Height, YUnitsToPixels!) - 2

IF Border THEN
   CHOOSE CASE BorderStyle
      CASE StyleLowered!
         l_XPos  = -1
         l_YPos  = -2
         l_Width = l_Width - 1
      CASE StyleRaised!
         l_XPos  = 1
         l_YPos  = -2
         l_Width = l_Width - 1
      CASE StyleShadowBox!
         l_XPos  = 0
         l_YPos  = -2
         l_Width = l_Width - 1
   END CHOOSE
ELSE
   IF TitleBar THEN
      l_XPos   = 0
      l_YPos   = -1
      l_Height = l_Height - 1
   ELSE
      l_XPos   = 1
      l_YPos   = 0
      l_Width  = l_Width  - 2
      l_Height = l_Height - 2
   END IF
END IF

l_XPos     = PixelsToUnits(l_XPos,   XPixelsToUnits!)
l_YPos     = PixelsToUnits(l_YPos,   YPixelsToUnits!)
l_Width    = PixelsToUnits(l_Width,  XPixelsToUnits!)
l_Height   = PixelsToUnits(l_Height, YPixelsToUnits!)

l_dwModify = "#1.X=" + String(l_XPos)
l_ErrStr   = Modify(l_dwModify)

l_dwModify = "#1.Y=" + String(l_YPos)
l_ErrStr   = Modify(l_dwModify)

l_dwModify = "#1.Width=" + String(l_Width)
l_ErrStr   = Modify(l_dwModify)

IF i_DWCValid THEN
   l_ErrStr = i_DWC.Modify(l_dwModify)
END IF

l_dwModify = "#1.Height=" + String(l_Height)
l_ErrStr   = Modify(l_dwModify)

IF i_DWCValid THEN
   l_ErrStr = i_DWC.Modify(l_dwModify)
END IF
end on

on constructor;//******************************************************************
//  PO Module     : u_DDDW_Filter_Main
//  Event         : Constructor
//  Description   : Initializes the Filter Object.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER    l_Idx,        l_Jdx
STRING     l_dwDescribe, l_dwModify, l_Attrib, l_ErrStr
USEROBJECT l_UserObject
WINDOW     l_Window

//------------------------------------------------------------------
//  Identify this object as a PowerObject Filter.
//------------------------------------------------------------------

Tag = "PowerObject Filter DDDW"

STRING   c_ColAttrib[] = &
            { ".BackGround.Color",   &
              ".Background.Mode",    &
              ".Border",             &
              ".Color",              &
              ".Font.CharSet",       &
              ".Font.Face",          &
              ".Font.Family",        &
              ".Font.Height",        &
              ".Font.Italic",        &
              ".Font.Pitch",         &
              ".Font.Strikethrough", &
              ".Font.Underline",     &
              ".Font.Weight",        &
              ".Font.Width" }

Enabled     = FALSE
SetNull(i_FilterDW)

IF Parent.TypeOf() = UserObject! THEN
   l_UserObject = Parent
   l_Attrib     = String(l_UserObject.BackColor)
ELSE
   l_Window     = Parent
   l_Attrib     = String(l_Window.BackColor)
END IF

l_dwModify = "DataWindow.Color=" + l_Attrib
l_ErrStr   = Modify(l_dwModify)

l_dwModify = "DataWindow.Header.Color=" + l_Attrib
l_ErrStr   = Modify(l_dwModify)

l_dwModify = "DataWindow.Detail.Color=" + l_Attrib
l_ErrStr   = Modify(l_dwModify)

l_dwModify = "DataWindow.Footer.Color=" + l_Attrib
l_ErrStr   = Modify(l_dwModify)

l_dwModify = "#1.DDDW.UseAsBorder=Yes"
l_ErrStr   = Modify(l_dwModify)

l_dwModify = "#1.Border=0"
l_ErrStr   = Modify(l_dwModify)

//------------------------------------------------------------------
//  Set the number and name of the DDDW column.
//     Column number is always 1.
//------------------------------------------------------------------

l_dwDescribe = "#1" + ".Name"
i_DWCColName = Describe(l_dwDescribe)

//------------------------------------------------------------------
//  Get the child DataWindow.
//------------------------------------------------------------------

i_DWCValid = (GetChild(i_DWCColName, i_DWC) = 1)

TriggerEvent("Resize")
end on

