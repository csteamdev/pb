﻿$PBExportHeader$u_dw_search.sru
$PBExportComments$User object that uses a DataWindow as a search object
forward
global type u_dw_search from datawindow
end type
end forward

global type u_dw_search from datawindow
int Width=1422
int Height=637
int TabOrder=1
string DataObject="d_main"
event po_validate pbm_custom75
end type
global u_dw_search u_dw_search

type variables
STRING			i_ColumnName[]
STRING			i_ColumnTabOrder[]
STRING			i_ColumnEditStyle[]
STRING			i_ColumnDefault[]

DATAWINDOW		i_SearchDW
TRANSACTION		i_SearchTransObj
STRING			i_SearchTable[]
STRING			i_SearchColumnDB[]
INTEGER			i_NumSearch

STRING			i_LoadTable[]
STRING			i_LoadCode[]
STRING			i_LoadDesc[]
STRING			i_LoadWhere[]
STRING			i_LoadKeyword[]

INTEGER			i_SearchRow = 1
STRING			i_SearchColumn
STRING			i_SearchValue
STRING			i_SearchValMsg
BOOLEAN		i_SearchRequired

INTEGER			i_SearchError
BOOLEAN		i_SearchRequiredError
BOOLEAN		i_SearchModified
STRING			i_SearchOriginal

INTEGER			i_InItemError
BOOLEAN		i_IgnoreVal
BOOLEAN		i_DisplayRequired
BOOLEAN		i_ItemValidated

BOOLEAN		i_DWCValid
DATAWINDOWCHILD	i_DWC
STRING			i_DWCColName

INTEGER			c_ValOK		= 0
INTEGER			c_ValFailed	= 1
INTEGER			c_ValFixed	= 2
INTEGER			c_ValNotProcessed	= -9

end variables

forward prototypes
public function integer fu_setcode (string search_column, string default_code)
public function integer fu_showcolumn (string search_column)
public function integer fu_hidecolumn (string search_column)
public function integer fu_enablecolumn (string search_column)
public function integer fu_disablecolumn (string search_column)
public function integer fu_refreshcode (string search_column)
public subroutine fu_reset ()
public function string fu_selectcode (string search_column)
public function integer fu_wiredw (string column_name[], datawindow search_dw, string search_table[], string search_column[], transaction search_transobj)
public function integer fu_buildsearch (boolean search_reset)
public function integer fu_loadcode (string search_column, string table_name, string column_code, string column_desc, string where_clause, string all_keyword)
public subroutine fu_unwiredw ()
end prototypes

on po_validate;////******************************************************************
////  PO Module     : u_DW_Search
////  Event         : po_Validate
////  Description   : Provides the opportunity for the developer to
////                  write code to validate the fields in this
////                  DataWindow.
////
////  Return Value  : INTEGER i_SearchError 
////
////                  If the developer codes the validation 
////                  testing, then the developer should set 
////                  this variable to one of the following
////                  validation return codes:
////
////                  c_ValOK     = The validation test passed and 
////                                the data is to be accepted.
////                  c_ValFailed = The validation test failed and 
////                                the data is to be rejected.  
////                                Do not allow the user to move 
////                                off of this field.
////                  c_ValFixed  = The validation test failed.  
////                                However, the data was able to be
////                                modified by developer to pass the
////                                validation test.  If a developer
////                                returns this code, they are 
////                                responsible for doing a SetItem()
////                                to place the fixed value into the
////                                column.  Note that if the 
////                                DataWindow validation routines 
////                                are used, they will do the 
////                                SetItem(), if necessary.
////
////  Change History:
////
////  Date     Person     Description of Change
////  -------- ---------- --------------------------------------------
////
////******************************************************************
////  Copyright ServerLogic 1992-1995.  All Rights Reserved.
////******************************************************************
//
////------------------------------------------------------------------
////  The following values are set for the developer before this
////  event is triggered:
////
////      STRING i_SearchValue -
////            The input the user has made.
////
////      STRING i_SearchColumn -
////            The name of the column being validated, folded
////            to lower case.
////
////      STRING i_SearchRow -
////            The row number of this DataWindow.  This value is
////            always equal to 1.
////
////      STRING i_SearchValMsg -
////            The validation error message provided by
////            PowerBuilder's extended column definition.
////
////      BOOLEAN i_SearchRequiredError -
////            This column is a required field and the user has
////            not entered anything.
////------------------------------------------------------------------
//
////------------------------------------------------------------------
////  Determine which column to validate.
////------------------------------------------------------------------
//
//CHOOSE CASE i_SearchColumn
//
////   //-------------------------------------------------------------
////   //  Enter the code for each column you want to validate.
////   //  <column> should be replaced by the name of the column
////   //  from the DataWindow.  Use the i_SearchError variable to
////   //  return the status of the validation.  Some sample
////   //  validation functions are listed below.  
////   //-------------------------------------------------------------
//
////   CASE "<column1>"
//
////      i_SearchError = f_PO_ValInt(i_SearchValue, "##,##0", TRUE)
//
////   CASE "<column2>"
//
////      i_SearchError = f_PO_ValDec(i_SearchValue, "##,##0.00", TRUE)
////      IF i_SearchError <> c_ValFailed THEN
////         IF Real(i_SearchValue) < 0 THEN
////            MessageBox("Search Object Error", "<error_message>")
////            i_SearchError = c_ValFailed
////         END IF
////      END IF
//
////   CASE "<column3>"
//
////      IF Upper(Left(i_SearchValue, 1)) = "Y"  THEN
////         SetItem(i_SearchRow, i_SearchColumn, "Yes")
////         i_SearchError = c_ValFixed
////      ELSE
////         IF Upper(Left(i_SearchValue, 1)) = "N"  THEN
////            SetItem(i_SearchRow, i_SearchColumn, "No")
////            i_SearchError = c_ValFixed
////         ELSE
////            MessageBox("Search Object Error", &
////                "Valid responses are 'Y' and 'N'")
////            i_SearchError = c_ValFailed
////         END IF
////      END IF
//
////   CASE "<column4>"
//
////      IF i_SearchRequiredError THEN
////         IF MessageBox("Search Object Error", &
////                "Do you want a default value of 'Active'?", &
////                Question!, YesNo!, 1) = 1 THEN
////            SetItem(i_SearchRow, i_SearchColumn, "Active")
////            i_SearchError = c_ValFixed
////         ELSE
////            i_SearchError = c_ValFailed
////         END IF
////      END IF
//
////   CASE "<column5>"  
////
////      Generate a default value for a CheckBox
////
////      IF i_SearchRequiredError THEN
////         SetItem(i_SearchRow, i_SearchColumn, "N")
////         i_SearchError = c_ValFixed
////      END IF
//
//END CHOOSE

end on

public function integer fu_setcode (string search_column, string default_code);//******************************************************************
//  PO Module     : u_DW_Search
//  Subroutine    : fu_SetCode
//  Description   : Sets a default value for a column in this
//                  object.
//
//  Parameters    : STRING Search_Column -
//                     The column in this DataWindow to set the
//                     default code for.
//                  STRING Default_Code  -
//                     The default value to set.
//
//  Return Value  : 0 = set OK   -1 = set failed or invalid column
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

STRING  l_ColType
INTEGER l_Return, l_Idx

//------------------------------------------------------------------
//  Set the default code depending on the edit style of the column.
//------------------------------------------------------------------

l_ColType  = UPPER(Describe(search_column + ".ColType"))
IF l_ColType = "?" THEN
   RETURN -1
END IF
IF Left(l_ColType, 3) = "DEC" THEN
   l_ColType = "NUMBER"
END IF
IF Left(l_ColType, 4) = "CHAR" THEN
   l_ColType = "STRING"
END IF

FOR l_Idx = 1 TO i_NumSearch
   IF i_ColumnName[l_Idx] = search_column THEN
      i_ColumnDefault[l_Idx] = default_code
      EXIT
   END IF
NEXT

CHOOSE CASE l_ColType
   CASE "NUMBER"
      l_Return = SetItem(i_SearchRow, search_column, Double(default_code))
   CASE "STRING"
      l_Return = SetItem(i_SearchRow, search_column, default_code)
   CASE "DATE"
      l_Return = SetItem(i_SearchRow, search_column, Date(default_code))
   CASE "DATETIME"
      l_Return = SetItem(i_SearchRow, search_column, DateTime(Date(default_code)))
   CASE "TIME"
      l_Return = SetItem(i_SearchRow, search_column, Time(default_code))
END CHOOSE

IF l_Return = 1 THEN
   l_Return = 0
END IF

RETURN l_Return
end function

public function integer fu_showcolumn (string search_column);//******************************************************************
//  PO Module     : u_DW_Search
//  Subroutine    : fu_ShowColumn
//  Description   : Make a column on this DataWindow visible.
//
//  Parameters    : STRING Search_Column -
//                     The column in this DataWindow to set to
//                     visible.
//
//  Return Value  : 0 = OK     -1 = invalid column
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

STRING l_Return

l_Return = Modify(search_column + ".Visible=1")

IF l_Return = "" THEN
   RETURN 0
ELSE
   RETURN -1
END IF

end function

public function integer fu_hidecolumn (string search_column);//******************************************************************
//  PO Module     : u_DW_Search
//  Subroutine    : fu_HideColumn
//  Description   : Make a column on this DataWindow invisible.
//
//  Parameters    : STRING Search_Column -
//                     The column in this DataWindow to set to
//                     invisible.
//
//  Return Value  : 0 = OK     -1 = invalid column
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

STRING l_Return

l_Return = Modify(search_column + ".Visible=0")

IF l_Return = "" THEN
   RETURN 0
ELSE
   RETURN -1
END IF

end function

public function integer fu_enablecolumn (string search_column);//******************************************************************
//  PO Module     : u_DW_Search
//  Subroutine    : fu_EnableColumn
//  Description   : Make a column on this DataWindow enabled.
//
//  Parameters    : STRING Search_Column -
//                     The column in this DataWindow to set to
//                     enable.
//
//  Return Value  : 0 = OK     -1 = invalid column
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

INTEGER l_Idx
STRING  l_Return = "?"

FOR l_Idx = 1 TO i_NumSearch
   IF i_ColumnName[l_Idx] = search_column THEN
      l_Return = Modify(search_column + ".TabSequence=" + i_ColumnTabOrder[l_Idx])
      EXIT
   END IF
NEXT

IF l_Return = "" THEN
   RETURN 0
ELSE
   RETURN -1
END IF

end function

public function integer fu_disablecolumn (string search_column);//******************************************************************
//  PO Module     : u_DW_Search
//  Subroutine    : fu_DisableColumn
//  Description   : Make a column on this DataWindow disabled.
//
//  Parameters    : STRING Search_Column -
//                     The column in this DataWindow to set to
//                     disable.
//
//  Return Value  : 0 = OK     -1 = invalid column
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

STRING  l_Return

l_Return = Modify(search_column + ".TabSequence=0")

IF l_Return = "" THEN
   RETURN 0
ELSE
   RETURN -1
END IF

end function

public function integer fu_refreshcode (string search_column);//******************************************************************
//  PO Module     : u_DW_Search
//  Function      : fu_RefreshCode
//  Description   : Re-retrieves the data into a drop down
//                  DataWindow, drop down list box or spin box.
//                  Note that the fu_LoadCode() routine does the 
//                  first retrieve.
//
//  Parameters    : (None)
//
//  Return Value  : 0 = OK, -1 = Error
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN          l_IsNumber, l_HaveAllKeyword
INTEGER          l_Idx
LONG             l_Error
STRING           l_EditStyle, l_Table, l_Code, l_Desc, l_Where
STRING           l_Keyword, l_Attrib, l_AllKeyword
DATAWINDOWCHILD  l_DWC

//------------------------------------------------------------------
//  Determine the edit style of the column.
//------------------------------------------------------------------

FOR l_Idx = 1 TO i_NumSearch
   IF i_ColumnName[l_Idx] = search_column THEN
      l_EditStyle = i_ColumnEditStyle[l_Idx]
      l_Table     = i_LoadTable[l_Idx]
      l_Code      = i_LoadCode[l_Idx]
      l_Desc      = i_LoadDesc[l_Idx]
      l_Where     = i_LoadWhere[l_Idx]
      l_Keyword   = i_LoadKeyword[l_Idx]
      EXIT
   END IF
NEXT

CHOOSE CASE l_EditStyle

   CASE "DDDW"

      //------------------------------------------------------------
      //  Get the child DataWindow.  If column is not a DDDW column,
      //  then return with error.  Otherwise, we need to do the 
      //  retrieve.
      //------------------------------------------------------------

      l_Error = GetChild(search_column, l_DWC)

      IF l_Error <> 1 THEN
         w_POManager_Std.MB.fu_MessageBox           &
            (w_POManager_Std.MB.c_MBI_DDDWChildFindError, &
            0, w_POManager_Std.MB.i_MB_Numbers[],  &
            0, w_POManager_Std.MB.i_MB_Strings[])
         RETURN -1
      END IF

      //------------------------------------------------------------
      //  Get the ALL keyword from the child DataWindow.
      //------------------------------------------------------------

      l_HaveAllKeyWord = FALSE
      IF l_DWC.RowCount() > 0 THEN
         l_Attrib   = l_DWC.Describe("#2.ColType")
         l_IsNumber = (Upper(l_Attrib) = "NUMBER")
      
         IF l_IsNumber THEN
            l_AllKeyWord     = String(l_DWC.GetItemNumber(1, 2))
            l_HaveAllKeyWord = (l_AllKeyWord = "-99999")
         ELSE
            l_AllKeyWord     = l_DWC.GetItemString(1, 2)
            l_HaveAllKeyWord = (l_AllKeyWord = "^")
         END IF
      END IF

      //------------------------------------------------------------
      //  Retrieve the data for the child DataWindow.
      //------------------------------------------------------------

      l_Error = l_DWC.Retrieve()
      IF l_Error < 0 THEN
         w_POManager_Std.MB.fu_MessageBox           &
            (w_POManager_Std.MB.c_MBI_DDDWRetrieveError, &
            0, w_POManager_Std.MB.i_MB_Numbers[],  &
            0, w_POManager_Std.MB.i_MB_Strings[])
         RETURN -1
      END IF

      //------------------------------------------------------------------
      //  If an ALL keyword is given, re-insert it into the first 
      //  row on the child DataWindow.
      //------------------------------------------------------------------

      IF l_HaveAllKeyWord THEN
         IF l_DWC.InsertRow(1) > 0 THEN
            l_DWC.SetItem(1, 1, l_Keyword)

            IF l_IsNumber THEN
               l_DWC.SetItem(1, 2, -99999)
            ELSE
               l_DWC.SetItem(1, 2, "^")
            END IF
         END IF
      END IF

   CASE "DDLB"

      RETURN fu_LoadCode(search_column, l_Table, l_Code, &
                         l_Desc, l_Where, l_Keyword)

   CASE "EDITMASK"

      RETURN fu_LoadCode(search_column, l_Table, l_Code, &
                         l_Desc, l_Where, l_Keyword)

   CASE ELSE

      RETURN -1

END CHOOSE

RETURN 0


end function

public subroutine fu_reset ();//******************************************************************
//  PO Module     : u_DW_Search
//  Function      : fu_Reset
//  Description   : Clears the search criteria from this object.
//
//  Parameters    : (None)
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER l_Idx

SetReDraw(FALSE)

RowsDiscard(1, 1, Primary!)
InsertRow(0)

FOR l_Idx = 1 TO i_NumSearch
   IF i_ColumnDefault[l_Idx] <> "" THEN
      fu_SetCode(i_ColumnName[l_Idx], i_ColumnDefault[l_Idx])
   END IF
NEXT

SetReDraw(TRUE)


end subroutine

public function string fu_selectcode (string search_column);//******************************************************************
//  PO Module     : u_DW_Search
//  Function      : fu_SelectCode
//  Description   : Gets the search criteria for a column in this
//                  object.
//
//  Parameters    : STRING Search_Column -
//                     The column in this DataWindow to get the
//                     search criteria for.
//
//  Return Value  : STRING -  <code value> = OK, "" = NULL
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

STRING  l_ColType
STRING  l_Return

//------------------------------------------------------------------
//  Get the value for the column depending on the edit style.
//------------------------------------------------------------------

l_ColType  = UPPER(Describe(search_column + ".ColType"))
IF l_ColType = "?" THEN
   RETURN ""
END IF
IF Left(l_ColType, 3) = "DEC" THEN
   l_ColType = "NUMBER"
END IF
IF Left(l_ColType, 4) = "CHAR" THEN
   l_ColType = "STRING"
END IF

CHOOSE CASE l_ColType
   CASE "NUMBER"
      l_Return = String(GetItemNumber(i_SearchRow, search_column))
   CASE "STRING"
      l_Return = GetItemString(i_SearchRow, search_column)
   CASE "DATE"
      l_Return = String(GetItemDate(i_SearchRow, search_column))
   CASE "DATETIME"
      l_Return = String(GetItemDateTime(i_SearchRow, search_column))
   CASE "TIME"
      l_Return = String(GetItemTime(i_SearchRow, search_column))
END CHOOSE

//----------------------------------------------------------------------
//  If the code is -99999 or ^ , return an empty string from the
//  function.
//----------------------------------------------------------------------

IF IsNull(l_Return) OR l_Return = "-99999" OR l_Return = "^" THEN
   l_Return = ""
END IF

RETURN l_Return

end function

public function integer fu_wiredw (string column_name[], datawindow search_dw, string search_table[], string search_column[], transaction search_transobj);//******************************************************************
//  PO Module     : u_DW_Search
//  Subroutine    : fu_WireDW
//  Description   : Wires columns in this DataWindow to the search
//                  DataWindow.
//
//  Parameters    : STRING Column_Name -
//                     The columns in this DataWindow that are to
//                     be wired to the database table/columns.
//                  DATAWINDOW Search_DW -
//                     The search DataWindow that is to be wired to
//                     this object.
//                  STRING Search_Table  -
//                     The tables in the database that the search
//                     object should build the WHERE clause for.
//                  STRING Search_Column -
//                     The columns in the database that the search
//                     object should build the WHERE clause for.
//                  TRANSACTION Search_TransObj - 
//                     Transaction object associated with the
//                     search DataWindow.
//
//  Return Value  : 0 = valid DataWindow   -1 = invalid DataWindow
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

INTEGER l_Idx, l_Jdx, l_NumStyles, l_Return = -1
STRING  l_EditStyles[], l_Describe, l_Style, l_Other, l_ColType

//------------------------------------------------------------------
//  Make sure that we were passed a valid DataWindow to be wired.
//------------------------------------------------------------------

IF IsValid(search_dw) THEN

   //---------------------------------------------------------------
   //  Remember the DataWindow tables and columns for searching.
   //---------------------------------------------------------------

   i_ColumnName[]     = column_name[]
   i_SearchDW         = search_dw
   i_SearchTable[]    = search_table[]
   i_SearchColumnDB[] = search_column[]
   i_SearchOriginal   = i_SearchDW.Describe("datawindow.table.select")
   i_SearchTransObj   = search_transobj

   //---------------------------------------------------------------
   //  Determine the edit style for each column.
   //---------------------------------------------------------------

   i_NumSearch = UpperBound(search_column[])

   l_EditStyles[1] = "Edit.Required"
   l_EditStyles[2] = "EditMask.Required"
   l_EditStyles[3] = "DDLB.Required"
   l_EditStyles[4] = "DDDW.Required"
   l_EditStyles[5] = "CheckBox.3D"
   l_EditStyles[6] = "RadioButtons.3D"

   l_NumStyles     = UpperBound(l_EditStyles[])

   FOR l_Idx = 1 TO i_NumSearch
      i_LoadTable[l_Idx]       = ""
      i_LoadCode[l_Idx]        = ""
      i_LoadDesc[l_Idx]        = ""
      i_LoadWhere[l_Idx]       = ""
      i_LoadKeyword[l_Idx]     = ""
      i_ColumnEditStyle[l_Idx] = "UNKNOWN"
      i_ColumnDefault[l_Idx]   = ""
      i_ColumnTabOrder[l_Idx]  = Describe(i_ColumnName[l_Idx] + ".TabSequence")
      FOR l_Jdx = 1 TO l_NumStyles
         l_Describe = i_ColumnName[l_Idx] + "." + l_EditStyles[l_Jdx]
         l_Style = Describe(l_Describe)
         IF l_Style <> "?" THEN
            i_ColumnEditStyle[l_Idx] = UPPER(MID(l_EditStyles[l_Jdx], &
                                       1, POS(l_EditStyles[l_Jdx], ".") - 1))
            IF i_ColumnEditStyle[l_Idx] = "CHECKBOX" THEN
               l_Other = Describe(i_ColumnName[l_Idx] + ".CheckBox.Other")
               IF l_Other = "?" OR l_Other = "" THEN
                  l_ColType = UPPER(Describe(i_ColumnName[l_Idx] + ".ColType"))
                  IF l_ColType = "NUMBER" THEN
                     Modify(i_ColumnName[l_Idx] + ".CheckBox.Other='-99999'")
                     i_ColumnDefault[l_Idx] = "-99999"
                     SetItem(i_SearchRow, i_ColumnName[l_Idx], -99999)
                  ELSE
                     Modify(i_ColumnName[l_Idx] + ".CheckBox.Other='^'")
                     i_ColumnDefault[l_Idx] = "^"
                     SetItem(i_SearchRow, i_ColumnName[l_Idx], "^")
                  END IF
               END IF
            END IF
            EXIT
         END IF
      NEXT
   NEXT

   Enabled            = TRUE
   l_Return           = 0

END IF

RETURN l_Return
end function

public function integer fu_buildsearch (boolean search_reset);//******************************************************************
//  PO Module     : u_DW_Search
//  Subroutine    : fu_BuildSearch
//  Description   : Extends the WHERE clause of a SQL Select 
//                  with the values in this search object.
//
//  Parameters    : BOOLEAN Search_Reset -
//                     Should the SQL Select be reset to its
//                     original state before the building begins.
//
//  Return Value  : 0 = build OK   -1 = build failed
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

N_PO_DATE_STD l_DateObj
BOOLEAN  l_First, l_Date
INTEGER  l_GroupByPos, l_OrderByPos, l_Idx, l_ColumnCnt, l_CurrentCol
INTEGER  l_Pos
STRING   l_Text, l_ColType, l_Concat, l_NewSelect, l_EditStyle
STRING   l_GroupBy, l_OrderBy, l_Quotes, l_Other, l_SearchColumn
STRING   l_DateFormat, l_Operator, l_Block, l_Not, l_Lower, l_Upper
STRING   l_Value, l_CC, l_Comma, l_ColumnName, l_SearchTable

//------------------------------------------------------------------
//  Do an AcceptText so the last column before the search will be
//  validated.
//------------------------------------------------------------------

IF AcceptText() = -1 THEN
   RETURN -1
ELSE
   l_ColumnCnt = Integer(Describe("datawindow.column.count"))
   l_CurrentCol = GetColumn()
   FOR l_Idx = 1 TO l_ColumnCnt
      SetColumn(l_Idx)
      TriggerEvent(ItemChanged!)
      IF i_SearchError = c_ValFailed THEN
         RETURN -1
      END IF
   NEXT
   SetColumn(l_CurrentCol)               
END IF

//------------------------------------------------------------------
//  Reset the original SQL Select.
//------------------------------------------------------------------

IF search_reset THEN
   i_SearchDW.Modify('datawindow.table.select="' + &
                     i_SearchOriginal + '"')
END IF

l_NewSelect = i_SearchDW.Describe("datawindow.table.select")

//------------------------------------------------------------------
//  If the SQL statement for this DataWindow does not currently
//  have a WHERE clause, then we need to add one.  If it already
//  does have a WHERE clause, we will just concatenate additional
//  criteria to it.
//------------------------------------------------------------------

IF Pos(Upper(l_NewSelect), "WHERE") = 0 THEN
   l_Concat = " WHERE "
ELSE
   l_Concat = " AND "
END IF

//------------------------------------------------------------------
//  See if we have an "GROUP BY" clause and remove it from the
//  select statement for the time being.
//------------------------------------------------------------------

l_GroupBy    = ""
l_GroupByPos = Pos(Upper(l_NewSelect), "GROUP BY")
IF l_GroupByPos > 0 THEN
   l_GroupBy   = " " + Mid(l_NewSelect, l_GroupByPos)
   l_NewSelect = Mid(l_NewSelect, 1, l_GroupByPos - 1)
ELSE

   //---------------------------------------------------------------
   //  See if we have an "ORDER BY" clause and remove it from the
   //  select statement for the time being.
   //---------------------------------------------------------------

   l_OrderBy    = ""
   l_OrderByPos = Pos(Upper(l_NewSelect), "ORDER BY")
   IF l_OrderByPos > 0 THEN
      l_OrderBy   = " " + Mid(l_NewSelect, l_OrderByPos)
      l_NewSelect = Mid(l_NewSelect, 1, l_OrderByPos - 1)
   END IF
END IF

//------------------------------------------------------------------
//  Cycle through each wired column in this DataWindow looking for
//  search criteria to add to the SQL Select.
//------------------------------------------------------------------

FOR l_Idx = 1 TO i_NumSearch
   l_ColumnName   = i_ColumnName[l_Idx]
   l_SearchTable  = i_SearchTable[l_Idx]
   l_SearchColumn = i_SearchColumnDB[l_Idx]
   l_EditStyle    = i_ColumnEditStyle[l_Idx]

   l_Text         = fu_SelectCode(l_ColumnName)

   IF l_Text = "" THEN
      CONTINUE
   END IF

   //---------------------------------------------------------------
   //  Assume that it does not contain anything that needs quoting.
   //---------------------------------------------------------------

   l_Date   = FALSE
   l_Quotes = ""

   //---------------------------------------------------------------
   //  See what type of data this search column contains.
   //---------------------------------------------------------------

   l_ColType = Upper(i_SearchDW.Describe(l_SearchColumn + ".ColType"))
   IF Left(l_ColType, 3) = "DEC" THEN
      l_ColType = "NUMBER"
   END IF
   IF Left(l_ColType, 4) = "CHAR" THEN
      l_ColType = "STRING"
   END IF

   CHOOSE CASE l_ColType
      CASE "NUMBER"
      CASE "DATE", "DATETIME"

         //---------------------------------------------------------
         //  If we have a DATE type and the format is not
         //  specified in the column, then use the short format.
         //---------------------------------------------------------

         l_DateObj = CREATE n_po_date_std
         l_Date = TRUE
         l_DateFormat = i_SearchDW.Describe(l_SearchColumn + &
                                            ".Edit.Format")
         IF l_DateFormat = "" OR l_DateFormat = "?" THEN
            l_DateFormat = "m/d/yy"
         END IF

      CASE "TIME"
      CASE ELSE

         //---------------------------------------------------------
         //  We have a data type that needs to be quoted 
         //  (e.g. STRING).
         //---------------------------------------------------------

         l_Quotes = "'"

   END CHOOSE

   CHOOSE CASE l_EditStyle

      CASE "EDIT"

         //---------------------------------------------------------
         //  Parse the value that the user entered and pass each 
         //  unrecognized token through validation.
         //---------------------------------------------------------

         l_Other = ""
         l_First = TRUE

         DO WHILE l_Text <> ""

            //------------------------------------------------------
            //  Look for an "OR" command and strip it off.
            //------------------------------------------------------

            IF Pos(l_Text, "|") > 0 AND &
               (Pos(l_Text, "~~|") = 0 OR &
               Pos(l_Text, "~~|") > Pos(l_Text, "|")) THEN
               l_Block = Trim(Mid(l_Text, 1, Pos(l_Text, "|") - 1))
               l_Text  = Trim(Mid(l_Text, Pos(l_Text, "|") + 1))
               l_CC    = " OR  "

            ELSEIF Pos(Upper(l_Text), " OR ") > 0 THEN
               l_Block = Trim(Mid(l_Text, 1, Pos(Upper(l_Text), " OR ") - 1))
               l_Text  = Trim(Mid(l_Text, Pos(Upper(l_Text), " OR ") + 4))
               l_CC    = " OR  "

            //------------------------------------------------------
            //  Look for an "AND" command and strip it off.
            //------------------------------------------------------

            ELSEIF Pos(l_Text, "&") > 0 AND &
               (Pos(l_Text, "~~&") = 0 OR &
               Pos(l_Text, "~~&") > Pos(l_Text, "&")) THEN
               l_Block = Trim(Mid(l_Text, 1, Pos(l_Text, "&") - 1))
               l_Text  = Trim(Mid(l_Text, Pos(l_Text, "&") + 1))
               l_CC    = " OR "

            ELSEIF Pos(Upper(l_Text), " AND ") > 0 THEN
               l_Block = Trim(Mid(l_Text, 1, Pos(Upper(l_Text), " AND ") - 1))
               l_Text  = Trim(Mid(l_Text, Pos(Upper(l_Text), " AND ") + 5))
               l_CC    = " OR "

            //------------------------------------------------------
            //  Look for a "," command and strip it off.
            //------------------------------------------------------

            ELSEIF Pos(l_Text, ",") > 0 AND &
               (Pos(l_Text, "~~,") = 0 OR &
               Pos(l_Text, "~~,") > Pos(l_Text, ",")) THEN
               l_Block = Trim(Mid(l_Text, 1, Pos(l_Text, ",") - 1))
               l_Text  = Trim(Mid(l_Text, Pos(l_Text, ",") + 1))
               l_CC    = " OR "

            ELSE

               //---------------------------------------------------
               //  We're not sure what it is.  Could be a "NOT" or 
               //  a "TO" or a...?
               //---------------------------------------------------

               l_Block = Trim(l_Text)
               l_Text  = ""
               IF Pos(Lower(l_Block), "not ") = 1 THEN
                  l_CC = " AND "
               ELSE
                  l_CC = " OR  "
               END IF
            END IF

            //------------------------------------------------------
            //  Look for any escape characters and strip it off.  
            //  Typically they would be around the keywords &, 
            //  and, |, or.
            //------------------------------------------------------

            DO
               l_Pos = Pos(l_Block, "~~")
               IF l_Pos > 0 THEN
                  l_Block = Replace(l_Block, l_Pos, 1, "")
		         END IF
            LOOP UNTIL l_Pos = 0

            //------------------------------------------------------
            //  Look for an "NOT" command and strip it off.
            //------------------------------------------------------

            l_Not = ""
            IF Pos(Lower(l_Block), "not ") = 1 THEN
               l_Not = "NOT"
               l_Block = Mid(Trim(l_Block), 5)
               IF l_Block = "" THEN EXIT
            END IF

            //------------------------------------------------------
            //  If this is the first token then clear our 
            //  conditional operator.
            //------------------------------------------------------

            IF l_First THEN
               l_CC    = ""
               l_First = FALSE
            END IF

            //------------------------------------------------------
            //  Look for an "TO" command.
            //------------------------------------------------------

            IF Pos(Lower(l_Block), " to ") > 0 THEN

               //---------------------------------------------------
               //  We found a "TO" command.  Grab the token 
               //  preceeding the "TO" and validate it.
               //---------------------------------------------------

               l_Lower = Mid(l_Block, 1, Pos(Lower(l_Block), &
                             " to ") - 1)

               //---------------------------------------------------
               //  Grab the token that comes after the "TO" and 
               //  validate it.
               //---------------------------------------------------

               l_Upper = Trim(Mid(l_Block, Pos(Lower(l_Block), &
                              " to ") + 4))

               //------------------------------------------------------------
               //  If the data is of type DATE, then we need to 
               //  convert the DATE formats to a format that will be
               //  accepted by the database that we are connected to.
               //------------------------------------------------------------

               IF l_Date THEN
                  l_Lower = l_DateObj.fu_SetDateDB( &
                                      l_SearchTable + "." + &
                                      l_SearchColumn,       &
                                      l_Lower,              &
                                      l_DateFormat,         &
                                      " >= ",               &
                                      i_SearchTransObj)
                  l_Upper = l_DateObj.fu_SetDateDB( &
                                      l_SearchTable + "." + &
                                      l_SearchColumn,       &
                                      l_Upper,              &
                                      l_DateFormat,         &
                                      " <= ",               &
                                      i_SearchTransObj)

                  //------------------------------------------------
                  //  Convert the "TO" command to a recognizable SQL 
                  //  statement.
                  //------------------------------------------------

                  l_Other = l_Other + l_CC + l_Not + "(" + &
                                   l_Lower + " AND " + l_Upper + ")"
               ELSE

                  //------------------------------------------------
                  //  Convert the "TO" command to a recognizable SQL 
                  //  statement.
                  //------------------------------------------------

                  l_Other = l_Other + l_CC + &
                                      l_SearchTable + "." + &
                                      l_SearchColumn +  " " + &
                                      l_Not + " BETWEEN " + &
                                      l_Quotes + &
                                      l_Lower + l_Quotes + &
                                      " AND " + &
                                      l_Quotes + l_Upper + l_Quotes
               END IF
 
            ELSE

               //------------------------------------------------------
               //  Well, we don't have a "TO" command.  See if we 
               //  have some other sort of operator.
               //------------------------------------------------------

               IF Pos(l_Block, ">=") = 1 THEN
                  l_Value = Trim(Mid(l_Block, 3))
                  IF l_Not = "NOT" THEN
                     l_Operator = " < "
                  ELSE
                     l_Operator = " >= "
                  END IF
               ELSEIF Pos(l_Block, "<=") = 1 THEN
                  l_Value = Trim(Mid(l_Block, 3))
                  IF l_Not = "NOT" THEN
                     l_Operator = " > "
                  ELSE
                     l_Operator = " <= "
                  END IF
               ELSEIF Pos(l_Block, ">") = 1 THEN
                  l_Value = Trim(Mid(l_Block, 2))
                  IF l_Not = "NOT" THEN
                     l_Operator = " <= "
                  ELSE
                     l_Operator = " > "
                  END IF
               ELSEIF Pos(l_Block, "<") = 1 THEN
                  l_Value = Trim(Mid(l_Block, 2))
                  IF l_Not = "NOT" THEN
                     l_Operator = " >= "
                  ELSE
                     l_Operator = " < "
                  END IF
               ELSE
                  l_Value = l_Block
                  IF l_Not = "NOT" THEN
                     IF Pos(l_Value, "*") > 0 OR &
                        Pos(l_Value, "%") > 0 THEN
                        l_Operator = " NOT LIKE "
                     ELSE
                        l_Operator = " <> "
                     END IF
                  ELSE
                     IF Pos(l_Value, "*") > 0 OR &
                        Pos(l_Value, "%") > 0 THEN
                        l_Operator = " LIKE "
                     ELSE
                        l_Operator = " = "
                     END IF
                  END IF
               END IF

               //---------------------------------------------------
               //  If the data is of type DATE, then we need to 
               //  convert the DATE formats to a format that will 
               //  be accepted by the database that we are connected
               //  to.
               //---------------------------------------------------

               IF l_Date THEN
                  l_Value = l_DateObj.fu_SetDateDB( &
                                      l_SearchTable + "." + &
                                      l_SearchColumn,       &
                                      l_Value,              &
                                      l_DateFormat,         &
                                      l_Operator,           &
                                      i_SearchTransObj)

                  //------------------------------------------------
                  //  Convert the operator command to a recognizable
                  //  SQL statement.
                  //------------------------------------------------

                  l_Other = l_Other + l_CC + l_Value

               ELSE

                  //------------------------------------------------------
                  //  Convert the operator command to a recognizable
                  //  SQL statement.
                  //------------------------------------------------------

                  l_Other = l_Other + l_CC + l_SearchTable + "." + &
                                             l_SearchColumn + &
                                             l_Operator + &
                                             l_Quotes + &
                                             l_Value + &
                                             l_Quotes
               END IF
            END IF
         LOOP

         //---------------------------------------------------------
         //  If we found a valid where clause, concatenate it onto 
         //  the new SQL statement that we are building.
         //---------------------------------------------------------

         IF Len(l_Other) > 0 THEN
            l_NewSelect = l_NewSelect + l_Concat + "(" + l_Other + ")"
         END IF

      CASE ELSE

         //---------------------------------------------------------
         //  Add the validated value.
         //---------------------------------------------------------

         l_Other = l_Quotes + l_Text + l_Quotes

         //---------------------------------------------------------
         //  Add the snippet of SQL generated by the this object to
         //  our new SQL statement.
         //---------------------------------------------------------

         IF Len(l_Other) > 0 THEN
            l_NewSelect = l_NewSelect + l_Concat + "(" + &
                                        l_SearchTable + "." + &
                                        l_SearchColumn + &
                                        " IN (" + l_Other + "))"
         END IF

   END CHOOSE

   l_Concat = " AND "

NEXT

//------------------------------------------------------------------
//  Stuff the parameter with the completed SQL statement.
//------------------------------------------------------------------

IF IsValid(l_DateObj) THEN
   DESTROY l_DateObj
END IF

l_NewSelect = "datawindow.table.select='" + &
              w_POManager_STD.MB.fu_QuoteChar(l_NewSelect, "'") + &
              w_POManager_STD.MB.fu_QuoteChar(l_GroupBy, "'")   + &
              w_POManager_STD.MB.fu_QuoteChar(l_OrderBy, "'")   + "'"
i_SearchDW.Modify(l_NewSelect)

RETURN 0
end function

public function integer fu_loadcode (string search_column, string table_name, string column_code, string column_desc, string where_clause, string all_keyword);//******************************************************************
//  PO Module     : u_DW_Search
//  Function      : fu_LoadCode
//  Description   : Load the columns code table using codes and 
//                  descriptions from the database.
//
//  Parameters    : STRING      Search_Column -
//                     The column in this DataWindow to load the
//                     code table for.
//                  STRING      Table_Name    - 
//                     Table from where the column with the code
//                     values resides.
//                  STRING      Column_Code   - 
//                     Column name with the code values.
//                  STRING      Column_Desc   - 
//                     Column name with the code descriptions.
//                  STRING      Where_Clause  - 
//                     WHERE cause statement to restrict the code 
//                     values.
//                  STRING      All_Keyword  - 
//                     Keyword to denote select all values (e.g. 
//                     "(All)").
//
//  Return Value  : 0 = OK, -1 = Error
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN          l_IsNumber
INTEGER          l_ColNbr, l_Idx, l_Jdx
LONG             l_Error, l_Nbr
STRING           l_SQLString, l_ErrStr, l_Code, l_Desc, l_ColType
STRING           l_Describe, l_Modify, l_Attrib, l_ErrorStrings[1]
STRING           l_EditStyle, l_ColStr
DATAWINDOWCHILD  l_DWC

STRING           c_ColAttrib[]  =            &
                    { ".BackGround.Color",   & 
                      ".Background.Mode",    &
                      ".Color",              &
                      ".Font.CharSet",       &
                      ".Font.Face",          &
                      ".Font.Family",        &
                      ".Font.Height",        &
                      ".Font.Italic",        &
                      ".Font.Pitch",         &
                      ".Font.Strikethrough", &
                      ".Font.Underline",     &
                      ".Font.Weight",        &
                      ".Font.Width",         &
                      ".Height",             &
                      ".Width" }

//------------------------------------------------------------------
//  Determine the edit style of the column.
//------------------------------------------------------------------

FOR l_Idx = 1 TO i_NumSearch
   IF i_ColumnName[l_Idx]  = search_column THEN
      l_EditStyle          = i_ColumnEditStyle[l_Idx]
      i_LoadTable[l_Idx]   = table_name
      i_LoadCode[l_Idx]    = column_code
      i_LoadDesc[l_Idx]    = column_desc
      i_LoadWhere[l_Idx]   = where_clause
      i_LoadKeyword[l_Idx] = all_keyword
      EXIT
   END IF
NEXT

//------------------------------------------------------------------
//  Determine the edit style of the column.
//------------------------------------------------------------------

CHOOSE CASE l_EditStyle

   CASE "DDDW"

      //------------------------------------------------------------
      //  Build the SQL SELECT statement.
      //------------------------------------------------------------

      l_SQLString = "SELECT " + Column_Desc + ", " + &
                    Column_Code + " FROM " + Table_Name

      IF Trim(Where_Clause) <> "" THEN
         l_SQLString = l_SQLString + " WHERE " + Where_Clause
      END IF

      //------------------------------------------------------------
      //  Get the child DataWindow.  If column is not a DDDW column,
      //  then return with error.  Otherwise, we need to set the
      //  Select statement in the drop down DataWindow.
      //------------------------------------------------------------

      l_Error = GetChild(search_column, l_DWC)

      IF l_Error <> 1 THEN
         w_POManager_Std.MB.fu_MessageBox           &
            (w_POManager_Std.MB.c_MBI_DDDWChildFindError, &
            0, w_POManager_Std.MB.i_MB_Numbers[],  &
            0, w_POManager_Std.MB.i_MB_Strings[])
         RETURN -1
      END IF

      //------------------------------------------------------------
      //  Set the transaction object.
      //------------------------------------------------------------

      l_Error = l_DWC.SetTransObject(i_SearchTransObj)
      
      IF l_Error <> 1 THEN
         w_POManager_Std.MB.fu_MessageBox           &
            (w_POManager_Std.MB.c_MBI_DDDWTransactionError, &
            0, w_POManager_Std.MB.i_MB_Numbers[],  &
            0, w_POManager_Std.MB.i_MB_Strings[])
         RETURN -1
      END IF

      //------------------------------------------------------------
      //  Assign the Select statement to the drop down DataWindow.
      //------------------------------------------------------------

      l_Modify = "DataWindow.Table.Select='" + &
                 w_POManager_STD.MB.fu_QuoteChar(l_SQLString, "'")  + "'"
      l_ErrStr = l_DWC.Modify(l_Modify)

      IF l_ErrStr <> "" THEN
         w_POManager_Std.MB.fu_MessageBox           &
            (w_POManager_Std.MB.c_MBI_DDDWModifySQLError, &
            0, w_POManager_Std.MB.i_MB_Numbers[],  &
            0, w_POManager_Std.MB.i_MB_Strings[])
         RETURN -1
      END IF

      //------------------------------------------------------------
      //  Retrieve the data for the child DataWindow.
      //------------------------------------------------------------

      l_Error = l_DWC.Retrieve()

      IF l_Error < 0 THEN
         w_POManager_Std.MB.fu_MessageBox           &
            (w_POManager_Std.MB.c_MBI_DDDWRetrieveError, &
            0, w_POManager_Std.MB.i_MB_Numbers[],  &
            0, w_POManager_Std.MB.i_MB_Strings[])
         RETURN -1
      END IF

      //------------------------------------------------------------
      //  If an ALL keyword is given, insert it into the first row 
      //  on the child DataWindow.
      //------------------------------------------------------------

      IF Trim(all_keyword) <> "" THEN
         IF l_DWC.InsertRow(1) > 0 THEN
            l_Attrib = l_DWC.Describe("#1.ColType")
            IF Upper(l_Attrib) <> "NUMBER" THEN
               l_DWC.SetItem(1, 1, all_keyword)
            END IF

            l_Attrib = l_DWC.Describe("#2.ColType")
            IF Upper(l_Attrib) = "NUMBER" THEN
               l_DWC.SetItem(1, 2, -99999)
            ELSE
               l_DWC.SetItem(1, 2, "^")
            END IF
         END IF
      END IF

      //------------------------------------------------------------
      //  Set the attributes of the column to be the same as the
      //  search column.
      //------------------------------------------------------------

      l_Jdx = UpperBound(c_ColAttrib[])
      FOR l_Idx = 1 TO l_Jdx
         l_Describe = search_column + c_ColAttrib[l_Idx]
         l_Attrib   = Describe(l_Describe)

         IF c_ColAttrib[l_Idx] = ".Font.Face" THEN
            IF l_Attrib = "?" OR l_Attrib = "!" THEN
               l_Attrib = "Arial"
            END IF
            l_Attrib = "'" + l_Attrib + "'"
         ELSE
            IF l_Attrib = "?" OR l_Attrib = "!" THEN
               l_Attrib = "0"
            END IF
         END IF

         l_Modify = "#1" + c_ColAttrib[l_Idx] + "=" + l_Attrib
         l_ErrStr = l_DWC.Modify(l_Modify)
      NEXT

   CASE "DDLB", "EDITMASK"

      //------------------------------------------------------------
      //  Determine the columns data type.
      //------------------------------------------------------------

      l_ColType = Describe(search_column + ".ColType")
      CHOOSE CASE UPPER(l_ColType)
         CASE "NUMBER"
            l_IsNumber = TRUE
         CASE ELSE
            l_IsNumber = FALSE
      END CHOOSE

      //------------------------------------------------------------
      //  Declare a dynamic SQL cursor to hold the data while 
      //  loading the code table.
      //------------------------------------------------------------

      i_SearchTransObj.SQLCode = 0
      DECLARE DDLB_Cursor DYNAMIC CURSOR FOR SQLSA ;

      IF i_SearchTransObj.SQLCode <> 0 THEN
         l_ErrorStrings[1] = i_SearchTransObj.SQLErrText
         w_POManager_Std.MB.fu_MessageBox( &
            w_POManager_Std.MB.c_MBI_CursorDeclareError, &
            0, w_POManager_Std.MB.i_MB_Numbers[], &
            1, l_ErrorStrings[])
         RETURN -1
      END IF

      //------------------------------------------------------------
      //  Build the SQL SELECT statement to get the data.
      //------------------------------------------------------------

      l_SQLString = "SELECT " + Column_Code + ", " + &
                    Column_Desc + " FROM " + Table_Name
      IF Trim(Where_Clause) <> "" THEN
         l_SQLString = l_SQLString + " WHERE " + Where_Clause
      END IF

      //------------------------------------------------------------
      //  Prepare the SQL statement for execution.
      //------------------------------------------------------------

      PREPARE SQLSA FROM :l_SQLString USING i_SearchTransObj ;

      IF i_SearchTransObj.SQLCode <> 0 THEN
         l_ErrorStrings[1] = i_SearchTransObj.SQLErrText
         w_POManager_Std.MB.fu_MessageBox( &
             w_POManager_Std.MB.c_MBI_CursorPrepareError, &
             0, w_POManager_Std.MB.i_MB_Numbers[], &
             1, l_ErrorStrings[])
         RETURN -1
      END IF

      //------------------------------------------------------------
      //  Open the dynamic cursor.
      //------------------------------------------------------------

      OPEN DYNAMIC DDLB_Cursor ;

      IF i_SearchTransObj.SQLCode <> 0 THEN
         l_ErrorStrings[1] = i_SearchTransObj.SQLErrText
         w_POManager_Std.MB.fu_MessageBox( &
             w_POManager_Std.MB.c_MBI_CursorOpenError, &
             0, w_POManager_Std.MB.i_MB_Numbers[], &
             1, l_ErrorStrings[])
         RETURN -1
      END IF

      //------------------------------------------------------------
      //  If a keyword has been given to select all, add it as a 
      //  selection to the code table.
      //------------------------------------------------------------

      l_Idx = 0
      IF TRIM(all_keyword) <> "" THEN
         l_Idx++
         IF l_IsNumber THEN
            l_Code = String(-99999)
         ELSE
            l_Code = "^"
         END IF
         l_Desc = all_keyword
         l_Desc = l_Desc + "~t" + l_Code
         SetValue(search_column, l_Idx, l_Desc)
      END IF

      //------------------------------------------------------------------
      //  For each row in the cursor, add the description and code 
      //  to the code table.
      //------------------------------------------------------------------

      IF NOT l_IsNumber THEN
         DO WHILE i_SearchTransObj.SQLCode = 0
            FETCH DDLB_Cursor INTO :l_Code, :l_Desc ;
            IF i_SearchTransObj.SQLCode = 0 THEN
               IF IsNull(l_Desc) THEN
                  l_Desc = l_Code
               END IF
               l_Idx++
               IF Column_Code <> Column_Desc THEN
                  l_Desc = l_Desc + "~t" + l_Code
               END IF
               SetValue(search_column, l_Idx, l_Desc)
            ELSE
               IF i_SearchTransObj.SQLCode <> 100 THEN
                  l_ErrorStrings[1] = i_SearchTransObj.SQLErrText
                  w_POManager_Std.MB.fu_MessageBox( &
                      w_POManager_Std.MB.c_MBI_CursorFetchError, &
                      0, w_POManager_Std.MB.i_MB_Numbers[], &
                      1, l_ErrorStrings[])
                  RETURN -1
               END IF
            END IF
         LOOP
      ELSE
         DO WHILE i_SearchTransObj.SQLCode = 0
            FETCH DDLB_Cursor INTO :l_Nbr, :l_Desc ;
            IF i_SearchTransObj.SQLCode = 0 THEN
               IF IsNull(l_Desc) THEN
                  l_Desc = String(l_Nbr)
               END IF
               l_Idx++
               IF Column_Code <> Column_Desc THEN
                  l_Desc = l_Desc + "~t" + String(l_Nbr)
               END IF
               SetValue(search_column, l_Idx, l_Desc)
            ELSE
               IF i_SearchTransObj.SQLCode <> 100 THEN
                  l_ErrorStrings[1] = i_SearchTransObj.SQLErrText
                  w_POManager_Std.MB.fu_MessageBox( &
                      w_POManager_Std.MB.c_MBI_CursorFetchError, &
                      0, w_POManager_Std.MB.i_MB_Numbers[], &
                      1, l_ErrorStrings[])
                  RETURN -1
               END IF
            END IF
         LOOP
      END IF

      //------------------------------------------------------------
      //  Close the dynamic cursor.
      //------------------------------------------------------------

      CLOSE DDLB_Cursor ;

      IF i_SearchTransObj.SQLCode <> 0 THEN
         l_ErrorStrings[1] = i_SearchTransObj.SQLErrText
         w_POManager_Std.MB.fu_MessageBox( &
             w_POManager_Std.MB.c_MBI_CursorCloseError, &
             0, w_POManager_Std.MB.i_MB_Numbers[], &
             1, l_ErrorStrings[])
         RETURN -1
      END IF

   CASE ELSE

      RETURN -1

END CHOOSE

RETURN 0
end function

public subroutine fu_unwiredw ();//******************************************************************
//  PO Module     : u_DW_Search
//  Subroutine    : fu_UnwireDW
//  Description   : Un-wires this DataWindow from a the search
//                  DataWindow.
//
//  Parameters    : (None)
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

DATAWINDOW  l_NullDW
INTEGER     l_Idx

SetNull(l_NullDW)

//------------------------------------------------------------------
//  Make sure we have a valid column to be unwired.
//------------------------------------------------------------------

FOR l_Idx = 1 TO i_NumSearch
   i_SearchColumnDB[l_Idx]  = ""
   i_SearchTable[l_Idx]     = ""
   i_LoadTable[l_Idx]       = ""
   i_LoadCode[l_Idx]        = ""
   i_LoadDesc[l_Idx]        = ""
   i_LoadWhere[l_Idx]       = ""
   i_LoadKeyword[l_Idx]     = ""
   i_ColumnName[l_Idx]      = ""
   i_ColumnTabOrder[l_Idx]  = ""
   i_ColumnEditStyle[l_Idx] = ""
   i_ColumnDefault[l_Idx]   = ""
NEXT

i_NumSearch = 0
IF i_NumSearch = 0 THEN

   //---------------------------------------------------------------
   //  Indicate that none of the columns in this DataWindow
   //  are wired to the search DataWindow.  Disable this object 
   //  since it is not wired to a DataWindow.
   //---------------------------------------------------------------

   i_SearchDW = l_NullDW
   Enabled    = FALSE
END IF

RETURN 
end subroutine

on itemchanged;//******************************************************************
//  PO Module     : u_DW_Search
//  Event         : ItemChanged
//  Description   : This event is used to trigger validation
//                  checking when a field has changed.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_SetActionCode, l_NumStyles, l_Idx
STRING   l_EditStyles[], l_Describe, l_Required

//------------------------------------------------------------------
//  If the ignore validation flag is set, return.
//------------------------------------------------------------------

IF i_IgnoreVal THEN
   GoTo Finished
END IF

//------------------------------------------------------------------
//  If validation is to be done, find the current row and column
//  that needs to be validated.
//------------------------------------------------------------------

i_SearchRow    = GetRow()
i_SearchColumn = GetColumnName()
i_SearchValue  = GetText()

IF i_SearchRow = 0 OR i_SearchColumn = "" THEN
   GoTo Finished
END IF

//------------------------------------------------------------------
//  Set the validation information that can be used by the developer
//  to code the po_Validate event.
//------------------------------------------------------------------

i_SearchValMsg = Describe(i_SearchColumn + ".ValidationMsg")
   IF i_SearchValMsg = "?" THEN
      i_SearchValMsg = ""
   END IF

//------------------------------------------------------------------
//  We can not find out what edit style the column is from
//  PowerBuilder.  Therefore, we have to cycle through all the
//  possible types seeing if we get a valid response.  Set up
//  an array containing all known styles.
//------------------------------------------------------------------

l_EditStyles[1] = ".Edit"
l_EditStyles[2] = ".EditMask"
l_EditStyles[3] = ".DDLB"
l_EditStyles[4] = ".DDDW"
l_NumStyles     = UpperBound(l_EditStyles[])

//------------------------------------------------------------------
//  For each edit style that we know about, see if this column
//  will respond to a DESCRIBE().
//------------------------------------------------------------------

i_SearchRequiredError = FALSE
FOR l_Idx = 1 TO l_NumStyles
   l_Describe = i_SearchColumn + l_EditStyles[l_Idx] + ".Required"
   l_Required = Upper(Describe(l_Describe))
   IF l_Required <> "?" THEN
      i_SearchRequired  = (l_Required  = "YES")
      IF i_SearchValue = "" AND i_SearchRequired THEN
         i_SearchRequiredError = TRUE
         EXIT
      END IF
   END IF
NEXT

//------------------------------------------------------------------
//  If l_Required is "?", then this must be a CheckBox or
//  RadioButton column.
//------------------------------------------------------------------

IF l_Required = "?" THEN
   i_SearchRequired  = FALSE
END IF

//------------------------------------------------------------------
//  Initialize i_SearchError to indicate that validation processing
//  has not been done.  Initialize i_DisplayRequired to indicate
//  that required field errors should be reported.  If the
//  developer does not want PowerClass to do required field
//  reporting, they can set this flag to FALSE.
//------------------------------------------------------------------

i_SearchError     = c_ValNotProcessed
i_DisplayRequired = TRUE

//------------------------------------------------------------------
//  po_Validate performs validation on column by column basis.
//  This event must be coded by the developer.
//
//  Validation routines are expected to do their own error
//  display.
//------------------------------------------------------------------

TriggerEvent("po_Validate")

//------------------------------------------------------------------
//  We need to display the required field error if:
//     a) If we have a required field error AND
//     b) The developer's routine did not catch or process
//        it AND
//     c) The developer said it was Ok to display it.
//------------------------------------------------------------------

IF i_SearchRequiredError THEN
   IF i_SearchError = c_ValNotProcessed THEN
      IF i_DisplayRequired THEN

         //---------------------------------------------------------
         //  Display the required field message.
         //---------------------------------------------------------

         w_POManager_Std.MB.fu_MessageBox           &
            (w_POManager_Std.MB.c_MBI_RequiredField, &
            0, w_POManager_Std.MB.i_MB_Numbers[],  &
            0, w_POManager_Std.MB.i_MB_Strings[])
 

         //---------------------------------------------------------
         //  Since this is a required field, we have an error.
         //---------------------------------------------------------

         i_SearchError = c_ValFailed
      END IF
   END IF
END IF

//------------------------------------------------------------------
//  Indictate that the DataWindow has been through validation.
//  Since ItemError! may be called immediately, i_ItemValidated
//  tells PowerObjects not to re-try the validation process (which
//  would display another validation error message box).
//------------------------------------------------------------------

i_ItemValidated = TRUE

Finished:

//------------------------------------------------------------------
//  If we were not triggered by ItemError and the developer did
//  not process the error, then indicate success.
//------------------------------------------------------------------

IF i_InItemError = 0 THEN
   IF i_SearchError = c_ValNotProcessed THEN
      i_SearchError = c_ValOk
   END IF
END IF

//------------------------------------------------------------------
//  The validation return code is interpreted as follows:
//     c_ValOk (0):
//        The the data was accepted.
//     c_ValFailed (1):
//        The data should be rejected and focus should be returned
//        to the field in error.
//     c_ValFixed (2):
//        The text was reformatted and placed back into the field
//        using SetItem().  The text now passes validation.
//     c_ValNotProcessed (-9):
//        The field was not validated by the developer's
//        po_Validate event.  Let PowerBuilder handle the
//        validation.
//------------------------------------------------------------------

l_SetActionCode = i_SearchError
IF l_SetActionCode = c_ValNotProcessed THEN
   l_SetActionCode = c_ValOk
END IF

//------------------------------------------------------------------
//  Set the flag to let the developer know if they should execute
//  their extended code at this time.
//------------------------------------------------------------------

SetActionCode(l_SetActionCode)
end on

on constructor;//******************************************************************
//  PO Module     : u_DW_Search
//  Event         : Constructor
//  Description   : Initializes the Search Object.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

//------------------------------------------------------------------
//  Identify this object as a PowerObject Search.
//------------------------------------------------------------------

Tag = "PowerObject Search DW"

//------------------------------------------------------------------
//  Insert an empty row.
//------------------------------------------------------------------

InsertRow(0)
end on

on itemerror;//******************************************************************
//  PO Module     : u_DW_Search
//  Event         : ItemError
//  Description   : This event is used to disable the display
//                  of the generic PowerBuilder validation error
//                  message.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER  l_SetActionCode
STRING   l_ErrorStrings[]

i_InItemError = 1

//------------------------------------------------------------------
//  If the ignore validation flag is set, return.
//------------------------------------------------------------------

IF i_IgnoreVal THEN
   RETURN
END IF

IF i_IgnoreVal THEN

   //---------------------------------------------------------------
   //  Don't display the error message from Powerbuilder, but
   //  allow focus to change.
   //---------------------------------------------------------------

   l_SetActionCode = 3

   GOTO Finished
END IF

//------------------------------------------------------------------
//  Trigger the ItemChanged! event to initiate the validation
//  process only if it has not already run (i_ItemValidated tells
//  if it has already been run).  ItemChanged! can be triggered
//  before this event by ACCEPTTEXT().
//------------------------------------------------------------------

IF NOT i_ItemValidated THEN

   //---------------------------------------------------------------
   //  See if we are worried about required field checking every
   //  time the user moves.  If not, then see if ItemError was
   //  triggered because of a required field error.  If so, then
   //  we don't need to do any more processing for ItemError.
   //---------------------------------------------------------------

   TriggerEvent(ItemChanged!)

   //---------------------------------------------------------------
   //  Unless the developer specified that validation error
   //  messages were not to be displayed, set the action code
   //  appropriately.
   //---------------------------------------------------------------

   CHOOSE CASE i_SearchError
      CASE c_ValOk

         //---------------------------------------------------------
         //  The developer told us that the value is good.
         //  Tell PowerBuilder to accept it and move.
         //---------------------------------------------------------

         l_SetActionCode = 2

      CASE c_ValFixed

         //---------------------------------------------------------
         //  The value was bad, but it was fixed using
         //  SETITEM().  Therefore, we don't want to accept
         //  the user's text over the correction.  Just
         //  reject the user's value and move.
         //---------------------------------------------------------

         l_SetActionCode = 3

      CASE c_ValFailed

         //---------------------------------------------------------
         //  The value failed validation and the developer was
         //  unable to fix it.  Reject the value and stay put.
         //---------------------------------------------------------

         l_SetActionCode = 1

      CASE c_ValNotProcessed

         //---------------------------------------------------------
         //  The developer did not do any processing in the
         //  po_Validate event.  See if ItemError! was
         //  triggered because of a required field error.  If
         //  it was, we don't want PowerBuilder to display an
         //  error message.
         //---------------------------------------------------------

         IF i_SearchRequiredError THEN
            i_SearchError   = c_ValOk
            l_SetActionCode = 3
         ELSE

            //------------------------------------------------------
            //  PowerBuilder thinks something is wrong and we
            //  now know that it is not a required field error.
            //  If there is a validation error message in the
            //  DataWindow call Display_DW_Val_Error() to
            //  display it.
            //------------------------------------------------------

            IF Len(Trim(i_SearchValMsg)) > 0 THEN
               l_ErrorStrings[1] = i_SearchValMsg
               w_POManager_Std.MB.fu_MessageBox           &
                  (w_POManager_Std.MB.c_MBI_OKDevValError, &
                  0, w_POManager_Std.MB.i_MB_Numbers[],  &
                  1, l_ErrorStrings[])
               i_SearchError   = c_ValFailed
               l_SetActionCode = 1
            ELSE

               //---------------------------------------------------
               //  Nothing else to do but let PowerBuilder
               //  display the message.
               //---------------------------------------------------

               i_SearchError   = c_ValFailed
               l_SetActionCode = 0
            END IF
         END IF
      CASE ELSE

         //---------------------------------------------------------
         //  If we get to here, then the developer gave us back
         //  a bad validation return.
         //---------------------------------------------------------

         i_SearchError   = c_ValFailed
         l_SetActionCode = 0
   END CHOOSE

ELSE

   //---------------------------------------------------------------
   //  The developer said that validation errors were not to
   //  be displayed, but there is an error with the data.
   //  Reject the value and don't move.
   //---------------------------------------------------------------

   l_SetActionCode = 1
END IF

//------------------------------------------------------------------
//  Clear the validation state flag indicating that it is Ok to
//  do validation checking.
//------------------------------------------------------------------

i_ItemValidated = FALSE

Finished:

i_InItemError = 0
SetActionCode(l_SetActionCode)
end on

on itemfocuschanged;//******************************************************************
//  PO Module     : u_DW_Search
//  Event         : ItemFocusChanged
//  Description   : This event is used to indicate when a focus
//                  changes between items in a DataWindow.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

//------------------------------------------------------------------
//  If validation processing is off, we do not want to do
//  anything.
//------------------------------------------------------------------

IF i_IgnoreVal THEN
   GOTO Finished
END IF

//------------------------------------------------------------------
//  Clear the validation state flag indicating the validation
//  needs to be run.
//------------------------------------------------------------------

i_ItemValidated = FALSE

Finished:

end on

on editchanged;//******************************************************************
//  PO Module     : u_DW_Search
//  Event         : EditChanged
//  Description   : This event is used to indicate when a change
//                  has been made to a field in the DataWindow and
//                  validation needs to be performed.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

//------------------------------------------------------------------
//  If the ignore validation flag is set, return.
//------------------------------------------------------------------

IF i_IgnoreVal THEN
   RETURN
END IF

//------------------------------------------------------------------
//  This flag indicates that the DataWindow has been modified.
//------------------------------------------------------------------

i_SearchModified = TRUE

end on

