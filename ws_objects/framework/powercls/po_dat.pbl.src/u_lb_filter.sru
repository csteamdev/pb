﻿$PBExportHeader$u_lb_filter.sru
$PBExportComments$User object that uses a list box as a filter object
forward
global type u_lb_filter from listbox
end type
end forward

global type u_lb_filter from listbox
int X=5
int Y=5
int Width=801
int Height=401
int TabOrder=1
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean MultiSelect=true
long BackColor=16777215
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
event po_valempty pbm_custom74
event po_validate pbm_custom75
end type
global u_lb_filter u_lb_filter

type variables
DATAWINDOW	i_FilterDW

TRANSACTION	i_FilterTransObj
STRING		i_FilterColumn
STRING		i_FilterValue
STRING		i_FilterOriginal

INTEGER		i_FilterError

STRING		i_LoadTable
STRING		i_LoadCode
STRING		i_LoadDesc
STRING		i_LoadWhere
STRING		i_LoadKeyword

INTEGER		c_ValOK		= 0
INTEGER		c_ValFailed	= 1

end variables

forward prototypes
public function integer fu_selectcode (ref string codes[])
public function integer fu_setcode (string default_code[])
public function integer fu_loadcode (string table_name, string column_code, string column_desc, string where_clause, string all_keyword)
public function integer fu_refreshcode ()
public function integer fu_wiredw (datawindow filter_dw, string filter_column, transaction filter_transobj)
public function integer fu_buildfilter (boolean filter_reset)
public subroutine fu_unwiredw ()
end prototypes

on po_valempty;////******************************************************************
////  PO Module     : u_LB_Filter
////  Event         : po_ValEmpty
////  Description   : Provides the opportunity for the developer to
////                  generate errors when the user has not made a
////                  selection in the LB object.
////
////  Return Value  : c_ValOK     = OK
////                  c_ValFailed = Error, criteria is required.
////
////  Change History:
////
////  Date     Person     Description of Change
////  -------- ---------- --------------------------------------------
////
////******************************************************************
////  Copyright ServerLogic 1994-1995.  All Rights Reserved.
////******************************************************************
//
////-------------------------------------------------------------------
////  Set the i_FilterError variable to c_ValFailed to indicate that
////  this filter object is required.
////-------------------------------------------------------------------
//
////SetFocus()
////MessageBox("Filter Object Error", &
////           "This is a required filter field", &
////           Exclamation!, Ok!)
////i_FilterError = c_ValFailed
end on

on po_validate;////******************************************************************
////  PO Module     : u_LB_Filter
////  Event         : po_Validate
////  Description   : Provides the opportunity for the developer to
////                  write code to validate the fields in this
////                  object.
////
////  Return Value  : INTEGER i_FilterError 
////
////                  If the developer codes the validation 
////                  testing, then the developer should set 
////                  this variable to one of the following
////                  validation return codes:
////
////                  c_ValOK     = The validation test passed and 
////                                the data is to be accepted.
////                  c_ValFailed = The validation test failed and 
////                                the data is to be rejected.  
////                                Do not allow the user to move 
////                                off of this field.
////
////  Change History:
////
////  Date     Person     Description of Change
////  -------- ---------- --------------------------------------------
////
////******************************************************************
////  Copyright ServerLogic 1992-1995.  All Rights Reserved.
////******************************************************************
//
////------------------------------------------------------------------
////  The following values are set for the developer before this
////  event is triggered:
////
////      STRING i_FilterValue -
////            The input the user has made.
////------------------------------------------------------------------
//
////i_FilterError = f_PO_ValMaxLen(i_FilterValue, 10, TRUE)
end on

public function integer fu_selectcode (ref string codes[]);//******************************************************************
//  PO Module     : u_LB_Filter
//  Function      : fu_SelectCode
//  Description   : Select the code associated with the description
//                  from a list box.
//
//  Parameters    : STRING Codes[] - 
//                     Array of codes corresponding to the 
//                     currently selected items in the list box.
//
//  Return Value  : INTEGER  - Number of items selected.
//
//  Change History:  
//    
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

INTEGER l_Cnt, l_Idx, l_TotalItems
STRING  l_Value, l_NullArray[]

//------------------------------------------------------------------
//  Pull the code from each item selected in the list box.
//------------------------------------------------------------------

l_Cnt = 0
l_TotalItems = TotalItems()
FOR l_Idx = 1 TO l_TotalItems
   IF State(l_Idx) = 1 THEN
      l_Cnt        = l_Cnt + 1
      l_Value      = Text(l_Idx)
      IF Pos(l_Value, "||") > 0 THEN
     	   Codes[l_Cnt] = Mid(l_Value, Pos(l_Value, "||") + 2)
     	   IF Codes[l_Cnt] = "NULL"   OR &
            Codes[l_Cnt] = "-99999" OR &
            Codes[l_Cnt] = "(ALL)" THEN
            l_Cnt   = 0
        	   Codes[] = l_NullArray[]
            EXIT
         END IF
      ELSE
         Codes[l_Cnt] = l_Value
      END IF
   END IF
NEXT

//------------------------------------------------------------------
//  Do any cleanup and exit the function.
//------------------------------------------------------------------

RETURN l_Cnt
end function

public function integer fu_setcode (string default_code[]);//******************************************************************
//  PO Module     : u_LB_Filter
//  Subroutine    : fu_SetCode
//  Description   : Sets a default value for this object.
//
//  Parameters    : STRING Default_Code -
//                     The values to set for this object.
//
//  Return Value  : 0 = set OK   -1 = set failed
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

INTEGER l_Return = -1
LONG    l_TotalCodes, l_TotalItems, l_Idx, l_Jdx
STRING  l_Value, l_Code

l_TotalCodes = UpperBound(default_code[])
l_TotalItems = TotalItems()

FOR l_Idx = 1 TO l_TotalCodes
   l_Code = TRIM(default_code[l_Idx])
   FOR l_Jdx = 1 TO l_TotalItems
      l_Value = Text(l_Jdx)
      l_Value = MID(l_Value, Pos(l_Value, "||") + 2)
      IF l_Value = l_Code THEN
         SetState(l_Jdx, TRUE)
         l_Return = 0
         EXIT
      END IF
   NEXT
NEXT 

RETURN l_Return
end function

public function integer fu_loadcode (string table_name, string column_code, string column_desc, string where_clause, string all_keyword);//******************************************************************
//  PO Module     : u_LB_Filter
//  Function      : fu_LoadCode
//  Description   : Load a list box using codes and descriptions
//                  from the database.
//
//  Parameters    : STRING      Table_Name   - 
//                     Table from where the column with the code
//                     values resides.
//                  STRING      Column_Code  - 
//                     Column name with the code values.
//                  STRING      Column_Desc  - 
//                     Column name with the code descriptions.
//                  STRING      Where_Clause - 
//                     WHERE cause statement to restrict the code 
//                     values.
//                  STRING      All_Keyword  - 
//                     Keyword to denote select all values (e.g. 
//                     "(All)").
//
//  Return Value  : 0 = OK, -1 = Error
//
//  Change History:  
//    
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN l_IsNumber
LONG    l_Nbr
STRING  l_SQLString, l_Code, l_Desc

i_LoadTable = table_name
i_LoadCode = column_code
i_LoadDesc = column_desc
i_LoadWhere = where_clause
i_LoadKeyword = all_keyword

//------------------------------------------------------------------
//  Declare a dynamic SQL cursor to hold the data while loading the 
//  list box.
//------------------------------------------------------------------

i_FilterTransObj.SQLCode = 0
DECLARE LB_Cursor DYNAMIC CURSOR FOR SQLSA ;

IF i_FilterTransObj.SQLCode <> 0 THEN
   RETURN -1
END IF

//------------------------------------------------------------------
//  Build the SQL SELECT statement to get the data.
//------------------------------------------------------------------

l_SQLString = "SELECT " + i_LoadCode + ", " + &
              i_LoadDesc + " FROM " + i_LoadTable
IF Trim(i_LoadWhere) <> "" THEN
   l_SQLString = l_SQLString + " WHERE " + i_LoadWhere
END IF

//------------------------------------------------------------------
//  Prepare the SQL statement for execution.
//------------------------------------------------------------------

PREPARE SQLSA FROM :l_SQLString USING i_FilterTransObj;

IF i_FilterTransObj.SQLCode <> 0 THEN
   RETURN -1
END IF

//------------------------------------------------------------------
//  Open the dynamic cursor.
//------------------------------------------------------------------

OPEN DYNAMIC LB_Cursor ;

IF i_FilterTransObj.SQLCode <> 0 THEN
   RETURN -1
END IF

//------------------------------------------------------------------
//  If a keyword has been given to select all, add it as a selection
//  to the list box.
//------------------------------------------------------------------

IF i_LoadKeyword <> "" THEN
   l_Code = "(ALL)"
   l_Desc = i_LoadKeyword
   l_Desc = String(l_Desc, Fill("@",80)) + "||" + l_Code
   AddItem(l_Desc)
END IF

//------------------------------------------------------------------
//  Determine if the code column is a number or a string.
//------------------------------------------------------------------

FETCH LB_Cursor INTO :l_Code, :l_Desc ;

IF i_FilterTransObj.SQLCode = 0 OR &
   i_FilterTransObj.SQLCode = 100 THEN
   l_IsNumber = FALSE
ELSE
   l_IsNumber = TRUE
END IF

CLOSE LB_Cursor ;
OPEN DYNAMIC LB_Cursor ;

//------------------------------------------------------------------
//  For each row in the cursor, add the description and code to the
//  list box.
//------------------------------------------------------------------

IF l_IsNumber THEN
   DO WHILE i_FilterTransObj.SQLCode = 0
      FETCH LB_Cursor INTO :l_Nbr, :l_Desc ;
      IF i_FilterTransObj.SQLCode = 0 THEN
         IF IsNull(l_Desc) THEN
  		      l_Nbr = -99999
  	      END IF
         IF i_LoadCode <> i_LoadDesc THEN
            l_Desc = String(l_Desc, Fill("@",80)) + &
                     "||" + String(l_Nbr)
         END IF
         AddItem(l_Desc)
      ELSE
         IF i_FilterTransObj.SQLCode <> 100 THEN
            RETURN -1
         END IF
      END IF
   LOOP
ELSE
   DO WHILE i_FilterTransObj.SQLCode = 0
      FETCH LB_Cursor INTO :l_Code, :l_Desc ;
      IF i_FilterTransObj.SQLCode = 0 THEN
         IF IsNull(l_Desc) THEN
  		      l_Code = "NULL"
  	      END IF
         IF i_LoadCode <> i_LoadDesc THEN
	         l_Desc = String(l_Desc, Fill("@",80)) + &
                     "||" + l_Code
         END IF
			IF l_Code <> "NULL" THEN
	         AddItem(l_Desc)
			End If
      ELSE
         IF i_FilterTransObj.SQLCode <> 100 THEN
            RETURN -1
         END IF
      END IF
   LOOP
END IF

//------------------------------------------------------------------
//  Close the dynamic cursor.
//------------------------------------------------------------------

CLOSE LB_Cursor ;

IF i_FilterTransObj.SQLCode <> 0 THEN
   RETURN -1
END IF

//------------------------------------------------------------------
//  Do any cleanup and exit the function.
//------------------------------------------------------------------

RETURN 0
end function

public function integer fu_refreshcode ();//******************************************************************
//  PO Module     : u_LB_Filter
//  Function      : fu_RefreshCode
//  Description   : Refresh a list box using codes and descriptions
//                  from the database.
//
//  Parameters    : (None)
//
//  Return Value  : 0 = OK, -1 = Error
//
//  Change History:  
//    
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

RETURN fu_LoadCode(i_LoadTable, i_LoadCode, i_LoadDesc, &
                   i_LoadWhere, i_LoadKeyword)

end function

public function integer fu_wiredw (datawindow filter_dw, string filter_column, transaction filter_transobj);//******************************************************************
//  PO Module     : u_LB_Filter
//  Subroutine    : fu_WireDW
//  Description   : Wires a column in a DataWindow to this object.
//
//  Parameters    : DATAWINDOW Filter_DW -
//                     The DataWindow that is to be wired to
//                     this filter object.
//                  STRING Filter_Column -
//                     The column in the DataWindow that
//                     the filter object should filter through.
//                  TRANSACTION Filter_TransObj - 
//                     Transaction object associated with the
//                     filter DataWindow.
//
//  Return Value  : 0 = valid DataWindow   -1 = invalid DataWindow
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

INTEGER l_Return = -1

//------------------------------------------------------------------
//  Make sure that we were passed a valid DataWindow to be wired.
//------------------------------------------------------------------

IF IsValid(filter_dw) THEN

   //---------------------------------------------------------------
   //  Remember the DataWindow table and column for filtering.
   //---------------------------------------------------------------

   i_FilterDW       = filter_dw
   i_FilterColumn   = filter_column
   i_FilterTransObj = filter_transobj
   i_FilterOriginal = i_FilterDW.Describe("datawindow.table.filter")
   IF i_FilterOriginal = "?" THEN
      i_FilterOriginal = ""
   END IF
   Enabled          = TRUE
   l_Return         = 0

END IF

RETURN l_Return
end function

public function integer fu_buildfilter (boolean filter_reset);//******************************************************************
//  PO Module     : u_LB_Filter
//  Subroutine    : fu_BuildFilter
//  Description   : Extends the filter clause of a DataWindow 
//                  with the values in this filter object.
//
//  Parameters    : BOOLEAN Filter_Reset -
//                     Should the DataWindow filter be reset to its
//                     original state before the building begins.
//
//  Return Value  : 0 = build OK   -1 = validation error
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN  l_First
INTEGER  l_GroupByPos, l_OrderByPos, l_TextCnt, l_Idx, l_Jdx
STRING   l_ColumnType, l_Concat, l_NewFilter, l_OR
STRING   l_GroupBy, l_OrderBy, l_Quotes, l_Other, l_TextList[]

//------------------------------------------------------------------
//  See if the DataWindow filter criteria needs to be reset to its
//  original syntax.
//------------------------------------------------------------------

IF filter_reset THEN
   i_FilterDW.Modify('datawindow.table.filter="' + &
                     i_FilterOriginal + '"')
END IF

l_NewFilter = i_FilterDW.Describe("datawindow.table.filter")
IF l_NewFilter = "?" THEN
   l_NewFilter = ""
END IF

l_TextCnt   = fu_SelectCode(l_TextList[])
IF l_TextCnt = 0 THEN
   l_TextCnt = 1
   l_TextList[1] = ""
END IF

//------------------------------------------------------------------
//  If this object has been un-wired, don't include in build.
//------------------------------------------------------------------

IF IsNull(i_FilterDW) THEN
   RETURN -1
END IF

l_Concat = ""
IF l_NewFilter <> "" THEN
   l_Concat = " AND "
END IF

//------------------------------------------------------------------
//  Check to see if the developer made this a required field for
//  filter criteria.
//------------------------------------------------------------------

i_FilterError = c_ValOK
FOR l_Idx = 1 TO l_TextCnt
   i_FilterValue = TRIM(l_TextList[l_Idx])
   IF i_FilterValue = "" THEN
      TriggerEvent("po_ValEmpty")

      //------------------------------------------------------------
      //  If we get a validation error, abort the filter.
      //------------------------------------------------------------

      IF i_FilterError = c_ValFailed THEN
         SetFocus()
         RETURN -1
      END IF
   END IF

   IF i_FilterValue <> "" THEN
      l_Jdx = l_Jdx + 1
      l_TextList[l_Jdx] = l_TextList[l_Idx]
   END IF

NEXT

l_TextCnt = l_Jdx

IF l_TextCnt = 1 THEN
   IF l_TextList[1] = "" THEN
      RETURN 0
   END IF
END IF

//------------------------------------------------------------------
//  Assume that it does not contain anything that needs quoting.
//------------------------------------------------------------------

l_Quotes = ""
l_OR     = ""
l_Other  = ""

//------------------------------------------------------------------
//  See what type of data this filter object contains.
//------------------------------------------------------------------

l_ColumnType = Upper(i_FilterDW.Describe(i_FilterColumn + ".ColType"))
IF Left(l_ColumnType, 3) = "DEC" THEN
   l_ColumnType = "NUMBER"
END IF

CHOOSE CASE l_ColumnType
   CASE "NUMBER"
   CASE "DATE"
   CASE "DATETIME"
   CASE "TIME"

      //------------------------------------------------------------
      //  We have a data type that needs to be quoted (e.g. STRING).
      //------------------------------------------------------------

   CASE ELSE
      l_Quotes = "'"
END CHOOSE

//------------------------------------------------------------------
//  Stuff the current token in and perform valdiation.
//------------------------------------------------------------------

i_FilterError = c_ValOK
FOR l_Idx = 1 TO l_TextCnt
   i_FilterValue = l_TextList[l_Idx]
   TriggerEvent("po_Validate")

   //---------------------------------------------------------------
   //  If we get a validation error, abort the filter.
   //---------------------------------------------------------------

   IF i_FilterError = c_ValFailed THEN
      SetFocus()
      RETURN -1
   END IF

   //---------------------------------------------------------------
   //  Add the validated value.
   //---------------------------------------------------------------

   l_TextList[l_Idx] = i_FilterValue
   l_Other = l_Other + l_OR + i_FilterColumn + " = " + &
                       l_Quotes + l_TextList[l_Idx] + l_Quotes
   l_OR = " OR "
NEXT

//------------------------------------------------------------------
//  Add the snippet generated by the this object to our new 
//  filter statement.
//------------------------------------------------------------------

IF Len(l_Other) > 0 THEN
   l_NewFilter = l_NewFilter + l_Concat + "(" + l_Other + ")"
END IF

//------------------------------------------------------------------
//  Stuff the parameter with the completed filter statement.
//------------------------------------------------------------------

l_NewFilter = 'datawindow.table.filter="' + l_NewFilter + '"'
i_FilterDW.Modify(l_NewFilter)

RETURN 0
end function

public subroutine fu_unwiredw ();//******************************************************************
//  PO Module     : u_LB_Filter
//  Subroutine    : fu_UnwireDW
//  Description   : Un-wires a DataWindow from the filter object.
//
//  Parameters    : (None)
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

SetNull(i_FilterDW)
Enabled = FALSE
end subroutine

on constructor;//******************************************************************
//  PO Module     : u_DDLB_Filter
//  Event         : Constructor
//  Description   : Initializes the Filter object.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

//------------------------------------------------------------------
//  Identify this object as a PowerObject Filter.
//------------------------------------------------------------------

Tag = "PowerObject Filter LB"
Enabled  = FALSE
SetNull(i_FilterDW)
end on

