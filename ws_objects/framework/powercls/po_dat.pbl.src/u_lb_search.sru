﻿$PBExportHeader$u_lb_search.sru
$PBExportComments$User object that uses a list box as a search object
forward
global type u_lb_search from listbox
end type
end forward

global type u_lb_search from listbox
int X=5
int Y=5
int Width=801
int Height=401
int TabOrder=1
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean MultiSelect=true
long BackColor=16777215
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
event po_valempty pbm_custom74
event po_validate pbm_custom75
end type
global u_lb_search u_lb_search

type variables
DATAWINDOW	i_SearchDW

TRANSACTION	i_SearchTransObj
STRING		i_SearchTable
STRING		i_SearchColumn
STRING		i_SearchValue
STRING		i_SearchOriginal

INTEGER		i_SearchError

STRING		i_LoadTable
STRING		i_LoadCode
STRING		i_LoadDesc
STRING		i_LoadWhere
STRING		i_LoadKeyword

INTEGER		c_ValOK		= 0
INTEGER		c_ValFailed	= 1

end variables

forward prototypes
public function integer fu_selectcode (ref string codes[])
public function integer fu_setcode (string default_code[])
public function integer fu_refreshcode ()
public function integer fu_loadcode (string table_name, string column_code, string column_desc, string where_clause, string all_keyword)
public function integer fu_buildsearch (boolean search_reset)
public function integer fu_wiredw (datawindow search_dw, string search_table, string search_column, transaction search_transobj)
public subroutine fu_unwiredw ()
end prototypes

on po_valempty;////******************************************************************
////  PO Module     : u_LB_Search
////  Event         : po_ValEmpty
////  Description   : Provides the opportunity for the developer to
////                  generate errors when the user has not made a
////                  selection in the LB object.
////
////  Return Value  : c_ValOK     = OK
////                  c_ValFailed = Error, criteria is required.
////
////  Change History:
////
////  Date     Person     Description of Change
////  -------- ---------- --------------------------------------------
////
////******************************************************************
////  Copyright ServerLogic 1994-1995.  All Rights Reserved.
////******************************************************************
//
////-------------------------------------------------------------------
////  Set the i_SearchError variable to c_ValFailed to indicate that
////  this search object is required.
////-------------------------------------------------------------------
//
////SetFocus()
////MessageBox("Search Object Error", &
////           "This is a required search field", &
////           Exclamation!, Ok!)
////i_SearchError = c_ValFailed
end on

on po_validate;////******************************************************************
////  PO Module     : u_LB_Search
////  Event         : po_Validate
////  Description   : Provides the opportunity for the developer to
////                  write code to validate the fields in this
////                  object.
////
////  Return Value  : INTEGER i_SearchError 
////
////                  If the developer codes the validation 
////                  testing, then the developer should set 
////                  this variable to one of the following
////                  validation return codes:
////
////                  c_ValOK     = The validation test passed and 
////                                the data is to be accepted.
////                  c_ValFailed = The validation test failed and 
////                                the data is to be rejected.  
////                                Do not allow the user to move 
////                                off of this field.
////
////  Change History:
////
////  Date     Person     Description of Change
////  -------- ---------- --------------------------------------------
////
////******************************************************************
////  Copyright ServerLogic 1992-1995.  All Rights Reserved.
////******************************************************************
//
////------------------------------------------------------------------
////  The following values are set for the developer before this
////  event is triggered:
////
////      STRING i_SearchValue -
////            The input the user has made.
////------------------------------------------------------------------
//
////i_SearchError = f_PO_ValMaxLen(i_SearchValue, 10, TRUE)
end on

public function integer fu_selectcode (ref string codes[]);//******************************************************************
//  PO Module     : u_LB_Search
//  Function      : fu_SelectCode
//  Description   : Select the code associated with the description
//                  from a list box.
//
//  Parameters    : STRING Codes[] - 
//                     Array of codes corresponding to the 
//                     currently selected items in the list box.
//
//  Return Value  : INTEGER  - Number of items selected.
//
//  Change History:  
//    
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

INTEGER l_Cnt, l_Idx, l_TotalItems
STRING  l_Value, l_NullArray[]

//------------------------------------------------------------------
//  Pull the code from each item selected in the list box.
//------------------------------------------------------------------

l_Cnt = 0
l_TotalItems = TotalItems()
FOR l_Idx = 1 TO l_TotalItems
   IF State(l_Idx) = 1 THEN
      l_Cnt        = l_Cnt + 1
      l_Value      = Text(l_Idx)
      IF Pos(l_Value, "||") > 0 THEN
     	   Codes[l_Cnt] = Mid(l_Value, Pos(l_Value, "||") + 2)
     	   IF Codes[l_Cnt] = "NULL"   OR &
            Codes[l_Cnt] = "-99999" OR &
            Codes[l_Cnt] = "(ALL)" THEN
            l_Cnt   = 0
        	   Codes[] = l_NullArray[]
            EXIT
         END IF
      ELSE
         Codes[l_Cnt] = l_Value
      END IF
   END IF
NEXT

//------------------------------------------------------------------
//  Do any cleanup and exit the function.
//------------------------------------------------------------------

RETURN l_Cnt
end function

public function integer fu_setcode (string default_code[]);//******************************************************************
//  PO Module     : u_LB_Search
//  Subroutine    : fu_SetCode
//  Description   : Sets a default value for this object.
//
//  Parameters    : STRING Default_Code -
//                     The values to set for this object.
//
//  Return Value  : 0 = set OK   -1 = set failed
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

INTEGER l_Return = -1
LONG    l_TotalCodes, l_TotalItems, l_Idx, l_Jdx
STRING  l_Value, l_Code

l_TotalCodes = UpperBound(default_code[])
l_TotalItems = TotalItems()

FOR l_Idx = 1 TO l_TotalCodes
   l_Code = TRIM(default_code[l_Idx])
   FOR l_Jdx = 1 TO l_TotalItems
      l_Value = Text(l_Jdx)
      l_Value = MID(l_Value, Pos(l_Value, "||") + 2)
      IF l_Value = l_Code THEN
         SetState(l_Jdx, TRUE)
         l_Return = 0
         EXIT
      END IF
   NEXT
NEXT 

RETURN l_Return
end function

public function integer fu_refreshcode ();//******************************************************************
//  PO Module     : u_LB_Search
//  Function      : fu_RefreshCode
//  Description   : Refresh a list box using codes and descriptions
//                  from the database.
//
//  Parameters    : (None)
//
//  Return Value  : 0 = OK, -1 = Error
//
//  Change History:  
//    
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

RETURN fu_LoadCode(i_LoadTable, i_LoadCode, i_LoadDesc, &
                   i_LoadWhere, i_LoadKeyword)

end function

public function integer fu_loadcode (string table_name, string column_code, string column_desc, string where_clause, string all_keyword);//******************************************************************
//  PO Module     : u_LB_Search
//  Function      : fu_LoadCode
//  Description   : Load a list box using codes and descriptions
//                  from the database.
//
//  Parameters    : STRING      Table_Name   - 
//                     Table from where the column with the code
//                     values resides.
//                  STRING      Column_Code  - 
//                     Column name with the code values.
//                  STRING      Column_Desc  - 
//                     Column name with the code descriptions.
//                  STRING      Where_Clause - 
//                     WHERE cause statement to restrict the code 
//                     values.
//                  STRING      All_Keyword  - 
//                     Keyword to denote select all values (e.g. 
//                     "(All)").
//
//  Return Value  : 0 = OK, -1 = Error
//
//  Change History:  
//    
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN l_IsNumber
LONG    l_Nbr
STRING  l_SQLString, l_Code, l_Desc

i_LoadTable = table_name
i_LoadCode = column_code
i_LoadDesc = column_desc
i_LoadWhere = where_clause
i_LoadKeyword = all_keyword

//------------------------------------------------------------------
//  Declare a dynamic SQL cursor to hold the data while loading the 
//  list box.
//------------------------------------------------------------------

i_SearchTransObj.SQLCode = 0
DECLARE LB_Cursor DYNAMIC CURSOR FOR SQLSA ;

IF i_SearchTransObj.SQLCode <> 0 THEN
   RETURN -1
END IF

//------------------------------------------------------------------
//  Build the SQL SELECT statement to get the data.
//------------------------------------------------------------------

l_SQLString = "SELECT " + i_LoadCode + ", " + &
              i_LoadDesc + " FROM " + i_LoadTable
IF Trim(i_LoadWhere) <> "" THEN
   l_SQLString = l_SQLString + " WHERE " + i_LoadWhere
END IF

//------------------------------------------------------------------
//  Prepare the SQL statement for execution.
//------------------------------------------------------------------

PREPARE SQLSA FROM :l_SQLString USING i_SearchTransObj;

IF i_SearchTransObj.SQLCode <> 0 THEN
   RETURN -1
END IF

//------------------------------------------------------------------
//  Open the dynamic cursor.
//------------------------------------------------------------------

OPEN DYNAMIC LB_Cursor ;

IF i_SearchTransObj.SQLCode <> 0 THEN
   RETURN -1
END IF

//------------------------------------------------------------------
//  If a keyword has been given to select all, add it as a selection
//  to the list box.
//------------------------------------------------------------------

IF i_LoadKeyword <> "" THEN
   l_Code = "(ALL)"
   l_Desc = i_LoadKeyword
   l_Desc = String(l_Desc, Fill("@",80)) + "||" + l_Code
   AddItem(l_Desc)
END IF

//------------------------------------------------------------------
//  Determine if the code column is a number or a string.
//------------------------------------------------------------------

FETCH LB_Cursor INTO :l_Code, :l_Desc ;

IF i_SearchTransObj.SQLCode = 0 OR &
   i_SearchTransObj.SQLCode = 100 THEN
   l_IsNumber = FALSE
ELSE
   l_IsNumber = TRUE
END IF

CLOSE LB_Cursor ;
OPEN DYNAMIC LB_Cursor ;

//------------------------------------------------------------------
//  For each row in the cursor, add the description and code to the
//  list box.
//------------------------------------------------------------------

IF l_IsNumber THEN
   DO WHILE i_SearchTransObj.SQLCode = 0
      FETCH LB_Cursor INTO :l_Nbr, :l_Desc ;
      IF i_SearchTransObj.SQLCode = 0 THEN
         IF IsNull(l_Desc) THEN
  		      l_Nbr = -99999
  	      END IF
         IF i_LoadCode <> i_LoadDesc THEN
            l_Desc = String(l_Desc, Fill("@",80)) + &
                     "||" + String(l_Nbr)
         END IF
         AddItem(l_Desc)
      ELSE
         IF i_SearchTransObj.SQLCode <> 100 THEN
            RETURN -1
         END IF
      END IF
   LOOP
ELSE
   DO WHILE i_SearchTransObj.SQLCode = 0
      FETCH LB_Cursor INTO :l_Code, :l_Desc ;
      IF i_SearchTransObj.SQLCode = 0 THEN
         IF IsNull(l_Desc) THEN
  		      l_Code = "NULL"
  	      END IF
         IF i_LoadCode <> i_LoadDesc THEN
	         l_Desc = String(l_Desc, Fill("@",80)) + &
                     "||" + l_Code
         END IF
			IF l_Code <> "NULL" THEN
	         AddItem(l_Desc)
			End If
      ELSE
         IF i_SearchTransObj.SQLCode <> 100 THEN
            RETURN -1
         END IF
      END IF
   LOOP
END IF

//------------------------------------------------------------------
//  Close the dynamic cursor.
//------------------------------------------------------------------

CLOSE LB_Cursor ;

IF i_SearchTransObj.SQLCode <> 0 THEN
   RETURN -1
END IF

//------------------------------------------------------------------
//  Do any cleanup and exit the function.
//------------------------------------------------------------------

RETURN 0
end function

public function integer fu_buildsearch (boolean search_reset);//******************************************************************
//  PO Module     : u_LB_Search
//  Subroutine    : fu_BuildSearch
//  Description   : Extends the WHERE clause of a SQL Select 
//                  with the values in this search object.
//
//  Parameters    : BOOLEAN Search_Reset -
//                     Should the SQL Select be reset to its
//                     original state before the building begins.
//
//  Return Value  : 0 = build OK   -1 = validation error
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN  l_First
INTEGER  l_GroupByPos, l_OrderByPos, l_TextCnt, l_Idx, l_Jdx
STRING   l_ColumnType, l_Concat, l_NewSelect, l_Comma
STRING   l_GroupBy, l_OrderBy, l_Quotes, l_Other, l_TextList[]

//------------------------------------------------------------------
//  See if the DataWindow search criteria needs to be reset to its
//  original syntax.
//------------------------------------------------------------------

IF search_reset THEN
   i_SearchDW.Modify('datawindow.table.select="' + &
                     i_SearchOriginal + '"')
END IF

l_NewSelect = i_SearchDW.Describe("datawindow.table.select")
l_TextCnt   = fu_SelectCode(l_TextList[])
IF l_TextCnt = 0 THEN
   l_TextCnt = 1
   l_TextList[1] = ""
END IF

//------------------------------------------------------------------
//  If this object has been un-wired, don't include in build.
//------------------------------------------------------------------

IF IsNull(i_SearchDW) THEN
   RETURN -1
END IF

//------------------------------------------------------------------
//  If the SQL statement for this DataWindow does not currently
//  have a WHERE clause, then we need to add one.  If it already
//  does have a WHERE clause, we will just concatenate additional
//  criteria to it.
//------------------------------------------------------------------

IF Pos(Upper(l_NewSelect), "WHERE") = 0 THEN
   l_Concat = " WHERE "
ELSE
   l_Concat = " AND "
END IF

//------------------------------------------------------------------
//  See if we have an "GROUP BY" clause and remove it from the
//  select statement for the time being.
//------------------------------------------------------------------

l_GroupBy    = ""
l_GroupByPos = Pos(Upper(l_NewSelect), "GROUP BY")
IF l_GroupByPos > 0 THEN
   l_GroupBy   = " " + Mid(l_NewSelect, l_GroupByPos)
   l_NewSelect = Mid(l_NewSelect, 1, l_GroupByPos - 1)
ELSE

   //---------------------------------------------------------------
   //  See if we have an "ORDER BY" clause and remove it from the
   //  select statement for the time being.
   //---------------------------------------------------------------

   l_OrderBy    = ""
   l_OrderByPos = Pos(Upper(l_NewSelect), "ORDER BY")
   IF l_OrderByPos > 0 THEN
      l_OrderBy   = " " + Mid(l_NewSelect, l_OrderByPos)
      l_NewSelect = Mid(l_NewSelect, 1, l_OrderByPos - 1)
   END IF
END IF

//------------------------------------------------------------------
//  Check to see if the developer made this a required field for
//  search criteria.
//------------------------------------------------------------------

i_SearchError = c_ValOK
FOR l_Idx = 1 TO l_TextCnt
   i_SearchValue = TRIM(l_TextList[l_Idx])
   IF i_SearchValue = "" THEN
      TriggerEvent("po_ValEmpty")

      //------------------------------------------------------------
      //  If we get a validation error, abort the search.
      //------------------------------------------------------------

      IF i_SearchError = c_ValFailed THEN
         SetFocus()
         RETURN -1
      END IF
   END IF

   IF i_SearchValue <> "" THEN
      l_Jdx = l_Jdx + 1
      l_TextList[l_Jdx] = l_TextList[l_Idx]
   END IF

NEXT

l_TextCnt = l_Jdx

//------------------------------------------------------------------
//  Assume that it does not contain anything that needs quoting.
//------------------------------------------------------------------

l_Quotes = ""
l_Comma  = ""
l_Other  = ""

//------------------------------------------------------------------
//  See what type of data this search object contains.
//------------------------------------------------------------------

l_ColumnType = Upper(i_SearchDW.Describe(i_SearchColumn + ".ColType"))
IF Left(l_ColumnType, 3) = "DEC" THEN
   l_ColumnType = "NUMBER"
END IF

CHOOSE CASE l_ColumnType
   CASE "NUMBER"
   CASE "DATE"
   CASE "DATETIME"
   CASE "TIME"

      //------------------------------------------------------------
      //  We have a data type that needs to be quoted (e.g. STRING).
      //------------------------------------------------------------

   CASE ELSE
      l_Quotes = "'"
END CHOOSE

//------------------------------------------------------------------
//  Stuff the current token in and perform valdiation.
//------------------------------------------------------------------

i_SearchError = c_ValOK
FOR l_Idx = 1 TO l_TextCnt
   i_SearchValue = l_TextList[l_Idx]
   TriggerEvent("po_Validate")

   //---------------------------------------------------------------
   //  If we get a validation error, abort the search.
   //---------------------------------------------------------------

   IF i_SearchError = c_ValFailed THEN
      SetFocus()
      RETURN -1
   END IF

   //---------------------------------------------------------------
   //  Add the validated value.
   //---------------------------------------------------------------

   l_TextList[l_Idx] = i_SearchValue
   l_Other = l_Other + l_Comma + l_Quotes + l_TextList[l_Idx] + &
                       l_Quotes
   l_Comma = ","
NEXT

//------------------------------------------------------------------
//  Add the snippet of SQL generated by the this object to our new 
//  SQL statement.
//------------------------------------------------------------------

IF Len(l_Other) > 0 THEN
   l_NewSelect = l_NewSelect + l_Concat + "(" + i_SearchTable + &
                               "." + i_SearchColumn + &
                               " IN (" + l_Other + "))"
END IF

//------------------------------------------------------------------
//  Stuff the parameter with the completed SQL statement.
//------------------------------------------------------------------

l_NewSelect = "datawindow.table.select='" + &
              w_POManager_STD.MB.fu_QuoteChar(l_NewSelect, "'") + &
              w_POManager_STD.MB.fu_QuoteChar(l_GroupBy, "'")   + &
              w_POManager_STD.MB.fu_QuoteChar(l_OrderBy, "'")   + "'"
i_SearchDW.Modify(l_NewSelect)

RETURN 0
end function

public function integer fu_wiredw (datawindow search_dw, string search_table, string search_column, transaction search_transobj);//******************************************************************
//  PO Module     : u_LB_Search
//  Subroutine    : fu_WireDW
//  Description   : Wires a column in a DataWindow to this object.
//
//  Parameters    : DATAWINDOW Search_DW -
//                     The DataWindow that is to be wired to
//                     this search object.
//                  STRING Search_Table -
//                     The table in the database that the search
//                     object should build the WHERE clause for.
//                  STRING Search_Column -
//                     The column in the database that the search
//                     object should build the WHERE clause for.
//                  TRANSACTION Search_TransObj - 
//                     Transaction object associated with the
//                     search DataWindow.
//
//  Return Value  : 0 = valid DataWindow   -1 = invalid DataWindow
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

INTEGER l_Return = -1

//------------------------------------------------------------------
//  Make sure that we were passed a valid DataWindow to be wired.
//------------------------------------------------------------------

IF IsValid(search_dw) THEN

   //---------------------------------------------------------------
   //  Remember the DataWindow table and column for searching.
   //---------------------------------------------------------------

   i_SearchDW       = search_dw
   i_SearchTable    = search_table
   i_SearchColumn   = search_column
   i_SearchTransObj = search_transobj
   i_SearchOriginal = i_SearchDW.Describe("datawindow.table.select")
   Enabled          = TRUE
   l_Return         = 0

END IF

RETURN l_Return
end function

public subroutine fu_unwiredw ();//******************************************************************
//  PO Module     : u_LB_Search
//  Subroutine    : fu_UnwireDW
//  Description   : Un-wires a DataWindow from the search object.
//
//  Parameters    : (None)
//
//  Return Value  : (None)
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1993-1995.  All Rights Reserved.
//******************************************************************

SetNull(i_SearchDW)
Enabled = FALSE
end subroutine

on constructor;//******************************************************************
//  PO Module     : u_DDLB_Search
//  Event         : Constructor
//  Description   : Initializes the Search object.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

//------------------------------------------------------------------
//  Identify this object as a PowerObject Search.
//------------------------------------------------------------------

Tag = "PowerObject Search LB"
Enabled  = FALSE
SetNull(i_SearchDW)
end on

