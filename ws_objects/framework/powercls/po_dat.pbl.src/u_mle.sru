﻿$PBExportHeader$u_mle.sru
$PBExportComments$MLE portion of the drop-down MLE object
forward
global type u_mle from multilineedit
end type
end forward

global type u_mle from multilineedit
int Width=494
int Height=361
int TabOrder=1
boolean VScrollBar=true
long BackColor=16777215
int TextSize=-10
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
event po_ddfocus pbm_custom75
event po_ddprocess pbm_custom74
end type
global u_mle u_mle

type variables
//-------------------------------------------------------------------------------
//  Drop-down MLE Instance Variables.
//-------------------------------------------------------------------------------

STRING		i_MLEDDType
INTEGER		i_MLEDDIndex
STRING		i_MLEDDColumn
INTEGER		i_MLEDDRow

INTEGER		i_NumDD
INTEGER		i_NumDDObjects
INTEGER		i_NumDDColumns

STRING		i_DDName[]
STRING		i_DDType[]
INTEGER		i_DDIndex[]
INTEGER		i_DDWidth[]
INTEGER		i_DDHeight[]
INTEGER		i_DDX[]
INTEGER		i_DDY[]
INTEGER		i_DDDetail[]
INTEGER		i_DDMLEWidth[]
INTEGER		i_DDMLEHeight[]
INTEGER		i_DDTextLimit[]

U_DD_MLE	i_DDObject[]

DATAWINDOW	i_DDDW[]
STRING		i_DDColumn[]

WINDOW		i_Window
USEROBJECT	i_UserObject
STRING		i_ParentObject
INTEGER		i_ParentHeight
DATAWINDOW	i_CurrentDW
BOOLEAN	i_DDClosed
BOOLEAN	i_DDTabOut

//-------------------------------------------------------------------------------
//  Drop-down MLE Instance Variables.
//-------------------------------------------------------------------------------

INTEGER		c_DefaultDDWidth		= 0
INTEGER		c_DefaultDDHeight		= 0
INTEGER		c_DefaultDDLimit		= 0
end variables

forward prototypes
public function integer fu_ddopen (datawindow dw_name)
public subroutine fu_ddmlewire (u_dd_mle dd_object, integer text_limit, integer percent_width, integer drop_height)
public subroutine fu_ddmlewiredw (datawindow dd_dw, string dd_column, integer text_limit, integer percent_width, integer drop_height)
public subroutine fu_ddkeydown ()
public subroutine fu_ddeditchanged (datawindow dw_name)
public function double fu_ddfindxpos (string dw_column)
end prototypes

on po_ddfocus;//******************************************************************
//  PO Module     : u_MLE
//  Event         : po_DDFocus
//  Description   : Set the focus on the drop-down object or 
//                  DataWindow column.
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

IF i_MLEDDType = "OBJECT" THEN
   i_DDObject[i_DDIndex[i_MLEDDIndex]].SetFocus()
ELSE
   i_DDDW[i_DDIndex[i_MLEDDIndex]].SetFocus()
END IF


end on

on po_ddprocess;//******************************************************************
//  PO Module     : u_MLE
//  Event         : po_DDProcess
//  Description   : Displays the MLE under the column using
//                  the column attributes for font and colors.  
//
//  Parameters    : (None)
//
//  Return Value  : (None)
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

STRING  l_Column, l_DWColumn
INTEGER l_Index, l_ColumnIndex, l_Idx, l_Width, l_FieldY, l_FirstRow
BOOLEAN l_Found
INTEGER l_ParentHeight, l_Y, l_OffsetDown, l_OffsetUp
DOUBLE  l_X
LONG    l_Row

SetPointer(HourGlass!)

l_Column   = i_CurrentDW.GetColumnName()
l_Row      = i_CurrentDW.GetRow()
l_DWColumn = i_CurrentDW.ClassName() + "." + l_Column

IF l_Column = i_MLEDDColumn AND l_Row = i_MLEDDRow AND i_DDClosed THEN
   i_DDClosed = FALSE
   RETURN
ELSE
   i_DDClosed = FALSE
END IF

//------------------------------------------------------------------
//  Find the DataWindow column to attach the MLE to.
//------------------------------------------------------------------

l_Found = FALSE
FOR l_Idx = 1 TO i_NumDD
   IF i_DDName[l_Idx] = l_DWColumn THEN
      l_Found = TRUE
      l_Index = l_Idx
      l_ColumnIndex = i_DDIndex[l_Idx]
      EXIT
   END IF
NEXT

IF l_Found THEN

   //---------------------------------------------------------------
   //  Determine if the MLE is on a window or a user object and get
   //  the height for it.
   //---------------------------------------------------------------

   IF i_ParentObject = "WINDOW" THEN
      l_ParentHeight = i_Window.WorkSpaceHeight()
   ELSE
      l_ParentHeight = i_UserObject.Height
   END IF

   //---------------------------------------------------------------
   //  Determine if the MLE needs to be moved to the current 
   //  DataWindow column or if it already there.
   //---------------------------------------------------------------

   i_ParentHeight = l_ParentHeight

   IF i_DDMLEWidth[l_Index] <> Width THEN
      Resize(i_DDMLEWidth[l_Index], i_DDMLEHeight[l_Index])
   END IF

   //---------------------------------------------------------------
   //  Set the attributes of the MLE to match the DataWindow
   //  column.
   //---------------------------------------------------------------

   IF i_MLEDDColumn <> l_Column THEN
      BackColor   = Long(i_CurrentDW.Describe(l_Column + ".Background.Color"))
      TextColor   = Long(i_CurrentDW.Describe(l_Column + ".Color"))
      TextSize    = Integer(i_CurrentDW.Describe(l_Column + ".Font.Height"))
      FaceName    = i_CurrentDW.Describe(l_Column + ".Font.Face")

      CHOOSE CASE Integer(i_CurrentDW.Describe(l_Column + ".Font.Family"))
         CASE 0
            FontFamily = AnyFont!
         CASE 1
            FontFamily = Roman!
         CASE 2
            FontFamily = Swiss!
         CASE 3
            FontFamily = Modern!
         CASE 4
            FontFamily = Decorative!
         CASE 5
            FontFamily = AnyFont!
      END CHOOSE

      CHOOSE CASE Integer(i_CurrentDW.Describe(l_Column + ".Font.Pitch"))
         CASE 0
            FontPitch = Default!
         CASE 1
            FontPitch = Fixed!
         CASE 2
            FontPitch = Variable!
      END CHOOSE

      CHOOSE CASE Integer(i_CurrentDW.Describe(l_Column + ".Font.Charset"))
         CASE 0
            FontCharset = Ansi!
         CASE 1
            FontCharset = DefaultCharset!
         CASE 2
            FontCharset = Symbol!
         CASE 128
            FontCharset = ShiftJIS!
         CASE 255
            FontCharset = Oem!
      END CHOOSE
   END IF

   //---------------------------------------------------------------
   //  Determine if the MLE should be displayed above or below 
   //  the DataWindow column.
   //---------------------------------------------------------------

   CHOOSE CASE Integer(i_CurrentDW.Describe(l_Column + ".Border"))
      CASE 1
         BorderStyle  = StyleShadowBox!
         l_OffsetDown = 6
         l_OffsetUp   = 0
      CASE 5
         BorderStyle = StyleLowered!
         l_OffsetDown = 10
         l_OffsetUp   = 0
      CASE 6
         BorderStyle = StyleRaised!
         l_OffsetDown = 10
         l_OffsetUp   = 0
      CASE ELSE
         BorderStyle = StyleBox!
         l_OffsetDown = 6
         l_OffsetUp   = -6
   END CHOOSE

   l_FirstRow = Integer(i_CurrentDW.Describe("datawindow.firstrowonpage"))
   l_FieldY = i_DDY[l_Index] + ((l_Row - l_FirstRow) * &
              i_DDDetail[l_Index])

   IF l_FieldY + i_DDHeight[l_Index] + &
      i_DDMLEHeight[l_Index] + l_OffsetDown < l_ParentHeight THEN
      l_Y = l_FieldY + i_DDHeight[l_Index] + l_OffsetDown
   ELSEIF l_FieldY - i_DDMLEHeight[l_Index] - l_OffsetUp > 1 THEN
      l_Y = l_FieldY - i_DDMLEHeight[l_Index] - l_OffsetUp 
   ELSE
      l_Y = l_FieldY + i_DDHeight[l_Index] + l_OffsetDown
   END IF

   IF i_CurrentDW.HScrollBar THEN
      l_X = fu_DDFindXPos(l_Column)
   ELSE
      l_X = i_DDX[l_Index]
   END IF

   Move(l_X, l_Y)

   i_MLEDDType   = "COLUMN"
   i_MLEDDIndex  = l_Index
   i_MLEDDColumn = l_Column
   i_MLEDDRow    = l_Row

   //---------------------------------------------------------------
   //  Copy the text from the DataWindow column to the MLE and 
   //  display the MLE object.
   //---------------------------------------------------------------

   Text = i_CurrentDW.GetText()
   SelectText(Len(Text) + 1, 0)
   Visible = TRUE
   TabOrder = i_CurrentDW.TabOrder - 1
   SetFocus()
END IF


end on

public function integer fu_ddopen (datawindow dw_name);//******************************************************************
//  PO Module     : u_MLE
//  Subroutine    : fu_DDOpen
//  Description   : Post the processing of the drop-down MLE so the
//                  drop-down list box is prevented from showing.  
//
//  Parameters    : DATAWINDOW dw_name - Name of the DataWindow the
//                                       column is on.
//
//  Return Value  : 0 = Allow the drop-down open event to complete
//                  1 = Prevent the open event from completing
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

STRING  l_Column, l_ClickedObject
INTEGER l_Idx

i_CurrentDW = dw_name
PostEvent("po_DDProcess")

l_ClickedObject = i_CurrentDW.GetObjectAtPointer()

IF l_ClickedObject = "" THEN
	l_Column = i_CurrentDW.ClassName() + "." + i_CurrentDW.GetColumnName()
ELSE
   l_Column = i_CurrentDW.ClassName() + "." + &
              MID(l_ClickedObject, 1, POS(l_ClickedObject, CHAR(9)) - 1)
END IF

//------------------------------------------------------------------
//  Find the DataWindow column to attach the MLE to.
//------------------------------------------------------------------

FOR l_Idx = 1 TO i_NumDD
   IF i_DDName[l_Idx] = l_Column THEN
      RETURN 1
   END IF
NEXT

RETURN 0
end function

public subroutine fu_ddmlewire (u_dd_mle dd_object, integer text_limit, integer percent_width, integer drop_height);//******************************************************************
//  PO Module     : u_MLE
//  Subroutine    : fu_DDMLEWire
//  Description   : Wires a drop-down MLE object to an MLE object.  
//
//  Parameters    : U_DD_MLE dd_object    - Name of the drop-down
//                                          object.
//                  INTEGER  text_limit   - Number of characters
//                                          entered until the MLE 
//                                          drops automatically.
//                  INTEGER  percent_width- percent of the drop
//                                          down's width to set for
//                                          the MLE width. Default
//                                          is 100.
//                  INTEGER  drop_height  - MLE height.  Default is
//                                          72% of the width.          
//
//  Return Value  : (None)
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER l_Idx
BOOLEAN l_Found

l_Found = FALSE

//------------------------------------------------------------------
//  Determine if a drop-down object has been wired to this MLE
//  before.
//------------------------------------------------------------------

FOR l_Idx = 1 TO i_NumDD
   IF i_DDName[l_Idx] = dd_object.ClassName() THEN
      l_Found = TRUE
      EXIT
   END IF
NEXT

//------------------------------------------------------------------
//  If not wired, store some of the drop-down objects attributes.
//------------------------------------------------------------------

IF NOT l_Found THEN
   i_NumDD = i_NumDD + 1
   i_DDName[i_NumDD]   = dd_object.ClassName()
   i_NumDDObjects      = i_NumDDObjects + 1
   i_DDType[i_NumDD]   = "OBJECT"
   i_DDIndex[i_NumDD]  = i_NumDDObjects
   i_DDWidth[i_NumDD]  = dd_object.Width
   i_DDHeight[i_NumDD] = dd_object.Height
   i_DDX[i_NumDD]      = dd_object.X
   i_DDY[i_NumDD]      = dd_object.Y
   i_DDDetail[i_NumDD] = 0
   i_DDTextLimit[i_NumDD] = text_limit

   //---------------------------------------------------------------
   //  Determine the width to display the MLE.
   //---------------------------------------------------------------

   IF percent_width > c_DefaultDDWidth THEN
      i_DDMLEWidth[i_NumDD]  = Int(i_DDWidth[i_NumDD] * &
                                   (percent_width/100))
   ELSE
      i_DDMLEWidth[i_NumDD]  = i_DDWidth[i_NumDD]
   END IF

   //---------------------------------------------------------------
   //  Determine the height to display the MLE.
   //---------------------------------------------------------------

   IF drop_height > c_DefaultDDHeight THEN
      i_DDMLEHeight[i_NumDD] = drop_height
   ELSE
      i_DDMLEHeight[i_NumDD] = Int(i_DDMLEWidth[i_NumDD] * .72)
   END IF

   //---------------------------------------------------------------
   //  Communicate information about this object to the drop-down
   //  object.
   //---------------------------------------------------------------

   i_DDObject[i_NumDDObjects] = dd_object
   dd_object.i_MLE            = THIS
   dd_object.i_ObjectIndex    = i_NumDDObjects
   dd_object.i_DDIndex        = i_NumDD
END IF

end subroutine

public subroutine fu_ddmlewiredw (datawindow dd_dw, string dd_column, integer text_limit, integer percent_width, integer drop_height);//******************************************************************
//  PO Module     : u_MLE
//  Subroutine    : fu_DDMLEWireDW
//  Description   : Wires a drop-down MLE column to an MLE object.  
//
//  Parameters    : DATAWINDOW dd_dw      - Name of DataWindow that
//                                          contains a column with
//                                          the drop-down.
//                  STRING     dd_column  - Name of the DataWindow 
//                                          column.
//                  INTEGER    text_limit - Number of characters
//                                          entered until the MLE 
//                                          drops automatically.
//                  INTEGER    percent_width - percent of the drop
//                                          down's width to set for
//                                          the MLE width. Default
//                                          is 100.
//                  INTEGER    drop_height- MLE height.  Default is
//                                          72% of the width.          
//
//  Return Value  : (None)
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER l_Idx
BOOLEAN l_Found
STRING  l_DWName

l_DWName = dd_dw.ClassName() + "." + dd_column
l_Found  = FALSE

//------------------------------------------------------------------
//  Determine if this DataWindow column has already been wired.
//------------------------------------------------------------------

FOR l_Idx = 1 TO i_NumDD
   IF i_DDName[l_Idx] = l_DWName THEN
      l_Found = TRUE
      EXIT
   END IF
NEXT

//------------------------------------------------------------------
//  If not wired, store some attributes about the DataWindow column.
//------------------------------------------------------------------

IF NOT l_Found THEN
   i_NumDD = i_NumDD + 1
   i_DDName[i_NumDD]   = l_DWName
   i_NumDDColumns      = i_NumDDColumns + 1
   i_DDType[i_NumDD]   = "COLUMN"
   i_DDIndex[i_NumDD]  = i_NumDDColumns
   i_DDWidth[i_NumDD]  = Integer(dd_dw.Describe(dd_column + ".Width"))
   i_DDHeight[i_NumDD] = Integer(dd_dw.Describe(dd_column + ".Height"))
   i_DDX[i_NumDD]      = dd_dw.X + Integer(dd_dw.Describe(dd_column + ".X"))
   i_DDY[i_NumDD]      = dd_dw.Y + &
                    Integer(dd_dw.Describe("datawindow.header.height")) + &
                    Integer(dd_dw.Describe(dd_column + ".Y"))
   i_DDDetail[i_NumDD]    = Integer(dd_dw.Describe("datawindow.detail.height"))
   i_DDTextLimit[i_NumDD] = text_limit

   //---------------------------------------------------------------
   //  Determine the width to display the MLE.
   //---------------------------------------------------------------

   IF percent_width > c_DefaultDDWidth THEN
      i_DDMLEWidth[i_NumDD]  = Int(i_DDWidth[i_NumDD] * &
                                   (percent_width/100))
   ELSE
      i_DDMLEWidth[i_NumDD]  = i_DDWidth[i_NumDD]
   END IF

   //---------------------------------------------------------------
   //  Determine the height to display the MLE.
   //---------------------------------------------------------------

   IF drop_height > c_DefaultDDHeight THEN
      i_DDMLEHeight[i_NumDD] = drop_height
   ELSE
      i_DDMLEHeight[i_NumDD] = Int(i_DDMLEWidth[i_NumDD] * .72)
   END IF

   //---------------------------------------------------------------
   //  Register the DataWindow and column this object will be wired
   //  to.
   //---------------------------------------------------------------

   i_DDDW[i_NumDDColumns]     = dd_dw
   i_DDColumn[i_NumDDColumns] = dd_column
   Visible                    = FALSE
   TabOrder                   = 0
END IF

end subroutine

public subroutine fu_ddkeydown ();//******************************************************************
//  PO Module     : u_MLE
//  Subroutine    : fu_DDKeyDown
//  Description   : Drops or raises the MLE using the ALT-DownArrow
//                  and ALT-UpArrow.  
//
//  Parameters    : (None)
//
//  Return Value  : (None)
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

IF i_MLEDDType = "OBJECT" THEN
   IF NOT Visible THEN
      IF GetFocus() = i_DDObject[i_DDIndex[i_MLEDDIndex]] THEN
         IF KeyDown(keyDownArrow!) THEN
            i_DDObject[i_DDIndex[i_MLEDDIndex]].PostEvent("po_DDProcess")
         END IF
      END IF
   ELSE
      IF GetFocus() = THIS THEN
         IF KeyDown(keyUpArrow!) THEN
            IF i_DDObject[i_DDIndex[i_MLEDDIndex]].Text <> Text THEN
               i_DDObject[i_DDIndex[i_MLEDDIndex]].Text = Text
            END IF
            Visible = FALSE
            TabOrder = 0
            i_DDObject[i_DDIndex[i_MLEDDIndex]].i_DDClosed = FALSE
            PostEvent("po_DDFocus")
         END IF
      END IF
   END IF
ELSE
   IF Visible THEN
      IF KeyDown(keyUpArrow!) THEN
         IF i_CurrentDW.GetItemString(i_MLEDDRow, i_MLEDDColumn) <> Text THEN
            i_CurrentDW.SetItem(i_MLEDDRow, i_MLEDDColumn, Text)
         END IF
         Visible = FALSE
         TabOrder = 0
         PostEvent("po_DDFocus")
      END IF
   END IF      
END IF
   
end subroutine

public subroutine fu_ddeditchanged (datawindow dw_name);//******************************************************************
//  PO Module     : u_MLE
//  Subroutine    : fu_DDEditChanged
//  Description   : Displays the MLE under the column after a given
//                  number of characters have been entered by the
//                  user.  
//
//  Parameters    : DATAWINDOW dw_name - Name of the DataWindow the
//                                       column is on.
//
//  Return Value  : (None)
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

IF i_DDTextLimit[i_MLEDDIndex] > 0 THEN
   IF Len(dw_name.GetText()) > i_DDTextLimit[i_MLEDDIndex] THEN
      i_CurrentDW = dw_name
      TriggerEvent("po_DDProcess")
   END IF
END IF
end subroutine

public function double fu_ddfindxpos (string dw_column);//******************************************************************
//  PO Module     : u_MLE
//  Subroutine    : fu_DDFindXPos
//  Description   : Finds the X position for the givent column when
//                  the DataWindow has a horizontal scroll bar.  
//
//  Parameters    : STRING dw_column - Name of the DataWindow 
//                                     column to position the MLE
//                                     under.
//
//  Return Value  : (None)
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER l_Pos
DOUBLE  l_ScrollMax, l_ScrollPos, l_ColumnX, l_VScrollWidth
DOUBLE  l_X, l_MaxScrollWidth, l_VisibleWidth, l_ScrollWidth
DOUBLE  l_Scrolled
STRING  l_Objects, l_Object, l_Type

l_Objects   = i_CurrentDW.Describe("datawindow.objects")
l_ScrollMax = Double(i_CurrentDW.Describe("datawindow.horizontalscrollmaximum"))
l_ScrollPos = Double(i_CurrentDW.Describe("datawindow.horizontalscrollposition"))
l_ColumnX   = Double(i_CurrentDW.Describe(dw_column + ".X"))

IF i_CurrentDW.VScrollBar THEN
   l_VScrollWidth = PixelsToUnits(16, XPixelsToUnits!)
END IF

l_MaxScrollWidth = 0
DO
   l_Pos = Pos(l_Objects, "~t")
   IF l_Pos > 0 THEN
      l_Object = MID(l_Objects, 1, l_Pos - 1)
      l_Objects = MID(l_Objects, l_Pos + 1)
   ELSE
      l_Object = l_Objects
      l_Objects = ""
   END IF
   IF Len(l_Object) > 0 THEN
      l_Type = i_CurrentDW.Describe(l_Object + ".Type")
      IF UPPER(l_Type) <> "LINE" THEN
         l_X = Double(i_CurrentDW.Describe(l_Object + ".X")) + &
               Double(i_CurrentDW.Describe(l_Object + ".Width"))
      ELSE
         l_X = Double(i_CurrentDW.Describe(l_Object + ".X2"))         
      END IF
      IF l_X > l_MaxScrollWidth THEN
         l_MaxScrollWidth = l_X
      END IF
   END IF
LOOP UNTIL l_Objects = ""

l_VisibleWidth = i_CurrentDW.Width - l_VScrollWidth
l_ScrollWidth  = l_MaxScrollWidth - l_VisibleWidth

l_Scrolled = (l_ScrollPos * l_ScrollWidth) / l_ScrollMax

RETURN i_CurrentDW.X + l_ColumnX - l_Scrolled
end function

on constructor;WINDOW     l_Window
USEROBJECT l_UserObject

//------------------------------------------------------------------
//  Identify this object as a PowerObject MLE.
//------------------------------------------------------------------

Tag = "PowerObject MLE"

//------------------------------------------------------------------
//  Make sure the tab order is 0 and the object is hidden.
//------------------------------------------------------------------

TabOrder = 0
Visible  = FALSE

//------------------------------------------------------------------
//  Determine the parent height.
//------------------------------------------------------------------

IF Parent.TypeOf() = Window! THEN
   i_Window       = Parent
   i_ParentObject = "WINDOW"
ELSE
   i_UserObject   = Parent
   i_ParentObject = "USEROBJECT"
END IF

end on

on losefocus;//******************************************************************
//  PO Module     : u_MLE
//  Event         : LoseFocus
//  Description   : When the MLE loses focus, set the text into
//                  the drop-down object or DataWindow column.
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

IF Visible AND i_MLEDDType = "OBJECT" THEN

   IF i_DDObject[i_DDIndex[i_MLEDDIndex]].Text <> Text THEN
      i_DDObject[i_DDIndex[i_MLEDDIndex]].Text = Text
   END IF
   Visible = FALSE
   TabOrder = 0
   IF GetFocus() = i_DDObject[i_DDIndex[i_MLEDDIndex]] AND NOT i_DDTabOut THEN
      i_DDObject[i_DDIndex[i_MLEDDIndex]].i_DDClosed = TRUE
   ELSE
      i_DDTabOut = FALSE
   END IF

ELSEIF Visible AND i_MLEDDType = "COLUMN" THEN

   IF i_CurrentDW.GetItemString(i_MLEDDRow, i_MLEDDColumn) <> Text THEN
      i_CurrentDW.SetItem(i_MLEDDRow, i_MLEDDColumn, Text)
   END IF
   Visible = FALSE
   TabOrder = 0
   IF NOT i_DDTabOut THEN
      i_DDClosed = TRUE
   ELSE
      i_DDTabOut = FALSE
   END IF
   IF GetFocus() = i_DDDW[i_DDIndex[i_MLEDDIndex]] THEN
      i_DDDW[i_DDIndex[i_MLEDDIndex]].SetColumn(i_MLEDDColumn)
      PostEvent("po_DDFocus")
   END IF

END IF

end on

on getfocus;//******************************************************************
//  PO Module     : u_MLE
//  Event         : GetFocus
//  Description   : Post a focus back to the drop-down object to
//                  force the drop-down to complete.
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

IF i_MLEDDType = "OBJECT" THEN
   IF NOT Visible THEN
      i_DDObject[i_DDIndex[i_MLEDDIndex]].SetFocus()
   END IF
END IF
end on

on other;IF NOT KeyDown(keyControl!) AND KeyDown(keyTab!) THEN
   i_DDTabOut = TRUE
END IF
end on

