﻿$PBExportHeader$u_progressbar.sru
$PBExportComments$Progress bar object
forward
global type u_progressbar from datawindow
end type
end forward

global type u_progressbar from datawindow
int Width=599
int Height=121
int TabOrder=1
boolean LiveScroll=true
end type
global u_progressbar u_progressbar

type variables
//-------------------------------------------------------------------------------
//  ProgressBar instance variables.
//-------------------------------------------------------------------------------

STRING		i_ProgressBGColor 		= "12632256"
STRING		i_ProgressColor 		= "12632256"
STRING		i_ProgressMeterColor 	= "16776960"
STRING		i_ProgressTextColor 	= "0"
STRING		i_ProgressBorder 		= "5"
STRING		i_ProgressDirection 	= "Horiz"
STRING		i_ProgressTextFont 	= "Arial"
STRING		i_ProgressTextSize 	= "-8"

STRING		c_DefaultFont 		= "Arial "
INTEGER		c_DefaultSize 		= 8

INTEGER		i_ProgressFontHeight 	= 9
INTEGER		i_Height
INTEGER		i_Width

BOOLEAN	i_ProgressPercent 		= TRUE
BOOLEAN	i_ProgressCreated 		= FALSE

//-------------------------------------------------------------------------------
//  ProgressBar default constants.
//-------------------------------------------------------------------------------

ULONG	c_ProgressBGGray
ULONG	c_ProgressBGBlack
ULONG	c_ProgressBGWhite
ULONG	c_ProgressBGDarkGray
ULONG	c_ProgressBGRed
ULONG	c_ProgressBGDarkRed
ULONG	c_ProgressBGGreen
ULONG	c_ProgressBGDarkGreen
ULONG	c_ProgressBGBlue
ULONG	c_ProgressBGDarkBlue
ULONG	c_ProgressBGMagenta
ULONG	c_ProgressBGDarkMagenta
ULONG	c_ProgressBGCyan
ULONG	c_ProgressBGDarkCyan
ULONG	c_ProgressBGYellow
ULONG	c_ProgressBGDarkYellow

ULONG	c_ProgressGray
ULONG	c_ProgressBlack
ULONG	c_ProgressWhite
ULONG	c_ProgressDarkGray
ULONG	c_ProgressRed
ULONG	c_ProgressDarkRed
ULONG	c_ProgressGreen
ULONG	c_ProgressDarkGreen
ULONG	c_ProgressBlue
ULONG	c_ProgressDarkBlue
ULONG	c_ProgressMagenta
ULONG	c_ProgressDarkMagenta
ULONG	c_ProgressCyan
ULONG	c_ProgressDarkCyan
ULONG	c_ProgressYellow
ULONG	c_ProgressDarkYellow

ULONG	c_ProgressTextBlack
ULONG	c_ProgressTextWhite
ULONG	c_ProgressTextGray
ULONG	c_ProgressTextDarkGray
ULONG	c_ProgressTextRed
ULONG	c_ProgressTextDarkRed
ULONG	c_ProgressTextGreen
ULONG	c_ProgressTextDarkGreen
ULONG	c_ProgressTextBlue
ULONG	c_ProgressTextDarkBlue
ULONG	c_ProgressTextMagenta
ULONG	c_ProgressTextDarkMagenta
ULONG	c_ProgressTextCyan
ULONG	c_ProgressTextDarkCyan
ULONG	c_ProgressTextYellow
ULONG	c_ProgressTextDarkYellow

ULONG	c_ProgressMeterGray
ULONG	c_ProgressMeterBlack
ULONG	c_ProgressMeterWhite
ULONG	c_ProgressMeterDarkGray
ULONG	c_ProgressMeterRed
ULONG	c_ProgressMeterDarkRed
ULONG	c_ProgressMeterGreen
ULONG	c_ProgressMeterDarkGreen
ULONG	c_ProgressMeterBlue
ULONG	c_ProgressMeterDarkBlue
ULONG	c_ProgressMeterMagenta
ULONG	c_ProgressMeterDarkMagenta
ULONG	c_ProgressMeterCyan
ULONG	c_ProgressMeterDarkCyan
ULONG	c_ProgressMeterYellow
ULONG	c_ProgressMeterDarkYellow


ULONG	c_ProgressBorderNone
ULONG	c_ProgressBorderBox
ULONG	c_ProgressBorderLowered
ULONG	c_ProgressBorderRaised
ULONG	c_ProgressBorderShadowBox

ULONG	c_ProgressDirectionHoriz
ULONG	c_ProgressDirectionVert
		
ULONG	c_ProgressPercentShow
ULONG	c_ProgressPercentHide

//-------------------------------------------------------------------------------
//  DataWindow syntax instance variable.
//-------------------------------------------------------------------------------

STRING	i_DWSyntax = 'release 4; ' + & 
	+ 'datawindow(' + &
	+ 'units=1 ' + &
	+ 'timer_interval=0 ' + &
	+ 'processing=0 ' + &
	+ 'print.documentname="" ' + &
	+ 'print.orientation = 0 ' + &
	+ 'print.margin.left = 24 ' + &
	+ 'print.margin.right = 24 ' + &
	+ 'print.margin.top = 24 ' + &
	+ 'print.margin.bottom = 24 ' + &
	+ 'print.paper.source = 0 ' + &
	+ 'print.paper.size = 0 ' 

//-------------------------------------------------------------------------------
//  Table syntax instance variable.
//-------------------------------------------------------------------------------

STRING	i_TableSyntax = 'table(column=(type=char(10) ' + &
	+ 'name=meterprogressbar ' + &
	+ 'dbname="meterprogressbar" ) ) ' + &
	+ 'data(null)' 

//-------------------------------------------------------------------------------
//  Text syntax instance variable.
//-------------------------------------------------------------------------------

STRING	i_TextSyntax = 'text(band=detail ' + &
	+ 'alignment="2" text="" ' 




end variables

forward prototypes
public function integer fu_progressoptions (string textfont, integer textsize, unsignedlong visualword)
public function integer fu_progresscreate ()
public function integer fu_setoptions (string optionstyle, string optiontype, unsignedlong visualword)
public function integer fu_setprogress (integer percent)
end prototypes

public function integer fu_progressoptions (string textfont, integer textsize, unsignedlong visualword);//******************************************************************
//  PO Module     : u_ProgressBar
//  Subroutine    : fu_ProgressOptions
//  Description   : Establishes the look-and-feel of the progress
//                  bar object.  
//
//  Parameters    : STRING  TextFont  - text font to be used for the
//												    progress bar text.
//                  INTEGER TextSize  - height of the progress bar
//													 text in pixels
//						  ULONG VisualWord  - visual options for the
//                                      progress bar.
//
//  Return Value  : (None)
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

//------------------------------------------------------------------
//  Set the font type of the progress bar text if it is different 
//  than the default.
//------------------------------------------------------------------

//IF TextFont <> c_DefaultFont THEN
	i_ProgressTextFont = TextFont
//END IF

//------------------------------------------------------------------
//  Set the font size of the progress bar text if it is different 
//  than the default.
//------------------------------------------------------------------

//IF TextSize <> c_DefaultSize THEN
	i_ProgressTextSize = STRING(TextSize * -1)
//END IF

i_ProgressFontHeight = TextSize

fu_SetOptions( "ProgressBar", "Options", VisualWord )

RETURN 0
end function

public function integer fu_progresscreate ();//******************************************************************
//  PO Module     : u_ProgressBar
//  Subroutine    : fu_ProgressCreate
//  Description   : Responsible for creating the progress bar 
//						  datawindow object.
//
//  Parameters    : (None)
//
//  Return Value  : 0 - create successful
//						 -1 - create failed 
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

STRING 	l_DWSyntax, l_BGTextSyntax, l_BarTextSyntax, l_TextSyntax
STRING 	l_HeightString
INTEGER 	l_Error, l_MeterHeight, l_MeterWidth, l_MeterY, l_MeterX
INTEGER	l_TextHeight, l_TextY, l_x, l_y

//------------------------------------------------------------------
// Build the syntax needed to create the datawindow object.
//------------------------------------------------------------------

l_HeightString = "height = " + String(Height)

l_DWSyntax = i_DWSyntax + &
				'color =' + i_ProgressBGColor + ')' + &
 				'detail(' + l_heightstring + ' color= "' + i_ProgressBGColor + '" ) '

//------------------------------------------------------------------
// Adjust the height and width of the progress bar to leave some
// room for a border.
//------------------------------------------------------------------

i_height = Height - 20
i_width =  Width - 24
l_x = 12
l_y = 6

i_Height = UnitsToPixels( i_Height, yUnitsToPixels! )
l_y = UnitsToPixels( l_y, yUnitsToPixels! )
i_Width = UnitsToPixels( i_Width, xUnitsToPixels! )
l_x = UnitsToPixels( l_x, xUnitsToPixels! )

//------------------------------------------------------------------
// Build the syntax needed to create the progress bar, which is
// a datawindow text field. 
//------------------------------------------------------------------

l_BGTextSyntax = i_TextSyntax + &
				'border="' + i_ProgressBorder + '" ' + &
				'color="' + i_ProgressColor + '" ' + &
				'x="' + String(l_x) + '" ' + &
				'y="' + String(l_y) + '" ' + &
				'height="' + String(i_Height) + '" ' + &
				'width="' + String(i_Width) + '" ' + &
				'name=ProgressBG ' + &
				'background.mode="0" ' + &
				'background.color="' + i_ProgressColor + '" ) ' 

//------------------------------------------------------------------
// Build the progress bar meter dimensions based on the direction 
// option.
//------------------------------------------------------------------

IF i_ProgressDirection = "Vert" THEN
	l_MeterHeight = 0
	l_MeterWidth = i_Width
	l_MeterX = l_X
	l_MeterY = i_Height 
ELSE
	l_MeterWidth = 0
	l_MeterHeight = i_Height
	l_MeterX = l_X
	l_MeterY = l_Y
END IF

//------------------------------------------------------------------
// Build the syntax needed to create the progress bar meter, which 
// is a datawindow text field. 
//------------------------------------------------------------------

l_BarTextSyntax = i_TextSyntax + &
				'border="0" ' + &
				'color="' + i_ProgressMeterColor + '" ' + &
				'x="' + String(l_MeterX) + '" ' + &
				'y="' + String(l_MeterY) + '" ' + &
				'height="' + String(l_MeterHeight) + '" ' + &
				'width="' + String(l_MeterWidth) + '" ' + &
				'name=ProgressBar ' + &
				'background.mode="0" ' + &
				'background.color="' + i_ProgressMeterColor + '" ) '  

//------------------------------------------------------------------
// Build the meter bar text dimensions based on the the font size
// specified.
//------------------------------------------------------------------

l_TextHeight = ( i_ProgressFontHeight ) + ( ( i_ProgressFontHeight / 2 ) + 1 )
l_TextY = ( i_height / 2 ) - ( l_TextHeight / 2 )

//------------------------------------------------------------------
// Build the syntax needed to create the progress bar text, which 
// is a datawindow text field. 
//------------------------------------------------------------------

l_TextSyntax = i_TextSyntax + &
					'border="0" ' + &
					'color="' + i_ProgressTextColor + '" ' + &
					'x="' + String(l_x) + '" ' + &
					'y="' + String(l_TextY) + '" ' + &
					'height="' + String(l_TextHeight) + '" ' + &
					'width="' + String(i_Width) + '" ' + &
					'name=ProgressText ' + &
					'font.face="' + i_ProgressTextFont + '" ' + &
					'font.height="' + String(i_ProgressTextSize) + '" ' + &
					'font.weight="400" ' + &
					'font.family="2" ' + &
					'font.pitch="2" ' + &
					'font.charset="0" ' + &
					'background.mode="1" ' +  &
					'background.color="16777215" ) '

l_DWSyntax = l_DWSyntax + i_TableSyntax + &
				 l_BGTextSyntax + l_BarTextSyntax + l_TextSyntax

//------------------------------------------------------------------
// Create the progress bar.
//------------------------------------------------------------------

l_Error = Create( l_DWSyntax )


RETURN l_Error


end function

public function integer fu_setoptions (string optionstyle, string optiontype, unsignedlong visualword);//******************************************************************
//  PO Module     : u_ProgressBar
//  Subroutine    : fu_SetOptions
//  Description   : Sets visual defaults and options. It is also 
//						  called by the constructor event to set initial 
//						  defaults.  
//
//  Parameters    : STRING OptionStyle - indicates which function
//                                       is the calling routine.
//                  STRING OptionType  - indicates whether defaults
//                                       or options are being set.
//                  ULONG  VisualWord  - visual options.
//
//  Return Value  : (None)
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

ULONG	l_BitMask

ULONG c_ProgressBGColorMask = 15
ULONG c_ProgressColorMask = 240
ULONG c_ProgressTextColorMask = 3840
ULONG c_ProgressMeterColorMask = 61440
ULONG c_ProgressBorderMask = 458752
ULONG	c_ProgressDirectionMask = 524288
ULONG c_ProgressPercentMask = 1048576

//------------------------------------------------------------------
// Set the progress bar options and defaults.
//------------------------------------------------------------------

CHOOSE CASE OptionStyle

CASE "ProgressBar"

	//--------------------------------------------------------------
	// Set the background color of the progress bar.
	//--------------------------------------------------------------

	IF OptionType = "Options" THEN
		l_BitMask = w_POManager_std.fw_BitMask(VisualWord, &
													  c_ProgressBGColorMask)
	ELSE
		l_BitMask = 0
	END IF
	CHOOSE CASE l_BitMask
		CASE c_ProgressBGGray
			i_ProgressBGColor = String(w_POManager_std.c_Gray)       
		CASE c_ProgressBGBlack  
			i_ProgressBGColor = String(w_POManager_std.c_Black)       
		CASE c_ProgressBGWhite   
			i_ProgressBGColor = String(w_POManager_std.c_White)       
		CASE c_ProgressBGDarkGray    
			i_ProgressBGColor = String(w_POManager_std.c_DarkGray)       
		CASE c_ProgressBGRed         
			i_ProgressBGColor = String(w_POManager_std.c_Red)       
		CASE c_ProgressBGDarkRed     
			i_ProgressBGColor = String(w_POManager_std.c_DarkRed)       
		CASE c_ProgressBGGreen       
			i_ProgressBGColor = String(w_POManager_std.c_Green)       
		CASE c_ProgressBGDarkGreen   
			i_ProgressBGColor = String(w_POManager_std.c_DarkGreen)       
		CASE c_ProgressBGBlue        
			i_ProgressBGColor = String(w_POManager_std.c_Blue)       
		CASE c_ProgressBGDarkBlue    
			i_ProgressBGColor = String(w_POManager_std.c_DarkBlue)       
		CASE c_ProgressBGMagenta     
			i_ProgressBGColor = String(w_POManager_std.c_Magenta)       
		CASE c_ProgressBGDarkMagenta 
			i_ProgressBGColor = String(w_POManager_std.c_DarkMagenta)       
		CASE c_ProgressBGCyan        
			i_ProgressBGColor = String(w_POManager_std.c_Cyan)       
		CASE c_ProgressBGDarkCyan    
			i_ProgressBGColor = String(w_POManager_std.c_DarkCyan)       
		CASE c_ProgressBGYellow      
			i_ProgressBGColor = String(w_POManager_std.c_Yellow)       
		CASE c_ProgressBGDarkYellow  
			i_ProgressBGColor = String(w_POManager_std.c_DarkYellow)       
	END CHOOSE

	//--------------------------------------------------------------
	// Set the color of the progress bar meter.
	//--------------------------------------------------------------

	IF OptionType = "Options" THEN
		l_BitMask = w_POManager_std.fw_BitMask(VisualWord, &
												     c_ProgressMeterColorMask)
	ELSE
		l_BitMask = 0
	END IF
	CHOOSE CASE l_BitMask
		CASE c_ProgressMeterGray
			i_ProgressMeterColor = String(w_POManager_std.c_Gray)       
		CASE c_ProgressMeterBlack  
			i_ProgressMeterColor = String(w_POManager_std.c_Black)       
		CASE c_ProgressMeterWhite   
			i_ProgressMeterColor = String(w_POManager_std.c_White)       
		CASE c_ProgressMeterDarkGray    
			i_ProgressMeterColor = String(w_POManager_std.c_DarkGray)       
		CASE c_ProgressMeterRed         
			i_ProgressMeterColor = String(w_POManager_std.c_Red)       
		CASE c_ProgressMeterDarkRed     
			i_ProgressMeterColor = String(w_POManager_std.c_DarkRed)       
		CASE c_ProgressMeterGreen       
			i_ProgressMeterColor = String(w_POManager_std.c_Green)       
		CASE c_ProgressMeterDarkGreen   
			i_ProgressMeterColor = String(w_POManager_std.c_DarkGreen)       
		CASE c_ProgressMeterBlue        
			i_ProgressMeterColor = String(w_POManager_std.c_Blue)       
		CASE c_ProgressMeterDarkBlue    
			i_ProgressMeterColor = String(w_POManager_std.c_DarkBlue)       
		CASE c_ProgressMeterMagenta     
			i_ProgressMeterColor = String(w_POManager_std.c_Magenta)       
		CASE c_ProgressMeterDarkMagenta 
			i_ProgressMeterColor = String(w_POManager_std.c_DarkMagenta)       
		CASE c_ProgressMeterCyan        
			i_ProgressMeterColor = String(w_POManager_std.c_Cyan)       
		CASE c_ProgressMeterDarkCyan    
			i_ProgressMeterColor = String(w_POManager_std.c_DarkCyan)       
		CASE c_ProgressMeterYellow      
			i_ProgressMeterColor = String(w_POManager_std.c_Yellow)       
		CASE c_ProgressMeterDarkYellow  
			i_ProgressMeterColor = String(w_POManager_std.c_DarkYellow)       
	END CHOOSE

	//--------------------------------------------------------------
	// Set the color of the progress bar text.
	//--------------------------------------------------------------

	IF OptionType = "Options" THEN
		l_BitMask = w_POManager_std.fw_BitMask(VisualWord, &
													  c_ProgressTextColorMask)
	ELSE
		l_BitMask = 0
	END IF
	CHOOSE CASE l_BitMask
		CASE c_ProgressTextGray
			i_ProgressTextColor = String(w_POManager_std.c_Gray)       
		CASE c_ProgressTextBlack  
			i_ProgressTextColor = String(w_POManager_std.c_Black)       
		CASE c_ProgressTextWhite   
			i_ProgressTextColor = String(w_POManager_std.c_White)       
		CASE c_ProgressTextDarkGray    
			i_ProgressTextColor = String(w_POManager_std.c_DarkGray)       
		CASE c_ProgressTextRed         
			i_ProgressTextColor = String(w_POManager_std.c_Red)       
		CASE c_ProgressTextDarkRed     
			i_ProgressTextColor = String(w_POManager_std.c_DarkRed)       
		CASE c_ProgressTextGreen       
			i_ProgressTextColor = String(w_POManager_std.c_Green)       
		CASE c_ProgressTextDarkGreen   
			i_ProgressTextColor = String(w_POManager_std.c_DarkGreen)       
		CASE c_ProgressTextBlue        
			i_ProgressTextColor = String(w_POManager_std.c_Blue)       
		CASE c_ProgressTextDarkBlue    
			i_ProgressTextColor = String(w_POManager_std.c_DarkBlue)       
		CASE c_ProgressTextMagenta     
			i_ProgressTextColor = String(w_POManager_std.c_Magenta)       
		CASE c_ProgressTextDarkMagenta 
			i_ProgressTextColor = String(w_POManager_std.c_DarkMagenta)       
		CASE c_ProgressTextCyan        
			i_ProgressTextColor = String(w_POManager_std.c_Cyan)       
		CASE c_ProgressTextDarkCyan    
			i_ProgressTextColor = String(w_POManager_std.c_DarkCyan)       
		CASE c_ProgressTextYellow      
			i_ProgressTextColor = String(w_POManager_std.c_Yellow)       
		CASE c_ProgressTextDarkYellow  
			i_ProgressTextColor = String(w_POManager_std.c_DarkYellow)       
	END CHOOSE

	//--------------------------------------------------------------
	// Set the color of the progress bar.
	//--------------------------------------------------------------

	IF OptionType = "Options" THEN
		l_BitMask = w_POManager_std.fw_BitMask(VisualWord, &
													  c_ProgressColorMask)
	ELSE
		l_BitMask = 0
	END IF
	CHOOSE CASE l_BitMask
		CASE c_ProgressGray
			i_ProgressColor = String(w_POManager_std.c_Gray)       
		CASE c_ProgressBlack  
			i_ProgressColor = String(w_POManager_std.c_Black)       
		CASE c_ProgressWhite   
			i_ProgressColor = String(w_POManager_std.c_White)       
		CASE c_ProgressDarkGray    
			i_ProgressColor = String(w_POManager_std.c_DarkGray)       
		CASE c_ProgressRed         
			i_ProgressColor = String(w_POManager_std.c_Red)       
		CASE c_ProgressDarkRed     
			i_ProgressColor = String(w_POManager_std.c_DarkRed)       
		CASE c_ProgressGreen       
			i_ProgressColor = String(w_POManager_std.c_Green)       
		CASE c_ProgressDarkGreen   
			i_ProgressColor = String(w_POManager_std.c_DarkGreen)       
		CASE c_ProgressBlue        
			i_ProgressColor = String(w_POManager_std.c_Blue)       
		CASE c_ProgressDarkBlue    
			i_ProgressColor = String(w_POManager_std.c_DarkBlue)       
		CASE c_ProgressMagenta     
			i_ProgressColor = String(w_POManager_std.c_Magenta)       
		CASE c_ProgressDarkMagenta 
			i_ProgressColor = String(w_POManager_std.c_DarkMagenta)       
		CASE c_ProgressCyan        
			i_ProgressColor = String(w_POManager_std.c_Cyan)       
		CASE c_ProgressDarkCyan    
			i_ProgressColor = String(w_POManager_std.c_DarkCyan)       
		CASE c_ProgressYellow      
			i_ProgressColor = String(w_POManager_std.c_Yellow)       
		CASE c_ProgressDarkYellow  
			i_ProgressColor = String(w_POManager_std.c_DarkYellow)       
	END CHOOSE

	//--------------------------------------------------------------
	// Set the border of the progress bar.
	//--------------------------------------------------------------

	IF OptionType = "Options" THEN
		l_BitMask = w_POManager_std.fw_BitMask(VisualWord, & 
													  c_ProgressBorderMask)
	ELSE
		l_BitMask = 0
	END IF
	CHOOSE CASE l_BitMask
		CASE c_ProgressBorderNone
			i_ProgressBorder = "0" 
		CASE c_ProgressBorderShadowBox        
			i_ProgressBorder = "1"      
		CASE c_ProgressBorderBox
			i_ProgressBorder = "2"      
		CASE c_ProgressBorderLowered
			i_ProgressBorder = "5"     
		CASE c_ProgressBorderRaised   
			i_ProgressBorder = "6"       
	END CHOOSE

	//--------------------------------------------------------------
	// Set the direction of the progress bar.
	//--------------------------------------------------------------

	IF OptionType = "Options" THEN
		l_BitMask = w_POManager_std.fw_BitMask(VisualWord, &
													  c_ProgressDirectionMask)
	ELSE
		l_BitMask = 0
	END IF
	CHOOSE CASE l_BitMask
		CASE c_ProgressDirectionHoriz
			i_ProgressDirection = "Horiz"      
		CASE c_ProgressDirectionVert
			i_ProgressDirection = "Vert"      
	END CHOOSE

	//--------------------------------------------------------------
	// Determine whether to display or hide the percentage text.
	//--------------------------------------------------------------

	IF OptionType = "Options" THEN
		l_BitMask = w_POManager_std.fw_BitMask(VisualWord, &
													  c_ProgressPercentMask)
	ELSE
		l_BitMask = 0
	END IF
	CHOOSE CASE l_BitMask
		CASE c_ProgressPercentShow
			i_ProgressPercent = TRUE      
		CASE c_ProgressPercentHide
			i_ProgressPercent = FALSE    
	END CHOOSE

END CHOOSE

RETURN 0
end function

public function integer fu_setprogress (integer percent);//******************************************************************
//  PO Module     : u_ProgressBar
//  Subroutine    : fu_SetProgress
//  Description   : Responsible for showing the percentage displayed
//					     on the progress bar.
//
//  Parameters    : INTEGER	percent - percentage displayed on the
//												    progress bar.
//
//  Return Value  : 0 - create successful
//						 -1 - create failed 
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

STRING	l_Text, l_Error, l_ModifyString
INTEGER	l_MeterHeight, l_MeterWidth, l_MeterY, l_MeterX
DECIMAL	l_percent

//------------------------------------------------------------------
//	Convert the percentage to decimal.
//------------------------------------------------------------------

l_percent = percent / 100

//------------------------------------------------------------------
//	Build the percentage string to display on the progress bar.
//------------------------------------------------------------------

IF i_ProgressPercent THEN
	l_Text = STRING(percent) + "%"
ELSE
	l_Text = ""
END IF

//------------------------------------------------------------------
// Build the progress bar meter dimensions based on the percentage
// and direction options specified. 
//------------------------------------------------------------------

IF i_ProgressDirection = "Vert" THEN
	l_MeterHeight = i_Height * l_Percent
	l_MeterY = i_Height - l_MeterHeight + 1
	l_ModifyString = "ProgressBar.height = " + &
							String(l_MeterHeight) +  &
							"ProgressBar.Y = " + &
							String(l_MeterY) + &
						   "ProgressText.text = " + "'" + l_Text + "'"
	l_Error = Modify( l_ModifyString )
	IF l_Error <> "" THEN
		RETURN -1
	END IF
ELSE
	l_MeterWidth = i_Width * l_Percent
	l_ModifyString = "ProgressBar.width = " + &
						   String(l_MeterWidth) + &
							" ProgressText.text = " + "'" + l_Text + "'"
	l_Error = Modify( l_ModifyString )
	IF l_Error <> "" THEN
		RETURN -1
	END IF
END IF

SetReDraw(TRUE)

RETURN 0 


end function

on constructor;//******************************************************************
//  PO Module     : u_ProgressBar
//  Event         : Constructor
//  Description   : Initializes the default progress bar options. 
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER l_Idx

//------------------------------------------------------------------
// Identify this object as a PowerObject ProgressBar
//------------------------------------------------------------------

Tag = "PowerObject Progress Bar"

//------------------------------------------------------------------
// Make sure the border of the progress bar control is turned off.
//------------------------------------------------------------------

Border = FALSE

//------------------------------------------------------------------
// Initialize the progress bar options
//------------------------------------------------------------------

//------------------------------------------------------------------
// Progress Bar background color.
//------------------------------------------------------------------

c_ProgressBGGray			= w_POManager_std.c_ProgressBGGray
c_ProgressBGBlack			= w_POManager_std.c_ProgressBGBlack
c_ProgressBGWhite       = w_POManager_std.c_ProgressBGWhite
c_ProgressBGDarkGray    = w_POManager_std.c_ProgressBGDarkGray
c_ProgressBGRed         = w_POManager_std.c_ProgressBGRed
c_ProgressBGDarkRed     = w_POManager_std.c_ProgressBGDarkRed
c_ProgressBGGreen       = w_POManager_std.c_ProgressBGGreen
c_ProgressBGDarkGreen   = w_POManager_std.c_ProgressBGDarkGreen
c_ProgressBGBlue        = w_POManager_std.c_ProgressBGBlue
c_ProgressBGDarkBlue    = w_POManager_std.c_ProgressBGDarkBlue
c_ProgressBGMagenta     = w_POManager_std.c_ProgressBGMagenta
c_ProgressBGDarkMagenta = w_POManager_std.c_ProgressBGDarkMagenta
c_ProgressBGCyan        = w_POManager_std.c_ProgressBGCyan
c_ProgressBGDarkCyan    = w_POManager_std.c_ProgressBGDarkCyan
c_ProgressBGYellow      = w_POManager_std.c_ProgressBGYellow
c_ProgressBGDarkYellow  = w_POManager_std.c_ProgressBGDarkYellow

//------------------------------------------------------------------
// Progress Bar color.
//------------------------------------------------------------------

c_ProgressGray				= w_POManager_std.c_ProgressGray
c_ProgressBlack			= w_POManager_std.c_ProgressBlack
c_ProgressWhite			= w_POManager_std.c_ProgressWhite
c_ProgressDarkGray		= w_POManager_std.c_ProgressDarkGray
c_ProgressRed				= w_POManager_std.c_ProgressRed
c_ProgressDarkRed			= w_POManager_std.c_ProgressDarkRed
c_ProgressGreen			= w_POManager_std.c_ProgressGreen
c_ProgressDarkGreen		= w_POManager_std.c_ProgressDarkGreen
c_ProgressBlue				= w_POManager_std.c_ProgressBlue
c_ProgressDarkBlue		= w_POManager_std.c_ProgressDarkBlue
c_ProgressMagenta			= w_POManager_std.c_ProgressMagenta
c_ProgressDarkMagenta	= w_POManager_std.c_ProgressDarkMagenta
c_ProgressCyan				= w_POManager_std.c_ProgressCyan
c_ProgressDarkCyan		= w_POManager_std.c_ProgressDarkCyan
c_ProgressYellow			= w_POManager_std.c_ProgressYellow
c_ProgressDarkYellow		= w_POManager_std.c_ProgressDarkYellow

//------------------------------------------------------------------
// Progress Bar text color.
//------------------------------------------------------------------

c_ProgressTextBlack		= w_POManager_std.c_ProgressTextBlack
c_ProgressTextWhite		= w_POManager_std.c_ProgressTextWhite
c_ProgressTextGray		= w_POManager_std.c_ProgressTextGray
c_ProgressTextDarkGray	= w_POManager_std.c_ProgressTextDarkGray
c_ProgressTextRed			= w_POManager_std.c_ProgressTextRed
c_ProgressTextDarkRed	= w_POManager_std.c_ProgressTextDarkRed
c_ProgressTextGreen		= w_POManager_std.c_ProgressTextGreen
c_ProgressTextDarkGreen	= w_POManager_std.c_ProgressTextDarkGreen
c_ProgressTextBlue		= w_POManager_std.c_ProgressTextBlue
c_ProgressTextDarkBlue	= w_POManager_std.c_ProgressTextDarkBlue
c_ProgressTextMagenta	= w_POManager_std.c_ProgressTextMagenta
c_ProgressTextDarkMagenta	= w_POManager_std.c_ProgressTextDarkMagenta
c_ProgressTextCyan		= w_POManager_std.c_ProgressTextCyan
c_ProgressTextDarkCyan	= w_POManager_std.c_ProgressTextDarkCyan
c_ProgressTextYellow		= w_POManager_std.c_ProgressTextYellow
c_ProgressTextDarkYellow	= w_POManager_std.c_ProgressTextDarkYellow

//------------------------------------------------------------------
// Progress Bar meter color.
//------------------------------------------------------------------

c_ProgressMeterGray			= w_POManager_std.c_ProgressMeterGray
c_ProgressMeterBlack			= w_POManager_std.c_ProgressMeterBlack
c_ProgressMeterWhite			= w_POManager_std.c_ProgressMeterWhite
c_ProgressMeterDarkGray		= w_POManager_std.c_ProgressMeterDarkGray
c_ProgressMeterRed			= w_POManager_std.c_ProgressMeterRed
c_ProgressMeterDarkRed		= w_POManager_std.c_ProgressMeterDarkRed
c_ProgressMeterGreen			= w_POManager_std.c_ProgressMeterGreen
c_ProgressMeterDarkGreen	= w_POManager_std.c_ProgressMeterDarkGreen
c_ProgressMeterBlue			= w_POManager_std.c_ProgressMeterBlue
c_ProgressMeterDarkBlue		= w_POManager_std.c_ProgressMeterDarkBlue
c_ProgressMeterMagenta		= w_POManager_std.c_ProgressMeterMagenta
c_ProgressMeterDarkMagenta	= w_POManager_std.c_ProgressMeterDarkMagenta
c_ProgressMeterCyan			= w_POManager_std.c_ProgressMeterCyan
c_ProgressMeterDarkCyan		= w_POManager_std.c_ProgressMeterDarkCyan
c_ProgressMeterYellow		= w_POManager_std.c_ProgressMeterYellow
c_ProgressMeterDarkYellow	= w_POManager_std.c_ProgressMeterDarkYellow

//------------------------------------------------------------------
// Progress Bar border type.
//------------------------------------------------------------------

c_ProgressBorderNone			= w_POManager_std.c_ProgressBorderNone
c_ProgressBorderShadowBox	= w_POManager_std.c_ProgressBorderShadowBox
c_ProgressBorderBox			= w_POManager_std.c_ProgressBorderBox
c_ProgressBorderLowered		= w_POManager_std.c_ProgressBorderLowered
c_ProgressBorderRaised		= w_POManager_std.c_ProgressBorderRaised

//------------------------------------------------------------------
// Progress Bar direction.
//------------------------------------------------------------------

c_ProgressDirectionHoriz	= w_POManager_std.c_ProgressDirectionHoriz
c_ProgressDirectionVert		= w_POManager_std.c_ProgressDirectionVert
		
//------------------------------------------------------------------
// Show or Hide ProgressBar percent.
//------------------------------------------------------------------

c_ProgressPercentShow		= w_POManager_std.c_ProgressPercentShow
c_ProgressPercentHide		= w_POManager_std.c_ProgressPercentHide


fu_SetOptions("ProgressBar", "Defaults", 0)

end on

