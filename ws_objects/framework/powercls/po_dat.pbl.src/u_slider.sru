﻿$PBExportHeader$u_slider.sru
$PBExportComments$Slider object
forward
global type u_slider from datawindow
end type
end forward

global type u_slider from datawindow
int Width=599
int Height=121
int TabOrder=1
boolean LiveScroll=true
event po_slidermoving pbm_dwnmousemove
event po_slidermoved pbm_lbuttonup
end type
global u_slider u_slider

type variables
//-----------------------------------------------------------------------------
//  Slider instance variables.
//-----------------------------------------------------------------------------

STRING  		i_SliderIndicator 		= "sliderup.bmp" 
STRING		i_SliderBGColor 		= "12632256"
STRING		i_SliderFrameColor 		= "12632256"
STRING		i_SliderBarColor 		= "16776960"
STRING		i_SliderCenterLineColor 	= "0"
STRING		i_SliderIndicatorPosition 	= "Out"

STRING		i_SliderFrameBorder 	= "1"
STRING		i_SliderBorder 		= "6"
STRING		i_SliderDirection 		= "Horiz"
BOOLEAN	i_SliderCenterLine 		= FALSE

//-----------------------------------------------------------------------------
//  Slider Tick instance variables.
//-----------------------------------------------------------------------------

DECIMAL		i_SliderMajorIncr 		= 1
DECIMAL		i_SliderMinorIncr 		= 0
STRING		i_SliderTickColor 		= "0"

//-----------------------------------------------------------------------------
//  Slider Text instance variables.
//-----------------------------------------------------------------------------

STRING		i_SliderTextSize 		= "-8"
STRING		i_SliderTextFont 		= "Arial"	
STRING 	 	i_SliderTextColor 		= "0"
BOOLEAN	i_SliderText 		= FALSE

INTEGER		i_SliderPosition 		= 0

//-----------------------------------------------------------------------------
//  Slider information instance variables.
//-----------------------------------------------------------------------------

INTEGER		i_Height
INTEGER		i_Width
INTEGER		i_StartPoint
INTEGER		i_EndPoint
INTEGER		i_BitmapHeight
INTEGER		i_BitmapWidth
INTEGER		i_BitmapX
INTEGER		i_BitmapY
INTEGER		i_TotalPoint
INTEGER		i_BarWidth
INTEGER		i_BarHeight
INTEGER		i_BarX
INTEGER		i_BarY
BOOLEAN	i_SliderMoving
INTEGER		i_PrevPointerX
INTEGER		i_PrevPointerY
INTEGER		i_PointerOffset

//-----------------------------------------------------------------------------
//  DataWindow syntax instance variable.
//-----------------------------------------------------------------------------

STRING	i_DWSyntax = 'release 4; ' + & 
	+ 'datawindow(' + &
	+ 'units=1 ' + &
	+ 'timer_interval=0 ' + &
	+ 'processing=0 ' + &
	+ 'print.documentname="" ' + &
	+ 'print.orientation = 0 ' + &
	+ 'print.margin.left = 24 ' + &
	+ 'print.margin.right = 24 ' + &
	+ 'print.margin.top = 24 ' + &
	+ 'print.margin.bottom = 24 ' + &
	+ 'print.paper.source = 0 ' + &
	+ 'print.paper.size = 0 ' 

//-----------------------------------------------------------------------------
//  DataWindow table syntax instance variable.
//-----------------------------------------------------------------------------

STRING	i_TableSyntax = 'table(column=(type=char(10) ' + &
	+ 'name=Slider ' + &
	+ 'dbname="Slider" ) ) ' + &
	+ 'data(null)' 

//-----------------------------------------------------------------------------
//  DataWindowtext syntax instance variable.
//-----------------------------------------------------------------------------

STRING	i_TextSyntax = 'text(band=detail ' + &
	+ 'alignment="2" text="" ' 

//-----------------------------------------------------------------------------
//  Slider default constants.
//-----------------------------------------------------------------------------

STRING		c_DefaultIndicator 		= "sliderup.bmp"
LONG		c_DefaultMajorIncr 	= 1
LONG		c_DefaultMinorIncr 	= 0
STRING		c_DefaultFont 		= "Arial"
INTEGER		c_DefaultSize 		= 8

//-----------------------------------------------------------------------------
//  Slider option constants.
//-----------------------------------------------------------------------------

ULONG		c_SliderBGGray
ULONG		c_SliderBGBlack
ULONG		c_SliderBGWhite
ULONG		c_SliderBGDarkGray
ULONG		c_SliderBGRed
ULONG		c_SliderBGDarkRed
ULONG		c_SliderBGGreen
ULONG		c_SliderBGDarkGreen
ULONG		c_SliderBGBlue	
ULONG		c_SliderBGDarkBlue
ULONG		c_SliderBGMagenta
ULONG		c_SliderBGDarkMagenta
ULONG		c_SliderBGCyan
ULONG		c_SliderBGDarkCyan
ULONG		c_SliderBGYellow
ULONG		c_SliderBGDarkYellow

ULONG		c_SliderFrameGray
ULONG		c_SliderFrameBlack
ULONG		c_SliderFrameWhite
ULONG		c_SliderFrameDarkGray
ULONG		c_SliderFrameRed
ULONG		c_SliderFrameDarkRed
ULONG		c_SliderFrameGreen
ULONG		c_SliderFrameDarkGreen
ULONG		c_SliderFrameBlue
ULONG		c_SliderFrameDarkBlue
ULONG		c_SliderFrameMagenta
ULONG		c_SliderFrameDarkMagenta
ULONG		c_SliderFrameCyan
ULONG		c_SliderFrameDarkCyan
ULONG		c_SliderFrameYellow
ULONG		c_SliderFrameDarkYellow

ULONG		c_SliderBarBlack
ULONG		c_SliderBarWhite
ULONG		c_SliderBarGray
ULONG		c_SliderBarDarkGray
ULONG		c_SliderBarRed
ULONG		c_SliderBarDarkRed
ULONG		c_SliderBarGreen
ULONG		c_SliderBarDarkGreen
ULONG		c_SliderBarBlue
ULONG		c_SliderBarDarkBlue
ULONG		c_SliderBarMagenta
ULONG		c_SliderBarDarkMagenta
ULONG		c_SliderBarCyan
ULONG		c_SliderBarDarkCyan
ULONG		c_SliderBarYellow
ULONG		c_SliderBarDarkYellow

ULONG		c_SliderCenterLineBlue
ULONG		c_SliderCenterLineWhite
ULONG		c_SliderCenterLineGray
ULONG		c_SliderCenterLineDarkGray
ULONG		c_SliderCenterLineRed
ULONG		c_SliderCenterLineDarkRed
ULONG		c_SliderCenterLineGreen
ULONG		c_SliderCenterLineDarkGreen
ULONG		c_SliderCenterLineBlack
ULONG		c_SliderCenterLineDarkBlue
ULONG		c_SliderCenterLineMagenta
ULONG		c_SliderCenterLineDarkMagenta
ULONG		c_SliderCenterLineCyan
ULONG		c_SliderCenterLineDarkCyan
ULONG		c_SliderCenterLineYellow
ULONG		c_SliderCenterLineDarkYellow


ULONG		c_SliderFrameNone
ULONG		c_SliderFrameShadowBox
ULONG		c_SliderFrameBox
ULONG		c_SliderFrameLowered
ULONG		c_SliderFrameRaised

ULONG		c_SliderBorderNone
ULONG		c_SliderBorderShadowBox
ULONG		c_SliderBorderBox
ULONG		c_SliderBorderLowered
ULONG		c_SliderBorderRaised

ULONG		c_SliderCenterLineShow
ULONG		c_SliderCenterLineHide

ULONG		c_SliderDirectionHoriz
ULONG		c_SliderDirectionVert

ULONG		c_SliderIndicatorOut
ULONG		c_SliderIndicatorIn

//-----------------------------------------------------------------------------
//  Slider text options constants.
//-----------------------------------------------------------------------------

ULONG		c_SliderTextGray
ULONG		c_SliderTextBlack
ULONG		c_SliderTextWhite
ULONG		c_SliderTextDarkGray
ULONG		c_SliderTextRed
ULONG		c_SliderTextDarkRed
ULONG		c_SliderTextGreen
ULONG		c_SliderTextDarkGreen
ULONG		c_SliderTextBlue
ULONG		c_SliderTextDarkBlue
ULONG		c_SliderTextMagenta
ULONG		c_SliderTextDarkMagenta
ULONG		c_SliderTextCyan
ULONG		c_SliderTextDarkCyan
ULONG		c_SliderTextYellow
ULONG		c_SliderTextDarkYellow

ULONG		c_SliderTextShow
ULONG		c_SliderTextHide

//-----------------------------------------------------------------------------
//  Slider tick options constants.
//-----------------------------------------------------------------------------

ULONG		c_SliderTickGray
ULONG		c_SliderTickBlack
ULONG		c_SliderTickWhite
ULONG		c_SliderTickDarkGray
ULONG		c_SliderTickRed
ULONG		c_SliderTickDarkRed
ULONG		c_SliderTickGreen
ULONG		c_SliderTickDarkGreen
ULONG		c_SliderTickBlue
ULONG		c_SliderTickDarkBlue
ULONG		c_SliderTickMagenta
ULONG		c_SliderTickDarkMagenta
ULONG		c_SliderTickCyan
ULONG		c_SliderTickDarkCyan
ULONG		c_SliderTickYellow
ULONG		c_SliderTickDarkYellow








end variables

forward prototypes
public function integer fu_setoptions (string optionstyle, string optiontype, unsignedlong visualword)
public function integer fu_slidercreate (decimal startpoint, decimal endpoint)
public function integer fu_slidertextoptions (string textfont, integer textsize, long visualword)
public function decimal fu_getslider ()
public function integer fu_slideroptions (string indicator, unsignedlong visualword)
public function integer fu_slidertickoptions (decimal majorincrement, decimal minorincrement, unsignedlong visualword)
public function integer fu_setslider (decimal position)
end prototypes

on po_slidermoving;//******************************************************************
//  PO Module     : u_Slider
//  Event         : po_slidermoving
//  Description   : Moves the slider indicator as the mouse is
//						  dragged over it. 
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER	l_PointerX, l_PointerY, l_Width, l_Height
INTEGER	l_NewPos, l_X, l_Y
STRING	l_ModifyString, l_Error

//------------------------------------------------------------------
//	Only allow the indicator to be moved if i_SliderMoving has been
// set to TRUE in the clicked event.
//------------------------------------------------------------------

IF i_SliderMoving THEN
	l_PointerX = UnitsToPixels( (PointerX()), xUnitsToPixels! )
	l_PointerY = UnitsToPixels( (PointerY()), yUnitsToPixels! )

	//------------------------------------------------------------
	//	Only allow the indicator to be moved if the mouse cursor
	// is inside the slider object.
	//------------------------------------------------------------

	IF PointerX() >= 0 and PointerX() <= Width and &
	   PointerY() >= 0 and PointerY() <= Height THEN

		//---------------------------------------------------------
		//	Determine the new coordinates for the indicator
		// based upon the direction of the slider and whether the
		// indicator is inside or outside the bar. Check the 
		// boundaries of the slider bar, to make sure that the
		// indicator cannot move outside them.  
		//---------------------------------------------------------

		IF i_SliderDirection = "Horiz" THEN
			IF i_PrevPointerX <> l_PointerX THEN
				i_BitmapX = l_PointerX - i_PointerOffset
				IF i_SliderIndicatorPosition = "Out" THEN
					IF i_BitmapX + ( i_BitmapWidth / 2 ) < i_BarX THEN
						i_BitmapX = i_BarX - ( i_BitMapWidth / 2 )
					ELSEIF i_BitmapX + ( i_BitmapWidth / 2 ) > &
												i_BarX + i_BarWidth THEN
						i_BitmapX = i_BarX + i_BarWidth - &
										( i_BitmapWidth / 2 )
					END IF
				ELSE
					IF i_BitmapX < i_BarX THEN
						i_BitmapX = i_BarX 
					ELSEIF i_BitmapX + ( i_BitmapWidth ) > &
												i_BarX + i_BarWidth THEN
						i_BitmapX = i_BarX + i_BarWidth - &
										( i_BitmapWidth )
					END IF
				END IF
				l_ModifyString = "sliderbmp.x =" + String(i_BitmapX)
				l_Error = Modify( l_ModifyString )
				i_PrevPointerX = l_PointerX
				SetReDraw(TRUE)
			END IF
		ELSE
			IF i_PrevPointerY <> l_PointerY THEN
				i_BitmapY = l_PointerY - i_PointerOffset
				IF i_SliderIndicatorPosition = "Out" THEN
					IF i_BitmapY + i_BitmapHeight / 2 < i_BarY THEN
						i_BitmapY = i_BarY - i_BitmapHeight / 2
					ELSEIF i_BitmapY + i_BitmapHeight / 2 > &
							 i_BarY + i_BarHeight THEN
						i_BitmapY = i_BarY + i_BarHeight - &
										( i_BitmapHeight / 2 )
					END IF
				ELSE
					IF i_BitmapY < i_BarY THEN
						i_BitmapY = i_BarY 
					ELSEIF i_BitmapY + i_BitmapHeight > &
							 i_BarY + i_BarHeight THEN
						i_BitmapY = i_BarY + i_BarHeight - &
										( i_BitmapHeight )
					END IF
				END IF
				l_ModifyString = "sliderbmp.y =" + String(i_BitmapY)
				l_Error = Modify( l_ModifyString )
				i_PrevPointerY = l_PointerY
				SetReDraw(TRUE)
			END IF
		END IF
	END IF
END IF
end on

on po_slidermoved;//******************************************************************
//  PO Module     : u_Slider
//  Event         : po_slidermoved
//  Description   : Releases the mouse from being captured by the
//						  slider object and sets i_SliderMoving to FALSE
//						  when the mouse is released.
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

w_POManager_std.EXT.fu_ReleaseCapture()
i_SliderMoving = FALSE
end on

public function integer fu_setoptions (string optionstyle, string optiontype, unsignedlong visualword);//******************************************************************
//  PO Module     : u_Slider
//  Subroutine    : fu_SetOptions
//  Description   : Sets visual defaults and options.  This function
//                  is used by all the option functions (i.e.
//                  fu_SliderOptions).  It is also called by the
//                  constructor event to set initial defaults.  
//
//  Parameters    : STRING OptionStyle - indicates which function
//                                       is the calling routine.
//                         OptionType  - indicates whether defaults
//                                       or options are being set.
//                  ULONG  VisualWord  - visual options.
//
//  Return Value  : (None)
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************


ULONG	l_BitMask

ULONG c_SliderBGColorMask = 15
ULONG c_SliderFrameColorMask = 240
ULONG c_SliderBarColorMask = 3840
ULONG c_SliderCenterLineColorMask = 61440
ULONG	c_SliderFrameMask = 458752
ULONG	c_SliderBorderMask = 3670016
ULONG	c_SliderCenterLineMask = 4194304
ULONG	c_SliderDirectionMask = 8388608
ULONG	c_SliderIndicatorMask = 16777216

ULONG c_SliderTextColorMask = 15
ULONG	c_SliderTextMask = 32

ULONG c_SliderTickColorMask = 15

//------------------------------------------------------------------
//  Set slider options and defaults.
//------------------------------------------------------------------

CHOOSE CASE OptionStyle

CASE "Slider"

	//--------------------------------------------------------------
	//  Set the background color of the slider.
	//--------------------------------------------------------------

	IF OptionType = "Options" THEN
		l_BitMask = w_POManager_std.fw_BitMask(VisualWord, &
						c_SliderBGColorMask)
	ELSE
		l_BitMask = 0
	END IF
	CHOOSE CASE l_BitMask
		CASE c_SliderBGGray
			i_SliderBGColor = String(w_POManager_std.c_Gray)       
		CASE c_SliderBGBlack  
			i_SliderBGColor = String(w_POManager_std.c_Black)       
		CASE c_SliderBGWhite   
			i_SliderBGColor = String(w_POManager_std.c_White)       
		CASE c_SliderBGDarkGray    
			i_SliderBGColor = String(w_POManager_std.c_DarkGray)       
		CASE c_SliderBGRed         
			i_SliderBGColor = String(w_POManager_std.c_Red)       
		CASE c_SliderBGDarkRed     
			i_SliderBGColor = String(w_POManager_std.c_DarkRed)       
		CASE c_SliderBGGreen       
			i_SliderBGColor = String(w_POManager_std.c_Green)       
		CASE c_SliderBGDarkGreen   
			i_SliderBGColor = String(w_POManager_std.c_DarkGreen)       
		CASE c_SliderBGBlue        
			i_SliderBGColor = String(w_POManager_std.c_Blue)       
		CASE c_SliderBGDarkBlue    
			i_SliderBGColor = String(w_POManager_std.c_DarkBlue)       
		CASE c_SliderBGMagenta     
			i_SliderBGColor = String(w_POManager_std.c_Magenta)       
		CASE c_SliderBGDarkMagenta 
			i_SliderBGColor = String(w_POManager_std.c_DarkMagenta)       
		CASE c_SliderBGCyan        
			i_SliderBGColor = String(w_POManager_std.c_Cyan)       
		CASE c_SliderBGDarkCyan    
			i_SliderBGColor = String(w_POManager_std.c_DarkCyan)       
		CASE c_SliderBGYellow      
			i_SliderBGColor = String(w_POManager_std.c_Yellow)       
		CASE c_SliderBGDarkYellow  
			i_SliderBGColor = String(w_POManager_std.c_DarkYellow)       
	END CHOOSE

	//--------------------------------------------------------------
	//  Set the frame color of the slider.
	//--------------------------------------------------------------

	IF OptionType = "Options" THEN
		l_BitMask = w_POManager_std.fw_BitMask(VisualWord, &
														c_SliderFrameColorMask)
	ELSE
		l_BitMask = 0
	END IF
	CHOOSE CASE l_BitMask
		CASE c_SliderFrameGray
			i_SliderFrameColor = String(w_POManager_std.c_Gray)       
		CASE c_SliderFrameBlack  
			i_SliderFrameColor = String(w_POManager_std.c_Black)       
		CASE c_SliderFrameWhite   
			i_SliderFrameColor = String(w_POManager_std.c_White)       
		CASE c_SliderFrameDarkGray    
			i_SliderFrameColor = String(w_POManager_std.c_DarkGray)       
		CASE c_SliderFrameRed         
			i_SliderFrameColor = String(w_POManager_std.c_Red)       
		CASE c_SliderFrameDarkRed     
			i_SliderFrameColor = String(w_POManager_std.c_DarkRed)       
		CASE c_SliderFrameGreen       
			i_SliderFrameColor = String(w_POManager_std.c_Green)       
		CASE c_SliderFrameDarkGreen   
			i_SliderFrameColor = String(w_POManager_std.c_DarkGreen)       
		CASE c_SliderFrameBlue        
			i_SliderFrameColor = String(w_POManager_std.c_Blue)       
		CASE c_SliderFrameDarkBlue    
			i_SliderFrameColor = String(w_POManager_std.c_DarkBlue)       
		CASE c_SliderFrameMagenta     
			i_SliderFrameColor = String(w_POManager_std.c_Magenta)       
		CASE c_SliderFrameDarkMagenta 
			i_SliderFrameColor = String(w_POManager_std.c_DarkMagenta)       
		CASE c_SliderFrameCyan        
			i_SliderFrameColor = String(w_POManager_std.c_Cyan)       
		CASE c_SliderFrameDarkCyan    
			i_SliderFrameColor = String(w_POManager_std.c_DarkCyan)       
		CASE c_SliderFrameYellow      
			i_SliderFrameColor = String(w_POManager_std.c_Yellow)       
		CASE c_SliderFrameDarkYellow  
			i_SliderFrameColor = String(w_POManager_std.c_DarkYellow)       
	END CHOOSE

	//--------------------------------------------------------------
	//  Set the slider bar color.
	//--------------------------------------------------------------

	IF OptionType = "Options" THEN
		l_BitMask = w_POManager_std.fw_BitMask(VisualWord, &
													  c_SliderBarColorMask)
	ELSE
		l_BitMask = 0
	END IF
	CHOOSE CASE l_BitMask
		CASE c_SliderBarGray
			i_SliderBarColor = String(w_POManager_std.c_Gray)       
		CASE c_SliderBarBlack  
			i_SliderBarColor = String(w_POManager_std.c_Black)       
		CASE c_SliderBarWhite   
			i_SliderBarColor = String(w_POManager_std.c_White)       
		CASE c_SliderBarDarkGray    
			i_SliderBarColor = String(w_POManager_std.c_DarkGray)       
		CASE c_SliderBarRed         
			i_SliderBarColor = String(w_POManager_std.c_Red)       
		CASE c_SliderBarDarkRed     
			i_SliderBarColor = String(w_POManager_std.c_DarkRed)       
		CASE c_SliderBarGreen       
			i_SliderBarColor = String(w_POManager_std.c_Green)       
		CASE c_SliderBarDarkGreen   
			i_SliderBarColor = String(w_POManager_std.c_DarkGreen)       
		CASE c_SliderBarBlue        
			i_SliderBarColor = String(w_POManager_std.c_Blue)       
		CASE c_SliderBarDarkBlue    
			i_SliderBarColor = String(w_POManager_std.c_DarkBlue)       
		CASE c_SliderBarMagenta     
			i_SliderBarColor = String(w_POManager_std.c_Magenta)       
		CASE c_SliderBarDarkMagenta 
			i_SliderBarColor = String(w_POManager_std.c_DarkMagenta)       
		CASE c_SliderBarCyan        
			i_SliderBarColor = String(w_POManager_std.c_Cyan)       
		CASE c_SliderBarDarkCyan    
			i_SliderBarColor = String(w_POManager_std.c_DarkCyan)       
		CASE c_SliderBarYellow      
			i_SliderBarColor = String(w_POManager_std.c_Yellow)       
		CASE c_SliderBarDarkYellow  
			i_SliderBarColor = String(w_POManager_std.c_DarkYellow)       
	END CHOOSE

	//--------------------------------------------------------------
	//  Set the slider center line color.
	//--------------------------------------------------------------

	IF OptionType = "Options" THEN
		l_BitMask = w_POManager_std.fw_BitMask(VisualWord, & 
                                         c_SliderCenterLineColorMask)
	ELSE
		l_BitMask = 0
	END IF
	CHOOSE CASE l_BitMask
		CASE c_SliderCenterLineGray
			i_SliderCenterLineColor = String(w_POManager_std.c_Gray)       
		CASE c_SliderCenterLineBlack  
			i_SliderCenterLineColor = String(w_POManager_std.c_Black)       
		CASE c_SliderCenterLineWhite   
			i_SliderCenterLineColor = String(w_POManager_std.c_White)       
		CASE c_SliderCenterLineDarkGray    
			i_SliderCenterLineColor = String(w_POManager_std.c_DarkGray)       
		CASE c_SliderCenterLineRed         
			i_SliderCenterLineColor = String(w_POManager_std.c_Red)       
		CASE c_SliderCenterLineDarkRed     
			i_SliderCenterLineColor = String(w_POManager_std.c_DarkRed)       
		CASE c_SliderCenterLineGreen       
			i_SliderCenterLineColor = String(w_POManager_std.c_Green)       
		CASE c_SliderCenterLineDarkGreen   
			i_SliderCenterLineColor = String(w_POManager_std.c_DarkGreen)       
		CASE c_SliderCenterLineBlue        
			i_SliderCenterLineColor = String(w_POManager_std.c_Blue)       
		CASE c_SliderCenterLineDarkBlue    
			i_SliderCenterLineColor = String(w_POManager_std.c_DarkBlue)       
		CASE c_SliderCenterLineMagenta     
			i_SliderCenterLineColor = String(w_POManager_std.c_Magenta)       
		CASE c_SliderCenterLineDarkMagenta 
			i_SliderCenterLineColor = String(w_POManager_std.c_DarkMagenta)       
		CASE c_SliderCenterLineCyan        
			i_SliderCenterLineColor = String(w_POManager_std.c_Cyan)       
		CASE c_SliderCenterLineDarkCyan    
			i_SliderCenterLineColor = String(w_POManager_std.c_DarkCyan)       
		CASE c_SliderCenterLineYellow      
			i_SliderCenterLineColor = String(w_POManager_std.c_Yellow)       
		CASE c_SliderCenterLineDarkYellow  
			i_SliderCenterLineColor = String(w_POManager_std.c_DarkYellow)       
	END CHOOSE

	//--------------------------------------------------------------
	//  Set the slider frame border.
	//--------------------------------------------------------------

	IF OptionType = "Options" THEN
		l_BitMask = w_POManager_std.fw_BitMask(VisualWord, &
                                         c_SliderFrameMask)
	ELSE
		l_BitMask = 0
	END IF
	CHOOSE CASE l_BitMask
		CASE c_SliderFrameNone
			i_SliderFrameBorder = "0"
		CASE c_SliderFrameShadowBox
			i_SliderFrameBorder = "1"
		CASE c_SliderFrameBox
			i_SliderFrameBorder = "2"
		CASE c_SliderFrameLowered
			i_SliderFrameBorder = "5"
		CASE c_SliderFrameRaised
			i_SliderFrameBorder = "6"
	END CHOOSE

	//--------------------------------------------------------------
	//  Set the slider border.
	//--------------------------------------------------------------

	IF OptionType = "Options" THEN
		l_BitMask = w_POManager_std.fw_BitMask(VisualWord, &
                                         c_SliderBorderMask)
	ELSE
		l_BitMask = 0
	END IF
	CHOOSE CASE l_BitMask
		CASE c_SliderBorderNone
			i_SliderBorder = "0" 
		CASE c_SliderBorderShadowBox        
			i_SliderBorder = "1"      
		CASE c_SliderBorderBox
			i_SliderBorder = "2"      
		CASE c_SliderBorderLowered
			i_SliderBorder = "5"     
		CASE c_SliderBorderRaised   
			i_SliderBorder = "6"       
	END CHOOSE

	//--------------------------------------------------------------
	//  Set the slider center line on or off.
	//--------------------------------------------------------------

	IF OptionType = "Options" THEN
		l_BitMask = w_POManager_std.fw_BitMask(VisualWord, &
                                         c_SliderCenterLineMask)
	ELSE
		l_BitMask = 0
	END IF
	CHOOSE CASE l_BitMask
		CASE c_SliderCenterLineShow
			i_SliderCenterLine = TRUE    
		CASE c_SliderCenterLineHide
			i_SliderCenterLine = FALSE 
	END CHOOSE

	//--------------------------------------------------------------
	//  Set the slider direction.
	//--------------------------------------------------------------

	IF OptionType = "Options" THEN
		l_BitMask = w_POManager_std.fw_BitMask(VisualWord, &
                                         c_SliderDirectionMask)
	ELSE
		l_BitMask = 0
	END IF
	CHOOSE CASE l_BitMask
		CASE c_SliderDirectionHoriz
			i_SliderDirection = "Horiz"      
		CASE c_SliderDirectionVert
			i_SliderDirection = "Vert"      
	END CHOOSE

	//--------------------------------------------------------------
	//  Set the slider bitmap to inside or outside the slider.
	//--------------------------------------------------------------

	IF OptionType = "Options" THEN
		l_BitMask = w_POManager_std.fw_BitMask(VisualWord, &
                                         c_SliderIndicatorMask)
	ELSE
		l_BitMask = 0
	END IF
	CHOOSE CASE l_BitMask
		CASE c_SliderIndicatorOut
			i_SliderIndicatorPosition = "Out"      
		CASE c_SliderIndicatorIn
			i_SliderIndicatorPosition = "In"      
	END CHOOSE

CASE "Tick"

	//--------------------------------------------------------------
	//  Set the slider tick color.
	//--------------------------------------------------------------

	IF OptionType = "Options" THEN
		l_BitMask = w_POManager_std.fw_BitMask(VisualWord, &
                                         c_SliderTickColorMask)
	ELSE
		l_BitMask = 0
	END IF
	CHOOSE CASE l_BitMask
		CASE c_SliderTickGray
			i_SliderTickColor = String(w_POManager_std.c_Gray)       
		CASE c_SliderTickBlack  
			i_SliderTickColor = String(w_POManager_std.c_Black)       
		CASE c_SliderTickWhite   
			i_SliderTickColor = String(w_POManager_std.c_White)       
		CASE c_SliderTickDarkGray    
			i_SliderTickColor = String(w_POManager_std.c_DarkGray)       
		CASE c_SliderTickRed         
			i_SliderTickColor = String(w_POManager_std.c_Red)       
		CASE c_SliderTickDarkRed     
			i_SliderTickColor = String(w_POManager_std.c_DarkRed)       
		CASE c_SliderTickGreen       
			i_SliderTickColor = String(w_POManager_std.c_Green)       
		CASE c_SliderTickDarkGreen   
			i_SliderTickColor = String(w_POManager_std.c_DarkGreen)       
		CASE c_SliderCenterLineBlue        
			i_SliderTickColor = String(w_POManager_std.c_Blue)       
		CASE c_SliderTickDarkBlue    
			i_SliderTickColor = String(w_POManager_std.c_DarkBlue)       
		CASE c_SliderTickMagenta     
			i_SliderTickColor = String(w_POManager_std.c_Magenta)       
		CASE c_SliderTickDarkMagenta 
			i_SliderTickColor = String(w_POManager_std.c_DarkMagenta)       
		CASE c_SliderTickCyan        
			i_SliderTickColor = String(w_POManager_std.c_Cyan)       
		CASE c_SliderTickDarkCyan    
			i_SliderTickColor = String(w_POManager_std.c_DarkCyan)       
		CASE c_SliderTickYellow      
			i_SliderTickColor = String(w_POManager_std.c_Yellow)       
		CASE c_SliderTickDarkYellow  
			i_SliderTickColor = String(w_POManager_std.c_DarkYellow)       
	END CHOOSE

CASE "Text"

	//--------------------------------------------------------------
	//  Set the slider text color.
	//--------------------------------------------------------------

	IF OptionType = "Options" THEN
		l_BitMask = w_POManager_std.fw_BitMask(VisualWord, &
                                         c_SliderTextColorMask)
	ELSE
		l_BitMask = 0
	END IF
	CHOOSE CASE l_BitMask
		CASE c_SliderTextGray
			i_SliderTextColor = String(w_POManager_std.c_Gray)
		CASE c_SliderTextBlack
			i_SliderTextColor = String(w_POManager_std.c_Black)
		CASE c_SliderTextWhite
			i_SliderTextColor = String(w_POManager_std.c_White)
		CASE c_SliderTextDarkGray
			i_SliderTextColor = String(w_POManager_std.c_DarkGray)
		CASE c_SliderTextRed
			i_SliderTextColor = String(w_POManager_std.c_Red)       
		CASE c_SliderTextDarkRed
			i_SliderTextColor = String(w_POManager_std.c_DarkRed)       
		CASE c_SliderTextGreen       
			i_SliderTextColor = String(w_POManager_std.c_Green)       
		CASE c_SliderTextDarkGreen   
			i_SliderTextColor = String(w_POManager_std.c_DarkGreen)       
		CASE c_SliderTextBlue        
			i_SliderTextColor = String(w_POManager_std.c_Blue)       
		CASE c_SliderTextDarkBlue    
			i_SliderTextColor = String(w_POManager_std.c_DarkBlue)       
		CASE c_SliderTextMagenta     
			i_SliderTextColor = String(w_POManager_std.c_Magenta)       
		CASE c_SliderTextDarkMagenta 
			i_SliderTextColor = String(w_POManager_std.c_DarkMagenta)       
		CASE c_SliderTextCyan        
			i_SliderTextColor = String(w_POManager_std.c_Cyan)       
		CASE c_SliderTextDarkCyan    
			i_SliderTextColor = String(w_POManager_std.c_DarkCyan)       
		CASE c_SliderTextYellow      
			i_SliderTextColor = String(w_POManager_std.c_Yellow)       
		CASE c_SliderTextDarkYellow  
			i_SliderTextColor = String(w_POManager_std.c_DarkYellow)       
	END CHOOSE

	//--------------------------------------------------------------
	//  Set the slider text on or off.
	//--------------------------------------------------------------

	IF OptionType = "Options" THEN
		l_BitMask = w_POManager_std.fw_BitMask(VisualWord, &
                                         c_SliderTextMask)
	ELSE
		l_BitMask = 0
	END IF
	CHOOSE CASE l_BitMask
		CASE c_SliderTextShow
			i_SliderText = TRUE    
		CASE c_SliderTextHide
			i_SliderText = FALSE 
	END CHOOSE

END CHOOSE

RETURN 0
end function

public function integer fu_slidercreate (decimal startpoint, decimal endpoint);//******************************************************************
//  PO Module     : u_Slider
//  Subroutine    : fu_SliderCreate
//  Description   : Responsible for creating the slider
//						  datawindow object.
//
//  Parameters    : DECIMAL startpoint - the starting major 
//													  increment value.
//						  DECIMAL endpoint -   the ending major
//													  increment value.
//
//  Return Value  : 0 - create successful
//						 -1 - create failed 
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

STRING 	l_DWSyntax, l_BGSyntax, l_BarSyntax
STRING	l_MajorLineSyntax, l_MinorLineSyntax, l_TextSyntax
STRING 	l_CenterLineSyntax, l_BitmapSyntax
STRING 	l_HeightString, l_TextString

INTEGER 	l_Error, l_Idx, l_Jdx
INTEGER	l_X, l_Y
INTEGER	l_Origin, l_MajorOrigin
INTEGER	l_TotalMajor, l_TotalMinor 
INTEGER	l_MajorLineHeight, l_MajorLineWidth
INTEGER	l_MinorLineHeight, l_MinorLineWidth
INTEGER  l_MajorIncrHeight, l_MajorIncrWidth
INTEGER	l_MajorLineX1, l_MajorLineX2, l_MajorLineY1, l_MajorLineY2
INTEGER	l_MinorLineX1, l_MinorLineX2, l_MinorLineY1, l_MinorLineY2
INTEGER	l_TextHeight, l_TextY, l_TextWidth, l_TextX
INTEGER	l_CenterLineX, l_CenterLineY
INTEGER	l_CenterLineHeight, l_CenterLineWidth

i_StartPoint = StartPoint
i_EndPoint = EndPoint

//------------------------------------------------------------------
//	Build the syntax needed to create the datawindow object
//------------------------------------------------------------------

l_HeightString = "height = " + String(Height)

l_DWSyntax = i_DWSyntax + &
				'color =' + i_SliderBGColor + ')' + &
 				'detail(' + l_heightstring + ' ' + &
				'color= "' + i_SliderBGColor + '" ) '

//------------------------------------------------------------------
//	Adjust the dimensions of the slider to leave room for
// a frame border, depending upon the frame border type.
//------------------------------------------------------------------

l_x = 0
l_y = -1
i_Width = UnitsToPixels( Width, xUnitsToPixels! )
i_Height = UnitsToPixels( Height, yUnitsToPixels! )

CHOOSE CASE i_SliderFrameBorder
CASE "0"
CASE "1"
	l_x = l_x + 1
	l_y = l_y + 1
	i_Width = i_width - 5
	i_height = i_Height - 4
CASE "2"
	l_x = l_x + 1
	l_y = l_y + 1
	i_Width = i_width - 2
	i_height = i_Height - 2
CASE "5"
	l_x = l_x + 2
	l_y = l_y + 2
	i_Width = i_width - 3
	i_height = i_Height - 3
CASE "6"
	l_x = l_x + 1
	l_y = l_y + 1
	i_Width = i_width - 3
	i_height = i_Height - 3
END CHOOSE

//------------------------------------------------------------------
//	Build the syntax needed to create the slider frame, which is a
// datawindow text field.
//------------------------------------------------------------------

l_BGSyntax = i_TextSyntax + &
				'border="' + i_SliderFrameBorder + '" ' + &
				'color="' + i_SliderFrameColor + '" ' + &
				'x="' + String(l_x) + '" ' + &
				'y="' + String(l_y) + '" ' + &
				'height="' + String(i_Height) + '" ' + &
				'width="' + String(i_Width) + '" ' + &
				'name=FrameBG ' + &
				'background.mode="0" ' + &
				'background.color="' + i_SliderFrameColor + '" ) '

//------------------------------------------------------------------
//	Determine the coordinates of the slider bar based on whether
// the indicator is to be placed inside or outside the slider, 
// and the direction of the slider.
//------------------------------------------------------------------

IF i_SliderDirection = "Horiz" THEN
	IF i_SliderIndicatorPosition = "Out" THEN
		i_BarY = i_Height / 6
		i_BarHeight = i_Height - ( i_BarY * 3 )
		i_BarX = i_BarY
		i_BarWidth = i_Width - ( i_BarX * 2 )
	ELSE
		i_BarX = i_Height / 6
		i_BarWidth = i_width - ( i_BarX * 2 ) 
		i_BarY = i_BarX
		i_BarHeight = i_Height - ( i_BarY * 2 )
	END IF
ELSE
	IF i_SliderIndicatorPosition = "Out" THEN
		i_BarY = i_Width / 6
		i_BarHeight = i_Height - ( i_BarY * 2 )
		i_BarX = i_BarY
		i_BarWidth = i_Width - ( i_BarX * 3 )
	ELSE
		i_BarX = i_Width / 6
		i_BarWidth = i_width - ( i_BarX * 2 ) 
		i_BarY = i_BarX
		i_BarHeight = i_Height - ( i_BarY * 2 )
	END IF
END IF

//------------------------------------------------------------------
// Adjust the origin of the slider bar wih respect the origin of the
// slider object. 
//------------------------------------------------------------------

i_BarX = i_BarX + l_x
i_BarY = i_BarY + l_y

//------------------------------------------------------------------
//	Determine the dimensions of the slider indicator if it is to be
// placed outside the slider bar. This calculation needs to be
// performed before adjusting for the slider bar border.
//------------------------------------------------------------------

IF i_SliderDirection = "Horiz" THEN
	IF i_SliderIndicatorPosition = "Out" THEN
		i_BitmapHeight = i_Height - i_BarHeight - i_BarY - 2 
		i_BitmapWidth = i_BitmapHeight
		i_BitmapY = i_BarY + i_BarHeight + 1
	END IF
ELSEIF i_SliderDirection = "Vert" THEN
	IF i_SliderIndicatorPosition = "Out" THEN
		i_BitmapWidth = i_Width - i_BarWidth - i_BarX - 2 
		i_BitmapHeight = i_BitmapWidth
		i_BitmapX = i_BarX + i_BarWidth + 1
	END IF
END IF

//------------------------------------------------------------------
//	Adjust the dimensions of the slider bar to leave room for a 
// border, depending upon the border type.
//------------------------------------------------------------------

CHOOSE CASE i_SliderBorder
CASE "0"
CASE "1"
	i_Barx = i_Barx + 1
	i_Bary = i_Bary + 1
	i_BarWidth = i_Barwidth - 5
	i_Barheight = i_BarHeight - 4
CASE "2"
	i_Barx = i_Barx + 1
	i_Bary = i_Bary + 1
	i_BarWidth = i_Barwidth - 2
	i_Barheight = i_BarHeight - 2
CASE "5"
	i_Barx = i_Barx + 2
	i_Bary = i_Bary + 2
	i_BarWidth = i_Barwidth - 3
	i_Barheight = i_BarHeight - 3
CASE "6"
	i_Barx = i_Barx + 1
	i_Bary = i_Bary + 1
	i_BarWidth = i_BarWidth - 3
	i_Barheight = i_BarHeight - 3
END CHOOSE

//------------------------------------------------------------------
//	If the indicator is placed inside the slider bar, adjust its
// dimensions based on the inside edge of the slider bar.
//------------------------------------------------------------------

IF i_SliderDirection = "Horiz" THEN
	IF i_SliderIndicatorPosition = "Out" THEN
		i_BitmapX = i_BarX - ( i_BitmapWidth + 1 ) / 2
	ELSE
		i_BitmapY = i_BarY + 1
		i_BitmapHeight = i_BarHeight - 2
		i_BitmapWidth = i_BitmapHeight / 2
		i_BitmapX = i_BarX
		IF i_SliderIndicator = "sliderup.bmp" THEN
			i_SliderIndicator = "sliderth.bmp"
		END IF
	END IF
ELSE
	IF i_SliderIndicatorPosition = "Out" THEN
		i_BitmapY = i_BarY + i_BarHeight - ( i_BitmapHeight + 1 ) / 2
		IF i_SliderIndicator = "sliderup.bmp" THEN
			i_SliderIndicator = "sliderlp.bmp"
		END IF
	ELSE
		i_BitmapX = i_BarX + 1
		i_BitmapWidth = i_BarWidth - 2
		i_BitmapHeight = i_BitmapWidth / 2
		i_BitmapY = i_BarY + i_BarHeight - i_BitmapHeight
		IF i_SliderIndicator = "sliderup.bmp" THEN
			i_SliderIndicator = "sliderth.bmp"
		END IF
	END IF
END IF


//------------------------------------------------------------------
//	Build the syntax needed to create the slider bar, which is a
// datawindow text field.
//------------------------------------------------------------------

l_BarSyntax = i_TextSyntax + &
				'border="' + i_SliderBorder + '" ' + &
				'color="' + i_SliderBarColor + '" ' + &
				'x="' + String(i_BarX) + '" ' + &
				'y="' + String(i_BarY) + '" ' + &
				'height="' + String(i_BarHeight) + '" ' + &
				'width="' + String(i_BarWidth) + '" ' + &
				'name=SliderBar ' + &
				'background.mode="0" ' + &
				'background.color="' + i_SliderBarColor + '" ) '

l_DWSyntax = l_DWSyntax + i_TableSyntax + &
				 l_BGSyntax + l_BarSyntax

//------------------------------------------------------------------
//	Determine the number of major and minor tick marks.
//------------------------------------------------------------------

l_totalmajor = ( endpoint - startpoint ) / i_SliderMajorIncr

IF i_SliderMinorIncr > 0 THEN
	l_totalminor = ( i_SliderMajorIncr / i_SliderMinorIncr )
END IF

//------------------------------------------------------------------
//	Build the syntax for the major and minor tick mark lines.
//------------------------------------------------------------------

IF i_SliderDirection = "Horiz" THEN
	l_MajorLineHeight = i_BarHeight * .6
	l_MajorLineY1 = i_BarY + ( i_BarHeight / 2 ) - &
									 ( l_MajorLineHeight / 2 )
	l_MajorLineY2 = l_MajorLineY1 + l_MajorLineHeight
	l_MajorIncrHeight = l_MajorLineY2 - l_MajorLineY1
	IF i_SliderIndicatorPosition = "Out" THEN
		l_Origin = i_BarX
	ELSE
		l_MajorOrigin = i_BarX + ( i_BitmapWidth / 2 )
		l_Origin = l_MajorOrigin
	END IF
ELSE
	l_MajorLineWidth = i_BarWidth * .6
	l_MajorLineX1 = i_BarX + ( i_BarWidth / 2 ) - &
									 ( l_MajorLineWidth / 2 )
	l_MajorLineX2 = l_MajorLineX1 + l_MajorLineWidth
	l_MajorIncrWidth = l_MajorLineX2 - l_MajorLineX1

	IF i_SliderIndicatorPosition = "Out" THEN
		l_Origin = i_BarY
	ELSE
		l_MajorOrigin = i_BarY + ( i_BitmapHeight / 2 )
		l_Origin = l_MajorOrigin
	END IF
END IF

FOR l_Idx = 1 TO l_TotalMajor
  IF l_Idx < l_TotalMajor THEN
		IF i_SliderDirection = "Horiz" THEN
			IF i_SliderIndicatorPosition = "Out" THEN
				l_MajorLineX1 = i_BarX + ( (i_BarWidth / &
												 l_TotalMajor ) * l_Idx )
			ELSE
				l_MajorLineX1 = l_MajorOrigin + ( ((i_BarWidth - &
									 i_BitmapWidth ) / l_TotalMajor) * l_Idx )
			END IF
			l_MajorLineX2 = l_MajorLineX1
			l_MajorIncrWidth = l_MajorLineX1 - l_Origin
		ELSE
			IF i_SliderIndicatorPosition = "Out" THEN
				l_MajorLineY1 = i_BarY + ( (i_BarHeight / &
													 l_TotalMajor ) * l_Idx )
			ELSE
				l_MajorLineY1 = l_MajorOrigin + ( ((i_BarHeight - &
									 i_BitmapHeight) / l_TotalMajor) * l_Idx )
			END IF
			l_MajorLineY2 = l_MajorLineY1
			l_MajorIncrHeight = l_MajorLineY1 - l_Origin
		END IF

		//--------------------------------------------------------------
		//	If the user has opted to show text on the slider, build
		// the syntax for the text. Otherwise build the syntax to
		// display the major tick marks.
		//--------------------------------------------------------------

		IF i_SliderText THEN
			IF i_SliderDirection = "Horiz" THEN
				l_TextString = STRING( i_SliderMajorIncr * l_Idx)
//											 ( l_TotalMajor - l_idx )) 
				l_TextHeight = ABS(INTEGER( i_SliderTextSize ) )
				l_TextY = i_BarY + ( i_BarHeight / 2 ) - &
									 	( l_TextHeight / 2 ) 
				l_TextWidth = i_BarWidth / l_TotalMajor
				l_TextX = l_MajorLineX1 - l_TextWidth / 2
			ELSE
				l_TextString = STRING ( i_SliderMajorIncr * &
											 ( l_TotalMajor - l_idx )) 
				l_TextHeight = ABS(INTEGER( i_SliderTextSize )) 
				l_TextWidth = i_BarWidth 
				l_TextX = i_BarX 
				l_TextY = ( l_MajorLineY1 - l_TextHeight / 2 )
			END IF

			l_TextSyntax = 'text(band=detail alignment="2" ' + &
					'text="' + l_textstring + '" ' + &
					'border="0" ' + &
					'color="' + i_SliderTextColor + '" ' + &
					'x="' + String(l_TextX) + '" ' + &
					'y="' + String(l_TextY) + '" ' + &
					'height="' + String(l_TextHeight) + '" ' + &
					'width="' + String(l_TextWidth) + '" ' + &
					'name=SliderText ' + &
					'font.face="' + i_SliderTextFont + '" ' + &
					'font.height="' + String(l_TextHeight) + '" ' + &
					'font.weight="400" ' + &
					'font.family="2" ' + &
					'font.pitch="2" ' + &
					'font.charset="0" ' + &
					'background.mode="1" ' + &
					'background.color="255" ) '

	 	l_DWSyntax = l_DWSyntax + l_TextSyntax

	ELSE

		l_MajorLineSyntax = 'line(band=detail ' + &
						'x1="' + String(l_MajorLineX1) + '" ' + &
						'y1="' + String(l_MajorLineY1) + '" ' + &
						'x2="' + String(l_MajorLineX2) + '" ' + &
						'y2="' + String(l_MajorLineY2) + '" ' + &
						'pen.style="0" pen.width="1" pen.color="' + i_SliderTickColor + '" ' + &
						'background.mode="2" ' + &
						'background.color="' + i_SliderTickColor + '" ) '

	 	l_DWSyntax = l_DWSyntax + l_MajorLineSyntax
	END IF

 END IF

 IF i_SliderMinorIncr > 0 THEN

  FOR l_Jdx = 1 TO l_TotalMinor - 1

		IF i_SliderDirection = "Horiz" THEN
			l_MinorLineHeight = i_BarHeight * .4

			l_MinorLineY1 = i_BarY + ( i_BarHeight / 2 ) - &
											 ( l_MinorLineHeight / 2 )
			l_MinorLineY2 = l_MinorLineY1 + l_MinorLineHeight
			l_MinorLineX1 = l_Origin + ( (l_MajorIncrWidth / &
													l_TotalMinor) * l_Jdx )
			l_MinorLineX2 = l_MinorLineX1
			
		ELSE
			l_MinorLineWidth = i_BarWidth * .4

			l_MinorLineX1 = i_BarX + ( i_BarWidth / 2 ) - &
											 ( l_MinorLineWidth / 2 )
			l_MinorLineX2 = l_MinorLineX1 + l_MinorLineWidth
	
			l_MinorLineY1 = l_Origin + ( (l_MajorIncrHeight / &
													l_TotalMinor) * l_Jdx )
			l_MinorLineY2 = l_MinorLineY1
		END IF

		l_MinorLineSyntax = 'line(band=detail ' + &
							'x1="' + String(l_MinorLineX1) + '" ' + &
							'y1="' + String(l_MinorLineY1) + '" ' + &
							'x2="' + String(l_MinorLineX2) + '" ' + &
							'y2="' + String(l_MinorLineY2) + '" ' + &
							'pen.style="0" pen.width="1" pen.color="' + i_SliderTickColor + '" ' + &
							'background.mode="2" ' + &
							'background.color="' + i_SliderTickColor + '" ) '

		l_DWSyntax = l_DWSyntax + l_MinorLineSyntax

	END FOR
 END IF

 IF i_SliderDirection = "Horiz" THEN
	l_Origin = l_MajorLineX1
 ELSE
	l_Origin = l_MajorLineY1
 END IF

END FOR

//------------------------------------------------------------------
//	Build the syntax needed to create the slider center line, which 
// is a rectangle.
//------------------------------------------------------------------

IF i_SliderCenterLine THEN
	IF i_SliderDirection = "Horiz" THEN
		l_CenterLineWidth = i_BarWidth * .93
		l_CenterLineHeight = i_BarHeight / 10
		l_CenterLineX = i_BarX + ((i_Barwidth / 2) - &
										 (l_CenterLineWidth / 2))
		l_CenterLineY = i_BarY + ((i_BarHeight /2) - &
										 (l_CenterLineHeight / 2))
	ELSE
		l_CenterLineHeight = i_BarHeight * .93
		l_CenterLineWidth = i_BarWidth / 10
		l_CenterLineX = i_BarX + ((i_Barwidth / 2) - &
										  (l_CenterLineWidth / 2))
		l_CenterLineY = i_BarY + ((i_BarHeight /2) - &
										  (l_CenterLineHeight / 2))
	END IF
	l_CenterLineSyntax = 'rectangle(band=detail ' + &
						'x="' + String(l_CenterLineX) + '" ' + &
						'y="' + String(l_CenterLineY) + '" ' + &
						'height="' + String(l_CenterLineHeight) + '" ' + &
						'width="' + String(l_CenterLineWidth) + '" ' + &
						'brush.hatch="6" ' + &
						'brush.color="' + i_SliderCenterLineColor + '" ' + &
						'pen.style="0" pen.width="0" ' + &
						'pen.color="' + i_SliderCenterLineColor + '" ' + &
						'background.mode="2" ' + &
						'background.color="' + i_SliderCenterLineColor + '" ) '

	l_DWSyntax = l_DWSyntax + l_CenterLineSyntax

END IF


//------------------------------------------------------------------
//	Build the syntax needed to create the slider indicator.
//------------------------------------------------------------------

l_BitmapSyntax = 'bitmap(band=detail ' + &
					  'filename="' + i_SliderIndicator + '"' + &
					  'x="' + String(i_BitmapX) + '" ' + &
					  'y="' + String(i_BitmapY) + '" ' + &
					  'height="' + String(i_BitmapHeight) + '" ' + &
					  'width="' + String(i_BitmapWidth) + '" ' + &
					  'name=sliderbmp border="0" )'

l_DWSyntax = l_DWSyntax + l_BitmapSyntax

//------------------------------------------------------------------
//	Create the slider.
//------------------------------------------------------------------

l_Error = Create( l_DWSyntax )

RETURN 0


end function

public function integer fu_slidertextoptions (string textfont, integer textsize, long visualword);//******************************************************************
//  PO Module     : u_Slider
//  Subroutine    : fu_SliderTextOptions
//  Description   : Establishes the look-and-feel of the slider
//                  text.  
//
//  Parameters    : STRING  TextFont  - text font to be used for the
//												    slider text.
//                  INTEGER TextSize  - height of the slider
//													 text in pixels
//						  ULONG VisualWord  - visual options for the
//                                      slider.
//
//  Return Value  : (None)
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

//------------------------------------------------------------------
//  Set the font type of the slider text.
//------------------------------------------------------------------

i_SliderTextFont = TextFont

//------------------------------------------------------------------
//  Set the font size of the slider text.
//------------------------------------------------------------------

i_SliderTextSize = STRING(TextSize * -1)


fu_SetOptions( "Text", "Options", VisualWord )

RETURN 0
end function

public function decimal fu_getslider ();//******************************************************************
//  PO Module     : u_Slider
//  Subroutine    : fu_GetSlider
//  Description   : Responsible for determining the position of the
//					     slider indicator. 
//
//  Parameters    : None	
//
//  Return Value  : DECIMAL position - the current position of the
//						  slider indicator.	
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************
//

STRING	l_Text, l_Error, l_ModifyString, l_BitmapSyntax
INTEGER	l_SliderHeight, l_SliderWidth, l_SliderY, l_SliderX
INTEGER	l_TextWidth
DECIMAL	l_position

IF i_SliderDirection = "Horiz" THEN
	IF i_SliderIndicatorPosition = "Out" THEN
		l_position = i_BitmapX + i_BitmapWidth / 2 - i_BarX
		l_position = l_position / i_BarWidth * &
						 	( i_EndPoint - i_StartPoint ) + i_StartPoint
	ELSE
		l_position = i_BitmapX - i_BarX
		l_position = l_position / (i_BarWidth - i_BitmapWidth) * &
							( i_EndPoint - i_StartPoint ) + i_StartPoint
	END IF
ELSE
	IF i_SliderIndicatorPosition = "Out" THEN
		l_position = i_BitmapY + i_BitmapHeight / 2 - i_BarY
		l_position = 1 - l_position / (i_BarHeight )
		l_position = l_position * ( i_EndPoint - i_StartPoint ) + &
							i_StartPoint
	ELSE
		l_position = i_BitmapY - i_BarY
		l_position = 1 - l_position / (i_BarHeight - i_BitmapHeight)
		l_position = l_position * ( i_EndPoint - i_StartPoint ) + &
							i_StartPoint
	END IF
END IF

RETURN l_position






end function

public function integer fu_slideroptions (string indicator, unsignedlong visualword);//******************************************************************
//  PO Module     : u_Slider
//  Subroutine    : fu_SliderOptions
//  Description   : Establishes the look-and-feel of the slider
//                  object.  
//
//  Parameters    : STRING Indicator  - bitmap to be used for the
//												    slider.
//						  ULONG VisualWord  - visual options for the
//                                      slider.
//
//  Return Value  : (None)
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

//------------------------------------------------------------------
//  Set the bitmap name for the slider if it is different than the 
//  default. 
//------------------------------------------------------------------

i_SliderIndicator = Indicator

fu_SetOptions( "Slider", "Options", VisualWord )

RETURN 0
end function

public function integer fu_slidertickoptions (decimal majorincrement, decimal minorincrement, unsignedlong visualword);//******************************************************************
//  PO Module     : u_Slider
//  Subroutine    : fu_SliderTickOptions
//  Description   : Establishes the tick positions and color
//						  of the slider object.
//
//  Parameters    : DECIMAL MajorIncrement  
//                  DECIMAL MinorIncrement  
//						  ULONG  VisualWord  - visual options for the
//                                       slider tick marks.
//
//  Return Value  : (None)
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

//------------------------------------------------------------------
//  Set the increment values if they are different than the defaults. 
//------------------------------------------------------------------

i_SliderMinorIncr = MinorIncrement

i_SliderMajorIncr = MajorIncrement

fu_SetOptions( "Tick", "Options", VisualWord )

RETURN 0
end function

public function integer fu_setslider (decimal position);//******************************************************************
//  PO Module     : u_Slider
//  Subroutine    : fu_SetSlider
//  Description   : Responsible for setting the position of the
//					     slider indicator. 
//
//  Parameters    : DECIMAL position - the new slider position.
//
//  Return Value  : 0 - set position successful
//						 -1 - set position failed 
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

STRING	l_Text, l_Error, l_ModifyString, l_BitmapSyntax
INTEGER	l_SliderHeight, l_SliderWidth, l_SliderY, l_SliderX
INTEGER	l_TextWidth
DECIMAL	l_position

IF i_SliderDirection = "Horiz" THEN
	l_position = position - i_StartPoint
	IF i_SliderIndicatorPosition = "Out" THEN
		l_position = l_position / (i_EndPoint - i_StartPoint ) * &
							i_BarWidth
		i_BitmapX = i_BarX + l_position - ( i_BitmapWidth / 2 ) 
	ELSE
		l_position = l_position / (i_EndPoint - i_StartPoint ) * &
							(i_BarWidth - i_BitmapWidth)
		i_BitmapX = i_BarX + l_position 
	END IF
	l_ModifyString = "sliderbmp.x =" + String(i_BitmapX)
	l_Error = Modify( l_ModifyString )
	IF l_Error <> "" THEN
		RETURN -1
	END IF
ELSE
	IF i_SliderIndicatorPosition = "Out" THEN
		l_position = (1 - (( position - i_StartPoint ) / &
							(i_EndPoint - i_StartPoint ))) * (i_BarHeight)
		i_BitmapY = i_BarY + l_position - ( i_BitmapHeight ) / 2 
	ELSE
		i_BitmapY = (1 - (( position - i_StartPoint ) / &
							(i_EndPoint - i_StartPoint ))) * &
							(i_BarHeight - i_BitmapHeight)
		i_BitmapY = i_BarY + i_BitmapY
	END IF
	l_ModifyString = "sliderbmp.y =" + String(i_BitmapY)
	l_Error = Modify( l_ModifyString )
	IF l_Error <> "" THEN
		RETURN -1
	END IF
END IF

SetReDraw(TRUE)

RETURN 0 





end function

on clicked;//******************************************************************
//  PO Module     : u_Slider
//  Event         : clicked
//  Description   : Controls actions when the user holds down
//						  the left mouse button while inside the
//						  slider object.
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

STRING	l_ClickedObject, l_ModifyString, l_Error
INTEGER	l_PointerX, l_PointerY

//------------------------------------------------------------------
// Capture the mouse so that the slider object will have control 
// over it until the user releases it.
//------------------------------------------------------------------

w_POManager_std.EXT.fu_SetCapture(THIS)

//------------------------------------------------------------------
// Determine the object on which the mouse pointer is currently 
// positioned.
//------------------------------------------------------------------

l_ClickedObject = GetObjectAtPointer()

l_ClickedObject = Left(l_ClickedObject, POS(l_ClickedObject, "~t") - 1)

IF l_ClickedObject = "sliderbmp" THEN

	//--------------------------------------------------------------
	// If the mouse is currently postioned on the slider indicator,
	// determine the distance it has moved from its previous
	// position.
	//--------------------------------------------------------------

	IF i_SliderDirection = "Horiz" THEN
		i_PrevPointerX = UnitsToPixels( ABS(PointerX()), xUnitsToPixels! )
		i_PointerOffset = i_PrevPointerX - i_BitmapX
	ELSE
		i_PrevPointerY = UnitsToPixels( ABS(PointerY()), yUnitsToPixels! )
		i_PointerOffset = i_PrevPointerY - i_BitmapY
	END IF	
	i_SliderMoving = TRUE
ELSE

	//--------------------------------------------------------------
	// If the mouse is currently postioned inside the slider bar,
	// change the indicator coordinates to the new position.
	//--------------------------------------------------------------

	l_PointerX = UnitsToPixels( ABS(PointerX()), xUnitsToPixels! )
	l_PointerY = UnitsToPixels( ABS(PointerY()), yUnitsToPixels! )

	//--------------------------------------------------------------
	// Check to make sure that the mouse pointer is within the 
	// bounds of the slider bar, before adjusting the coordinates.
	//--------------------------------------------------------------

	IF l_PointerX >= i_BarX and l_PointerX <= i_BarX + i_BarWidth and &
	   l_PointerY >= i_BarY and l_PointerY <= i_BarY + i_BarHeight THEN
		IF i_SliderDirection = "Horiz" THEN
			i_BitmapX = l_PointerX - ( i_BitmapWidth / 2 )
			IF i_SliderIndicatorPosition = "Out" THEN
				IF i_BitmapX + ( i_BitmapWidth / 2 ) < i_BarX THEN
					i_BitmapX = i_BarX - ( i_BitMapWidth / 2 )
				ELSEIF i_BitmapX + ( i_BitmapWidth / 2 ) > i_BarX + i_BarWidth THEN
					i_BitmapX = i_BarX + i_BarWidth - ( i_BitmapWidth / 2 )
				END IF
			ELSE
				IF i_BitmapX < i_BarX THEN
					i_BitmapX = i_BarX 
				ELSEIF i_BitmapX + i_BitmapWidth > i_BarX + i_BarWidth THEN
					i_BitmapX = i_BarX + i_BarWidth - ( i_BitmapWidth )
				END IF
			END IF
			l_ModifyString = "sliderbmp.x =" + String(i_BitmapX)
		ELSE
			i_BitmapY = l_PointerY - ( i_BitmapHeight / 2 )
			IF i_SliderIndicatorPosition = "Out" THEN
				IF i_BitmapY + i_BitmapHeight / 2 < i_BarY THEN
					i_BitmapY = i_BarY - i_BitmapHeight / 2
				ELSEIF i_BitmapY + i_BitmapHeight / 2 > i_BarY + i_BarHeight THEN
					i_BitmapY = i_BarY + i_BarHeight - ( i_BitmapHeight / 2 )
				END IF
			ELSE
				IF i_BitmapY < i_BarY THEN
					i_BitmapY = i_BarY 
				ELSEIF i_BitmapY + i_BitmapHeight > i_BarY + i_BarHeight THEN
					i_BitmapY = i_BarY + i_BarHeight - ( i_BitmapHeight )
				END IF
			END IF	
			l_ModifyString = "sliderbmp.y =" + String(i_BitmapY)
		END IF

		//------------------------------------------------------------
		//  Modify the slider datawindow object to display the 
		//	 indicator at the new position.
		//------------------------------------------------------------

		l_Error = Modify( l_ModifyString )
		SetReDraw(TRUE)
	END IF
END IF

end on

on constructor;//******************************************************************
//  PO Module     : u_Slider
//  Event         : Constructor
//  Description   : Initializes the default slider options. 
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER l_Idx

//------------------------------------------------------------------
//  Identify this object as a PowerObject Slider
//------------------------------------------------------------------

Tag = "PowerObject Slider"

//------------------------------------------------------------------
//  Make sure that the border of the slider control is turned off.
//------------------------------------------------------------------

Border = FALSE

//------------------------------------------------------------------
//  Initialize the slider options.
//------------------------------------------------------------------

//------------------------------------------------------------------
//  Slider background color.
//------------------------------------------------------------------

c_SliderBGGray				= w_POManager_std.c_SliderBGGray
c_SliderBGBlack			= w_POManager_std.c_SliderBGBlack
c_SliderBGWhite			= w_POManager_std.c_SliderBGWhite
c_SliderBGDarkGray		= w_POManager_std.c_SliderBGDarkGray
c_SliderBGRed				= w_POManager_std.c_SliderBGRed
c_SliderBGDarkRed			= w_POManager_std.c_SliderBGDarkGray
c_SliderBGGreen			= w_POManager_std.c_SliderBGGreen
c_SliderBGDarkGreen		= w_POManager_std.c_SliderBGDarkGreen
c_SliderBGBlue				= w_POManager_std.c_SliderBGBlue
c_SliderBGDarkBlue		= w_POManager_std.c_SliderBGDarkBlue
c_SliderBGMagenta			= w_POManager_std.c_SliderBGMagenta
c_SliderBGDarkMagenta	= w_POManager_std.c_SliderBGDarkMagenta
c_SliderBGCyan				= w_POManager_std.c_SliderBGCyan
c_SliderBGDarkCyan		= w_POManager_std.c_SliderBGDarkCyan
c_SliderBGYellow			= w_POManager_std.c_SliderBGYellow
c_SliderBGDarkYellow		= w_POManager_std.c_SliderBGDarkYellow

//------------------------------------------------------------------
//  Slider frame color.
//------------------------------------------------------------------

c_SliderFrameGray				= w_POManager_std.c_SliderFrameGray
c_SliderFrameBlack			= w_POManager_std.c_SliderFrameBlack
c_SliderFrameWhite			= w_POManager_std.c_SliderFrameWhite
c_SliderFrameDarkGray		= w_POManager_std.c_SliderFrameDarkGray
c_SliderFrameRed				= w_POManager_std.c_SliderFrameRed
c_SliderFrameDarkRed			= w_POManager_std.c_SliderFrameDarkGray
c_SliderFrameGreen			= w_POManager_std.c_SliderFrameGreen
c_SliderFrameDarkGreen		= w_POManager_std.c_SliderFrameDarkGreen
c_SliderFrameBlue				= w_POManager_std.c_SliderFrameBlue
c_SliderFrameDarkBlue		= w_POManager_std.c_SliderFrameDarkBlue
c_SliderFrameMagenta			= w_POManager_std.c_SliderFrameMagenta
c_SliderFrameDarkMagenta	= w_POManager_std.c_SliderFrameDarkMagenta
c_SliderFrameCyan				= w_POManager_std.c_SliderFrameCyan
c_SliderFrameDarkCyan		= w_POManager_std.c_SliderFrameDarkCyan
c_SliderFrameYellow			= w_POManager_std.c_SliderFrameYellow
c_SliderFrameDarkYellow		= w_POManager_std.c_SliderFrameDarkYellow

//------------------------------------------------------------------
//  Slider bar color.
//------------------------------------------------------------------

c_SliderBarGray			= w_POManager_std.c_SliderBarGray
c_SliderBarBlack			= w_POManager_std.c_SliderBarBlack
c_SliderBarWhite			= w_POManager_std.c_SliderBarWhite
c_SliderBarDarkGray		= w_POManager_std.c_SliderBarDarkGray
c_SliderBarRed				= w_POManager_std.c_SliderBarRed
c_SliderBarDarkRed		= w_POManager_std.c_SliderBarDarkGray
c_SliderBarGreen			= w_POManager_std.c_SliderBarGreen
c_SliderBarDarkGreen		= w_POManager_std.c_SliderBarDarkGreen
c_SliderBarBlue			= w_POManager_std.c_SliderBarBlue
c_SliderBarDarkBlue		= w_POManager_std.c_SliderBarDarkBlue
c_SliderBarMagenta		= w_POManager_std.c_SliderBarMagenta
c_SliderBarDarkMagenta	= w_POManager_std.c_SliderBarDarkMagenta
c_SliderBarCyan			= w_POManager_std.c_SliderBarCyan
c_SliderBarDarkCyan		= w_POManager_std.c_SliderBarDarkCyan
c_SliderBarYellow			= w_POManager_std.c_SliderBarYellow
c_SliderBarDarkYellow	= w_POManager_std.c_SliderBarDarkYellow

//------------------------------------------------------------------
//  Slider center line color.
//------------------------------------------------------------------

c_SliderCenterLineGray			= w_POManager_std.c_SliderCenterLineGray
c_SliderCenterLineBlack			= w_POManager_std.c_SliderCenterLineBlack
c_SliderCenterLineWhite			= w_POManager_std.c_SliderCenterLineWhite
c_SliderCenterLineDarkGray		= w_POManager_std.c_SliderCenterLineDarkGray
c_SliderCenterLineRed			= w_POManager_std.c_SliderCenterLineRed
c_SliderCenterLineDarkRed		= w_POManager_std.c_SliderCenterLineDarkGray
c_SliderCenterLineGreen			= w_POManager_std.c_SliderCenterLineGreen
c_SliderCenterLineDarkGreen	= w_POManager_std.c_SliderCenterLineDarkGreen
c_SliderCenterLineBlue			= w_POManager_std.c_SliderCenterLineBlue
c_SliderCenterLineDarkBlue		= w_POManager_std.c_SliderCenterLineDarkBlue
c_SliderCenterLineMagenta		= w_POManager_std.c_SliderCenterLineMagenta
c_SliderCenterLineDarkMagenta	= w_POManager_std.c_SliderCenterLineDarkMagenta
c_SliderCenterLineCyan			= w_POManager_std.c_SliderCenterLineCyan
c_SliderCenterLineDarkCyan		= w_POManager_std.c_SliderCenterLineDarkCyan
c_SliderCenterLineYellow		= w_POManager_std.c_SliderCenterLineYellow
c_SliderCenterLineDarkYellow	= w_POManager_std.c_SliderCenterLineDarkYellow

//------------------------------------------------------------------
//  Slider tick mark color.
//------------------------------------------------------------------

c_SliderTickGray			= w_POManager_std.c_SliderTickGray
c_SliderTickBlack			= w_POManager_std.c_SliderTickBlack
c_SliderTickWhite			= w_POManager_std.c_SliderTickWhite
c_SliderTickDarkGray		= w_POManager_std.c_SliderTickDarkGray
c_SliderTickRed				= w_POManager_std.c_SliderTickRed
c_SliderTickDarkRed		= w_POManager_std.c_SliderTickDarkGray
c_SliderTickGreen			= w_POManager_std.c_SliderTickGreen
c_SliderTickDarkGreen		= w_POManager_std.c_SliderTickDarkGreen
c_SliderTickBlue			= w_POManager_std.c_SliderTickBlue
c_SliderTickDarkBlue		= w_POManager_std.c_SliderTickDarkBlue
c_SliderTickMagenta		= w_POManager_std.c_SliderTickMagenta
c_SliderTickDarkMagenta	= w_POManager_std.c_SliderTickDarkMagenta
c_SliderTickCyan			= w_POManager_std.c_SliderTickCyan
c_SliderTickDarkCyan		= w_POManager_std.c_SliderTickDarkCyan
c_SliderTickYellow			= w_POManager_std.c_SliderTickYellow
c_SliderTickDarkYellow	= w_POManager_std.c_SliderTickDarkYellow

//-----------------------------------------------------------------
//  Slider text color.
//-----------------------------------------------------------------

c_SliderTextGray			= w_POManager_std.c_SliderTextGray
c_SliderTextBlack			= w_POManager_std.c_SliderTextBlack
c_SliderTextWhite			= w_POManager_std.c_SliderTextWhite
c_SliderTextDarkGray		= w_POManager_std.c_SliderTextDarkGray
c_SliderTextRed				= w_POManager_std.c_SliderTextRed
c_SliderTextDarkRed		= w_POManager_std.c_SliderTextDarkGray
c_SliderTextGreen			= w_POManager_std.c_SliderTextGreen
c_SliderTextDarkGreen		= w_POManager_std.c_SliderTextDarkGreen
c_SliderTextBlue			= w_POManager_std.c_SliderTextBlue
c_SliderTextDarkBlue		= w_POManager_std.c_SliderTextDarkBlue
c_SliderTextMagenta		= w_POManager_std.c_SliderTextMagenta
c_SliderTextDarkMagenta	= w_POManager_std.c_SliderTextDarkMagenta
c_SliderTextCyan			= w_POManager_std.c_SliderTextCyan
c_SliderTextDarkCyan		= w_POManager_std.c_SliderTextDarkCyan
c_SliderTextYellow			= w_POManager_std.c_SliderTextYellow
c_SliderTextDarkYellow	= w_POManager_std.c_SliderTextDarkYellow

//------------------------------------------------------------------
// Slider frame border style.
//------------------------------------------------------------------

c_SliderFrameNone  =	w_POManager_std.c_SliderFrameNone
c_SliderFrameShadowBox  =	w_POManager_std.c_SliderFrameShadowBox
c_SliderFrameBox  =	w_POManager_std.c_SliderFrameBox
c_SliderFrameLowered  =	w_POManager_std.c_SliderFrameLowered
c_SliderFrameRaised  =	w_POManager_std.c_SliderFrameRaised

//------------------------------------------------------------------
//  Slider border color.
//------------------------------------------------------------------

c_SliderBorderNone  =	w_POManager_std.c_SliderBorderNone
c_SliderBorderShadowBox  =	w_POManager_std.c_SliderBorderShadowBox
c_SliderBorderBox  =	w_POManager_std.c_SliderBorderBox
c_SliderBorderLowered  =	w_POManager_std.c_SliderBorderLowered
c_SliderBorderRaised  =	w_POManager_std.c_SliderBorderRaised

//------------------------------------------------------------------
//  Turn the slider center line on or off.
//------------------------------------------------------------------

c_SliderCenterLineShow  =	w_POManager_std.c_SliderCenterLineShow 
c_SliderCenterLineHide  =	w_POManager_std.c_SliderCenterLineHide

//------------------------------------------------------------------
// Place the slider indicator inside or outside.
//------------------------------------------------------------------

c_SliderIndicatorOut  =	w_POManager_std.c_SliderIndicatorOut 
c_SliderIndicatorIn  =	w_POManager_std.c_SliderIndicatorIn 

//------------------------------------------------------------------
//  Slider direction.
//------------------------------------------------------------------

c_SliderDirectionHoriz  =	w_POManager_std.c_SliderDirectionHoriz
c_SliderDirectionVert  =	w_POManager_std.c_SliderDirectionVert

//------------------------------------------------------------------
//  Hide or show slider text.
//------------------------------------------------------------------

c_SliderTextShow =	w_POManager_std.c_SliderTextShow
c_SliderTextHide =	w_POManager_std.c_SliderTextHide

fu_SetOptions("Slider", "Defaults", 0)



end on

