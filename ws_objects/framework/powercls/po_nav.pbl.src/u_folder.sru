﻿$PBExportHeader$u_folder.sru
$PBExportComments$Tab folder object
forward
global type u_folder from datawindow
end type
type s_tab_array from structure within u_folder
end type
end forward

type s_tab_array from structure
    string tabname
    boolean tabdisable
    boolean tabhide
    integer tabnumobjects
    windowobject tabobjects[]
    boolean tabobjectcentered[]
end type

global type u_folder from datawindow
integer width = 1211
integer height = 716
integer taborder = 1
string dataobject = "d_folder_tb"
event po_tabclicked pbm_custom75
event po_tabvalidate pbm_custom74
end type
global u_folder u_folder

type variables
//-------------------------------------------------------------------------------
//  Folder instance variables.
//-------------------------------------------------------------------------------

S_TAB_ARRAY      i_TabArray[]
STRING		i_TabPos[]
INTEGER		i_TabRows
INTEGER		i_Tabs

INTEGER		i_CurrentTab 		= 0
INTEGER		i_SelectedTab 		= 0
INTEGER		i_ClickedTab		= 0
INTEGER		i_TabError		= 0
STRING		i_SelectedTabName
STRING		i_ClickedTabName
BOOLEAN	i_FolderResize
BOOLEAN	i_PermInvisible

//-------------------------------------------------------------------------------
//  Default constants.
//-------------------------------------------------------------------------------

INTEGER		c_DefaultHeight	= 0
LONG		c_DefaultVisual	= 0
STRING		c_DefaultFont	= ""
INTEGER		c_DefaultSize	= 0

//-------------------------------------------------------------------------------
//  Folder option constants.
//-------------------------------------------------------------------------------

ULONG		c_FolderTabTop
ULONG		c_FolderTabBottom
ULONG		c_FolderTabLeft
ULONG		c_FolderTabRight

ULONG		c_Folder3D
ULONG		c_Folder2D

ULONG		c_FolderBlack
ULONG		c_FolderWhite
ULONG		c_FolderGray
ULONG		c_FolderDarkGray
ULONG		c_FolderRed
ULONG		c_FolderDarkRed
ULONG		c_FolderGreen
ULONG		c_FolderDarkGreen
ULONG		c_FolderBlue
ULONG		c_FolderDarkBlue
ULONG		c_FolderMagenta
ULONG		c_FolderDarkMagenta
ULONG		c_FolderCyan
ULONG		c_FolderDarkCyan
ULONG		c_FolderYellow
ULONG		c_FolderDarkYellow

ULONG		c_FolderBGGray
ULONG		c_FolderBGBlack
ULONG		c_FolderBGWhite
ULONG		c_FolderBGDarkGray
ULONG		c_FolderBGRed
ULONG		c_FolderBGDarkRed
ULONG		c_FolderBGGreen
ULONG		c_FolderBGDarkGreen
ULONG		c_FolderBGBlue
ULONG		c_FolderBGDarkBlue
ULONG		c_FolderBGMagenta
ULONG		c_FolderBGDarkMagenta
ULONG		c_FolderBGCyan
ULONG		c_FolderBGDarkCyan
ULONG		c_FolderBGYellow
ULONG		c_FolderBGDarkYellow

ULONG		c_FolderBorderOn

ULONG		c_FolderBorderOff

ULONG		c_FolderResizeOn
ULONG		c_FolderResizeOff

//-------------------------------------------------------------------------------
//  Tab constants.
//-------------------------------------------------------------------------------

ULONG		c_TextRegular
ULONG		c_TextBold
ULONG		c_TextItalic
ULONG		c_TextUnderline

ULONG		c_TextCenter
ULONG		c_TextLeft
ULONG		c_TextRight

ULONG		c_TextBlack
ULONG		c_TextWhite
ULONG		c_TextGray
ULONG		c_TextDarkGray
ULONG		c_TextRed
ULONG		c_TextDarkRed
ULONG		c_TextGreen
ULONG		c_TextDarkGreen
ULONG		c_TextBlue
ULONG		c_TextDarkBlue
ULONG		c_TextMagenta
ULONG		c_TextDarkMagenta
ULONG		c_TextCyan
ULONG		c_TextDarkCyan
ULONG		c_TextYellow
ULONG		c_TextDarkYellow

ULONG		c_TextRotationOn
ULONG		c_TextRotationOff

//-------------------------------------------------------------------------------
//  Current tab constants.
//-------------------------------------------------------------------------------

ULONG		c_TextCurrentRegular
ULONG		c_TextCurrentBold
ULONG		c_TextCurrentItalic
ULONG		c_TextCurrentUnderline

ULONG		c_TextCurrentBlack
ULONG		c_TextCurrentWhite
ULONG		c_TextCurrentGray
ULONG		c_TextCurrentDarkGray
ULONG		c_TextCurrentRed
ULONG		c_TextCurrentDarkRed
ULONG		c_TextCurrentGreen
ULONG		c_TextCurrentDarkGreen
ULONG		c_TextCurrentBlue
ULONG		c_TextCurrentDarkBlue
ULONG		c_TextCurrentMagenta
ULONG		c_TextCurrentDarkMagenta
ULONG		c_TextCurrentCyan
ULONG		c_TextCurrentDarkCyan
ULONG		c_TextCurrentYellow
ULONG		c_TextCurrentDarkYellow

ULONG		c_TabCurrentGray
ULONG		c_TabCurrentBlack
ULONG		c_TabCurrentWhite
ULONG		c_TabCurrentDarkGray
ULONG		c_TabCurrentRed
ULONG		c_TabCurrentDarkRed
ULONG		c_TabCurrentGreen
ULONG		c_TabCurrentDarkGreen
ULONG		c_TabCurrentBlue
ULONG		c_TabCurrentDarkBlue
ULONG		c_TabCurrentMagenta
ULONG		c_TabCurrentDarkMagenta
ULONG		c_TabCurrentCyan
ULONG		c_TabCurrentDarkCyan
ULONG		c_TabCurrentYellow
ULONG		c_TabCurrentDarkYellow

ULONG		c_TabCurrentColorFolder
ULONG		c_TabCurrentColorBorder
ULONG		c_TabCurrentColorTab

//-------------------------------------------------------------------------------
//  Disable tab constants.
//-------------------------------------------------------------------------------

ULONG		c_TextDisableRegular
ULONG		c_TextDisableBold
ULONG		c_TextDisableItalic
ULONG		c_TextDisableUnderline

ULONG		c_TextDisableBlack
ULONG		c_TextDisableWhite
ULONG		c_TextDisableGray
ULONG		c_TextDisableDarkGray
ULONG		c_TextDisableRed
ULONG		c_TextDisableDarkRed
ULONG		c_TextDisableGreen
ULONG		c_TextDisableDarkGreen
ULONG		c_TextDisableBlue
ULONG		c_TextDisableDarkBlue
ULONG		c_TextDisableMagenta
ULONG		c_TextDisableDarkMagenta
ULONG		c_TextDisableCyan
ULONG		c_TextDisableDarkCyan
ULONG		c_TextDisableYellow
ULONG		c_TextDisableDarkYellow
end variables

forward prototypes
public subroutine fu_assigntab (integer tab, string tablabel, ref windowobject tabobjects[])
public subroutine fu_folderresize ()
public function integer fu_selecttab (integer newtab)
public subroutine fu_disabletab (integer tab)
public subroutine fu_enabletab (integer tab)
public subroutine fu_taboptions (string textfont, integer textsize, unsignedlong visualword)
public subroutine fu_tabdisableoptions (string textfont, integer textsize, unsignedlong visualword)
public subroutine fu_tabcurrentoptions (string textfont, integer textsize, unsignedlong visualword)
public function integer fu_selecttabname (string tabname)
public subroutine fu_folderoptions (integer tabheight, unsignedlong visualword)
public subroutine fu_foldercreate (integer numberoftabs, integer numbertabsperrow)
public subroutine fu_hidetab (integer tab)
public subroutine fu_showtab (integer tab)
private subroutine fu_setoptions (string optionstyle, string optiontype, unsignedlong visualword)
public subroutine fu_disabletabname (string tabname)
public subroutine fu_enabletabname (string tabname)
public subroutine fu_tabkeydown ()
public subroutine fu_hidetabname (string tabname)
public subroutine fu_showtabname (string tabname)
public subroutine fu_assigntabobjects (integer tab, ref windowobject tabobjects[])
public subroutine fu_assigntablabel (integer tab, string tablabel)
public subroutine fu_folderworkspace (ref integer ws_x, ref integer ws_y, ref integer ws_width, ref integer ws_height)
public function integer fu_centertabobject (dragobject tab_object)
public subroutine fu_tabinfo (integer tab, ref string name, ref boolean tab_enabled, ref boolean tab_visible, ref windowobject objects[])
public subroutine fu_foldercreatetb (integer tabs_per_row)
public subroutine fu_foldercreatelr (integer tabs_per_row)
public function dragobject fu_opentab (integer tab, string uo_name, window window_name)
public subroutine fu_unassigntabobject (integer tab, ref windowobject tabobject)
public subroutine fu_buildlabels ()
end prototypes

on po_tabclicked;////******************************************************************
////                  This is a Developer coded event
////******************************************************************
////
////  PO Module     : u_Folder
////  Event         : po_TabClicked
////  Description   : Provides an opportunity for the developer to
////                  do any processing after a tab is selected.
////
////  Variables     : The following variables are set for the developer
////                  before this event is triggered:
////
////                  INTEGER i_SelectedTab     - Number of the tab
////                                              selected by the user.
////                  STRING  i_SelectedTabName - Name of the tab
////                                              (without the &) of
////                                              the tab selected by
////                                              the user.
////
////  Return Value  : None.  All tab processing is completed before
////                  this event is triggered.
////
////  Change History: 
////
////  Date     Person     Description of Change
////  -------- ---------- --------------------------------------------
////
////******************************************************************
////  Copyright ServerLogic 1994-1995.  All Rights Reserved.
////******************************************************************
//
////------------------------------------------------------------------
////  Sample code for determining which tab was clicked by the user.
////------------------------------------------------------------------
//
//CHOOSE CASE i_SelectedTabName
//   CASE "Personal"
//      <code>
//   CASE "Address"
//      <code>
//   CASE "Benefits"
//      <code>
//   CASE "Status"
//      <code>
//END CHOOSE
//
////------------------------------------------------------------------
////  Sample code for dynamically creating a user object that contains
////  all the objects for the selected tab.
////------------------------------------------------------------------
//
//INTEGER l_WorkSpaceX, l_WorkSpaceY
//INTEGER l_WorkSpaceWidth, l_WorkSpaceHeight
//
//fu_FolderWorkSpace(l_WorkSpaceX, l_WorkSpaceY, l_WorkSpaceWidth, &
//                   l_WorkSpaceHeight)
//
//CHOOSE CASE i_SelectedTabName
//   CASE "Personal"
//      IF NOT IsValid(<user_object>) THEN
//         OpenUserObject(<user_object>, l_WorkSpaceX, l_WorkSpaceY)
//      END IF
//   CASE "Address"
//      IF NOT IsValid(<user_object>) THEN
//         OpenUserObject(<user_object>, l_WorkSpaceX, l_WorkSpaceY)
//      END IF
//   CASE "Benefits"
//      IF NOT IsValid(<user_object>) THEN
//         OpenUserObject(<user_object>, l_WorkSpaceX, l_WorkSpaceY)
//      END IF
//   CASE "Status"
//      IF NOT IsValid(<user_object>) THEN
//         OpenUserObject(<user_object>, l_WorkSpaceX, l_WorkSpaceY)
//      END IF
//END CHOOSE
end on

on po_tabvalidate;////******************************************************************
////                  This is a Developer coded event
////******************************************************************
////
////  PO Module     : u_Folder
////  Event         : po_TabValidate
////  Description   : Provides an opportunity for the developer to
////                  do any processing before a tab is selected.
////
////  Variables     : The following variables are set for the developer
////                  before this event is triggered:
////
////                  INTEGER i_ClickedTab      - Number of the new tab
////                                              selected by the user.
////                  STRING  i_ClickedTabName  - Name of the tab
////                                              (without the &) of
////                                              the selected tab.
////                  INTEGER i_SelectedTab     - Number of the 
////                                              current tab.
////                  STRING  i_SelectedTabName - Name of the tab
////                                              (without the &) of
////                                              the current tab.
////
////  Return Value  : INTEGER i_TabError - Indicates if an error has
////                                       occured in the processing
////                                       of this event.  Set to -1
////                                       to stop the new tab from
////                                       becoming the current tab.
////
////  Change History: 
////
////  Date     Person     Description of Change
////  -------- ---------- --------------------------------------------
////
////******************************************************************
////  Copyright ServerLogic 1994-1995.  All Rights Reserved.
////******************************************************************
//
////------------------------------------------------------------------
////  Sample code for validating a DataWindow on the current tab
////  before allowing the new tab to become selected.
////------------------------------------------------------------------
//
//CHOOSE CASE i_SelectedTabName
//   CASE "Personal"
//      IF <dw>.AcceptText() = -1 THEN
//         i_TabError = -1
//      END IF
//   CASE "Address"
//      <code>
//   CASE "Benefits"
//      <code>
//   CASE "Status"
//      <code>
//END CHOOSE
end on

public subroutine fu_assigntab (integer tab, string tablabel, ref windowobject tabobjects[]);//******************************************************************
//  PO Module     : u_Folder
//  Subroutine    : fu_AssignTab
//  Description   : Assigns a label and window objects to a tab.  To
//                  assign just a label, use the fu_AssignTabLabel
//                  function.  To assign just window objects, use
//                  the fu_AssignTabObjects function.
//
//  Parameters    : INTEGER      Tab          - tab number
//                  STRING       TabLabel     - tab label
//                  WINDOWOBJECT TabObjects[] - array of window
//                                              objects
//
//  Return Value  : (None)
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER l_Idx

//------------------------------------------------------------------
//  Assign the tab label and the window objects to the tab.
//  Hide each of the window objects.  
//------------------------------------------------------------------

i_TabArray[Tab].TabNumObjects = UpperBound(TabObjects[])
i_TabArray[Tab].TabName = TabLabel
FOR l_Idx = 1 TO i_TabArray[Tab].TabNumObjects
   i_TabArray[Tab].TabObjects[l_Idx]        = TabObjects[l_Idx]
   i_TabArray[Tab].TabObjectCentered[l_Idx] = FALSE
   i_TabArray[Tab].TabHide    = FALSE
   i_TabArray[Tab].TabDisable = FALSE
   IF i_SelectedTab = Tab THEN
      TabObjects[l_Idx].Visible = TRUE
   ELSE        
      TabObjects[l_Idx].Visible = FALSE
   END IF
NEXT

end subroutine

public subroutine fu_folderresize ();//******************************************************************
//  PO Module     : u_Folder
//  Subroutine    : fu_FolderResize
//  Description   : Responsible for the actual creation of the
//                  folder object.  Called by the fu_FolderCreate
//                  function for initial creation and by the
//                  resize event when the object is resized.  
//
//  Parameters    : (None)
//
//  Return Value  : (None)
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER l_Width
STRING l_TabSide

l_TabSide = GetItemString(1, "f_side")

IF l_TabSide = "T" OR l_TabSide = "B" THEN
   l_Width = UnitsToPixels(Width, XUnitsToPixels!) - 2
   l_Width = l_Width - Mod(l_Width - &
             ((i_TabRows - 1) * GetItemNumber(1, "t_offset")), &
             GetItemNumber(1, "t_rows"))
   SetItem(1, "f_width", l_Width)
   SetItem(1, "f_height", UnitsToPixels(Height, YUnitsToPixels!) - 2)

   Modify("datawindow.detail.height=" + String(UnitsToPixels(Height, YUnitsToPixels!)))
ELSE
   l_Width = UnitsToPixels(Height, YUnitsToPixels!) - 2
   l_Width = l_Width - Mod(l_Width - &
             ((i_TabRows - 1) * GetItemNumber(1, "t_offset")), &
             GetItemNumber(1, "t_rows"))
   SetItem(1, "f_width", l_Width)
   SetItem(1, "f_height", UnitsToPixels(Width, XUnitsToPixels!) - 2)

   Modify("datawindow.detail.height=" + String(UnitsToPixels(Height, YUnitsToPixels!)))
END IF

IF NOT Visible THEN
   IF NOT i_PermInvisible THEN
      Show()
   END IF
ELSE
   SetReDraw(TRUE)
END IF


end subroutine

public function integer fu_selecttab (integer newtab);//******************************************************************
//  PO Module     : u_Folder
//  Function      : fu_SelectTab
//  Description   : Sets the given tab to be the current tab.  
//
//  Parameters    : INTEGER      Tab - tab number
//
//  Return Value  : INTEGER      0 = OK   -1 = selection failed
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER l_Idx, l_Pos, l_SelectedRow
STRING  l_TmpPos, l_TabPos[]

//------------------------------------------------------------------
//  If the same tab is selected, an invalid tab is passed, or the
//  tab is disabled or hidden then return with no processing.
//------------------------------------------------------------------

IF NewTab = i_SelectedTab OR NewTab < 0 OR NewTab > i_Tabs THEN
   RETURN -1
ELSE
   IF i_TabArray[NewTab].TabDisable OR i_TabArray[NewTab].TabHide THEN
	   RETURN -1
   END IF
END IF

//------------------------------------------------------------------
//  Validate the current tab before moving to the new selected tab.
//------------------------------------------------------------------

IF i_SelectedTab > 0 THEN
   i_ClickedTab = NewTab
   l_Pos = POS(i_TabArray[NewTab].TabName, "&")
   IF l_Pos > 0 THEN
      i_ClickedTabName = Replace(i_TabArray[NewTab].TabName, &
                                 l_Pos, 1, "")
   ELSE
      i_ClickedTabName = i_TabArray[NewTab].TabName
   END IF

   i_TabError = 0
   TriggerEvent("po_TabValidate")

   IF i_TabError = -1 THEN
      RETURN -1
   END IF
END IF
 
//------------------------------------------------------------------
//  If a current tab exists, make the objects associated with the
//  tab invisible.
//------------------------------------------------------------------

SetReDraw(FALSE)

IF i_CurrentTab > 0 THEN
   FOR l_Idx = 1 TO i_TabArray[i_SelectedTab].TabNumObjects
      i_TabArray[i_SelectedTab].TabObjects[l_Idx].Visible = FALSE
   NEXT
END IF

//------------------------------------------------------------------
//  Check if a new tab was selected.  This function might have been
//  called just to turn off the current tab.
//------------------------------------------------------------------

IF NewTab > 0 THEN
   i_SelectedTab     = NewTab
   l_Pos = POS(i_TabArray[NewTab].TabName, "&")
   IF l_Pos > 0 THEN
      i_SelectedTabName = Replace(i_TabArray[NewTab].TabName, &
                                  l_Pos, 1, "")
   ELSE
      i_SelectedTabName = i_TabArray[NewTab].TabName
   END IF

   IF i_TabRows > 1 THEN
      l_SelectedRow = Ceiling(Pos(GetItemString(1, "t_pos"), &
                      "|" + String(NewTab, "00")) / Len(i_TabPos[1]))
      IF l_SelectedRow > 1 THEN
         l_TmpPos = ""
         FOR l_Idx = 1 TO i_TabRows
            l_TmpPos = l_TmpPos + i_TabPos[l_SelectedRow]
            l_TabPos[l_Idx] = i_TabPos[l_SelectedRow]
            l_SelectedRow = l_SelectedRow + 1
            IF l_SelectedRow > i_TabRows THEN
               l_SelectedRow = 1
            END IF
         NEXT
         i_TabPos[] = l_TabPos[]
         SetItem(1, "t_pos", l_TmpPos)
      END IF
   END IF

   //---------------------------------------------------------------
   //  Turn the objects assigned to the current tab visible.
   //---------------------------------------------------------------

   FOR l_Idx = 1 to i_TabArray[i_SelectedTab].TabNumObjects 
      i_TabArray[i_SelectedTab].TabObjects[l_Idx].Visible = TRUE
   NEXT

   i_CurrentTab = NewTab
ELSE
   i_CurrentTab = 0
   i_SelectedTab = 0
   i_SelectedTabName = ""
END IF

SetItem(1, "t_select", NewTab)

SetReDraw(TRUE)

//------------------------------------------------------------------
//  Trigger an event to allow the developer to do processing after
//  a new current tab is selected.
//------------------------------------------------------------------

TriggerEvent("po_tabclicked")

RETURN 0
end function

public subroutine fu_disabletab (integer tab);//******************************************************************
//  PO Module     : u_Folder
//  Subroutine    : fu_DisableTab
//  Description   : Disables a tab that had been enabled.  
//
//  Parameters    : INTEGER      Tab          - tab number
//
//  Return Value  : (None)
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

IF Tab = i_SelectedTab THEN
   RETURN
END IF

//------------------------------------------------------------------
//  Set the tab to disable.
//------------------------------------------------------------------

i_TabArray[Tab].TabDisable = TRUE
fu_BuildLabels()

RETURN
end subroutine

public subroutine fu_enabletab (integer tab);//******************************************************************
//  PO Module     : u_Folder
//  Subroutine    : fu_EnableTab
//  Description   : Enables a tab that had been disabled.  
//
//  Parameters    : INTEGER      Tab          - tab number
//
//  Return Value  : (None)
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

IF Tab = i_SelectedTab THEN
   RETURN
END IF

//------------------------------------------------------------------
//  Set the tab to enable.
//------------------------------------------------------------------

i_TabArray[Tab].TabDisable = FALSE
fu_BuildLabels()

RETURN
end subroutine

public subroutine fu_taboptions (string textfont, integer textsize, unsignedlong visualword);//******************************************************************
//  PO Module     : u_Folder
//  Subroutine    : fu_TabOptions
//  Description   : Establishes the look-and-feel of the tabs
//                  portion of the tab folder object.  
//
//  Parameters    : STRING  TextFont   - font style of the text.
//                  STRING  TextSize   - text size in points
//                  ULONG   VisualWord - visual options for the
//                                       tabs portion.
//
//  Return Value  : (None)
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

//------------------------------------------------------------------
//  Set the font style of the text for the tabs.
//------------------------------------------------------------------

IF TextFont <> c_DefaultFont THEN
   SetItem(1, "t_font", TextFont)
   CHOOSE CASE UPPER(TextFont)
      CASE "ARIAL"
         SetItem(1, "t_family", 2)
         SetItem(1, "t_pitch", 2)
         SetItem(1, "t_charset", 0)
      CASE "COURIER", "COURIER NEW"
         SetItem(1, "t_family", 1)
         SetItem(1, "t_pitch", 1)
         SetItem(1, "t_charset", 0)
      CASE "MODERN"
         SetItem(1, "t_family", 1)
         SetItem(1, "t_pitch", 2)
         SetItem(1, "t_charset", -1)
      CASE "MS SANS SERIF"
         SetItem(1, "t_family", 2)
         SetItem(1, "t_pitch", 2)
         SetItem(1, "t_charset", 0)
      CASE "MS SERIF"
         SetItem(1, "t_family", 1)
         SetItem(1, "t_pitch", 2)
         SetItem(1, "t_charset", 0)
      CASE "ROMAN"
         SetItem(1, "t_family", 1)
         SetItem(1, "t_pitch", 2)
         SetItem(1, "t_charset", -1)
      CASE "SCRIPT"
         SetItem(1, "t_family", 4)
         SetItem(1, "t_pitch", 2)
         SetItem(1, "t_charset", -1)
      CASE "SMALL FONTS"
         SetItem(1, "t_family", 2)
         SetItem(1, "t_pitch", 2)
         SetItem(1, "t_charset", 0)
      CASE "SYSTEM"
         SetItem(1, "t_family", 2)
         SetItem(1, "t_pitch", 2)
         SetItem(1, "t_charset", 0)
      CASE "TIMES NEW ROMAN", "TIMES ROMAN"
         SetItem(1, "t_family", 1)
         SetItem(1, "t_pitch", 2)
         SetItem(1, "t_charset", 0)
      CASE "MS LINEDRAW"
         SetItem(1, "t_family", 1)
         SetItem(1, "t_pitch", 1)
         SetItem(1, "t_charset", 2)
      CASE "TERMINAL"
         SetItem(1, "t_family", 1)
         SetItem(1, "t_pitch", 1)
         SetItem(1, "t_charset", -1)
      CASE "CENTURY SCHOOLBOOK"
         SetItem(1, "t_family", 1)
         SetItem(1, "t_pitch", 2)
         SetItem(1, "t_charset", 0)
      CASE "CENTURY GOTHIC"
         SetItem(1, "t_family", 2)
         SetItem(1, "t_pitch", 2)
         SetItem(1, "t_charset", 0)
      CASE ELSE
         SetItem(1, "t_family", 2)
         SetItem(1, "t_pitch", 2)
         SetItem(1, "t_charset", 0)
   END CHOOSE
END IF

//------------------------------------------------------------------
//  Set the font size of the heading.
//------------------------------------------------------------------

IF TextSize <> c_DefaultSize THEN
   SetItem(1, "t_size", TextSize)
END IF

//------------------------------------------------------------------
//  Set the visual options for the tab portion of the object.
//------------------------------------------------------------------

fu_SetOptions("Tab", "Options", VisualWord)

end subroutine

public subroutine fu_tabdisableoptions (string textfont, integer textsize, unsignedlong visualword);//******************************************************************
//  PO Module     : u_Folder
//  Subroutine    : fu_TabDisableOptions
//  Description   : Establishes the look-and-feel of the disabled
//                  tab portion of the tab folder object.  
//
//  Parameters    : STRING  TextFont   - font style of the text.
//                  STRING  TextSize   - text size in points
//                  ULONG   VisualWord - visual options for the
//                                       disabled tab portion.
//
//  Return Value  : (None)
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

//------------------------------------------------------------------
//  Set the font style of the text for the disabled tabs.
//------------------------------------------------------------------

IF TextFont <> c_DefaultFont THEN
   SetItem(1, "d_font", TextFont)
   CHOOSE CASE UPPER(TextFont)
      CASE "ARIAL"
         SetItem(1, "d_family", 2)
         SetItem(1, "d_pitch", 2)
         SetItem(1, "d_charset", 0)
      CASE "COURIER", "COURIER NEW"
         SetItem(1, "d_family", 1)
         SetItem(1, "d_pitch", 1)
         SetItem(1, "d_charset", 0)
      CASE "MODERN"
         SetItem(1, "d_family", 1)
         SetItem(1, "d_pitch", 2)
         SetItem(1, "d_charset", -1)
      CASE "MS SANS SERIF"
         SetItem(1, "d_family", 2)
         SetItem(1, "d_pitch", 2)
         SetItem(1, "d_charset", 0)
      CASE "MS SERIF"
         SetItem(1, "d_family", 1)
         SetItem(1, "d_pitch", 2)
         SetItem(1, "d_charset", 0)
      CASE "ROMAN"
         SetItem(1, "d_family", 1)
         SetItem(1, "d_pitch", 2)
         SetItem(1, "d_charset", -1)
      CASE "SCRIPT"
         SetItem(1, "d_family", 4)
         SetItem(1, "d_pitch", 2)
         SetItem(1, "d_charset", -1)
      CASE "SMALL FONTS"
         SetItem(1, "d_family", 2)
         SetItem(1, "d_pitch", 2)
         SetItem(1, "d_charset", 0)
      CASE "SYSTEM"
         SetItem(1, "d_family", 2)
         SetItem(1, "d_pitch", 2)
         SetItem(1, "d_charset", 0)
      CASE "TIMES NEW ROMAN", "TIMES ROMAN"
         SetItem(1, "d_family", 1)
         SetItem(1, "d_pitch", 2)
         SetItem(1, "d_charset", 0)
      CASE "MS LINEDRAW"
         SetItem(1, "d_family", 1)
         SetItem(1, "d_pitch", 1)
         SetItem(1, "d_charset", 2)
      CASE "TERMINAL"
         SetItem(1, "d_family", 1)
         SetItem(1, "d_pitch", 1)
         SetItem(1, "d_charset", -1)
      CASE "CENTURY SCHOOLBOOK"
         SetItem(1, "d_family", 1)
         SetItem(1, "d_pitch", 2)
         SetItem(1, "d_charset", 0)
      CASE "CENTURY GOTHIC"
         SetItem(1, "d_family", 2)
         SetItem(1, "d_pitch", 2)
         SetItem(1, "d_charset", 0)
      CASE ELSE
         SetItem(1, "d_family", 2)
         SetItem(1, "d_pitch", 2)
         SetItem(1, "d_charset", 0)
   END CHOOSE
END IF

//------------------------------------------------------------------
//  Set the font size of the heading.
//------------------------------------------------------------------

IF TextSize <> c_DefaultSize THEN
   SetItem(1, "d_size", TextSize)
END IF

//------------------------------------------------------------------
//  Set the visual options for the disable tab portion of the object.
//------------------------------------------------------------------

fu_SetOptions("TabDisable", "Options", VisualWord)

end subroutine

public subroutine fu_tabcurrentoptions (string textfont, integer textsize, unsignedlong visualword);//******************************************************************
//  PO Module     : u_Folder
//  Subroutine    : fu_TabCurrentOptions
//  Description   : Establishes the look-and-feel of the current tab
//                  portion of the tab folder object.  
//
//  Parameters    : STRING  TextFont   - font style of the text.
//                  STRING  TextSize   - text size in points
//                  ULONG   VisualWord - visual options for the
//                                       current tab portion.
//
//  Return Value  : (None)
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

//------------------------------------------------------------------
//  Set the font style of the text for the current tab.
//------------------------------------------------------------------

IF TextFont <> c_DefaultFont THEN
   SetItem(1, "c_font", TextFont)
   CHOOSE CASE UPPER(TextFont)
      CASE "ARIAL"
         SetItem(1, "c_family", 2)
         SetItem(1, "c_pitch", 2)
         SetItem(1, "c_charset", 0)
      CASE "COURIER", "COURIER NEW"
         SetItem(1, "c_family", 1)
         SetItem(1, "c_pitch", 1)
         SetItem(1, "c_charset", 0)
      CASE "MODERN"
         SetItem(1, "c_family", 1)
         SetItem(1, "c_pitch", 2)
         SetItem(1, "c_charset", -1)
      CASE "MS SANS SERIF"
         SetItem(1, "c_family", 2)
         SetItem(1, "c_pitch", 2)
         SetItem(1, "c_charset", 0)
      CASE "MS SERIF"
         SetItem(1, "c_family", 1)
         SetItem(1, "c_pitch", 2)
         SetItem(1, "c_charset", 0)
      CASE "ROMAN"
         SetItem(1, "c_family", 1)
         SetItem(1, "c_pitch", 2)
         SetItem(1, "c_charset", -1)
      CASE "SCRIPT"
         SetItem(1, "c_family", 4)
         SetItem(1, "c_pitch", 2)
         SetItem(1, "c_charset", -1)
      CASE "SMALL FONTS"
         SetItem(1, "c_family", 2)
         SetItem(1, "c_pitch", 2)
         SetItem(1, "c_charset", 0)
      CASE "SYSTEM"
         SetItem(1, "c_family", 2)
         SetItem(1, "c_pitch", 2)
         SetItem(1, "c_charset", 0)
      CASE "TIMES NEW ROMAN", "TIMES ROMAN"
         SetItem(1, "c_family", 1)
         SetItem(1, "c_pitch", 2)
         SetItem(1, "c_charset", 0)
      CASE "MS LINEDRAW"
         SetItem(1, "c_family", 1)
         SetItem(1, "c_pitch", 1)
         SetItem(1, "c_charset", 2)
      CASE "TERMINAL"
         SetItem(1, "c_family", 1)
         SetItem(1, "c_pitch", 1)
         SetItem(1, "c_charset", -1)
      CASE "CENTURY SCHOOLBOOK"
         SetItem(1, "c_family", 1)
         SetItem(1, "c_pitch", 2)
         SetItem(1, "c_charset", 0)
      CASE "CENTURY GOTHIC"
         SetItem(1, "c_family", 2)
         SetItem(1, "c_pitch", 2)
         SetItem(1, "c_charset", 0)
      CASE ELSE
         SetItem(1, "c_family", 2)
         SetItem(1, "c_pitch", 2)
         SetItem(1, "c_charset", 0)
   END CHOOSE
END IF

//------------------------------------------------------------------
//  Set the font size of the heading.
//------------------------------------------------------------------

IF TextSize <> c_DefaultSize THEN
   SetItem(1, "c_size", TextSize)
END IF

//------------------------------------------------------------------
//  Set the visual options for the current tab portion of the object.
//------------------------------------------------------------------

fu_SetOptions("TabCurrent", "Options", VisualWord)
end subroutine

public function integer fu_selecttabname (string tabname);//******************************************************************
//  PO Module     : u_Folder
//  Function      : fu_SelectTabName
//  Description   : Uses the tab label to set the given tab to be 
//                  the current tab. 
//
//  Parameters    : INTEGER      TabName - tab label
//
//  Return Value  : INTEGER      0 = OK   -1 = selection failed
//
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER l_Idx, l_Pos, l_Tabs
STRING  l_TabName

//------------------------------------------------------------------
//  Cycle through the tabs looking for the tab to select.  Use the
//  tab label after stripping the '&'.  After finding the tab, call
//  the fu_SelectTab function to do the actual selecting.
//------------------------------------------------------------------

l_Tabs = GetItemNumber(1, "t_num")
FOR l_Idx = 1 to l_Tabs
   l_Pos = POS(i_TabArray[l_Idx].TabName, "&")
   IF l_Pos > 0 THEN
      l_TabName = Replace(i_TabArray[l_Idx].TabName, l_Pos, 1, "")
   ELSE
      l_TabName = i_TabArray[l_Idx].TabName
   END IF
	IF TabName = l_TabName THEN
		IF fu_SelectTab(l_Idx) = -1 THEN
		   RETURN -1
      ELSE
         RETURN 0
      END IF
	END IF
NEXT

RETURN -1
end function

public subroutine fu_folderoptions (integer tabheight, unsignedlong visualword);//******************************************************************
//  PO Module     : u_Folder
//  Subroutine    : fu_FolderOptions
//  Description   : Establishes the look-and-feel of the folder
//                  portion of the tab folder object.  
//
//  Parameters    : INTEGER TabHeight  - height of the tab in pixels
//                  ULONG   VisualWord - visual options for the
//                                       folder portion.
//
//  Return Value  : (None)
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

//------------------------------------------------------------------
//  Set the height of the tab if it is different than the default.
//------------------------------------------------------------------

IF TabHeight <> c_DefaultHeight THEN
   SetItem(1, "t_height", TabHeight)
END IF

//------------------------------------------------------------------
//  Set the visual options for the folder portion of the object.
//------------------------------------------------------------------

fu_SetOptions("Folder", "Options", VisualWord)

end subroutine

public subroutine fu_foldercreate (integer numberoftabs, integer numbertabsperrow);//******************************************************************
//  PO Module     : u_Folder
//  Subroutine    : fu_FolderCreate
//  Description   : Creates the folder with the given number of
//                  tabs and tabs per row.  
//
//  Parameters    : INTEGER NumberOfTabs       - number of tabs
//                  INTEGER NumberOfTabsPerRow - number of tabs
//                                               per row
//
//  Return Value  : (None)
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

WINDOW     l_Window
USEROBJECT l_UserObject
STRING     l_TabSide, l_TabRotate, l_Data, l_Label, l_SecuredTab[], l_Ret
INTEGER    l_Idx, l_Pos, l_SecuredTabs, l_Jdx, l_SecuredBit[]
BOOLEAN    l_DisableAll

//------------------------------------------------------------------
//  Initialize the folder tab settings.
//------------------------------------------------------------------

SetReDraw(FALSE)
i_Tabs = 0
i_CurrentTab = 0
i_SelectedTab = 0
i_SelectedTabName = ""

l_TabSide   = GetItemString(1, "f_side")
l_TabRotate = GetItemString(1, "t_rotate")

//------------------------------------------------------------------
//  Determine if the correct DataWindow object is present.
//------------------------------------------------------------------

i_PermInvisible = FALSE
IF NOT Visible THEN
   i_PermInvisible = TRUE
END IF

IF (l_TabSide = "T" OR l_TabSide = "B") AND &
   DataObject = "d_folder_lr" THEN
   IF Visible THEN
      Hide()
   END IF
   l_Data = Describe("datawindow.data")
   DataObject = "d_folder_tb"
   DeleteRow(1)
   ImportString(l_Data)
ELSEIF (l_TabSide = "L" OR l_TabSide = "R") AND &
   DataObject = "d_folder_tb" THEN
   IF Visible THEN
      Hide()
   END IF
   l_Data = Describe("datawindow.data")
   DataObject = "d_folder_lr"
   DeleteRow(1)
   ImportString(l_Data)
END IF

//------------------------------------------------------------------
//  Set the number of tabs and tabs per row variables.
//------------------------------------------------------------------

i_Tabs = NumberOfTabs
SetItem(1, "t_num", i_Tabs)
SetItem(1, "t_rows", NumberTabsPerRow)
SetItem(1, "t_visible", "")

//------------------------------------------------------------------
//  Determine the background color of the parent.
//------------------------------------------------------------------

IF Parent.TypeOf() = Window! THEN
   l_Window          = Parent
   Modify("datawindow.color = '" + String(l_Window.BackColor) + "'")
ELSE
   l_UserObject      = Parent
   Modify("datawindow.color = '" + String(l_UserObject.BackColor) + "'")
END IF

//------------------------------------------------------------------
//  Determine if extra tabs or rows are needed.
//------------------------------------------------------------------

IF l_TabSide = "T" OR l_TabSide = "B" THEN
   fu_FolderCreateTB(NumberTabsPerRow)
ELSE
   fu_FolderCreateLR(NumberTabsPerRow)
END IF

//------------------------------------------------------------------
//  Check to see if PowerLock security indicates that any of the
//  tabs should be hidden.  If PowerLock security is not installed
//  then a stub function is run.
//------------------------------------------------------------------

l_SecuredTabs = f_pl_GetFolderInfo(THIS, l_SecuredTab[], &
                                   l_SecuredBit[])

l_DisableAll = FALSE
IF l_SecuredTabs < 0 THEN
   IF l_SecuredTabs = -9999 THEN
      l_SecuredTabs = 0
   ELSE
      l_SecuredTabs = l_SecuredTabs * -1
   END IF
   l_DisableAll = TRUE
END IF

//------------------------------------------------------------------
//  Set the tab labels.
//
//  PowerBuilder Bug!  If text is rotated the '&' will display as
//  a character and not as an underline.  The workaround is to remove
//  the '&' from the text when text rotation is being used.
//-------------------------------------------------------------------

FOR l_Idx = 1 TO i_Tabs
   IF ((l_TabSide = "L" OR l_TabSide = "R") AND l_TabRotate = 'N') OR &
      ((l_TabSide = "T" OR l_TabSide = "B") AND l_TabRotate = 'Y') THEN
      l_Pos = Pos(i_TabArray[l_Idx].TabName, "&")
      IF l_Pos > 0 THEN 
         i_TabArray[l_Idx].TabName = Replace(i_TabArray[l_Idx].TabName, l_Pos, 1, "")
      END IF
   END IF

   IF Pos(i_TabArray[l_Idx].TabName, "&") > 0 THEN
      l_Label = Replace(i_TabArray[l_Idx].TabName, &
                        Pos(i_TabArray[l_Idx].TabName, "&"), 1, "")
   ELSE
      l_Label = i_TabArray[l_Idx].TabName
   END IF

   IF l_DisableAll THEN
      fu_DisableTab(l_Idx)
   END IF

   FOR l_Jdx = 1 TO l_SecuredTabs
      IF l_SecuredTab[l_Jdx] = l_Label THEN
         IF l_SecuredBit[l_Jdx] = 1 THEN
            fu_HideTab(l_Idx)
         ELSEIF l_SecuredBit[l_Jdx] = 2 THEN
            fu_DisableTab(l_Idx)
         ELSEIF l_SecuredBit[l_Jdx] = 3 THEN
             fu_HideTab(l_Idx)
             fu_DisableTab(l_Idx)
         END IF
         EXIT
      END IF
   NEXT
NEXT

fu_BuildLabels()

//------------------------------------------------------------------
//  Call the fu_FolderResize function to create the folder.
//------------------------------------------------------------------

fu_FolderResize()


end subroutine

public subroutine fu_hidetab (integer tab);//******************************************************************
//  PO Module     : u_Folder
//  Subroutine    : fu_HideTab
//  Description   : Hides a tab that had been visible.  Can only
//                  hide tabs on folders that have one row of tabs.  
//
//  Parameters    : INTEGER      Tab          - tab number
//
//  Return Value  : (None)
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

STRING  l_HideString

//-------------------------------------------------------------------
//  If the tab is the current tab then return.
//-------------------------------------------------------------------

IF Tab = i_SelectedTab THEN
   RETURN
END IF

//------------------------------------------------------------------
//  Set the tab to invisible.
//------------------------------------------------------------------

l_HideString = GetItemString(1, "t_visible")
IF IsNull(l_HideString) THEN
   l_HideString = ""
END IF

IF Pos(l_HideString, " " + String(tab, "00") + " ") = 0 THEN
   SetItem(1, "t_visible", l_HideString + " " + String(tab, "00") + " ")
   i_TabArray[Tab].TabHide = TRUE
   fu_BuildLabels()
END IF

RETURN
end subroutine

public subroutine fu_showtab (integer tab);//******************************************************************
//  PO Module     : u_Folder
//  Subroutine    : fu_ShowTab
//  Description   : Shows a tab that had been previously hidden.  
//
//  Parameters    : INTEGER      Tab          - tab number
//
//  Return Value  : (None)
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

LONG    l_Pos
STRING  l_HideString

//-------------------------------------------------------------------
//  If the tab is the current tab then return.
//-------------------------------------------------------------------

IF Tab = i_SelectedTab THEN
   RETURN
END IF

//------------------------------------------------------------------
//  Set the tab to visible.
//------------------------------------------------------------------

l_HideString = GetItemString(1, "t_visible")
l_Pos = Pos(l_HideString, " " + String(tab, "00") + " ")
IF l_Pos > 0 THEN
   SetItem(1, "t_visible", Replace(l_HideString, l_Pos, 4, " "))
   i_TabArray[Tab].TabHide = FALSE
   fu_BuildLabels()
END IF

RETURN
end subroutine

private subroutine fu_setoptions (string optionstyle, string optiontype, unsignedlong visualword);//******************************************************************
//  PO Module     : u_Folder
//  Subroutine    : fu_SetOptions
//  Description   : Sets visual defaults and options.  This function
//                  is used by all the option functions (i.e.
//                  fu_FolderOptions).  It is also called by the
//                  constructor event to set initial defaults.  
//
//  Parameters    : STRING OptionStyle - indicates which function
//                                       is the calling routine.
//                         OptionType  - indicates whether defaults
//                                       or options are being set.
//                  ULONG  VisualWord  - visual options.
//
//  Return Value  : (None)
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

ULONG l_BitMask
ULONG c_FolderTabMask        = 3
ULONG c_FolderStyleMask      = 4
ULONG c_FolderColorMask      = 120
ULONG c_FolderBGColorMask    = 1920
ULONG c_FolderBorderMask     = 2048
ULONG c_FolderResizeMask     = 4096

ULONG c_TextStyleMask        = 3
ULONG c_TextAlignMask        = 12
ULONG c_TextColorMask        = 240
ULONG c_TextRotationMask     = 256

ULONG c_TextCurrentStyleMask = 3
ULONG c_TextCurrentColorMask = 60
ULONG c_TabCurrentColorMask  = 960
ULONG c_TabCurrentStyleMask  = 3072

ULONG c_TextDisableStyleMask = 3
ULONG c_TextDisableColorMask = 60

CHOOSE CASE OptionStyle

   //---------------------------------------------------------------
   //  Set the folder options and defaults.
   //---------------------------------------------------------------

   CASE "Folder"

      //------------------------------------------------------------
      //  Set the background color of the folder.
      //------------------------------------------------------------

      IF OptionType = "Options" THEN
         l_BitMask = w_pomanager_std.fw_BitMask(VisualWord, c_FolderBGColorMask)
      ELSE
         l_BitMask = 0
      END IF

      CHOOSE CASE l_BitMask
         CASE c_FolderBGBlack
            Modify("datawindow.color=" + String(w_pomanager_std.c_Black))
         CASE c_FolderBGWhite
            Modify("datawindow.color=" + String(w_pomanager_std.c_White))
         CASE c_FolderBGGray
            Modify("datawindow.color=" + String(w_pomanager_std.c_Gray))
         CASE c_FolderBGDarkGray
            Modify("datawindow.color=" + String(w_pomanager_std.c_DarkGray))
         CASE c_FolderBGRed
            Modify("datawindow.color=" + String(w_pomanager_std.c_Red))
         CASE c_FolderBGDarkRed
            Modify("datawindow.color=" + String(w_pomanager_std.c_DarkRed))
         CASE c_FolderBGGreen
            Modify("datawindow.color=" + String(w_pomanager_std.c_Green))
         CASE c_FolderBGDarkGreen
            Modify("datawindow.color=" + String(w_pomanager_std.c_DarkGreen))
         CASE c_FolderBGBlue
            Modify("datawindow.color=" + String(w_pomanager_std.c_Blue))
         CASE c_FolderBGDarkBlue
            Modify("datawindow.color=" + String(w_pomanager_std.c_DarkBlue))
         CASE c_FolderBGMagenta
            Modify("datawindow.color=" + String(w_pomanager_std.c_Magenta))
         CASE c_FolderBGDarkMagenta
            Modify("datawindow.color=" + String(w_pomanager_std.c_DarkMagenta))
         CASE c_FolderBGCyan
            Modify("datawindow.color=" + String(w_pomanager_std.c_Cyan))
         CASE c_FolderBGDarkCyan
            Modify("datawindow.color=" + String(w_pomanager_std.c_DarkCyan))
         CASE c_FolderBGYellow
            Modify("datawindow.color=" + String(w_pomanager_std.c_Yellow))
         CASE c_FolderBGDarkYellow
            Modify("datawindow.color=" + String(w_pomanager_std.c_DarkYellow))
      END CHOOSE
		
      //------------------------------------------------------------
      //  Set the location of the tabs.
      //------------------------------------------------------------

      IF OptionType = "Options" THEN
         l_BitMask = w_pomanager_std.fw_BitMask(VisualWord, c_FolderTabMask)
      ELSE
         l_BitMask = 0
      END IF
  
      CHOOSE CASE l_BitMask
         CASE c_FolderTabTop
            SetItem(1, "f_side", "T")
         CASE c_FolderTabBottom
            SetItem(1, "f_side", "B")
         CASE c_FolderTabLeft
            SetItem(1, "f_side", "L")
         CASE c_FolderTabRight
            SetItem(1, "f_side", "R")
      END CHOOSE

      //------------------------------------------------------------
      //  Set the color of the folder.
      //------------------------------------------------------------

      IF OptionType = "Options" THEN
         l_BitMask = w_pomanager_std.fw_BitMask(VisualWord, c_FolderColorMask)
      ELSE
         l_BitMask = 0
      END IF

      CHOOSE CASE l_BitMask
         CASE c_FolderBlack
            SetItem(1, "f_color", w_pomanager_std.c_Black)
            SetItem(1, "f_shade", w_pomanager_std.c_Black)
         CASE c_FolderWhite
            SetItem(1, "f_color", w_pomanager_std.c_White)
            SetItem(1, "f_shade", w_pomanager_std.c_White)
         CASE c_FolderGray
            SetItem(1, "f_color", w_pomanager_std.c_Gray)
            SetItem(1, "f_shade", w_pomanager_std.c_DarkGray)
         CASE c_FolderDarkGray
            SetItem(1, "f_color", w_pomanager_std.c_DarkGray)
            SetItem(1, "f_shade", w_pomanager_std.c_DarkGray)
         CASE c_FolderRed
            SetItem(1, "f_color", w_pomanager_std.c_Red)
            SetItem(1, "f_shade", w_pomanager_std.c_DarkRed)
         CASE c_FolderDarkRed
            SetItem(1, "f_color", w_pomanager_std.c_DarkRed)
            SetItem(1, "f_shade", w_pomanager_std.c_DarkRed)
         CASE c_FolderGreen
            SetItem(1, "f_color", w_pomanager_std.c_Green)
            SetItem(1, "f_shade", w_pomanager_std.c_DarkGreen)
         CASE c_FolderDarkGreen
            SetItem(1, "f_color", w_pomanager_std.c_DarkGreen)
            SetItem(1, "f_shade", w_pomanager_std.c_DarkGreen)
         CASE c_FolderBlue
            SetItem(1, "f_color", w_pomanager_std.c_Blue)
            SetItem(1, "f_shade", w_pomanager_std.c_DarkBlue)
         CASE c_FolderDarkBlue
            SetItem(1, "f_color", w_pomanager_std.c_DarkBlue)
            SetItem(1, "f_shade", w_pomanager_std.c_DarkBlue)
         CASE c_FolderMagenta
            SetItem(1, "f_color", w_pomanager_std.c_Magenta)
            SetItem(1, "f_shade", w_pomanager_std.c_DarkMagenta)
         CASE c_FolderDarkMagenta
            SetItem(1, "f_color", w_pomanager_std.c_DarkMagenta)
            SetItem(1, "f_shade", w_pomanager_std.c_DarkMagenta)
         CASE c_FolderCyan
            SetItem(1, "f_color", w_pomanager_std.c_Cyan)
            SetItem(1, "f_shade", w_pomanager_std.c_DarkCyan)
         CASE c_FolderDarkCyan
            SetItem(1, "f_color", w_pomanager_std.c_DarkCyan)
            SetItem(1, "f_shade", w_pomanager_std.c_DarkCyan)
         CASE c_FolderYellow
            SetItem(1, "f_color", w_pomanager_std.c_Yellow)
            SetItem(1, "f_shade", w_pomanager_std.c_DarkYellow)
         CASE c_FolderDarkYellow
            SetItem(1, "f_color", w_pomanager_std.c_DarkYellow)
            SetItem(1, "f_shade", w_pomanager_std.c_DarkYellow)
      END CHOOSE
		// enme 17-02-2009
		SetItem(1, "f_color", s_themes.theme[s_themes.current_theme].f_folder_color)
		SetItem(1, "f_shade", s_themes.theme[s_themes.current_theme].f_shade_color)
		SetItem(1, "t_bcolor", GetItemNumber(1, "f_color"))

		
//      SetItem(1, "t_bcolor", GetItemNumber(1, "f_color"))

      //------------------------------------------------------------
      //  Set the style of the folder to 2D or 3D.
      //------------------------------------------------------------

      IF OptionType = "Options" THEN
         l_BitMask = w_pomanager_std.fw_BitMask(VisualWord, c_FolderStyleMask)
      ELSE
         l_BitMask = 0
      END IF

      CHOOSE CASE l_BitMask
         CASE c_Folder3D
            SetItem(1, "f_style", "3D")
         CASE c_Folder2D
            SetItem(1, "f_style", "2D")
      END CHOOSE

      //------------------------------------------------------------
      //  Determine if the folder border should be displayed.
      //------------------------------------------------------------

      IF OptionType = "Options" THEN
         l_BitMask = w_pomanager_std.fw_BitMask(VisualWord, c_FolderBorderMask)
      ELSE
         l_BitMask = 0
      END IF

      CHOOSE CASE l_BitMask
         CASE c_FolderBorderOn
            SetItem(1, "f_border", "Y")
         CASE c_FolderBorderOff
            SetItem(1, "f_border", "N")
      END CHOOSE

      //------------------------------------------------------------
      //  Determine if the folder should allow to be resized.
      //------------------------------------------------------------

      IF OptionType = "Options" THEN
         l_BitMask = w_pomanager_std.fw_BitMask(VisualWord, c_FolderResizeMask)
      ELSE
         l_BitMask = 0
      END IF

      CHOOSE CASE l_BitMask
         CASE c_FolderResizeOn
            SetItem(1, "f_resize", "Y")
         CASE c_FolderResizeOff
            SetItem(1, "f_resize", "N")
      END CHOOSE

   //------------------------------------------------------------------
   //  Set the tab options and defaults.
   //------------------------------------------------------------------

   CASE "Tab"

      IF OptionType = "Options" THEN
         l_BitMask = w_pomanager_std.fw_BitMask(VisualWord, c_TextStyleMask)
      ELSE
         l_BitMask = 0
      END IF

      CHOOSE CASE l_BitMask
         CASE c_TextRegular
            SetItem(1, "t_style", "regular")
         CASE c_TextBold
            SetItem(1, "t_style", "bold")
         CASE c_TextItalic
            SetItem(1, "t_style", "italic")
         CASE c_TextUnderline
            SetItem(1, "t_style", "underline")
      END CHOOSE

      IF OptionType = "Options" THEN
         l_BitMask = w_pomanager_std.fw_BitMask(VisualWord, c_TextAlignMask)
      ELSE
         l_BitMask = 0
      END IF

      CHOOSE CASE l_BitMask
         CASE c_TextCenter
            SetItem(1, "t_align", 2)
         CASE c_TextLeft
            SetItem(1, "t_align", 0)
         CASE c_TextRight
            SetItem(1, "t_align", 1)
      END CHOOSE

      IF OptionType = "Options" THEN
         l_BitMask = w_pomanager_std.fw_BitMask(VisualWord, c_TextColorMask)
      ELSE
         l_BitMask = 0
      END IF

      CHOOSE CASE l_BitMask
         CASE c_TextBlack
            SetItem(1, "t_color", w_pomanager_std.c_Black)
         CASE c_TextWhite
            SetItem(1, "t_color", w_pomanager_std.c_White)
         CASE c_TextGray
            SetItem(1, "t_color", w_pomanager_std.c_Gray)
         CASE c_TextDarkGray
            SetItem(1, "t_color", w_pomanager_std.c_DarkGray)
         CASE c_TextRed
            SetItem(1, "t_color", w_pomanager_std.c_Red)
         CASE c_TextDarkRed
            SetItem(1, "t_color", w_pomanager_std.c_DarkRed)
         CASE c_TextGreen
            SetItem(1, "t_color", w_pomanager_std.c_Green)
         CASE c_TextDarkGreen
            SetItem(1, "t_color", w_pomanager_std.c_DarkGreen)
         CASE c_TextBlue
            SetItem(1, "t_color", w_pomanager_std.c_Blue)
         CASE c_TextDarkBlue
            SetItem(1, "t_color", w_pomanager_std.c_DarkBlue)
         CASE c_TextMagenta
            SetItem(1, "t_color", w_pomanager_std.c_Magenta)
         CASE c_TextDarkMagenta
            SetItem(1, "t_color", w_pomanager_std.c_DarkMagenta)
         CASE c_TextCyan
            SetItem(1, "t_color", w_pomanager_std.c_Cyan)
         CASE c_TextDarkCyan
            SetItem(1, "t_color", w_pomanager_std.c_DarkCyan)
         CASE c_TextYellow
            SetItem(1, "t_color", w_pomanager_std.c_Yellow)
         CASE c_TextDarkYellow
            SetItem(1, "t_color", w_pomanager_std.c_DarkYellow)
      END CHOOSE

      IF OptionType = "Options" THEN
         l_BitMask = w_pomanager_std.fw_BitMask(VisualWord, c_TextRotationMask)
      ELSE
         l_BitMask = 0
      END IF

      CHOOSE CASE l_BitMask
         CASE c_TextRotationOn
            SetItem(1, "t_rotate", "Y")
         CASE c_TextRotationOff
            SetItem(1, "t_rotate", "N")
      END CHOOSE

   //------------------------------------------------------------------
   //  Set the current tab options and defaults.
   //------------------------------------------------------------------

   CASE "TabCurrent"

      IF OptionType = "Options" THEN
         l_BitMask = w_pomanager_std.fw_BitMask(VisualWord, c_TextCurrentStyleMask)
      ELSE
         l_BitMask = 0
      END IF

      CHOOSE CASE l_BitMask
         CASE c_TextCurrentRegular
            SetItem(1, "c_style", "regular")
         CASE c_TextCurrentBold
            SetItem(1, "c_style", "bold")
         CASE c_TextCurrentItalic
            SetItem(1, "c_style", "italic")
         CASE c_TextCurrentUnderline
            SetItem(1, "c_style", "underline")
      END CHOOSE

      IF OptionType = "Options" THEN
         l_BitMask = w_pomanager_std.fw_BitMask(VisualWord, c_TextCurrentColorMask)
      ELSE
         l_BitMask = 0
      END IF

      CHOOSE CASE l_BitMask
         CASE c_TextCurrentBlack
            SetItem(1, "c_color", w_pomanager_std.c_Black)
         CASE c_TextCurrentWhite
            SetItem(1, "c_color", w_pomanager_std.c_White)
         CASE c_TextCurrentGray
            SetItem(1, "c_color", w_pomanager_std.c_Gray)
         CASE c_TextCurrentDarkGray
            SetItem(1, "c_color", w_pomanager_std.c_DarkGray)
         CASE c_TextCurrentRed
            SetItem(1, "c_color", w_pomanager_std.c_Red)
         CASE c_TextCurrentDarkRed
            SetItem(1, "c_color", w_pomanager_std.c_DarkRed)
         CASE c_TextCurrentGreen
            SetItem(1, "c_color", w_pomanager_std.c_Green)
         CASE c_TextCurrentDarkGreen
            SetItem(1, "c_color", w_pomanager_std.c_DarkGreen)
         CASE c_TextCurrentBlue
            SetItem(1, "c_color", w_pomanager_std.c_Blue)
         CASE c_TextCurrentDarkBlue
            SetItem(1, "c_color", w_pomanager_std.c_DarkBlue)
         CASE c_TextCurrentMagenta
            SetItem(1, "c_color", w_pomanager_std.c_Magenta)
         CASE c_TextCurrentDarkMagenta
            SetItem(1, "c_color", w_pomanager_std.c_DarkMagenta)
         CASE c_TextCurrentCyan
            SetItem(1, "c_color", w_pomanager_std.c_Cyan)
         CASE c_TextCurrentDarkCyan
            SetItem(1, "c_color", w_pomanager_std.c_DarkCyan)
         CASE c_TextCurrentYellow
            SetItem(1, "c_color", w_pomanager_std.c_Yellow)
         CASE c_TextCurrentDarkYellow
            SetItem(1, "c_color", w_pomanager_std.c_DarkYellow)
      END CHOOSE
		// EnMe 17-02-2009
		SetItem(1, "c_color", s_themes.theme[s_themes.current_theme].f_tab_enabled_textcolor )

      IF OptionType = "Options" THEN
         l_BitMask = w_pomanager_std.fw_BitMask(VisualWord, c_TabCurrentColorMask)
      ELSE
         l_BitMask = 0
      END IF

      CHOOSE CASE l_BitMask
         CASE c_TabCurrentBlack
            SetItem(1, "c_bcolor", w_pomanager_std.c_Black)
         CASE c_TabCurrentWhite
            SetItem(1, "c_bcolor", w_pomanager_std.c_White)
         CASE c_TabCurrentGray
            SetItem(1, "c_bcolor", w_pomanager_std.c_Gray)
         CASE c_TabCurrentDarkGray
            SetItem(1, "c_bcolor", w_pomanager_std.c_DarkGray)
         CASE c_TabCurrentRed 
            SetItem(1, "c_bcolor", w_pomanager_std.c_Red)
         CASE c_TabCurrentDarkRed 
            SetItem(1, "c_bcolor", w_pomanager_std.c_DarkRed)
         CASE c_TabCurrentGreen 
            SetItem(1, "c_bcolor", w_pomanager_std.c_Green)
         CASE c_TabCurrentDarkGreen 
            SetItem(1, "c_bcolor", w_pomanager_std.c_DarkGreen)
         CASE c_TabCurrentBlue 
            SetItem(1, "c_bcolor", w_pomanager_std.c_Blue)
         CASE c_TabCurrentDarkBlue 
            SetItem(1, "c_bcolor", w_pomanager_std.c_DarkBlue)
         CASE c_TabCurrentMagenta 
            SetItem(1, "c_bcolor", w_pomanager_std.c_Magenta)
         CASE c_TabCurrentDarkMagenta 
            SetItem(1, "c_bcolor", w_pomanager_std.c_DarkMagenta)
         CASE c_TabCurrentCyan 
            SetItem(1, "c_bcolor", w_pomanager_std.c_Cyan)
         CASE c_TabCurrentDarkCyan 
            SetItem(1, "c_bcolor", w_pomanager_std.c_DarkCyan)
         CASE c_TabCurrentYellow 
            SetItem(1, "c_bcolor", w_pomanager_std.c_Yellow)
         CASE c_TabCurrentDarkYellow 
            SetItem(1, "c_bcolor", w_pomanager_std.c_DarkYellow)
      END CHOOSE
		// EnMe 17-02-2009
		SetItem(1, "c_bcolor", s_themes.theme[s_themes.current_theme].f_tab_color )

      IF OptionType = "Options" THEN
         l_BitMask = w_pomanager_std.fw_BitMask(VisualWord, c_TabCurrentStyleMask)
      ELSE
         l_BitMask = 0
      END IF

      CHOOSE CASE l_BitMask
         CASE c_TabCurrentColorFolder
            SetItem(1, "c_colorarea", "FOLDER")
         CASE c_TabCurrentColorBorder
            SetItem(1, "c_colorarea", "BORDER")
         CASE c_TabCurrentColorTab
            SetItem(1, "c_colorarea", "TAB")
      END CHOOSE

   //------------------------------------------------------------------
   //  Set the disable tab options and defaults.
   //------------------------------------------------------------------

   CASE "TabDisable"

      IF OptionType = "Options" THEN
         l_BitMask = w_pomanager_std.fw_BitMask(VisualWord, c_TextDisableStyleMask)
      ELSE
         l_BitMask = 0
      END IF

      CHOOSE CASE l_BitMask
         CASE c_TextDisableRegular
            SetItem(1, "d_style", "regular")
         CASE c_TextDisableBold
            SetItem(1, "d_style", "bold")
         CASE c_TextDisableItalic
            SetItem(1, "d_style", "italic")
         CASE c_TextDisableUnderline
            SetItem(1, "d_style", "underline")
      END CHOOSE

      IF OptionType = "Options" THEN
         l_BitMask = w_pomanager_std.fw_BitMask(VisualWord, c_TextDisableColorMask)
      ELSE
         l_BitMask = 0
      END IF

      CHOOSE CASE l_BitMask
         CASE c_TextDisableBlack
            SetItem(1, "d_color", w_pomanager_std.c_Black)
         CASE c_TextDisableWhite
            SetItem(1, "d_color", w_pomanager_std.c_White)
         CASE c_TextDisableGray
            SetItem(1, "d_color", w_pomanager_std.c_Gray)
         CASE c_TextDisableDarkGray
            SetItem(1, "d_color", w_pomanager_std.c_DarkGray)
         CASE c_TextDisableRed
            SetItem(1, "d_color", w_pomanager_std.c_Red)
         CASE c_TextDisableDarkRed
            SetItem(1, "d_color", w_pomanager_std.c_DarkRed)
         CASE c_TextDisableGreen
            SetItem(1, "d_color", w_pomanager_std.c_Green)
         CASE c_TextDisableDarkGreen
            SetItem(1, "d_color", w_pomanager_std.c_DarkGreen)
         CASE c_TextDisableBlue
            SetItem(1, "d_color", w_pomanager_std.c_Blue)
         CASE c_TextDisableDarkBlue
            SetItem(1, "d_color", w_pomanager_std.c_DarkBlue)
         CASE c_TextDisableMagenta
            SetItem(1, "d_color", w_pomanager_std.c_Magenta)
         CASE c_TextDisableDarkMagenta
            SetItem(1, "d_color", w_pomanager_std.c_DarkMagenta)
         CASE c_TextDisableCyan
            SetItem(1, "d_color", w_pomanager_std.c_Cyan)
         CASE c_TextDisableDarkCyan
            SetItem(1, "d_color", w_pomanager_std.c_DarkCyan)
         CASE c_TextDisableYellow
            SetItem(1, "d_color", w_pomanager_std.c_Yellow)
         CASE c_TextDisableDarkYellow
            SetItem(1, "d_color", w_pomanager_std.c_DarkYellow)
      END CHOOSE
		// EnMe 17-02-2009
		SetItem(1, "d_color", s_themes.theme[s_themes.current_theme].f_tab_disabled_textcolor )

END CHOOSE



end subroutine

public subroutine fu_disabletabname (string tabname);//******************************************************************
//  PO Module     : u_Folder
//  Subroutine    : fu_DisableTabName
//  Description   : Disables a tab that had been enabled.  The tab
//                  is specified by the tab label instead of the
//                  tab number.  
//
//  Parameters    : INTEGER      TabName          - tab label
//
//  Return Value  : (None)
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER l_Idx, l_Pos, l_Tabs
STRING  l_TabName

//------------------------------------------------------------------
//  Cycle through the tabs looking for the tab to disable.  Use the
//  tab label after stripping the '&'.  After finding the tab, call
//  the fu_DisableTab function to do the actual disabling.
//------------------------------------------------------------------

l_Tabs = GetItemNumber(1, "t_num")
FOR l_Idx = 1 to l_Tabs
   l_Pos = POS(i_TabArray[l_Idx].TabName, "&")
   IF l_Pos > 0 THEN
      l_TabName = Replace(i_TabArray[l_Idx].TabName, l_Pos, 1, "")
   ELSE
      l_TabName = i_TabArray[l_Idx].TabName
   END IF
	IF TabName = l_TabName THEN
		fu_DisableTab(l_Idx)
		RETURN
	END IF
NEXT

RETURN
end subroutine

public subroutine fu_enabletabname (string tabname);//******************************************************************
//  PO Module     : u_Folder
//  Subroutine    : fu_EnableTabname
//  Description   : Enables a tab that had been disabled.  The tab
//                  is specified by  the tab label instead of the
//                  tab number.  
//
//  Parameters    : INTEGER      TabName          - tab label
//
//  Return Value  : (None)
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER l_Idx, l_Pos, l_Tabs
STRING  l_TabName

//------------------------------------------------------------------
//  Cycle through the tabs looking for the tab to enable.  Use the
//  tab label after stripping the '&'.  After finding the tab, call
//  the fu_EnableTab function to do the actual enabling.
//------------------------------------------------------------------

l_Tabs = GetItemNumber(1, "t_num")
FOR l_Idx = 1 to l_Tabs
   l_Pos = POS(i_TabArray[l_Idx].TabName, "&")
   IF l_Pos > 0 THEN
      l_TabName = Replace(i_TabArray[l_Idx].TabName, l_Pos, 1, "")
   ELSE
      l_TabName = i_TabArray[l_Idx].TabName
   END IF
	IF TabName = l_TabName THEN
		fu_EnableTab(l_Idx)
		RETURN
	END IF
NEXT

RETURN
end subroutine

public subroutine fu_tabkeydown ();//******************************************************************
//  PO Module     : u_Folder
//  Subroutine    : fu_TabKeyDown
//  Description   : Processes keyboard input for selecting a tab.  
//
//  Parameters    : (None)
//
//  Return Value  : (None)
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER l_Idx, l_Pos, l_TabChar, l_Tabs

//------------------------------------------------------------------
//  Cycle through the tabs looking to see if the keyboard input from
//  the user matches any of tabs.  If so, call the fu_SelectTab
//  function to select the tab.
//------------------------------------------------------------------

l_Tabs = GetItemNumber(1, "t_num")
FOR l_Idx = 1 TO l_Tabs
   l_Pos = POS(i_TabArray[l_Idx].TabName, "&")
   IF l_Pos > 0 THEN
      l_TabChar = ASC(UPPER(MID(i_TabArray[l_Idx].TabName, l_Pos + 1, 1)))
      IF KeyDown(l_TabChar) THEN
         fu_SelectTab(l_Idx)
         EXIT
      END IF
   END IF
NEXT

RETURN


end subroutine

public subroutine fu_hidetabname (string tabname);//******************************************************************
//  PO Module     : u_Folder
//  Subroutine    : fu_HideTabName
//  Description   : Hides a tab that had been visible.  Uses the
//                  label of a tab instaed of the tab number. 
//
//  Parameters    : INTEGER      TabName          - tab label
//
//  Return Value  : (None)
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER l_Idx, l_Pos, l_Tabs
STRING  l_TabName

//------------------------------------------------------------------
//  Cycle through the tabs looking for the tab to hide.  Use the
//  tab label after stripping the '&'.  After finding the tab, call
//  the fu_HideTab function to do the actual hiding.
//------------------------------------------------------------------

l_Tabs = GetItemNumber(1, "t_num")
FOR l_Idx = 1 to l_Tabs
   l_Pos = POS(i_TabArray[l_Idx].TabName, "&")
   IF l_Pos > 0 THEN
      l_TabName = Replace(i_TabArray[l_Idx].TabName, l_Pos, 1, "")
   ELSE
      l_TabName = i_TabArray[l_Idx].TabName
   END IF
	IF TabName = l_TabName THEN
		fu_HideTab(l_Idx)
	END IF
NEXT

RETURN
end subroutine

public subroutine fu_showtabname (string tabname);//******************************************************************
//  PO Module     : u_Folder
//  Subroutine    : fu_ShowTabName
//  Description   : Uses the tab label to show a tab that had been 
//                  previously hidden.  
//
//  Parameters    : INTEGER      TabName       - tab label
//
//  Return Value  : (None)
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER l_Idx, l_Pos, l_Tabs
STRING  l_TabName

//------------------------------------------------------------------
//  Cycle through the tabs looking for the tab to show.  Use the
//  tab label after stripping the '&'.  After finding the tab, call
//  the fu_ShowTab function to do the actual showing.
//------------------------------------------------------------------

l_Tabs = GetItemNumber(1, "t_num")
FOR l_Idx = 1 to l_Tabs
   l_Pos = POS(i_TabArray[l_Idx].TabName, "&")
   IF l_Pos > 0 THEN
      l_TabName = Replace(i_TabArray[l_Idx].TabName, l_Pos, 1, "")
   ELSE
      l_TabName = i_TabArray[l_Idx].TabName
   END IF
	IF TabName = l_TabName THEN
		fu_ShowTab(l_Idx)
	END IF
NEXT

RETURN
end subroutine

public subroutine fu_assigntabobjects (integer tab, ref windowobject tabobjects[]);//******************************************************************
//  PO Module     : u_Folder
//  Subroutine    : fu_AssignTabObjects
//  Description   : Assigns window objects to a tab.  
//
//  Parameters    : INTEGER      Tab          - tab number
//                  WINDOWOBJECT TabObjects[] - array of window 
//                                              objects
//
//  Return Value  : (None)
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER l_Idx

//------------------------------------------------------------------
//  Assign the window objects to the tab. Hide each of the window 
//  objects.
//------------------------------------------------------------------

i_TabArray[Tab].TabNumObjects = UpperBound(TabObjects[])
FOR l_Idx = 1 To i_TabArray[Tab].TabNumObjects
   i_TabArray[Tab].TabObjects[l_Idx] = TabObjects[l_Idx]
   i_TabArray[Tab].TabObjectCentered[l_Idx] = FALSE
   IF i_SelectedTab = Tab THEN
      TabObjects[l_Idx].Visible = TRUE
   ELSE        
      TabObjects[l_Idx].Visible = FALSE
   END IF
NEXT

end subroutine

public subroutine fu_assigntablabel (integer tab, string tablabel);//******************************************************************
//  PO Module     : u_Folder
//  Subroutine    : fu_AssignTabLabel
//  Description   : Assigns a label to a tab.  
//
//  Parameters    : INTEGER      Tab          - tab number
//                  STRING       TabLabel     - tab label
//
//  Return Value  : (None)
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

//------------------------------------------------------------------
//  Assign the label to the tab.  
//------------------------------------------------------------------

i_TabArray[Tab].TabName    = TabLabel
i_TabArray[Tab].TabHide    = FALSE
i_TabArray[Tab].TabDisable = FALSE



end subroutine

public subroutine fu_folderworkspace (ref integer ws_x, ref integer ws_y, ref integer ws_width, ref integer ws_height);//******************************************************************
//  PO Module     : u_Folder
//  Subroutine    : fu_FolderWorkSpace
//  Description   : Returns the working area of the folder object.
//                  This may be used by the developer to place
//                  objects on the folder. The values are returned
//                  in PowerBuilder units. 
//
//  Parameters    : INTEGER WS_X      - top-left corner of the folder
//                                      portion of the object.
//                  INTEGER WS_Y      - top-left corner of the folder
//                                      portion of the object.
//                  INTEGER WS_WIDTH  - width of the folder portion
//                                      of the object.
//                  INTEGER WS_HEIGHT - height of the folder portion
//                                      of the object.
//
//  Return Value  : (None)
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

USEROBJECT l_UserObject
INTEGER    l_X, l_Y

IF Parent.TypeOf() = Window! THEN
   l_X          = X
   l_Y          = Y
ELSE
   l_UserObject = Parent
   l_X          = l_UserObject.X + X
   l_Y          = l_UserObject.Y + Y
END IF

WS_X      = l_X + PixelsToUnits(GetItemNumber(1, "f_workx"), XPixelsToUnits!)
WS_Y      = l_Y + PixelsToUnits(GetItemNumber(1, "f_worky"), YPixelsToUnits!)
WS_Width  = PixelsToUnits(GetItemNumber(1, "f_workwidth"), XPixelsToUnits!)
WS_Height = PixelsToUnits(GetItemNumber(1, "f_workheight"), YPixelsToUnits!)
end subroutine

public function integer fu_centertabobject (dragobject tab_object);//******************************************************************
//  PO Module     : u_Folder
//  Subroutine    : fu_CenterTabObject
//  Description   : Centers the given window object on the folder. 
//
//  Parameters    : DRAGOBJECT Tab_Object - the object to center.
//
//  Return Value  : 0 = object centered   -1 = object too large to
//                                             center
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER l_FolderX, l_FolderY, l_FolderWidth, l_FolderHeight, l_Jdx
INTEGER l_ObjectWidth, l_ObjectHeight, l_X, l_Y, l_Return, l_Idx
INTEGER l_NumControls
BOOLEAN l_Finished, l_Found
USEROBJECT l_UserObject

l_ObjectWidth  = tab_object.Width
l_ObjectHeight = tab_object.Height

l_X            = X + PixelsToUnits(GetItemNumber(1, "f_workx"), XPixelsToUnits!)
l_Y            = Y + PixelsToUnits(GetItemNumber(1, "f_worky"), YPixelsToUnits!)
l_FolderWidth  = PixelsToUnits(GetItemNumber(1, "f_workwidth"), XPixelsToUnits!)
l_FolderHeight = PixelsToUnits(GetItemNumber(1, "f_workheight"), YPixelsToUnits!)

IF Parent.TypeOf() = Window! THEN
   l_FolderX = l_X
   l_FolderY = l_Y
ELSE
   l_UserObject = Parent
   l_Found = FALSE
   l_NumControls = UpperBound(l_UserObject.Control[])
   FOR l_Idx = 1 TO l_NumControls
      IF l_UserObject.Control[l_Idx] = tab_object THEN
         l_Found = TRUE
         EXIT
      END IF
   NEXT

   IF l_Found THEN
      l_FolderX = l_X
      l_FolderY = l_Y
   ELSE
      l_FolderX = l_UserObject.X + l_X
      l_FolderY = l_UserObject.Y + l_Y
   END IF
END IF

IF l_ObjectWidth > l_FolderWidth THEN
   l_X = l_FolderX
   l_Return = -1
ELSE
   l_X = ((l_FolderWidth - l_ObjectWidth) / 2) + l_FolderX
END IF

IF l_ObjectHeight > l_FolderHeight THEN
   l_Y = l_FolderY
   l_Return = -1
ELSE
   l_Y = ((l_FolderHeight - l_ObjectHeight) / 2) + l_FolderY
END IF

l_Finished = FALSE
FOR l_Idx = 1 TO i_Tabs
   IF i_TabArray[l_Idx].TabNumObjects > 0 THEN
      FOR l_Jdx = 1 TO i_TabArray[l_Idx].TabNumObjects
         IF i_TabArray[l_Idx].TabObjects[l_Jdx] = tab_object THEN
            i_TabArray[l_Idx].TabObjectCentered[l_Jdx] = TRUE
            l_Finished = TRUE
            EXIT
         END IF
      NEXT

      IF l_Finished THEN
         EXIT
      END IF
   END IF
NEXT

tab_object.Move(l_X, l_Y)

tab_object.BringToTop = TRUE

RETURN l_Return
end function

public subroutine fu_tabinfo (integer tab, ref string name, ref boolean tab_enabled, ref boolean tab_visible, ref windowobject objects[]);//******************************************************************
//  PO Module     : u_Folder
//  Subroutine    : fu_TabInfo
//  Description   : Returns information about the given tab.  
//
//  Parameters    : INTEGER      Tab         - tab number
//                  STRING       Name        - tab label
//                  BOOLEAN      Tab_Enabled - is tab enabled?
//                  BOOLEAN      Tab_Visible - is tab visible?
//                  WINDOWOBJECT Objects[]   - array of window 
//                                             objects
//
//  Return Value  : (None)
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

IF tab <= 0 OR tab > GetItemNumber(1, "t_num") THEN
   RETURN
END IF

//------------------------------------------------------------------
//  Return the name of the tab label.
//------------------------------------------------------------------

name = i_TabArray[tab].TabName

//------------------------------------------------------------------
//  Return the status of the tab.
//------------------------------------------------------------------

IF i_TabArray[tab].TabDisable THEN
   tab_enabled = FALSE
ELSE
   tab_enabled = TRUE
END IF

IF i_TabArray[tab].TabHide THEN
   tab_visible = FALSE
ELSE
   tab_visible = TRUE
END IF

//------------------------------------------------------------------
//  Return an array of window objects assigned to the tab.
//------------------------------------------------------------------

objects[] = i_TabArray[tab].TabObjects[]

RETURN
end subroutine

public subroutine fu_foldercreatetb (integer tabs_per_row);//******************************************************************
//  PO Module     : u_Folder
//  Subroutine    : fu_FolderCreateTB
//  Description   : Creates extra tabs and rows for the folder when
//                  tabs are on the top or bottom.  
//
//  Parameters    : INTEGER Tabs_Per_Row - number of tabs per row
//
//  Return Value  : (None)
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

STRING     l_TabN, l_TabS, l_Text, l_Fill, l_ROut, l_TOut, l_TIn
STRING     l_RIn, l_LOut, l_LIn, l_RowN, l_Rect
INTEGER    l_Idx, l_NumRows, l_TotalTabs

//------------------------------------------------------------------
//  Determine if more than 12 tabs are needed.
//------------------------------------------------------------------

l_TotalTabs = GetItemNumber(1, "t_tnum")
IF l_TotalTabs > 12 THEN
   FOR l_Idx = 13 to l_TotalTabs
      l_TabN = String(l_Idx)
      l_TabS = String(l_Idx, "00")
      l_Rect = "create rectangle(name=tab" + l_TabN + "_rect band=detail pen.style=~"0~" " + &
               "visible=~"0~tif(t_tnum < " + l_TabN + " or (pos(t_visible,' " + l_TabS + " ') > 0 and (t_num / t_rows) <= 1), 0, 1)~" " + &
               "pen.width=~"1~" pen.color=~"0~tif(t_current = " + l_TabN + ", c_bcolor, if(mid(t_pos,(" + l_TabN + " - 1) * 8 + 8, 1) = 'Y', d_bcolor, t_bcolor))~" " + & 
               "brush.hatch=~"6~" brush.color=~"0~tif(t_current = " + l_TabN + ", c_bcolor, if(mid(t_pos,(" + l_TabN + " - 1) * 8 + 8, 1) = 'Y', d_bcolor, t_bcolor))~" " + & 
               "background.mode=~"2~" background.color=~"0~tif(t_current = " + l_TabN + ", c_bcolor, if(mid(t_pos,(" + l_TabN + " - 1) * 8 + 8, 1) = 'Y', d_bcolor, t_bcolor))~" " + &
               "x=~"0~t(mod(" + l_TabN + " - 1,t_rows) + 0) * t_width + (ceiling(" + l_TabN + " / t_rows) - 1) * t_offset + 2~" " + & 
               "y=~"0~tif(f_side = 'T', t_theight - (ceiling(" + l_TabN + " / t_rows) * t_height) + 4, if(ceiling(" + l_TabN + " / t_rows) > 1, t_theight + ((ceiling(" + l_TabN + " / t_rows) - 1) * t_height) + 1, if(t_current = " + l_TabN + ", if(f_border = 'Y' and f_style = '3D', t_theight + ((ceiling(" + l_TabN + " / t_rows) - 1) * t_height) - 2, t_theight + ((ceiling(" + l_TabN + " / t_rows) - 1) * t_height)), t_theight + ((ceiling(" + l_TabN + " / t_rows) - 1) * t_height) + 1)))~" " + &
               "height=~"0~tif(ceiling(" + l_TabN + " / t_rows) > 1, t_height - 4, if(t_current = " + l_TabN + ", if(f_border = 'Y' and f_style = '3D', t_height - 1, t_height - 3), t_height - 4))~" " + & 
               "width=~"0~tt_width - 2~" ) "
      l_Text = "create text(name=tab" + l_TabN + "_text band=detail text=~" ~tmid(t_label,integer(mid(t_pos, (" + l_TabN + " - 1) * 8 + 4, 2)), integer(mid(t_pos, (" + l_TabN + " - 1) * 8 + 6, 2)) )~" " + & 
               "visible=~"0~tif(t_tnum < " + l_TabN + " or (pos(t_visible,' " + l_TabS + " ') > 0 and (t_num / t_rows) <= 1), 0, 1)~" " + &
               "border=~"0~" alignment=~"2~tt_align~" " + & 
               "background.color=~"0~tif(t_current = " + l_TabN + ", c_bcolor, if(mid(t_pos,(" + l_TabN + " - 1) * 8 + 8, 1) = 'Y', d_bcolor, t_bcolor))~" " + &
               "color=~"0~tif(t_current = " + l_TabN + ", c_color, if(mid(t_pos,(" + l_TabN + " - 1) * 8 + 8, 1) = 'Y', d_color, t_color))~" " + & 
               "font.height=~"0~tif(t_current = " + l_TabN + ", c_size * -1, if(mid(t_pos,(" + l_TabN + " - 1) * 8 + 8, 1) = 'Y', d_size * -1, t_size * -1))~" " + & 
               "font.face=~"Arial~tif(t_current = " + l_TabN + ", c_font, if(mid(t_pos,(" + l_TabN + " - 1) * 8 + 8, 1) = 'Y', d_font, t_font))~" " + & 
               "font.family=~"2~tif(t_current = " + l_TabN + ", c_family, if(mid(t_pos,(" + l_TabN + " - 1) * 8 + 8, 1) = 'Y', d_family, t_family))~" " + & 
               "font.pitch=~"2~tif(t_current = " + l_TabN + ", c_pitch, if(mid(t_pos,(" + l_TabN + " - 1) * 8 + 8, 1) = 'Y', d_pitch, t_pitch))~" " + & 
               "font.charset=~"0~tif(t_current = " + l_TabN + ", c_charset, if(mid(t_pos,(" + l_TabN + " - 1) * 8 + 8, 1) = 'Y', d_charset, t_charset))~" " + &   
               "font.weight=~"0~tif(t_current = " + l_TabN + ", if(c_style='bold', 700, 400), if(mid(t_pos,(" + l_TabN + " - 1) * 8 + 8, 1) = 'Y', if(d_style='bold', 700, 400), if(t_style='bold', 700, 400)))~" " + & 
               "font.italic=~"0~tif(t_current = " + l_TabN + ", if(c_style='italic', 1, 0), if(mid(t_pos,(" + l_TabN + " - 1) * 8 + 8, 1) = 'Y', if(d_style='italic', 1, 0), if(t_style='italic', 1, 0)))~" " + & 
               "font.underline=~"0~tif(t_current = " + l_TabN + ", if(c_style='underline', 1, 0), if(mid(t_pos,(" + l_TabN + " - 1) * 8 + 8, 1) = 'Y', if(d_style='underline', 1, 0), if(t_style='underline', 1, 0)))~" " + & 
               "x=~"0~t(mod(" + l_TabN + " - 1,t_rows) + 0) * t_width + (ceiling(" + l_TabN + " / t_rows) - 1) * t_offset + 3~" " + & 
               "y=~"0~tif(f_side = 'T', t_theight - (ceiling(" + l_TabN + " / t_rows) * t_height) + 4, t_theight + ((ceiling(" + l_TabN + " / t_rows) - 1) * t_height) + 1)~" " + & 
               "height=~"0~tt_height - 4~" " + & 
               "width=~"0~tt_width - 4~" ) "
      l_Fill = "create line(name=tab" + l_TabN + "_fill1 band=detail background.color=~"12632256~" " + & 
               "visible=~"0~tif(t_tnum < " + l_TabN + " or (pos(t_visible,' " + l_TabS + " ') > 0 and (t_num / t_rows) <= 1), 0, 1)~" " + &
               "pen.width=~"1~" pen.color=~"0~tif(t_current = " + l_TabN + ", c_bcolor, t_bcolor)~" " + & 
               "x1=~"0~t(mod(" + l_TabN + " - 1,t_rows) + 0) * t_width + (ceiling(" + l_TabN + " / t_rows) - 1) * t_offset + 3~" " + & 
               "y1=~"0~tt_theight - ((ceiling(" + l_TabN + " / t_rows) * t_height) * f_direct) + (2 * f_direct)~" " + & 
               "x2=~"0~t(mod(" + l_TabN + " - 1,t_rows) + 1) * t_width + (ceiling(" + l_TabN + " / t_rows) - 1) * t_offset - 3~" " + & 
               "y2=~"0~tt_theight - ((ceiling(" + l_TabN + " / t_rows) * t_height) * f_direct) + (2 * f_direct)~" ) " + &
               "create line(name=tab" + l_TabN + "_fill2 band=detail background.color=~"12632256~" " + & 
               "visible=~"0~tif(t_tnum < " + l_TabN + " or (pos(t_visible,' " + l_TabS + " ') > 0 and (t_num / t_rows) <= 1), 0, 1)~" " + &
               "pen.width=~"1~" pen.color=~"0~tif(t_current = " + l_TabN + ", c_bcolor, t_bcolor)~" " + & 
               "x1=~"0~t(mod(" + l_TabN + " - 1,t_rows) + 0) * t_width + (ceiling(" + l_TabN + " / t_rows) - 1) * t_offset + 2~" " + & 
               "y1=~"0~tt_theight - ((ceiling(" + l_TabN + " / t_rows) * t_height) * f_direct) + (3 * f_direct)~" " + & 
               "x2=~"0~t(mod(" + l_TabN + " - 1,t_rows) + 1) * t_width + (ceiling(" + l_TabN + " / t_rows) - 1) * t_offset - 2~" " + & 
               "y2=~"0~tt_theight - ((ceiling(" + l_TabN + " / t_rows) * t_height) * f_direct) + (3 * f_direct)~" ) "
      l_LOut = "create line(name=tab" + l_TabN + "_left_out band=detail background.color=~"12632256~" " + & 
               "visible=~"0~tif(t_tnum < " + l_TabN + " or (pos(t_visible,' " + l_TabS + " ') > 0 and (t_num / t_rows) <= 1), 0, 1)~" " + &
               "pen.width=~"1~" pen.color=~"0~" " + & 
               "x1=~"0~t(mod(" + l_TabN + " - 1,t_rows) + 0) * t_width + (ceiling(" + l_TabN + " / t_rows) - 1) * t_offset~" " + & 
               "y1=~"0~tt_theight - ((ceiling(" + l_TabN + " / t_rows) * t_height) * f_direct) + (4 * f_direct)~" " + & 
               "x2=~"0~t(mod(" + l_TabN + " - 1,t_rows) + 0) * t_width + (ceiling(" + l_TabN + " / t_rows) - 1) * t_offset~" " + & 
               "y2=~"0~tt_theight - ((ceiling(" + l_TabN + " / t_rows) - 1) * t_height) * f_direct~" ) " + &
               "create line(name=tab" + l_TabN + "_leftc_out band=detail background.color=~"12632256~" " + & 
               "visible=~"0~tif(t_tnum < " + l_TabN + " or (pos(t_visible,' " + l_TabS + " ') > 0 and (t_num / t_rows) <= 1), 0, 1)~" " + &
               "pen.width=~"1~" pen.color=~"0~" " + & 
               "x1=~"0~t(mod(" + l_TabN + " - 1,t_rows) + 0) * t_width + (ceiling(" + l_TabN + " / t_rows) - 1) * t_offset~" " + & 
               "y1=~"0~tt_theight - ((ceiling(" + l_TabN + " / t_rows) * t_height) * f_direct) + (4 * f_direct)~" " + & 
               "x2=~"0~t(mod(" + l_TabN + " - 1,t_rows) + 0) * t_width + (ceiling(" + l_TabN + " / t_rows) - 1) * t_offset + 4~" " + & 
               "y2=~"0~tt_theight - (ceiling(" + l_TabN + " / t_rows) * t_height) * f_direct~" ) "
      l_TOut = "create line(name=tab" + l_TabN + "_top_out band=detail background.color=~"12632256~" " + & 
               "visible=~"0~tif(t_tnum < " + l_TabN + " or (pos(t_visible,' " + l_TabS + " ') > 0 and (t_num / t_rows) <= 1), 0, 1)~" " + &
               "pen.width=~"1~" pen.color=~"0~" " + & 
               "x1=~"0~t(mod(" + l_TabN + " - 1,t_rows) + 0) * t_width + (ceiling(" + l_TabN + " / t_rows) - 1) * t_offset + 4~" " + & 
               "y1=~"0~tt_theight - (ceiling(" + l_TabN + " / t_rows) * t_height) * f_direct~" " + & 
               "x2=~"0~t(mod(" + l_TabN + " - 1,t_rows) + 1) * t_width + (ceiling(" + l_TabN + " / t_rows) - 1) * t_offset - 3~" " + & 
               "y2=~"0~tt_theight - (ceiling(" + l_TabN + " / t_rows) * t_height) * f_direct~" ) "
      l_ROut = "create line(name=tab" + l_TabN + "_rightc_out band=detail background.color=~"12632256~" " + & 
               "visible=~"0~tif(t_tnum < " + l_TabN + " or (pos(t_visible,' " + l_TabS + " ') > 0 and (t_num / t_rows) <= 1), 0, 1)~" " + &
               "pen.width=~"1~" pen.color=~"0~" " + & 
               "x1=~"0~t(mod(" + l_TabN + " - 1,t_rows) + 1) * t_width + (ceiling(" + l_TabN + " / t_rows) - 1) * t_offset - 4~" " + & 
               "y1=~"0~tt_theight - (ceiling(" + l_TabN + " / t_rows) * t_height) * f_direct~" " + & 
               "x2=~"0~t(mod(" + l_TabN + " - 1,t_rows) + 1) * t_width + (ceiling(" + l_TabN + " / t_rows) - 1) * t_offset~" " + & 
               "y2=~"0~tt_theight - ((ceiling(" + l_TabN + " / t_rows) * t_height) * f_direct) + (4 * f_direct)~" ) " + &
               "create line(name=tab" + l_TabN + "_right_out band=detail background.color=~"12632256~" " + & 
               "visible=~"0~tif(t_tnum < " + l_TabN + " or (pos(t_visible,' " + l_TabS + " ') > 0 and (t_num / t_rows) <= 1), 0, 1)~" " + &
               "pen.width=~"1~" pen.color=~"0~" " + & 
               "x1=~"0~t(mod(" + l_TabN + " - 1,t_rows) + 1) * t_width + (ceiling(" + l_TabN + " / t_rows) - 1) * t_offset~" " + & 
               "y1=~"0~tt_theight - ((ceiling(" + l_TabN + " / t_rows) * t_height) * f_direct) + (4 * f_direct)~" " + & 
               "x2=~"0~t(mod(" + l_TabN + " - 1,t_rows) + 1) * t_width + (ceiling(" + l_TabN + " / t_rows) - 1) * t_offset~" " + & 
               "y2=~"0~tt_theight - ((ceiling(" + l_TabN + " / t_rows) - 1) * t_height) * f_direct~" ) "
      l_LIn  = "create line(name=tab" + l_TabN + "_left_in band=detail background.color=~"12632256~" " + & 
               "visible=~"0~tif(t_tnum < " + l_TabN + " or (pos(t_visible,' " + l_TabS + " ') > 0 and (t_num / t_rows) <= 1), 0, 1)~" " + &
               "pen.width=~"1~" pen.color=~"0~tif(f_style='3D', 16777215, if(t_current = " + l_TabN + ", c_bcolor, t_bcolor))~" " + & 
               "x1=~"0~t(mod(" + l_TabN + " - 1,t_rows) + 0) * t_width + (ceiling(" + l_TabN + " / t_rows) - 1) * t_offset + 1~" " + & 
               "y1=~"0~tt_theight - ((ceiling(" + l_TabN + " / t_rows) * t_height) * f_direct) + (4 * f_direct)~" " + & 
               "x2=~"0~t(mod(" + l_TabN + " - 1,t_rows) + 0) * t_width + (ceiling(" + l_TabN + " / t_rows) - 1) * t_offset + 1~" " + & 
               "y2=~"0~tt_theight - ((ceiling(" + l_TabN + " / t_rows) - 1) * t_height) * f_direct~" ) " + &
               "create line(name=tab" + l_TabN + "_leftc_in band=detail background.color=~"12632256~" " + & 
               "visible=~"0~tif(t_tnum < " + l_TabN + " or (pos(t_visible,' " + l_TabS + " ') > 0 and (t_num / t_rows) <= 1), 0, 1)~" " + &
               "pen.width=~"1~" pen.color=~"0~tif(f_style='3D', 16777215, if(t_current = " + l_TabN + ", c_bcolor, t_bcolor))~" " + & 
               "x1=~"0~t(mod(" + l_TabN + " - 1,t_rows) + 0) * t_width + (ceiling(" + l_TabN + " / t_rows) - 1) * t_offset + 1~" " + & 
               "y1=~"0~tt_theight - ((ceiling(" + l_TabN + " / t_rows) * t_height) * f_direct) + (4 * f_direct)~" " + & 
               "x2=~"0~t(mod(" + l_TabN + " - 1,t_rows) + 0) * t_width + (ceiling(" + l_TabN + " / t_rows) - 1) * t_offset + 4~" " + & 
               "y2=~"0~tt_theight - ((ceiling(" + l_TabN + " / t_rows) * t_height) * f_direct) + (1 * f_direct)~" ) "
      l_TIn  = "create line(name=tab" + l_TabN + "_top_in band=detail background.color=~"12632256~" " + & 
               "visible=~"0~tif(t_tnum < " + l_TabN + " or (pos(t_visible,' " + l_TabS + " ') > 0 and (t_num / t_rows) <= 1), 0, 1)~" " + &
               "pen.width=~"1~" pen.color=~"0~tif(f_style='3D', if(f_side = 'T', 16777215, f_shade), if(t_current = " + l_TabN + ", c_bcolor, t_bcolor))~" " + & 
               "x1=~"0~t(mod(" + l_TabN + " - 1,t_rows) + 0) * t_width + (ceiling(" + l_TabN + " / t_rows) - 1) * t_offset + 4~" " + & 
               "y1=~"0~tt_theight - ((ceiling(" + l_TabN + " / t_rows) * t_height) * f_direct) + (1 * f_direct)~" " + & 
               "x2=~"0~t(mod(" + l_TabN + " - 1,t_rows) + 1) * t_width + (ceiling(" + l_TabN + " / t_rows) - 1) * t_offset - 3~" " + & 
               "y2=~"0~tt_theight - ((ceiling(" + l_TabN + " / t_rows) * t_height) * f_direct) + (1 * f_direct)~" ) "
      l_RIn  = "create line(name=tab" + l_TabN + "_rightc_in band=detail background.color=~"12632256~" " + & 
               "visible=~"0~tif(t_tnum < " + l_TabN + " or (pos(t_visible,' " + l_TabS + " ') > 0 and (t_num / t_rows) <= 1), 0, 1)~" " + &
               "pen.width=~"1~" pen.color=~"0~tif(f_style='3D', f_shade, if(t_current = " + l_TabN + ", c_bcolor, t_bcolor))~" " + & 
               "x1=~"0~t(mod(" + l_TabN + " - 1,t_rows) + 1) * t_width + (ceiling(" + l_TabN + " / t_rows) - 1) * t_offset - 3~" " + & 
               "y1=~"0~tt_theight - ((ceiling(" + l_TabN + " / t_rows) * t_height) * f_direct) + (2 * f_direct)~" " + & 
               "x2=~"0~t(mod(" + l_TabN + " - 1,t_rows) + 1) * t_width + (ceiling(" + l_TabN + " / t_rows) - 1) * t_offset - 1~" " + & 
               "y2=~"0~tt_theight - ((ceiling(" + l_TabN + " / t_rows) * t_height) * f_direct) + (4 * f_direct)~" ) " + &
               "create line(name=tab" + l_TabN + "_right_in band=detail background.color=~"12632256~" " + & 
               "visible=~"0~tif(t_tnum < " + l_TabN + " or (pos(t_visible,' " + l_TabS + " ') > 0 and (t_num / t_rows) <= 1), 0, 1)~" " + &
               "pen.width=~"1~" pen.color=~"0~tif(f_style='3D', f_shade, if(t_current = " + l_TabN + ", c_bcolor, t_bcolor))~" " + & 
               "x1=~"0~t(mod(" + l_TabN + " - 1,t_rows) + 1) * t_width + (ceiling(" + l_TabN + " / t_rows) - 1) * t_offset - 1~" " + & 
               "y1=~"0~tt_theight - ((ceiling(" + l_TabN + " / t_rows) * t_height) * f_direct) + (4 * f_direct)~" " + & 
               "x2=~"0~t(mod(" + l_TabN + " - 1,t_rows) + 1) * t_width + (ceiling(" + l_TabN + " / t_rows) - 1) * t_offset - 1~" " + & 
               "y2=~"0~tt_theight - ((ceiling(" + l_TabN + " / t_rows) - 1) * t_height) * f_direct~" )"

      Modify(l_Rect + l_Text + l_Fill + l_LOut + l_TOut + l_ROut + l_LIn + l_TIn + l_RIn)
   NEXT
END IF

//------------------------------------------------------------------
//  Determine if more than two rows are needed.
//------------------------------------------------------------------

l_NumRows = Ceiling(i_Tabs / tabs_per_row)
IF l_NumRows > 3 THEN
   FOR l_Idx = 4 TO l_NumRows
      l_RowN = String(l_Idx)
      Modify("create line(name=row" + l_RowN + "_right_out band=detail background.color=~"12632256~" " + & 
             "pen.width=~"1~" pen.color=~"0~" " + &
             "visible=~"0~tif(ceiling(t_num / t_rows) > (" + l_RowN + " - 1) and f_border = 'Y', 1, 0)~" " + & 
             "x1=~"0~tt_rows * t_width + (" + l_RowN + " - 1) * t_offset~" " + & 
             "y1=~"0~tif(f_side = 'T', t_theight - ((" + l_RowN + " - 1) * t_height), t_theight + ((" + l_RowN + " - 1) * t_height))~" " + & 
             "x2=~"0~tt_rows * t_width + (" + l_RowN + " - 1) * t_offset~" " + & 
             "y2=~"0~tif(f_side = 'T', f_height - (" + l_RowN + " - 1) * t_offset, (" + l_RowN + " - 1) * t_offset)~" ) " + &
             "create line(name=row" + l_RowN + "_right_in band=detail background.color=~"12632256~" " + & 
             "pen.width=~"1~" pen.color=~"0~tif(f_style='3D', f_shade, f_color)~" " + & 
             "visible=~"0~tif(ceiling(t_num / t_rows) > (" + l_RowN + " - 1) and f_border = 'Y', 1, 0)~" " + & 
             "x1=~"0~tt_rows * t_width + (" + l_RowN + " - 1) * t_offset - 1~" " + & 
             "y1=~"0~tif(f_side = 'T', t_theight - ((" + l_RowN + " - 1) * t_height), t_theight + ((" + l_RowN + " - 1) * t_height))~" " + & 
             "x2=~"0~tt_rows * t_width + (" + l_RowN + " - 1) * t_offset - 1~" " + & 
             "y2=~"0~tif(f_side = 'T', f_height - (" + l_RowN + " - 1) * t_offset - 1, (" + l_RowN + " - 1) * t_offset + 1)~" ) " + &
             "create line(name=row" + l_RowN + "_bottom_out band=detail background.color=~"12632256~" " + & 
             "pen.width=~"1~" pen.color=~"0~" " + & 
             "visible=~"0~tif(ceiling(t_num / t_rows) > (" + l_RowN + " - 1) and f_border = 'Y', 1, 0)~" " + & 
             "x1=~"0~tf_width - ((ceiling(t_num / t_rows) - (" + l_RowN + " - 1)) * t_offset) + 1~" " + & 
             "y1=~"0~tif(f_side = 'T', f_height - (" + l_RowN + " - 1) * t_offset, (" + l_RowN + " -1) * t_offset)~" " + & 
             "x2=~"0~tf_width - (ceiling(t_num / t_rows) - (" + l_RowN + " - 0)) * t_offset~" " + & 
             "y2=~"0~tif(f_side = 'T', f_height - (" + l_RowN + " - 1) * t_offset, (" + l_RowN + " - 1) * t_offset)~" ) " + &
             "create line(name=row" + l_RowN + "_bottom_in band=detail background.color=~"12632256~" " + & 
             "pen.width=~"1~" pen.color=~"0~tif(f_style='3D', if(f_side = 'T', f_shade, 16777215), f_color)~" " + & 
             "visible=~"0~tif(ceiling(t_num / t_rows) > (" + l_RowN + " - 1) and f_border = 'Y', 1, 0)~" " + & 
             "x1=~"0~tf_width - ((ceiling(t_num / t_rows) - (" + l_RowN + " - 1)) * t_offset) + 1~" " + & 
             "y1=~"0~tif(f_side = 'T', f_height - ((" + l_RowN + " - 1) * t_offset) - 1, ((" + l_RowN + " - 1) * t_offset) + 1)~" " + & 
             "x2=~"0~tf_width - (ceiling(t_num / t_rows) - (" + l_RowN + " - 0)) * t_offset~" " + & 
             "y2=~"0~tif(f_side = 'T', f_height - ((" + l_RowN + " - 1) * t_offset) - 1, ((" + l_RowN + " - 1) * t_offset) + 1)~" ) " + &
             "create line(name=row" + l_RowN + "_top_out band=detail background.color=~"12632256~" " + & 
             "pen.width=~"1~" pen.color=~"0~" " + & 
             "visible=~"0~tif(ceiling(t_num / t_rows) > (" + l_RowN + " - 1) and f_border = 'N', 1, 0)~" " + & 
             "x1=~"0~tf_width - ((ceiling(t_num / t_rows) - (" + l_RowN + " - 1)) * t_offset) - 4~" " + & 
             "y1=~"0~tt_theight - ((" + l_RowN + " - 1) * t_height) * f_direct~" " + & 
             "x2=~"0~tf_width - (ceiling(t_num / t_rows) - (" + l_RowN + " - 0)) * t_offset + 1~" " + & 
             "y2=~"0~tt_theight - ((" + l_RowN + " - 1) * t_height) * f_direct~" ) " + &
             "create rectangle(name=row" + l_RowN + "_folder band=detail " + &
             "visible=~"0~tif(ceiling(t_num / t_rows) > (" + l_RowN + " - 1) and f_border = 'Y', 1, 0)~" " + & 
             "pen.style=~"0~" pen.width=~"1~" " + & 
             "pen.color=~"0~tf_color~" " + & 
             "brush.hatch=~"6~" brush.color=~"0~tf_color~" " + & 
             "background.mode=~"2~" background.color=~"0~tf_color~" " + & 
             "x=~"0~tf_width - ((ceiling(t_num / t_rows) - (" + l_RowN + " - 1)) * t_offset) + 1~" " + & 
             "y=~"0~tif(f_side = 'T', t_theight - (" + l_RowN + " * t_height) + t_height, ((" + l_RowN + " - 1) * t_offset) + 2)~" " + & 
             "height=~"0~tif(f_side = 'T', f_height - t_theight + ((" + l_RowN + " - 1) * t_height) - ((" + l_RowN + " - 1) * t_offset) - 1, t_theight + ((" + l_RowN + " - 1) * t_height) - ((" + l_RowN + " - 1) * t_offset) - 1)~" " + & 
             "width=~"0~tt_offset - 2~" ) ")
   NEXT
END IF

RETURN
end subroutine

public subroutine fu_foldercreatelr (integer tabs_per_row);//******************************************************************
//  PO Module     : u_Folder
//  Subroutine    : fu_FolderCreateLR
//  Description   : Creates extra tabs and rows for the folder when
//                  tabs are on the left or right.
//
//  Parameters    : INTEGER Tabs_Per_Row - number of tabs per row
//
//  Return Value  : (None)
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

STRING     l_TabN, l_TabS, l_Text, l_Fill, l_ROut, l_TOut, l_TIn
STRING     l_RIn, l_LOut, l_LIn, l_RowN, l_Rect
INTEGER    l_Idx, l_NumRows, l_TotalTabs

//------------------------------------------------------------------
//  Determine if more than 12 tabs are needed.
//------------------------------------------------------------------

l_TotalTabs = GetItemNumber(1, "t_tnum")
IF l_TotalTabs > 12 THEN
   FOR l_Idx = 13 to l_TotalTabs
      l_TabN = String(l_Idx)
      l_TabS = String(l_Idx, "00")
      l_Rect = "create rectangle(name=tab" + l_TabN + "_rect band=detail pen.style=~"0~" " + & 
               "visible=~"0~tif(t_tnum < " + l_TabN + " or (pos(t_visible,' " + l_TabS + " ') > 0 and (t_num / t_rows) <= 1), 0, 1)~" " + &
               "pen.width=~"1~" pen.color=~"0~tif(t_current = " + l_TabN + ", c_bcolor, if(mid(t_pos,(" + l_TabN + " - 1) * 8 + 8, 1) = 'Y', d_bcolor, t_bcolor))~" " + &  
               "brush.hatch=~"6~" brush.color=~"0~tif(t_current = " + l_TabN + ", c_bcolor, if(mid(t_pos,(" + l_TabN + " - 1) * 8 + 8, 1) = 'Y', d_bcolor, t_bcolor))~" " + &  
               "background.mode=~"2~" background.color=~"0~tif(t_current = " + l_TabN + ", c_bcolor, if(mid(t_pos,(" + l_TabN + " - 1) * 8 + 8, 1) = 'Y', d_bcolor, t_bcolor))~" " + & 
               "y=~"0~t(mod(" + l_TabN + " - 1,t_rows) + 0) * t_width + (ceiling(" + l_TabN + " / t_rows) - 1) * t_offset + 2~" " + &  
               "x=~"0~tif(f_side = 'L', t_theight - (ceiling(" + l_TabN + " / t_rows) * t_height) + 4, if(ceiling(" + l_TabN + " / t_rows) > 1, t_theight + ((ceiling(" + l_TabN + " / t_rows) - 1) * t_height) + 1, if(t_current = " + l_TabN + ", if(f_border = 'Y' and f_style = '3D', t_theight + ((ceiling(" + l_TabN + " / t_rows) - 1) * t_height) - 2, t_theight + ((ceiling(" + l_TabN + " / t_rows) - 1) * t_height)), t_theight + ((ceiling(" + l_TabN + " / t_rows) - 1) * t_height) + 1)))~" " + &  
               "width=~"0~tif(ceiling(" + l_TabN + " / t_rows) > 1, t_height - 4, if(t_current = " + l_TabN + ", if(f_border = 'Y' and f_style = '3D', t_height - 1, t_height - 3), t_height - 4))~" " + &  
               "height=~"0~tt_width - 2~" ) "
      l_Text = "create text(name=tab" + l_TabN + "_text band=detail text=~" ~tmid(t_label,integer(mid(t_pos, (" + l_TabN + " - 1) * 8 + 4, 2)), integer(mid(t_pos, (" + l_TabN + " - 1) * 8 + 6, 2)) )~" " + & 
               "visible=~"0~tif(t_tnum < " + l_TabN + " or (pos(t_visible,' " + l_TabS + " ') > 0 and (t_num / t_rows) <= 1), 0, 1)~" " + & 
               "border=~"0~" alignment=~"2~tt_align~" " + &  
               "background.color=~"0~tif(t_current = " + l_TabN + ", c_bcolor, if(mid(t_pos,(" + l_TabN + " - 1) * 8 + 8, 1) = 'Y', d_bcolor, t_bcolor))~" " + & 
               "color=~"0~tif(t_current = " + l_TabN + ", c_color, if(mid(t_pos,(" + l_TabN + " - 1) * 8 + 8, 1) = 'Y', d_color, t_color))~" " + &  
               "font.escapement=~"0~tif(t_rotate = 'N', if(f_side = 'L', 900, 2700), 0)~" " + & 
               "font.height=~"0~tif(t_current = " + l_TabN + ", c_size * -1, if(mid(t_pos,(" + l_TabN + " - 1) * 8 + 8, 1) = 'Y', d_size * -1, t_size * -1))~" " + &  
               "font.face=~"Arial~tif(t_current = " + l_TabN + ", c_font, if(mid(t_pos,(" + l_TabN + " - 1) * 8 + 8, 1) = 'Y', d_font, t_font))~" " + & 
               "font.family=~"2~tif(t_current = " + l_TabN + ", c_family, if(mid(t_pos,(" + l_TabN + " - 1) * 8 + 8, 1) = 'Y', d_family, t_family))~" " + &  
               "font.pitch=~"2~tif(t_current = " + l_TabN + ", c_pitch, if(mid(t_pos,(" + l_TabN + " - 1) * 8 + 8, 1) = 'Y', d_pitch, t_pitch))~" " + &  
               "font.charset=~"0~tif(t_current = " + l_TabN + ", c_charset, if(mid(t_pos,(" + l_TabN + " - 1) * 8 + 8, 1) = 'Y', d_charset, t_charset))~" " + &   
               "font.weight=~"0~tif(t_current = " + l_TabN + ", if(c_style='bold', 700, 400), if(mid(t_pos,(" + l_TabN + " - 1) * 8 + 8, 1) = 'Y', if(d_style='bold', 700, 400), if(t_style='bold', 700, 400)))~" " + &  
               "font.italic=~"0~tif(t_current = " + l_TabN + ", if(c_style='italic', 1, 0), if(mid(t_pos,(" + l_TabN + " - 1) * 8 + 8, 1) = 'Y', if(d_style='italic', 1, 0), if(t_style='italic', 1, 0)))~" " + &  
               "font.underline=~"0~tif(t_current = " + l_TabN + ", if(c_style='underline', 1, 0), if(mid(t_pos,(" + l_TabN + " - 1) * 8 + 8, 1) = 'Y', if(d_style='underline', 1, 0), if(t_style='underline', 1, 0)))~" " + &  
               "y=~"0~t(mod(" + l_TabN + " - 1,t_rows) + 0) * t_width + (ceiling(" + l_TabN + " / t_rows) - 1) * t_offset + t_yrotate~" " + &  
               "x=~"0~tif(f_side = 'L', t_theight - (ceiling(" + l_TabN + " / t_rows) * t_height) + t_xrotate, t_theight + ((ceiling(" + l_TabN + " / t_rows) - 1) * t_height) + t_xrotate)~" " + &  
               "height=~"0~tif(t_rotate = 'N', 1, t_size + 4)~" " + &  
               "width=~"0~tif(t_rotate = 'N', t_width - 2, t_height - 4)~" ) "
      l_Fill = "create line(name=tab" + l_TabN + "_fill1 band=detail background.color=~"12632256~" " + &  
               "visible=~"0~tif(t_tnum < " + l_TabN + " or (pos(t_visible,' " + l_TabS + " ') > 0 and (t_num / t_rows) <= 1), 0, 1)~" " + & 
               "pen.width=~"1~" pen.color=~"0~tif(t_current = " + l_TabN + ", c_bcolor, t_bcolor)~" " + &  
               "y1=~"0~t(mod(" + l_TabN + " - 1,t_rows) + 0) * t_width + (ceiling(" + l_TabN + " / t_rows) - 1) * t_offset + 3~" " + &  
               "x1=~"0~tt_theight - ((ceiling(" + l_TabN + " / t_rows) * t_height) * f_direct) + (2 * f_direct)~" " + &  
               "y2=~"0~t(mod(" + l_TabN + " - 1,t_rows) + 1) * t_width + (ceiling(" + l_TabN + " / t_rows) - 1) * t_offset - 3~" " + &  
               "x2=~"0~tt_theight - ((ceiling(" + l_TabN + " / t_rows) * t_height) * f_direct) + (2 * f_direct)~" ) " + & 
               "create line(name=tab" + l_TabN + "_fill2 band=detail background.color=~"12632256~" " + &  
               "visible=~"0~tif(t_tnum < " + l_TabN + " or (pos(t_visible,' " + l_TabS + " ') > 0 and (t_num / t_rows) <= 1), 0, 1)~" " + & 
               "pen.width=~"1~" pen.color=~"0~tif(t_current = " + l_TabN + ", c_bcolor, t_bcolor)~" " + &  
               "y1=~"0~t(mod(" + l_TabN + " - 1,t_rows) + 0) * t_width + (ceiling(" + l_TabN + " / t_rows) - 1) * t_offset + 2~" " + &  
               "x1=~"0~tt_theight - ((ceiling(" + l_TabN + " / t_rows) * t_height) * f_direct) + (3 * f_direct)~" " + &  
               "y2=~"0~t(mod(" + l_TabN + " - 1,t_rows) + 1) * t_width + (ceiling(" + l_TabN + " / t_rows) - 1) * t_offset - 2~" " + &  
               "x2=~"0~tt_theight - ((ceiling(" + l_TabN + " / t_rows) * t_height) * f_direct) + (3 * f_direct)~" ) "
      l_LOut = "create line(name=tab" + l_TabN + "_left_out band=detail background.color=~"12632256~" " + &  
               "visible=~"0~tif(t_tnum < " + l_TabN + " or (pos(t_visible,' " + l_TabS + " ') > 0 and (t_num / t_rows) <= 1), 0, 1)~" " + & 
               "pen.width=~"1~" pen.color=~"0~" " + &  
               "y1=~"0~t(mod(" + l_TabN + " - 1,t_rows) + 0) * t_width + (ceiling(" + l_TabN + " / t_rows) - 1) * t_offset~" " + &  
               "x1=~"0~tt_theight - ((ceiling(" + l_TabN + " / t_rows) * t_height) * f_direct) + (4 * f_direct)~" " + &  
               "y2=~"0~t(mod(" + l_TabN + " - 1,t_rows) + 0) * t_width + (ceiling(" + l_TabN + " / t_rows) - 1) * t_offset~" " + &  
               "x2=~"0~tt_theight - ((ceiling(" + l_TabN + " / t_rows) - 1) * t_height) * f_direct~" ) " + & 
               "create line(name=tab" + l_TabN + "_leftc_out band=detail background.color=~"12632256~" " + &  
               "visible=~"0~tif(t_tnum < " + l_TabN + " or (pos(t_visible,' " + l_TabS + " ') > 0 and (t_num / t_rows) <= 1), 0, 1)~" " + & 
               "pen.width=~"1~" pen.color=~"0~" " + &  
               "y1=~"0~t(mod(" + l_TabN + " - 1,t_rows) + 0) * t_width + (ceiling(" + l_TabN + " / t_rows) - 1) * t_offset~" " + &  
               "x1=~"0~tt_theight - ((ceiling(" + l_TabN + " / t_rows) * t_height) * f_direct) + (4 * f_direct)~" " + &  
               "y2=~"0~t(mod(" + l_TabN + " - 1,t_rows) + 0) * t_width + (ceiling(" + l_TabN + " / t_rows) - 1) * t_offset + 4~" " + &  
               "x2=~"0~tt_theight - (ceiling(" + l_TabN + " / t_rows) * t_height) * f_direct~" ) "
      l_TOut = "create line(name=tab" + l_TabN + "_top_out band=detail background.color=~"12632256~" " + &  
               "visible=~"0~tif(t_tnum < " + l_TabN + " or (pos(t_visible,' " + l_TabS + " ') > 0 and (t_num / t_rows) <= 1), 0, 1)~" " + & 
               "pen.width=~"1~" pen.color=~"0~" " + &  
               "y1=~"0~t(mod(" + l_TabN + " - 1,t_rows) + 0) * t_width + (ceiling(" + l_TabN + " / t_rows) - 1) * t_offset + 4~" " + &  
               "x1=~"0~tt_theight - (ceiling(" + l_TabN + " / t_rows) * t_height) * f_direct~" " + &  
               "y2=~"0~t(mod(" + l_TabN + " - 1,t_rows) + 1) * t_width + (ceiling(" + l_TabN + " / t_rows) - 1) * t_offset - 3~" " + &  
               "x2=~"0~tt_theight - (ceiling(" + l_TabN + " / t_rows) * t_height) * f_direct~" ) "
      l_ROut = "create line(name=tab" + l_TabN + "_rightc_out band=detail background.color=~"12632256~" " + &  
               "visible=~"0~tif(t_tnum < " + l_TabN + " or (pos(t_visible,' " + l_TabS + " ') > 0 and (t_num / t_rows) <= 1), 0, 1)~" " + & 
               "pen.width=~"1~" pen.color=~"0~" " + &  
               "y1=~"0~t(mod(" + l_TabN + " - 1,t_rows) + 1) * t_width + (ceiling(" + l_TabN + " / t_rows) - 1) * t_offset - 4~" " + &  
               "x1=~"0~tt_theight - (ceiling(" + l_TabN + " / t_rows) * t_height) * f_direct~" " + &  
               "y2=~"0~t(mod(" + l_TabN + " - 1,t_rows) + 1) * t_width + (ceiling(" + l_TabN + " / t_rows) - 1) * t_offset~" " + &  
               "x2=~"0~tt_theight - ((ceiling(" + l_TabN + " / t_rows) * t_height) * f_direct) + (4 * f_direct)~" ) " + & 
               "create line(name=tab" + l_TabN + "_right_out band=detail background.color=~"12632256~" " + &  
               "visible=~"0~tif(t_tnum < " + l_TabN + " or (pos(t_visible,' " + l_TabS + " ') > 0 and (t_num / t_rows) <= 1), 0, 1)~" " + & 
               "pen.width=~"1~" pen.color=~"0~" " + &  
               "y1=~"0~t(mod(" + l_TabN + " - 1,t_rows) + 1) * t_width + (ceiling(" + l_TabN + " / t_rows) - 1) * t_offset~" " + &  
               "x1=~"0~tt_theight - ((ceiling(" + l_TabN + " / t_rows) * t_height) * f_direct) + (4 * f_direct)~" " + &  
               "y2=~"0~t(mod(" + l_TabN + " - 1,t_rows) + 1) * t_width + (ceiling(" + l_TabN + " / t_rows) - 1) * t_offset~" " + &  
               "x2=~"0~tt_theight - ((ceiling(" + l_TabN + " / t_rows) - 1) * t_height) * f_direct~" ) "
      l_LIn  = "create line(name=tab" + l_TabN + "_left_in band=detail background.color=~"12632256~" " + &  
               "visible=~"0~tif(t_tnum < " + l_TabN + " or (pos(t_visible,' " + l_TabS + " ') > 0 and (t_num / t_rows) <= 1), 0, 1)~" " + & 
               "pen.width=~"1~" pen.color=~"0~tif(f_style='3D', 16777215, if(t_current = " + l_TabN + ", c_bcolor, t_bcolor))~" " + &  
               "y1=~"0~t(mod(" + l_TabN + " - 1,t_rows) + 0) * t_width + (ceiling(" + l_TabN + " / t_rows) - 1) * t_offset + 1~" " + &  
               "x1=~"0~tt_theight - ((ceiling(" + l_TabN + " / t_rows) * t_height) * f_direct) + (4 * f_direct)~" " + &  
               "y2=~"0~t(mod(" + l_TabN + " - 1,t_rows) + 0) * t_width + (ceiling(" + l_TabN + " / t_rows) - 1) * t_offset + 1~" " + &  
               "x2=~"0~tt_theight - ((ceiling(" + l_TabN + " / t_rows) - 1) * t_height) * f_direct~" ) " + & 
               "create line(name=tab" + l_TabN + "_leftc_in band=detail background.color=~"12632256~" " + &  
               "visible=~"0~tif(t_tnum < " + l_TabN + " or (pos(t_visible,' " + l_TabS + " ') > 0 and (t_num / t_rows) <= 1), 0, 1)~" " + & 
               "pen.width=~"1~" pen.color=~"0~tif(f_style='3D', 16777215, if(t_current = " + l_TabN + ", c_bcolor, t_bcolor))~" " + &  
               "y1=~"0~t(mod(" + l_TabN + " - 1,t_rows) + 0) * t_width + (ceiling(" + l_TabN + " / t_rows) - 1) * t_offset + 1~" " + &  
               "x1=~"0~tt_theight - ((ceiling(" + l_TabN + " / t_rows) * t_height) * f_direct) + (4 * f_direct)~" " + &  
               "y2=~"0~t(mod(" + l_TabN + " - 1,t_rows) + 0) * t_width + (ceiling(" + l_TabN + " / t_rows) - 1) * t_offset + 4~" " + &  
               "x2=~"0~tt_theight - ((ceiling(" + l_TabN + " / t_rows) * t_height) * f_direct) + (1 * f_direct)~" ) "
      l_TIn  = "create line(name=tab" + l_TabN + "_top_in band=detail background.color=~"12632256~" " + &  
               "visible=~"0~tif(t_tnum < " + l_TabN + " or (pos(t_visible,' " + l_TabS + " ') > 0 and (t_num / t_rows) <= 1), 0, 1)~" " + & 
               "pen.width=~"1~" pen.color=~"0~tif(f_style='3D', if(f_side = 'L', 16777215, f_shade), if(t_current = " + l_TabN + ", c_bcolor, t_bcolor))~" " + &  
               "y1=~"0~t(mod(" + l_TabN + " - 1,t_rows) + 0) * t_width + (ceiling(" + l_TabN + " / t_rows) - 1) * t_offset + 4~" " + &  
               "x1=~"0~tt_theight - ((ceiling(" + l_TabN + " / t_rows) * t_height) * f_direct) + (1 * f_direct)~" " + &  
               "y2=~"0~t(mod(" + l_TabN + " - 1,t_rows) + 1) * t_width + (ceiling(" + l_TabN + " / t_rows) - 1) * t_offset - 3~" " + &  
               "x2=~"0~tt_theight - ((ceiling(" + l_TabN + " / t_rows) * t_height) * f_direct) + (1 * f_direct)~" ) "
      l_RIn  = "create line(name=tab" + l_TabN + "_rightc_in band=detail background.color=~"12632256~" " + &  
               "visible=~"0~tif(t_tnum < " + l_TabN + " or (pos(t_visible,' " + l_TabS + " ') > 0 and (t_num / t_rows) <= 1), 0, 1)~" " + & 
               "pen.width=~"1~" pen.color=~"0~tif(f_style='3D', f_shade, if(t_current = " + l_TabN + ", c_bcolor, t_bcolor))~" " + &  
               "y1=~"0~t(mod(" + l_TabN + " - 1,t_rows) + 1) * t_width + (ceiling(" + l_TabN + " / t_rows) - 1) * t_offset - 3~" " + &  
               "x1=~"0~tt_theight - ((ceiling(" + l_TabN + " / t_rows) * t_height) * f_direct) + (2 * f_direct)~" " + &  
               "y2=~"0~t(mod(" + l_TabN + " - 1,t_rows) + 1) * t_width + (ceiling(" + l_TabN + " / t_rows) - 1) * t_offset - 1~" " + &  
               "x2=~"0~tt_theight - ((ceiling(" + l_TabN + " / t_rows) * t_height) * f_direct) + (4 * f_direct)~" ) " + & 
               "create line(name=tab" + l_TabN + "_right_in band=detail background.color=~"12632256~" " + &  
               "visible=~"0~tif(t_tnum < " + l_TabN + " or (pos(t_visible,' " + l_TabS + " ') > 0 and (t_num / t_rows) <= 1), 0, 1)~" " + & 
               "pen.width=~"1~" pen.color=~"0~tif(f_style='3D', f_shade, if(t_current = " + l_TabN + ", c_bcolor, t_bcolor))~" " + &  
               "y1=~"0~t(mod(" + l_TabN + " - 1,t_rows) + 1) * t_width + (ceiling(" + l_TabN + " / t_rows) - 1) * t_offset - 1~" " + &  
               "x1=~"0~tt_theight - ((ceiling(" + l_TabN + " / t_rows) * t_height) * f_direct) + (4 * f_direct)~" " + &  
               "y2=~"0~t(mod(" + l_TabN + " - 1,t_rows) + 1) * t_width + (ceiling(" + l_TabN + " / t_rows) - 1) * t_offset - 1~" " + &  
               "x2=~"0~tt_theight - ((ceiling(" + l_TabN + " / t_rows) - 1) * t_height) * f_direct~" ) "

      Modify(l_Rect + l_Text + l_Fill + l_LOut + l_TOut + l_ROut + l_LIn + l_TIn + l_RIn)
   NEXT
END IF

//------------------------------------------------------------------
//  Determine if more than two rows are needed.
//------------------------------------------------------------------

l_NumRows = Ceiling(i_Tabs / tabs_per_row)
IF l_NumRows > 3 THEN
   FOR l_Idx = 4 TO l_NumRows
      l_RowN = String(l_Idx)
      Modify("create line(name=row" + l_RowN + "_right_out band=detail background.color=~"12632256~" " + & 
             "pen.width=~"1~" pen.color=~"0~" " + & 
             "visible=~"0~tif(ceiling(t_num / t_rows) > (" + l_RowN + " - 1) and f_border = 'Y', 1, 0)~" " + &  
             "y1=~"0~tt_rows * t_width + (" + l_RowN + " - 1) * t_offset~" " + &  
             "x1=~"0~tif(f_side = 'L', t_theight - ((" + l_RowN + " - 1) * t_height), t_theight + ((" + l_RowN + " - 1) * t_height))~" " + &  
             "y2=~"0~tt_rows * t_width + (" + l_RowN + " - 1) * t_offset~" " + &  
             "x2=~"0~tif(f_side = 'L', f_height - (" + l_RowN + " - 1) * t_offset, (" + l_RowN + " - 1) * t_offset)~" ) " + & 
             "create line(name=row" + l_RowN + "_right_in band=detail background.color=~"12632256~" " + & 
             "pen.width=~"1~" pen.color=~"0~tif(f_style='3D', f_shade, f_color)~" " + &  
             "visible=~"0~tif(ceiling(t_num / t_rows) > (" + l_RowN + " - 1) and f_border = 'Y', 1, 0)~" " + &  
             "y1=~"0~tt_rows * t_width + (" + l_RowN + " - 1) * t_offset - 1~" " + &  
             "x1=~"0~tif(f_side = 'L', t_theight - ((" + l_RowN + " - 1) * t_height), t_theight + ((" + l_RowN + " - 1) * t_height))~" " + &  
             "y2=~"0~tt_rows * t_width + (" + l_RowN + " - 1) * t_offset - 1~" " + &  
             "x2=~"0~tif(f_side = 'L', f_height - (" + l_RowN + " - 1) * t_offset - 1, (" + l_RowN + " - 1) * t_offset + 1)~" ) " + & 
             "create line(name=row" + l_RowN + "_bottom_out band=detail background.color=~"12632256~" " + &  
             "pen.width=~"1~" pen.color=~"0~" " + &  
             "visible=~"0~tif(ceiling(t_num / t_rows) > (" + l_RowN + " - 1) and f_border = 'Y', 1, 0)~" " + &  
             "y1=~"0~tf_width - ((ceiling(t_num / t_rows) - (" + l_RowN + " - 1)) * t_offset) + 1~" " + &  
             "x1=~"0~tif(f_side = 'L', f_height - (" + l_RowN + " - 1) * t_offset, (" + l_RowN + " -1) * t_offset)~" " + &  
             "y2=~"0~tf_width - (ceiling(t_num / t_rows) - (" + l_RowN + " - 0)) * t_offset~" " + &  
             "x2=~"0~tif(f_side = 'L', f_height - (" + l_RowN + " - 1) * t_offset, (" + l_RowN + " - 1) * t_offset)~" ) " + & 
             "create line(name=row" + l_RowN + "_bottom_in band=detail background.color=~"12632256~" " + &  
             "pen.width=~"1~" pen.color=~"0~tif(f_style='3D', if(f_side = 'L', f_shade, 16777215), f_color)~" " + &  
             "visible=~"0~tif(ceiling(t_num / t_rows) > (" + l_RowN + " - 1) and f_border = 'Y', 1, 0)~" " + &  
             "y1=~"0~tf_width - ((ceiling(t_num / t_rows) - (" + l_RowN + " - 1)) * t_offset) + 1~" " + &  
             "x1=~"0~tif(f_side = 'L', f_height - ((" + l_RowN + " - 1) * t_offset) - 1, ((" + l_RowN + " - 1) * t_offset) + 1)~" " + &  
             "y2=~"0~tf_width - (ceiling(t_num / t_rows) - (" + l_RowN + " - 0)) * t_offset~" " + &  
             "x2=~"0~tif(f_side = 'L', f_height - ((" + l_RowN + " - 1) * t_offset) - 1, ((" + l_RowN + " - 1) * t_offset) + 1)~" ) " + & 
             "create line(name=row" + l_RowN + "_top_out band=detail background.color=~"12632256~" " + &  
             "pen.width=~"1~" pen.color=~"0~" " + &  
             "visible=~"0~tif(ceiling(t_num / t_rows) > (" + l_RowN + " - 1) and f_border = 'N', 1, 0)~" " + &  
             "y1=~"0~tf_width - ((ceiling(t_num / t_rows) - (" + l_RowN + " - 1)) * t_offset) - 4~" " + &  
             "x1=~"0~tt_theight - ((" + l_RowN + " - 1) * t_height) * f_direct~" " + &  
             "y2=~"0~tf_width - (ceiling(t_num / t_rows) - (" + l_RowN + " - 0)) * t_offset + 1~" " + &  
             "x2=~"0~tt_theight - ((" + l_RowN + " - 1) * t_height) * f_direct~" ) " + & 
             "create rectangle(name=row" + l_RowN + "_folder band=detail " + & 
             "visible=~"0~tif(ceiling(t_num / t_rows) > (" + l_RowN + " - 1) and f_border = 'Y', 1, 0)~" " + &  
             "pen.style=~"0~" pen.width=~"1~" " + &  
             "pen.color=~"0~tf_color~" " + &  
             "brush.hatch=~"6~" brush.color=~"0~tf_color~" " + &  
             "background.mode=~"2~" background.color=~"0~tf_color~" " + &  
             "y=~"0~tf_width - ((ceiling(t_num / t_rows) - (" + l_RowN + " - 1)) * t_offset) + 1~" " + &  
             "x=~"0~tif(f_side = 'L', t_theight - (" + l_RowN + " * t_height) + t_height, ((" + l_RowN + " - 1) * t_offset) + 2)~" " + &  
             "width=~"0~tif(f_side = 'L', f_height - t_theight + ((" + l_RowN + " - 1) * t_height) - ((" + l_RowN + " - 1) * t_offset) - 1, t_theight + ((" + l_RowN + " - 1) * t_height) - ((" + l_RowN + " - 1) * t_offset) - 1)~" " + &  
             "height=~"0~tt_offset - 2~" ) ")
   NEXT
END IF

RETURN
end subroutine

public function dragobject fu_opentab (integer tab, string uo_name, window window_name);//******************************************************************
//  PO Module     : u_Folder
//  Subroutine    : fu_OpenTab
//  Description   : Opens a user object for the given tab.  
//
//  Parameters    : INTEGER Tab      - tab number to attach the 
//                                     user object to.
//                  STRING  UO_Name  - name of the user object to
//                                     open.
//                  WINDOW           - window name the user object
//                                     opens on.     
//
//  Return Value  : USEROBJECT       - the user object that was
//                                     opened.  An error will return
//                                     NULL.
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

DRAGOBJECT l_UserObject, l_NullObject, l_Object[]

//------------------------------------------------------------------
//  If an invalid tab is passed, or the tab is disabled or hidden 
//  then return with no processing.
//------------------------------------------------------------------

SetPointer(HourGlass!)
SetNull(l_NullObject)

IF Tab < 0 OR Tab > i_Tabs THEN
   RETURN l_NullObject
ELSE
   IF i_TabArray[Tab].TabDisable OR i_TabArray[Tab].TabHide THEN
	   RETURN l_NullObject
   END IF
END IF

//------------------------------------------------------------------
//  Open the user object.
//------------------------------------------------------------------

window_name.SetReDraw(FALSE)
IF window_name.OpenUserObject(l_UserObject, uo_name, 1, 1) = -1 THEN
   window_name.SetReDraw(TRUE)
   RETURN l_NullObject
END IF

//------------------------------------------------------------------
//  Assign the user object to the tab.
//------------------------------------------------------------------

l_Object[1] = l_UserObject
fu_AssignTabObjects(tab, l_Object[])

//------------------------------------------------------------------
//  Center the user object.
//------------------------------------------------------------------

fu_CenterTabObject(l_UserObject)

//------------------------------------------------------------------
//  Return the name of the user object.
//------------------------------------------------------------------

window_name.SetReDraw(TRUE)

RETURN l_UserObject
end function

public subroutine fu_unassigntabobject (integer tab, ref windowobject tabobject);//******************************************************************
//  PO Module     : u_Folder
//  Subroutine    : fu_UnassignTabObject
//  Description   : Remove an assignment of a window object from
//                  the given tab.  
//
//  Parameters    : INTEGER      Tab       - tab number
//                  WINDOWOBJECT TabObject - object to unassign 
//
//  Return Value  : (None)
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER l_Idx, l_Jdx

l_Jdx = 0
FOR l_Idx = 1 TO i_TabArray[Tab].TabNumObjects
   IF i_TabArray[Tab].TabObjects[l_Idx] <> TabObject THEN
      l_Jdx = l_Jdx + 1
      i_TabArray[Tab].TabObjects[l_Jdx] = &
         i_TabArray[Tab].TabObjects[l_Idx]
      i_TabArray[Tab].TabObjectCentered[l_Jdx] = &
         i_TabArray[Tab].TabObjectCentered[l_Idx]
   END IF
NEXT
i_TabArray[Tab].TabNumObjects = l_Jdx

end subroutine

public subroutine fu_buildlabels ();//******************************************************************
//  PO Module     : u_Folder
//  Subroutine    : fu_BuildLabels
//  Description   : Creates the string for the tab labels.  
//
//  Parameters    : (None)
//
//  Return Value  : (None)
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

STRING     l_TabPosRow, l_TabPos, l_TabPosTotal, l_TabLabel, l_Name
STRING     l_TmpPos, l_TmpTabPos[], l_HideString, l_Disable
INTEGER    l_Tab, l_Idx, l_NumTabs, l_TotalTabs, l_TabsPerRow
INTEGER    l_SelectedTab, l_SelectedRow

l_TotalTabs    = GetItemNumber(1, "t_tnum")
l_TabsPerRow   = GetItemNumber(1, "t_rows")
l_TabLabel     = ""
l_TabPosRow    = ""
l_TabPosTotal  = ""
i_TabRows      = 0

//------------------------------------------------------------------
//  Build the tab label string.
//------------------------------------------------------------------

FOR l_Idx = 1 TO l_TotalTabs
   l_Disable = "N"
   IF l_Idx <= i_Tabs THEN
      IF NOT i_TabArray[l_Idx].TabHide THEN
         l_Name = i_TabArray[l_Idx].TabName
      ELSE
         l_Name = " "
      END IF
      IF i_TabArray[l_Idx].TabDisable THEN
         l_Disable = "Y"
      END IF
   ELSE
      l_Name = " "
   END IF

   l_TabPos      = "|" + String(l_Idx, "00") + &
                   String(Len(l_TabLabel) + 1, "00") + &
                   String(Len(l_Name), "00") + l_Disable
   l_TabPosRow   = l_TabPosRow + l_TabPos
   l_TabPosTotal = l_TabPosTotal + l_TabPos
   l_TabLabel    = l_TabLabel + l_Name
   l_Tab         = l_Tab + 1
   IF Mod(l_Tab, l_TabsPerRow) = 0 THEN
      i_TabRows = i_TabRows + 1
      i_TabPos[i_TabRows] = l_TabPosRow
      l_TabPosRow = ""
      l_Tab       = 0
   END IF
NEXT

//------------------------------------------------------------------
//  Shuffle the tab rows in the correct order.
//------------------------------------------------------------------

l_SelectedTab = GetItemNumber(1, "t_select")
IF l_SelectedTab > 0 AND i_TabRows > 1 THEN
   l_SelectedRow = Ceiling(Pos(l_TabPosTotal, &
                   "|" + String(l_SelectedTab, "00")) / Len(i_TabPos[1]))
   IF l_SelectedRow > 1 THEN
      l_TmpPos = ""
      FOR l_Idx = 1 TO i_TabRows
         l_TmpPos = l_TmpPos + i_TabPos[l_SelectedRow]
         l_TmpTabPos[l_Idx] = i_TabPos[l_SelectedRow]
         l_SelectedRow = l_SelectedRow + 1
         IF l_SelectedRow > i_TabRows THEN
            l_SelectedRow = 1
         END IF
      NEXT
      i_TabPos[] = l_TmpTabPos[]
      SetItem(1, "t_pos", l_TmpPos)
   ELSE
      SetItem(1, "t_pos", l_TabPosTotal)
   END IF
ELSE
   SetItem(1, "t_pos", l_TabPosTotal)  
END IF

SetItem(1, "t_label", l_TabLabel)

//------------------------------------------------------------------
//  If there is only one row of tabs, hide any filler tabs.
//------------------------------------------------------------------

IF i_TabRows = 1 AND i_Tabs < l_TabsPerRow THEN
   l_HideString = GetItemString(1, "t_visible")
   IF IsNull(l_HideString) THEN
      l_HideString = ""
   END IF

   FOR l_Idx = i_Tabs + 1 TO l_TabsPerRow
      IF Pos(l_HideString, " " + String(l_Idx, "00") + " ") = 0 THEN
         l_HideString = l_HideString + " " + String(l_Idx, "00") + " "
      END IF
   NEXT

   SetItem(1, "t_visible", l_HideString)
END IF

RETURN
end subroutine

on constructor;//******************************************************************
//  PO Module     : u_Folder
//  Event         : Constructor
//  Description   : Initializes the default folder options and 
//                  determines if any of the tabs are initially
//                  created invisible.
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

//------------------------------------------------------------------
//  Identify this object as a PowerObject Folder.  This 
//  identification is used for PowerLock security.
//------------------------------------------------------------------

Tag = "PowerObject Folder"

//------------------------------------------------------------------
//  Make sure the border of the folder control is turned off and
//  the tab order is 0.
//------------------------------------------------------------------

i_FolderResize = FALSE
Border         = FALSE
TabOrder       = 0
i_FolderResize = TRUE

//------------------------------------------------------------------
//  Initialize the folder options.
//------------------------------------------------------------------

SetReDraw(FALSE)

c_FolderTabTop        = w_pomanager_std.c_FolderTabTop
c_FolderTabBottom     = w_pomanager_std.c_FolderTabBottom
c_FolderTabLeft       = w_pomanager_std.c_FolderTabLeft
c_FolderTabRight      = w_pomanager_std.c_FolderTabRight

c_Folder3D            = w_pomanager_std.c_Folder3D
c_Folder2D            = w_pomanager_std.c_Folder2D

c_FolderGray          = w_pomanager_std.c_FolderGray
c_FolderBlack         = w_pomanager_std.c_FolderBlack
c_FolderWhite         = w_pomanager_std.c_FolderWhite
c_FolderDarkGray      = w_pomanager_std.c_FolderDarkGray
c_FolderRed           = w_pomanager_std.c_FolderRed
c_FolderDarkRed       = w_pomanager_std.c_FolderDarkRed
c_FolderGreen         = w_pomanager_std.c_FolderGreen
c_FolderDarkGreen     = w_pomanager_std.c_FolderDarkGreen
c_FolderBlue          = w_pomanager_std.c_FolderBlue
c_FolderDarkBlue      = w_pomanager_std.c_FolderDarkBlue
c_FolderMagenta       = w_pomanager_std.c_FolderMagenta
c_FolderDarkMagenta   = w_pomanager_std.c_FolderDarkMagenta
c_FolderCyan          = w_pomanager_std.c_FolderCyan
c_FolderDarkCyan      = w_pomanager_std.c_FolderDarkCyan
c_FolderYellow        = w_pomanager_std.c_FolderYellow
c_FolderDarkYellow    = w_pomanager_std.c_FolderDarkYellow

c_FolderBGGray        = w_pomanager_std.c_FolderBGGray
c_FolderBGBlack       = w_pomanager_std.c_FolderBGBlack
c_FolderBGWhite       = w_pomanager_std.c_FolderBGWhite
c_FolderBGDarkGray    = w_pomanager_std.c_FolderBGDarkGray
c_FolderBGRed         = w_pomanager_std.c_FolderBGRed
c_FolderBGDarkRed     = w_pomanager_std.c_FolderBGDarkRed
c_FolderBGGreen       = w_pomanager_std.c_FolderBGGreen
c_FolderBGDarkGreen   = w_pomanager_std.c_FolderBGDarkGreen
c_FolderBGBlue        = w_pomanager_std.c_FolderBGBlue
c_FolderBGDarkBlue    = w_pomanager_std.c_FolderBGDarkBlue
c_FolderBGMagenta     = w_pomanager_std.c_FolderBGMagenta
c_FolderBGDarkMagenta = w_pomanager_std.c_FolderBGDarkMagenta
c_FolderBGCyan        = w_pomanager_std.c_FolderBGCyan
c_FolderBGDarkCyan    = w_pomanager_std.c_FolderBGDarkCyan
c_FolderBGYellow      = w_pomanager_std.c_FolderBGYellow
c_FolderBGDarkYellow  = w_pomanager_std.c_FolderBGDarkYellow

c_FolderBorderOn      = w_pomanager_std.c_FolderBorderOn
c_FolderBorderOff     = w_pomanager_std.c_FolderBorderOff

c_FolderResizeOn      = w_pomanager_std.c_FolderResizeOn
c_FolderResizeOff     = w_pomanager_std.c_FolderResizeOff

//------------------------------------------------------------------
//  Initialize the tab options.
//------------------------------------------------------------------

c_TextRegular         = w_pomanager_std.c_TextRegular
c_TextBold            = w_pomanager_std.c_TextBold
c_TextItalic          = w_pomanager_std.c_TextItalic
c_TextUnderline       = w_pomanager_std.c_TextUnderline

c_TextCenter          = w_pomanager_std.c_TextCenter
c_TextLeft            = w_pomanager_std.c_TextLeft
c_TextRight           = w_pomanager_std.c_TextRight

c_TextBlack           = w_pomanager_std.c_TextBlack
c_TextWhite           = w_pomanager_std.c_TextWhite
c_TextGray            = w_pomanager_std.c_TextGray
c_TextDarkGray        = w_pomanager_std.c_TextDarkGray
c_TextRed             = w_pomanager_std.c_TextRed
c_TextDarkRed         = w_pomanager_std.c_TextDarkRed
c_TextGreen           = w_pomanager_std.c_TextGreen
c_TextDarkGreen       = w_pomanager_std.c_TextDarkGreen
c_TextBlue            = w_pomanager_std.c_TextBlue
c_TextDarkBlue        = w_pomanager_std.c_TextDarkBlue
c_TextMagenta         = w_pomanager_std.c_TextMagenta
c_TextDarkMagenta     = w_pomanager_std.c_TextDarkMagenta
c_TextCyan            = w_pomanager_std.c_TextCyan
c_TextDarkCyan        = w_pomanager_std.c_TextDarkCyan
c_TextYellow          = w_pomanager_std.c_TextYellow
c_TextDarkYellow      = w_pomanager_std.c_TextDarkYellow

c_TextRotationOn      = w_pomanager_std.c_TextRotationOn
c_TextRotationOff     = w_pomanager_std.c_TextRotationOff

//------------------------------------------------------------------
//  Initialize the current tab options.
//------------------------------------------------------------------

c_TextCurrentRegular   = w_pomanager_std.c_TextCurrentRegular
c_TextCurrentBold      = w_pomanager_std.c_TextCurrentBold
c_TextCurrentItalic    = w_pomanager_std.c_TextCurrentItalic
c_TextCurrentUnderline = w_pomanager_std.c_TextCurrentUnderline

c_TextCurrentBlack       = w_pomanager_std.c_TextCurrentBlack
c_TextCurrentWhite       = w_pomanager_std.c_TextCurrentWhite
c_TextCurrentGray        = w_pomanager_std.c_TextCurrentGray
c_TextCurrentDarkGray    = w_pomanager_std.c_TextCurrentDarkGray
c_TextCurrentRed         = w_pomanager_std.c_TextCurrentRed
c_TextCurrentDarkRed     = w_pomanager_std.c_TextCurrentDarkRed
c_TextCurrentGreen       = w_pomanager_std.c_TextCurrentGreen
c_TextCurrentDarkGreen   = w_pomanager_std.c_TextCurrentDarkGreen
c_TextCurrentBlue        = w_pomanager_std.c_TextCurrentBlue
c_TextCurrentDarkBlue    = w_pomanager_std.c_TextCurrentDarkBlue
c_TextCurrentMagenta     = w_pomanager_std.c_TextCurrentMagenta
c_TextCurrentDarkMagenta = w_pomanager_std.c_TextCurrentDarkMagenta
c_TextCurrentCyan        = w_pomanager_std.c_TextCurrentCyan
c_TextCurrentDarkCyan    = w_pomanager_std.c_TextCurrentDarkCyan
c_TextCurrentYellow      = w_pomanager_std.c_TextCurrentYellow
c_TextCurrentDarkYellow  = w_pomanager_std.c_TextCurrentDarkYellow

c_TabCurrentGray        = w_pomanager_std.c_TabCurrentGray
c_TabCurrentBlack       = w_pomanager_std.c_TabCurrentBlack
c_TabCurrentWhite       = w_pomanager_std.c_TabCurrentWhite
c_TabCurrentDarkGray    = w_pomanager_std.c_TabCurrentDarkGray
c_TabCurrentRed         = w_pomanager_std.c_TabCurrentRed
c_TabCurrentDarkRed     = w_pomanager_std.c_TabCurrentDarkRed
c_TabCurrentGreen       = w_pomanager_std.c_TabCurrentGreen
c_TabCurrentDarkGreen   = w_pomanager_std.c_TabCurrentDarkGreen
c_TabCurrentBlue        = w_pomanager_std.c_TabCurrentBlue
c_TabCurrentDarkBlue    = w_pomanager_std.c_TabCurrentDarkBlue
c_TabCurrentMagenta     = w_pomanager_std.c_TabCurrentMagenta
c_TabCurrentDarkMagenta = w_pomanager_std.c_TabCurrentDarkMagenta
c_TabCurrentCyan        = w_pomanager_std.c_TabCurrentCyan
c_TabCurrentDarkCyan    = w_pomanager_std.c_TabCurrentDarkCyan
c_TabCurrentYellow      = w_pomanager_std.c_TabCurrentYellow
c_TabCurrentDarkYellow  = w_pomanager_std.c_TabCurrentDarkYellow

c_TabCurrentColorFolder = w_pomanager_std.c_TabCurrentColorFolder
c_TabCurrentColorBorder = w_pomanager_std.c_TabCurrentColorBorder
c_TabCurrentColorTab    = w_pomanager_std.c_TabCurrentColorTab

//------------------------------------------------------------------
//  Initialize the disabled tab options.
//------------------------------------------------------------------

c_TextDisableRegular   = w_pomanager_std.c_TextDisableRegular
c_TextDisableBold      = w_pomanager_std.c_TextDisableBold
c_TextDisableItalic    = w_pomanager_std.c_TextDisableItalic
c_TextDisableUnderline = w_pomanager_std.c_TextDisableUnderline

c_TextDisableBlack       = w_pomanager_std.c_TextDisableBlack
c_TextDisableWhite       = w_pomanager_std.c_TextDisableWhite
c_TextDisableGray        = w_pomanager_std.c_TextDisableGray
c_TextDisableDarkGray    = w_pomanager_std.c_TextDisableDarkGray
c_TextDisableRed         = w_pomanager_std.c_TextDisableRed
c_TextDisableDarkRed     = w_pomanager_std.c_TextDisableDarkRed
c_TextDisableGreen       = w_pomanager_std.c_TextDisableGreen
c_TextDisableDarkGreen   = w_pomanager_std.c_TextDisableDarkGreen
c_TextDisableBlue        = w_pomanager_std.c_TextDisableBlue
c_TextDisableDarkBlue    = w_pomanager_std.c_TextDisableDarkBlue
c_TextDisableMagenta     = w_pomanager_std.c_TextDisableMagenta
c_TextDisableDarkMagenta = w_pomanager_std.c_TextDisableDarkMagenta
c_TextDisableCyan        = w_pomanager_std.c_TextDisableCyan
c_TextDisableDarkCyan    = w_pomanager_std.c_TextDisableDarkCyan
c_TextDisableYellow      = w_pomanager_std.c_TextDisableYellow
c_TextDisableDarkYellow  = w_pomanager_std.c_TextDisableDarkYellow

//------------------------------------------------------------------
//  Set the instance variables with the default options.
//------------------------------------------------------------------

fu_SetOptions("Folder", "Defaults", 0)
fu_SetOptions("Tab", "Defaults", 0)
fu_SetOptions("TabCurrent", "Defaults", 0)
fu_SetOptions("TabDisable", "Defaults", 0)

end on

on clicked;//******************************************************************
//  PO Module     : u_Folder
//  Event         : Clicked
//  Description   : Determines if a tab has been clicked.
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER l_ClickedTab, l_ClickedPos
STRING  l_ClickedObject

//------------------------------------------------------------------
//  Get the object in the DataWindow that was clicked on.  
//------------------------------------------------------------------

l_ClickedObject = GetObjectAtPointer()

IF l_ClickedObject = "" THEN
	RETURN
END IF

//------------------------------------------------------------------
//  Determine if a tab was clicked on.  
//------------------------------------------------------------------

l_ClickedObject = MID(l_ClickedObject, 1, POS(l_ClickedObject, CHAR(9)) - 1)

IF Left(l_ClickedObject, 3) <> "tab" THEN
   RETURN
END IF

//------------------------------------------------------------------
//  Get the tab number and use it to select the tab.  
//------------------------------------------------------------------

i_ClickedTab = 0
l_ClickedPos = Integer(Mid(l_ClickedObject, 4, Pos(l_ClickedObject, "_") - 4))
l_ClickedTab = Integer(Mid(GetItemString(1, "t_pos"), (l_ClickedPos - 1) * 8 + 2, 2))

fu_SelectTab(l_ClickedTab)

end on

on resize;//******************************************************************
//  PO Module     : u_Folder
//  Event         : Resize
//  Description   : Resize the folder control.
//
//  Change History: 
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1994-1995.  All Rights Reserved.
//******************************************************************

INTEGER    l_FolderX, l_FolderY, l_FolderWidth, l_FolderHeight
INTEGER    l_ObjectWidth, l_ObjectHeight, l_X, l_Y, l_Idx, l_Jdx
DRAGOBJECT l_Object

//------------------------------------------------------------------
//  If the resize option is on, resize the folder object when 
//  the control has been resized by its parent.
//------------------------------------------------------------------

IF GetItemString(1, "f_resize") = "Y" AND i_FolderResize THEN
   fu_FolderResize()

   fu_FolderWorkSpace(l_FolderX, l_FolderY, &
                      l_FolderWidth, l_FolderHeight)

   //---------------------------------------------------------------
   //  If any of the objects have been centered before, re-center
   //  them after the resize.
   //---------------------------------------------------------------

   FOR l_Idx = 1 TO i_Tabs
      IF i_TabArray[l_Idx].TabNumObjects > 0 THEN
         FOR l_Jdx = 1 TO i_TabArray[l_Idx].TabNumObjects
            IF i_TabArray[l_Idx].TabObjectCentered[l_Jdx] THEN
               l_Object = i_TabArray[l_Idx].TabObjects[l_Jdx]
               l_ObjectWidth  = l_Object.Width
               l_ObjectHeight = l_Object.Height

               IF l_ObjectWidth > l_FolderWidth THEN
                  l_X = l_FolderX
               ELSE
                  l_X = ((l_FolderWidth - l_ObjectWidth) / 2) + l_FolderX
               END IF

               IF l_ObjectHeight > l_FolderHeight THEN
                  l_Y = l_FolderY
               ELSE
                  l_Y = ((l_FolderHeight - l_ObjectHeight) / 2) + l_FolderY
               END IF

               l_Object.Move(l_X, l_Y)
            END IF
         NEXT
      END IF
   NEXT
END IF
end on

on u_folder.create
end on

on u_folder.destroy
end on

